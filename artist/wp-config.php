<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rowberts_artist');

/** MySQL database username */
define('DB_USER', 'rowberts_staging');

/** MySQL database password */
define('DB_PASSWORD', 'Ma&7HZXuxR');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 */
define('AUTH_KEY',         '=PmVO2)4*||Cz#?ThXnYMaOsO.Iky#aWD>!=C4hWfH|x83c{~l2O~L/>WE9=P!+A');
define('SECURE_AUTH_KEY',  'SH)n#M)f,$kjuF#X}L_Q>5Qa/|BN|?l1pr?Oc|Wu2+5xqy^e`z-Ioc)BO`s]-Cwh');
define('LOGGED_IN_KEY',    'lU+GpDg,%#NAXb.q:$Oky%7j?e#c[qEiZJ)B2yV08Q.`6F)s.3d{m/nE6Y25:(av');
define('NONCE_KEY',        'pJVz7yP|IJ$t?NpliOmdv8ap:U8cZ5+u?PU83 Um`m&k,6W$$21DaqIWf%!dc7o1');
define('AUTH_SALT',        's+/$1{4?Q0pc_|I3KyP&_X:75q5+FMD<v!p$]-Z|S<48t?gI~|#>v1,NriJ#3yDV');
define('SECURE_AUTH_SALT', 'g2-Yho;|=tb-[n{,a]2+%G,{e.IiQP5c|ZifvA1&+axVKX+t!vR?.B( |`.>%r?K');
define('LOGGED_IN_SALT',   '[(0!q+OPIGW`sg/Q$Q]sZQb^6HNmOY&)x19P&P&SK9;.#.))+O?&;!rNX<?4WU-e');
define('NONCE_SALT',       'f[e>MZ4~Y[_Buf4|S1)VKHe|$p=r4=HFDZ *H,h;CPsQO=H%YK=7)9xTR-NI$+V?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/*Enable Auto Updates for WP Core (Major), Themes & Plugins by CER */
define('WP_AUTO_UPDATE_CORE', true);
add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );
