msgid ""
msgstr ""
"Project-Id-Version: The 411 Pro Theme for Genesis Framework\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-12-01 22:33-0500\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;"
"_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2\n"
"X-Textdomain-Support: yes\n"
"X-Poedit-Basepath: ..\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-SearchPath-0: .\n"

#: functions.php:9
msgid "The 411 Pro Theme"
msgstr ""

#: functions.php:204
msgid "Continue Reading"
msgstr ""

#: functions.php:253
msgid "Welcome Message"
msgstr ""

#: functions.php:254
msgid "This is the welcome message widget area."
msgstr ""

#: functions.php:258
msgid "Social Icons"
msgstr ""

#: functions.php:259
msgid "This is the social icons widget area."
msgstr ""

#: functions.php:263
msgid "Click Here"
msgstr ""

#: functions.php:264
msgid "This is the click here widget area."
msgstr ""

#: functions.php:268
msgid "After Entry"
msgstr ""

#: functions.php:269
msgid "This is the after entry widget area."
msgstr ""
