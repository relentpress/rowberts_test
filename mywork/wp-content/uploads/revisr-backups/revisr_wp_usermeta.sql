
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;
INSERT INTO `wp_usermeta` VALUES (1,1,'first_name',''),(2,1,'last_name',''),(3,1,'nickname','relentpress'),(4,1,'description',''),(5,1,'rich_editing','true'),(6,1,'comment_shortcuts','false'),(7,1,'admin_color','fresh'),(8,1,'use_ssl','0'),(9,1,'show_admin_bar_front','true'),(10,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(11,1,'wp_user_level','10'),(12,1,'dismissed_wp_pointers','wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),(13,1,'show_welcome_panel','0'),(14,1,'wp_dashboard_quick_press_last_post_id','95'),(15,1,'session_tokens','a:3:{s:64:\"32ea3d22ee48ca0022d49ebfad5ed24d33fd55a5267253c13d0a8c58fdd0f536\";i:1418266894;s:64:\"ad5a64bc16974f1e888723f3ede5d032ca536ea34f3aca05e11fd037685fe2fd\";i:1418268626;s:64:\"f97b79ac98e154f78f7d23e5ba483821e0bd2983083d1336fb1e591e802e666f\";i:1418268696;}'),(16,1,'_yoast_wpseo_profile_updated','1414551840'),(17,1,'wpseo_title',''),(18,1,'wpseo_metadesc',''),(19,1,'wpseo_metakey',''),(20,1,'wpseo_excludeauthorsitemap',''),(21,1,'googleplus',''),(22,1,'twitter',''),(23,1,'facebook',''),(24,1,'source_domain','mywork.rowberts.com'),(25,1,'primary_blog','1'),(26,2,'nickname','artist'),(27,2,'first_name',''),(28,2,'last_name',''),(29,2,'description',''),(30,2,'rich_editing','true'),(31,2,'comment_shortcuts','false'),(32,2,'admin_color','fresh'),(33,2,'use_ssl','0'),(34,2,'show_admin_bar_front','true'),(37,2,'dismissed_wp_pointers','wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),(38,2,'show_welcome_panel','2'),(39,2,'primary_blog','2'),(40,2,'source_domain','mywork.rowberts.com'),(41,2,'wp_2_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(42,2,'wp_2_user_level','10'),(43,2,'wp_3_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(44,2,'wp_3_user_level','10'),(45,2,'wp_4_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(46,2,'wp_4_user_level','10'),(47,2,'wp_5_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(48,2,'wp_5_user_level','10'),(49,2,'wp_6_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(50,2,'wp_6_user_level','10'),(51,2,'wp_7_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(52,2,'wp_7_user_level','10'),(53,2,'wp_8_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(54,2,'wp_8_user_level','10'),(55,2,'wp_9_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(56,2,'wp_9_user_level','10');
/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

