
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_blc_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_blc_links` (
  `link_id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `first_failure` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_check` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_success` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_check_attempt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `check_count` int(4) unsigned NOT NULL DEFAULT '0',
  `final_url` text CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `redirect_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `log` text NOT NULL,
  `http_code` smallint(6) NOT NULL DEFAULT '0',
  `status_code` varchar(100) DEFAULT '',
  `status_text` varchar(250) DEFAULT '',
  `request_duration` float NOT NULL DEFAULT '0',
  `timeout` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `broken` tinyint(1) NOT NULL DEFAULT '0',
  `may_recheck` tinyint(1) NOT NULL DEFAULT '1',
  `being_checked` tinyint(1) NOT NULL DEFAULT '0',
  `result_hash` varchar(200) NOT NULL DEFAULT '',
  `false_positive` tinyint(1) NOT NULL DEFAULT '0',
  `dismissed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  KEY `url` (`url`(150)),
  KEY `final_url` (`final_url`(150)),
  KEY `http_code` (`http_code`),
  KEY `broken` (`broken`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_blc_links` WRITE;
/*!40000 ALTER TABLE `wp_blc_links` DISABLE KEYS */;
INSERT INTO `wp_blc_links` VALUES (1,'https://wordpress.org/','0000-00-00 00:00:00','2014-10-11 04:15:17','2014-10-11 04:15:17','2014-10-11 04:15:17',0,'https://wordpress.org/',0,'=== HTTP code : 200 ===\n\nHTTP/1.1 200 OK\r\nServer: nginx\r\nDate: Sat, 11 Oct 2014 04:15:18 GMT\r\nContent-Type: text/html; charset=utf-8\r\nTransfer-Encoding: chunked\r\nConnection: close\r\nVary: Accept-Encoding\r\nX-Frame-Options: SAMEORIGIN\r\nX-nc: HIT lax 249\r\n\r\n\nLink is valid.',200,'','',0.798376,0,0,1,0,'200|0|0|https://wordpress.org/',0,0),(2,'http://mywork.rowberts.com/wp-admin/','0000-00-00 00:00:00','2014-10-11 04:15:18','2014-10-11 04:15:18','2014-10-11 04:15:18',0,'http://mywork.rowberts.com/wp-login.php?redirect_to=http%3A%2F%2Ftemplate.rowberts.com.au%2Fwp-admin%2F&reauth=1',1,'=== HTTP code : 200 ===\n\nHTTP/1.1 302 Found\r\nServer: nginx\r\nDate: Sat, 11 Oct 2014 04:15:19 GMT\r\nContent-Type: text/html\r\nConnection: close\r\nExpires: Wed, 11 Jan 1984 05:00:00 GMT\r\nCache-Control: no-cache, must-revalidate, max-age=0\r\nPragma: no-cache\r\nX-NewRelic-App-Data: PxQBU1NWCAQTV1hXDwkGVkYdFGQHBDcQUQxLA1tMXV1dORY0QwhvTRNGGgACDlFdbEsIDFNUT0gUWUYRTlZNUgBSCFIUCBoLBVwOVB1MAk5GUg9RB1YNVgJQVVtRCQICVRMaVQMKEAdt\r\nSet-Cookie: wfvt_2356029004=5438aed7a3bf1; expires=Sat, 11-Oct-2014 04:45:19 GMT; Max-Age=1800; path=/; httponly\r\nLocation: http://mywork.rowberts.com/wp-login.php?redirect_to=http%3A%2F%2Ftemplate.rowberts.com.au%2Fwp-admin%2F&reauth=1\r\n\r\nHTTP/1.1 200 OK\r\nServer: nginx\r\nDate: Sat, 11 Oct 2014 04:15:20 GMT\r\nContent-Type: text/html; charset=UTF-8\r\nConnection: close\r\nExpires: Wed, 11 Jan 1984 05:00:00 GMT\r\nCache-Control: no-cache, must-revalidate, max-age=0\r\nPragma: no-cache\r\nX-Frame-Options: SAMEORIGIN\r\nX-NewRelic-App-Data: PxQBU1NWCAQTV1hXDwkGVkYdFGQHBDcQUQxLA1tMXV1dORY0QwhvTRNGGg0JBFFdHhQJEhUdB0hUAQYDUkpTTAZUAFYMFBkDH0cIAFJXBAdXBgBYAgAOAwBQQ05RUFsVAWw=\r\nSet-Cookie: wfvt_2356029004=5438aed86190e; expires=Sat, 11-Oct-2014 04:45:20 GMT; Max-Age=1800; path=/; httponly\r\nSet-Cookie: wordpress_test_cookie=WP+Cookie+check; path=/\r\nSet-Cookie: wordpress_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/wp-admin\r\nSet-Cookie: wordpress_sec_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/wp-admin\r\nSet-Cookie: wordpress_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/wp-content/plugins\r\nSet-Cookie: wordpress_sec_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/wp-content/plugins\r\nSet-Cookie: wordpress_logged_in_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/\r\nSet-Cookie: wordpress_logged_in_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/\r\nSet-Cookie: wordpress_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/\r\nSet-Cookie: wordpress_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/\r\nSet-Cookie: wordpress_sec_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/\r\nSet-Cookie: wordpress_sec_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/\r\nSet-Cookie: wordpressuser_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/\r\nSet-Cookie: wordpresspass_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/\r\nSet-Cookie: wordpressuser_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/\r\nSet-Cookie: wordpresspass_3396ffa607780cabb658aa002fc3bed4=+; expires=Fri, 11-Oct-2013 04:15:20 GMT; Max-Age=-31536000; path=/\r\n\r\n\nLink is valid.',200,'','',1.92278,0,0,1,0,'200|0|0|http://mywork.rowberts.com/wp-login.php',0,0);
/*!40000 ALTER TABLE `wp_blc_links` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

