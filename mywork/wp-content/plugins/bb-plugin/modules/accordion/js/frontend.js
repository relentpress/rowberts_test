(function($) {
    
    $('.fl-accordion-button').click(function() {
    
        var button      = $(this),
            accordion   = button.closest('.fl-accordion'),
            allContent  = $('.fl-accordion-content'),
            allIcons    = $('.fl-accordion-button i'),
            content     = button.siblings('.fl-accordion-content'),
            icon        = button.find('i');
    
        if(accordion.hasClass('fl-accordion-collapse')) {
            allContent.slideUp('normal');	
            allIcons.removeClass('fa-minus');
            allIcons.addClass('fa-plus');
        }
        
        if(content.is(':hidden')) {
            content.slideDown('normal');
            icon.addClass('fa-minus');
            icon.removeClass('fa-plus');
        }
        else {
            content.slideUp('normal');
            icon.addClass('fa-plus');
            icon.removeClass('fa-minus');
        }
    });
    
})(jQuery);