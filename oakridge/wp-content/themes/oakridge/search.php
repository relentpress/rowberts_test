<?php
	/* Template Name: Search */
	m("head");
?>
	<section id="single">
		<div class="wrap">
			<?php get_search_form(); ?>
			<div class="single-border"><br /><br />
				<h1 class="single-title">You are searching for: <b><?php echo $s; ?></b></h1><br />
<?php 
// the query
$the_query = new WP_Query( 's='.$s ); ?>

<?php if ( $the_query->have_posts() ) : ?>

	<!-- pagination here -->

	<!-- the loop -->

	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
		<span style="position:relative; font-size:11px; color:#ccc;top: -10px;left: 2px;"><?php the_time('F j Y'); ?></span>
		<section style="">
			<?php 
				$yoast_meta = get_post_meta($post->ID, '_yoast_wpseo_metadesc', true);
				if ($yoast_meta) { //check if the variable(with meta value) isn't empty
				    echo $yoast_meta;
				}
			?>
		</section>
	<?php endwhile; ?>
	<!-- end of the loop -->

	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

			</div>
		</div>
	</section>	


<?php
	m("foot");
 ?>




