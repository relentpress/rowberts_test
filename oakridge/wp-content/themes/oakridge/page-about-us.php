<?php /* Template Name: About Us - Template */
	m("head");	
?>

<section id="about-us">
	<div class="wrap">
		<div class="border">
			<section>
				<aside>
					<?php /* <h2 class="title">Trade Tools</h2>
					<ul class="menu clearfix">
						<li><a href="#">OZV</a></li>
						<li><a href="#">Lode Estates</a></li>
						<li><a href="#">Helena Ranch</a></li>
						<li><a href="#">Maggio</a></li>
						<li><a href="#">Moss Roxx</a></li>
						<li><a href="#">Silk Oak</a></li>
						<li><a href="#">Old Soul</a></li>
						<li><a href="#">3 Girls</a></li>
						<li><a href="#">Gnarled Vine</a></li>
					</ul> */ ?>
					<?php dynamic_sidebar('about-us-sidebar'); ?>
				</aside>
				<article class="row">
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>  
								<?php the_content(); ?>
						<?php endwhile; ?>
					<?php endif; ?> 
				</article>
				<div class="clearfix"></div>
			</section>
		</div>
	</div>
</section>

<?php
	m("foot");
?>