<?php /* Template Name: Default Template */
	m("head");	
?>

<section id="page">
	<div class="wrap">
		<div class="border">
			<section>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<aside>
							<?php the_title('<h2 class="title">', '</h2>'); ?>
							<?php dynamic_sidebar('default-sidebar'); ?>
						</aside>
						<article class="row">    
							<?php the_content(); ?>
						</article>
						<div class="clearfix"></div>
					<?php endwhile; ?>
				<?php endif; ?> 
			</section>
		</div>
	</div>
</section>

<?php
	m("foot");
?>