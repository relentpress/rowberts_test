<?php
	/* Template Name: Single Post */
	m("head");
?>
	<section id="single">
		<div class="wrap">
			<?php get_search_form(); ?>
			<div class="single-border">
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>    
						<br /><br />
						<?php the_title('<h1 class="single-title">', '</h1>'); ?>
						<?php if ( has_post_thumbnail() ) {	
						?>
							<div class="single-post-thumbnail"><?php the_post_thumbnail(); ?></div>
						<?php
						}  ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?> 
			</div>
		</div>
	</section>		
<?php
	m("foot");
 ?>


