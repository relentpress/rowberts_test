<div style="padding:30px 0">
	<div id="timeline" data-columns>
	<?php $query = new WP_Query( 'posts_per_page=30' ); ?>
		<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();  ?>
		<div class="item">
			<div class="item-border">
				<h3><?php the_time("F d, Y"); ?></h3>
				<?php the_title('<a class="title" href="'.get_the_permalink().'">', '</a>'); ?>
				<?php if ( has_post_thumbnail() ) {	the_post_thumbnail("blog-post-thumbnail"); }  ?>
				<?php the_content(); ?>
				<a href="<?php the_permalink(); ?>" class="read-more">Read More...</a>
			</div>			
		</div>		
		<?php endwhile; ?>
		<?php else: ?>
			<h1>The BLOG is all good but no posts yet.</h1>  
        <?php endif; ?>
	</div>			
</div>				