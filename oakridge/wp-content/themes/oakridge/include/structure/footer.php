  <footer id="section">
    <div class="wrap">

      <!-- FOOTER RIGHT -->

      <div id="footer-right">
        <?php dynamic_sidebar('footer-menu'); ?>
        <div class="clearfix"></div>
      </div>

      <!-- FOOTER RIGHT END -->

      <!-- FOOTER LEFT -->

      <div id="footer-left">
          <a id="footer-logo" href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo("stylesheet_directory"); ?>/images/footer-logo.png"></a>

          <div id="social-media">
            <?php if($url=genesis_get_option( 'behance', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-behance"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'delicious', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-delicious"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'devianart', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-devianart"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'digg', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-digg"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'dribble', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-dribble"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'facebook', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-facebook"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'googleplus', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-google-plus"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'instagram', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-instagram"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'linkedin', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-linkedin"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'pinterest', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-pinterest"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'reddit', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-reddit"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'skype', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-skype"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'stumbleupon', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-stumbleupon"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'tumblr', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-tumblr"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'twitter', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-twitter"></i></a>'; } ?>
            <?php if($url=genesis_get_option( 'youtube', 'home-settings' )){echo '<a href="'.$url.'"><i class="icon-play"></i></a>'; } ?>
          </div>

          <div itemscope itemtype="http://schema.org/localBusiness">
            <span itemprop="address"><?php echo ($val = genesis_get_option( 'footerAddress', 'home-settings' )) ? $val : ""; ?></span><br />
            <span itemprop="telephone"><?php echo ($val = genesis_get_option( 'footerTelephone', 'home-settings' )) ? $val : ""; ?></span><br />
            <span itemprop="email"><?php echo ($val = genesis_get_option( 'footerEmail', 'home-settings' )) ? $val : ""; ?></span>
          </div>
      </div>

      <!-- FOOTER LEFT END -->

      <div class="clearfix"></div>
    </div>
  </footer>

  <footer id="copyright">
    <div class="wrap">
      <p><?php echo ($val = genesis_get_option( 'footerCopy', 'home-settings' )) ? $val : "© 2014 oak ridge winery all rights reserved"; ?></p>
    </div>
  </footer>

<?php wp_footer(); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php bloginfo("stylesheet_directory"); ?>/js/jquery.min.js"></script>
    <script type="text/javascript">
        var dir = "<?php bloginfo("stylesheet_directory"); ?>/images/";
    </script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php bloginfo("stylesheet_directory"); ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo("stylesheet_directory"); ?>/js/html5Preloader.js"></script>
    <script src="<?php bloginfo("stylesheet_directory"); ?>/js/rotary_extension.js"></script>
    <script src="<?php bloginfo("stylesheet_directory"); ?>/js/main.js"></script>
  </body>
</html>