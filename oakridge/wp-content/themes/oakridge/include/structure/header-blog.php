<!DOCTYPE html>
<html <?php language_attributes( 'html' ); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
    <link rel="icon" type="image/png" href="favicon.png">
    <!--[if IE]><link rel="shortcut icon" href="favicon.ico"/><![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Cinzel:400,700,900|EB+Garamond|Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800|Prata' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="<?php bloginfo("stylesheet_directory"); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php bloginfo("stylesheet_directory"); ?>/css/animate.css" rel="stylesheet">
    <link href="<?php bloginfo("stylesheet_directory"); ?>/css/design.elements.css" rel="stylesheet">
    <link href="<?php bloginfo("stylesheet_directory"); ?>/masonry/css/salvattore.css" rel="stylesheet">
    <link href="<?php bloginfo("stylesheet_directory"); ?>/masonry/css/normalize.css" rel="stylesheet">
    <link href="<?php bloginfo("stylesheet_directory"); ?>/style.css" rel="stylesheet">
    <link href="<?php bloginfo("stylesheet_directory"); ?>/css/responsive.css" rel="stylesheet">
    <link href="<?php bloginfo("stylesheet_directory"); ?>/css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
  </head>

    <body>

  <header>
    <div id="brands-container"  class="smoothScale">
      <div class="wrap">
        <div id="logo-container">
          <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo("stylesheet_directory"); ?>/images/logo.png"></a>
        </div>

      <input type="checkbox" name="mobile-menu-brands" id="mobile-menu-brands" />
      <label for="mobile-menu-brands"></label>
        <ul id="brands" class="animated fadeInDown">
          <li><a href="#" class="icon-oak-ridge-winery"></a></li>
          <li><a href="#" class="icon-ozv"></a></li>
          <li><a href="#" class="icon-lodi-estates"></a></li>
          <li><a href="#" class="icon-helena-ranch"></a></li>
          <li><a href="#" class="icon-maggio"></a></li>
          <li><a href="#" class="icon-moss-roxx"></a></li>
          <li><a href="#" class="icon-silk-oak"></a></li>
          <li><a href="#" class="icon-old-soul"></a></li>
        </ul>
      </div>
    </div>
    <div id="navigation-container" class="smoothScale">
      <div class="wrap">
      <div class="clearfix"></div>

      <input type="checkbox" name="mobile-menu-primary-secondary" id="mobile-menu-primary-secondary" />
      <label for="mobile-menu-primary-secondary"></label>

        <div id="secondary-container">
          <form id="mailing-list" action="subscribe" class="animated fadeInDownBig">
            <input type="text" name="s" id="subscribe" placeholder="join our mailing list">
            <input type="submit" value="go">
          </form>

        <?php
          wp_nav_menu(
            array(
                'menu_class' => 'secondary-menu animated fadeInDownBig',
                'sort_order' => '',
                'menu' => 'Secondary Menu',
                'container'    => '',
                'theme_location'  => 'secondary_menu'
               )
            );
        ?>
        </div>

        <?php
          wp_nav_menu(
            array(
                'menu_class' => 'primary-menu animated fadeInDownBig',
                'sort_order' => '',
                'menu' => 'Primary Menu',
                'container'    => '',
                'theme_location'  => 'primary_menu'
               )
            );
        ?>

        
      </div>
    </div>
  </header>


