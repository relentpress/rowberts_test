<section id="tile-grid">
    <div class="wrap">
      <div id="grid-container">

        <span id="grid-column" class="add-bottom-border">  
          <div class="tile-style-1 smoothScale hover-opac">
            <div class="l">
              <img src="<?php bloginfo("stylesheet_directory"); ?>/images/homepage-image-1.png">
            </div>
            <div class="r">
              <h3>Wine Club</h3>
              <p>Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do eiusmod tempor incididunt ut labor.</p>
              <a href="#">readmore...</a>
              <a href="#" class="signup"><span>Sign Up</span></a>
            </div>
          </div>

          <div class="tile-style-2 hover-opac">
            <div class="r">
              <h3>EVENTS</h3>
              <p>Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do eiusmod tempor incididunt ut labor.</p>
              <a href="#">readmore...</a>
            </div>
            <div class="l">
              <img src="<?php bloginfo("stylesheet_directory"); ?>/images/homepage-image-3.png">
            </div>
          </div>
        </span>

        <span id="grid-column" class="add-top-border">  
          <div class="tile-style-2 smoothScale hover-opac">
            <div class="l">
              <img src="<?php bloginfo("stylesheet_directory"); ?>/images/homepage-image-4.png">
            </div>
            <div class="r orange">
              <h3>EVENTS</h3>
              <p>Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do eiusmod tempor incididunt ut labor.</p>
              <a href="#">readmore...</a>
            </div>
          </div>

          <div class="tile-style-1 hover-opac">
            <div class="l">
              <img src="<?php bloginfo("stylesheet_directory"); ?>/images/homepage-image-6.png">
            </div>
            <div class="r">
              <h3>Wine Club</h3>
              <p>Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do eiusmod tempor incididunt ut labor.</p>
              <a href="#">readmore...</a>
              <a href="#" class="signup"><span>Sign Up</span></a>
            </div>
          </div>
        </span>

        <span id="grid-column" class="add-top-border">  
          <div class="tile-style-2 smoothScale hover-opac">
            <div class="r">
              <h3>EVENTS</h3>
              <p>Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do eiusmod tempor incididunt ut labor.</p>
              <a href="#">readmore...</a>
            </div>
            <div class="l">
              <img src="<?php bloginfo("stylesheet_directory"); ?>/images/homepage-image-5.png">
            </div>
          </div>

          <div class="tile-style-1 hover-opac">
            <div class="l">
              <img src="<?php bloginfo("stylesheet_directory"); ?>/images/homepage-image-2.png">
            </div>
            <div class="r orange">
              <h3>Find Our Wines</h3>
              <p>Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do eiusmod tempor incididunt ut labor.</p>
              <a href="#">readmore...</a>
            </div>
          </div>
          
        </span>

        <div class="clearfix"></div>

      </div>
    </div>
  </section>