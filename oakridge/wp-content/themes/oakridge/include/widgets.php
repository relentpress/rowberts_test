<?php
#
	#
	unregister_sidebar( 'header-right' );
	unregister_sidebar( 'sidebar' );
	unregister_sidebar( 'sidebar-alt' );
	remove_theme_support( 'genesis-menus' );

	genesis_register_sidebar( array(
		'id' => 'whats-new',
		'name' => 'What\'s New',
		'description' => 'What\'s New At Oak Ridge',
		'before_title' => '<h3 class="style-middle-line"><span>',
		'after_title' => '</span></h3><br />',
	) );

	genesis_register_sidebar( array(
		'id' => 'default-sidebar',
		'name' => 'Default - Sidebar Menu',
		'description' => 'Default - Sidebar Menu',
		'before_title' => '<h2 class="title" style="display:none">',
		'after_title' => '</h2>',
	) );

	genesis_register_sidebar( array(
		'id' => 'trade-tools-sidebar',
		'name' => 'Trade Tools - Sidebar Menu',
		'description' => 'Trade Tools - Sidebar Menu',
		'before_title' => '<h2 class="title">',
		'after_title' => '</h2>',
	) );

	genesis_register_sidebar( array(
		'id' => 'about-us-sidebar',
		'name' => 'About Us - Sidebar Menu',
		'description' => 'About Us - Sidebar Menu',
		'before_title' => '<h2 class="title">',
		'after_title' => '</h2>',
	) );

	genesis_register_sidebar( array(
		'id' => 'footer-menu',
		'name' => 'Footer Menu',
		'description' => 'Footer Custom Menu',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );

	register_nav_menus( array(
		'secondary_menu' => 'Secondary Menu',
		'primary_menu' => 'Primary Menu',
	) );
	#
#
?>