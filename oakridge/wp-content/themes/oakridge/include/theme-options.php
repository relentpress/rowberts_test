<?php
class childThemeSettings_HOME extends Genesis_Admin_Boxes {
	
	function __construct() {
		
		// Specify a unique page ID. 
		$page_id = 'home-id';
		
		// Set it as a child to genesis, and define the menu and page titles
		$menu_ops = array(
			'submenu' => array(
				'parent_slug' => 'genesis',
				'page_title'  => 'Oak Ridge Winery - Home Page',
				'menu_title'  => 'Home Settings',
			)
		);
		
		// Set up page options. These are optional, so only uncomment if you want to change the defaults
		$page_ops = array(
		//	'screen_icon'       => 'options-general',
		//	'save_button_text'  => 'Save Settings',
		//	'reset_button_text' => 'Reset Settings',
		//	'save_notice_text'  => 'Settings saved.',
		//	'reset_notice_text' => 'Settings reset.',
		);		
		
		// Give it a unique settings field. 
		// You'll access them from genesis_get_option( 'option_name', 'child-settings' );
		$settings_field = 'home-settings';
		
		// Set the default values
		$default_settings = array(

			/*Welcome Text*/
			'welcomeTitle' => '',
			'welcomeMessage' => '',
			'welcomeImage' => '',

			/*Social Media*/
			'behance' => '',
			'delicious' => '',
			'devianart' => '',
			'digg' => '',
			'dribble' => '',
			'facebook'   => '',
			'googleplus' => '',
			'instagram' => '',
			'linkedin' => '',
			'pinterest' => '',
			'reddit' => '',
			'skype' => '',
			'stumbleupon' => '',
			'tumblr' => '',
			'twitter' => '',
			'youtube' => '',

			/*Footer*/
			'footerAddress' => '',
			'footerTelephone' => '',
			'footerEmail' => '',
			'footerCopy' => '',
		);
		
		// Create the Admin Page
		$this->create( $page_id, $menu_ops, $page_ops, $settings_field, $default_settings );

		// Initialize the Sanitization Filter
		add_action( 'genesis_settings_sanitizer_init', array( $this, 'sanitization_filters' ) );
			
	}

	function sanitization_filters() {
		
		genesis_add_option_filter( 'no_html', $this->settings_field,
			array(
				'phone',
				'address',
				'file_upload',
				'example-jpg-nonce',
			) );
	}
	
	 function help() {
	 	$screen = get_current_screen();

		$screen->add_help_tab( array(
			'id'      => 'sample-help', 
			'title'   => 'Sample Help',
			'content' => '<p>Help content goes here.</p>',
		) );
	 }
	

	function metaboxes() {

		add_meta_box('weclome_screen', 'Welcome Screen', array( $this, 'weclome_screen' ), $this->pagehook, 'main', 'high');
		
		add_meta_box('social_media', 'Social Media', array( $this, 'social_media' ), $this->pagehook, 'main', 'high');

		add_meta_box('Footer', 'Footer', array( $this, 'Footer' ), $this->pagehook, 'main', 'high');
		
	}

	function weclome_screen() {
		echo '<p>Background Image:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'welcomeImage' ) . '" id="' . $this->get_field_id( 'welcomeImage' ) . '" value="' . esc_attr( $this->get_field_value( 'welcomeImage' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Title:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'welcomeTitle' ) . '" id="' . $this->get_field_id( 'welcomeTitle' ) . '" value="' . esc_attr( $this->get_field_value( 'welcomeTitle' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Message:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'welcomeMessage' ) . '" id="' . $this->get_field_id( 'welcomeMessage' ) . '" value="' . esc_attr( $this->get_field_value( 'welcomeMessage' ) ) . '" size="50" />';
		echo '</p>';
	}

	function social_media() {
		
		echo '<p>Behance:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'behance' ) . '" id="' . $this->get_field_id( 'behance' ) . '" value="' . esc_attr( $this->get_field_value( 'behance' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Delicious:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'delicious' ) . '" id="' . $this->get_field_id( 'delicious' ) . '" value="' . esc_attr( $this->get_field_value( 'delicious' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Devianart:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'devianart' ) . '" id="' . $this->get_field_id( 'devianart' ) . '" value="' . esc_attr( $this->get_field_value( 'devianart' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Digg:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'digg' ) . '" id="' . $this->get_field_id( 'digg' ) . '" value="' . esc_attr( $this->get_field_value( 'digg' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Dribble:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'dribble' ) . '" id="' . $this->get_field_id( 'dribble' ) . '" value="' . esc_attr( $this->get_field_value( 'dribble' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Facebook:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'facebook' ) . '" id="' . $this->get_field_id( 'facebook' ) . '" value="' . esc_attr( $this->get_field_value( 'facebook' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Google Plus:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'googleplus' ) . '" id="' . $this->get_field_id( 'googleplus' ) . '" value="' . esc_attr( $this->get_field_value( 'googleplus' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Instagram:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'instagram' ) . '" id="' . $this->get_field_id( 'instagram' ) . '" value="' . esc_attr( $this->get_field_value( 'instagram' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Linkedin:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'linkedin' ) . '" id="' . $this->get_field_id( 'linkedin' ) . '" value="' . esc_attr( $this->get_field_value( 'linkedin' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Pinterest:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'pinterest' ) . '" id="' . $this->get_field_id( 'pinterest' ) . '" value="' . esc_attr( $this->get_field_value( 'pinterest' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Reddit:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'reddit' ) . '" id="' . $this->get_field_id( 'reddit' ) . '" value="' . esc_attr( $this->get_field_value( 'reddit' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Pinterest:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'pinterest' ) . '" id="' . $this->get_field_id( 'pinterest' ) . '" value="' . esc_attr( $this->get_field_value( 'pinterest' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Skype:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'skype' ) . '" id="' . $this->get_field_id( 'skype' ) . '" value="' . esc_attr( $this->get_field_value( 'skype' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Stumbleupon:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'stumbleupon' ) . '" id="' . $this->get_field_id( 'stumbleupon' ) . '" value="' . esc_attr( $this->get_field_value( 'stumbleupon' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Tumblr:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'tumblr' ) . '" id="' . $this->get_field_id( 'tumblr' ) . '" value="' . esc_attr( $this->get_field_value( 'tumblr' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Twitter:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'twitter' ) . '" id="' . $this->get_field_id( 'twitter' ) . '" value="' . esc_attr( $this->get_field_value( 'twitter' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Youtube:<br />';
		echo '<input type="text" name="' . $this->get_field_name( 'youtube' ) . '" id="' . $this->get_field_id( 'youtube' ) . '" value="' . esc_attr( $this->get_field_value( 'youtube' ) ) . '" size="50" />';
		echo '</p>';

	}

	function Footer() {
	
		echo '<p>Address: <br />';
		echo '<input type="text" name="' . $this->get_field_name( 'footerAddress' ) . '" id="' . $this->get_field_id( 'footerAddress' ) . '" value="' . esc_attr( $this->get_field_value( 'footerAddress' ) ) . '" size="50" />';
		echo '</p>';
	
		echo '<p>Telephone: <br />';
		echo '<input type="text" name="' . $this->get_field_name( 'footerTelephone' ) . '" id="' . $this->get_field_id( 'footerTelephone' ) . '" value="' . esc_attr( $this->get_field_value( 'footerTelephone' ) ) . '" size="50" />';
		echo '</p>';
	
		echo '<p>Email Address: <br />';
		echo '<input type="text" name="' . $this->get_field_name( 'footerEmail' ) . '" id="' . $this->get_field_id( 'footerEmail' ) . '" value="' . esc_attr( $this->get_field_value( 'footerEmail' ) ) . '" size="50" />';
		echo '</p>';

		echo '<p>Copyright Text: <br />';
		echo '<input type="text" name="' . $this->get_field_name( 'footerCopy' ) . '" id="' . $this->get_field_id( 'footerCopy' ) . '" value="' . esc_attr( $this->get_field_value( 'footerCopy' ) ) . '" size="50" />';
		echo '</p>';

	}
	
	
}



add_action( 'genesis_admin_menu', 'childThemeSettings_HOME_' );

function childThemeSettings_HOME_() {
	global $_child_theme_settings;
	$_child_theme_settings = new childThemeSettings_HOME;	 	
}