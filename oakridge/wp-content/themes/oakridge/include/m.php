<?php

	function m($PARAM){
		$par = strtolower($PARAM);
		switch($par){
			case "head":
				get_template_part("include/structure/header");	break;

			case "head-blog":
				get_template_part("include/structure/header-blog");	break;

			case "head-wine":
				get_template_part("include/structure/header-wine");	break;

			case "foot":
				get_template_part("include/structure/footer");	break;

			case "foot-blog":
				get_template_part("include/structure/footer-blog");	break;

			case "tile-grid":
				get_template_part("include/structure/tile-grid");	break;

			case "loop-blog":
				get_template_part("include/structure/loop-blog");	break;
		}
	}

?>