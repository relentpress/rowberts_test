<?php

	function imgDir(){
		echo get_bloginfo("stylesheet_directory") . '/images';
	}

	function searchForm() {
		return
				'<form role="search" method="get" id="search-bar" class="animated fadeInDown smoothScale" action="' . home_url( '/' ) . '" >
				<input type="text" placeholder="search this website" value="' . get_search_query() . '" name="s" id="s" />
				<input type="submit" value="'. esc_attr__( 'go' ) .'" />
				</form>';
	}

	add_image_size( "blog-post-thumbnail", 270, 300, true );
	add_image_size( "single-post-thumbnail", 270, 300, true ); 

	add_filter( 'get_search_form', 'searchForm' );

?>