<?php
	function mdir($att) {
		$att = shortcode_atts(
			array(
				'type' => ' ',
			), $att, ' ' );

		return get_bloginfo("stylesheet_directory") . '/'. $att['type'];
	}
	add_shortcode('mdir', 'mdir');

?>