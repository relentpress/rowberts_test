<?php /* Template Name: Club ~ Purchase Template */
	m("head");	
?>

<section id="purchase">
	<div class="wrap">
		<div class="border">
			<section>


		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<?php the_title('<h3 class="style-middle-line2"><span> ', '</span></h3><br/><br/>'); ?>
				<article class="row">    
					<?php the_content(); ?>
				</article>
			<?php endwhile; ?>
		<?php endif; ?> 

				<aside class="row">
						<form class="club">
							<div class="col-md-12">
								<h5>Select Barrel Club Membership*</h5>
								<label><input type="checkbox"> VIP</label> <br />
								<label><input type="checkbox"> American Oak</label> <br />
								<label><input type="checkbox"> French Oak</label> <br /><br />
							</div>

								<div class="clearfix"></div>

							<div class="col-md-12">
								<input type="text" placeholder="name">
								<input type="email" placeholder="email">
								<input type="text" placeholder="address">
								<input type="text" placeholder="city">
							</div>

								<div class="clearfix"></div>

							<div class="col-md-2">
								<p class="text-style-1">state</p>
							</div>
							<div class="col-md-5 no-padding-left">
								<input type="text" placeholder="chooose your state">
							</div>
							<div class="col-md-5 no-padding-left">
								<input type="text" placeholder="zip">
							</div>

								<div class="clearfix"></div>

							<div class="col-md-2">
								<p class="text-style-1">tel#</p>
							</div>
							<div class="col-md-3 no-padding-left">
								<input type="text" placeholder="area code">
							</div>
							<div class="col-md-3 no-padding-left">
								<input type="text" placeholder="">
							</div>
							<div class="col-md-4 no-padding-left">
								<input type="text" placeholder="">
							</div>

								<div class="clearfix"></div>

							<div class="col-md-3">
								<p class="text-style-1">birthday</p>
							</div>
							<div class="col-md-3 no-padding-left">
								<input type="text" placeholder="mm">
							</div>
							<div class="col-md-3 no-padding-left">
								<input type="text" placeholder="dd">
							</div>
							<div class="col-md-3 no-padding-left">
								<input type="text" placeholder="yyyy">
							</div>

								<div class="clearfix"></div>

							<div class="col-md-12">	
								<h5>Ship</h5>
								<label class="text-style-1"><input type="checkbox"> ship</label> <br />
								<label class="text-style-1"><input type="checkbox"> pick-up</label> <br /><br />
							</div>

								<div class="clearfix"></div>	

							<div class="col-md-12">
								<h5>Shipping Address (If Different)</h5>
								<input type="text" placeholder="name">
								<input type="email" placeholder="email">
								<input type="text" placeholder="address">
								<input type="text" placeholder="city">
							</div>

								<div class="clearfix"></div>	

							<div class="col-md-2">
								<p class="text-style-1">state</p>
							</div>
							<div class="col-md-5 no-padding-left">
								<input type="text" placeholder="chooose your state">
							</div>
							<div class="col-md-5 no-padding-left">
								<input type="text" placeholder="zip">
							</div>		

								<div class="clearfix"></div>	
							
							<div class="col-md-12">
								<h5>Payment Information</h5>
							</div>
							<div class="col-md-4">
								<label><input type="checkbox"> Visa</label> 
							</div>
							<div class="col-md-3">
								<label><input type="checkbox"> Mc</label> 
							</div>
							<div class="col-md-5">
								<label><input type="checkbox"> Discover</label> 
							</div>

								<div class="clearfix"></div>	

							<div class="col-md-12">
								<h5>Shipping Address (If Different)</h5>
								<input type="text" placeholder="card #">
							</div>		

								<div class="clearfix"></div>	

							<div class="col-md-3">
								<p class="text-style-1">ex. date</p>
							</div>
							<div class="col-md-3 no-padding-left">
								<input type="text" placeholder="mm">
							</div>
							<div class="col-md-3 no-padding-left">
								<input type="text" placeholder="dd">
							</div>	
							<div class="col-md-3 no-padding-left">
								<input type="text" placeholder="yyyy">
							</div>	

								<div class="clearfix"></div>	

							<div class="col-md-4">
								<p class="text-style-1">3-digit code</p>
							</div>
							<div class="col-md-3 no-padding-left">
								<input type="text" placeholder="">
							</div>					
									
								<div class="clearfix"></div>
							<div class="col-md-12">	
								<p class="fs11 font-family-open-sans">* You may cancel at any time.</p>
							</div>		
						</form>
				</aside>
				<div class="clearfix"></div>
			</section>
		</div> 
	</div>
</section>

<?php
	m("foot");
?>