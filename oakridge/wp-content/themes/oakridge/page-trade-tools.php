<?php /* Template Name: Trade Tools - Template */
	m("head");	
?>

<section id="trade-tools">
	<div class="wrap">
		<div class="border">
			<section>
				<aside>
					<?php /* <h2 class="title">Trade Tools</h2>
					<ul class="menu clearfix">
						<li><a href="#">OZV</a></li>
						<li><a href="#">Lode Estates</a></li>
						<li><a href="#">Helena Ranch</a></li>
						<li><a href="#">Maggio</a></li>
						<li><a href="#">Moss Roxx</a></li>
						<li><a href="#">Silk Oak</a></li>
						<li><a href="#">Old Soul</a></li>
						<li><a href="#">3 Girls</a></li>
						<li><a href="#">Gnarled Vine</a></li>
					</ul> */ ?>
					<?php dynamic_sidebar('trade-tools-sidebar'); ?>
				</aside>
				<article>
					<div class="row">
						<div class="col-md-6">
							<div class="brand-entry">
								<h2 class="title">3 GIRLS CABERNET SAUVIGNON 2012</h2>
								<div class="featured-image">
									<img src="<?php bloginfo("stylesheet_directory"); ?>/images/bottle.png">
								</div>
								<div class="excerpt">
									<p>&#149; Test Content &nbsp; &nbsp; &#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
								</div>
							</div>	
						</div>

						<div class="col-md-6">
							<div class="brand-entry">
								<h2 class="title">3 GIRLS CABERNET SAUVIGNON 2012</h2>
								<div class="featured-image">
									<img src="<?php bloginfo("stylesheet_directory"); ?>/images/bottle.png">
								</div>
								<div class="excerpt">
									<p>&#149; Test Content &nbsp; &nbsp; &#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="brand-entry">
								<h2 class="title">3 GIRLS CABERNET SAUVIGNON 2012</h2>
								<div class="featured-image">
									<img src="<?php bloginfo("stylesheet_directory"); ?>/images/bottle.png">
								</div>
								<div class="excerpt">
									<p>&#149; Test Content &nbsp; &nbsp; &#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="brand-entry">
								<h2 class="title">3 GIRLS CABERNET SAUVIGNON 2012</h2>
								<div class="featured-image">
									<img src="<?php bloginfo("stylesheet_directory"); ?>/images/bottle.png">
								</div>
								<div class="excerpt">
									<p>&#149; Test Content &nbsp; &nbsp; &#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
									<p>&#149; Test Content</p>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</article>
				<div class="clearfix"></div>
			</section>
		</div>
	</div>
</section>

<?php
	m("foot");
?>