<?php /* Template Name: Blog Template */
	m("head-blog");	
?>

<section id="page" class="fullwidth blog">
	<div class="wrap">
		<h3 class="style-middle-line3"><span> What's New At Oak Ridge</span></h3>
		<?php m("loop-blog"); ?>
	</div>
</section>

<?php
	m("foot-blog");
?>