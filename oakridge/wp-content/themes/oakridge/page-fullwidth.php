<?php /* Template Name: Fullwidth */
	m("head");	
?>

<section id="page" class="fullwidth">
	<div class="wrap">
		<div class="border">
			<section>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<article class="row">    
							<?php the_title('<h2 class="title">', '</h2>'); ?>
							<?php the_content(); ?>
						</article>
						<div class="clearfix"></div>
					<?php endwhile; ?>
				<?php endif; ?> 
			</section>
		</div>
	</div>
</section>

<?php
	m("foot");
?>