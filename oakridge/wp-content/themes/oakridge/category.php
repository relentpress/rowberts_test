<?php /* Template Name: The Wines - Template */
	m("head-wine");	
?>

<section id="the-wines">
	<div class="wrap">
		<div class="border">
			<section>
				<aside>
					<h2 class="title">Vintages &amp; Varietals</h2>
					<ul class="menu clearfix">
						<li><a href="#">SILK OAK 2010 LODI CABERNET SAUVIGNON</a></li>
						<li><a href="#">SILK OAK 2009 LODI CABERNET SAUVIGNON</a></li>
					</ul>
				</aside>
				<article>	
					<h1>SILK OAK 2010 LODI CABERNET SAUVIGNON</h1>
					<div class="media">
					  <a class="media-left media-top" href="#">
					    <img src="<?php bloginfo("stylesheet_directory"); ?>/images/big-bottle.png" data-src="<?php bloginfo("stylesheet_directory"); ?>/images/big-bottle.png" alt="">
					  </a>
					  <div class="media-body">
					   Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Et harumd undlookum like Greek to me, dereud facilis est er expedit distinct. Nam liber te conscient to factor tum poen legum odioque civiuda. Et tam neque pecun modut est neque nonor et imper ned libidig met, consectetur adipiscing elit, sed ut labore et dolore magna aliquam makes one wonder who would ever read this stuff? Bis nostrud exercitation ullam mmodo consequet. <br /><br />

Duis aute in voluptate velit esse cillum dolore eu fugiat nulla pariatur. At vver eos et accusam dignissum qui blandit est praesent luptatum delenit aigue excepteur sint occae. Et harumd dereud facilis est er expedit distinct. Nam libe soluta nobis eligent optio est congue nihil impedit doming id Lorem ipsum dolor sit amet, consectetur adipiscing elit, set eiusmod tempor incidunt et labore et dolore magna aliquam. Ut enim ad minim veniam, quis nostrud exerc. Irure dolor in reprehend incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse molestaie cillum. Tia non ob ea soluad incommod quae egen ium improb fugiend. Officia deserunt mollit anim id est laborum Et harumd dereud facilis est er expedit distinct. Nam liber te conscient to factor tum poen legum odioque civiuda et tam. <br /><br />

Neque pecun modut est neque nonor et imper ned libidig met, consectetur adipiscing elit, sed ut labore et dolore magna aliquam is nostrud exercitation ullam mmodo consequet. Duis aute in voluptate velit esse cillum dolore eu fugiat nulla pariatur. At vver eos et accusam dignissum qui blandit est praesent. Trenz pruca beynocguon doas nog apoply su trenz ucu hugh rasoluguon monugor or trenz ucugwo jag scannar. Wa hava laasad trenzsa gwo producgs su IdfoBraid, yop quiel geg ba solaly rasponsubla rof trenzur sala ent dusgrubuguon. <br /><br />

Offoctivo immoriatoly, hawrgasi pwicos asi sirucor.Thas sirutciun applios tyu thuso itoms ghuso pwicos gosi sirucor in mixent gosi sirucor ic mixent ples cak ontisi sowios uf Zerm hawr rwivos. Unte af phen neige pheings atoot Prexs eis phat eit sakem eit vory gast te Plok peish ba useing phen roxas. Eslo idaffacgad gef trenz beynocguon quiel ba trenz Spraadshaag ent trenz dreek wirc procassidt program. Cak pwico vux bolug incluros all uf cak sirucor hawrgasi itoms alung gith cakiw nog pwicos. Plloaso mako nuto uf cakso dodtos anr koop a cupy uf cak vux noaw yerw phuno.

					  </div>
					</div>

					<div id="download">
						<div class="row">
							<div class="col-xs-6 col-md-2">
								<p class="text-right">
								<br />
									<b>Technical</b> <br />
									Alcohol <br />
									pH <br />
									Res. Sugar <br />
									Total Acid <br />
									<b>Verietal Blend</b> <br />
									87% <br />
									13% <br />
									UPC <br />
								</p><br />
							</div>
							<div class="col-xs-6 col-md-2">
								<p><br />
									&nbsp; <br />
									13.00% <br />
									3.61 <br />
									0.93% <br />
									5.8 gm/L <br />
									&nbsp; <br />
									Cabernet Sauvignon <br />
									Merlot <br />
									082544008018
								</p><br />
							</div>						
							<div class="col-xs-12 col-md-3">
								<img class="download-image" style="position:relative; z-index:25" src="<?php bloginfo("stylesheet_directory"); ?>/images/download-image.png">
							</div>			
							<div class="col-xs-12 col-md-5">
								<div class="download-section">
									<a href="#"><img src="<?php bloginfo("stylesheet_directory"); ?>/images/pdf.png">DOWNLOAD AS A PDF VISIT OUR TRADE TOOLS AREA BUY A BOTTLE</a>
								</div>
							</div>
						</div>
					</div>

				</article>
				<div class="clearfix"></div>
			</section>
		</div>
	</div>
</section>

<?php
	m("foot");
?>