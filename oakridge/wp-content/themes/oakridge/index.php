<?php
	/* Template Name: Landing Page */
	m("head");
?>

 <section id="slider-cover" <?php echo ($val = genesis_get_option( 'welcomeImage', 'home-settings' )) ? 'style="background-image:url('. $val .');"' : ''; ?>>
  	<div class="wrap">
        
      <div id="welcome-message-container">

        <?php get_search_form(); ?>
        
        <?php if ($val = genesis_get_option( 'welcomeTitle', 'home-settings' )) : ?>

        <div id="welcome-message" class="smoothScale">
          <h2><?php echo $val; ?></h2>
          <p><?php echo ($val = genesis_get_option( 'welcomeMessage', 'home-settings' )) ? $val : ''; ?> </p>
        </div>
        
        <?php endif; ?>

      </div>
  	</div>
  </section>

  <?php m("tile-grid"); ?>


<?php if(is_active_sidebar("whats-new")) : ?>
 <section id="whats-new">
    <div class="wrap">
        <div id="row">

          <!-- <h3 class="style-middle-line"><span>What's New At Oak Ridge</span></h3> -->
          <?php dynamic_sidebar("whats-new"); ?>

        </div>

        <div class="clearfix"></div>
    </div>
  </section>
<?php endif; ?>

<?php
	m("foot");
?>


