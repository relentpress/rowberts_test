<?php

/**
 * Helper class for theme functions.
 *
 * @class FLTheme
 */
final class FLTheme {

	/**
     * @property $fonts
     * @private
     */
	static private $fonts;

    /**
     * @method get_setting
     */
    static public function get_setting($key = '')
    {
        $settings = FLAdmin::get_settings();
        
        if(isset($settings->$key)) {
            return $settings->$key;
        }
        else {
            return '';
        }
    }

    /**
     * @method get_settings
     */
    static public function get_settings()
    {
        return FLAdmin::get_settings();
    }

    /**
     * @method setup
     */
    static public function setup()
    {   
        // RSS feed links support
        add_theme_support('automatic-feed-links');
        
        // Post thumbnail support
        add_theme_support('post-thumbnails'); 
        
        // WooCommerce support
        add_theme_support('woocommerce');
        
        // Nav menus
        register_nav_menus(array(
            'bar'     => __('Top Bar Menu', 'fl-automator'),
            'header'  => __('Header Menu', 'fl-automator'),
            'footer'  => __('Footer Menu', 'fl-automator')
        ));
                
        // Localization
        load_theme_textdomain('fl-automator', FL_THEME_DIR . '/lang');
        
        // Include the settings.
        require_once FL_THEME_DIR . '/includes/settings-config.php';
    }

    /**
     * @method enqueue_scripts
     */  
    static public function enqueue_scripts()
    {
        // Font Awesome
        wp_enqueue_style('font-awesome', FL_THEME_URL . '/css/font-awesome.min.css');
        
        // jQuery
        wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-throttle', FL_THEME_URL . '/js/jquery.throttle.min.js', array(), '', true);
		
		// Lightbox
		if(self::get_setting('lightbox') == 'enabled') {    		
		    wp_enqueue_style('jquery-magnificpopup', FL_THEME_URL . '/css/jquery.magnificpopup.css');
    		wp_enqueue_script('jquery-magnificpopup', FL_THEME_URL . '/js/jquery.magnificpopup.min.js', array(), '', true);
		}
        
        // Threaded Comments
        if(is_singular() && comments_open() && get_option('thread_comments')) {
    		wp_enqueue_script('comment-reply');
    	}
        
        // Preview JS
        if(FLAdmin::is_preview()) {
            wp_enqueue_script('fl-automator-preview', FL_THEME_URL . '/js/preview.js', array(), '', true);
            wp_localize_script('fl-automator-preview', 'preview', array('preset' => $_GET['fl-preview']));
        }
        
        // Bootstrap and theme JS
        wp_enqueue_script('bootstrap', FL_THEME_URL . '/js/bootstrap.min.js', array(), '', true);
        wp_enqueue_script('fl-automator', FL_THEME_URL . '/js/theme.js', array(), '', true);
    }

    /**
     * @method widgets_init
     */    
    static public function widgets_init()
    {
    	$show_footer_widgets = self::get_setting('show_footer_widgets');
    	$woo_layout 	     = self::get_setting('woo_layout');
    	
        // Primary Sidebar
        register_sidebar(array(
    		'name'          => __('Primary Sidebar', 'fl-automator'),
    		'id'            => 'blog-sidebar',
    		'before_widget' => '<aside id="%1$s" class="fl-widget %2$s">',
    		'after_widget'  => '</aside>',
    		'before_title'  => '<h4 class="fl-widget-title">',
    		'after_title'   => '</h4>'
    	));
    	
    	// Footer Widgets
    	if($show_footer_widgets != 'disabled') {
	        register_sidebars(4, array(
	    		'name'          => __('Footer Column %d', 'fl-automator'),
	    		'id'            => 'footer-col',
	    		'before_widget' => '<aside id="%1$s" class="fl-widget %2$s">',
	    		'after_widget'  => '</aside>',
	    		'before_title'  => '<h4 class="fl-widget-title">',
	    		'after_title'   => '</h4>'
	    	));
    	}
    	
    	// WooCommerce Sidebar
    	if($woo_layout != 'no-sidebar' && FLAdmin::is_plugin_active('woocommerce')) {
            register_sidebar(array(
        		'name'          => __('WooCommerce Sidebar', 'fl-automator'),
        		'id'            => 'woo-sidebar',
        		'before_widget' => '<aside id="%1$s" class="fl-widget %2$s">',
        		'after_widget'  => '</aside>',
        		'before_title'  => '<h4 class="fl-widget-title">',
        		'after_title'   => '</h4>'
        	));
        }
    }
    
    /**
     * @method title
     */  
    static public function title()
    {
        $sep            = apply_filters('fl_title_separator', ' | ');
        $title          = wp_title($sep, false, 'right');
        $name           = get_bloginfo('name');
        $description    = get_bloginfo('description');
        
        if(empty($title) && empty($description)) {
            $title = $name;
        }
        else if(empty($title)) {
            $title = $name . ' | ' . $description;
        }
        else if(!empty($name) && !stristr($title, $name)) {
            $title = !stristr($title, $sep) ? $title . $sep . $name : $title . $name;
        }
        
        echo apply_filters('fl_title', $title);
    }
    
    /**
     * @method favicon
     */  
    static public function favicon()
    {
        $url = self::get_setting('favicon');
        
        if(!empty($url)) {
            echo '<link rel="shortcut icon" href="'. $url .'" />' . "\n";
        }
    }
    
    /**
     * @method fonts
     */  
    static public function fonts()
    {
        $settings  = self::get_settings();
        $logo_type = $settings->logo_type;
        
        self::add_font($settings->text_font, array(300, 400, 700));
        self::add_font($settings->heading_font, $settings->heading_weight);
        
        if($logo_type == 'text') {
        	self::add_font($settings->logo_font, $settings->logo_weight);
		}
		
		self::render_fonts();
    }
    
    /**
     * @method add_font
     */  
    static public function add_font($name, $variants = array())
    {
        $protocol   = (stripos(get_option('siteurl'), 'https://') === 0) ? 'https' : 'http';
    	$google_url = $protocol . '://fonts.googleapis.com/css?family=';
    	
    	if(isset(self::$fonts[$name])) {
    		foreach((array)$variants as $variant) {
	    		if(!in_array($variant, self::$fonts[$name]['variants'])) {
		    		self::$fonts[$name]['variants'][] = $variant;
	    		}
    		}
    	}
    	else {
	    	self::$fonts[$name] = array(
	    		'url'	   => isset(FLFontFamilies::$google[$name]) ? $google_url . $name : '',
	    		'variants' => (array)$variants
	    	);
    	}
    }
    
    /**
     * @method render_fonts
     */  
    static public function render_fonts()
    {
    	foreach(self::$fonts as $font) {
    		if(!empty($font['url'])) {
				echo '<link rel="stylesheet" href="'. $font['url'] . ':'. implode(',', $font['variants']) .'" />' . "\n";
			}
		}
    }
    
    /**
     * @method head
     */      
    static public function head()
    {
    	$settings  = self::get_settings();
    	
    	// Skin
        echo '<link rel="stylesheet" href="' . FLAdmin::skin_url() . '" />' . "\n";

		// CSS
        if(!empty($settings->css)) {
	        echo '<style>' . $settings->css . '</style>' . "\n";
        }

		// JS
        if(!empty($settings->js)) {
	        echo '<script>' . $settings->js . '</script>' . "\n";
        }

		// Head
        if(!empty($settings->head)) {
	        echo $settings->head . "\n";
        }
        
        do_action('fl_head');
    }
    
    /**
     * @method body_class
     */      
    static public function body_class($classes)
    {
        if(self::get_setting('layout') == 'full-width') {
            $classes[] = 'fl-full-width';
        }
        else {
	        $classes[] = 'fl-fixed-width';
        }
        
        return $classes;
    }
    
    /**
     * @method nav_menu_fallback
     */      
    static public function nav_menu_fallback($args)
    {
        $url  = current_user_can('edit_theme_options') ? admin_url('nav-menus.php') : home_url();
        $text = current_user_can('edit_theme_options') ? __('Choose Menu', 'fl-automator') :  __('Home', 'fl-automator');
        
        echo '<ul class="fl-page-' . $args['theme_location'] . '-nav nav navbar-nav menu">';
        echo '<li>';
        echo '<a href="' . $url . '">' . $text . '</a>';
        echo '</li>';
        echo '</ul>';
    }
    
    /**
     * @method top_bar_col1
     */  
    static public function top_bar_col1()
    {
        $settings = self::get_settings();
        
        if($settings->top_bar_layout != 'none') {

            if($settings->top_bar_layout == '1-col') {
                echo '<div class="col-md-12 text-center clearfix">';
            }
            else {
                echo '<div class="col-md-6 col-sm-6 text-left clearfix">';
            }
            
            if($settings->top_bar_col1_layout == 'text') {
                echo '<div class="fl-page-bar-text">' . $settings->top_bar_col1_text . '</div>';
            }
            if($settings->top_bar_col1_layout == 'social') {
                self::social_icons(false);
            }
            if($settings->top_bar_col1_layout == 'menu') {
                wp_nav_menu(array(
                    'theme_location' => 'bar',
                    'items_wrap' => '<ul id="%1$s" class="fl-page-bar-nav nav navbar-nav %2$s">%3$s</ul>',
                    'container' => false,
                    'fallback_cb' => 'FLTheme::nav_menu_fallback'
                )); 
            }
         
            echo '</div>';   
        }
    }
    
    /**
     * @method top_bar_col2
     */  
    static public function top_bar_col2()
    {
        $settings = self::get_settings();
        
        if($settings->top_bar_layout == '2-cols') {

            echo '<div class="col-md-6 col-sm-6 text-right clearfix">';
            
            if($settings->top_bar_col2_layout == 'text') {
                echo '<div class="fl-page-bar-text">' . $settings->top_bar_col2_text . '</div>';
            }
            if($settings->top_bar_col2_layout == 'social') {
                self::social_icons(false);
            }
            if($settings->top_bar_col2_layout == 'menu') {
                wp_nav_menu(array(
                    'theme_location' => 'bar',
                    'items_wrap' => '<ul id="%1$s" class="fl-page-bar-nav nav navbar-nav %2$s">%3$s</ul>',
                    'container' => false,
                    'fallback_cb' => 'FLTheme::nav_menu_fallback'
                )); 
            }
         
            echo '</div>';   
        }
    }
    
    /**
     * @method top_bar
     */      
    static public function top_bar()
    {
    	$top_bar_layout = self::get_setting('top_bar_layout');
    	
    	if($top_bar_layout != 'none') {
        	include FL_THEME_DIR . '/includes/top-bar.php';
        }
    }
    
    /**
     * @method fixed_header
     */      
    static public function fixed_header()
    {
    	$header_layout = self::get_setting('fixed_header');
    	
    	if($header_layout == 'visible') {
        	include FL_THEME_DIR . '/includes/fixed-header.php';
        }
    }
    
    /**
     * @method header_layout
     */      
    static public function header_layout()
    {
        include FL_THEME_DIR . '/includes/nav-' . self::get_setting('nav_position') . '.php';
    }
    
    /**
     * @method header_content
     */      
    static public function header_content()
    {
        $settings = self::get_settings();
        
        if($settings->header_content == 'text' || $settings->header_content == 'social-text') {
            echo '<div class="fl-page-header-text">'. $settings->header_text .'</div>'; 
        }
        if($settings->header_content == 'social' || $settings->header_content == 'social-text') {
            self::social_icons();
        }
    }
    
    /**
     * @method logo
     */  
    static public function logo()
    {
        $logo_type  = self::get_setting('logo_type');
        $logo_image = self::get_setting('logo_image');
        $logo_text  = self::get_setting('logo_text');
        $name       = empty($logo_text) ? get_bloginfo('name') : $logo_text;
        
        if($logo_type == 'image') {
            if(empty($logo_image)) {
	            echo '<span class="icon-automator-logo"></span>';
            }
            else {
	            echo '<img class="fl-logo-img" src="'. $logo_image .'" itemprop="logo" />';
	            echo '<meta itemprop="name" content="' . $name . '" />';
            }
        }
        else {
            echo '<span class="fl-logo-text" itemprop="name">'. $logo_text .'</span>';
        }
    }
    
    /**
     * @method nav_search
     */      
    static public function nav_search()
    {
    	$nav_search = self::get_setting('nav_search');
    	
    	if($nav_search == 'visible') {
        	include FL_THEME_DIR . '/includes/nav-search.php';
        }
    }
    
    /**
     * @method social_icons
     */  
    static public function social_icons($circle = true)
    {
        $settings = self::get_settings();

        $keys = array(
            'facebook', 
            'twitter', 
            'google', 
            'linkedin', 
            'yelp', 
            'pinterest', 
            'tumblr', 
            'vimeo', 
            'youtube', 
            'flickr', 
            'instagram', 
            'dribbble', 
            '500px', 
            'blogger', 
            'github', 
            'rss',
            'email'
        );
    
        echo '<div class="fl-social-icons">';
            
        foreach($keys as $key) {
            $link_target = ' target="_blank"';

            if(!empty($settings->$key)) {

                if($key == 'email') {
                    $settings->$key = 'mailto:' . $settings->$key;
                    $link_target = '';
                }
                
                $class = 'fl-icon fl-icon-color-'. $settings->social_color .' fl-icon-'. $key .' fl-icon-'. $key;
                $class .= $circle ? '-circle' : '-regular';
                echo '<a href="'. $settings->$key . '"' . $link_target . ' class="'. $class .'"></a>';
            }
        }
        
        echo '</div>';
    }
    
    /**
     * @method footer_widgets
     */  
    static public function footer_widgets()
    {
        if(self::has_footer_widgets()) {
            include FL_THEME_DIR . '/includes/footer-widgets.php';
        }
    }
    
    /**
     * @method has_footer_widgets
     */  
    static public function has_footer_widgets()
    {
    	$show = self::get_setting('show_footer_widgets');
    	
        if($show == 'disabled' || (!is_home() && $show == 'home')) {
            return false;
        }
        
        for($i = 1; $i <= 4; $i++) {
        
            $id = $i == 1 ? 'footer-col' : 'footer-col-' . $i;
            
            if(is_active_sidebar($id)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * @method display_footer_widgets
     */  
    static public function display_footer_widgets()
    {
        $active = array();
        $num_active = 0;
            
        for($i = 1; $i <= 4; $i++) {
        
            $id = $i == 1 ? 'footer-col' : 'footer-col-' . $i;
            
            if(is_active_sidebar($id)) {
                $active[] = $id;
                $num_active++;
            }
        }
        if($num_active > 0) {
            
            $col_length = 12/$num_active;
            
            for($i = 0; $i < $num_active; $i++) {
                echo '<div class="col-md-' . $col_length . '">';
                dynamic_sidebar($active[$i]);
                echo '</div>';
            }
        }
    }
    
    /**
     * @method footer
     */  
    static public function footer()
    {
        $footer_layout = self::get_setting('footer_layout');
    	
    	if($footer_layout != 'none') {
        	include FL_THEME_DIR . '/includes/footer.php';
        }
    }
    
    /**
     * @method footer_col1
     */  
    static public function footer_col1()
    {
        $settings = self::get_settings();
        
        if($settings->footer_layout != 'none') {

            if($settings->footer_layout == '1-col') {
                echo '<div class="col-md-12 text-center clearfix">';
            }
            else {
                echo '<div class="col-md-6 col-sm-6 text-left clearfix">';
            }
            
            if($settings->footer_col1_layout == 'text' || $settings->footer_col1_layout == 'social-text') {
                if(empty($settings->footer_col1_text)) {
                    include FL_THEME_DIR . '/includes/copyright.php';
                }
                else {
                    echo '<div class="fl-page-footer-text">' . $settings->footer_col1_text . '</div>';
                }
            }
            if($settings->footer_col1_layout == 'social' || $settings->footer_col1_layout == 'social-text') {
                self::social_icons();
            }
            if($settings->footer_col1_layout == 'menu') {
                wp_nav_menu(array(
                    'theme_location' => 'footer',
                    'items_wrap' => '<ul id="%1$s" class="fl-page-footer-nav nav navbar-nav %2$s">%3$s</ul>',
                    'container' => false,
                    'fallback_cb' => 'FLTheme::nav_menu_fallback'
                )); 
            }
         
            echo '</div>';   
        }
    }
    
    /**
     * @method footer_col2
     */  
    static public function footer_col2()
    {
        $settings = self::get_settings();
        
        if($settings->footer_layout == '2-cols') {

            echo '<div class="col-md-6 col-sm-6 text-right clearfix">';
            
            if($settings->footer_col2_layout == 'text' || $settings->footer_col2_layout == 'social-text') {
                echo '<div class="fl-page-footer-text">' . $settings->footer_col2_text . '</div>';
            }
            if($settings->footer_col2_layout == 'social' || $settings->footer_col2_layout == 'social-text') {
                self::social_icons();
            }
            if($settings->footer_col2_layout == 'menu') {
                wp_nav_menu(array(
                    'theme_location' => 'footer',
                    'items_wrap' => '<ul id="%1$s" class="fl-page-footer-nav nav navbar-nav %2$s">%3$s</ul>',
                    'container' => false,
                    'fallback_cb' => 'FLTheme::nav_menu_fallback'
                )); 
            }
         
            echo '</div>';   
        }
    }
    
    /**
     * @method sidebar
     */  
    static public function sidebar($position, $section = 'blog')
    {
        $size = self::get_setting($section . '_sidebar_size');
        
        if(strstr(self::get_setting($section . '_layout'), $position)) {
            echo '<div class="fl-sidebar fl-sidebar-'. $position .' col-md-' . $size . '" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">';
            dynamic_sidebar($section . '-sidebar');
            echo '</div>';
        }
    }
    
    /**
     * @method content_class
     */  
    static public function content_class($section = 'blog')
    {
        $layout       = self::get_setting($section . '_layout');
        $sidebar_size = self::get_setting($section . '_sidebar_size');
        $content_size = '8';
        
        if($sidebar_size == '2') {
            $content_size = '10';
        }
        elseif($sidebar_size == '3') {
            $content_size = '9';
        }
        
        if(strstr($layout, 'left')) {
            echo 'fl-content-right col-md-' . $content_size;
        }
        else if(strstr($layout, 'right')) {
            echo 'fl-content-left col-md-' . $content_size;
        }
        else {
            echo 'col-md-12';
        }
    }
    
    /**
     * @method archive_page_header
     */  
    static public function archive_page_header()
    {
    	// Category
    	if(is_category()) { 
    		$page_title = single_cat_title('', false);
    	}
    	// Tag
    	else if(is_tag()) {
    		$page_title = __('Posts Tagged ', 'fl-automator') . '&#8216;' . single_tag_title('', false) . '&#8217;';
    	}
    	// Day
    	else if(is_day()) {
    		$page_title = __('Archive for ', 'fl-automator') . get_the_date();
    	}
    	// Month
    	else if(is_month()) {
    		$page_title = __('Archive for ', 'fl-automator') . single_month_title(' ', false);
    	}
    	// Year
    	else if(is_year()) {
    		$page_title = __('Archive for ', 'fl-automator') . get_the_time('Y');
    	}
    	// Author
    	else if(is_author()) {
    		$page_title = __('Posts by ', 'fl-automator') . get_the_author();
    	}
    	// Search
    	else if(is_search()) {
    		$page_title = __('Search results for: ', 'fl-automator') . get_search_query();
    	}
    	// Paged
    	else if(isset($_GET['paged']) && !empty($_GET['paged'])) {
    		$page_title = __('Archives', 'fl-automator');
    	}
    	// Index
    	else {
    		$page_title = '';
    	}
    	
    	if(!empty($page_title)) {
    		echo '<header class="fl-archive-header">';
    		echo '<h1 class="fl-archive-title">' . $page_title . '</h1>';
    		echo '</header>';
    	}
    }
    
    /**
     * @method archive_nav
     */  
    static public function archive_nav()
    {
        global $wp_query;
        
        if(function_exists('wp_pagenavi')) {
            wp_pagenavi();
        }
        elseif($wp_query->max_num_pages > 1) {
            echo '<nav class="fl-archive-nav clearfix">';
    	    echo '<div class="fl-archive-nav-prev">' . get_previous_posts_link(__('&laquo; Newer Posts', 'fl-automator')) . '</div>';
    	    echo '<div class="fl-archive-nav-next">' . get_next_posts_link(__('Older Posts &raquo;', 'fl-automator')) . '</div>';
    	    echo '</nav>';
        }
    }
    
    /**
     * @method excerpt_more
     */  
    static public function excerpt_more($more) 
    {
    	return '&hellip;';
    }
    
    /**
     * @method post_top_meta
     */  
    static public function post_top_meta()
    {
        global $post;
        
        $settings       = self::get_settings();
        $show_author    = $settings->blog_show_author == 'visible' ? true : false;
        $show_date      = $settings->blog_show_date == 'visible' ? true : false;
        $comments       = comments_open() || '0' != get_comments_number();
        
        // Wrapper
        if($show_author || $show_date || $comments) {
            echo '<div class="fl-post-meta fl-post-meta-top">';
        }
        
        // Author
        if($show_author) {
            echo '<span class="fl-post-author" itemprop="author" itemscope="itemscope" itemtype="http://schema.org/Person">' . __('By ', 'fl-automator'); 
            echo '<a href="' . get_author_posts_url(get_the_author_meta('ID')) . '" itemprop="url"><span itemprop="name">' . get_the_author_meta('display_name', get_the_author_meta('ID')) . '</span></a>';
            echo '</span>';
        }
        
        // Date
        if($show_date) {
        
            if($show_author) {
                echo '<span class="fl-sep"> | </span>';
            }
        
            echo '<span class="fl-post-date" itemprop="datePublished" datetime="' . get_the_time('Y-m-d') . '">' . get_the_date() . '</span>';
        }
        
        // Comments 
        if($comments) {
        
            if(!empty($show_date)) {
                echo '<span class="fl-sep"> | </span>';
            }
            
            echo '<span class="fl-comments-popup-link">';
            comments_popup_link('0 <i class="fa fa-comment"></i>', '1 <i class="fa fa-comment"></i>', '% <i class="fa fa-comment"></i>');
            echo '</span>';
        }
        
        // Close Wrapper
        if($show_author || $show_date || $comments) {
            echo '</div>';
        }
        
        // Scheme Image Meta
        if(has_post_thumbnail()) {
            echo '<meta itemprop="image" content="' . wp_get_attachment_url(get_post_thumbnail_id($post->ID)) . '">';
        }
        
        // Scheme Comment Meta
        $comment_count = wp_count_comments($post->ID);
        
        echo '<meta itemprop="interactionCount" content="UserComments:' . $comment_count->approved . '">';
    }
    
    /**
     * @method post_bottom_meta
     */  
    static public function post_bottom_meta()
    {
        $settings   = self::get_settings();
        $show_full  = $settings->blog_show_full;
        $show_cats  = $settings->blog_show_cats == 'visible' ? true : false;
        $show_tags  = $settings->blog_show_tags == 'visible' && get_the_tags() ? true : false;
        $comments   = comments_open() || '0' != get_comments_number();
        $tagged     = $show_cats ? __(' and tagged ', 'fl-automator') : __('Tagged ', 'fl-automator');
        
        // Only show if we're showing the full post.
        if($show_full || is_single()) {
        
            // Wrapper
            if($show_cats || $show_tags || $comments) {
                echo '<div class="fl-post-meta fl-post-meta-bottom">';
            }
            
            // Categories and Tags
            if($show_cats || $show_tags) {
            
                echo '<div class="fl-post-cats-tags">';
                
                if($show_cats) {
                    _e('Posted in ', 'fl-automator'); 
                    the_category(', ');
                }
                if($show_tags) {
                    echo $tagged;
                    the_tags('');
                }
                
                echo '</div>';
            }
            
            // Comments 
            if($comments && !is_single()) {
                comments_popup_link(__('Leave a comment', 'fl-automator'), __('1 Comment', 'fl-automator'), __('% Comments', 'fl-automator'));
            }
            
            // Close Wrapper
            if($show_cats || $show_tags || $comments) {
                echo '</div>';
            }
        }
    }
    
    /**
     * @method display_comment
     */  
    static public function display_comment($comment, $args, $depth)
    {
        $GLOBALS['comment'] = $comment;
        
        include FL_THEME_DIR . '/includes/comment.php';
    }
    
    /**
     * @method init_woocommerce
     */  
    static public function init_woocommerce()
    {
        remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
        remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
        remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
        
        add_action('woocommerce_before_main_content', 'FLTheme::woocommerce_wrapper_start', 10);
        add_action('woocommerce_after_main_content', 'FLTheme::woocommerce_wrapper_end', 10);
    }
    
    /**
     * @method woocommerce_wrapper_start
     */  
    static public function woocommerce_wrapper_start()
    {
        $layout = self::get_setting('woo_layout');
        $col_size = $layout == 'no-sidebar' ? '12' : '8';
        
        echo '<div class="container">';
        echo '<div class="row">';
        self::sidebar('left', 'woo');
        echo '<div class="fl-content ';
        self::content_class('woo');
        echo '">';
    }
    
    /**
     * @method woocommerce_wrapper_end
     */  
    static public function woocommerce_wrapper_end()
    {
        $layout = self::get_setting('woo_layout');
        
        echo '</div>';
        self::sidebar('right', 'woo');
        echo '</div>';
        echo '</div>';
    }
}