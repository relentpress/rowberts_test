<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rowberts_agentpress');

/** MySQL database username */
define('DB_USER', 'rowberts_staging');

/** MySQL database password */
define('DB_PASSWORD', 'Ma&7HZXuxR');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'c89>.kab)ykS4/Z]eVxb1F!bbg$%%[#YdGk^1H<vsI12XlnrCE[U%)SD+^Y_.L ;');
define('SECURE_AUTH_KEY',  'AN2CIH/%oD3wdO[VeC,i%3/8>Q(=C3)U7Ug&iJ{:LGb|8T)^.EjZ88HH+y+49;-5');
define('LOGGED_IN_KEY',    'tNYFyaU+AAB?)vYD|JW-Cu]Eo4 Yf4^EXcM)D .6,iV2Zr+d lq%7SKN!6z[wBr8');
define('NONCE_KEY',        '5 ;j A!Oadg1tH`lWrx+%]|Ck{+f;ezKym_/:h{hAP~1KmbUhZb<z*1Arnj3~#.:');
define('AUTH_SALT',        'Gw3fV:p-|O q @+DMmCJFCPg-qh H.,~BKjJM`NO3Rofb4anK{I@rr?(_Q1?(ikD');
define('SECURE_AUTH_SALT', '!s 97x%%a*q+{zb$KXF/.~c4g++6P2e&P9F$yIo!gT4CYZ;>mWsSp561Jg&luJZ4');
define('LOGGED_IN_SALT',   '5+|}_ylN9KD9sS`+ r!9W5Pr,Hng7g,8,Bi B{`yMby:-}|/N>;{Nr7sAGJZvin:');
define('NONCE_SALT',       '%Z]-n<WxS?CC(4x@&}Xsx6`2GA;GBb|E.<bGKpPUyrMZ2*T;(/K<I/@Lm63]Kh`e');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/*Enable Auto Updates for WP Core (Major), Themes & Plugins by CER */
define('WP_AUTO_UPDATE_CORE', true);
add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );
