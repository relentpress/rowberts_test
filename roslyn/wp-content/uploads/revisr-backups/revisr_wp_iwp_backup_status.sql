
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_iwp_backup_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_iwp_backup_status` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `historyID` int(11) NOT NULL,
  `taskName` varchar(255) NOT NULL,
  `action` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `stage` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `finalStatus` varchar(50) DEFAULT NULL,
  `statusMsg` varchar(255) NOT NULL,
  `requestParams` text NOT NULL,
  `responseParams` longtext,
  `taskResults` text,
  `startTime` int(11) DEFAULT NULL,
  `endTime` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_iwp_backup_status` WRITE;
/*!40000 ALTER TABLE `wp_iwp_backup_status` DISABLE KEYS */;
INSERT INTO `wp_iwp_backup_status` VALUES (1,31538,'Backup Now','now','backup','full','finished','completed','0','completed','a:5:{s:9:\"task_name\";s:10:\"Backup Now\";s:9:\"mechanism\";s:10:\"singleCall\";s:4:\"args\";a:15:{s:4:\"type\";s:6:\"backup\";s:6:\"action\";s:3:\"now\";s:4:\"what\";s:4:\"full\";s:15:\"optimize_tables\";s:0:\"\";s:7:\"exclude\";a:1:{i:0;s:0:\"\";}s:17:\"exclude_file_size\";i:10;s:18:\"exclude_extensions\";s:13:\"eg. .zip,.mp4\";s:7:\"include\";a:1:{i:0;s:0:\"\";}s:13:\"del_host_file\";s:1:\"1\";s:12:\"disable_comp\";s:1:\"1\";s:12:\"fail_safe_db\";N;s:15:\"fail_safe_files\";N;s:5:\"limit\";s:2:\"15\";s:11:\"backup_name\";s:22:\"Before Old DB deletion\";s:9:\"parentHID\";i:31538;}s:8:\"username\";s:11:\"relentpress\";s:12:\"account_info\";a:1:{s:13:\"iwp_amazon_s3\";a:6:{s:14:\"as3_access_key\";s:20:\"AKIAJI3VH3OBF6CN7UOQ\";s:14:\"as3_secure_key\";s:40:\"aM6GlQO9x6VGAfTT6fG1exfq4tPXIFf5XxVfRZQ4\";s:10:\"as3_bucket\";s:11:\"relentpress\";s:17:\"as3_bucket_region\";s:16:\"s3.amazonaws.com\";s:13:\"as3_directory\";s:10:\"wp_backups\";s:15:\"as3_site_folder\";s:1:\"1\";}}}',NULL,'a:2:{s:15:\"backhack_status\";a:6:{s:14:\"adminHistoryID\";i:31538;s:7:\"db_dump\";a:2:{s:5:\"start\";d:1418610914.271117;s:3:\"end\";d:1418610914.5404911;}s:6:\"db_zip\";a:2:{s:5:\"start\";d:1418610914.546885;s:3:\"end\";d:1418610914.5799339;}s:9:\"files_zip\";a:2:{s:5:\"start\";d:1418610914.7248199;s:3:\"end\";d:1418610919.798408;}s:9:\"amazon_s3\";a:2:{s:5:\"start\";d:1418610919.805475;s:3:\"end\";d:1418610922.7579811;}s:8:\"finished\";a:1:{s:3:\"end\";d:1418610922.768141;}}s:12:\"task_results\";a:1:{i:31538;a:6:{s:4:\"size\";s:8:\"24.64 MB\";s:11:\"backup_name\";s:22:\"Before Old DB deletion\";s:9:\"mechanism\";s:10:\"singleCall\";s:4:\"time\";i:1418610919;s:14:\"adminHistoryID\";i:31538;s:8:\"amazons3\";s:85:\"roslyn.sg.rowberts.com.au_backup_full_2014-12-15_b65fa919b1722ba33ddc75ac218c0471.zip\";}}}',1418610914,0);
/*!40000 ALTER TABLE `wp_iwp_backup_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

