<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rowberts_epik');

	define('DB_USER', 'rowberts_staging');
	define('DB_PASSWORD', 'Ma&7HZXuxR');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`<In`c5J%9]AiblYsjaenQ||=-+wt&<WRL/nB,=t%@-vku69Ai7%sv*5lCF+I7>[');
define('SECURE_AUTH_KEY',  'SHkNePMTNTObh+k_=%E>plTN7|C|ri Q,MGK<xcx_<PgLpZ8d]%wgyR{,d*@8`|N');
define('LOGGED_IN_KEY',    'N<ev5Oi-[@W/f8m:YE)|=NcQZU*C2KPB`LX]wl]6XXN%ibTn+py[^s>;w7W+]_ye');
define('NONCE_KEY',        '.Lg[G1]4~DQCE6CyZikPmrR0/15:QuHBi<AReg+u;FnF<=JC3B4/EOxw-Tzrd4tR');
define('AUTH_SALT',        '_&yXi91(;n4S6Qz<b39j]3d0T-Gh>(ji1UAK~jPQv5qqXO}Cj$IEa,g/ho;hfv y');
define('SECURE_AUTH_SALT', 'wF`S.I5<SV(-c;2v<Bzx?ToqDxn6u,,D^zYyW<GAQ +mp8k7t:#|15]+C=.^{3n#');
define('LOGGED_IN_SALT',   ':mY0NR<nKn|fB>wWV8%~>}T+nF?O)i[}YvXlg?-yUtI0~Ea>o)!^xf)/ h{@SYzV');
define('NONCE_SALT',       '`LZ-@0T>K9)=])~o),?nR5?*z!-F?inf#uNE-+]x~Lp|:h0},u5dEuR?+y)zjN*V');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/*Enable Auto Updates for WP Core (Major), Themes & Plugins by CER */
define('WP_AUTO_UPDATE_CORE', true);
add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );
