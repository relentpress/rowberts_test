<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/** VARIABLE CONNECTION STRINGS BASED ON SERVER **/

/** Developer One **/
if( stristr( $_SERVER['SERVER_NAME'], "employee-assist.local.dev" ) ) {

	// Dev Environment
	define( 'DB_NAME', 'rowberts_employee-assist' );
	define( 'DB_USER', 'root' );
	define( 'DB_PASSWORD', 'root' );
	define( 'DB_HOST', 'localhost' );
	
	define( 'WP_HOME', 'http://employee-assist.local.dev');
	define( 'WP_SITEURL', WP_HOME);
	
	// Dev will always want debug on and caching off
	define( 'WP_DEBUG', true );
	define( 'WP_CACHE', false );	 
} 

/** Developer Two **/
elseif( stristr( $_SERVER['SERVER_NAME'], "localhost" ) ) {
 
	// Staging Environment
	define( 'DB_NAME', 'rowberts_employee-assist' );
	define( 'DB_USER', 'root' );
	define( 'DB_PASSWORD', '' );
	define( 'DB_HOST', 'localhost' );
	
	define( 'WP_HOME', 'http://localhost/employee-assist'); 
	define( 'WP_SITEURL', WP_HOME);
	
	// Dev will always want debug on and caching off
	define( 'WP_DEBUG', true );
	define( 'WP_CACHE', false );		 
} 

/** Staging **/
elseif( stristr( $_SERVER['SERVER_NAME'], "employee-assist.sg.rowberts.com.au" ) ) {
 
	// Staging Environment
	define( 'DB_NAME', 'rowberts_employee-assist' );
	define('DB_USER', 'rowberts_staging');
	define('DB_PASSWORD', 'Ma&7HZXuxR');
	define( 'DB_HOST', 'localhost' );
	
	define( 'WP_HOME', 'http://employee-assist.sg.rowberts.com.au'); 
	define( 'WP_SITEURL', WP_HOME);
	
	// UAT Environment will always be the same as production so turn off debug and turn on caching
	define( 'WP_DEBUG', false );
	define( 'WP_CACHE', true );		 
} 

/** Live **/
else {
 
	// Production Environment
	define( 'DB_NAME', 'project_live' );
	define( 'DB_USER', 'project_live_user' );
	define( 'DB_PASSWORD', 'password' );
	define( 'DB_HOST', 'localhost' );
	
	//define( 'WP_HOME', 'http://project.com'); 
	//define( 'WP_SITEURL', WP_HOME); 
	
	// Live Environment will always be the same as production so turn off debug and turn on caching
	define( 'WP_DEBUG', false );
	define( 'WP_CACHE', true );
		
}
 
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         './RdDR--#/FA6LJ9`Oi}=75e4n>6YV{+z+v%Tbz_|A}qNJ| rQ<).p=njr40Qy$G');
define('SECURE_AUTH_KEY',  'pR+$2>}[e:6|hiZma}7Db@@.Q&v$Kj~d&_4wPd}6VsuBL`{byZN|sR?2_~/z$Kl{');
define('LOGGED_IN_KEY',    '7h!%:,pT&THjE*kg(lWlDhwE!_<-y&1Ey$ICQMyBg ! 9AD{e~TAZ{#:SxH8r! y');
define('NONCE_KEY',        ']KaGhuP0-DUk(a~,UTVD$nJeX#_LsG]Ta4c|@0m).U!)jde/N>mKsp``)t+Inj0)');
define('AUTH_SALT',        'pmosudC5T}g1RdguV+tH.JA1C+<:=K;[2S2|Km)dSP|<&I::v}O*|NExQ:fc*)po');
define('SECURE_AUTH_SALT', 'a<uh2m&vzR9|QlI4=v@Qsga-dM-6WKYt]$,ilA8ywOXsR8ue/jhZa0$:|33DZw+d');
define('LOGGED_IN_SALT',   'IrTeN_C]$FnzRK.Axiv{|<7v(Z,<r??r~9Jy$u l[?h|sc{rzv5f9!|)j910fnTo');
define('NONCE_SALT',       'XmO#WBmXt>6:-~7%Fc9LGEql4F2 [zx1o,2Y?|Jb3-%|*DteO/xRMR^#aq6lEiqr');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
