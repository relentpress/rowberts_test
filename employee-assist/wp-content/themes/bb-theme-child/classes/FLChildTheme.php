<?php

/**
 * Helper class for theme functions.
 *
 * @class FLChildTheme
 */
final class FLChildTheme {
    
    /**
     * @method styles
     */
    static public function stylesheet()
    {
        echo '<link rel="stylesheet" href="' . FL_CHILD_THEME_URL . '/style.css" />';
    }


    /**
     * @method fixed_header
     */      
    static public function fixed_header()
    {
    	$header_layout = self::get_setting('fixed_header');
    	
    	if($header_layout == 'visible') {
        	include FL_CHILD_THEME_URL . '/includes/fixed-header.php';
        }
    }
}
