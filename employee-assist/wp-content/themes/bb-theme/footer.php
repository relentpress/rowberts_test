        <?php do_action('fl_content_close'); ?>
    
    </div><!-- .fl-page-content -->
    <footer itemscope="itemscope" itemtype="http://schema.org/WPFooter">
        <?php 
        
        do_action('fl_after_content'); 
            
        FLTheme::footer_widgets();
        
        do_action('fl_after_footer_widgets');
        
        FLTheme::footer();
        
        do_action('fl_after_footer');
        
        ?>
    </footer>
</div><!-- .fl-page -->
<?php wp_footer(); ?>
<?php do_action('fl_body_close'); ?>
</body>
</html>