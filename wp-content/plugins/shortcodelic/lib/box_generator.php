<div id="shortcodelic_box_generator" class="shortcodelic_meta shortcodelic_editelement_panel" data-title="<?php _e('Box generator', 'shortcodelic'); ?>">
    <div class="alignleft shortcodelic_options_area">
        <h3><?php printf( __( 'Add a feature icon (%1$sclick here%2$s)'  ), '<a href="#" class="shortcodelic_icon_button">', '</a>', 'shortcodelic'); ?>:</h3>
        <input type="text" data-name="icon" value="" placeholder="Click the link above">

        <div class="clear"></div>

        <h3><?php _e('Featured icon position', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select data-name="position">
                    <option value='topicon'><?php _e('icon on top', 'shortcodelic'); ?></option>
                    <option value='bottomicon'><?php _e('icon on bottom', 'shortcodelic'); ?></option>
                    <option value='lefticon'><?php _e('icon on left', 'shortcodelic'); ?></option>
                    <option value='righticon'><?php _e('icon on right', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Styles', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select data-name="custom">
                    <option value=''><?php _e('default', 'shortcodelic'); ?></option>
                     <?php $shortcodelic_array_boxes_ = get_option('shortcodelic_array_boxes_');
                    if (isset($shortcodelic_array_boxes_) && is_array($shortcodelic_array_boxes_)) foreach ($shortcodelic_array_boxes_ as $key => $value) { ?>
                        <option value='<?php echo $key; ?>'><?php echo $key; ?></option>
                    <?php } ?>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><label class="alignleft" for="dismiss_box"><?php _e('&quot;Dismiss&quot; button', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('By ticking this checkbox an ID will be added to the shortcode and all the users that will hide the text box by clicking the dismiss button won\'t be able to see the box anymore until the ID will be changed or their cookies will be deleted','shortcodelic'); ?></p>"></i>:
            <input type="checkbox" id="dismiss_box" data-name="dismiss_box" value="<?php echo current_time('timestamp'); ?>">
            <span></span>
        </label></h3>
        
        <div class="clear"></div>

    </div>

    <div class="alignleft shortcodelic_opts_list_2 shortcodelic_tinymce_area">
    </div>

</div><!-- #shortcodelic_box_generator -->