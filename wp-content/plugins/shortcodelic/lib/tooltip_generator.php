<div id="shortcodelic_tooltips_generator" class="shortcodelic_meta shortcodelic_editelement_panel" data-title="<?php _e('Tooltip generator', 'shortcodelic'); ?>">
    <div class="alignleft shortcodelic_options_area">
        <h3><?php _e('Position', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select data-name="position">
                    <option value=''><?php _e('top', 'shortcodelic'); ?></option>
                    <option value='right'><?php _e('right', 'shortcodelic'); ?></option>
                    <option value='left'><?php _e('left', 'shortcodelic'); ?></option>
                    <option value='bottom'><?php _e('bottom', 'shortcodelic'); ?></option>
                    <option value='top-right'><?php _e('top-right', 'shortcodelic'); ?></option>
                    <option value='top-left'><?php _e('top-left', 'shortcodelic'); ?></option>
                    <option value='bottom-right'><?php _e('bottom-right', 'shortcodelic'); ?></option>
                    <option value='bottom-left'><?php _e('bottom-left', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Animation', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select data-name="animation">
                    <option value=''><?php _e('grow', 'shortcodelic'); ?></option>
                    <option value='fade'><?php _e('fade', 'shortcodelic'); ?></option>
                    <option value='swing'><?php _e('swing', 'shortcodelic'); ?></option>
                    <option value='slide'><?php _e('slide', 'shortcodelic'); ?></option>
                    <option value='fall'><?php _e('fall', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Max width', 'shortcodelic'); ?>:</h3>
        <div class="slider_div">
            <input type="text" data-name="maxwidth" value="0">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><label class="alignleft" for="tooltip_interactive"><?php _e('Interactive', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('By ticking this box you\'ll be able to stay with the mouse over the tooltip and click links inside it, select text etc.','shortcodelic'); ?></p>"></i>:
            <input type="checkbox" id="tooltip_interactive" data-name="interactive" value="true">
            <span></span>
        </label></h3>
        
        <div class="clear"></div>

    </div>

    <div class="alignleft shortcodelic_opts_list_2 shortcodelic_tinymce_area">
    </div>

</div><!-- #shortcodelic_tooltips_generator -->