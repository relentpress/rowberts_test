<?php

    global $post, $display, $content_width;

    if ( ! isset( $content_width ) ) $content_width = 1280;

    //delete_post_meta('2', 'shortcodelic_tabs');

?>

<?php /*********** SHORTCODE SELECTOR ***********/ ?>

<div id="shortcodelic_selection" class="shortcodelic_meta" data-title="<?php _e('Shortcodes', 'shortcodelic'); ?>">
    <label class="for_select">
        <span class="for_select">
            <select>
                <option value=""><?php _e('Select a shortcode generator', 'shortcodelic'); ?></option>
                <option value="slideshows" data-cols="three"><?php _e('Slideshow', 'shortcodelic'); ?></option>
                <option value="tabs" data-cols="two"><?php _e('Tabs &amp; Accordions', 'shortcodelic'); ?></option>
                <option value="tables" data-cols="three"><?php _e('Data tables', 'shortcodelic'); ?></option>
                <option value="maps" data-cols="three"><?php _e('Maps', 'shortcodelic'); ?></option>
                <option value="carousels" data-cols="three"><?php _e('Carousels', 'shortcodelic'); ?></option>
                <option value="postcarousels" data-cols="three"><?php _e('Carousels from posts', 'shortcodelic'); ?></option>
<?php
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    if (is_plugin_active('woocommerce/woocommerce.php')) { ?>
                <option value="woocarousels" data-cols="three"><?php _e('WooCommerce carousel', 'shortcodelic'); ?></option>
    <?php }
?>
            </select>
        </span>
    </label>
</div><!-- #shortcodelic_selection -->

<div id="shortcodelic_slideshows_wrap" data-url="<?php echo SHORTCODELIC_URL . '/lib/slideshow_generator.php'; ?>">
<?php /*********** SLIDESHOW GENERATOR ***********/

require_once( SHORTCODELIC_PATH . 'lib/slideshow_generator.php' );

?>
</div>

<div id="shortcodelic_tabs_wrap" data-url="<?php echo SHORTCODELIC_URL . '/lib/tabs_generator.php'; ?>">
<?php /*********** TABS GENERATOR ***********/

require_once( SHORTCODELIC_PATH . 'lib/tabs_generator.php' );

?>
</div>

<div id="shortcodelic_tables_wrap" data-url="<?php echo SHORTCODELIC_URL . '/lib/tables_generator.php'; ?>">
<?php /*********** TABLES GENERATOR ***********/

require_once( SHORTCODELIC_PATH . 'lib/tables_generator.php' );

?>
</div>

<div id="shortcodelic_maps_wrap" data-url="<?php echo SHORTCODELIC_URL . '/lib/maps_generator.php'; ?>">
<?php /*********** MAPS GENERATOR ***********/

require_once( SHORTCODELIC_PATH . 'lib/maps_generator.php' );

?>
</div>
    
<div id="shortcodelic_carousels_wrap" data-url="<?php echo SHORTCODELIC_URL . '/lib/carousels_generator.php'; ?>">
<?php /*********** CAROUSEL GENERATOR ***********/

require_once( SHORTCODELIC_PATH . 'lib/carousels_generator.php' );

?>
</div>

<div id="shortcodelic_postcarousels_wrap" data-url="<?php echo SHORTCODELIC_URL . '/lib/postcarousels_generator.php'; ?>">
<?php /*********** POST CAROUSEL GENERATOR ***********/

require_once( SHORTCODELIC_PATH . 'lib/postcarousels_generator.php' );

?>
</div>

<?php if (is_plugin_active('woocommerce/woocommerce.php')) { ?>
<div id="shortcodelic_woocarousels_wrap" data-url="<?php echo SHORTCODELIC_URL . '/lib/woocarousels_generator.php'; ?>">
<?php /*********** WOOCOMMERCE CAROUSEL GENERATOR ***********/

require_once( SHORTCODELIC_PATH . 'lib/woocarousels_generator.php' );

?>
</div>
<?php } ?>

<?php /*********** FONT SELECTOR ***********/

require_once( SHORTCODELIC_PATH . 'lib/fonticon_generator.php' );

?>

<?php /*********** TOOLTIP GENERATOR ***********/

require_once( SHORTCODELIC_PATH . 'lib/tooltip_generator.php' );

?>

<?php /*********** PROGRESS BARS GENERATOR ***********/

require_once( SHORTCODELIC_PATH . 'lib/progress_generator.php' );

?>

<?php /*********** PROGRESS BUTTONS GENERATOR ***********/

require_once( SHORTCODELIC_PATH . 'lib/buttons_generator.php' );

?>

<?php /*********** PROGRESS BOXES GENERATOR ***********/

require_once( SHORTCODELIC_PATH . 'lib/box_generator.php' );

?>

<div id="shortcodelic_tinymce">
    <?php 
        wp_editor( '','shortcodelicTMCE' );
        wp_editor( '','tooltipTMCE' );
        wp_editor( '','boxTMCE' );
    ?> 
</div>

<div id="spinner_wrap">
    <div id="spinner2"></div>
</div>

