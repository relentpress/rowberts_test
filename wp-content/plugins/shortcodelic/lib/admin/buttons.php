<?php
	global $options;

    if (isset($_GET['page']) && $_GET['page']=='shortcodelic_buttons') {
?>

        <section id="pix_content_loaded">
            <h3><?php _e('Options','shortcodelic'); ?>: <small><?php _e('Buttons','shortcodelic'); ?></small></h3>

            <?php if (get_option('shortcodelic_allow_ajax')=='true') { ?>
            <form action="/" class="dynamic_form ajax_form cf">
            <?php } else { ?>
            <form method="post" class="dynamic_form cf" action="<?php echo admin_url("admin.php?page=shortcodelic_admin"); ?>">
            <?php } ?>

                <?php
                    $shortcodelic_array_buttons_ = get_option('shortcodelic_array_buttons_');
                    echo '<input type="hidden" name="shortcodelic_array_buttons_" value="">';
                ?>

                <div class="pix_columns cf clone hidden">
                    <h4 class="section_title active"><a href="#" class="delete"><i class="scicon-awesome-trash"></i></a><span>clone</span></h4>

                    <div class="admin-section-toggle">
                        <div class="pix_column_divider alignleft"></div><!-- .pix_column_divider -->
                        <div class="pix_column alignleft">

                            <label for="shortcodelic_array_buttons_i_color"><?php _e('Text color','shortcodelic'); ?>:</label>
                            <div class="pix_color_picker">
                                <input id="shortcodelic_array_buttons_i_color" data-name="shortcodelic_array_buttons_[i][color]" type="text" value="#ff9541">
                                <a class="pix_button" href="#"></a>
                                <div class="colorpicker"></div>
                                <i class="scicon-elusive-cancel"></i>
                            </div>
                            <br>

                            <label for="shortcodelic_array_buttons_i_background"><?php _e('Background color','shortcodelic'); ?>:</label>
                            <div class="pix_color_picker">
                                <input id="shortcodelic_array_buttons_i_background" data-name="shortcodelic_array_buttons_[i][background]" type="text" value="transparent">
                                <a class="pix_button" href="#"></a>
                                <div class="colorpicker"></div>
                                <i class="scicon-elusive-cancel"></i>
                            </div>
                            <br>

                            <label for="shortcodelic_array_buttons_i_texthover"><?php _e('Text color on hover state','shortcodelic'); ?>:</label>
                            <div class="pix_color_picker">
                                <input id="shortcodelic_array_buttons_i_texthover" data-name="shortcodelic_array_buttons_[i][texthover]" type="text" value="#ffffff">
                                <a class="pix_button" href="#"></a>
                                <div class="colorpicker"></div>
                                <i class="scicon-elusive-cancel"></i>
                            </div>
                            <br>

                            <label for="shortcodelic_array_buttons_i_bghover"><?php _e('Background color on hover state','shortcodelic'); ?>:</label>
                            <div class="pix_color_picker">
                                <input id="shortcodelic_array_buttons_i_bghover" data-name="shortcodelic_array_buttons_[i][bghover]" type="text" value="#ff9541">
                                <a class="pix_button" href="#"></a>
                                <div class="colorpicker"></div>
                                <i class="scicon-elusive-cancel"></i>
                            </div>
                            <br>

                            <label for="shortcodelic_array_buttons_i_borderradius"><?php _e( 'Border radius', 'shortcodelic' ); ?>:</label>
                            <div class="slider_div stroke">
                                <input type="text" value="4" data-name="shortcodelic_array_buttons_[i][borderradius]" id="shortcodelic_array_buttons_i_borderradius">
                                <div class="slider_cursor"></div>
                            </div><!-- .slider_div -->
                            <br>

                            <label class="for_select" for="shortcodelic_array_buttons_i_fx"><?php _e('Effect on hover','shortcodelic'); ?>:
                                <span class="for_select">
                                    <select data-name="shortcodelic_array_buttons_[i][fx]" id="shortcodelic_array_buttons_i_fx">
                                        <option value=""><?php _e('default','shortcodelic'); ?></option>
                                        <option value="expand"><?php _e('expand color','shortcodelic'); ?></option>
                                    </select>
                                </span>
                            </label>
                            <br>

                            <label class="for_select" for="shortcodelic_array_buttons_i_icon"><?php _e('Icons','shortcodelic'); ?>:
                                <span class="for_select">
                                    <select data-name="shortcodelic_array_buttons_[i][icon]" id="shortcodelic_array_buttons_i_icon">
                                        <option value=""><?php _e('just show','shortcodelic'); ?></option>
                                        <option value="hover"><?php _e('show on hover only','shortcodelic'); ?></option>
                                    </select>
                                </span>
                            </label>
                            <br>

                        </div><!-- .pix_column.first -->
                        <div class="pix_column alignright">

                            <label for="shortcodelic_array_buttons_i_bordercolor"><?php _e('Border color','shortcodelic'); ?>:</label>
                            <div class="pix_color_picker">
                                <input id="shortcodelic_array_buttons_i_bordercolor" data-name="shortcodelic_array_buttons_[i][bordercolor]" type="text" value="#ff9541">
                                <a class="pix_button" href="#"></a>
                                <div class="colorpicker"></div>
                                <i class="scicon-elusive-cancel"></i>
                            </div>
                            <br>

                            <label for="shortcodelic_array_buttons_i_borderwidth"><?php _e( 'Border width', 'shortcodelic' ); ?>:</label>
                            <div class="slider_div stroke">
                                <input type="text" value="3" data-name="shortcodelic_array_buttons_[i][borderwidth]" id="shortcodelic_array_buttons_i_borderwidth">
                                <div class="slider_cursor"></div>
                            </div><!-- .slider_div -->
                            <br>

                            <label for="shortcodelic_array_buttons_i_bordercolorhover"><?php _e('Border color on hover state','shortcodelic'); ?>:</label>
                            <div class="pix_color_picker">
                                <input id="shortcodelic_array_buttons_i_bordercolorhover" data-name="shortcodelic_array_buttons_[i][bordercolorhover]" type="text" value="#ff9541">
                                <a class="pix_button" href="#"></a>
                                <div class="colorpicker"></div>
                                <i class="scicon-elusive-cancel"></i>
                            </div>
                            <br>

                            <label for="shortcodelic_array_buttons_i_style"><?php _e( 'Custom styles', 'shortcodelic' ); ?>:</label>
                            <textarea data-name="shortcodelic_array_buttons_[i][style]" id="shortcodelic_array_buttons_i_style" class="codemirror">.buttonelic.[i] {
    font-weight: bold;
    padding: 0.5em 1em;
    text-transform: uppercase;
}</textarea>
                            <br>

                        </div><!-- .pix_column.second -->

                    </div><!-- .admin-section-toggle -->
                </div><!-- .pix_columns -->

                <?php apply_filters( 'shortcodelic_extra_button_styles', '' ); ?>
                <?php $i = 0; if (isset($shortcodelic_array_buttons_) && is_array($shortcodelic_array_buttons_)) foreach ($shortcodelic_array_buttons_ as $key => $value) { ?>

                    <div class="pix_columns cf">
                        <h4 class="section_title"><a href="#" class="delete"><i class="scicon-awesome-trash"></i></a><span><?php echo $key; ?></span></h4>

                        <div class="admin-section-toggle">
                            <div class="pix_column_divider alignleft"></div><!-- .pix_column_divider -->
                            <div class="pix_column alignleft">

                                <label for="shortcodelic_array_buttons_<?php echo $key; ?>_color"><?php _e('Text color','shortcodelic'); ?>:</label>
                                <div class="pix_color_picker">
                                    <input id="shortcodelic_array_buttons_<?php echo $key; ?>_color" name="shortcodelic_array_buttons_[<?php echo $key; ?>][color]" type="text" value="<?php echo esc_attr($shortcodelic_array_buttons_[$key]['color']); ?>">
                                    <a class="pix_button" href="#"></a>
                                    <div class="colorpicker"></div>
                                    <i class="scicon-elusive-cancel"></i>
                                </div>
                                <br>

                                <label for="shortcodelic_array_buttons_<?php echo $key; ?>_background"><?php _e('Background color','shortcodelic'); ?>:</label>
                                <div class="pix_color_picker">
                                    <input id="shortcodelic_array_buttons_<?php echo $key; ?>_background" name="shortcodelic_array_buttons_[<?php echo $key; ?>][background]" type="text" value="<?php echo esc_attr($shortcodelic_array_buttons_[$key]['background']); ?>">
                                    <a class="pix_button" href="#"></a>
                                    <div class="colorpicker"></div>
                                    <i class="scicon-elusive-cancel"></i>
                                </div>
                                <br>

                                <label for="shortcodelic_array_buttons_<?php echo $key; ?>_texthover"><?php _e('Text color on hover state','shortcodelic'); ?>:</label>
                                <div class="pix_color_picker">
                                    <input id="shortcodelic_array_buttons_<?php echo $key; ?>_texthover" name="shortcodelic_array_buttons_[<?php echo $key; ?>][texthover]" type="text" value="<?php echo esc_attr($shortcodelic_array_buttons_[$key]['texthover']); ?>">
                                    <a class="pix_button" href="#"></a>
                                    <div class="colorpicker"></div>
                                    <i class="scicon-elusive-cancel"></i>
                                </div>
                                <br>

                                <label for="shortcodelic_array_buttons_<?php echo $key; ?>_bghover"><?php _e('Background color on hover state','shortcodelic'); ?>:</label>
                                <div class="pix_color_picker">
                                    <input id="shortcodelic_array_buttons_<?php echo $key; ?>_bghover" name="shortcodelic_array_buttons_[<?php echo $key; ?>][bghover]" type="text" value="<?php echo esc_attr($shortcodelic_array_buttons_[$key]['bghover']); ?>">
                                    <a class="pix_button" href="#"></a>
                                    <div class="colorpicker"></div>
                                    <i class="scicon-elusive-cancel"></i>
                                </div>
                                <br>

                                <label for="shortcodelic_array_buttons_<?php echo $key; ?>_borderradius"><?php _e( 'Border radius', 'shortcodelic' ); ?>:</label>
                                <div class="slider_div stroke">
                                    <input type="text" value="<?php echo esc_attr($shortcodelic_array_buttons_[$key]['borderradius']); ?>" name="shortcodelic_array_buttons_[<?php echo $key; ?>][borderradius]" id="shortcodelic_array_buttons_<?php echo $key; ?>_borderradius">
                                    <div class="slider_cursor"></div>
                                </div><!-- .slider_div -->
                                <br>

                                <label class="for_select" for="shortcodelic_array_buttons_<?php echo $key; ?>_fx"><?php _e('Effect on hover','shortcodelic'); ?>:
                                    <span class="for_select">
                                        <select name="shortcodelic_array_buttons_[<?php echo $key; ?>][fx]" id="shortcodelic_array_buttons_<?php echo $key; ?>_fx">
                                            <option value="" <?php selected($shortcodelic_array_buttons_[$key]['fx'], ''); ?>><?php _e('default','shortcodelic'); ?></option>
                                            <option value="expand" <?php selected($shortcodelic_array_buttons_[$key]['fx'], 'expand'); ?>><?php _e('expand color','shortcodelic'); ?></option>
                                        </select>
                                    </span>
                                </label>
                                <br>

                                <label class="for_select" for="shortcodelic_array_buttons_<?php echo $key; ?>_icon"><?php _e('Icons','shortcodelic'); ?>:
                                    <span class="for_select">
                                        <select name="shortcodelic_array_buttons_[<?php echo $key; ?>][icon]" id="shortcodelic_array_buttons_<?php echo $key; ?>_icon">
                                            <option value="" <?php selected($shortcodelic_array_buttons_[$key]['icon'], ''); ?>><?php _e('just show','shortcodelic'); ?></option>
                                            <option value="hover" <?php selected($shortcodelic_array_buttons_[$key]['icon'], 'hover'); ?>><?php _e('show on hover only','shortcodelic'); ?></option>
                                        </select>
                                    </span>
                                </label>
                                <br>

                            </div><!-- .pix_column.first -->
                            <div class="pix_column alignright">

                                <label for="shortcodelic_array_buttons_<?php echo $key; ?>_bordercolor"><?php _e('Border color','shortcodelic'); ?>:</label>
                                <div class="pix_color_picker">
                                    <input id="shortcodelic_array_buttons_<?php echo $key; ?>_bordercolor" name="shortcodelic_array_buttons_[<?php echo $key; ?>][bordercolor]" type="text" value="<?php echo esc_attr($shortcodelic_array_buttons_[$key]['bordercolor']); ?>">
                                    <a class="pix_button" href="#"></a>
                                    <div class="colorpicker"></div>
                                    <i class="scicon-elusive-cancel"></i>
                                </div>
                                <br>

                                <label for="shortcodelic_array_buttons_<?php echo $key; ?>_borderwidth"><?php _e( 'Border width', 'shortcodelic' ); ?>:</label>
                                <div class="slider_div stroke">
                                    <input type="text" value="<?php echo esc_attr($shortcodelic_array_buttons_[$key]['borderwidth']); ?>" name="shortcodelic_array_buttons_[<?php echo $key; ?>][borderwidth]" id="shortcodelic_array_buttons_<?php echo $key; ?>_borderwidth">
                                    <div class="slider_cursor"></div>
                                </div><!-- .slider_div -->
                                <br>

                                <label for="shortcodelic_array_buttons_<?php echo $key; ?>_bordercolorhover"><?php _e('Border color on hover state','shortcodelic'); ?>:</label>
                                <div class="pix_color_picker">
                                    <input id="shortcodelic_array_buttons_<?php echo $key; ?>_bordercolorhover" name="shortcodelic_array_buttons_[<?php echo $key; ?>][bordercolorhover]" type="text" value="<?php echo esc_attr($shortcodelic_array_buttons_[$key]['bordercolorhover']); ?>">
                                    <a class="pix_button" href="#"></a>
                                    <div class="colorpicker"></div>
                                    <i class="scicon-elusive-cancel"></i>
                                </div>
                                <br>

                                <label for="shortcodelic_array_buttons_<?php echo $key; ?>_style"><?php _e( 'Custom styles', 'shortcodelic' ); ?>:</label>
                                <textarea name="shortcodelic_array_buttons_[<?php echo $key; ?>][style]" id="shortcodelic_array_buttons_<?php echo $key; ?>_style" class="codemirror"><?php echo esc_attr($shortcodelic_array_buttons_[$key]['style']); ?></textarea>
                                <br>

                            </div><!-- .pix_column.second -->

                        </div><!-- .admin-section-toggle -->
                    </div><!-- .pix_columns -->

                <?php $i++; } ?>

                <a href="#" class="add-button"><i class="scicon-entypo-plus"></i></a>

                <input type="hidden" name="compile_css" value="compile_css" />

                <input type="hidden" name="action" value="shortocodelic_data_save" />
                <input type="hidden" name="shortcodelic_security" value="<?php echo wp_create_nonce('shortcodelic_data'); ?>" />
                <button type="submit" class="pix-save-options pix_button fake_button alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <button type="submit" class="pix-save-options pix_button fake_button2 alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <button type="submit" class="pix-save-options pix_button alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <div id="gradient-save-button"></div>

            </form><!-- .dynamic_form -->

            <div class="hidden">
                <div id="type_button">
                    <label><?php _e('Type here below a class for your button','shortcodelic'); ?></label>
                    <input type="text" placeholder="type a class" value="">
                    <small><?php _e('Use latin characters and numbers only, do not start with a number and avoid empty spaces','shortcodelic'); ?></small>
                </div><!-- #type_button -->

                <div id="delete_button">
                    <label><?php _e('Are you sure you want to delete it? You won\'t be able to restore it later','shortcodelic'); ?></label>
                </div><!-- #type_button -->
            </div><!-- .hidden -->

        </section><!-- #pix_content_loaded -->
<?php 
	}