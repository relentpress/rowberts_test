<?php
	global $options;

    if (isset($_GET['page']) && $_GET['page']=='shortcodelic_tabs') {
?>

        <section id="pix_content_loaded">
            <h3><?php _e('Options','shortcodelic'); ?>: <small><?php _e('Tabs and accordions','shortcodelic'); ?></small></h3>

            <?php if (get_option('shortcodelic_allow_ajax')=='true') { ?>
            <form action="/" class="dynamic_form ajax_form cf">
            <?php } else { ?>
            <form method="post" class="dynamic_form cf" action="<?php echo admin_url("admin.php?page=shortcodelic_admin"); ?>">
            <?php } ?>
            
                <div class="pix_columns cf">
                    <div class="pix_column_divider alignleft"></div><!-- .pix_column_divider -->
                    <div class="pix_column alignleft">

                        <label for="shortcodelic_tab_border_color"><?php _e('Border color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_tab_border_color" name="shortcodelic_tab_border_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_tab_border_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_tab_bg_color"><?php _e('Background color of the tabs','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_tab_bg_color" name="shortcodelic_tab_bg_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_tab_bg_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_tab_color"><?php _e('Text color of the tabs','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_tab_color" name="shortcodelic_tab_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_tab_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                    </div><!-- .pix_column.first -->
                    <div class="pix_column alignright">

                        <label for="shortcodelic_tab_border_width"><?php _e( 'Border width', 'shortcodelic' ); ?>:</label>
                        <div class="slider_div stroke">
                            <input type="text" value="<?php echo stripslashes(esc_html(get_option('shortcodelic_tab_border_width'))); ?>" name="shortcodelic_tab_border_width" id="shortcodelic_tab_border_width">
                            <div class="slider_cursor"></div>
                        </div><!-- .slider_div -->
                        <br>

                        <label for="shortcodelic_panel_bg_color"><?php _e('Background color of the panels','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_panel_bg_color" name="shortcodelic_panel_bg_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_panel_bg_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_panel_text_color"><?php _e('Text color of the panels','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_panel_text_color" name="shortcodelic_panel_text_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_panel_text_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>
                    </div><!-- .pix_column.second -->

                </div><!-- .pix_columns -->

                <div class="clear"></div>

                <input type="hidden" name="compile_css" value="compile_css" />

                <input type="hidden" name="action" value="shortocodelic_data_save" />
                <input type="hidden" name="shortcodelic_security" value="<?php echo wp_create_nonce('shortcodelic_data'); ?>" />
                <button type="submit" class="pix-save-options pix_button fake_button alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <button type="submit" class="pix-save-options pix_button fake_button2 alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <button type="submit" class="pix-save-options pix_button alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <div id="gradient-save-button"></div>

            </form><!-- .dynamic_form -->

        </section><!-- #pix_content_loaded -->
<?php 
	}