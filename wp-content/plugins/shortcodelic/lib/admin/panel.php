<?php
	global $options;

	$plugin = get_plugin_data(SHORTCODELIC_PATH.'shortcodelic.php');

    if (isset($_GET['page']) && $_GET['page']=='shortcodelic_admin') {
?>

	<?php
	/******************************
	*
	*	Save option or compile css (for the AJAX method look functions.php -> save_via_ajax() )
	*
	******************************/
		foreach ($_REQUEST as $key => $value) {
			if ( $key=='compile_css' && $value=="compile_css" ) {
				$context = $_REQUEST;
                $shortCodelic = new ShortCodelic(); 
                $shortCodelic->compile_css($context);
			} elseif ( $key=='register_details' && $value=="register_details" ) {
				$context = $_REQUEST;
                $shortCodelic = new ShortCodelic(); 
                $shortCodelic->check_license($context);
			} elseif ( preg_match("/shortcodelic_array/", $key) ) {
				delete_option($key);
				add_option($key, $value);
			} else {
				if(isset($_REQUEST[$key]) ) {
					update_option($key, $value);
				}
			}
		}

	?>
<div id="pix_ap_wrap">
	<?php if ( get_option('shortcodelic_info_update')=='' ) { ?>
	<div class="outer_info alert">
		Since <strong>ShortCodelic 1.2.0</strong> is available the automatic update for the plugin, just go to <strong>Shortcodelic &rarr; General &rarr; Register</strong> and follow the instructions: enter the details of your purchase (your CodeCanyon username and your purchaser license code... screenshot about that are available on the admin panel itself).<br>
		After entering the details and saving, you should receive a &quot;Success&quot; message: now you'll be notified when a new version of the plugin is available and you'll be able to update directly from your WP dashboard.<br>
		The automatic update is intended for the regular licenses only and it allows until <strong>5 installations</strong> (I imagined beta test sites and some domain changes etc.). For questions regarding extended licenses or any issues contact me on <a href="http://codecanyon.net/user/pixedelic" target="_blank">CodeCanyon</a><br>
		<strong>N.B.:</strong> the notifier works only with the plugin activated.
		<span class="info_close"><input type="hidden" name="shortcodelic_info_update" value=""></span>
		<input type="hidden" name="action" value="shortocodelic_data_save" />
		<input type="hidden" name="shortcodelic_security" value="<?php echo wp_create_nonce('shortcodelic_data'); ?>" />
	</div>
	<?php } ?>
	<header id="pix_ap_header" class="cf">
		<section class="alignleft">
			<h2><?php echo $plugin['Name']; ?></h2>
		</section><!-- .alignleft -->
		<section class="alignright">
			<h5><?php _e('Version','shortcodelic'); ?> <?php echo $plugin['Version']; ?><?php do_action('pix_check_update'); ?></h5>
			<a href="http://www.pixedelic.com/shortcodelic_current_version_changelog.php" class="pix_button pix-iframe"><?php _e('See the changelog','shortcodelic'); ?></a>
		</section><!-- .alignright -->
	</header><!-- #pix_ap_header -->

	<section id="pix_ap_body" class="cf">
		<div id="pix_ap_main_nav_fake">
		</div><!-- #pix_ap_main_nav_fake -->
		<nav id="pix_ap_main_nav">
			<ul>
				<li class="cf" data-store="general">
					<a href="#">
						<i class="scicon-awesome-cog"></i>
					</a>
					<ul>
						<?php if ( apply_filters('shortcodelic_display_verygeneral', true) === true ) { ?>
                    	<li data-store="verygeneral">
                        	<a href="<?php echo get_admin_url(); ?>admin.php?page=shortcodelic_verygeneral"><?php _e('Very general','shortcodelic'); ?></a>
                        </li>
                        <?php } ?>
						<?php if ( apply_filters('shortcodelic_display_register', true) === true ) { ?>
                    	<li data-store="register">
                        	<a href="<?php echo get_admin_url(); ?>admin.php?page=shortcodelic_register"><?php _e('Register','shortcodelic'); ?></a>
                        </li>
                        <?php } ?>
						<?php if ( apply_filters('shortcodelic_display_slideshows', true) === true ) { ?>
						<li data-store="slideshows">
							<a href="<?php echo get_admin_url(); ?>admin.php?page=shortcodelic_slideshows"><?php _e('Slideshows','shortcodelic'); ?></a>
						</li>
                        <?php } ?>
						<?php if ( apply_filters('shortcodelic_display_tabs', true) === true ) { ?>
						<li data-store="tabs">
							<a href="<?php echo get_admin_url(); ?>admin.php?page=shortcodelic_tabs"><?php _e('Tabs and accordions','shortcodelic'); ?></a>
						</li>
                        <?php } ?>
						<?php if ( apply_filters('shortcodelic_display_tables', true) === true ) { ?>
						<li data-store="tables">
							<a href="<?php echo get_admin_url(); ?>admin.php?page=shortcodelic_tables"><?php _e('Tables','shortcodelic'); ?></a>
						</li>
                        <?php } ?>
						<?php if ( apply_filters('shortcodelic_display_carousels', true) === true ) { ?>
						<li data-store="carousels">
							<a href="<?php echo get_admin_url(); ?>admin.php?page=shortcodelic_carousels"><?php _e('Carousels','shortcodelic'); ?></a>
						</li>
                        <?php } ?>
						<?php if ( apply_filters('shortcodelic_display_tooltips', true) === true ) { ?>
						<li data-store="tooltips">
							<a href="<?php echo get_admin_url(); ?>admin.php?page=shortcodelic_tooltips"><?php _e('Tooltips','shortcodelic'); ?></a>
						</li>
                        <?php } ?>
						<?php if ( apply_filters('shortcodelic_display_progress', true) === true ) { ?>
						<li data-store="progress">
							<a href="<?php echo get_admin_url(); ?>admin.php?page=shortcodelic_progress"><?php _e('Progress bars','shortcodelic'); ?></a>
						</li>
                        <?php } ?>
						<?php if ( apply_filters('shortcodelic_display_buttons', true) === true ) { ?>
						<li data-store="buttons">
							<a href="<?php echo get_admin_url(); ?>admin.php?page=shortcodelic_buttons"><?php _e('Buttons','shortcodelic'); ?></a>
						</li>
                        <?php } ?>
						<?php if ( apply_filters('shortcodelic_display_boxes', true) === true ) { ?>
						<li data-store="boxes">
							<a href="<?php echo get_admin_url(); ?>admin.php?page=shortcodelic_boxes"><?php _e('Text boxes','shortcodelic'); ?></a>
						</li>
                        <?php } ?>
						<?php if ( apply_filters('shortcodelic_display_css', true) === true ) { ?>
						<li data-store="css">
							<a href="<?php echo get_admin_url(); ?>admin.php?page=shortcodelic_css"><?php _e('Compile CSS file','shortcodelic'); ?></a>
						</li>
                        <?php } ?>
					</ul>
				</li>
				<li class="cf documentation">
					<a href="<?php echo SHORTCODELIC_URL; ?>lib/admin/documentation.htm">
						<i class="scicon-modern-book-2"></i>
					</a>
				</li>
			</ul>
		</nav>
        <section id="pix_content_loaded">
        </section><!-- #pix_content_loaded -->
	</section><!-- #pix_ap_body -->

	<div id="spinner_wrap">
    	<div id="spinner">
		    <span id="ball_1" class="spinner_ball"></span>
		    <span id="ball_2" class="spinner_ball"></span>
		    <span id="ball_3" class="spinner_ball"></span>
		    <span id="ball_4" class="spinner_ball"></span>
		    <span id="ball_5" class="spinner_ball"></span>
		</div>
    	<div id="spinner2">
		    <span id="ball_1_2" class="spinner_ball_2"></span>
		    <span id="ball_2_2" class="spinner_ball_2"></span>
		    <span id="ball_3_2" class="spinner_ball_2"></span>
		    <span id="ball_4_2" class="spinner_ball_2"></span>
		    <span id="ball_5_2" class="spinner_ball_2"></span>
		</div>
	</div>
</div><!-- #pix_ap_wrap -->

<?php 
	}