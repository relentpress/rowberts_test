<?php
	global $options;

    if (isset($_GET['page']) && $_GET['page']=='shortcodelic_tooltips') {
?>

        <section id="pix_content_loaded">
            <h3><?php _e('Options','shortcodelic'); ?>: <small><?php _e('Tooltips','shortcodelic'); ?></small></h3>

            <?php if (get_option('shortcodelic_allow_ajax')=='true') { ?>
            <form action="/" class="dynamic_form ajax_form cf">
            <?php } else { ?>
            <form method="post" class="dynamic_form cf" action="<?php echo admin_url("admin.php?page=shortcodelic_admin"); ?>">
            <?php } ?>

                <div class="pix_columns cf">
                    <div class="pix_column_divider alignleft"></div><!-- .pix_column_divider -->
                    <div class="pix_column alignleft">

                        <label for="shortcodelic_tooltip_color"><?php _e('Tooltip text color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_tooltip_color" name="shortcodelic_tooltip_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_tooltip_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_tooltip_link"><?php _e('Tooltip link color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_tooltip_link" name="shortcodelic_tooltip_link" type="text" value="<?php echo esc_attr(get_option('shortcodelic_tooltip_link')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_tooltip_bg"><?php _e('Tooltip background color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_tooltip_bg" name="shortcodelic_tooltip_bg" type="text" value="<?php echo esc_attr(get_option('shortcodelic_tooltip_bg')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_tooltip_radius"><?php _e( 'Tooltip border radius', 'shortcodelic' ); ?>:</label>
                        <div class="slider_div stroke">
                            <input type="text" value="<?php echo stripslashes(esc_html(get_option('shortcodelic_tooltip_radius'))); ?>" name="shortcodelic_tooltip_radius" id="shortcodelic_tooltip_radius">
                            <div class="slider_cursor"></div>
                        </div><!-- .slider_div -->
                        <br>

                    </div><!-- .pix_column.first -->
                    <div class="pix_column alignright">

                        <label for="shortcodelic_tooltip_border"><?php _e( 'Tooltip border width', 'shortcodelic' ); ?>:</label>
                        <div class="slider_div stroke">
                            <input type="text" value="<?php echo stripslashes(esc_html(get_option('shortcodelic_tooltip_border'))); ?>" name="shortcodelic_tooltip_border" id="shortcodelic_tooltip_border">
                            <div class="slider_cursor"></div>
                        </div><!-- .slider_div -->
                        <br>

                        <label for="shortcodelic_tooltip_border_color"><?php _e('Tooltip border color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_tooltip_border_color" name="shortcodelic_tooltip_border_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_tooltip_border_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_tooltip_shadow_size"><?php _e( 'Shadow size', 'shortcodelic' ); ?>:</label>
                        <div class="slider_div stroke">
                            <input type="text" value="<?php echo stripslashes(esc_html(get_option('shortcodelic_tooltip_shadow_size'))); ?>" name="shortcodelic_tooltip_shadow_size" id="shortcodelic_tooltip_shadow_size">
                            <div class="slider_cursor"></div>
                        </div><!-- .slider_div -->
                        <br>

                        <label for="shortcodelic_tooltip_shadow_color"><?php _e('Shadow color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_tooltip_shadow_color" name="shortcodelic_tooltip_shadow_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_tooltip_shadow_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_tooltip_shadow_opacity"><?php _e( 'Shadow opacity', 'shortcodelic' ); ?>:</label>
                        <div class="slider_div opacity">
                            <input type="text" value="<?php echo stripslashes(esc_html(get_option('shortcodelic_tooltip_shadow_opacity'))); ?>" name="shortcodelic_tooltip_shadow_opacity" id="shortcodelic_tooltip_shadow_opacity">
                            <div class="slider_cursor"></div>
                        </div><!-- .slider_div -->
                        <br>

                    </div><!-- .pix_column.second -->

                </div><!-- .pix_columns -->

                <div class="clear"></div>

                <input type="hidden" name="compile_css" value="compile_css" />

                <input type="hidden" name="action" value="shortocodelic_data_save" />
                <input type="hidden" name="shortcodelic_security" value="<?php echo wp_create_nonce('shortcodelic_data'); ?>" />
                <button type="submit" class="pix-save-options pix_button fake_button alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <button type="submit" class="pix-save-options pix_button fake_button2 alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <button type="submit" class="pix-save-options pix_button alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <div id="gradient-save-button"></div>

            </form><!-- .dynamic_form -->

        </section><!-- #pix_content_loaded -->
<?php 
	}