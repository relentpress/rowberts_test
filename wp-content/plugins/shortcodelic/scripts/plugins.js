/**
 *
 **/
function mobiledetector(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		return true;
	}
}
function shuffle(array) {
	var size = array.length,
		rand, temp;
	for (var i = 1; i < size; i += 1) {
		rand = Math.round(Math.random() * i);
		temp = array[rand];
		array[rand] = array[i];
		array[i] = temp;
	}
	return array;
}

var isScrolledIntoView = function(elem) {
    var docViewTop = jQuery(window).scrollTop(),
		docViewBottom = docViewTop + jQuery(window).height(),
		elemTop = jQuery(elem).offset().top,
		elemBottom = elemTop + jQuery(elem).actual('height'),
		val = jQuery(elem).actual('height') < 80 ? jQuery(elem).actual('height') : 80;

    return ( (elemTop <= (docViewBottom-val) && elemTop >= docViewTop ) || (elemBottom <= (docViewBottom-val) && elemBottom >= docViewTop ) );
};
var isScrolledIntoViewHalf = function(elem) {
    var docViewTop = jQuery(window).scrollTop(),
		docViewBottom = docViewTop + jQuery(window).height(),
		elemTop = jQuery(elem).offset().top,
		elemBottom = elemTop + jQuery(elem).actual('height'),
		val = jQuery(elem).actual('height') < 80 ? jQuery(elem).actual('height') : 80;

    return ( (elemTop <= docViewBottom+val && elemTop >= docViewTop-val && elemBottom <= docViewBottom+val && elemBottom >= docViewTop-val));
};
/*! Copyright 2012, Ben Lin (http://dreamerslab.com/)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Version: 1.0.14
 *
 * Requires: jQuery 1.2.3 ~ 1.9.0
 */
;(function(e){e.fn.extend({actual:function(t,n){if(!this[t]){throw'$.actual => The jQuery method "'+t+'" you called does not exist'}var r={absolute:false,clone:false,includeMargin:false};var i=e.extend(r,n);var s=this.eq(0);var o,u;if(i.clone===true){o=function(){var e="position: absolute !important; top: -1000 !important; ";s=s.clone().attr("style",e).appendTo("body")};u=function(){s.remove()}}else{var a=[];var f="";var l;o=function(){if(e.fn.jquery>="1.8.0")l=s.parents().addBack().filter(":hidden");else l=s.parents().andSelf().filter(":hidden");f+="visibility: hidden !important; display: block !important; ";if(i.absolute===true)f+="position: absolute !important; ";l.each(function(){var t=e(this);a.push(t.attr("style"));t.attr("style",f)})};u=function(){l.each(function(t){var n=e(this);var r=a[t];if(r===undefined){n.removeAttr("style")}else{n.attr("style",r)}})}}o();var c=/(outer)/g.test(t)?s[t](i.includeMargin):s[t]();u();return c}})})(jQuery);


// adds .naturalWidth() and .naturalHeight() methods to jQuery
// for retreaving a normalized naturalWidth and naturalHeight.
// by Jack Moore - jacklmoore.com
;(function($){
	var
	props = ['Width', 'Height'],
	prop;

	while (prop = props.pop()) {
		(function (natural, prop) {
			$.fn[natural] = (natural in new Image()) ? 
			function () {
				return this[0][natural];
			} : 
			function () {
				var 
				node = this[0],
				img,
				value;

				if (node.tagName.toLowerCase() === 'img') {
					img = new Image();
					img.src = node.src,
					value = img[prop];
				}
				return value;
			};
		}('natural' + prop, prop.toLowerCase()));
	}
}(jQuery));

var buttonelicInit = function() {
	jQuery('.buttonelic').each(function(){
		var t = jQuery(this);
		if (t.data('init')!==true) {
			t.data('init', true);
			t.wrapInner('<span />');
			if ( jQuery('[class*="scicon-"]',t).length && jQuery('[class*="scicon-"]',t).css('position') == 'absolute' ) {
				t.addClass('buttonicon');
				jQuery('span',t).after(jQuery('[class*="scicon-"]',t));
			}
			t.append('<span />');
			var buttonIcon = function(){
				if ( jQuery('[class*="scicon-"]',t).length && jQuery('[class*="scicon-"]',t).css('position') == 'absolute' ) {
					var h = jQuery('> span:last-child',t).actual('height');
					jQuery('[class*="scicon-"]',t).css({lineHeight:h+'px'});
				}
			};
			buttonIcon();
			t.hover(function(){
				buttonIcon();
			});
		}
	});
};
jQuery(document).ready(function(){
	buttonelicInit();
    jQuery(document).bind('ajaxSuccess ajaxComplete', function() {
		buttonelicInit();
    });
});