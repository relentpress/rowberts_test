if (!jQuery.support.transition) jQuery.fn.transition = jQuery.fn.animate;

jQuery(function(){
	jQuery(window).bind('scroll resize load shortcodelic_ev', function(){
		var showProgress = function(elem){
			elem.each(function(){
				var t = jQuery(this),
					par = t.parent(),
					perc = t.find('[data-value][data-maximum]').data('value'),
					percTxt = t.find('[data-value][data-maximum]').data('value'),
					max = t.find('[data-value][data-maximum]').data('maximum'),
					opts = t.data('opts'),
					barColor = typeof opts.barColor !== 'undefined' && opts.barColor !== '' ? opts.barColor : '#ff9541',
					trackColor = typeof opts.trackColor !== 'undefined' && opts.trackColor !== '' ? opts.trackColor : '#f2f2f2',
					lineCap = typeof opts.lineCap !== 'undefined' && opts.lineCap !== '' ? opts.lineCap : 'butt',
					lineWidth = typeof opts.lineWidth !== 'undefined' && !isNaN(parseFloat(opts.lineWidth)) ? parseFloat(opts.lineWidth) : 20,
					size = typeof opts.size !== 'undefined' && !isNaN(parseFloat(opts.size)) ? parseFloat(opts.size) : 50,
					animate = typeof opts.animate !== 'undefined' && !isNaN(parseFloat(opts.animate)) ? parseFloat(opts.animate) : 1500,
					sizeChart = size-(lineWidth),
					fS = parseFloat(sizeChart*0.35) > 11 ? parseFloat(sizeChart*0.35) : 12,
					fS2 = parseFloat(lineWidth*0.5);

				animate = Math.floor((animate/2) + (animate * (perc/max)));

				perc = Math.floor((perc/max)*100);

				if ( t.hasClass('pix_progress_pie') ) {

					if ( Modernizr.csstransforms ) {
						t.find('.chart-amount').css({
							fontSize: fS*2,
							opacity: 0,
							height: size,
							lineHeight: size+'px',
							width: size
						}).transition({
							opacity: 1,
							scale: (1-(parseFloat(lineWidth)/(parseFloat(size)/2)))/2
						},(animate*0.9),'easeOutQuad');
					} else {
						t.find('.chart-amount').css({
							fontSize: fS*(1-(parseFloat(lineWidth)/(parseFloat(size)/2))),
							height: size,
							lineHeight: size+'px',
							width: size
						});
					}

					t.find('.chart-label').css({
						opacity: 0,
						top: '-'+(size/2)+'px',
					}).transition({
						opacity: 1,
						top: 0
					},(animate*0.9),'easeOutQuad');

					var options = {
						segmentShowStroke : false,
						percentageInnerCutout : 100-(parseFloat(lineWidth)/(parseFloat(size)/2))*100,
						animation : animate !== 0 ? true : false,
						animationSteps : parseFloat(animate)*0.06,
						animationEasing : "easeOutQuad",
						animateRotate : true,
						animateScale : false,
						onAnimationComplete : null
					};
					jQuery('canvas',t).each(function(){

						var canvas = jQuery(this),
							newSize = par.width(),
							newFs = parseFloat(newSize-(lineWidth/(size/newSize)))*0.35,
							ctx;
						newFs = newFs > 11 ? newFs : 12;

						var doughnutChartData = [
							{
								value: parseFloat(perc),
								color: barColor
							},
							{
								value : 100-parseFloat(perc),
								color : trackColor
							}							
						];

						if ( par.width() < size ) {
							t.css({
								height: newSize
							})
							.find('canvas').attr('height',newSize).attr('width',newSize);
							ctx = canvas.get(0).getContext("2d");
							new Chart(ctx).Doughnut(doughnutChartData,options);
							if ( Modernizr.csstransforms ) {
								t.find('.chart-amount').css({
									fontSize: newFs*2,
									height: newSize,
									lineHeight: newSize+'px',
									width: newSize
								});
							} else {
								t.find('.chart-amount').css({
									fontSize: newFs*(1-(parseFloat(lineWidth)/(parseFloat(size)/2))),
									height: newSize,
									lineHeight: newSize+'px',
									width: newSize
								});
							}
						} else {
							t.css({
								height: size,
								maxWidth: size
							});
							canvas.attr('height',size).attr('width',size);
							ctx = canvas.get(0).getContext("2d");
							new Chart(ctx).Doughnut(doughnutChartData,options);
							if ( Modernizr.csstransforms ) {
								t.find('.chart-amount').css({
									fontSize: fS*2,
									height: size,
									lineHeight: size+'px',
									width: size
								});
							} else {
								t.find('.chart-amount').css({
									fontSize: fS*(1-(parseFloat(lineWidth)/(parseFloat(size)/2))),
									height: size,
									lineHeight: size+'px',
									width: size
								});
							}
						}

						var wResize = jQuery(window).width();

						jQuery(window).bind('resize',function(){
							if ( jQuery(window).width() != wResize && t.is(':visible') ) {
								wResize = jQuery(window).width();
								options.animation = false;
								newSize = par.width();
								newFs = parseFloat(newSize-(lineWidth/(size/newSize)))*0.35;
								newFs = newFs > 11 ? newFs : 12;
								if ( par.width() < size ) {
									t.css({
										height: newSize
									})
									.find('canvas').attr('height',newSize).attr('width',newSize);
									ctx = canvas.get(0).getContext("2d");
									new Chart(ctx).Doughnut(doughnutChartData,options);
									t.find('.chart-amount').css({
										fontSize: newFs*2,
										height: newSize,
										lineHeight: newSize+'px',
										width: newSize
									});
								} else {
									t.css({
										height: size,
										maxWidth: size
									});
									canvas.attr('height',size).attr('width',size);
									ctx = canvas.get(0).getContext("2d");
									new Chart(ctx).Doughnut(doughnutChartData,options);
									t.find('.chart-amount').css({
										fontSize: fS*2,
										height: size,
										lineHeight: size+'px',
										width: size
									});
								}
							}
						});

						jQuery(window).bind('shortcodelic_ev',function(){
							if(t.is(':visible')) {
								wResize = jQuery(window).width();
								options.animation = false;
								newSize = par.width();
								newFs = parseFloat(newSize-(lineWidth/(size/newSize)))*0.35;
								newFs = newFs > 11 ? newFs : 12;
								if ( par.width() < size ) {
									t.css({
										height: newSize
									})
									.find('canvas').attr('height',newSize).attr('width',newSize);
									ctx = canvas.get(0).getContext("2d");
									new Chart(ctx).Doughnut(doughnutChartData,options);
									t.find('.chart-amount').css({
										fontSize: newFs*2,
										height: newSize,
										lineHeight: newSize+'px',
										width: newSize
									});
								} else {
									t.css({
										height: size,
										maxWidth: size
									});
									canvas.attr('height',size).attr('width',size);
									ctx = canvas.get(0).getContext("2d");
									new Chart(ctx).Doughnut(doughnutChartData,options);
									t.find('.chart-amount').css({
										fontSize: fS*2,
										height: size,
										lineHeight: size+'px',
										width: size
									});
								}
							}
						});

					});
					
				} else {

					var bRadius = lineCap == 'round' ? lineWidth/2 : 0,
						label = t.find('.chart-label'),
						w = parseFloat(t.width()),
						labelW, labelH;

					t.append(label);

					labelW = parseFloat(label.actual('outerWidth', { includeMargin : true }));
					labelH = parseFloat(label.actual('outerHeight', { includeMargin : true }));

					percent = w*(parseFloat(perc)/100);

					t.css({
						backgroundColor: trackColor,
						borderRadius: bRadius,
						height: lineWidth,
						marginTop: labelH*1.5
					});
					t.find('.chart-amount').add(label).css({
						opacity: 0,
						marginTop: '-'+(labelH)+'px'
					});

					if ( (percent) > (labelW+40) ) {
						label.addClass('left').removeClass('right');
					} else {
						label.removeClass('left').addClass('right');
					}

					t.find('[data-value][data-maximum]').transition({
						width: percent
					}, animate, 'easeOutQuad', function(){
						jQuery(this).css({width:perc+'%'});
					});
					t.find('.chart-amount').add(label).delay(animate/3).transition({
						opacity: 1,
					}, animate*(2/3), 'easeOutQuad');
				}

				jQuery({someValue:0}).animate({someValue:percTxt}, {
					duration: animate,
					easing: "easeOutQuad",
					step: function() {
						t.find('.percent').not('.label').text(Math.ceil(this.someValue));
					},
					complete: function(){
						t.find('.percent').not('.label').text(percTxt);
					}
				});
			});
		};

		jQuery('.pix_progress_bar').each(function(){
			var t = jQuery(this),
				opts = t.data('opts'),
				barColor = typeof opts.barColor !== 'undefined' && opts.barColor !== '' ? opts.barColor : '#ff9541',
				lineCap = typeof opts.lineCap !== 'undefined' && opts.lineCap !== '' ? opts.lineCap : 'butt',
				lineWidth = typeof opts.lineWidth !== 'undefined' && !isNaN(parseFloat(opts.lineWidth)) ? parseFloat(opts.lineWidth) : 20,
				bRadius = lineCap == 'round' ? lineWidth/2 : 0;
			t.find('[data-value][data-maximum]').css({
				backgroundColor: barColor,
				borderRadius: bRadius
			});
		});
		jQuery('.pix_progress_pie:visible, .pix_progress_bar:visible').not('.scrolled').each(function(){
			var t = jQuery(this),
				opts = t.data('opts'),
				animate = typeof opts.animate !== 'undefined' && !isNaN(parseFloat(opts.animate)) ? parseFloat(opts.animate) : 1500;
			if ( animate === 0 ) {
				showProgress(jQuery(this).addClass('scrolled'));
			} else if ( isScrolledIntoView(jQuery(this)) ) {
				showProgress(jQuery(this).addClass('scrolled'));
			}
		});
	}).triggerHandler('resize');
});