jQuery.noConflict();

/*! Copyright 2012, Ben Lin (http://dreamerslab.com/)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Version: 1.0.14
 *
 * Requires: jQuery 1.2.3 ~ 1.9.0
 */
;(function(e){e.fn.extend({actual:function(t,n){if(!this[t]){throw'$.actual => The jQuery method "'+t+'" you called does not exist'}var r={absolute:false,clone:false,includeMargin:false};var i=e.extend(r,n);var s=this.eq(0);var o,u;if(i.clone===true){o=function(){var e="position: absolute !important; top: -1000 !important; ";s=s.clone().attr("style",e).appendTo("body")};u=function(){s.remove()}}else{var a=[];var f="";var l;o=function(){if(e.fn.jquery>="1.8.0")l=s.parents().addBack().filter(":hidden");else l=s.parents().andSelf().filter(":hidden");f+="visibility: hidden !important; display: block !important; ";if(i.absolute===true)f+="position: absolute !important; ";l.each(function(){var t=e(this);a.push(t.attr("style"));t.attr("style",f)})};u=function(){l.each(function(t){var n=e(this);var r=a[t];if(r===undefined){n.removeAttr("style")}else{n.attr("style",r)}})}}o();var c=/(outer)/g.test(t)?s[t](i.includeMargin):s[t]();u();return c}})})(jQuery);


/*!
	jQuery Autosize v1.16.15
	(c) 2013 Jack Moore - jacklmoore.com
	updated: 2013-06-07
	license: http://www.opensource.org/licenses/mit-license.php
*/
;(function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i="hidden",n="border-box",s="lineHeight",a='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',r=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],l="oninput",c="onpropertychange",h=e(a).data("autosize",!0)[0];h.style.lineHeight="99px","99px"===e(h).css(s)&&r.push(s),h.style.lineHeight="",e.fn.autosize=function(s){return s=e.extend({},o,s||{}),h.parentNode!==document.body&&e(document.body).append(h),this.each(function(){function o(){if(t=w,h.className=s.className,p=parseInt(f.css("maxHeight"),10),e.each(r,function(e,t){h.style[t]=f.css(t)}),l in w){var o=w.style.width;w.style.width="0px",w.offsetWidth,w.style.width=o}}function a(){var e,n,a;t!==w&&o(),h.value=w.value+s.append,h.style.overflowY=w.style.overflowY,a=parseInt(w.style.height,10),h.style.width=Math.max(f.width(),0)+"px",h.scrollTop=0,h.scrollTop=9e4,e=h.scrollTop,p&&e>p?(e=p,n="scroll"):d>e&&(e=d),e+=b,w.style.overflowY=n||i,a!==e&&(w.style.height=e+"px",z&&s.callback.call(w,w))}var d,p,u,w=this,f=e(w),b=0,z=e.isFunction(s.callback);if(!f.data("autosize")){if((f.css("box-sizing")===n||f.css("-moz-box-sizing")===n||f.css("-webkit-box-sizing")===n)&&(b=f.outerHeight()-f.height()),d=Math.max(parseInt(f.css("minHeight"),10)-b||0,f.height()),u="none"===f.css("resize")||"vertical"===f.css("resize")?"none":"horizontal",f.css({overflow:i,overflowY:i,wordWrap:"break-word",resize:u}).data("autosize",!0),c in w?l in w?w[l]=w.onkeyup=a:w[c]=function(){"value"===event.propertyName&&a()}:w[l]=a,s.resizeDelay!==!1){var x,y=e(w).width();e(window).on("resize.autosize",function(){clearTimeout(x),x=setTimeout(function(){e(w).width()!==y&&a()},parseInt(s.resizeDelay,10))})}f.on("autosize",a),a()}})}})(window.jQuery||window.Zepto);

/*
 * jQuery resize event - v1.1 - 3/14/2010
 * http://benalman.com/projects/jquery-resize-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
;(function($,h,c){var a=$([]),e=$.resize=$.extend($.resize,{}),i,k="setTimeout",j="resize",d=j+"-special-event",b="delay",f="throttleWindow";e[b]=250;e[f]=true;$.event.special[j]={setup:function(){if(!e[f]&&this[k]){return false}var l=$(this);a=a.add(l);$.data(this,d,{w:l.width(),h:l.height()});if(a.length===1){g()}},teardown:function(){if(!e[f]&&this[k]){return false}var l=$(this);a=a.not(l);l.removeData(d);if(!a.length){clearTimeout(i)}},add:function(l){if(!e[f]&&this[k]){return false}var n;function m(s,o,p){var q=$(this),r=$.data(this,d);r.w=o!==c?o:q.width();r.h=p!==c?p:q.height();n.apply(this,arguments)}if($.isFunction(l)){n=l;return m}else{n=l.handler;l.handler=m}}};function g(){i=h[k](function(){a.each(function(){var n=$(this),m=n.width(),l=n.height(),o=$.data(this,d);if(m!==o.w||l!==o.h){n.trigger(j,[o.w=m,o.h=l])}});g()},e[b])}})(jQuery,this);

/********************************
*
*   If an element if visible
*
********************************/
function isScrolledIntoCompleteView(elem, val) {
    var docViewTop = jQuery(window).scrollTop();
    var docViewBottom = docViewTop + jQuery(window).height();

    var elemTop = jQuery(elem).offset().top;
    var elemBottom = elemTop + jQuery(elem).actual('height') + val;

    return ( elemTop >= docViewTop && elemBottom <= docViewBottom);
}

/********************************
*
*   Select
*
********************************/
function getSelValue(){
    jQuery('#pix_content_loaded select').each(function(){
	    jQuery(this).bind('change',function(){
	        var tx = jQuery('option:selected',this).text();
	        if ( !jQuery(this).parents('span').eq(0).find('.appended').length ) {
				jQuery(this).parents('span').eq(0).prepend('<span class="appended" />');
			}
			var elm = jQuery(this).siblings('.appended');
			jQuery(elm).text(tx);
	    }).triggerHandler('change');
	});
}

/********************************
*
*   Color pickers
*
********************************/
function init_farbtastic(){
	if(jQuery.isFunction(jQuery.fn.farbtastic)) {
		/*jQuery('.colorpicker').each(function(){
			jQuery(this).after('<div class="picker_close" />');
		})*/
		jQuery(document).on('click','.pix_color_picker .pix_button',function(){
			var t = jQuery(this);
			t.siblings('.colorpicker, i').fadeIn(150);

			t.siblings('i').on('click',function() {
				t.siblings('.colorpicker, i').fadeOut(150);
			});
	
			return false;
		});

		jQuery('.pix_color_picker').each(function() {
			var divPicker = jQuery(this).find('.colorpicker');
			var inputPicker = jQuery(this).find('input[type=text]');
			divPicker.farbtastic(inputPicker);
			jQuery.farbtastic(divPicker).setColor(jQuery(this).find('input[type=text]').val());
		});
	}
}

/********************************
*
*   Sliders
*
********************************/
function init_sliders() {
	if(jQuery.isFunction(jQuery.fn.slider)) {
		jQuery('.slider_div').each(function(){
			var t = jQuery(this);
			var value = jQuery('input',t).val();
			if(t.hasClass('milliseconds')){
				var mi = 0;
				var m = 50000;
				var s = 100;
			} else if(t.hasClass('milliseconds_2')){
				var mi = 0;
				var m = 5000;
				var s = 10;
			} else if(t.hasClass('opacity')){
				var mi = 0;
				var m = 1;
				var s = 0.05;
			} else if(t.hasClass('border')){
				var mi = 0;
				var m = 50;
				var s = 1;
			} else if(t.hasClass('em')){
				var mi = 0;
				var m = 8;
				var s = 0.001;
			} else if(t.hasClass('percent')){
				var mi = 0;
				var m = 100;
				var s = 1;
			} else if(t.hasClass('ratio')){
				var mi = 0;
				var m = 20;
				var s = 0.01;
			} else {
				var mi = 1;
				var m = 20;
				var s = 1;
			}
			jQuery('.slider_cursor',t).slider({
				range: 'min',
				value: value,
				min: mi,
				max: m,
				step: s,
				slide: function( event, ui ) {
					jQuery('input',t).val( ui.value );
				}
			});
			jQuery('a',t).mousedown(function(event){
				t.addClass('active');
			});
			jQuery(document).mouseup(function(){
				t.removeClass('active');
			});
			jQuery('input',t).keyup(function(){
				var v = jQuery('input',t).val();
				jQuery('.slider_cursor',t).slider({
					range: 'min',
					value: v,
					min: 0,
					max: m,
					step: s,
					slide: function( event, ui ) {
						jQuery('input',t).val( ui.value );
					}
				});
			});
			jQuery('.slider_cursor',t).each(function(){
				if ( jQuery('.ui-slider-range-min',this).length > 1 ) {
					jQuery('.ui-slider-range-min',this).not(':last').remove();
				}
			});
		});
	}
}

/********************************
*
*	Navsidebar menu
*
********************************/
function adminNav(){
	if(pagenow=='toplevel_page_shortcodelic_admin') {
		function pix_ajax_update() {
			var spinner = jQuery('#spinner2');
			jQuery('#pix_ap_header h2').append(spinner.animate({opacity:1},100));

			var shortcodelic_active_tab = localStorage.getItem("shortcodelic_active_tab"),
				shortcodelic_active_link = localStorage.getItem("shortcodelic_active_link");

			if(typeof shortcodelic_active_tab == 'undefined' || shortcodelic_active_tab == false || shortcodelic_active_tab == null) {
				shortcodelic_active_tab = 'general';
			}
			if(typeof shortcodelic_active_link == 'undefined' || shortcodelic_active_link == false || shortcodelic_active_link == null) {
				shortcodelic_active_link = 'register';
			}

			var url = jQuery('#pix_ap_body li[data-store='+shortcodelic_active_link+'] a').attr('href');

			jQuery.ajax({
				url: url,
				success: function(loadeddata){
					var height = jQuery('#pix_content_loaded').outerHeight();
					jQuery('#pix_content_loaded').outerHeight(height);

					jQuery('#pix_ap_body').find('li[data-store='+shortcodelic_active_tab+'], li[data-store='+shortcodelic_active_link+']').addClass('current');

					var html = jQuery("<div/>").append(loadeddata.replace(/<script(.|\s)*?\/script>/g, "")).find('#pix_content_loaded').html();
					jQuery('#pix_content_loaded').animate({opacity:0},0).html('<div class="cf">'+html+'</div>');
					var newH = jQuery('#pix_content_loaded > div').actual('outerHeight');
					jQuery('#pix_content_loaded').html(html);
						
					jQuery('#pix_content_loaded').animate({height:newH},800,'easeOutQuad',function(){
						spinner.animate({opacity:0},100,function(){
							jQuery('#spinner_wrap').prepend(spinner);
						});
						jQuery('#pix_content_loaded').css({height:'auto'});
						jQuery('#pix_content_loaded').animate({opacity:1},250,function(){
							floatingSaveButton();
						});
					});
					floatingSaveButton();
					saveOptions();
					getSelValue();
					init_sliders();
					init_farbtastic();
					infoDialog();
					changelogButton();
					sortElems();
					pagesCheckboxes();
					checkLicense();
					closeAlerts();
					if(jQuery('#shortcodelic_css_code').length){
						var editor = CodeMirror.fromTextArea(document.getElementById("shortcodelic_css_code"), {theme:'solarized light'});
					}
					jQuery('#pix_content_loaded textarea.codemirror:visible').not('.done').each(function(){
						var txtArea = jQuery(this).addClass('done'),
							editor = CodeMirror.fromTextArea(txtArea.get(0), {theme:'solarized light'}).setSize('100%', 180);
					});
				},
				error: function(){
					/*shortcodelic_active_tab = 'general_head';
					shortcodelic_active_link = 'admin_panel';*/
				}
			});
		}
	
		pix_ajax_update();

		jQuery('nav#pix_ap_main_nav > ul > li').not('.current').not('.documentation').each(function(){

			var li = jQuery(this);

				jQuery('> a, > ul', li).hover(function(){
					if ( !li.hasClass('current') ) {
						li.addClass('hover');
						jQuery('nav#pix_ap_main_nav li.current').removeClass('current').addClass('fake-current');
					}
				},function(){
					li.removeClass('hover');
					jQuery('nav#pix_ap_main_nav li.fake-current').removeClass('fake-current').addClass('current');
				});

			jQuery('> a', li).bind('click',function(){
				return false;
			});
		});

		jQuery('nav#pix_ap_main_nav > ul > li li').not('.current').each(function(){

			var li = jQuery(this);

			jQuery('> a', li).bind('click',function(){
				var t = jQuery(this);

				jQuery('nav#pix_ap_main_nav > ul li').removeClass('current').removeClass('fake-current');
				li.parents('li').eq(0).addClass('current');
				li.addClass('current');

				if ( jQuery('nav#pix_ap_main_nav > ul li.hover').length ) {
					var tab = jQuery('nav#pix_ap_main_nav > ul li.hover').attr('data-store');
					if (Modernizr.localstorage) {
						localStorage.setItem("shortcodelic_active_tab", tab);
					}
				}

				var link = li.attr('data-store');
				if (Modernizr.localstorage) {
					localStorage.setItem("shortcodelic_active_link", link);
				}
				pix_ajax_update();
				return false;
			});
		});

		/*Height of the ULs*/
		jQuery('nav#pix_ap_main_nav > ul > li ul').each(function(){
			var h = jQuery(this).actual('outerHeight'),
				wrapH = jQuery(this).parents('ul').eq(0).actual('outerHeight');

			if ( h < wrapH ) {
				jQuery(this).outerHeight(wrapH);
			}
		});

		jQuery(document).off('click','nav#pix_ap_main_nav > ul > li.documentation a');
		jQuery(document).on('click','nav#pix_ap_main_nav > ul > li.documentation a',function() {
			var href = jQuery(this).attr('href'),
				title = jQuery(this).text(),
				h = jQuery(window).height(),
				div = jQuery('<div />');
			if ( href.match(/\.(jpg|png|gif)/i) ) {
				var spinner = jQuery('#spinner2');
				jQuery('#pix_ap_header h2').append(spinner.animate({opacity:1},100));
	            jQuery('<img />').load( function(){
	            	//alert(jQuery(this).get(0).naturalWidth);
					spinner.animate({opacity:0},100,function(){
						jQuery('#spinner_wrap').prepend(spinner);
					});
					div.append(jQuery(this)).dialog({
						height: jQuery(this).get(0).naturalHeight,
						width: jQuery(this).get(0).naturalWidth,
						modal: false,
						dialogClass: 'wp-dialog pix-dialog pix-dialog-info pix-dialog-iframe',
						position: 'center',
						title: title,
						zIndex: 100,
						open: function(){
					        jQuery('body').addClass('overflowHidden');        
							jQuery('body').append('<div id="pix-modal-overlay" />');
						},
						close: function(){
							jQuery('#pix-modal-overlay').remove();
					        jQuery('body').removeClass('overflowHidden');     
					        div.remove();   
			    			jQuery(window).unbind('resize');  
						}
					});
					div.bind('resize',function() {
						div.dialog('option','position','center');
					}).triggerHandler('resize');
	            }).attr('src', href).each(function() {
	                if(this.complete) jQuery(this).load();
	            });
	        } else {
				div.append(jQuery("<iframe />").attr("src", href)).dialog({
					height: (h*0.8),
					width: 1082,
					modal: false,
					dialogClass: 'wp-dialog pix-dialog pix-dialog-info pix-dialog-iframe',
					position: 'center',
					title: title,
					zIndex: 100,
					open: function(){
				        jQuery('body').addClass('overflowHidden');        
						jQuery('body').append('<div id="pix-modal-overlay" />');
					},
					close: function(){
						jQuery('#pix-modal-overlay').remove();
				        jQuery('body').removeClass('overflowHidden');     
				        div.remove();   
		    			jQuery(window).unbind('resize');  
					}
				});
				jQuery(window).bind('resize',function() {
					h = jQuery(window).height();
					div.dialog( 'option', {'height':(h*0.8)} );
				});
				div.bind('resize',function() {
					div.dialog('option','position','center');
				}).triggerHandler('resize');
			}
			return false;
		});
	}
}

/********************************
*
*	Show option
*
********************************/
function floatingSaveButton(){
	jQuery(window).bind('scroll resize',function(){
		jQuery('#pix_content_loaded .pix-save-options.fake_button2').each(function(){
			var t = jQuery(this),
				l = jQuery(window).width()-(t.offset().left+t.outerWidth()),
				cont = jQuery('#pix_content_loaded'),
				lCont = cont.offset().left,
				wCont = cont.outerWidth();
			jQuery('#pix_content_loaded #gradient-save-button').css({
				left: lCont,
				width: wCont
			});
			if (isScrolledIntoCompleteView(this, 20))	{
				jQuery('#pix_content_loaded .pix-save-options').not('.fake_button').not('.fake_button2').removeClass('fixed').css({
					right: 20
				});
				jQuery('#pix_content_loaded #gradient-save-button').fadeOut(150);
			} else {
				jQuery('#pix_content_loaded .pix-save-options').not('.fake_button').not('.fake_button2').addClass('fixed').css({
					right: l
				});
				jQuery('#pix_content_loaded #gradient-save-button').fadeIn(150);
			}
		});
	}).triggerHandler('scroll');
}

/********************************
*
*	Save options
*
********************************/
function saveOptions(){
	jQuery(document).off('submit','form.ajax_form');
	jQuery(document).on('submit','form.ajax_form',function() {
		var spinner = jQuery('#spinner'),
			t = jQuery(this),
			data = t.serialize();

		t.find('button[type="submit"]').append(spinner).animate({paddingLeft:40},150,function(){spinner.animate({opacity:1},100);});

		if ( jQuery(this).find('input[name="compile_css"]').val() != 'compile_css' ) {


			jQuery.post(ajaxurl, data)
				.success(function(html) {
					spinner.delay(500).animate({opacity:0},100,function(){
						jQuery('#spinner_wrap').append(spinner);
						jQuery('form.ajax_form button[type="submit"] i').fadeIn(100).delay(500).fadeOut(100,function(){
							jQuery('button[type="submit"].pix-save-options',t).animate({paddingLeft:15},150);
						});
					});
				})
				.error(function() { console.log('no'); });

		} else {

			var cont = { action: 'css_shortcodelic_ajax', context: data };

			jQuery.post(ajaxurl, data).success(function(html) {
				jQuery.post(ajaxurl, cont).success(function(html){ 
					spinner.delay(500).animate({opacity:0},100,function(){
						jQuery('#spinner_wrap').append(spinner);
						jQuery('form.ajax_form button[type="submit"] i').fadeIn(100).delay(500).fadeOut(100,function(){
							jQuery('button[type="submit"].pix-save-options',t).animate({paddingLeft:15},150);
						});
					});
				});
			});

			var cont2 = { action: 'css_compile_ajax', context: data };

			jQuery.post(ajaxurl, data).success(function(html) {
				jQuery.post(ajaxurl, cont2);
			});
		
		}
		return false;
	});
}

/********************************
*
*	Download CSS
*
********************************/
function downloadCSS(){
	jQuery(document).off('click','a.pix-save-options.save-css');
	jQuery(document).on('click','a.pix-save-options.save-css',function(e) {
		e.preventDefault();
		var t = jQuery(this),
			form = t.parents('form').eq(0),
			data = { action: 'download_css' };
		jQuery.post(ajaxurl, data)
			.success(function(html) {
				if ( !form.find('.get_css_textarea').length)
					t.before('<textarea class="get_css_textarea" />');
				var getCss = form.find('.get_css_textarea');
				getCss
					.prop('readOnly',true)
					.val(html)
					.animate({
					height: 200,
					opacity: 1,
					marginBottom: 50
				},400,'easeOutQuad');
			})
			.error(function() { console.log('no'); });
	});
}

/********************************
*
*	Changelog button
*
********************************/
function changelogButton(){
	jQuery(document).off('click','.pix-iframe');
	jQuery(document).on('click','.pix-iframe',function() {
		var href = jQuery(this).attr('href'),
			title = jQuery(this).text(),
			h = jQuery(window).height(),
			div = jQuery('<div />');
		if ( href.match(/\.(jpg|png|gif)/i) ) {
			var spinner = jQuery('#spinner2');
			jQuery('#pix_ap_header h2').append(spinner.animate({opacity:1},100));
            jQuery('<img />').load( function(){
            	//alert(jQuery(this).get(0).naturalWidth);
				spinner.animate({opacity:0},100,function(){
					jQuery('#spinner_wrap').prepend(spinner);
				});
				div.append(jQuery(this)).dialog({
					height: jQuery(this).get(0).naturalHeight,
					width: jQuery(this).get(0).naturalWidth,
					modal: false,
					dialogClass: 'wp-dialog pix-dialog pix-dialog-info pix-dialog-iframe',
					position: 'center',
					title: title,
					zIndex: 100,
					open: function(){
				        jQuery('body').addClass('overflowHidden');        
						jQuery('body').append('<div id="pix-modal-overlay" />');
					},
					close: function(){
						jQuery('#pix-modal-overlay').remove();
				        jQuery('body').removeClass('overflowHidden');     
				        div.remove();   
		    			jQuery(window).unbind('resize');  
					}
				});
				div.bind('resize',function() {
					div.dialog('option','position','center');
				}).triggerHandler('resize');
            }).attr('src', href).each(function() {
                if(this.complete) jQuery(this).load();
            });
        } else {
			div.append(jQuery("<iframe />").attr("src", href)).dialog({
				height: (h*0.8),
				width: '80%',
				modal: false,
				dialogClass: 'wp-dialog pix-dialog pix-dialog-info pix-dialog-iframe',
				position: 'center',
				title: title,
				zIndex: 100,
				open: function(){
			        jQuery('body').addClass('overflowHidden');        
					jQuery('body').append('<div id="pix-modal-overlay" />');
				},
				close: function(){
					jQuery('#pix-modal-overlay').remove();
			        jQuery('body').removeClass('overflowHidden');     
			        div.remove();   
	    			jQuery(window).unbind('resize');  
				}
			});
			jQuery(window).bind('resize',function() {
				h = jQuery(window).height();
				div.dialog( 'option', {'height':(h*0.8)} );
			});
			div.bind('resize',function() {
				div.dialog('option','position','center');
			}).triggerHandler('resize');
		}
		return false;
	});
}


/********************************
*
*	Dialog boxes for info
*
********************************/
function infoDialog(){
	jQuery(document).off('click','[data-dialog]');
	jQuery(document).on('click','[data-dialog]',function() {
		var html = jQuery(this).attr('data-dialog'),
			w = (typeof jQuery(this).attr('data-width') != 'undefined') ? jQuery(this).attr('data-width') : 400,
			title = jQuery(this).text(),
			div = jQuery('<div />').html(html);
		div.dialog({
			height: 'auto',
			width: w,
			modal: false,
			dialogClass: 'wp-dialog pix-dialog pix-dialog-info',
			position: 'center',
			title: title,
			zIndex: 100,
			open: function(){
		        jQuery('body').addClass('overflowHidden');        
				jQuery('body').append('<div id="pix-modal-overlay" />');
			},
			close: function(){
				jQuery('#pix-modal-overlay').remove();
		        jQuery('body').removeClass('overflowHidden');        
		        div.remove();   
    			jQuery(window).unbind('resize');  
			}
		});
		jQuery(window).bind('resize',function() {
			h = jQuery(window).height(),
			div.dialog('option',{'position':'center'});
		});
		return false;
	});
}

/******************************************************
*
*	Sortable
*
******************************************************/
function sortElems(){
	jQuery('.pix-sorting').each(function(){ 
		var pixsorting = jQuery(this);

/* Add an element */
		jQuery(document).off('click','.add-element');
		jQuery(document).on('click','.add-element',function(){
			var clone = pixsorting.find('.clone').clone();
			clone.find('textarea').trigger('oninput');
			clone.fadeIn().removeClass('hidden').removeClass('clone');
			clone.find('[data-clone]').each(function(){
				var attrb = jQuery(this).attr('data-clone');
				jQuery(this).removeAttr('data-clone');
				attrb = attrb.replace(/\[i\]/g, '['+pixsorting.find('> div').not('.clone').length+']');
				jQuery(this).attr('name',attrb).trigger('oninput');
			});
			jQuery(this).before(clone);
			getSelValue();
			infoDialog();
			sortElems();
			return false;
		});

/* Remove an element */
		jQuery(document).off('click','.pix-sorting-elem .delete-element');
		jQuery(document).on('click','.pix-sorting-elem .delete-element',function(){
			var el = jQuery(this).parents('.pix-sorting-elem').eq(0);
			el.animate({opacity:0,height:0,minHeight:0,paddingTop:0,paddingBottom:0,margin:0},400, 'easeOutQuad', function(){ 
				jQuery(this).remove();
	            pixsorting.find('> div').not('.clone').each(function(){
	            	var ind = jQuery(this).index();
	            	jQuery('[name]',this).each(function(){
						var attrb = jQuery(this).attr('name');
						attrb = attrb.replace(/_\[(.*?)\]\[/g, '_['+ ( ind - 1 ) +'][');
						jQuery(this).attr('name',attrb);
					});
	            });
	        });
			return false;
		});

/* Edit an element */
		jQuery(document).off('click','.edit-element');
		jQuery(document).on('click','.edit-element',function(){
			var t = jQuery(this),
				par = t.parents('.pix-sorting-elem').eq(0),
				div = jQuery('.dialog-edit-sorting-elements').clone(),
				h = jQuery(window).height(),
				w = (typeof div.attr('data-width') != 'undefined') ? div.attr('data-width') : 400,
				title = div.attr('data-title');
			div.dialog({
				height: 'auto',
				width: w,
				modal: false,
				dialogClass: 'wp-dialog pix-dialog pix-dialog-info pix-dialog-input',
				title: title,
				zIndex: 100,
				open: function(){
					getSelValue();
			        jQuery('body').addClass('overflowHidden');        
					jQuery('body').append('<div id="pix-modal-overlay" />');
					jQuery(this).closest('.ui-dialog').find('.ui-button').last().addClass('ui-dialog-titlebar-edit pix_button');

					par.find('[data-name]').each(function(){
						if ( jQuery(this).attr('data-name')=='color' && jQuery(this).val()=='' ) {
							div.find('[data-name="'+jQuery(this).attr('data-name')+'"]').val('transparent');
						} else {
							div.find('[data-name="'+jQuery(this).attr('data-name')+'"] option[value="'+jQuery(this).val()+'"]').prop('selected',true).trigger('change');
							div.find('[data-name="'+jQuery(this).attr('data-name')+'"]').val(jQuery(this).val()).trigger('keyup');
							if ( jQuery(this).attr('data-name')=='icon' ) {
								div.find('.shortcodelic-icon-preview i').attr('class',jQuery(this).val());
							}							
						}
					});
				},
				buttons: {
					'': function() {
						jQuery(this).find('[data-name]').each(function(){
							par.find('[data-name="'+jQuery(this).attr('data-name')+'"]').val(jQuery(this).val()).prop('readOnly',false).trigger('oninput').prop('readOnly',true);
							par.find('[data-class="'+jQuery(this).attr('data-name')+'"]').attr('class',jQuery(this).val());
							par.find('[data-color="'+jQuery(this).attr('data-name')+'"]').css('background-color',jQuery(this).val());
						});
						jQuery(this).dialog('close');
					}
				},
				close: function(){
					jQuery('#pix-modal-overlay').remove();
					div.remove();
			        jQuery('body').removeClass('overflowHidden'); 
        			jQuery(window).unbind('resize');  
				}
			});
			jQuery(window).bind('resize',function() {
				div.dialog( 'option', {'height':'auto'} );
				h = jQuery(window).height();
				var divH = div.parents('.ui-dialog').eq(0).outerHeight();
				if (divH > (h*0.8)) {
					div.dialog( 'option', {'height':(h*0.8)} );
				}
			}).triggerHandler('resize');
			div.bind('resize',function() {
				div.dialog('option','position','center');
			}).triggerHandler('resize');
			return false;
		});
	});
}

/********************************
*
*	Pages checkboxes
*
********************************/
function pagesCheckboxes(){
	jQuery(document).off('click','.for_pages input[type="checkbox"]');
	jQuery(document).on('click','.for_pages input[type="checkbox"]',function() {
		if (jQuery(this).is(':checked')) {
			checked = true;
		} else {
			checked = false;
		}
		jQuery(this).parents('blockquote').eq(0).find('blockquote input[type="checkbox"]').prop('checked',checked);
	});
}

/********************************
*
*	Check license
*
********************************/
function checkLicense(){
	if ( typeof shortcodelic_check_license != 'undefined' && shortcodelic_check_license != false && shortcodelic_check_license != null) {
		jQuery('#check_license_message').html(shortcodelic_check_message).slideDown();
	}

	shortcodelic_check_license = undefined;
}

/********************************
*
*	Close alerts
*
********************************/
function closeAlerts(){
	jQuery(document).off('click', '.info_close');
	jQuery(document).on('click', '.info_close', function(){
		jQuery('input[type=hidden]',this).val('close');
		jQuery(this).parents('.outer_info').eq(0).slideUp(250,'easeOutQuad').animate({opacity:0},{queue:false,duration:250},'easeOutQuad');
		var data = jQuery(this).parents('.outer_info').eq(0).find('input').serialize();
		jQuery.post(ajaxurl, data);
	});
}

/********************************
*
*	Add buttons styles
*
********************************/
function addButtons(){
	jQuery(document).off('click', '.section_title');
	jQuery(document).on('click', '.section_title', function(){
		var t = jQuery(this).toggleClass('active'),
			next = t.next('.admin-section-toggle'),
			parent = t.parents('.pix_columns').eq(0);
		if ( t.hasClass('active') ) {
			next.slideDown(250,'easeOutQuad').animate({opacity:1},{queue:false,duration:250, easing:'easeOutQuad', complete:function(){
				jQuery(this).addClass('visible');
				jQuery('#pix_content_loaded textarea.codemirror:visible').not('.done').each(function(){
					var txtArea = jQuery(this).addClass('done'),
						editor = CodeMirror.fromTextArea(txtArea.get(0), {theme:'solarized light'}).setSize('100%', 180);
				});
			}});
		} else {
			next.slideUp(250,'easeOutQuad').animate({opacity:0},{queue:false,duration:250, easing:'easeOutQuad', complete:function(){
				jQuery(this).removeClass('visible');
			}});
		}
	});

	jQuery(document).off('click', '.section_title > .delete');
	jQuery(document).on('click', '.section_title > .delete', function(e){
		e.preventDefault();
		e.stopPropagation();
		var t = jQuery(this),
			div = jQuery('#delete_button').clone(),
			form = t.parents('form'),
			button = t.parents('.pix_columns').eq(0);
		div.dialog({
			height: 230,
			width: 300,
			modal: false,
			dialogClass: 'wp-dialog pix-dialog pix-dialog-info',
			position: 'center',
			zIndex: 100,
			open: function(){
		        jQuery('body').addClass('overflowHidden');        
				jQuery('body').append('<div id="pix-modal-overlay" />');
			},
			buttons: {
				'': function() {
					jQuery(this).dialog('close');
					button.slideUp(250,'easeOutQuad').animate({opacity:0},{queue:false, duration:250, easing:'easeOutQuad', complete:function(){
						button.remove();
						form.submit();
					}});
				}
			},
			close: function(){
				jQuery('#pix-modal-overlay').remove();
		        jQuery('body').removeClass('overflowHidden');     
		        div.remove();
    			jQuery(window).unbind('resize');  
			}
		});
		div.bind('resize',function() {
			div.dialog('option','position','center');
		}).triggerHandler('resize');
	});

	jQuery(document).off('click', '.add-button');
	jQuery(document).on('click', '.add-button', function(){
		var t = jQuery(this).toggleClass('active'),
			div = jQuery('#type_button').clone(),
			clone = t.parents('form').eq(0).find('.pix_columns.clone').clone();
		div.dialog({
			height: 250,
			width: 400,
			modal: false,
			dialogClass: 'wp-dialog pix-dialog pix-dialog-info pix-dialog-input',
			position: 'center',
			zIndex: 100,
			open: function(){
		        jQuery('body').addClass('overflowHidden');        
				jQuery('body').append('<div id="pix-modal-overlay" />');
			},
			buttons: {
				'': function() {
					var val = jQuery('input[type="text"]',div).val(),
						dialog = jQuery(this),
						data = {
						action: 'shortcodelic_sanitize',
						title: val
					};
					jQuery.post(ajaxurl, data)
						.success(function(html){
							dialog.dialog('close');
							t.before(clone.removeClass('clone').removeClass('hidden'));
							clone.find('[data-name]').each(function(){
								var attrb = jQuery(this).attr('data-name'),
									nwattrb = attrb.replace(/\[i\]/g, '['+html+']'),
									nwid = attrb.replace(/\[i\]\[(.*?)\]/g, html+'_$1');
								jQuery(this).removeAttr('data-name').attr('name',nwattrb).attr('id',nwid);
								jQuery('label[for="'+attrb+'"]').attr('for',nwid);
							});
							clone.find('textarea').each(function(){
								var txtArea = jQuery(this),
									valTArea = txtArea.val();
								valTArea = valTArea.replace(/\.\[i\]/g, '.'+html);
								txtArea.val(valTArea);
							});
							clone.find('.section_title').addClass('active').find('span').text(html);
							clone.find('.admin-section-toggle').slideDown(250,'easeOutQuad').animate({opacity:1},{queue:false,duration:250, easing:'easeOutQuad', complete:function(){
								jQuery(this).addClass('visible');
								clone.find('textarea').not('done').each(function(){
									var txtArea = jQuery(this).addClass('done'),
										editor = CodeMirror.fromTextArea(txtArea.get(0), {theme:'solarized light'}).setSize('100%', 180);
								});
							}});
							getSelValue();
							init_sliders();
							init_farbtastic();		
					}); 
				}
			},
			close: function(){
				jQuery('#pix-modal-overlay').remove();
		        jQuery('body').removeClass('overflowHidden');     
		        div.remove();   
    			jQuery(window).unbind('resize');  
			}
		});
		div.bind('resize',function() {
			setTimeout(function(){ div.dialog('option','position','center')},1);
		}).triggerHandler('resize');
	});
}

jQuery(function(){
	floatingSaveButton();
	saveOptions();
	downloadCSS();
	adminNav();
	jQuery('textarea').livequery(function(){
		jQuery(this).autosize();
	});
	getSelValue();
	init_sliders();
	init_farbtastic();
	infoDialog();
	changelogButton();
	sortElems();
	pagesCheckboxes();
	addButtons();
});
