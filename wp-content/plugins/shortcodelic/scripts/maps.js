jQuery(function(){
	var it = 0;
	var shortcodelicMaps = function(){
		jQuery('.pix_maps:visible, .entry-header.pix_maps').not('.init').each(function(){
			var t = jQuery(this).addClass('init'),
				id = t.attr('id'),
				opts = t.data('opts');
			console.log(opts);
			var w = parseFloat(opts.width)+'%',
				h = parseFloat(opts.height),
				coords = opts.coords.replace(/n/g, ''),
				maptype = opts.maptype != 'HIDE' ? true : false,
				typepos = opts.maptype,
				pancontrol = opts.pancontrol != 'HIDE' ? true : false,
				panpos = opts.pancontrol,
				zoomcontrol = opts.zoomcontrol != 'HIDE' ? true : false,
				zoompos = opts.zoomcontrol,
				scalecontrol = opts.scalecontrol != 'HIDE' ? true : false,
				swcontrol = opts.swcontrol != 'HIDE' ? true : false,
				swpos = opts.swcontrol,
				markers = opts.markers,
				styles = typeof eval('styles_'+opts.id) !== 'undefined' ? eval('styles_'+opts.id) : false,
				map;
			it = it+1;

			coords = coords.split(",");

			jQuery(window).bind('resize', function(){
				var wid = t.width(),
				hei = wid*(h*0.01);
				t.css({
					height: hei,
					width: w
				});
			}).triggerHandler('resize');

			if ( opts.type == 'streetview' ) {
				map = GMaps.createPanorama({
					el: id,
					lat: coords[0],
					lng: coords[1],
					scrollwheel: false,
					panControlOptions: {
						position: google.maps.ControlPosition[panpos]
					},
					zoomControlOptions: {
						position: google.maps.ControlPosition[zoompos]
					},
					pov : {
						heading : parseFloat(opts.heading),
						pitch : parseFloat(opts.pitch),
						zoom : Math.floor(parseFloat(opts.zoom)/3)-1
					}
				});
			} else {
				map = new GMaps({
					div: id,
					lat: coords[0],
					lng: coords[1],
					scrollwheel: false,
					zoom: parseFloat(opts.zoom),
					mapTypeId: opts.type,
					mapTypeControl: maptype,
					mapTypeControlOptions: {
						position: google.maps.ControlPosition[typepos]
					},
					panControl: pancontrol,
					panControlOptions: {
						position: google.maps.ControlPosition[panpos]
					},
					zoomControl: zoomcontrol,
					zoomControlOptions: {
						position: google.maps.ControlPosition[zoompos]
					},
					scaleControl: scalecontrol,
					streetViewControl: swcontrol,
					streetViewControlOptions: {
						position: google.maps.ControlPosition[swpos]
					},
					styles: styles
				});
				map.setCenter(coords[0], coords[1]);

				jQuery(window).bind('scroll',function(){
					if ( markers !== '' && typeof markers == 'object' && isScrolledIntoView(t) && t.data('scrolled') !== true ) {
						t.data('scrolled', true);
						jQuery.each(markers, function(i, marker) {
							if ( marker !== '' && typeof marker == 'object' ) {
								var image,
									delay = marker.delay !== '' && typeof marker.delay !== 'undefined' ? marker.delay : 1,
									caption = marker.caption !== '' && typeof marker.caption !== 'undefined' ?  { content: marker.caption.replace(/\[apostrophe;\]/g, '\'').replace(/\[quotes;\]/g, '\"') } : false,
									animation = marker.fx !== '' && typeof marker.fx !== 'undefined' ? google.maps.Animation.DROP : '',
									markercoords = marker.coords !== '' && typeof marker.coords !== 'undefined' ? marker.coords.replace(/n/g, '') : opts.coords.replace(/n/g, '');

								markercoords = markercoords.split(",");

								if ( marker.bg !== '' && typeof marker.bg !== 'undefined' ) {
									var newImg = new Image();
									newImg.onload = function() {
										var NIheight = newImg.height,
											NIwidth = newImg.width;

										if ( marker.hdpi !== '' && typeof marker.hdpi !== 'undefined' ) {
											NIheight = NIheight/2;
											NIwidth = NIwidth/2;
										}

										image = new google.maps.MarkerImage(marker.bg, null, null, null, new google.maps.Size(NIwidth,NIheight));

										map.addMarker({
											lat: markercoords[0],
											lng: markercoords[1],
											icon: image,
											animation: animation,
											infoWindow: caption
										});
									};
									newImg.src = marker.bg;
								} else {
									var set = setTimeout(function(){
										map.addMarker({
											lat: markercoords[0],
											lng: markercoords[1],
											animation: animation,
											infoWindow: caption
										});
									}, delay);
								}
							}
						});
					}
				}).triggerHandler('scroll');

				if ( opts.start !== '' && typeof opts.start !== 'undefined' && opts.end !== '' && typeof opts.end !== 'undefined' ) {
					var start = opts.start.replace(/n/g, ''),
						end = opts.end.replace(/n/g, '');
					map.drawRoute({
						origin: start,
						destination: end,
						travelMode: opts.travelmode,
						strokeColor: opts.color,
						strokeOpacity: opts.opacity,
						strokeWeight: opts.strokeweight
					});
				}
			}
			jQuery(window).bind('shortcodelic_ev', function(){
				map.refresh();
			});

		});
	};
	shortcodelicMaps();
	jQuery(window).bind('shortcodelic_ev',function(){
		shortcodelicMaps();
	});
});