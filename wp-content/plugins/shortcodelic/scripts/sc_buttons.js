jQuery.noConflict();

/**************
	BUTTONS
**************/

function shortcodelicTinyMCEinit() {
	var DOM = tinymce.DOM;

	tinymce.init;

	tinymce.create('tinymce.plugins.ShortCodelic', {
		mceTout : 0,
		init : function(ed, url) {
			var t = this,
				activeID = tinyMCE.activeEditor.id;


/* SCHORTCODELIC */
			if ( activeID != 'shortcodelicTMCE' && activeID != 'tooltipTMCE' && activeID != 'boxTMCE' ) {
				ed.addButton('shortcodelic_sc', {
					title : 'Advanced shortcodelic generator',
					icon: 'shortcodelic_sc',
					onclick : function() {
						openShortcodeSelector(activeID);
					}
				});
			}

			ed.addButton('shortcodelic_fonticon', {
				title : 'Shortcodelic font icons',
				icon: 'shortcodelic_fonticon',
				onclick : function() {
					openFontSelector(activeID);
				}
			});

			if ( activeID != 'tooltipTMCE' ) {
				ed.addButton('shortcodelic_tooltip', {
					title : 'Shortcodelic tooltips',
					icon: 'shortcodelic_tooltip',
					stateSelector: 'a',
					onclick : function() {
						openTooltipGen(activeID);
					},
					onPostRender: function() {
						var ctrl = this;
						ed.on('NodeChange', function(e) {
							ctrl.disabled( ed.selection.isCollapsed() && e.element.nodeName !== 'A' && e.element.nodeName !== 'SPAN' && typeof jQuery(e.element).data('tooltip')=='undefined' );
							ctrl.active( e.element.nodeName === 'A' && e.element.nodeName === 'SPAN' && typeof jQuery(e.element).data('tooltip')!='undefined' && ! e.element.name );
						});
					}
				});

				ed.addButton('shortcodelic_removetooltip', {
					title : 'Remove tooltips',
					icon: 'shortcodelic_removetooltip',
					onclick : function() {
						removeTooltip(activeID);
					},
					onPostRender: function() {
						var ctrl = this;
						ed.on('NodeChange', function(e) {
							ctrl.disabled( ed.selection.isCollapsed() && e.element.nodeName !== 'A' && e.element.nodeName !== 'SPAN' && typeof jQuery(e.element).data('tooltip')=='undefined' );
							ctrl.active( e.element.nodeName === 'A' && e.element.nodeName === 'SPAN' && typeof jQuery(e.element).data('tooltip')!='undefined' && ! e.element.name );
						});
					}
				});

			}

			if ( activeID != 'shortcodelicTMCE' && activeID != 'tooltipTMCE' ) {
				ed.addButton('shortcodelic_progress', {
					title : 'Shortcodelic progress indicators',
					icon: 'shortcodelic_progress',
					onclick : function() {
						openProgressGen(activeID);
					}
				});
			}

			ed.addButton('shortcodelic_buttons', {
				title : 'Shortcodelic buttons',
				icon: 'shortcodelic_buttons',
				onclick : function() {
					openButtonsGen(activeID);
				},
				onPostRender: function() {
					var ctrl = this;
					ed.on('NodeChange', function(e) {
						ctrl.disabled( ed.selection.isCollapsed() && e.element.nodeName !== 'A' && e.element.nodeName !== 'SPAN' );
						ctrl.active( e.element.nodeName === 'A' && e.element.nodeName === 'SPAN' && ! e.element.name );
					});
				}
			});

			ed.addButton('shortcodelic_removebutton', {
				title : 'Remove buttons',
				icon: 'shortcodelic_removebutton',
				onclick : function() {
					removeButtons(activeID);
				},
				onPostRender: function() {
					var ctrl = this;
					ed.on('NodeChange', function(e) {
						ctrl.disabled( ed.selection.isCollapsed() && e.element.nodeName !== 'A' && e.element.nodeName !== 'SPAN' );
						ctrl.active( e.element.nodeName === 'A' && e.element.nodeName === 'SPAN' && ! e.element.name );
					});
				}
			});

			if ( activeID != 'shortcodelicTMCE' && activeID != 'tooltipTMCE' && activeID != 'boxTMCE' ) {
				ed.addButton('shortcodelic_box', {
					title : 'Add a message box',
					icon: 'shortcodelic_box',
					onclick : function() {
						openBoxGen(activeID);
					}
				});
			}

			ed.addButton('shortcodelic_removespan', {
				title : 'Remove spans',
				icon: 'shortcodelic_removespan',
				onclick : function() {
					removeSpans(activeID);
				},
				onPostRender: function() {
					var ctrl = this,
						disable,
						enable;
					ed.on('NodeChange', function(e) {
						disable = ( e.element.nodeName !== 'SPAN' || typeof jQuery(e.element).attr('class') != 'undefined' || typeof jQuery(e.element).attr('id') != 'undefined' || typeof jQuery(e.element).attr('style') == 'undefined' );
						enable = ( e.element.nodeName === 'SPAN' && typeof jQuery(e.element).attr('class') == 'undefined' && typeof jQuery(e.element).attr('id') == 'undefined' && typeof jQuery(e.element).attr('style') != 'undefined' );
						ctrl.disabled( disable );
						ctrl.active( enable );
					});
				}
			});


			if (activeID == 'shortcodelicTMCE' || activeID == 'boxTMCE' || activeID == 'tooltipTMCE') {
				var editor = tinymce.activeEditor;
				clearTimeout(set);
				var set = setTimeout(function(){
					var h = (jQuery('.shortcodelic-dialog:visible').height() - (jQuery('.shortcodelic-dialog:visible .mce-toolbar-grp').height() + jQuery('.shortcodelic-dialog:visible .wp-editor-tabs').height() + 150)),
						h2 = (jQuery('.shortcodelic-dialog').height() - (jQuery('#qt_shortcodelicTMCE_toolbar').height() + jQuery('#wp-shortcodelicTMCE-editor-tools .wp-editor-tabs').height()));

					editor.theme.resizeTo('auto', (h));
					jQuery('#wp-shortcodelicTMCE-editor-container textarea').css({height:(h2-20)});

					jQuery(window).bind('resize',function(){
						editor = tinymce.activeEditor;
						h = (jQuery('.shortcodelic-dialog:visible').height() - (jQuery('.shortcodelic-dialog:visible .mce-toolbar-grp').height() + jQuery('.shortcodelic-dialog:visible .wp-editor-tabs').height() + 150));
						h2 = (jQuery('.shortcodelic_editelement_panel:visible').height() - (jQuery('#qt_shortcodelicTMCE_toolbar').height() + jQuery('#wp-shortcodelicTMCE-editor-tools .wp-editor-tabs').height()));
						editor.theme.resizeTo('auto', (h-42));
						jQuery('#wp-shortcodelicTMCE-editor-container textarea').css({height:(h2-20)});
					});
				},10);
			}

		}

	});

	tinymce.PluginManager.add('shortcodelic', tinymce.plugins.ShortCodelic);
}
(function() { shortcodelicTinyMCEinit(); })();
