<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rowberts_beautiful');

/** MySQL database username */
define('DB_USER', 'rowberts_staging');

/** MySQL database password */
define('DB_PASSWORD', 'Ma&7HZXuxR');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 */
define('AUTH_KEY',         '|rfu`!*@to>M~=~XHd+tCOMke {v<KvB|H3#T-4@4qlPMC6$p._R%l1]@~N|t~T0');
define('SECURE_AUTH_KEY',  'u-YK-!6ycDsd2<>SA98Gh/>Da}T^T`#*IkWup;)X|h1k]uHd>2)zgeJ_FfY>+be5');
define('LOGGED_IN_KEY',    ')8g24*W^>i+yz)R.Zp^x{3[k/{bFZLBmJ?fpA*jD-h~?Ly[:YsX!4+Q24vW=-]B5');
define('NONCE_KEY',        't4p&r2:p;RIN3;_EqRw%ci<@@.0U]/HW>qQC`c$&qrBS>e:(-&kF6o*xO2n0k~vd');
define('AUTH_SALT',        'L9&J;m+~f@+#gM+XsG!`k~G|tQe<~$EUrA[(p;17x=AHpMH|!dZ]m`TefQv_lr`-');
define('SECURE_AUTH_SALT', '%9p!BY1aG?4!?X=cUI{a/t7~BYx$.-.cY!Al__Usit*+Mcc%:-z8,!n9<iD7lfpk');
define('LOGGED_IN_SALT',   'ku-_#L,N.ve$^/V0T+9o(Qn;!<s t}o|]h9mIi++c4C z$;^_6=:lc7g9LJ}eksC');
define('NONCE_SALT',       'a2a<F3g`Z{+$t(YtonbdQc)/(xbqQUdl<aC/L#v{c?hjAzdQyybK]4ily/Z&bOsd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/*Enable Auto Updates for WP Core (Major), Themes & Plugins by CER */
define('WP_AUTO_UPDATE_CORE', true);
add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );
