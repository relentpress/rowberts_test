/************************************************************
* InfiniteWP Admin panel - WordFence Plugin									*
* Copyright (c) 2014 Revmakx								*
* www.revmakx.com											*
*															*
************************************************************/
function showWordFenceOptions() {
	var activeSites =$('.website_cont.active').length;
	if(activeSites>0){
		$('.load_wordfence_prev_scan').removeClass('disabled');
		$('.load_wordfence_scan').removeClass('disabled');
	}else{
		$('.load_wordfence_prev_scan').addClass('disabled');
		$('.load_wordfence_scan').addClass('disabled');
	}	
}

function formArrayLoadwordFenceResult(data)
{
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="loadwordFenceResult";	
}

function formArrayScanWordFence(data)
{
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="ScanWordFence";

}

function formArrayScanCheckWordFence(data)
{
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="ScanCheckWordFence";	
}

function wordfenceLoadScan(data) {
	var content = data.data.wordFenceLoad;
	var contentData = classifyResult(content);
	content = loadWordFenceContentHTML(contentData);
	$("#pageContent").find('#wordfence_content').append(content).show();
	showWordFenceOptions();
}

function wordfenceScan(data) {
	showWordFenceOptions();
}

function ScanCheckWordFence(data) {
	showWordFenceOptions();
}

function wordfencePluginInitialization(data) {
	$("#historyQueue").show();
	var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['pluginInintializationReload']='Wordfence';
	doCall(ajaxCallPath,tempArray);
}