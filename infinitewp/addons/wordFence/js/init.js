/************************************************************
* InfiniteWP Admin panel - WordFence Plugin									*
* Copyright (c) 2014 Revmakx								*
* www.revmakx.com											*
*															*
************************************************************/
/*
 * Document On loads
 */
$(function(){
	/*
	 * Load previous scaned result
	 */
	$(".load_wordfence_prev_scan").live('click',function() {
		var siteIDs = new Array();
		$("#pageContent").find('#wordfence_content').html('').hide();
		$('.website_cont.active').each(function(){
			siteIDs.push($(this).attr('sid'));
		});

		var tempArray={};
		var action = 'loadResult';
		$(".load_wordfence_prev_scan").addClass('disabled');

		tempArray['action']='wordFenceLoad';
		tempArray['args']={};
		tempArray['args']['siteIDs']=siteIDs;
		tempArray['args']['params']={};
		tempArray['args']['params']['action'] = action;
		tempArray['requiredData'] = {};
		tempArray['requiredData']['wordFenceLoad'] = 1;
		doCall(ajaxCallPath,tempArray,'formArrayLoadwordFenceResult');
		
	});
	
	$(".load_wordfence_scan").live('click',function() {
		var siteIDs = new Array();
		$("#pageContent").find('#wordfence_content').html('').hide();
		$('.website_cont.active').each(function(){
			siteIDs.push($(this).attr('sid'));
		});

		var tempArray={};
		var action = 'loadScan';
		$(".load_wordfence_scan").addClass('disabled');

		tempArray['action']='wordFenceScan';
		tempArray['args']={};
		tempArray['args']['siteIDs']=siteIDs;
		tempArray['args']['params']={};
		tempArray['args']['params']['action'] = action;
		tempArray['requiredData'] = {};
		tempArray['requiredData']['wordFenceScan'] = 1;
		doCall(ajaxCallPath,tempArray,'formArrayScanWordFence');
		
	});
	
	$("#wordfenceResult").live('click',function() {
		var actionID = $(this).attr('aid');
		var tempArray={};
		tempArray['requiredData'] = {};
		tempArray['requiredData']['wordFenceGetSiteID'] = {};
		tempArray['requiredData']['wordFenceGetSiteID']['actionId'] = actionID;
		doCall(ajaxCallPath,tempArray,'loadWordFenceResult');
	});
	
	$(".adminWordfencePopout").live('click',function(e) {
		if($(this).attr('clicked')!=1)
		{
			loadWordfenceAdminPopout(this,$(this).attr('sid'));
			$(this).attr('clicked','0');
		}
		
		e.stopImmediatePropagation();

	});

	$('.website_cont.hidden_wf').live('hover',function(e){
		if($(this).hasClass('disabled'))
		{
			$(this).removeClass('disabled');
			$(this).find('.tips').show();
		}
		else if($(this).find('.tips').length)
		{
			$(this).addClass('disabled');
			$(this).find('.tips').hide();
		}
		
	});

	$('.website_cont.hidden_wf .tips').live('click',function(){
		if(typeof $(this).attr('code') !== 'undefined' && $(this).attr('code') !== false){
		var action = $(this).attr('code') == '1'?'activate':'install' ; 
		}else{
			return false;
		}
		var siteID = $(this).closest('.website_cont.hidden_wf').attr('sid');
		if(action == 'install'){
			var tempArray={};
			tempArray['requiredData']={};
			var valArray={};
			valArray['type']='plugins';
			valArray['siteID']=siteID;
			valArray['plugin_slug']='wordfence';
			tempArray['requiredData']['installNotInstalledPlugin']=valArray;
			doCall(ajaxCallPath,tempArray,"installWordFence","json","none");
		}else if(action == 'activate'){

			var changeArray={};
			var dID, type, action, name, valArray;
			dID="wordfence/wordfence.php";
			type = 'plugins';
			action="activate";
			name="WordFence";
			changeArray[siteID]={};
			changeArray[siteID][type]={};
			valArray={};
			valArray['name']=name;
			valArray['path']=dID;
			valArray['action']=action;
			changeArray[siteID][type][0]=valArray;

			var tempArray={};
			tempArray['args']={};
			tempArray['args']['params']={};
			tempArray['action']='managePlugins';
			tempArray['args']['params'] = changeArray;
			doHistoryCall(ajaxCallPath,tempArray,'wordfencePluginInitialization');
		}
		$(this).closest('.website_cont.hidden_wf').removeClass('disabled');
		$(this).remove();
	});
        
        $('#wordfence_installation_bulk').live('click',function(){
		var activationSiteIDs = [],installationSiteIDs = [],isActing=1;
		$('.website_cont.hidden_wf .tips').each(function(){
			var siteID = $(this).closest('.website_cont.hidden_wf').attr('sid');
			if(typeof $(this).attr('code') !== 'undefined' && $(this).attr('code') !== false){
			if($(this).attr('code') == '1'){
				activationSiteIDs.push(siteID);
				}else if($(this).attr('code') == '0'){
				installationSiteIDs.push(siteID);
			}
			}else{
				isActing = 0;
				return false;
			}
		});
		// activation bulk starts
		if(activationSiteIDs.length>0){
			var changeArray={};
			var dID, type, action, name, valArray;
			dID="wordfence/wordfence.php";
			type = 'plugins';
			action="activate";
			name="WordFence";
			$.each(activationSiteIDs,function(index,siteID){
				changeArray[siteID]={};
				changeArray[siteID][type]={};
				valArray={};
				valArray['name']=name;
				valArray['path']=dID;
				valArray['action']=action;
				changeArray[siteID][type][0]=valArray;
			});

			var tempArray={};
			tempArray['args']={};
			tempArray['args']['params']={};
			tempArray['action']='managePlugins';
			tempArray['args']['params'] = changeArray;
			doHistoryCall(ajaxCallPath,tempArray,'wordfencePluginInitialization');
		}
		// activation bulk ends

		// installation bulk starts
		if(installationSiteIDs.length>0){
			
			var tempArray={};
			tempArray['requiredData']={};
			var valArray={};
			valArray['type']='plugins';
			valArray['siteID']=installationSiteIDs;
			valArray['plugin_slug']='wordfence';
			tempArray['requiredData']['installNotInstalledPlugin']=valArray;
			doCall(ajaxCallPath,tempArray,"installWordFenceMultiSites","json","none");
		}
		// installation bulk ends
		if(isActing){	
		$('.website_cont.disabled .tips').remove();
		$('.website_cont.disabled').removeClass('disabled');
		}
	});

	
});