/************************************************************
* InfiniteWP Admin panel - WordFence Plugin									*
* Copyright (c) 2014 Revmakx								*
* www.revmakx.com											*
*															*
************************************************************/
/*
 * Load Site selector
 */
function loadWordFenceMainPage() {
	var content='<div class="steps_hdr"><span id="processType">SELECT WEBSITE TO MANAGE</span> <span class="itemUpper">WordFence</span><div id="wordfence_installation_bulk" class="float-right"><a class="siteSelectorSelect">Install & activate WordFence plugin in all sites</a></div></div><div class="siteSelectorContainer">'+siteSelectorVar+'</div><div class="btn_action float-right load_wordfence_prev_scan disabled" ><a class="rep_sprite" id="LoadWordfence">Load Previous Scan Results</a></div><div class="btn_action float-right load_wordfence_scan disabled" ><a class="rep_sprite" id="scanWordfence">Scan Now</a></div><div id="wordfence_content" style="display:none;clear:both;"></div>';
	$("#pageContent").html(content);
	$(".website_cont").addClass('hidden_wf disabled');
	currentPage="wordFence";
	getRecentPluginsStatusAndCheck();
	var tempArray = {};
	tempArray['requiredData'] = {};
	tempArray['requiredData']['getRecentPluginsStatus'] = 1;
	doCall(ajaxCallPath,tempArray,'getRecentPluginsStatusAndCheck');
}

function classifyResult(data) {
	var siteIDs   = data['siteIDs'];
	var classified = {};
	$.each(siteIDs,function(index,siteID){
		var siteData = [];
		var siteURL = data['siteURLs'][index];
		if (typeof data[siteID].issuesLists != 'undefined') {
			siteData = data[siteID].issuesLists;
		} else if(typeof data[siteID].scan != 'undefined') {
			siteData['scan'] = data[siteID].scan;
		} else if(typeof data[siteID].warning != 'undefined') {
			siteData['warning'] = data[siteID].warning;
		}
		classified[siteID] = {'issues':[],'scan':[],'warning':[]};
		classified[siteID]['issueCount'] = classified[siteID]['scanCount'] = classified[siteID]['warningCount'] = 0;

		classified[siteID]['siteURL'] = siteURL;
		if (typeof siteData['new'] != 'undefined') {
			$.each(siteData['new'],function(resIndex,resData){
				var issues = {'severity':resData.severity, 'shortMsg':resData.shortMsg, 'timeAgo':resData.timeAgo}; 
				classified[siteID]['issues'].push(issues);
				classified[siteID]['issueCount']++;
			});
		}else if (typeof siteData['scan'] != 'undefined') {
			classified[siteID]['scan'].push('A scan is already running. Use the \'Scan Now\' button, if you would like to terminate the running scan and initiate new scan.');
			classified[siteID]['scanCount']++;
		}else if (typeof siteData['warning'] != 'undefined') {
			classified[siteID]['warning'].push(siteData['warning']);
			classified[siteID]['warningCount']++;
		}
	});
	return classified;
}

function loadWordFenceContentHTML(contentData) {
	var HTMLData='<div class="actionContent result_block siteSearch" id=""><div class="th rep_sprite"><div class="type_filter"> <input name="filter" type="text" class="input_type_filter searchSiteBL" value="type to filter sites" /> </div> </div><div class="no_match hiddenCont" style="display:none">Bummer, there are no scan result that match.<br>Try typing fewer characters.</div><div class="rows_cont">';
	//For display the issue
	$.each(contentData,function(siteID,siteData){
		var issueResult = '';
		var issueCount = siteData.issueCount;
		if(siteData.scanCount==0 && siteData.warningCount==0) {
			$.each(siteData.issues,function(issueId,issueData){
				issueClass = (issueData.severity == "1")?"error":"warning";
				issueResult = issueResult+"<div class='row_updatee'><div class='row_updatee_ind'><div class='icon_severity "+issueClass+"'></div><div class='items_cont float-left'><div class='item'>"+issueData.shortMsg+"</div><div class='action float-right' style='padding:10px;'>"+issueData.timeAgo+" ago</div></div><div class='clear-both'></div></div></div>";
			});
			if(issueCount!=0) {
			HTMLData = HTMLData+"<div class='ind_row_cont'><div class='row_summary' sid='"+siteID+"'><div class='row_arrow'></div><div class='count_cont float-left'>"+issueCount+"</div><div class='row_name searchable'>"+siteData.siteURL+"</div><div class='row_action float-right'><a class='adminWordfencePopout' sid='"+siteID+"'><span class='statusSpan'>View details</span></a></div><div class='clear-both'></div></div><div class='row_detailed' sid='"+siteID+"' style='display:none;clear:both;'><div class='rh'><div class='row_arrow'></div><div class='count_cont float-left'>"+issueCount+"</div><div class='row_name'>"+siteData.siteURL+"</div><div class='row_action float-right'><a class='adminWordfencePopout' sid='"+siteID+"'><span class='statusSpan'>View details</span></a></div><div class='clear-both'></div></div><div class='rd'>"+issueResult+"</div></div></div>";
			} else {
				HTMLData = HTMLData+"<div class='ind_row_cont'><div class='row_no_detail' sid='"+siteID+"'><div class='row_name searchable'>"+siteData.siteURL+"</div><div class='item_result success'>No issues found</div><div class='clear-both'></div></div></div>";
			}
		}
	});
	//For display the exist scan running
	$.each(contentData,function(siteID,siteData){
		var scanResult = '';
		var scanCount = siteData.scanCount;
		if(scanCount!=0) {
			HTMLData = HTMLData+"<div class='ind_row_cont'><div class='row_no_detail' sid='"+siteID+"'><div class='row_name searchable'>"+siteData.siteURL+"</div><div class='item_result'>Scan under progress...</div><div class='clear-both'></div></div></div>";
		}
	});
	//For display the Errors or Warning
	$.each(contentData,function(siteID,siteData){
		var warningResult = '';
		var warningCount = siteData.warningCount;
		if(warningCount!=0) {
			HTMLData = HTMLData+"<div class='ind_row_cont'><div class='row_no_detail' sid='"+siteID+"'><div class='row_name searchable'>"+siteData.siteURL+"</div><div class='item_result error'>WordFence not found</div><div class='clear-both'></div></div></div>";
		}
	});
	
	
	HTMLData = HTMLData+'</div></div>';
	return HTMLData;
}

function loadWordfenceAdminPopout(object,sid) {

	where='&where=admin';
	var_0='var_0=page__IWPVAR__Wordfence';
	
	var processLink=ajaxCallPath+'?action=loadSite&siteID='+sid+where+'&'+var_0;
	$(object).attr('href',processLink);
	$(object).attr('target','_blank');
	$(object).attr('clicked','1');
	
	resetBottomToolbar();
	
}

function loadWordFenceResult(data) {
var siteIds = data.data.wordFenceGetSiteID;
loadWordFenceMainPage();
	$.each(siteIds,function(key,siteID){
		$(".website_cont[sid='"+siteID+"']").each(function(){
			$(this).addClass('active');
		});
	});
showWordFenceOptions();
$( ".load_wordfence_prev_scan" ).trigger( "click" );
}



function installWordFence(data){
	var data = data.data.installNotInstalledPlugin;
	var dLink = data[0].download_link;
	var tempArray={};
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['args']['siteIDs']={};
	var params={}; 
	tempArray['action'] = 'installPlugins';
	params['plugins']={};
	tempArray['args']['siteIDs'][0] = data[1];
	params['plugins']['0']=dLink;
	params['activate'] = true;
	tempArray['args']['params'] = params;
	doHistoryCall(ajaxCallPath,tempArray,'wordfencePluginInitialization');
}

function installWordFenceMultiSites(data){
	var data = data.data.installNotInstalledPlugin;
	var dLink = data[0].download_link;
	var tempArray={};
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['args']['siteIDs']=data[1];
	var params={}; 
	tempArray['action'] = 'installPlugins';
	params['plugins']={};
	params['plugins']['0']=dLink;
	params['activate'] = true;
	tempArray['args']['params'] = params;
	doHistoryCall(ajaxCallPath,tempArray,'wordfencePluginInitialization');
}