<?php
/************************************************************
* InfiniteWP Admin panel - WordFence Plugin					*
* Copyright (c) 2014 Revmakx								*
* www.revmakx.com											*
*															*
************************************************************/
class manageClientsWordFence{
	 
	/*
	 * Kick start the Scan on WordFence
	 */
	public static function wordFenceScanProcessor($siteIDs, $params){
		$type = "wordFence";
		$action = "scan";
		$requestAction = "wordfence_scan";
		
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => 'wordFenceScan', 'detailedAction' => 'scanInitiated');
		$events=1;
		foreach ($siteIDs as $siteID) {
			$siteData = getSiteData(intval($siteID));
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['directExecute'] 	= false;
			$PRP['events'] 			= $events;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
			prepareRequestAndAddHistory($PRP);
		}
	}
	
	/*
	 * Response for Wordfence Scan start from client side
	 */
	public static function wordFenceScanResponseProcessor($historyID, $responseData){
		responseDirectErrorHandler($historyID, $responseData);
		
		$response = array();
		if(isset($responseData['success'])){
			$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
			$actionID = $historyData['actionID'];
			$siteID = $historyData['siteID'];
			$response['_'.$siteID] = $responseData['success'];
			
			DB::insert("?:temp_storage", array('type' => 'wordFence', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
			DB::update("?:history_additional_data", "status='success'", "historyID=".$historyID."");
			return;
		}
	}
	
	/*
	 * Check the Scan is running or not in Wordfence
	 */
	public static function wordFenceScanCheckProcessor($siteIDs, $params){
		$type = "wordFence";
		$action = "scanCheck";
		$requestAction = "wordfence_scan_check";
		
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => 'wordFenceScanCheck', 'detailedAction' => 'scanCheck');
		$events=1;
		foreach ($siteIDs as $siteID) {
			$siteData = getSiteData(intval($siteID));
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['directExecute'] 	= false;
			$PRP['events'] 			= $events;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
			$PRP['timeScheduled'] = time()+600;
			prepareRequestAndAddHistory($PRP);
		}
	}
	
	/*
	 * Response for the Scan check 
	 */
	public static function wordFenceScanCheckResponseProcessor($historyID, $responseData){
		responseDirectErrorHandler($historyID, $responseData);
		$response = array();
		if(isset($responseData['success'])){
			$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
			$actionID = $historyData['actionID'];
			$siteID = $historyData['siteID'];
			$response['_'.$siteID] = $responseData['success'];
			
			DB::insert("?:temp_storage", array('type' => 'wordFence', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
			DB::update("?:history_additional_data", "status='success'", "historyID=".$historyID."");
			return;
		}
	}
	
	/*
	 * Load the Previous Scaned Result
	 */
	public static function wordFenceLoadProcessor($siteIDs, $params){
		$type = "wordFence";
		$action = "load";
		$requestAction = "wordfence_load";
		
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => 'wordFenceLoad', 'detailedAction' => 'loadPreviousScanResults');
		$events=1;
		foreach ($siteIDs as $siteID) {
			$siteData = getSiteData(intval($siteID));
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['directExecute'] 	= false;
			$PRP['events'] 			= $events;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
			prepareRequestAndAddHistory($PRP);
		}
	}
	
	/*
	 * Response for Load the Previous Scaned Result
	 */
	public static function wordFenceLoadResponseProcessor($historyID, $responseData){
		responseDirectErrorHandler($historyID, $responseData);
		$response = array();
		if(isset($responseData['success'])){
			$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
			$actionID = $historyData['actionID'];
			$siteID = $historyData['siteID'];
			$response['_'.$siteID] = $responseData['success'];
			
			DB::insert("?:temp_storage", array('type' => 'wordFence', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
			DB::update("?:history_additional_data", "status='success'", "historyID=".$historyID."");
			return;
		}
	}
	
}
manageClients::addClass('manageClientsWordFence');