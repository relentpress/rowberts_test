<?php
/************************************************************
* InfiniteWP Admin panel - WordFence Plugin					*
* Copyright (c) 2014 Revmakx								*
* www.revmakx.com											*
*															*
************************************************************/
function wordFenceAddonMenus(&$menus){
	$menus['protect']['subMenus'][] = array('page' => 'wordFence', 'displayName' => 'Wordfence');
}

function wordFenceResponseProcessors(&$responseProcessor){
	$responseProcessor['wordFence']['scan'] = 'wordFenceScan';
	$responseProcessor['wordFence']['load'] = 'wordFenceLoad';
	$responseProcessor['wordFence']['scanCheck'] = 'wordFenceScanCheck';
}

function wordFenceTaskTitleTemplate(&$template){
	$template['wordFence']['scan'][""] = "Initiating wordfence scan in <#sitesCount#> site<#sitesCountPlural#>";
	$template['wordFence']['load'][""] = "Loading wordfence scan results from <#sitesCount#> site<#sitesCountPlural#>";
}

/***********************************
 *                                 *
 * Require data functions starts   * 
 *                                 *
 ***********************************/
 
function wordFenceLoad() {
	$actionID = Reg::get('currentRequest.actionID');
	$linksData = DB::getFields("?:temp_storage", "data", "type = 'wordFence' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'wordFence' AND paramID = '".$actionID."'");
		
	if(empty($linksData)){
		return array();
	}
	$refinedData = array();

	foreach($linksData as $data){
		$refinedData = array_merge_recursive($refinedData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($refinedData);
	foreach($refinedData as $siteID => $links){
		$siteData = DB::getRow("?:sites","URL","siteID=".$siteID."");
		$refinedData['siteURLs'][] = $siteData['URL'];
		$refinedData['siteIDs'][] = $siteID;
	}

        return $refinedData;
}

function wordFenceScan() {
	$actionID = Reg::get('currentRequest.actionID');
	$linksData = DB::getFields("?:temp_storage", "data", "type = 'wordFence' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'wordFence' AND paramID = '".$actionID."'");
		
	if(empty($linksData)){
		return array();
	}
	$refinedData = array();

	foreach($linksData as $data){
		$refinedData = array_merge_recursive($refinedData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($refinedData);
	foreach($refinedData as $siteID => $links){
		$siteData = DB::getRow("?:sites","URL","siteID=".$siteID."");
		$refinedData['siteURLs'][] = $siteData['URL'];
		$refinedData['siteIDs'][] = $siteID;
	}
	addNotification($type='N', $title='SCAN INITIATED', $message='The WordFence scan is underway on the selected sites. We will notify you here once its completed.', $state='U', $callbackOnClose='', $callbackReference='');
	$scheduleTime = time()+(60*5);
	addNotification($type='N', $title='SCAN RUNNING STATUS', $message='Your scan may be completed. <a id="wordfenceResult" aid="'.$actionID.'">View the result</a>', $state='U', $callbackOnClose='', $callbackReference='', $scheduleTime);
        return $refinedData;
}

function wordFenceScanCheck() {
	$recheck = false;
	$actionID = Reg::get('currentRequest.actionID');
	$linksData = DB::getFields("?:temp_storage", "data", "type = 'wordFence' AND paramID = '".$actionID."'");
	DB::delete("?:temp_storage", "type = 'wordFence' AND paramID = '".$actionID."'");
		
	if(empty($linksData)){
		return array();
	}
	$refinedData = array();

	foreach($linksData as $data){
		$refinedData = array_merge_recursive($refinedData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($refinedData);
	foreach($refinedData as $siteID => $links){
		$siteData = DB::getRow("?:sites","URL","siteID=".$siteID."");
		$refinedData['siteURLs'][] = $siteData['URL'];
		$refinedData['siteIDs'][] = $siteID;
	}

    return $refinedData;
}

function wordFenceGetSiteID($args) {
	$siteIds = DB::getFields("?:history", "siteID", "actionID = '".$args['actionId']."'");
        return $siteIds;
}