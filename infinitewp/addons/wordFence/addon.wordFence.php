<?php

/*
slug: wordFence
version: 1.0.2
*/

class addonWordFence{
	
	private static $version = '1.0.2';
	
	public static function version(){
		return self::version;	
	}
	
	public static function init(){
		require_once(APP_ROOT."/addons/wordFence/controllers/func.php");	
		require_once(APP_ROOT."/addons/wordFence/controllers/manageClientsWordFence.php");
		panelRequestManager::addFunctions('wordFenceLoad','wordFenceScan','wordFenceScanCheck','wordFenceGetSiteID');
		regHooks('responseProcessors', 'taskTitleTemplate', 'addonMenus');
	}	
}

?>