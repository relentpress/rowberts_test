<?php
class manageClientsClientPluginBranding{
	
	public static function clientPluginBrandingProcessor($siteIDs, $params){
		$type = "clientPluginBranding";
		$action = "change";
		$requestAction = "client_brand";
		
		updateOption("clientPluginBrandingVariables", serialize($params));
		$requestParams = array('brand' => array('name' => $params['pluginName'], 'desc' => $params['description'], 'author' => $params['authourName'], 'author_url' => $params['authourURL'], 'hide' => $params['hide']));
		
		
		if(!empty($siteIDs)){
			$sites = getSitesData($siteIDs);
		}
		else{
			$sites = getSitesData();
		}
		
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => 'clientPluginBranding', 'detailedAction' => 'get');
		$events=1; 
		
		if(is_array($sites))
		  foreach($sites as $siteID => $siteData){
			  
			  $PRP = array();
			  $PRP['requestAction'] 	= $requestAction;
			  $PRP['requestParams'] 	= $requestParams;
			  $PRP['siteData'] 		= $siteData;
			  $PRP['type'] 			= $type;
			  $PRP['action'] 			= $action;
			  $PRP['events'] 			= $events;
			  $PRP['historyAdditionalData'] 	= $historyAdditionalData;
			  
			  
			  prepareRequestAndAddHistory($PRP);
		}	
	}
	
	public static function clientPluginBrandingResponseProcessor($historyID, $responseData){
		responseDirectErrorHandler($historyID, $responseData);
		
		if(empty($responseData['success'])){
			return false;
		}
		
		if(!empty($responseData['success'])){
			DB::update("?:history_additional_data", "status='success'", "historyID=".$historyID."");
		}
	}
}

manageClients::addClass('manageClientsClientPluginBranding');
?>