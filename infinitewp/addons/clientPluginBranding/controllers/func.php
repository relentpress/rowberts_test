<?php
function clientPluginBrandingResponseProcessors(&$responseProcessor){
	$responseProcessor['clientPluginBranding']['change'] = 'clientPluginBranding';
}

function clientPluginBrandingTaskTitleTemplate(&$template){
	$template['clientPluginBranding']['change']['']	= "Brand client plugin in <#sitesCount#> site<#sitesCountPlural#>";
}

function clientPluginBrandingPostAddSite($siteID){
	$clientPluginBrandingVariables = getOption('clientPluginBrandingVariables');
		
	if(empty($clientPluginBrandingVariables)){
		return false;
	}
	
	$clientPluginBrandingVariables = @unserialize($clientPluginBrandingVariables);
	
	$_POST = array('action' => 'clientPluginBranding', 'args' => array('params' => array('pluginName' => $clientPluginBrandingVariables['pluginName'], 'description' => $clientPluginBrandingVariables['description'], 'authourName' => $clientPluginBrandingVariables['authourName'], 'authourURL' => $clientPluginBrandingVariables['authourURL'], 'hide' => $clientPluginBrandingVariables['hide']), 'siteIDs' => array($siteID)));
			  
	 panelRequestManager::handler($_POST);
}

function clientPluginBrandingAddonHeadJS(&$headJS){
	$headJS[] = 'var clientPluginBrandingSettings = '.@json_encode(@unserialize(getOption('clientPluginBrandingVariables'))).';';
}

function clientPluginBrandingGet(){
	$clientPluginBrandingVariables = @unserialize(getOption('clientPluginBrandingVariables'));
		
	if(empty($clientPluginBrandingVariables)){
		return false;
	}
	else{
		return $clientPluginBrandingVariables;
	}
}

?>