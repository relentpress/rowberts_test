var clientPluginBrandingSettings,client_plugin_branding_content='';
function processBranding(data)
{
	$("#saveSettingsBtn").removeClass('disabled');
	$(".settings_cont .btn_loadingDiv").remove();
	var mainData = data.data;
	if(typeof mainData.clientPluginBrandingGet != 'undefined' && mainData.clientPluginBrandingGet != false){
		$("#saveSuccess").show();
		setTimeout(function () { $("#saveSuccess").hide();},1000);
		$("#settings_btn").removeClass('active');
		clientPluginBrandingSettings=mainData.clientPluginBrandingGet;
		loadNewClientPluginBranding(clientPluginBrandingSettings);
	}

}

function loadNewClientPluginBranding(data)
{
	if(data.pluginName!=undefined)
	$("#brandingTab #pluginName").val(data.pluginName);
	if(data.description!=undefined)
	$("#brandingTab #description").val(data.description);
	if(data.authourName!=undefined)
	$("#brandingTab #authourName").val(data.authourName);
	if(data.authourURL!=undefined )
	$("#brandingTab #authourURL").val(data.authourURL);
	if(data.hide!=undefined)
	{
	if(data.hide==1)
	{
	$("#brandingTab .hideClientPlugin").addClass('active');
	$("#brandingTab .formVal").addClass('disabled');
	}
	}
}