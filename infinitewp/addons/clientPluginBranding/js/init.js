$(function () {
	// $("#settings_cont .btn_radio_slelect.mainSettingsTab").prepend('<li><a class="rep_sprite optionSelect settingsButtons" item="brandingTab">Client Plugin Branding</a></li>');
	cpBrandingSettings = '<li>Client Plugin Branding</li>';
        client_plugin_branding_content = '<div class="settings settingsItem branding" id="brandingTab" style="border: 0px; padding: 0px; "> <div style="padding:10px;"> <div class="label" style="padding: 10px 0;">CHANGE THE CLIENT PLUGIN DETAILS, DISPLAYED ON THE WP SITES</div> <div style="float:left; width:49%; margin-right:2%;"> <div class="form_label">PLUGIN NAME</div> <input name="" type="text" class="half formVal required" id="pluginName"> </div> <div style="float:left; width:49%"> <div class="form_label">AUTHOR NAME</div> <input name="" type="text" class="half formVal" id="authourName"> </div> <div class="clear-both"></div> <div><div class="form_label">PLUGIN DESCRIPTION</div> <input name="" type="text" class="full formVal" id="description"></div> <div class="clear-both"></div> <div><div class="form_label">AUTHOR URL</div> <input name="" type="text" class="full formVal txtHelp" helptxt="http://" value="http://" id="authourURL"></div> <div class="clear-both"></div> </div> <div class="checkbox hideClientPlugin" style="border-top: 1px solid #E0E0E0;">Hide InfiniteWP Client Plugin from the plugin list</div> </div><div class="th_sub rep_sprite" ><div class="success rep_sprite_backup float-left" id="saveSuccess" style="display:none">Saved successfully!</div><div class="btn_action float-right"><a class="rep_sprite" id="saveSettingsBtn" page="brandingTab">Save Changes</a></div></div>';
	// $("#settings_cont #appSettingsTab").after('<div class="settings settingsItem branding" id="brandingTab" style="border: 0px; padding: 0px; display:none"> <div style="padding:10px;"> <div class="label" style="padding: 10px 0;">CHANGE THE CLIENT PLUGIN DETAILS, DISPLAYED ON THE WP SITES</div> <div style="float:left; width:49%; margin-right:2%;"> <div class="form_label">PLUGIN NAME</div> <input name="" type="text" class="half formVal required" id="pluginName"> </div> <div style="float:left; width:49%"> <div class="form_label">AUTHOR NAME</div> <input name="" type="text" class="half formVal" id="authourName"> </div> <div class="clear-both"></div> <div><div class="form_label">PLUGIN DESCRIPTION</div> <input name="" type="text" class="full formVal" id="description"></div> <div class="clear-both"></div> <div><div class="form_label">AUTHOR URL</div> <input name="" type="text" class="full formVal txtHelp" helptxt="http://" value="http://" id="authourURL"></div> <div class="clear-both"></div> </div> <div class="checkbox hideClientPlugin" style="border-top: 1px solid #E0E0E0;">Hide InfiniteWP Client Plugin from the plugin list</div> </div>');
	if(clientPluginBrandingSettings!=undefined)
	{
		loadNewClientPluginBranding(clientPluginBrandingSettings);
	}
	$(".hideClientPlugin").live('click',function() {
		makeSelection(this);
		if($(this).hasClass('active'))
		$("#brandingTab .formVal").addClass('disabled');
		else
		$("#brandingTab .formVal").removeClass('disabled');
		
	});
	$("#brandingTab .formVal.disabled").live('focus',function() {
		$(this).blur();
	});
});