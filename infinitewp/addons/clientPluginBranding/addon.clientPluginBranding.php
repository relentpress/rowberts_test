<?php

/*
slug: clientPluginBranding
version: 1.0.3
*/

class addonClientPluginBranding{
	
	private static $version = '1.0.3';
	
	public static function version(){
		return self::version;	
	}
	
	public static function init(){
		require_once(APP_ROOT."/addons/clientPluginBranding/controllers/func.php");	
		require_once(APP_ROOT."/addons/clientPluginBranding/controllers/manageClientsClientPluginBranding.php");
		panelRequestManager::addFunctions('clientPluginBrandingGet');
		regHooks('responseProcessors', 'taskTitleTemplate', 'addonHeadJS', 'postAddSite');
	}	
}

?>