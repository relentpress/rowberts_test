<?php
class manageClientsWpOptimize{
	
	public static function wpOptimizeProcessor($siteIDs, $params){
		
		$type = "wp";
		$action = "optimize";
		$requestAction = "wp_optimize";
			  
		$requestParams = $params['type'];	
		
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => "optimize", 'detailedAction' => $type);
		  
		  foreach($siteIDs as $siteID){
			  $siteData = getSiteData($siteID);
			  $events=1;
			  
			  $PRP = array();
			  $PRP['requestAction'] 			= $requestAction;
			  $PRP['requestParams'] 			= $requestParams;
			  $PRP['siteData'] 					= $siteData;
			  $PRP['type'] 						= $type;
			  $PRP['action'] 					= $action;
			  $PRP['events'] 					= $events;
			  $PRP['sendAfterAllLoad'] 			= true;
			  $PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			  prepareRequestAndAddHistory($PRP);
		  }
		
	}
	
	public static function wpOptimizeResponseProcessor($historyID, $responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		  
		  if( empty($responseData['success'])  ){
			  return false;
		  }
	
		  $updateLink = array();
		  if(!empty($responseData['success'])){
			$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
			$actionID = $historyData['actionID'];
			$siteID = $historyData['siteID'];
			$response['_'.$siteID] = $responseData['success'];
			
			DB::insert("?:temp_storage", array('type' => 'wpOptimize', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
			DB::update("?:history_additional_data", "status='success'", "historyID=".$historyID."");
		}
	}
}
manageClients::addClass('manageClientsWpOptimize');
?>