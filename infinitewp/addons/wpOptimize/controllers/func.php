<?php
function wpOptimizeAddonMenus(&$menus){
	//$menus[] = '<li><a class="navLinks optionSelect" page="wpOptimize">WP Optimize</a></li>';
	$menus['maintain']['subMenus'][] = array('page' => 'wpOptimize', 'displayName' => 'WP Maintenance');
}

function wpOptimizeResponseProcessors(&$responseProcessor){
	$responseProcessor['wp']['optimize'] = 'wpOptimize';
}

function wpOptimizeResult(){
	
	
	$actionID = Reg::get('currentRequest.actionID');
		
	$getDatas = DB::getFields("?:temp_storage", "data", "type = 'wpOptimize' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'wpOptimize' AND paramID = '".$actionID."'");
		
	if(empty($getDatas)){
		return array();
	}
	$finalData = array();
	foreach($getDatas as $data){
		$finalData = array_merge_recursive($finalData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($finalData);
		
	return $finalData;
}


function wpOptimizeTaskTitleTemplate(&$template){
	$template['wp']['optimize']['']	= "Perform maintenance in <#sitesCount#> site<#sitesCountPlural#> ";
}

?>