function loadOptimizeMainPage()
{
	var content='</div><div class="steps_hdr">Select websites for <span id="processType">Maintenance</span> </div><div class="siteSelectorContainer">'+siteSelectorVar+'</div></div> <div class="result_block shadow_stroke_box siteSearch bulk_publish_page_post" style="border:0;box-shadow: 0 0 0  rgba(0, 0, 0, 0)"><div class="optionsContent result_block_noborder"></div>';
	$("#pageContent").html(content);
	$(".optionsContent").html('<div class="" style=""><div class="steps_hdr">MAINTENANCE OPTIONS</div> <div style="width:220px;"> <div class="checkbox selectOptimize active" type="revisions" key="clean-revisions" onclick="">Remove all Post Revisions</div> <div class="checkbox selectOptimize active" status="" type="autodraft" key="clean-autodraft" onclick="">Remove all auto Draft posts</div><div class="checkbox selectOptimize active" type="spam" key="clean-comments" onclick="">Clear "Spam" comments</div><div class="checkbox selectOptimize active" type="unapproved" key="unapproved-comments" onclick="">Clear Unapproved Comments</div><div class="checkbox selectOptimize active" key="optimize-db" type="optimize-db" onclick="">Optimize database Tables</div> </div><div class="btn_action float-left fetchPostCont"><a class="rep_sprite loadOptimize" style="margin: 8px 30px 8px 0px; width: 121px; text-align:center;">Perform Maintenance</a></div><div class="shadow_stroke_box response_wpOptimize responseFullDiv" style="display:none;margin-top: 50px;"> <div class="th rep_sprite"><div class="title"><span class="droid700">Response</span></div></div><div id="responseCont"></div></div></div>');
	siteSelectorNanoReset();
}
function loadOptimizePanel(data)
{
	var optimizeContent = data.data.wpOptimizeResult,content='';
	$(".loadOptimize").removeClass('disabled');
	$(".btn_loadingDiv").remove();
	$.each(optimizeContent, function(siteID, val) {
		content=content+'<div class="site_response_cont" style="border-bottom:1px solid #efefef;"><div class="site_name" style="padding:8px; font-weight:700;">'+site[siteID].name+'</div><div class="response" style="padding:8px 8px 8px 26px; line-height: 22px;">'+val+'</div></div>';
	 });
	 $(".responseFullDiv").show();
	$("#responseCont").html(content);
}