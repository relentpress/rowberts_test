<?php

/*
slug: wpOptimize
version: 1.0.1
*/

class addonWpOptimize{
	
	private static $version = '1.0.1';

	public static function version(){
		return self::version;	
	}
	
	public static function init(){
		require_once(APP_ROOT."/addons/wpOptimize/controllers/func.php");	
		require_once(APP_ROOT."/addons/wpOptimize/controllers/manageClientsWpOptimize.php");
		panelRequestManager::addFunctions('wpOptimizeResult');
		regHooks('addonMenus', 'responseProcessors', 'taskTitleTemplate');
	}	
	
}
?>