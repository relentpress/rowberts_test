<?php 
$lang = array();
$lang['plugin']	    		= "Plugins updated";
$lang['themes'] 			= "Themes updated";
$lang['core'] 				= "WordPress updated";
$lang['comments'] 			= "Comments managed";
$lang['backup'] 			= "Backups performed";
$lang['googleAnalytics'] 	= "Google analytics";
$lang['malwareScan'] 		= "Malware scans performed";
$lang['posts'] 				= "Posts managed";
$lang['pages'] 				= "Pages managed";
$lang['links'] 				= "Links managed";
$lang['malwareScanningSucuri'] = "Malware scanning - Sucuri";
$lang['ga:newVisits'] 		= "New visits";
$lang['ga:visits'] 			= "Visits";
$lang['ga:visitors'] 		= "Unique visitors";
$lang['ga:percentNewVisits'] = "% New visits";
$lang['ga:pageviews'] 		= "Page views";
$lang['ga:pageviewsPerVisit'] = "Pages / Visit";
$lang['ga:visitBounceRate'] = "Bounce rate";
$lang['ga:avgTimeOnSite'] 	= "Average visit duration";

?>