<?php

/*
slug: clientReporting
version: 2.3.1
*/


class addonClientReporting{
	
	private static $version = '2.3.1';

	public static function version(){
		return self::version;
	}
	
	public static function init(){
		require_once(APP_ROOT."/addons/clientReporting/controllers/func.php");	
		if(defined('CLIENT_REPORTING_LANG') && file_exists(APP_ROOT.'/addons/clientReporting/lang/'.CLIENT_REPORTING_LANG.'.php')){
			require_once(APP_ROOT.'/addons/clientReporting/lang/'.CLIENT_REPORTING_LANG.'.php');	
			$GLOBALS['LANG'] = $lang;
		}
		else{
			require_once(APP_ROOT.'/addons/clientReporting/lang/en-us.php');	
			$GLOBALS['LANG'] = $lang;
		}

		panelRequestManager::addFunctions('clientReportingGenerate','clientReportingGeneratePdf','clientReportingGetLogo', 'clientReportingSchedule', 'clientReportingScheduleReportViewHTML', 'clientReportingManualReportViewHTML', 'clientReportingGetSchedule', 'clientReportingScheduleReportDelete', 'clientReportingDeleteSentPdf', 'clientReportingScheduleReportPause', 'clientReportingScheduleReportRunNow', 'clientReportingGetHeaderFooterCont');
		regHooks('addonMenus', 'cronRun', 'getNextSchedule');
	}
	
	public static function install(){
		
		$Q1 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:client_report_schedule` (
						  `clientReportScheduleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
						  `profileName` varchar(255) NOT NULL,
						  `period` enum('weekly','monthly') NOT NULL,
						  `schedule` varchar(50) NOT NULL,
						  `params` text,
						  `nextSchedule` int(10) unsigned DEFAULT NULL,
						  `lastRun` int(10) unsigned DEFAULT NULL,
						  `paused` enum('1','0') DEFAULT NULL,
						  PRIMARY KEY (`clientReportScheduleID`)
						) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");


		$Q2 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:client_report_sent_pdf` (
						  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
						  `clientReportScheduleID` int(10) unsigned DEFAULT NULL,
						  `profileName` varchar(255) DEFAULT NULL,
						  `params` text,
						  `pdfURL` varchar(255) NOT NULL,
						  `reportType` enum('schedule','manual') DEFAULT NULL,
						  `sentTime` int(10) unsigned DEFAULT NULL,
						  PRIMARY KEY (`ID`)
						) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");
		
		if($Q1 && $Q2){
			return true;
		}
		return false;

	}
	
	
	public static function update(){
		
		$Q1 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:client_report_schedule` (
						  `clientReportScheduleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
						  `profileName` varchar(255) NOT NULL,
						  `period` enum('weekly','monthly') NOT NULL,
						  `schedule` varchar(50) NOT NULL,
						  `params` text,
						  `nextSchedule` int(10) unsigned DEFAULT NULL,
						  `lastRun` int(10) unsigned DEFAULT NULL,
						  `paused` enum('1','0') DEFAULT NULL,
						  PRIMARY KEY (`clientReportScheduleID`)
						) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");
		
		
		$Q2 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:client_report_sent_pdf` (
						  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
						  `clientReportScheduleID` int(10) unsigned DEFAULT NULL,
						  `profileName` varchar(255) DEFAULT NULL,
						  `params` text,
						  `pdfURL` varchar(255) NOT NULL,
						  `reportType` enum('schedule','manual') DEFAULT NULL,
						  `sentTime` int(10) unsigned DEFAULT NULL,
						  PRIMARY KEY (`ID`)
						) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");
		
		if($Q1 && $Q2){
			return true;
		}
		return false;
	}
}

?>