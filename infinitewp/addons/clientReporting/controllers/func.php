<?php

function clientReportingGetNextSchedule(&$nextScheduleTime){
	$nextTime = DB::getField("?:client_report_schedule", "MIN(nextSchedule)", "paused='0' ");
	if(!empty($nextTime)){
		$nextScheduleTime[] = $nextTime;
	}
}

function clientReportingAddonMenus(&$menus){
	//$menus[] = '<li><a class="navLinks optionSelect" page="clientReporting">Client Reporting</a></li>';
	$menus['tools']['subMenus'][] = array('page' => 'clientReporting', 'displayName' => 'Client Reporting');
}

function clientReportingHeaderFooterLogoSave($params){
	
	if(!empty($params)){
		$params['headerContent'] = $params['headerContent'];
		$params['footerContent'] = $params['footerContent'];
		updateOption("headerFooterLogoCont", serialize($params));
	}
}

function clientReportingGetLogo(){
	$headerFooterLogoCont = getOption("headerFooterLogoCont");
	$headerFooterLogoCont = unserialize($headerFooterLogoCont);
	return $headerFooterLogoCont['logoName'];
}

function clientReportingGetHeaderFooterCont(){
	$headerFooterCont = getOption("headerFooterLogoCont");
	$headerFooterData = unserialize($headerFooterCont);
	$headerFooterCont = array('headerContent' => $headerFooterData['headerContent'], 'footerContent' => $headerFooterData['footerContent']);
	return $headerFooterCont;
}

function clientReportingGetSchedule($params){
	if(!empty($params['clientReportScheduleID'])){
		$getManualSentReport = DB::getRow("?:client_report_schedule", "*", "clientReportScheduleID = '".$params['clientReportScheduleID']."'");
		$scheduleData = unserialize($getManualSentReport['params']); 
		$scheduleData['clientReportScheduleID'] = $getManualSentReport['clientReportScheduleID'];
		return $scheduleData;
	}
}

function manualReportViewGetData(){
	$getManualSentReport = DB::getArray("?:client_report_sent_pdf", "*", "reportType = 'manual' ORDER BY ID ASC");
	if(!empty($getManualSentReport))
	foreach($getManualSentReport as $arrKey => $value){
		$paramData = unserialize($value['params']);
		$manualSentReportData[$value['ID']] = array('sendPdfID' => $value['ID'], 'toMail' => unserialize($paramData['toEmail']), 'sitesCount' => $paramData['sitesCount'], 'sentTime' => $value['sentTime'], 'pdfURL' => APP_URL.'uploads/'.unserialize(base64_decode($value['pdfURL'])));
	}
	
	return $manualSentReportData;
}

function clientReportingManualReportViewHTML(){
	$manualSentReportData = manualReportViewGetData();
	$HTML = TPL::get('/addons/clientReporting/templates/manualReportView.tpl.php', array('manualSentReportData' => $manualSentReportData));
	return $HTML;
}


function scheduleReportViewGetData(){
	$getSchedule = DB::getArray("?:client_report_schedule", "*", "1 ORDER BY clientReportScheduleID ASC");
	if(!empty($getSchedule)){
		foreach($getSchedule as $key => $value){
			$params = unserialize($value['params']);
			$siteIDCount = count($params['siteID']);
			$scheduleReportData[$value['clientReportScheduleID']] = array('clientReportScheduleID' => $value['clientReportScheduleID'], 'profileName' => $value['profileName'], 'siteIDCount' => $siteIDCount, 'period' => $value['period'], 'paused' => $value['paused']);
			$getScheduleSentReport = DB::getArray("?:client_report_sent_pdf", "*", "reportType = 'schedule' AND clientReportScheduleID = '".$value['clientReportScheduleID']."'");
			
			if(empty($getScheduleSentReport)){ continue; }
			foreach($getScheduleSentReport as $arrayKey => $sentReportData){
				$paramData = unserialize($sentReportData['params']);
				$scheduleReportData[$value['clientReportScheduleID']]['data'][$sentReportData['ID']] = array('sentPdfID' => $sentReportData['ID'], 'toMail' => unserialize($paramData['toEmail']), 'sentTime' => $sentReportData['sentTime'], 'pdfURL' =>  APP_URL.'uploads/'.unserialize(base64_decode($sentReportData['pdfURL'])));
			}
		}		
		
		return $scheduleReportData;
	}
}

function clientReportingScheduleReportViewHTML(){
	$scheduleReportData = scheduleReportViewGetData();
	$HTML = TPL::get('/addons/clientReporting/templates/scheduleReportView.tpl.php', array('scheduleReportData' => $scheduleReportData));
	return $HTML;
}

//Add or Edit Schedule
function clientReportingSchedule($params){
	if(empty($params)){	return false; }
	$params['headerFooterLogoCont']['headerContent'] = $params['headerFooterLogoCont']['headerContent'];
	$params['headerFooterLogoCont']['footerContent'] = $params['headerFooterLogoCont']['footerContent'];
	$params['clientNote'] 							 = $params['clientNote'];
	$params['email']['message'] 					 = $params['email']['message'];
	if(empty($params['clientReportScheduleID'])){
		clientReportingHeaderFooterLogoSave($params['headerFooterLogoCont']);
		$nextSchedule = clientReportingNextSchedule($params['period'], $params['schedule']);
		$addSchedule = DB::insert("?:client_report_schedule", array("profileName" => $params['profileName'], "period" => $params['period'], "schedule" => $params['schedule'], "params" => serialize($params), "nextSchedule" => $nextSchedule, "paused" => 0));
	
		if($addSchedule){ return true; } else{ return false; }
	}
	else{

		$nextSchedule = clientReportingNextSchedule($params['period'], $params['schedule']);
		$editSchedule = DB::update("?:client_report_schedule", array("profileName" => $params['profileName'], "period" => $params['period'], "schedule" => $params['schedule'], "params" => serialize($params), "nextSchedule" => $nextSchedule), "clientReportScheduleID = '".$params['clientReportScheduleID']."'");
	
		if($editSchedule){ return true; } else{ return false; }
	}
}

//pause or resume the scheduled report
function clientReportingScheduleReportPause($params){
	if(empty($params)){	return false; }
	else{
		$paused = DB::update("?:client_report_schedule", array('paused' => $params['paused']), "clientReportScheduleID = '".$params['clientReportScheduleID']."'");
		if($paused){ return true; }
	}
}

function clientReportingScheduleReportDelete($params){
	if(empty($params)){	return false; }
	$getSentPdfURL = DB::getArray("?:client_report_sent_pdf", "pdfURL", "clientReportScheduleID = '".$params['clientReportScheduleID']."'");
	if(!empty($getSentPdfURL)){
		foreach($getSentPdfURL as $key => $URL){
			$pdfURL = unserialize(base64_decode($URL));
			$pdfPath = parse_url($pdfURL);
			@unlink($pdfPath);
		}
	}
	DB::delete("?:client_report_sent_pdf", "clientReportScheduleID = '".$params['clientReportScheduleID']."'");
	$deleteScheduleAndPdf = DB::delete("?:client_report_schedule", "clientReportScheduleID = '".$params['clientReportScheduleID']."'");
	
	if($deleteScheduleAndPdf){ return true; } else{ return false; }
}

function clientReportingDeleteSentPdf($params){
	if(empty($params)){	return false; }
	
	$getSentPdfURL = DB::getField("?:client_report_sent_pdf", "pdfURL", "ID = '".$params['sentPdfID']."' AND reportType = '".$params['reportType']."'");
	
	$pdfURL = unserialize(base64_decode($getSentPdfURL));
	$pdfPath = parse_url($pdfURL);
	$fileNameArray = explode('/', $pdfPath['path']);
	$fileName = end($fileNameArray);
	@unlink(APP_ROOT.'/uploads/'.$fileName);
	
	$deleteSentPdf = DB::delete("?:client_report_sent_pdf", "ID = '".$params['sentPdfID']."'");
	
	if($deleteSentPdf){ return true; } else{ return false; }
}

function clientReportingScheduleReportRunNow($params){
	$res = clientReportingCronRun($params);
	if($res){
		addNotification($type='N', $title='Client Report', $message='Run now completed.', $state='U', $callbackOnClose='', $callbackReference='');
	}
	return $res;
}

//Run scheduled client report.
function clientReportingCronRun($params=''){
	needFreshCall(3);
	
	if($params['runType'] == "scheduleReportRunNow"){
		$settings = DB::getArray("?:client_report_schedule", "*", "clientReportScheduleID = '".$params['clientReportScheduleID']."'");
	}
	else{
		$settings = DB::getArray("?:client_report_schedule", "*", "nextSchedule <= '".time()."' AND paused='0'");
	}
	
	if(!empty($settings)){
		foreach($settings as $key => $value){
			$params = unserialize($value['params']);
			
			//calculate the next schedule and update in the DB, Uncomment this later.
			if($params['runType'] == "scheduleReportRunNow"){
				$nextSchedule = $value['nextSchedule'];
			}
			else{
				$nextSchedule = clientReportingNextSchedule($params['period'], $params['schedule']);	
			}
			
			DB::update("?:client_report_schedule", array('nextSchedule' => $nextSchedule), "clientReportScheduleID='".$value['clientReportScheduleID']."'");
			
			if($params['period'] == 'monthly'){
				$fromDate = date("M d, Y", strtotime("-1 month"));
			}
			else{
				$fromDate = date("M d, Y", strtotime("-1 week"));
			}
			$toDate 				= date("M d, Y");
			$dates 					= $fromDate .'-'. $toDate;
			$generateReportparams 	= array("type" =>$params['type'],
										"action" => $params['action'],
										"siteID" => $params['siteID'],
										"dates" => $dates,
										"view" => $params['period']);
			
			$clientReportGeneratedData = clientReportingGenerate($generateReportparams);
			
			
			$pdfData['headerContent'] 	= $params['headerFooterLogoCont']['headerContent'];
			$pdfData['footerContent'] 	= $params['headerFooterLogoCont']['footerContent'];
			$pdfData['clientNote'] 		= $params['clientNote'];
			$pdfData['reportType'] 		= "schedule";
			$pdfData['email'] 			= $params['email'];
			$pdfData['profileName'] 	= $params['profileName'];
			$pdfData['clientReportScheduleID'] 	= $value['clientReportScheduleID'];
			$pdfData['dateRange'] 		= $dates;
			
			$emptyTask = true;
			if(!empty($clientReportGeneratedData)){
				foreach($clientReportGeneratedData as $siteID => $typeName){
					$val = array();
					if(!empty($typeName)){
					foreach($typeName as $type => $success){
						foreach($success as $countValue){
							$val[$type] = $countValue;
						}
					}
					$pdfData['siteID'][$siteID] = $val;
						$emptyTask = false;
					}
				}
			}
				
			if(empty($clientReportGeneratedData) || $emptyTask == true){ // it will send a mail to the panel admin (from email mentioned in the schedule.) abt no tasks or pdf.
				$pdfData['email']['message'] = "No operations were performed during ".$pdfData['dateRange']." in your reporting schedule - <strong>".$params['profileName']."</strong>. So the report was not emailed to the client.";
				$pdfData['email']['toEmail'] = $params['email']['fromEmail'];
				return sendMailPdfAttach($pdfData['email']);
			}
			$res = clientReportingGeneratePdf($pdfData);
			return $res;
		}
	}
}

function clientReportingNextSchedule($type, $schedule){
	
	$schedule = explode("|", $schedule);
	if (empty($schedule))
		return false;
	switch ($type) {
				
		case 'weekly':
			if (isset($schedule[2]) && $schedule[2]) {
				$delay_time = $schedule[2] * 60;
			}
			$current_weekday  = date('w');
			$schedule_weekday = $schedule[1];
			$current_hour     = date("H");
			$schedule_hour    = $schedule[0];
			
			if ($current_weekday > $schedule_weekday)
				$weekday_offset = 7 - ($current_weekday - $schedule_weekday);
			else
				$weekday_offset = $schedule_weekday - $current_weekday;
			
			
			if (!$weekday_offset) { //today is scheduled weekday
				if ($current_hour >= $schedule_hour)
					$time = mktime($schedule_hour, 0, 0, date("m"), date("d") + 7, date("Y"));
				else
					$time = mktime($schedule_hour, 0, 0, date("m"), date("d"), date("Y"));
			} else {
				$time = mktime($schedule_hour, 0, 0, date("m"), date("d") + $weekday_offset, date("Y"));
			}
			
			break;
		
		case 'monthly':
			if (isset($schedule[2]) && $schedule[2]) {
				$delay_time = $schedule[2] * 60;
			}
			$current_monthday  = date('j');
			$schedule_monthday = $schedule[1];
			$current_hour      = date("H");
			$schedule_hour     = $schedule[0];
			
			if ($current_monthday > $schedule_monthday) {
				$time = mktime($schedule_hour, 0, 0, date("m") + 1, $schedule_monthday, date("Y"));
			} else if ($current_monthday < $schedule_monthday) {
				$time = mktime($schedule_hour, 0, 0, date("m"), $schedule_monthday, date("Y"));
			} else if ($current_monthday == $schedule_monthday) {
				if ($current_hour >= $schedule_hour)
					$time = mktime($schedule_hour, 0, 0, date("m") + 1, $schedule_monthday, date("Y"));
				else
					$time = mktime($schedule_hour, 0, 0, date("m"), $schedule_monthday, date("Y"));
				break;
			}
			
			break;
		default:
			break;
	}
	
	if (isset($delay_time) && $delay_time) {
		$time += $delay_time;
	}
	
	return $time;

}

function clientReportingGenerate($params){
	
	if(empty($params)){ return false; }

	clientReportingHeaderFooterLogoSave($params['headerFooterLogoCont']);
	
	if(!empty($params['dates'])){
		
		$dates 		= explode('-', $params['dates']);
		$fromDate 	= strtotime(trim($dates[0]));
		$toDate		= strtotime(trim($dates[1]));
		//$result['dateRange'] 	= date("M d, Y", $fromDate) .'-'. date("M d, Y", $toDate);
		if(!empty($fromDate) && !empty($toDate) && $fromDate != -1 && $toDate != -1){
			$action = '';
			
			$typeIn = $params['type'];
			if(is_array($typeIn)){
				//if(in_array('backup', $params['type'])){//old code - backup count not working
	//				$action = "OR (type = 'scheduleBackup' AND action = 'runTask')";
	//			}
				
				if(in_array('backup', $typeIn)){//new code
					$action = "OR 
					(
					(type = 'backup' AND (action = 'now' OR  action = 'multiCallNow')) 
					OR 
					(type = 'scheduleBackup' AND (action = 'runTask' OR  action = 'multiCallRunTask'))
					
				)";
					if(($key = array_search('backup', $typeIn)) !== false) {
						unset($typeIn[$key]);
					}
					
				}
			}
			$toDate += 86399;
			$where = "( ( type IN ('".implode("','", array_unique($typeIn))."') AND action != 'get') ".$action." ) AND microtimeAdded >= ".$fromDate." AND  microtimeAdded <= ".$toDate." ";
		}
	}
	$siteName = array();
	$data = array();
	foreach($params['siteID'] as $siteIDs => $siteID){
		$data[$siteID] = array();
		$actionsHistoryData = array();

		$actionIDs = DB::getFields("?:history", "actionID", "siteID = '".$siteID."' AND ".$where. " GROUP BY actionID ORDER BY historyID ");
		
		$siteName[] = DB::getField("?:sites", "name", "siteID = '".$siteID."'");
		
		if(!empty($actionIDs)){ 
			$actionsHistoryData = array();
			foreach($actionIDs as $actionID){
				$actionsHistoryData[ $actionID ] = panelRequestManager::getActionStatus($actionID);
				unset($actionIDs);
			}
		if(!empty($actionsHistoryData) && is_array($actionsHistoryData))
		foreach($actionsHistoryData as $actionID => $actionData){
			//if(in_array($actionData['type'], $params['type'])){
				if($actionData['type'] == "PTC" && !empty($params['action'])){
					foreach($params['action'] as $actionKey => $action){
						foreach($actionData['detailedStatus'] as $key => $detailedAction){
								if($detailedAction['siteID'] == $siteID && $detailedAction['detailedAction'] == $action){
								array_push($params['type'], $action);
								if($detailedAction['status'] == 'success')
								$data[$siteID][$detailedAction['detailedAction']][] = $actionData['status'];
							}
						}
					}
					
				}else{
					foreach($actionData['detailedStatus'] as $key => $detailedAction){
						if($detailedAction['siteID'] == $siteID && $detailedAction['status'] == 'success')
						$data[$siteID][$actionData['type']][] = $detailedAction['status'];
					}
				}
			//}		
		}
		}else{
			if(!in_array('googleAnalytics', $params['type']) && !in_array('malwareScanningSucuri', $params['type']))
				continue;
		}
		if(in_array('googleAnalytics', $params['type'])){
			if(function_exists('googleAnalyticsGetProfileData')){
				
				$profileID = DB::getField("?:google_analytics_profiles_sites", "gaProfileID", "siteID = '".$siteID."'");
				if(!empty($profileID)){
					$args['siteID'] = $siteID;
					//$args['view'] = ($params['view'] == "monthly") ? "month" : "week";
					$args['clientReportAnalyticsDate']['startDate'] = date('Y-m-d', $fromDate);
					$args['clientReportAnalyticsDate']['endDate'] = date('Y-m-d', $toDate);
					$GAPD = googleAnalyticsGetProfileData($args);
					if(!empty($GAPD))
					$data[$siteID]['googleAnalytics'] = $GAPD;
				}
			}
		}
		
		/*if(in_array('malwareScanningSucuri', $params['type'])){
			if(DB::getExists("SHOW TABLES LIKE '?:other_history'"))
			$data[$siteID]['malwareScanningSucuri'] = DB::getField("?:other_history", "count(status)", "type = 'malwareScanningSucuri' AND siteID	= '".$siteID."' AND status = 'completed'");
		}*/
	}
	
	if(!empty($params['type']) && is_array($params['type']))
	foreach($params['type'] as $key => $type){
		foreach($data as $sID => $actionType){
			if(empty($actionType)) $result[$sID] = array();
			if($type == 'backup' && !empty($actionType['scheduleBackup'])){ $actionType[$type] = @array_merge($actionType['scheduleBackup'], (array)$actionType[$type]); }
			if($type == 'googleAnalytics' && !empty($actionType['googleAnalytics'])){
				foreach($actionType['googleAnalytics'] as $gaType => $gaValue){
					
					$floatVal = floatval ($gaValue);
					$val = round($floatVal, 2);
					
					if($gaType == 'ga:percentNewVisits' || $gaType == 'ga:visitBounceRate')
					$val = $val.'%';
					
					$result[$sID][L($gaType)] = array('success' => $val);	
				}
			}
			else{
				if(is_array($actionType[$type])){
					$result[$sID][L($type)] = @array_count_values($actionType[$type]);	
				}
			}
		}
	}
	
	if($params['combinedSitesReport'] == 'yes'){
		$dataAll = array();
		$siteName = implode(", ", $siteName);
		foreach($result as $siteID => $type){
			foreach($type as $status => $value){
				foreach($value as $key => $var){
					$dataAll[$siteName][$status][$key]+=$var;
				}
			}
		}
		$result = $dataAll;
	}
	
	return $result;
	
}

function clientReportingGeneratePdf($params){

$logoName = clientReportingGetLogo();
$footerContent = $params['footerContent'];
$clientNote = $params['clientNote'];
$reportTitle = $params['reportTitle'];
$headerContent = $params['headerContent'];
$dateRange = $params['dateRange'];

define('FOOTERCONTENT', $footerContent);

require_once(APP_ROOT.'/addons/clientReporting/lib/tcpdf/config/lang/eng.php');
require_once(APP_ROOT.'/addons/clientReporting/lib/tcpdf/tcpdf.php');

//if(class_exists('MYPDF') != true){

class MYPDF extends TCPDF {

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        
		// Footer Content
        $this->Cell(10, 10, FOOTERCONTENT, 'T', false, 'L', 0, '', 0, false, 'T', 'M');
		
		// Page number
		$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), T, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}

//}
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, false, 'ISO-8859-1', false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING);

// set default header data
$pdf->SetHeaderData( $logoName, PDF_HEADER_LOGO_WIDTH, '', $headerContent);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// set font
$pdf->SetFont('freeserif', '', 12);

//$pdf->SetFont('helvetica', '', 11);

// add a page
$pdf->AddPage();

$clientNoteArea = '';
if(!empty($clientNote)){
	$clientNoteArea = '<div style="border-top:1px solid #d2d5d7; border-bottom:1px solid #d2d5d7; margin:20px; padding:10px; line-height: 5px;"><div style="color:#525556; font-weight:bold; padding:10px;">CLIENT NOTE</div>'.$clientNote.'</div>';
}

if(!empty($params['siteID']) && is_array($params['siteID'])){
	$sitesCount = count($params['siteID']);
$tb = <<<EOF
		Report generated for the period - $dateRange
EOF;
$pdf->writeHTML($tb, true, false, false, false, '');
foreach($params['siteID'] as $siteID => $data){
	$siteName = DB::getField("?:sites", "name", "siteID = '".$siteID."'");
	if(!$siteName){
		$siteName = $siteID;
	}
	$pdfHTML = '';
	foreach($data as $key => $value){
		$pdfHTML =  $pdfHTML.'<tr><td>'.$key.'</td> <td>'.$value.'</td></tr>';
	}
	 
$tbl = <<<EOF
	<style>
	.creport_data_table tr td{ border-bottom: 1px solid #eee;}
	</style>
	<br /><br />
	<table class="creport_data_table" border="0" cellpadding="5" cellspacing="0" nobr="true" style="border-collapse: collapse; border: 1px solid #D2D5D7; margin-top:10px;">
	
	 <tr style="background-color:#E5E8E9; color:#525556;">
	  <th colspan="2" align="left">$siteName</th>
	 </tr>
	 $pdfHTML
	 </table>
EOF;

 $pdf->writeHTML($tbl, true, false, false, false, '');
}
}

$tb2 = <<<EOF
<div style="clear:both"></div>
$clientNoteArea
<div style="clear:both"></div>
EOF;

$pdf->writeHTML($tb2, true, false, false, false, '');
  	//Close and output PDF document
	$url = APP_URL.'uploads/';
	$path = APP_ROOT.'/uploads/';
	$fileName = 'clientReport-'.md5(date('m-d-Y-').microtime()).'.pdf';
	
	if($params['reportType'] == 'schedule' || $params['generatePdfAndSendMail'] == '1'){
		$pdf->Output($path.$fileName, 'F');
		$sendMail = sendMailPdfAttach($params['email'], $path.$fileName);
		if($sendMail){
			DB::insert("?:client_report_sent_pdf", array('clientReportScheduleID' => $params['clientReportScheduleID'], 'profileName' => $params['profileName'], 'params' => serialize(array('toEmail' => serialize($params['email']['toEmail']), 'sitesCount' => $sitesCount)), 'pdfURL' => base64_encode(serialize($fileName)), 'reportType' => $params['reportType'], 'sentTime' => time()));
			//if($params['generatePdfAndSendMail'] == '1'){
				addNotification($type='N', $title='Client Report', $message='E-Mail Sent Successfully.', $state='U', $callbackOnClose='', $callbackReference='');
				//return true;
			//}
			return true;
		}
	}
	else{
		$pdf->Output($fileName, 'D');
		exit;
	}
}

function sendMailPdfAttach($params, $attachment=''){
	
	require_once(APP_ROOT.'/lib/phpmailer.php');

	$mail = new PHPMailer(); // defaults to using php "mail()"
	
	$body = $params['message'];
	
	$mail->SetFrom($params['fromEmail'], $fromName);
	
	$toEmails = explode(',', $params['toEmail']);
	
	foreach($toEmails as $key => $toEmail){
		$mail->AddAddress($toEmail, $toName);
	}
	
	$mail->Subject = $params['subject'];
	
	if(!empty($attachment)){
		$mail->AddAttachment($attachment);
	}
	
	$mail->MsgHTML($body);
		
	if(!$mail->Send()) {
	  addNotification($type='E', $title='Mail Error', $message=$mail->ErrorInfo, $state='U');
	  return false;
	} else {
	  //echo "Message sent!";
	  return true;
	}
}

if(!function_exists('L')){
	function L($var){
		return $GLOBALS['LANG'][$var];
	}
}

?>