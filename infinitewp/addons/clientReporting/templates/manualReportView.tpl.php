<?php if(!empty($d['manualSentReportData'])){   ?>
  <div class="rows_cont" id="backupList">
  <?php foreach($d['manualSentReportData'] as $manualSentID => $manualSentData){ ?>
    <div class="ind_row_cont">
      <div class="row_summary manualReportRowSummary disabled">
      	<?php TPL::captureStart('sitesBackupsRowSummary'); ?>
        <div class="row_arrow "></div>
        <div class="row_name searchable "><?php echo $manualSentData['toMail'] ?></div>
        
                  <div class="manualReportTime delConfHide " style="margin: 10px 0px;"><?php echo @date(Reg::get('dateFormatLong'), $manualSentData['sentTime']); ?></div>
                  <div class="row_backup_action rep_sprite" style="float:right;">
                  	<a class="trash rep_sprite_backup removeManualReport" reportType = "manual" manualReportPdfkey="<?php echo $manualSentData['sendPdfID']; ?>"></a>
                    <div class="del_conf" style="display:none;">
                      <div class="label">Sure?</div>
                      <div class="yes manualDeleteReportPdf" reportType = "manual" manualReportPdfkey="<?php echo $manualSentData['sendPdfID']; ?>">Yes</div>
                      <div class="no manualDeleteReportPdf">No</div>
                    </div>
                  </div>
                  <div class="row_action float-left delConfHide "><a class="view_report_manual_a" href= "<?php echo $manualSentData['pdfURL']; ?>" target="_blank">View</a></div>
                  <div class="sites_bu rep_sprite_backup delConfHide"><?php echo $manualSentData['sitesCount']; ?></div>
              
        <div class="clear-both"></div>
        <?php TPL::captureStop('sitesBackupsRowSummary'); ?>
        <?php echo TPL::captureGet('sitesBackupsRowSummary'); ?>
      </div>
      
    </div>
    <?php } 
//END foreach($d['sitesBackups'] as $siteID => $siteBackups) ?>
  </div>
<?php } else { ?>
<div class="empty_data_set"><div class="line2">Looks like there are <span class="droid700">no reports here</span>. Create a <a class="createNewManualReport">Report Now</a>.</div></div>
<script>$(".searchItems","#backupPageMainCont").hide();</script>
<?php } ?>

