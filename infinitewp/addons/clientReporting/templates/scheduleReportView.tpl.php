<?php

if(!empty($d['scheduleReportData'])){ ?>
<div class="rows_cont">
<?php foreach($d['scheduleReportData'] as $reportID => $scheduleData){ ?>
 <div class="ind_row_cont">
	<div class="row_summary expand_summary_<?php echo $reportID ?>" >
	<?php TPL::captureStart('sitesBackupsRowSummary'); ?>
	<div class="row_arrow"></div>
	<div class="row_checkbox on_off"> 
		<?php if($scheduleData['paused'] == '0') { ?>
        <div class="cc_mask cc_schedule_report_mask" scheduleReportkey="<?php echo $scheduleData['clientReportScheduleID']; ?>"><div class="cc_img cc_schedule_img on"></div></div>
        <?php } if($scheduleData['paused'] == '1') { ?>
        <div class="cc_mask cc_schedule_report_mask" scheduleReportkey="<?php echo $scheduleData['clientReportScheduleID']; ?>"><div class="cc_img cc_schedule_img off"></div></div>
        <?php } ?>
	  </div>
	<div class="row_backup_action rep_sprite"><a class="run_now_report rep_sprite_backup" runType = "scheduleReportRunNow" scheduleReportkey="<?php echo $scheduleData['clientReportScheduleID']; ?>"></a></div>
	<div class="row_name searchable"><?php echo $scheduleData['profileName']; ?></div>
	<div class="row_backup_action rep_sprite" style="float:right;"><a class="trash rep_sprite_backup removeScheduleReport" scheduleReportkey="<?php echo $scheduleData['clientReportScheduleID']; ?>"></a>
	 <div class="del_conf" style="display:none;">
			  <div class="label">Sure?</div>
			  <div class="yes scheduleDeleteReport" scheduleReportkey="<?php echo $scheduleData['clientReportScheduleID']; ?>">Yes</div>
			  <div class="no scheduleDeleteReport">No</div>
			</div>
			</div>
	<div class="row_action float-left"><a class="editScheduleReport" scheduleReportkey="<?php echo $scheduleData['clientReportScheduleID']; ?>">Edit</a></div>
	<div class="time_bu rep_sprite_backup delConfHide"><?php echo ucfirst($scheduleData['period']); ?></div>
	<div class="sites_bu rep_sprite_backup delConfHide"><?php echo $scheduleData['siteIDCount']; ?></div>
	<div class="clear-both"></div>
	<?php TPL::captureStop('sitesBackupsRowSummary'); ?>
	<?php echo TPL::captureGet('sitesBackupsRowSummary'); ?>
  </div> 
  
  <div class="row_detailed" style="display:none;">
	<div class="rh">
	<?php echo TPL::captureGet('sitesBackupsRowSummary'); ?>
	  </div>
		 <div class="rd">
		<div class="row_updatee">
        
  <?php 

  if(!empty($scheduleData['data']))
  		foreach($scheduleData['data'] as $sentReportID){ ?>
	 
		<div class="row_updatee_ind">
			<div class="label_updatee float-left">
			<div class="label droid700 float-right reportNameForScheduleReport" style="text-overflow: ellipsis;"><?php echo $sentReportID['toMail'] ?></div>
		  </div>
			<div class="items_cont float-left">
			
			<div class="item_ind float-left  topBackup">
				
				<div class="rep_sprite_backup stats time delConfHide"><?php echo @date(Reg::get('dateFormatLong'), $sentReportID['sentTime']); ?></div>
				
			  <div class="row_backup_action rep_sprite" style="float:right;"><a class="trash rep_sprite_backup removeBackup removeReportTooltip" sid="<?php echo $datas['siteID']; ?>" scheduleReportPdfkey="<?php echo $sentReportID['sentPdfID']; ?>"></a>
				<div class="del_conf" style="display:none;">
			  <div class="label">Sure?</div>
			  <div class="yes scheduleDeleteReportPdf" reportType = "schedule" scheduleReportPdfkey="<?php echo $sentReportID['sentPdfID']; ?>" >Yes</div>
			  <div class="no scheduleDeleteReportPdf">No</div>
			</div></div>
				
				<?php if(!empty($datas['downloadURL'])){ ?><div class="row_backup_action rep_sprite delConfHide" style="float:right;"><a class="download rep_sprite_backup" href="<?php echo $datas['downloadURL']; ?>"></a></div> <?php } ?>
				<div class="row_action float-left delConfHide"><a target="_blank" href="<?php echo $sentReportID['pdfURL'];  ?>">View</a></div>
			  </div>
			 
		  </div>
			<div class="clear-both"></div>
		  </div>
		  
<?php  }?></div>
	  </div></div></div><!--ind_row_cont Ends-->
<?php }?>
	
  </div>

<?php }//End if
else{ ?>
<div class="empty_data_set"><div class="line2">Looks like there are <span class="droid700">no reports scheduled now</span>. Create a <a class="createNewScheduleReport">Report Schedule</a>.</div></div>
<script>$(".searchItems","#backupPageMainCont").hide();</script>
<?php } ?>