$(function () {
	var tempArray={};
	tempArray['requiredData'] = {};
	tempArray['requiredData']['clientReportingGetLogo']=1;
	tempArray['requiredData']['clientReportingGetHeaderFooterCont']=1;
	doCall(ajaxCallPath,tempArray,'storeReportData','json',"none");
});

$(".selectReport").live('click',function() {
		if(!($(this).hasClass('active')))
	{
		$(this).addClass('active');
	}
	else
	{
		$(this).removeClass('active');
	}
});

$('#mainReport').live('click',function() {
	//loadReportingMainPage();
	$('#loadingDiv').show();
	var tempArray={};
	tempArray['requiredData'] = {};
	tempArray['requiredData']['clientReportingManualReportViewHTML']=1;
	tempArray['requiredData']['clientReportingGetHeaderFooterCont']=1;
	doCall(ajaxCallPath,tempArray,'loadAllManualScheduleReports','json',"none");
});
$('#scheduleReport').live('click',function() {
	//loadReportingSchedulePage();
	$('#loadingDiv').show();
	var tempArray={};
	tempArray['requiredData'] = {};
	tempArray['requiredData']['clientReportingScheduleReportViewHTML']=1;
	tempArray['requiredData']['clientReportingGetHeaderFooterCont']=1;
	doCall(ajaxCallPath,tempArray,'loadAllScheduleReports','json',"none");
});

$("#generateReport").live('click',function() {
	/*var repTitle = $('.report_title').val();
	if(repTitle == "")
	{
		$('.report_title').qtip({id:"toggleGroupQtip",content: { text: 'Please Enter Report Title' }, position: { my: 'bottom center', at: 'top center',  adjust:{ y: -6} }, show: {  }, hide: {  }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 10, height:5} } });
		$('.report_title').addClass('error_report');
	}*/
	var clientReportDate = $('.reportDate').text();
	if(clientReportDate == "")
	{
		$('#widgetCalendar').closest('.op_cont').addClass('error_report');
	}
	else
	{
		if($('#widgetCalendar').closest('.op_cont').hasClass('error_report'))
		{
			$('#widgetCalendar').closest('.op_cont').removeClass('error_report');
		}
		$('.emailContent_scheduleReport').remove();
		$('.reportContent').show();
		$('#loadingDiv').show();
		$(this).append('<div class="btn_loadingDiv left"></div>');
		$(this).addClass('disabled');	
		var actions = {},PTCaction={},i=0,tempArray={};
		tempArray['requiredData'] = {};
		var sites = getSelectedSites();
		var combinedSitesReprt = '';
		combinedSitesReport = $('.combineAllReport').attr("var");
		$('.actions .selectReport.active').each(function(){
			
			actions[i] = $(this).attr("val");
			if($(this).attr("action") != "")
			PTCaction[i] = $(this).attr("action");
			i++;
		});
		tempArray['requiredData']['clientReportingGenerate']={};
		tempArray['requiredData']['clientReportingGenerate']['combinedSitesReport']=combinedSitesReport;
		tempArray['requiredData']['clientReportingGenerate']['siteID']={};
		tempArray['requiredData']['clientReportingGenerate']['type']={};
		tempArray['requiredData']['clientReportingGenerate']['action']={};
		tempArray['requiredData']['clientReportingGenerate']['siteID']=sites;
		tempArray['requiredData']['clientReportingGenerate']['type']=actions;
		tempArray['requiredData']['clientReportingGenerate']['action']=PTCaction;
		tempArray['requiredData']['clientReportingGenerate']['dates']=clientReportDate;
		
		tempArray['requiredData']['clientReportingGenerate']['headerFooterLogoCont']={};
		tempArray['requiredData']['clientReportingGenerate']['headerFooterLogoCont']['headerContent']=$('.headerContent textarea').val();
		tempArray['requiredData']['clientReportingGenerate']['headerFooterLogoCont']['footerContent']=$('.footerContent textarea').val();
		tempArray['requiredData']['clientReportingGenerate']['clientNote']=$('.clientNote textarea').val();
		tempArray['requiredData']['clientReportingGenerate']['headerFooterLogoCont']['logoName']=$('.imageSpace').attr("imgSrc");
		doCall(ajaxCallPath,tempArray,'processReporting','json',"none");
	}
});

$("#createNewAction").live('click',function() {
	var newActionBar = $(this).closest('.newActionBar');
	var newActionTxt = $('.newActionTxt',newActionBar).val();
	var newActionVal = $('.newActionVal',newActionBar).val();
	var reportData =$(this).closest('.reportData');
	var reportDataUL = $('.reportDataUL',reportData);
	if(newActionTxt == "")
	{
		
	}
	else
	{
	$(reportDataUL).append('<li class="reportDataLi" style=""><div class="float-left reportActualName" style="width: 280px;padding: 10px 15px 8px 15px;">'+newActionTxt.toTitleCase()+'</div><input class="float-left reportActualData" style="width:65px;" type="text" value="'+newActionVal+'"/><div class="rep_sprite reportDeleteIconDiv float-right"><span class="rep_sprite_backup  reportDeleteIcon" ></span></div></li>');
	}
});

$(".reportDeleteIcon").live('click',function() {
	$(this).closest('.reportDataLi').remove();
});
$(".reportDeleteIcon").live('hover',function() {
	$(this).closest('.reportDataLi').addClass('warn');
	//return false;
});
$(".reportDeleteIcon").live('mouseout',function() {
	$(this).closest('.reportDataLi').removeClass('warn');
	//return false;
});

$(".reportEdit").live('blur',function() {
	var reportDataLi = $(this).closest('.reportDataLi');
	$('.reportActualData',reportDataLi).val($(this).val());
});

$('.reportSelect').live('click',function(){
	var action = $(this).text();
	if(action == "All")
	{
		$('.selectReport').each(function(){
			if(!($(this).hasClass('active')))
			{
				$(this).addClass('active');
			}
		});
	}
	else if(action == "None")
	{
		$('.selectReport').each(function(){
			if($(this).hasClass('active'))
			{
				$(this).removeClass('active');
			}
		});
	}
	return false;
});
$('.title_collapse').live('click',function(){
	return false;
});
$('.combineAllReport').live('click',function(){
	if($(this).hasClass('no'))
	{
		$(this).removeClass('no');
		$(this).addClass('yes');
		$(this).addClass('active');
		$(this).attr('var', 'yes');
	}
	else
	{
		$(this).removeClass('yes');
		$(this).addClass('no');
		$(this).removeClass('active');
		$(this).attr('var', 'no');
	}
	return false;
});

$('.downPDF').live('click',function(){
	/*$('.reportingFormClass').html('<input type="hidden" class="hiddenReport" id="hiddenReportInput" name="" value=""/><input   type="submit" class="rep_sprite " style="" id="downloadPDF"/>');
	$('.reportData').each(function(key,val){
		var reportSiteID = $(this).attr("attr");
		//tempArray['requiredData']['clientReportingGeneratePdf']['siteID'][reportSiteID]={};
		$('.reportDataLi',this).each(function(){
			//tempArray['requiredData']['clientReportingGeneratePdf']['siteID'][reportSiteID][$('.reportActualName',this).text()] = $('.reportActualData',this).text();
			$('.hiddenReport').before('<input type="hidden" class="" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][siteID]['+reportSiteID+']['+$(".reportActualName",this).text()+']" value='+$('.reportActualData',this).val()+'>')
		});
		
	});
	//$('.hiddenReport').after('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][reportTitle]" value="'+$('.report_title').val()+'">');
	$('.hiddenReport').before('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][headerContent]" value="'+$('.headerContent textarea').val()+'">');
	//$('.hiddenReport').after('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][privateNote]" value="'+$('.privateNote textarea').val()+'">');
	$('.hiddenReport').before('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][footerContent]" value="'+$('.footerContent textarea').val()+'">');
	$('.hiddenReport').before('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][clientNote]" value="'+$('.clientNote textarea').val()+'">');
	$('#downloadPDF').click();*/
	downloadPDF_report();
	$('.hiddenReport').before('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][dateRange]" value="'+$('.reportDate').text()+'">');
	$('.hiddenReport').before('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][generatePdfAndSendMail]" value="0">');
	$('.hiddenReport').before('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][reportType]" value="manual">');
	$('#downloadPDF').click();
});
$('#sendFinalEmail_report').live('click',function(){
	    
		var emailArray = {};
		var fromName_report=$('#fromName_report').val();
		var fromEmail_report=$('#fromEmail_report').val();
		var toEmail=$('#toEmail_report').val();
		var fromMessage_report=$('#fromMessage_report').val();
		var fromSubject_report=$('#fromSubject_report').val();
		var tempArray = {};
		tempArray['requiredData'] = {};
		tempArray['requiredData']['clientReportingGeneratePdf'] = {};
		tempArray['requiredData']['clientReportingGeneratePdf']['siteID'] = {};
		if(toEmail == "")
		{
			$('#toEmail_report').qtip({id:"toggleGroupQtip",content: { text: 'Please enter Email Address' }, position: { my: 'bottom center', at: 'top center',  adjust:{ y: -6} }, show: {  }, hide: {  }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 10, height:5} } });
			$('#toEmail_report').addClass('error_report');
		}
	else
	{
		//downloadPDF_report();
		$(this).append('<div class="btn_loadingDiv left"></div>');
		$(this).addClass('disabled');
		tempArray['requiredData']['clientReportingGeneratePdf']['headerContent'] = $('.headerContent textarea').val();
		tempArray['requiredData']['clientReportingGeneratePdf']['footerContent'] = $('.footerContent textarea').val();
		tempArray['requiredData']['clientReportingGeneratePdf']['clientNote'] = $('.clientNote textarea').val();
		tempArray['requiredData']['clientReportingGeneratePdf']['siteID'] = {};
		$('.reportData').each(function(key,val){
			var reportSiteID = $(this).attr("attr");
			tempArray['requiredData']['clientReportingGeneratePdf']['siteID'][reportSiteID]={};
			$('.reportDataLi',this).each(function(){
				tempArray['requiredData']['clientReportingGeneratePdf']['siteID'][reportSiteID][$('.reportActualName',this).text()] = $('.reportActualData',this).val();
			});
		});
		tempArray['requiredData']['clientReportingGeneratePdf']['email'] = {};
		tempArray['requiredData']['clientReportingGeneratePdf']['email']['fromName'] = fromName_report;
		tempArray['requiredData']['clientReportingGeneratePdf']['email']['toEmail'] = toEmail;
		tempArray['requiredData']['clientReportingGeneratePdf']['email']['fromEmail'] = fromEmail_report;
		tempArray['requiredData']['clientReportingGeneratePdf']['email']['message'] = fromMessage_report;
		tempArray['requiredData']['clientReportingGeneratePdf']['email']['subject'] = fromSubject_report;
		tempArray['requiredData']['clientReportingGeneratePdf']['dateRange'] = $('.reportDate').text();
		tempArray['requiredData']['clientReportingGeneratePdf']['generatePdfAndSendMail'] = 1;
		tempArray['requiredData']['clientReportingGeneratePdf']['reportType'] = 'manual';
		$('#mainReport').click();
		//$('#downloadPDF').click();
		$('#loadingDiv').show();
		doCall(ajaxCallPath,tempArray,'processManualSendEmail_report','json',"none");
		/*var tempArray={};
		tempArray['requiredData'] = {};
		tempArray['requiredData']['clientReportingManualReportViewHTML']=1;
		tempArray['requiredData']['clientReportingGetHeaderFooterCont']=1;
		doCall(ajaxCallPath,tempArray,'loadAllManualScheduleReports','json',"none");*/
	}
});

$('.report_title').live('click',function(){
	if($(this).hasClass('error_report'))
	{
		$(this).removeClass('error_report');
	}
});

$('.reportDataLi.warn').live('hover',function(){
	$(this).removeClass('warn');
	
});
$('#clientReport_SendEmail').live('click',function(){
	$(this).hide();
	$('.emailContent_report').show();
});

$('.createNewScheduleReport').live('click',function(){
	createReportingMainPage();
	$('#mainReport').removeClass('active');
	$('#scheduleReport').addClass('active');
	$('.clientReportPageTitle').text('SCHEDULE REPORT');
	$('.selectDate_report').text('SELECT SCHEDULE TIME');
	$('.reportOutline #widgetCalendar').after('<div class="float-left" style="margin: 10px;"><div class="label" style="font-weight: 700;font-size: 11px;">WHEN?</div> <ul class="btn_radio_slelect">  <li><a class="rep_sprite optionSelect whenType " var="week" id="weeklySchedule_report">Weekly</a></li> <li><a class="rep_sprite optionSelect whenType whenType_month" var="month" id="monthlySchedule_report">Monthly</a></li> </ul> <div class="clear-both"></div> <div class="monthly weekly daily whenArgs" style="margin: -25px 0px 0px 126px;"><span style="float:left; padding:5px 10px 5px 27px;">at</span> <div class="time_select_btn rep_sprite timeSelectBtn_report" id="timeSelectBtn"><a timeval="0">12 am</a></div> <div class="time_select_options" style="display: none;"> <ul class="time_select_options_am"> <div class="label">am</div> <li><a std="am" timeval="0">12</a></li> <li><a std="am" timeval="1">1</a></li> <li><a std="am" timeval="2">2</a></li> <li><a std="am" timeval="3">3</a></li> <li><a std="am" timeval="4">4</a></li> <li><a std="am" timeval="5">5</a></li> <li><a std="am" timeval="6">6</a></li> <li><a std="am" timeval="7">7</a></li> <li><a std="am" timeval="8">8</a></li> <li><a std="am" timeval="9">9</a></li> <li><a std="am" timeval="10">10</a></li> <li><a std="am" timeval="11">11</a></li> <div class="clear-both"></div> </ul> <ul class="time_select_options_pm"> <div class="label">pm</div> <li><a std="pm" timeval="12">12</a></li> <li><a std="pm" timeval="13">1</a></li> <li><a std="pm" timeval="14">2</a></li> <li><a std="pm" timeval="15">3</a></li> <li><a std="pm" timeval="16">4</a></li> <li><a std="pm" timeval="17">5</a></li> <li><a std="pm" timeval="18">6</a></li> <li><a std="pm" timeval="19">7</a></li> <li><a std="pm" timeval="20">8</a></li> <li><a std="pm" timeval="21">9</a></li> <li><a std="pm" timeval="22">10</a></li> <li><a std="pm" timeval="23">11</a></li> <div class="clear-both"></div> </ul> </div> </div><div style="margin-top: 35px; display: none;" class="monthly whenArgs"><span style="float:left; padding:15px 10px 5px;">every</span> <div class="date_select_cont float-left" style="margin: 10px 0px 0px 3px;"> <ul> <li><a class="rep_sprite active selectDate_report">1</a></li> <li><a class="rep_sprite selectDate_report">2</a></li> <li><a class="rep_sprite selectDate_report">3</a></li> <li><a class="rep_sprite selectDate_report">4</a></li> <li><a class="rep_sprite selectDate_report">5</a></li> <li><a class="rep_sprite selectDate_report">6</a></li> <li><a class="rep_sprite selectDate_report">7</a></li> <div class="clear-both"></div> </ul> <ul> <li><a class="rep_sprite selectDate_report">8</a></li> <li><a class="rep_sprite selectDate_report">9</a></li> <li><a class="rep_sprite selectDate_report">10</a></li> <li><a class="rep_sprite selectDate_report">11</a></li> <li><a class="rep_sprite selectDate_report">12</a></li> <li><a class="rep_sprite selectDate_report">13</a></li> <li><a class="rep_sprite selectDate_report">14</a></li> <div class="clear-both"></div> </ul> <ul> <li><a class="rep_sprite selectDate_report">15</a></li> <li><a class="rep_sprite selectDate_report">16</a></li> <li><a class="rep_sprite selectDate_report">17</a></li> <li><a class="rep_sprite selectDate_report">18</a></li> <li><a class="rep_sprite selectDate_report">19</a></li> <li><a class="rep_sprite selectDate_report">20</a></li> <li><a class="rep_sprite selectDate_report">21</a></li> <div class="clear-both"></div> </ul> <ul> <li><a class="rep_sprite selectDate_report">22</a></li> <li><a class="rep_sprite selectDate_report">23</a></li> <li><a class="rep_sprite selectDate_report">24</a></li> <li><a class="rep_sprite selectDate_report">25</a></li> <li><a class="rep_sprite selectDate_report">26</a></li> <li><a class="rep_sprite selectDate_report">27</a></li> <li><a class="rep_sprite selectDate_report">28</a></li> <div class="clear-both"></div> </ul> <ul> <li><a class="rep_sprite selectDate_report">29</a></li> <li><a class="rep_sprite selectDate_report">30</a></li> <li><a class="rep_sprite selectDate_report">31</a></li> <li><a class="rep_sprite empty">&nbsp;</a></li> <li><a class="rep_sprite empty">&nbsp;</a></li> <li><a class="rep_sprite empty">&nbsp;</a></li> <li><a class="rep_sprite empty">&nbsp;</a></li> <div class="clear-both"></div> </ul> </div><div class="clear-both"></div><div style="padding: 10px;width: 270px;margin-left: 42px;">Report will be generated for the previous 30 days</div> <div class="clear-both"></div> </div><div style="margin-top: 45px; display: none;" class="weekly whenArgs"><span style="float:left; padding:5px 10px;">every</span> <div class="day_select_cont float-left"> <ul> <li><a class="rep_sprite active optionSelect" dayval="1">MON</a></li> <li><a class="rep_sprite optionSelect" dayval="2">TUE</a></li> <li><a class="rep_sprite optionSelect" dayval="3">WED</a></li> <li><a class="rep_sprite optionSelect" dayval="4">THU</a></li> <li><a class="rep_sprite optionSelect" dayval="5">FRI</a></li> <li><a class="rep_sprite optionSelect" dayval="6">SAT</a></li> <li><a class="rep_sprite optionSelect" dayval="7">SUN</a></li> </ul> </div> <div class="clear-both"></div><div style="padding: 10px;width: 270px;margin-left: 42px;">Report will be generated for the previous 7 days</div></div> </div><div class="clear-both"></div>');
	$('.whenType_month').click();
	$('.emailContent_scheduleReport').show();
	$('.reportOutline #widgetCalendar').hide();
	$('#generateReport').hide();
	$('.combineSitesTab').hide();
	$('#scheduleReportSubmit').show();
	$('.report_title').show();
	//$('#scheduleReportClose').show();
});

$('#scheduleReportSubmit').live('click',function(){
	
	loadscheduleReportSubmit();
	/*var sTime=$(".reportOutline #timeSelectBtn a").attr('timeval');
	var week_or_month = $('.reportOutline .whenType.active').text().toLowerCase();
	var clientReportDate = '',week_month_val='';
	if(week_or_month == 'weekly')
	{
		clientReportDate = sTime+'|'+$(".reportOutline .day_select_cont .optionSelect.active").attr('dayval');
		week_month_val = $(".reportOutline .day_select_cont .optionSelect.active").attr('dayval');
	}
	else
	{
		clientReportDate = sTime+'|'+$(".reportOutline .date_select_cont .selectDate_report.active").text();
		week_month_val = $(".reportOutline .date_select_cont .selectDate_report.active").text();
	}
	var actions = {},PTCaction={},i=0,tempArray={};
	tempArray['requiredData'] = {};
	var sites = getSelectedSites();
	$('.actions .selectReport.active').each(function(){
		
		actions[i] = $(this).attr("val");
		if($(this).attr("action") != "")
		PTCaction[i] = $(this).attr("action");
		i++;
	});
	var profileName_cr = $('.report_title').val();
	var profileName_cr = $('.report_title').val();
	if(profileName_cr == "")
	{
		$('.report_title').qtip({id:"toggleGroupQtip",content: { text: 'Please Enter Report Title' }, position: { my: 'bottom center', at: 'top center',  adjust:{ y: -6} }, show: {  }, hide: {  }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 10, height:5} } });
		$('.report_title').addClass('error_report');
	}
	else
	{
		var emailArray = {};
		emailArray['fromName']=$('#fromName_report').val();
		emailArray['fromEmail']=$('#fromEmail_report').val();
		emailArray['toEmail']=$('#toEmail_report').val();
		emailArray['message']=$('#fromMessage_report').val();
		emailArray['subject']=$('#fromSubject_report').val();
		tempArray['requiredData']['clientReportingSchedule']={};
		tempArray['requiredData']['clientReportingSchedule']['profileName'] = profileName_cr;
		tempArray['requiredData']['clientReportingSchedule']['headerContent']=$('.headerContent textarea').val();
		tempArray['requiredData']['clientReportingSchedule']['footerContent']=$('.footerContent textarea').val();
		tempArray['requiredData']['clientReportingSchedule']['clientNote']=$('.clientNote textarea').val();
		tempArray['requiredData']['clientReportingSchedule']['combinedSitesReport']=$('.combineAllReport').attr("var");
		tempArray['requiredData']['clientReportingSchedule']['siteID']={};
		tempArray['requiredData']['clientReportingSchedule']['type']={};
		tempArray['requiredData']['clientReportingSchedule']['action']={};
		tempArray['requiredData']['clientReportingSchedule']['siteID']=sites;
		tempArray['requiredData']['clientReportingSchedule']['type']=actions;
		tempArray['requiredData']['clientReportingSchedule']['action']=PTCaction;
		tempArray['requiredData']['clientReportingSchedule']['period']=week_or_month;
		tempArray['requiredData']['clientReportingSchedule']['schedule']=clientReportDate;
		tempArray['requiredData']['clientReportingSchedule']['email']=emailArray;
		tempArray['requiredData']['clientReportingSchedule']['week_month_val']= week_month_val;
		tempArray['requiredData']['clientReportingSchedule']['schedule_time']= sTime;
		tempArray['requiredData']['clientReportingSchedule']['logoName']=$('.imageSpace').attr("imgSrc");
		doCall(ajaxCallPath,tempArray,'processScheduleReport','json',"none");
	}*/
});

$(".timeSelectBtn_report").live('click',function() {
	if((typeof scheduleAddonFlag == 'undefined')||(scheduleAddonFlag!=1))
    $(".time_select_options").toggle();
return false;
});

$(".time_select_options a").live('click',function() {
	$("#timeSelectBtn a").text($(this).text()+" "+$(this).attr('std')).attr('timeval',$(this).attr('timeval'));
	$(".time_select_options").hide();
	
});
$(".selectDate_report").live('click',function() {
$(".selectDate_report").removeClass('active');
$(this).addClass('active');
});
$(".whenType").live('click',function() {
	$(".whenArgs").hide();
	$("."+$(this).text().toLowerCase()).show();
	

});
/*$("#sendFinalEmail_report").live('click',function() {
	$('#fromName_report').val();
	$('#fromEmail_report').val();
	$('#toEmail_report').val();
	$('#fromMessage_report').val();
	
});*/
$("#closeEmail_report").live('click',function() {
	$('#clientReport_SendEmail').show();
	$('.emailContent_report').hide();
});

$("#scheduleReportClose").live('click',function() {
	//loadReportingSchedulePage();
	$(this).append('<div class="btn_loadingDiv left"></div>');
	$(this).addClass('disabled');
	if($('#scheduleReport').hasClass('active'))
	{
		var tempArray = {};
		tempArray['requiredData'] = {};
		tempArray['requiredData']['clientReportingScheduleReportViewHTML']=1;
		doCall(ajaxCallPath,tempArray,'loadAllScheduleReports','json',"none");
	}
	else
	{
		var tempArray={};
		tempArray['requiredData'] = {};
		tempArray['requiredData']['clientReportingManualReportViewHTML']=1;
		doCall(ajaxCallPath,tempArray,'loadAllManualScheduleReports','json',"none");
	}
});

$(".createNewManualReport").live('click',function() {
	createReportingMainPage();
	
});
$(".removeScheduleReport").live('click',function() { 

var closestVar=$(this).closest('.ind_row_cont');
$(".row_summary",closestVar).addClass('del');
$(".row_summary_report",closestVar).addClass('del');
$(".row_detailed",closestVar).addClass('del');
$(".row_summary .delConfHide,.row_detailed .rh .delConfHide ",closestVar).hide();
$(".row_summary_report .delConfHide,.row_detailed .rh .delConfHide ",closestVar).hide();
return false;
	
});
$(".removeManualReport").live('click',function() { 
var closestVar=$(this).closest('.ind_row_cont');
$(".row_summary",closestVar).addClass('del');
$(".row_detailed",closestVar).addClass('del');
$(".row_summary .delConfHide,.row_detailed .rh .delConfHide ",closestVar).hide();
return false;
});

$('.scheduleDeleteReport').live('click',function() {
	//loadReportingMainPage();
	var closestUpdatee=$(closestVar).closest('.row_updatee');
	var topVar=$(this).closest('.ind_row_cont');
	if($(this).hasClass('no'))
	{
	   var  topVar=".ind_row_cont .row_summary, .row_detailed";
	   var closestVar=$(this).closest('.ind_row_cont');
	   $(topVar).removeClass('del');
	   $(".delConfHide",closestVar).show();
	}
	else
	{
		var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['clientReportingScheduleReportDelete']={};
		tempArray['requiredData']['clientReportingScheduleReportDelete']['clientReportScheduleID']=$(this).attr('schedulereportkey');
		$(topVar).remove();
		if($(this).attr('schedulereportkey')==undefined ||  $(this).attr('schedulereportkey')=='') //SB mod
		{
			if($(".row_updatee_ind",closestUpdatee).length==0)
			$($(closestUpdatee).closest('.ind_row_cont').remove());
		}
		doHistoryCall(ajaxCallPath,tempArray,"");
		//doCall(ajaxCallPath,tempArray,'processscheduleDeleteReport','json',"none");
	}
	return false;
});

$('.manualDeleteReportPdf').live('click',function() {
	//loadReportingMainPage();
	var closestUpdatee=$(closestVar).closest('.row_updatee');
	var topVar=$(this).closest('.ind_row_cont');
	if($(this).hasClass('no'))
	{
	   var  topVar=".ind_row_cont .row_summary, .row_detailed";
	   var closestVar=$(this).closest('.ind_row_cont');
	   $(topVar).removeClass('del');
	   $(".delConfHide",closestVar).show();
	}
	else
	{
		var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['clientReportingDeleteSentPdf']={};
		tempArray['requiredData']['clientReportingDeleteSentPdf']['sentPdfID']=$(this).attr('manualreportpdfkey');
		tempArray['requiredData']['clientReportingDeleteSentPdf']['reportType']=$(this).attr('reportType');
		$(topVar).remove();
		if($(this).attr('schedulereportkey')==undefined ||  $(this).attr('schedulereportkey')=='') //SB mod
		{
			if($(".row_updatee_ind",closestUpdatee).length==0)
			$($(closestUpdatee).closest('.ind_row_cont').remove());
		}
		doHistoryCall(ajaxCallPath,tempArray,"");
		//doCall(ajaxCallPath,tempArray,'processscheduleDeleteReport','json',"none");
	}
	return false;
});

$('.scheduleDeleteReportPdf').live('click',function() {
	//loadReportingMainPage();
	var closestUpdatee=$(this).closest('.row_updatee');
	var topVar=$(this).closest('.row_updatee_ind');
	if($(this).hasClass('no'))
	{
	  // var  topVar=".ind_row_cont .row_summary, .row_detailed";
	  // var closestVar=$(this).closest('.ind_row_cont');
	   $('.item_ind',topVar).removeClass('del');
	   $(".delConfHide",topVar).show();
	}
	else
	{
		var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['clientReportingDeleteSentPdf']={};
		tempArray['requiredData']['clientReportingDeleteSentPdf']['sentPdfID']=$(this).attr('schedulereportpdfkey');
		tempArray['requiredData']['clientReportingDeleteSentPdf']['reportType']=$(this).attr('reportType');
		$(topVar).remove();
		if($(this).attr('schedulereportkey')==undefined ||  $(this).attr('schedulereportkey')=='') //SB mod
		{
			if($(".row_updatee_ind",closestUpdatee).length==0)
			$($(closestUpdatee).closest('.ind_row_cont').remove());
		}
		doHistoryCall(ajaxCallPath,tempArray,"");
		//doCall(ajaxCallPath,tempArray,'processscheduleDeleteReport','json',"none");
	}
	return false;
});
$(".cc_schedule_report_mask").live('click',function() { 

var closest=$(this).closest(".ind_row_cont");
var tempArray={};
tempArray['requiredData']={};
tempArray['requiredData']['clientReportingScheduleReportPause']={};
tempArray['requiredData']['clientReportingScheduleReportPause']['clientReportScheduleID']=$(this).attr('schedulereportkey');
if($(".cc_schedule_img",this).hasClass('on'))
tempArray['requiredData']['clientReportingScheduleReportPause']['paused']=1;
else
tempArray['requiredData']['clientReportingScheduleReportPause']['paused']=0;
doCall(ajaxCallPath,tempArray,'');
//doCall(ajaxCallPath,tempArray,'processscheduleDeleteReport','json',"none");
$(".cc_schedule_img",closest).toggleClass("on off");
return false;
});

$(".editScheduleReport").live('click',function() { 
	var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['clientReportingGetSchedule']={};
	tempArray['requiredData']['clientReportingGetSchedule']['clientReportScheduleID']=$(this).attr("schedulereportkey");
	doCall(ajaxCallPath,tempArray,'loadEditScheduleReportPage');
	return false;
});

$('#reScheduleReportSubmit').live('click',function() {
	loadscheduleReportSubmit('reSchedule');
});

$(".run_now_report").live('click',function() { 
	$('#loadingDiv').show();
	row_summary_expand_var = $(this).attr("schedulereportkey");
	var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['clientReportingScheduleReportRunNow']={};
	tempArray['requiredData']['clientReportingScheduleReportRunNow']['clientReportScheduleID']=$(this).attr("schedulereportkey");
	tempArray['requiredData']['clientReportingScheduleReportRunNow']['runType']=$(this).attr("runType");
	//doCall(ajaxCallPath,tempArray,'');
	doCall(ajaxCallPath,tempArray,'processsRunNowReport','json',"none");
	return false;
});

$('.removeManualReport').live('hover',function() { 
	var row_summary = $(this).closest('.row_summary');
	if($(row_summary).hasClass('disabled'))
	{
		$(row_summary).removeClass('disabled');
	}
	else
	{
		$(row_summary).addClass('disabled');
	}
});

$('.manualDeleteReportPdf').live('hover',function() {
	var row_summary = $(this).closest('.row_summary');
	if($(row_summary).hasClass('disabled'))
	{
		$(row_summary).removeClass('disabled');
	}
	else
	{
		$(row_summary).addClass('disabled');
	}
});

/*$('.manualReportRowSummary').live('click',function() {
	
	expandThis(this,'summary');
});*/
$('.manualReportRowSummary .row_action').live('click',function() {
	
	$(this).closest('.manualReportRowSummary').after('<a class="hiddenViewReport_manual" style="display:none" href='+$(this.attr("href"))+' target="_blank" ></a>');
	$('.hiddenViewReport_manual').click();
	$('.hiddenViewReport_manual').remove();
	return false;
	//$(this).closest('.row_summary').show();
});

$('.manualReportRowSummary .row_action').live('hover',function() {
	var row_summary = $(this).closest('.row_summary');
	if($(row_summary).hasClass('disabled'))
	{
		$(row_summary).removeClass('disabled');
	}
	else
	{
		$(row_summary).addClass('disabled');
	}
});
$('.op_cont.error_report').live('hover',function() {
	$(this).removeClass('error_report');
});
$('.textbox_report.error_report').live('click',function(){
	$(this).removeClass('error_report');
});
$('.reportsButton').live('click',function(){
	if($('#mainReport').hasClass('active'))
	{
		$('#loadingDiv').show();
		var tempArray={};
		tempArray['requiredData'] = {};
		tempArray['requiredData']['clientReportingManualReportViewHTML']=1;
		tempArray['requiredData']['clientReportingGetHeaderFooterCont']=1;
		doCall(ajaxCallPath,tempArray,'loadAllManualScheduleReports','json',"none");
	}
	if($('#scheduleReport').hasClass('active'))
	{
		$('#loadingDiv').show();
		var tempArray={};
		tempArray['requiredData'] = {};
		tempArray['requiredData']['clientReportingScheduleReportViewHTML']=1;
		tempArray['requiredData']['clientReportingGetHeaderFooterCont']=1;
		doCall(ajaxCallPath,tempArray,'loadAllScheduleReports','json',"none");
	}
});
