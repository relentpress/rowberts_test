var isClientReport = 1;
function showReportingOptions(){ 
	if(!isSiteSelected())
	{
		$(".reportOutline").hide();
		$(".optionsContent").hide();
		$(".reportContent").hide();
	}
	else
	{  
	    $(".reportOutline").show();
		$(".optionsContent").show();// For manage / install panel
		$(".reportContent").html('');
	}
}