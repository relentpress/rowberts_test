var imgSrc='',imgSrcName='',pdfPath="addons/clientReporting/pdfGenerate.php",headerContent_source='',footerContent_source='',row_summary_expand_var='';
function loadReportingMainPage()
{
	$('#loadingDiv').show();
	var tempArray={};
	tempArray['requiredData'] = {};
	tempArray['requiredData']['clientReportingManualReportViewHTML']=1;
	tempArray['requiredData']['clientReportingGetHeaderFooterCont']=1;
	doCall(ajaxCallPath,tempArray,'loadAllManualScheduleReports','json',"none");
}
function createReportingMainPage()
{
	/*var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['clientReportingGeneratePdf']={};
	tempArray['requiredData']['clientReportingGeneratePdf']['content'] = {};*/ 
	var content='<div class="site_nav_sub"> <ul> <li><a class="optionSelect active" id="mainReport">Manual</a></li> <li><a class="optionSelect" id="scheduleReport">Scheduled</a></li> <div class="clear-both"></div> </ul> </div><div class="reportsTypeSelector"><ul class="btn_rounded_gloss"> <li><a class="rep_sprite optionSelect reportsButton" utype="" onclick="">Reports</a></li> <li><a class="rep_sprite optionSelect createNewReportActive active " utype="" onclick="">Create New</a></li> </ul></div><div class="steps_hdr ">Select websites to <span id="processType">Create</span> <span class="itemUpper clientReportPageTitle">Report</span></div><div class="btn_action float-left" style="margin: -37px 0px 0px 886px;"><a class="rep_sprite " style="display:none" id="scheduleReportClose">Close</a></div><div class="siteSelectorContainer">'+siteSelectorVar+'</div></div> <div class="result_block shadow_stroke_box siteSearch bulk_publish_page_post reportOutline" style="border:0;box-shadow: 0 0 0  rgba(0, 0, 0, 0)"><div class="optionsContent " style="display:block"></div><div class="reportContent " style="display:none;border: 1px solid #D2D5D7; margin-top:20px;"></div>';
	$("#pageContent").html(content);
	/*$(".optionsContent").html('<div style="padding:10px 0;"> <table  border="0" class="bulk_post_form_table history"> <tr>  <td ><ul class="checkbox_filters float-right"> <li><a class="rep_sprite revisions selectOptimize active" status="on" type="revisions" key="clean-revisions">Remove all Post Revisions</a></li> <li><a class="rep_sprite autodraft selectOptimize "status="" type="autodraft" key="clean-autodraft">Remove all auto Draft posts</a></li> <li><a class="rep_sprite spam selectOptimize " type="spam" key="clean-comments" status="">Clear "Spam" comments</a></li> <li><a class="rep_sprite unapproved selectOptimize" type="unapproved" key="unapproved-comments" status="">Clear Unapproved Comments</a></li> <li><a class="rep_sprite db_optimize selectOptimize" key="optimize-db" type="optimize-db" status="">Optimize database Tables</a></li></ul></td> </tr> <tr> <td>&nbsp;</td> <td><div class="btn_action float-left fetchPostCont" ><a class="rep_sprite loadOptimize " style="margin: 8px 30px 8px 0px; width: 64px;">Optimize</a></div></td> </tr> </table> <div class="clear-both"></div><div class="shadow_stroke_box response responseFullDiv" style="display:none"> <div class="th rep_sprite"><div class="title"><span class="droid700">Response</span></div></div><div id="responseCont"></div></div></div>');*/
	$(".optionsContent").html('<div class="th rep_sprite"><div class="title"><span class="droid700">CLIENT REPORTING</span></div></div>   <div class="add_post_left_cont"><input name="" maxLength="70" type="text" class="report_title" style="display:none" placeholder="Enter profile name for schedule report"><input class="hiddenReportID" type="hidden" reportId=""><ul id="sortableLeft"><li style="width:358px;" class="float-left"> <div class="op_cont_outer " style=""> <div class="title_collapse rep_sprite disabled"><span class="collapse_arrow selectDate_report">SELECT DATE RANGE</span><div class="clear-both"></div></div> <div class="op_cont" style="border: 0px;margin: 0px 0px -2px 0px;"> <div class="" id="widgetCalendar" style="margin: 16px 79px;"></div><input type="hidden" class="reportDate"/> </div></div></li><li style="width:358px;border-left: 1px solid #D2D5D7;" class="float-left"> <div class="op_cont_outer " style=""> <div class="title_collapse rep_sprite disabled"><span class="collapse_arrow">Upload Logo</span><div class="clear-both"></div></div> <div class="op_cont" style="padding:11px;border-bottom: 0;"><div class="imgWrap_report"><div class="" style="display: table-cell;width: 105px;"></div><div class="imageSpace" imgSrc='+imgSrcName+'></div>  <div id="imageUploaderContent"></div> </div><div style="text-align: center; padding:10px;">Image will not be cropped or resized</div></div></div></li><li style="width:358px;border-right: 1px solid #D2D5D7;" class="float-left"> <div class="op_cont_outer " style=""> <div class="title_collapse rep_sprite disabled"><span class="collapse_arrow">HEADER CONTENT</span><div class="clear-both"></div></div> <div class="op_cont headerContent">  <textarea name="" cols="" rows="" style="height: 60px;width: 91%;"></textarea> </div></div></li><li style="width:358px;display:none;" class="float-left" > <div class="op_cont_outer " style=""> <div class="title_collapse rep_sprite disabled"><span class="collapse_arrow">PRIVATE NOTE</span><div class="clear-both"></div></div> <div class="op_cont privateNote" >  <textarea name="" cols="" rows="" style="height: 60px;width: 302px"></textarea> </div></div></li><li style="width:358px;" class="float-left"> <div class="op_cont_outer " style=""> <div class="title_collapse rep_sprite disabled"><span class="collapse_arrow">FOOTER CONTENT</span><div class="clear-both"></div></div> <div class="op_cont footerContent">  <textarea maxLength="80" placeholder="Max 80 Characters" name="" cols="" rows="" style="height: 60px;width: 91%"></textarea> </div></div></li><li style="width:100%;" class="float-left"> <div class="op_cont_outer " style=""> <div class="title_collapse rep_sprite disabled "><span class="collapse_arrow">CLIENT NOTE</span><div class="clear-both"></div></div> <div class="op_cont clientNote">  <textarea name="" cols="" rows="" style="height: 60px;width: 687px"></textarea> </div></div></li><li style="width:100%;display:none" class="float-left emailContent_scheduleReport"> <div class="op_cont_outer " style=""> <div class="title_collapse rep_sprite disabled "><span class="collapse_arrow">EMAIL CONTENT</span><div class="clear-both"></div></div> <div class="op_cont emailContent_report"> <span class="">FROM</span> <input name="" type="text" class="textbox_report" style="" placeholder="Name" id="fromName_report"><input name="" type="text" class="textbox_report"  style="margin-left: 0;" placeholder="Email" id="fromEmail_report"><span class="">TO </span><input name="" type="text" class="textbox_report" style="width: 150px;" placeholder="Email" id="toEmail_report"><div class="clear-both"></div> <span class="">SUBJECT</span><input name="" class="textbox_report" cols="" rows="" style="width: 685px" id="fromSubject_report"></input><span class="">MESSAGE</span><textarea name="" cols="" rows="" style="height: 60px;width: 687px" id="fromMessage_report"></textarea> </div></div></li></ul></div>        <div class="add_post_right_cont">  <div class="op_cont_outer"><div class="title_collapse rep_sprite "><div class="collapse_arrow">Actions</div> <div class="clear-both"></div><div class="select_cont_report" style=""><span>Select: </span><a class="reportSelect">All</a><a class="reportSelect">None</a></div></div> <div class="op_cont"><ul id="sortableRight"><li class="op_cont_outer actions"><div class="checkbox selectReport active " val="backup" action="">Backup</div><div class="checkbox selectReport active" val="PTC" action="core">WordPress updated</div><div class="checkbox selectReport active" val="PTC"  action="plugin">Plugins updated</div><div class="checkbox selectReport active" val="PTC" action="themes">Themes updated</div><div class="checkbox selectReport cmt_checkbox_report active" val="comments" action="">Comments managed</div><div class="checkbox selectReport active ppl_checkbox_report" val="posts" action="">Posts managed</div><div class="checkbox selectReport ppl_checkbox_report active" val="pages"  action="">Pages managed</div><div class="checkbox selectReport ppl_checkbox_report active" val="links" action="" >Links managed</div><div class="checkbox selectReport active gg_checkbox_report"  val="googleAnalytics" action="">Google Analytics</div><div class="checkbox selectReport active ms_checkbox_report"  val="malwareScanningSucuri" action="">Malware scan</div></li></ul></div> </div><div class="op_cont_outer combineSitesTab"><div class="title_collapse rep_sprite " style="margin-bottom: 1px;"><div class="collapse_arrow">Combine reports</div> <div class="clear-both"></div></div> <div class="op_cont"><ul id="sortableRight"><li class="op_cont_outer actions"><div class="checkbox combineAllReport no" val="links" action="">Combine all sites</div></li></ul></div> </div>   </div><div class="clear-both"></div><div class="th rep_sprite" style="border-top: 1px solid #D2D5D7; border-bottom:0;"><div class="save_snippet_form"></div><div class="btn_action float-right" ><a class="rep_sprite " style="display:none" id="reScheduleReportSubmit">Reschedule Report</a></div><div class="btn_action float-right"><a class="rep_sprite " style="display:none" id="scheduleReportSubmit">Schedule Report</a></div><div class="btn_action float-right"><a class="rep_sprite " style="" id="generateReport">Generate Report</a></div> </div>');
	$('.imageSpace').html(imgSrc);
	
	createImgUploader();
	var now3 = new Date();
				now3.addDays(-4);
				var now4 = new Date();
				//$('.reportDate').text(now3+'-'+now4);
$('#widgetCalendar').DatePicker({
					flat: true,
					format: 'b d, Y',
					date: [],
					calendars:1,
					mode: 'range',
					starts: 1,
					onChange: function(formated)
					 {
							$('.reportDate').get(0).innerHTML = formated.join(' - ');
							//$("#widgetField #dateContainer").append('<div class="cal_arrow"></div>');
					 }
				});
	/*if((typeof isGoogle != 'undefined')&&(isGoogle == '1'))	
	{
		$('.gg_checkbox_report').remove();
	}*/
	var malCheckCount = 0,ggCheckCount = 0,pplCheckCount = 0,cmtCheckCount = 0;
	$('.navLinks').each(function(k,v){
		if($(this).attr('page') == 'malwareSecurity')
		{
			malCheckCount = 1;	
		}
		if($(this).attr('page') == 'googleAnalytics')
		{
			ggCheckCount = 1;
		}
		if($(this).attr('page') == 'posts')
		{
			pplCheckCount = 1;
		}
		if($(this).attr('page') == 'comments')
		{
			cmtCheckCount = 1;
		}
	});
	if(malCheckCount == 0)
	{
		$('.ms_checkbox_report').remove();
	}
	if(pplCheckCount == 0)
	{
		$('.ppl_checkbox_report').remove();
	}
	if(ggCheckCount == 0)
	{
		$('.gg_checkbox_report').remove();
	}
	if(cmtCheckCount == 0)
	{
		$('.cmt_checkbox_report').remove();
	}
	$('.reportOutline').hide();		
	$('.optionsContent').hide();
	$('.PDFcontent').hide();	
	$('.headerContent textarea').val(headerContent_source);	
	$('.footerContent textarea').val(footerContent_source);
	siteSelectorNanoReset();
}
function loadReportingSchedulePage()
{
	var content='<div class="site_nav_sub"> <ul> <li><a class="optionSelect " id="mainReport">Manual</a></li> <li><a class="optionSelect active" id="scheduleReport">Scheduled</a></li> <div class="clear-both"></div> </ul> </div></div> <div class="result_block shadow_stroke_box siteSearch bulk_publish_page_post reportOutline" style="border:0;box-shadow: 0 0 0  rgba(0, 0, 0, 0)"><div class="optionsContent result_block_noborder" style="display:block"></div><div class="scheduleReportContent " style="display:none;border: 1px solid #D2D5D7; margin-top:20px;"></div>';
	$("#pageContent").html(content);
	$(".scheduleReportContent").html('<div class="th rep_sprite"><div class="type_filter "><input name="" type="text" class="input_type_filter searchItems" value="type to filter"><div class="clear_input rep_sprite_backup" onclick=""></div></div><div class="btn_action float-right"><a class="rep_sprite createNewScheduleReport">Create New Report Schedule</a></div></div>');
	$(".scheduleReportContent").show();
}
function createImgUploader(){            
            var uploader = new qq.FileUploader({
                element: document.getElementById('imageUploaderContent'),
				action: systemURL+'uploadScript.php',
				params : {'imageOnly':1},
				debug: true
            }); 
			$('.upload_progress').remove();          
        }
function processReporting(data){
	$('#generateReport').removeClass('disabled'); 
	$('.btn_loadingDiv').remove();
	var mainContent='',subContent='',tmpCnt='',reportTitlePDF='';
	reportTitlePDF = $('.report_title').val();
	var headCntVal = $('.headerContent textarea').val();
	if($('.clientNote textarea').val() == "")
	{
		var pvtNote = "";
    }
	else
	{
		var pvtNote = '<div class="notesCnt"><span style="font-weight:700; font-size:12px;">NOTES</span><div class="pvtNote">'+$('.clientNote textarea').val()+'</div></div>';
	}
	var footCnt = '<div class="footCnt">'+$('.footerContent textarea').val()+'</div>';
	var clientNote = '<div class="clientCnt">'+$('.clientNote textarea').val()+'</div>';
	var reportData = data.data.clientReportingGenerate,y=0,subSiteName='';
	mainContent = '<div class="logo_cont" ><div class="float-left">'+imgSrc+'</div><div class="report_title_pdf">'+reportTitlePDF+'</div><div id="creport_header" >'+headCntVal+'</div></div><div class="clear-both"></div><div class="line_report"></div><div class="date_top_cr" style="margin-left: 14px;margin-top: 12px;">Report generated for the period - '+$('.reportDate').text()+'</div>';
	if(reportData != null)
	{  
	    var repLength = 0;
		$.each(reportData,function(){
			 repLength = repLength+1;
		});
		
	$.each(reportData,function(key,value){
		tmpCnt = '';
		var subSitesWidth = '96%';
		/*if(repLength == '1')
		{
			var subSitesWidth = '96%';
		}
		else
		{
			var subSitesWidth = '450px';
		}*/
		if(typeof site[key] == 'undefined')
		{
			subSiteName = key;
		}
		else
		{
			subSiteName = site[key].name;
		}
		subContent = subContent+'<div class="subSites float-left" style="width:'+subSitesWidth+';"><div class="th rep_sprite"><div class="title"><span class="droid700 reportSite" >'+subSiteName+'</span></div></div><div class="reportData" attr="'+key+'" ><ul class="reportDataUL">';
		if(value != null)
		{
		$.each(value,function(k,v){
			if(v != null)
			{
				var reportValue = 0;
				/*if((k == "posts") || (k == "pages") || (k == "links") || (k == "comments") )
				k = k+" Managed";
				else if((k == "plugin") || (k == "theme"))
				k = k+"s Updated";
				else if(k == "core")
				k = "WordPress Updated"	;
				else if(k == "backup")
				k = "Backups Performed";*/
				/*if(k == 'googleAnalytics')
				k = 'Google Analytics';
				else if(k == 'malwareScan')
				k = 'Malware Scan';*/
				
				if(typeof v.success != 'undefined')
				{
			tmpCnt=tmpCnt+'<li class="reportDataLi" style=""><div class="float-left reportActualName" style="width: 280px;padding: 9px 15px;">'+k+'</div><input class="float-left reportActualData" style="width:65px;" type="text" value="'+v.success+'"/><div class="rep_sprite  reportDeleteIconDiv float-right"><span class="rep_sprite_backup  reportDeleteIcon" ></span></div></li>';
				}
			}
		});
		}
		subContent = subContent+tmpCnt+'</ul><div class="th rep_sprite newActionBar" style="border-top: 1px solid #D2D5D7; border-bottom:0;"><input class="newActionTxt" style="width:53%;margin: 9px;" type="text" placeholder="new action"/><input class="newActionVal" style="width: 19%;" placeholder="1" type="text"/> <div class="btn_action float-right"><a class="rep_sprite " id="createNewAction" style="width: 40px; text-align: center;">Add</a></div> </div></div></div>'
	});
	}
	$('.reportContent').html(mainContent+subContent+pvtNote+footCnt+'<div class="clear-both"></div><div class="th rep_sprite PDFcontent" style="border: 1px solid #D2D5D7;"><div class="save_snippet_form"></div><div class="btn_action float-right ">  <a class="rep_sprite downPDF" id=""> Download PDF</a> </div> <div class="btn_action float-right"><a class="rep_sprite " style="" id="clientReport_SendEmail">Send Email</a></div><div class="PDF_form" style="display:none">  </div></div><div class="emailContent_report" style="display:none"> <span class="">FROM NAME</span> <input name="" type="text" class="half formVal required textbox_report" id="fromName_report"><span class="">FROM EMAIL</span> <input name="" type="text" class="half formVal required textbox_report" id="fromEmail_report">  <span class="">TO EMAIL</span> <input name="" type="text" class="half formVal required textbox_report" style="width:366px" id="toEmail_report"> <div class="form_label" style="margin-left:4px">SUBJECT</div><input name="" class="textbox_report" cols="" rows="" style="width: 917px" id="fromSubject_report"></input> <div class="form_label" style="margin-left:4px">MESSAGE</div> <textarea name="" type="text" style="width:925px" class="half formVal required" id="fromMessage_report"> </textarea><div class="th rep_sprite" style="border-top: 1px solid #D2D5D7; border-bottom:0;"><div class="save_snippet_form"></div><div class="btn_action float-right"><a class="rep_sprite" style="" id="sendFinalEmail_report">Send Email</a></div><div class="btn_action float-left"><a class="rep_sprite" style="" id="closeEmail_report">Cancel</a></div> </div></div> ');
	$('.PDFcontent').show();
	$('.PDF_form').html('<form action="ajax.php" method="post" class="reportingFormClass"><input type="hidden" class="hiddenReport" id="hiddenReportInput" name="" value=""/><input   type="submit" class="rep_sprite " style="" id="downloadPDF"/>Download PDF</form>');
	var cssSource = 'addons/clientReporting/css/report.css';
	var coreCSS = 'css/core.css';
	var reportDataHTML = '<link rel="stylesheet" type="text/css" href="'+systemURL+cssSource+'"  /><page format="100x200" orientation="L"  >'+$('.reportContent').html()+'</page>';
	var tempArray={};
	tempArray['requiredData']={}
	tempArray['requiredData']['clientReportingGeneratePdf'] ={};
	tempArray['requiredData']['clientReportingGeneratePdf']['siteID'] ={};
	tempArray['requiredData']['clientReportingGeneratePdf']['header']= $('.headerContent textarea').val();
	tempArray['requiredData']['clientReportingGeneratePdf']['privateNote']= $('.privateNote textarea').val();
	tempArray['requiredData']['clientReportingGeneratePdf']['footerContent']= $('.footerContent textarea').val();
	tempArray['requiredData']['clientReportingGeneratePdf']['clientNote']= $('.clientNote textarea').val();
	
	//tempArray['requiredData']['clientReportingGeneratePdf']['footerContent']=
	$('.reportData').each(function(key,val){
		var reportSiteID = $(this).attr("attr");
		tempArray['requiredData']['clientReportingGeneratePdf']['siteID'][reportSiteID]={};
		$('.reportDataLi',this).each(function(){
			tempArray['requiredData']['clientReportingGeneratePdf']['siteID'][reportSiteID][$('.reportActualName',this).text()] = $('.reportActualData',this).val();
			$('.hiddenReport').after('<input type="hidden" class="" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][siteID]['+reportSiteID+']['+$(".reportActualName",this).text()+']" value='+$('.reportActualData',this).val()+'>')
		});
		
	});
	//tempArray['requiredData']['clientReportingGeneratePdf']['content'] = reportDataHTML;
	$('.hiddenReport').after('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][headerContent]" value="'+$('.headerContent textarea').val()+'">');
	$('.hiddenReport').after('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][privateNote]" value="'+$('.privateNote textarea').val()+'">');
	$('.hiddenReport').after('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][footerContent]" value="'+$('.footerContent textarea').val()+'">');
	$('.hiddenReport').after('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][clientNote]" value="'+$('.clientNote textarea').val()+'">');
	/*$('.PDF_form').html('<form action="ajax.php" method="post"><input type="hidden" class="hiddenReport" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][content]" value=""/><input type="hidden" class="hiddenReport2" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][siteID]" value=""/><input   type="submit" class="rep_sprite " style="" id="downloadPDF"/>Download PDF</form>');*/
	//$('.hiddenReport').val(reportDataHTML);
	$('#loadingDiv').hide();
	$("html, body").animate({ scrollTop: $(document).height() }, "slow");
 
}

/*function processPDF(data){
}*/
function storeReportData(data){
	if((data.data.clientReportingGetLogo != null)&&(data.data.clientReportingGetLogo != ""))
	{
	imgSrcName = data.data.clientReportingGetLogo;
	var imgTemp = "uploads/";
	imgSrc = '<img src="'+systemURL+imgTemp+data.data.clientReportingGetLogo+'" width="116" />';
	}
	if((data.data.clientReportingGetHeaderFooterCont != null)&&(data.data.clientReportingGetHeaderFooterCont != ""))
	{
		headerContent_source = data.data.clientReportingGetHeaderFooterCont.headerContent;
		footerContent_source = data.data.clientReportingGetHeaderFooterCont.footerContent;
	}
}
function processScheduleReport(data){
	var response_message_report = data.data.clientReportingSchedule;
	if(response_message_report == true)
	{
		$('#loadingDiv').show();
		var tempArray={};
		tempArray['requiredData'] = {};
		tempArray['requiredData']['clientReportingScheduleReportViewHTML']=1;
		tempArray['requiredData']['clientReportingGetHeaderFooterCont']=1;
		doCall(ajaxCallPath,tempArray,'loadAllScheduleReports','json',"none");
	}
	else
	{
		$('#loadingDiv').hide();
		$('.btn_loadingDiv').remove();
		/*$('.scheduleReportClose').removeClass('disabled');
		$('#scheduleReportSubmit').removeClass('disabled');*/
	}
}
function downloadPDF_report(){
	$('.reportingFormClass').html('<input type="hidden" class="hiddenReport" id="hiddenReportInput" name="" value=""/><input   type="submit" class="rep_sprite " style="" id="downloadPDF"/>');
	$('.reportData').each(function(key,val){
		var reportSiteID = $(this).attr("attr");
		//tempArray['requiredData']['clientReportingGeneratePdf']['siteID'][reportSiteID]={};
		$('.reportDataLi',this).each(function(){
			//tempArray['requiredData']['clientReportingGeneratePdf']['siteID'][reportSiteID][$('.reportActualName',this).text()] = $('.reportActualData',this).text();
			$('.hiddenReport').before('<input type="hidden" class="" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][siteID]['+reportSiteID+']['+$(".reportActualName",this).text()+']" value='+$('.reportActualData',this).val()+'>')
		});
		
	});
	//$('.hiddenReport').after('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][reportTitle]" value="'+$('.report_title').val()+'">');
	$('.hiddenReport').before('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][headerContent]" value="'+$('.headerContent textarea').val()+'">');
	
	//$('.hiddenReport').after('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][privateNote]" value="'+$('.privateNote textarea').val()+'">');
	$('.hiddenReport').before('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][footerContent]" value="'+$('.footerContent textarea').val()+'">');
	$('.hiddenReport').before('<input type="hidden" id="hiddenReportInput" name="requiredData[clientReportingGeneratePdf][clientNote]" value="'+$('.clientNote textarea').val()+'">');
	
}

function loadAllScheduleReports(data){
	var content='';
	if((data.data.clientReportingGetHeaderFooterCont != null)&&(data.data.clientReportingGetHeaderFooterCont != ""))
	{
		headerContent_source = data.data.clientReportingGetHeaderFooterCont.headerContent;
		footerContent_source = data.data.clientReportingGetHeaderFooterCont.footerContent;
	}
	data=data.data.clientReportingScheduleReportViewHTML;
	 /* var content='<div class="site_nav_sub"> <ul> <li><a class="optionSelect " id="mainReport">Manual</a></li> <li><a class="optionSelect active" id="scheduleReport">Scheduled</a></li> <div class="clear-both"></div> </ul> </div></div> <div class="result_block shadow_stroke_box siteSearch bulk_publish_page_post reportOutline" style="border:0;box-shadow: 0 0 0  rgba(0, 0, 0, 0)"><div class="optionsContent result_block_noborder" style="display:block"></div><div class="scheduleReportContent " style="display:none;border: 1px solid #D2D5D7; margin-top:20px;"></div>';
	  $(".scheduleReportContent").html('<div class="th rep_sprite"><div class="type_filter "><input name="" type="text" class="input_type_filter searchItems" value="type to filter"><div class="clear_input rep_sprite_backup" onclick=""></div></div><div class="btn_action float-right"><a class="rep_sprite createNewScheduleReport">Create New Report Schedule</a></div></div>');*/
		content=content+'<div class="site_nav_sub"> <ul> <li><a class="optionSelect" id="mainReport">Manual</a></li> <li><a class="optionSelect active" id="scheduleReport">Scheduled</a></li> <div class="clear-both"></div> </ul> </div><div><ul class="btn_rounded_gloss"> <li><a class="rep_sprite optionSelect active" utype="" onclick="">Reports</a></li> <li><a class="rep_sprite optionSelect createNewScheduleReport" utype="" onclick="">Create New</a></li> </ul></div>';
	content=content+'<div class="result_block backup  shadow_stroke_box siteSearch" id="backupPageMainCont" style="margin:70px 0px"><div class="th rep_sprite"><div class="type_filter "><input name="" type="text" class="input_type_filter searchItems" value="type to filter"><div class="clear_input rep_sprite_backup"  onclick=""></div></div><div class="btn_action float-right"><a class="rep_sprite createNewScheduleReport" style="display:none">Create New Report Schedule</a></div></div><div class="no_match hiddenCont" style="display:none">Bummer, there are no backups that match.<br />Try typing fewer characters.</div>'+data+'</div>';
	$("#pageContent").html(content);
	$(".removeScheduleReport").qtip({content: { text: 'Delete Schedule' }, position: { my: 'left center', at: 'right center' }, show: { event: 'mouseenter' }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 5, height:8} } });
	$(".removeReportTooltip").qtip({content: { text: 'Delete Report' }, position: { my: 'left center', at: 'right center' }, show: { event: 'mouseenter' }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 5, height:8} } });
	$(".run_now_report").qtip({content: { text: 'Run Now' }, position: { my: 'left center', at: 'right center' }, show: { event: 'mouseenter' }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 5, height:8} } });
	/*$('.reportNameForScheduleReport').each(function(k,v){
		$(this).qtip({id:"toggleGroupQtip",content: { text: $(this).text() }, position: { my: 'bottom center', at: 'top center',  adjust:{ y: -6} }, show: {  }, hide: {  }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 10, height:5} } });
	});*/
	var expand_summary_class = ".expand_summary_"+row_summary_expand_var;
	$(expand_summary_class).hide();
	$(expand_summary_class).next('.row_detailed').show( "blind", {}, 500);
	$('#loadingDiv').hide();
	$('.btn_loadingDiv').remove();
	$('.scheduleReportClose').removeClass('disabled');
	$('#scheduleReportSubmit').removeClass('disabled');
	row_summary_expand_var = '';
}

function loadAllManualScheduleReports(data){
	if((data.data.clientReportingGetHeaderFooterCont != null)&&(data.data.clientReportingGetHeaderFooterCont != ""))
	{
		headerContent_source = data.data.clientReportingGetHeaderFooterCont.headerContent;
		footerContent_source = data.data.clientReportingGetHeaderFooterCont.footerContent;
	}
	var content='';
	data=data.data.clientReportingManualReportViewHTML;
	content=content+'<div class="site_nav_sub"> <ul> <li><a class="optionSelect active" id="mainReport">Manual</a></li> <li><a class="optionSelect " id="scheduleReport">Scheduled</a></li> <div class="clear-both"></div> </ul> </div><div><ul class="btn_rounded_gloss"> <li><a class="rep_sprite optionSelect active" utype="" onclick="">Reports</a></li> <li><a class="rep_sprite optionSelect createNewManualReport" utype="" onclick="">Create New</a></li> </ul></div>';
	content=content+'<div class="result_block backup  shadow_stroke_box siteSearch" id="backupPageMainCont" style="margin:70px 0px"><div class="th rep_sprite"><div class="type_filter "><input name="" type="text" class="input_type_filter searchItems" value="type to filter"><div class="clear_input rep_sprite_backup"  onclick=""></div></div><div class="btn_action float-right"><a class="rep_sprite createNewManualReport" style="display:none" >Create New Report</a></div></div><div class="no_match hiddenCont" style="display:none">Bummer, there are no backups that match.<br />Try typing fewer characters.</div>'+data+'</div>';
	$("#pageContent").html(content);
	$(".removeManualReport").qtip({content: { text: 'Delete Report' }, position: { my: 'left center', at: 'right center' }, show: { event: 'mouseenter' }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 5, height:8} } });
	$('.manualReportRowSummary .row_name').each(function(k,v){
		$(this).qtip({id:"toggleGroupQtip",content: { text: $(this).text() }, position: { my: 'bottom center', at: 'top center',  adjust:{ y: -6} }, show: {  }, hide: {  }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 10, height:5} } });
	});
	
	$('#loadingDiv').hide();
	$('.btn_loadingDiv').remove();
	$('.scheduleReportClose').removeClass('disabled');
}

function loadEditScheduleReportPage(data){
	data = data.data.clientReportingGetSchedule;
	$('.createNewScheduleReport').click();
	var siteArray_report = data.siteID;
	$.each(siteArray_report,function(k,v){
		$('.website_cont').each(function(key,val){
			if($(this).attr('sid') == v)
			{
				$(this).click();
			}
		});
	});
	var report_title = $('.report_title').val(data.profileName);
	if( typeof data.headerFooterLogoCont != 'undefined')
	{
		var headerContent = $('.headerContent textarea').val(data.headerFooterLogoCont.headerContent);
		var footerContent = $('.footerContent textarea').val(data.headerFooterLogoCont.footerContent);
	}
	var clientNote = $('.clientNote textarea').val(data.clientNote);
	$('#fromName_report').val(data.email.fromName);
	$('#fromEmail_report').val(data.email.fromEmail);
	$('#toEmail_report').val(data.email.toEmail);
	$('#fromSubject_report').val(data.email.subject);
	$('#fromMessage_report').val(data.email.message);
	$('.hiddenReportID').val(data.clientReportScheduleID);
	$(".reportOutline #timeSelectBtn a").text(data.sTime_value);
	$('.whenType').each(function(k,v){
		var attr = $(this).attr('var');
		attr = attr+'ly';
		if(attr == data.period)
		{
			$(this).click();
		}
	});
	$('.day_select_cont .optionSelect').removeClass('active');
	$('.day_select_cont .optionSelect').each(function(k,v){
		if($(this).attr('dayval') == data.week_month_val)
		{
			$(this).addClass('active');
		}
	});
	$('.date_select_cont  .selectDate_report').removeClass('active');
	$('.date_select_cont  .selectDate_report').each(function(k,v){
		if($(this).text() == data.week_month_val)
		{
			$(this).addClass('active');
		}
	});
	var typeArray = {},actionArray = {};
	typeArray = data.type;
	actionArray = data.action;
	
	$('.actions .selectReport').removeClass('active');
	$('.actions .selectReport').each(function(k,v){
		var checkbox_cr = $(this);
		$.each(typeArray,function(k,v){
			if(($(checkbox_cr).attr('val') == v)&&($(checkbox_cr).attr('val') != 'PTC'))
			{
				$(checkbox_cr).addClass('active');
			}
		});
		$.each(actionArray,function(k,v){
			if($(checkbox_cr).attr('action') == v)
			{
				$(checkbox_cr).addClass('active');
			}
		});
	});
	$('#reScheduleReportSubmit').show();
	$('#scheduleReportSubmit').hide();
	$('.createNewReportActive').removeClass('active');
	$('.createNewReportActive').addClass('createNewScheduleReport');
}

function loadscheduleReportSubmit(action){
	
	var sTime=$(".reportOutline #timeSelectBtn a").attr('timeval');
	var sTime_value = $(".reportOutline #timeSelectBtn a").text();
	var week_or_month = $('.reportOutline .whenType.active').text().toLowerCase();
	var clientReportDate = '',week_month_val='';
	if(week_or_month == 'weekly')
	{
		clientReportDate = sTime+'|'+$(".reportOutline .day_select_cont .optionSelect.active").attr('dayval');
		week_month_val = $(".reportOutline .day_select_cont .optionSelect.active").attr('dayval');
	}
	else
	{
		clientReportDate = sTime+'|'+$(".reportOutline .date_select_cont .selectDate_report.active").text();
		week_month_val = $(".reportOutline .date_select_cont .selectDate_report.active").text();
	}
	var actions = {},PTCaction={},i=0,tempArray={};
	tempArray['requiredData'] = {};
	var sites = getSelectedSites();
	$('.actions .selectReport.active').each(function(){
		
		actions[i] = $(this).attr("val");
		if($(this).attr("action") != "")
		PTCaction[i] = $(this).attr("action");
		i++;
	});
	var profileName_cr = $('.report_title').val();
	if(profileName_cr == "")
	{
		$('.report_title').qtip({id:"toggleGroupQtip",content: { text: 'Please enter profile name for report' }, position: { my: 'bottom center', at: 'top center',  adjust:{ y: -6} }, show: {  }, hide: {  }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 10, height:5} } });
		$('.report_title').addClass('error_report');
	}
	else
	{
		$('#reScheduleReportSubmit').addClass('disabled');
		$('#scheduleReportSubmit').addClass('disabled');
		var emailArray = {};
		emailArray['fromName']=$('#fromName_report').val();
		emailArray['fromEmail']=$('#fromEmail_report').val();
		emailArray['toEmail']=$('#toEmail_report').val();
		emailArray['message']=$('#fromMessage_report').val();
		emailArray['subject']=$('#fromSubject_report').val();
		tempArray['requiredData']['clientReportingSchedule']={};
		tempArray['requiredData']['clientReportingSchedule']['profileName'] = profileName_cr;
		tempArray['requiredData']['clientReportingSchedule']['headerFooterLogoCont']={};
		tempArray['requiredData']['clientReportingSchedule']['headerFooterLogoCont']['headerContent']=$('.headerContent textarea').val();
		tempArray['requiredData']['clientReportingSchedule']['headerFooterLogoCont']['footerContent']=$('.footerContent textarea').val();
		tempArray['requiredData']['clientReportingSchedule']['clientNote']=$('.clientNote textarea').val();
		tempArray['requiredData']['clientReportingSchedule']['combinedSitesReport']=$('.combineAllReport').attr("var");
		tempArray['requiredData']['clientReportingSchedule']['siteID']={};
		tempArray['requiredData']['clientReportingSchedule']['type']={};
		tempArray['requiredData']['clientReportingSchedule']['action']={};
		tempArray['requiredData']['clientReportingSchedule']['siteID']=sites;
		tempArray['requiredData']['clientReportingSchedule']['type']=actions;
		tempArray['requiredData']['clientReportingSchedule']['action']=PTCaction;
		tempArray['requiredData']['clientReportingSchedule']['period']=week_or_month;
		tempArray['requiredData']['clientReportingSchedule']['schedule']=clientReportDate;
		tempArray['requiredData']['clientReportingSchedule']['email']=emailArray;
		tempArray['requiredData']['clientReportingSchedule']['week_month_val']= week_month_val;
		tempArray['requiredData']['clientReportingSchedule']['schedule_time']= sTime;
		tempArray['requiredData']['clientReportingSchedule']['sTime_value']= sTime_value;
		tempArray['requiredData']['clientReportingSchedule']['headerFooterLogoCont']['logoName']=$('.imageSpace').attr("imgSrc");
		if(action == "reSchedule")
		{
			$('#reScheduleReportSubmit').append('<div class="btn_loadingDiv left"></div>');
			tempArray['requiredData']['clientReportingSchedule']['clientReportScheduleID']= $('.hiddenReportID').val();
		}
		else
		{
			$('#scheduleReportSubmit').append('<div class="btn_loadingDiv left"></div>');
		}
		doCall(ajaxCallPath,tempArray,'processScheduleReport','json',"none");
	}
}

function processscheduleDeleteReport(data)
{
	
}
function processsRunNowReport(data)
{
	$('#loadingDiv').hide();
	if(data.data.clientReportingScheduleReportRunNow == true)
	{
		
		var tempArray={};
		tempArray['requiredData'] = {};
		tempArray['requiredData']['clientReportingScheduleReportViewHTML']=1;
		tempArray['requiredData']['clientReportingGetHeaderFooterCont']=1;
		doCall(ajaxCallPath,tempArray,'loadAllScheduleReports','json',"none");
	}
}
function processManualSendEmail_report(data)
{
	if(data.data.clientReportingGeneratePdf == true)
	{
		var tempArray={};
		tempArray['requiredData'] = {};
		tempArray['requiredData']['clientReportingManualReportViewHTML']=1;
		tempArray['requiredData']['clientReportingGetHeaderFooterCont']=1;
		doCall(ajaxCallPath,tempArray,'loadAllManualScheduleReports','json',"none");
	}
	else
	{
		$('#loadingDiv').hide();
		$('.btn_loadingDiv').remove();
		$('#sendFinalEmail_report').removeClass('disabled');
	}
}
