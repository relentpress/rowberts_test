var repSiteID,repCid,repPid,repText;
function showCommentOptions()
{
	if(!isSiteSelected())
	{
	$(".optionsContent").hide();
	$(".advancedInstallOptions").hide();
	$("#s111").hide();
	}
	else
 {  
	$(".mycontent").show();
	$(".optionsContent").show();// For manage / install panel
	$(".actionContent").html('').hide();
	
 }
}

function optionSelectComments(object)
{
	if($(object).hasClass('active'))
	{
		$(object).removeClass('active');
	}
	else
	{
		$(object).addClass('active');
	}
}



function applyChangesComments()
{
	if($('.userSelect').hasClass('active'))
	{
	if($('.actionButtonComment').hasClass('active'))
	{
	$(".status_applyChangesCheck").removeClass('disabled');
	}
	else
	$(".status_applyChangesCheck").addClass('disabled');
	}
	else
	{
		$(".status_applyChangesCheck").addClass('disabled');
	}
}


function applyChangesComment(object)
{
	var changeArray={};
	var arrayCounter=0, siteID, dID, type, action, name, valArray={},siteIDS={},n=0,sites={},mainParent,count=0;
	closestVar=$(object).closest('.siteSearch');
	if($(".select_spam_comments").hasClass('active'))
	{	
	action='spam';
	$(".select_spam_comments").removeClass('active');
	}
    if($(".select_approve_comments").hasClass('active'))
	{
	action='approve';
	$(".select_approve_comments").removeClass('active');
	}
	if($(".select_trash_comments").hasClass('active'))
	{
	action='trash';
	$(".select_trash_comments").removeClass('active');
	}
	if($(".select_delete_comments").hasClass('active'))
	{
	action='delete';
	$(".select_delete_comments").removeClass('active');
	}
	$(".applyChangesComments.active").each(function () {
		
		siteID = $(this).attr('siteid');
		if(valArray[siteID]==undefined)
		{
		valArray[siteID]={};
		}
		cid=$(this).attr('commentid');
		valArray[siteID][arrayCounter]=cid;
		arrayCounter++;
		hideBlock(this);
	});
	$('.postCheckBox.active').each(function(){
		mainParent = $(this).closest('.row_ind_post_cont');
		var main = $(this).closest('.row_ind_post_cont');
		var full = $(this).closest('.rd');
		main.fadeOut('slow',function(){
	    });		
		
	});
	$('.postCheckBox:visible',mainParent).each(function(){
		count++;
	});
	if(count<2)
	{
		$(mainParent).fadeOut('slow',function(){
	    });	
	}
	$('.rh.active').each(function(){
		$(this).parent().fadeOut('slow',function(){
	    });	
	});
	var tempArray={};
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['args']['params']['siteID']={};
	tempArray['action']='bulkEditComments';
	//tempArray['args']['siteIDs']=siteArray;
	tempArray['args']['params']['siteID'] = valArray;
	tempArray['args']['params']['docomaction'] = action;
	tempArray['requiredData']={};
	
	doHistoryCall(ajaxCallPath,tempArray,'');


}

function removeActiveElementsComments() 
{   

	$(".applyChangesComments.active").each(function () {
		$(this).closest("row_ind_comment_cont").hide();
	});
}


function formArrayLoadComments(data)
{
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="loadComments";
}


function replyComments(site,postID,cid,repText)
{ 
var siteID,dID,type,action,name,valArray,act,requestData,cidi,reqArr={},pid;
    var siteI=site;
	cidi=cid;
	siteID={};
	siteID['0']=siteI;
	action='replyComments'
	pid=postID;
	valArray={};
	 valArray['commentID'] =cidi ;
     valArray['postID'] =pid;
	 valArray['replyText']=repText;

	 requestData={};
	 requestData['args']={};
	  requestData['action'] = "replyComments";	
		requestData['args']['siteIDs'] = siteID;
		requestData['args']['params'] = valArray;
		reqArr['commentsReplyGet']={};
	   requestData['requiredData'] = reqArr;
      doHistoryCall(ajaxCallPath,requestData,'');

return false;

}

function objLen(obj) {
    var L=0;
    $.each(obj, function(i, elem) {
		$.each(elem, function(k, v) {
        L++;
		});
    });
    return L;
}

function hideBlock(obj) {
	var count=0,childCount=0;
	$(obj).closest('.row_ind_comment_cont').fadeOut('slow',function(){
		$(this).remove();
	});
	var main = $(obj).closest('.row_ind_post_cont');
	var full = $(obj).closest('.row_detailed');
	$('.row_ind_comment_cont:visible',main).each(function(){
		childCount++;
	});
	if(childCount<2)
	{
		$(main).fadeOut('slow',function(){
	});
	}
	$('.row_ind_post_cont:visible',full).each(function(){
		count++;
	});
	if((count==1)&&(childCount==1))
	{
		$(full).fadeOut('slow',function(){
	});
	}
	
}


