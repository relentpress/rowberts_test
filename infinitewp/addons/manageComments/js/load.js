 var isComment=1,recentLength=0,commentsJson=0;

function loadCommentManage()
{
	var content='<div class="site_nav_sub"> <ul >  <li> <a class="navLinkUpdate optionSelect commentClass recentComments" page="comments"><span class="float-left">Recent</span><span class="update_count float-left droid700 commentCountClass" id="">'+recentLength+'</span></a> </li> <li><a class="manageComments optionSelect">MANAGE</a></li> <div class="clear-both"></div> </ul> </div></div><div class="steps_hdr">  <span id="processType">Showing Recent </span> Comments <span class="itemUpper"></span></div><div class="siteSelectorContainer">'+siteSelectorVar+'</div><div class="advancedInstallOptions" style="display:none"><div class="steps_hdr">INSTALLATION OPTIONS</div> <div style="width:220px;"> <div class="checkbox generalSelect activateItem" onclick="">Activate  <span class="itemNameLower">'+activeItem+'</span> after install</div> <div class="checkbox generalSelect overwriteItem" onclick="">Overwrite, if <span class="itemNameLower">'+activeItem+'</span> already exist.</div> </div></div> <div class="result_block shadow_stroke_box siteSearch itemPanel comments" style="border:0;box-shadow: 0 0 0  rgba(0, 0, 0, 0)"><div class="optionsContent " style="display:none"></div><div class="optionsContentRecent"></div>';
	$("#pageContent").html(content);
	if(recentLength < 1)
	{
		$(".commentCountClass").hide();
	}
	else
	{
		$(".commentCountClass").show();
	}
	$(".recentComments").click();
	siteSelectorNanoReset();
}
   
   function loadManageCommentPanel(data)
 {
	$(".fetchManageComments").text($(".fetchManageComments").attr('tempname')).removeClass('disabled');
	$(".btn_loadingDiv").remove();
	manage=data;
		var comCont=manage.data.manageCommentsGet.siteView,apprCount=0,unapprCount=0,spamCount=0,trashCount=0;
        
		if(comCont!=undefined)
		{
		 $.each(comCont, function(key,value){
			 
			 if(value!=false)
			 {
			 $.each(value,function(i,object){
				 if(object.comment_approved == '1')
				 {
				 apprCount=apprCount+1;				 
				 }
				 if(object.comment_approved == '0')
				 {
				 unapprCount=unapprCount+1;				 
				 }
				 if(object.comment_approved == 'spam')
				 {
				 spamCount=spamCount+1;				 
				 }
				 if(object.comment_approved == 'trash')
				 {
				 trashCount=trashCount+1;				 
				 }
			       });
			  } 
			 });
			 }
	var content='';
	content=' <div class="th rep_sprite"><ul class="btn_radio_slelect float-left" style="margin-left:10px;"><li><a class="rep_sprite manage_unapproved_comments optionSelect active">Pending <span>('+unapprCount+')</span></a></li><li><a class="rep_sprite manage_approved_comments optionSelect ">Approved <span>('+apprCount+')</span></a></li><li><a class="rep_sprite manage_spam_comments optionSelect">Spam <span>('+spamCount+')</span></a></li><li><a class="rep_sprite manage_trash_comments optionSelect">Trash <span>('+trashCount+')</span></a></li> </ul> <div class="type_filter "> <input name="" id="manageFilter" type="text" class="input_type_filter searchComments manageFilter " value="type to filter" style="margin-left: -8px; width: 77px;" /> <div class="clear_input rep_sprite_backup" onclick=""></div></div> <div class="btn_action float-right"><a class="rep_sprite status_applyChangesCheck js_changes_comments disabled">Apply Changes</a></div> <ul class="th_sub_nav float-right comments_management" style="padding: 3px 0 4px 2px;"><li><a class="rep_sprite select_approve_comments userSelect">Approve</a></li><li><a class="rep_sprite select_spam_comments userSelect ">Spam</a></li><li><a class="rep_sprite select_trash_comments userSelect ">Trash</a></li> <li><a class="rep_sprite select_delete_comments userSelect ">Delete Permanently</a></li></ul> </div> <div id="view_content" class="rows_cont" ></div>';
	$(".actionContent").html(content).show();
	CommentsListPanel("unapproved");
	text=$(".fetchInstallTxt2").val();
	//$(".manage_approved_comments").click();

 }


    function loadRecentCommentPanel(data)
 {
	  manage=data;
	  var content='';
	 content=' <div class="th rep_sprite"><div class="title"><span class="droid700">Search Websites</span> </div><div class="type_filter "> <input name="" id="manageFilter" type="text" class="input_type_filter searchComments manageFilter " value="type to filter" /> <div class="clear_input rep_sprite_backup" onclick=""></div></div><div class="btn_action float-right"><a class="rep_sprite status_applyChangesCheck js_changes_comments disabled">Apply Changes</a></div> <ul class="th_sub_nav float-right comments_management" style="padding: 3px 0 4px 2px;"><li><a class="rep_sprite select_approve_comments userSelect">Approve</a></li><li><a class="rep_sprite select_spam_comments userSelect">Spam</a></li><li><a class="rep_sprite select_trash_comments  userSelect">Trash</a></li>  <li><a class="rep_sprite select_delete_comments userSelect ">Delete Permanently</a></li> </ul> </div> <div id="view_content" class="rows_cont" ></div>';
	$(".optionsContentRecent .actionContent").html(content).show();
	CommentsListPanel("unapproved");
	text=$(".fetchInstallTxt2").val();

 }


function CommentsListPanel(view)
 {   
	 $(".status_applyChangesCheck").addClass('disabled');
	 var content='',statusVar,firstKey,actionVar,actionLi,siteID,dID,contName,delCont,extraDeactivateClass,styleSheetTmp,dLink,noContentText,posts={},m=0,postsTemp={},newpost,tempObject,jsontemp={},newPid={},cLenInit=0,cLen={},buttonWidth;
	 postsTemp={};

	 if(view=='comments')
	var json=manage.data.manageCommentsGet.siteView;
	 else if(view =='approved')
	 {   
		 var json=manage.data.manageCommentsGet.siteView;
		 buttonWidth="width:116px";
		 jsontemp={};
		 if(json!=undefined)
		 {
		 $.each(json, function(key,value){
			 
			 if(value!=false){
				 jsontemp[key]={};
			 $.each(value,function(i,object){
				 if(object.comment_approved == '1'){
				 jsontemp[key][i]=object;				 
				 }
			 });

			 }
			 
			 
		 });
		 }
		 json=jsontemp;
	 }
	 else if(view =='unapproved')
	 {   
	     buttonWidth="";
	     if($('.recentComments').hasClass('active'))
		 {   
		     if(manage!=0)
			 {
		     if(((manage.data.manageCommentsGetRecent)!= undefined))
	        {
			 json=manage.data.manageCommentsGetRecent.siteView;
			 if(json==null)
			 {
			 json=0;
			 //content=content+'<div class="ind_row_cont" ><div class="row_summary" ><div class="row_name"> No Comments to Show </div><div class="clear-both"></div></div></div>';
			 }
			}
			else
			{
				json=0;
			}
			 }
			 else
			{
				json=0;
			}
			 
		 }
		 else
		 {
		 json=manage.data.manageCommentsGet.siteView;
		 }
		  jsontemp={};
		  if(json!=undefined)
		 {
		 $.each(json, function(key,value){
			 if(value!=false){
			 jsontemp[key]={};
			 $.each(value,function(i,object){
				 if(object.comment_approved == '0'){
				 jsontemp[key][i]=object;				 
				 }
			 });

			 }
		 });
		 }
		 json=jsontemp;	 
	 }
	 else if(view =='spam')
	 {   
	     buttonWidth="width:116px";
		 json=manage.data.manageCommentsGet.siteView;
		  jsontemp={};
		  if(json!=undefined)
		 {
		 $.each(json, function(key,value){
			 if(value!=false){
			 jsontemp[key]={};
			 $.each(value,function(i,object){
				 if(object.comment_approved == 'spam'){
				 jsontemp[key][i]=object;				 
				 }
			 });
			 }
		 });
		 }
		 json=jsontemp;	 
	 }
	  else if(view =='trash')
	 {   
	     buttonWidth="";
		 json=manage.data.manageCommentsGet.siteView;
		  jsontemp={};
		  if(json!=undefined)
		 {
		 $.each(json, function(key,value){
			 if(value!=false){
			 jsontemp[key]={};
			 $.each(value,function(i,object){
				 if(object.comment_approved == 'trash'){
				 jsontemp[key][i]=object;				 
				 }
			 });
             }
		 });
		 }
		 json=jsontemp;	 
	 }
    var totalComments = objLen(json);
	 if(json!=null && json!=undefined && getPropertyCount(json)>0 && totalComments>0)
 {
	 $.each(json, function(i, object) {
		 if(object!=false){
		 var j=i;
		 cLenInit = 0;
		
		 newpost={};
		 posts={};
		 tempObject=object;
		 $.each(object, function(k, v) {
			 posts[v.post_title]={};
			 posts[v.post_title]=v.comment_content;
	
			 cLenInit=cLenInit+1;
		 });
		 cLen[i]=cLenInit;
		 $.each(posts,function(key,value){
			 newpost[key]={};
			 newPid[key]={};
	
		  $.each(tempObject, function(k, v) {
			if(key==v.post_title)
			{   newpost[key][k]={};
				newpost[key][k]['content']=v.comment_content;
				newpost[key][k]['author']=v.comment_author;
				newpost[key][k]['date']=v.comment_date;
				newpost[key][k]['pid']=v.comment_post_ID;
				m++;
				newPid[key]=v.comment_post_ID;
	
			}
		 });
		 });
		 if(cLen[j]!=0)
		 {
		 content=content+'<div class="ind_row_cont "> <div class="row_summary" > <div class="row_arrow"></div><div class="count_cont selector">'+maxLen(cLen[j])+'</div><div class="row_name searchable">'+site[j].name+'</div> <div class="clear-both"></div> </div>';
		 content=content+'<div class="row_detailed" style="display:none"> <div class="rh"> <div class="row_arrow"></div><div class="checkbox checkSite" style="padding-bottom:11px;"></div><div class="count_cont"><span>'+cLen[j]+'</span></div><div class="row_name">'+site[j].name+'</div> <div class="clear-both"></div> </div><div class="rd">';
		 }
		 $.each(newpost,function(key,value) {
		
		 
	    
	     content=content+'<div class="row_ind_post_cont  "><div class="checkbox postCheckBox "></div><div class="row_ind_post disabled">'+key+'</div><div class="row_action_btn rep_sprite" style="float:right;"><a class="open_pop_out rep_sprite_backup" pid='+newPid[key]+' sid='+j+' href='+site[j].URL+'/?p='+newPid[key]+' target="_blank"></a></div><div class="clear-both"></div>';
		 $.each(value,function(k,v){
			 content=content+'<div class="row_ind_comment_cont "><div class="checkbox actionButtonComment optionSelectComment applyChangesComments" commentID='+k+' siteID='+j+'></div><div class="row_comment">'+v.content+'</div><div class="clear-both"></div><div class="replyComment reply_cont" style="display : none"><textarea id="replyContent" class="onEnterReply" onEnterBtn=".op_reply_me" commentID='+k+' pid='+v.pid+' siteID='+j+' name="" cols="" rows=""></textarea><div class="tr rep_sprite"><a class="cancel_reply ">Cancel</a><div class="btn_action"><a class="rep_sprite op_reply_me" commentID='+k+' pid='+v.pid+' siteID='+j+'>Reply</a></div><div class="clear-both"></div></div></div><div class="clear-both"></div><div class="comment_who icon_sprite_WPcomments">'+v.author+'</div><div class="comment_when rep_sprite_backup">'+v.date+'</div><ul class="comments_ops" style='+buttonWidth+'><li class="rep_sprite"><a class="op_approve icon_sprite_WPcomments" title="Approve this comment"commentID='+k+' siteID='+j+'></a></li><li class="rep_sprite"><a class="op_reply icon_sprite_WPcomments" title="Reply" commentID='+k+' pid='+v.pid+' siteID='+j+'></a></li><li class="rep_sprite"><a class="op_edit comment icon_sprite_WPcomments" title="Edit" commentID='+k+' siteID='+j+'></a></li><li class="rep_sprite"><a class="op_spam rep_sprite_backup" title="Spam"commentID='+k+' siteID='+j+'></a></li><li class="rep_sprite"><a class="op_trash comments rep_sprite_backup" type="comments" title="Trash"commentID='+k+' siteID='+j+'></a></li><li class="rep_sprite"><a class="op_del_perm comments comments rep_sprite_backup" type="comments" title="Delete Permanently"commentID='+k+' siteID='+j+'></a></li></ul><div class="clear-both"></div></div>'; 
		 });
		
		 content=content+'</div>'
		 
		 
		 });
		 

		  content=content+'</div></div></div>';
		 }
		  });
		  }
		if((totalComments==0)||(json == 0))
		content=content+'<div class="ind_row_cont" ><div class="rd" ><div class="row_name"> No Comments to Show </div><div class="clear-both"></div></div></div>';
		if(view=="sites")
		noContentText='websites';
		else
		noContentText = 'websites';
		content='<div class="no_match hiddenCont" style="display:none">Bummer, there are no '+noContentText+' that match.<br />Try typing lesser characters.</div>'+content;
		 $("#view_content").html(content);
		 //hideApproveButton();
		 $('.op_del_perm').each(function(){
			$(this).parent().hide();
		});
 }




function hideApproveButton()
{

}

function hideBulkActions()
{   
    //$('.select_approve_comments').show();
	   $('.select_approve_comments').show();
		$('.select_spam_comments').show();
		$('.select_trash_comments').show();
	
}


function formArrayLoadComments(data)
{ 
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="loadComments";
}

function recentInitial() {
	  var tempArray={};
	  tempArray['action']='getStats';
	  tempArray['requiredData']={};
	  tempArray['requiredData']['manageCommentsGetRecent']=1;
	  doCall(ajaxCallPath,tempArray,"recentCommentsInitialLoad");
	  
  }

function recentCommentsInitialLoad(data){
	commentsJson=data;
	recentLength=commentsJson.data.manageCommentsGetRecent.siteView;       
	if(recentLength!=undefined)
	{
	recentLength=objLen(recentLength);
	}
	else
	{
		recentLength=0;
	}
	if(typeof recentLength != 'undefined' )
	{
	$("#totalCommentCount").text(recentLength);
	if(recentLength<1)
	{
		$('.commentCountClass').hide();
	}
	else
	{
		$('.commentCountClass').text(recentLength).show();
	}
	}
	//refreshRecent(data.data.getWaitData[actionID]);
}
/*function refreshRecent(data,refreshClientUpdate,noReload)
{

	if(refreshClientUpdate==undefined)
	refreshClientUpdate=0;
	
	if((typeof isComment != 'undefined' )&&(isComment == 1))                                                        //To load Recent Comments While Reload Data
	{
		
	if(((data.data.manageCommentsGetRecent)!= undefined))
	{
		
	commentsJson=data;
	recentLength=commentsJson.data.manageCommentsGetRecent.siteView;  
	if(recentLength!=undefined)
	{
	recentLength=objLen(recentLength);
	}
	else
	{
		recentLength=0;
	}
	}
	}	
	updateCountRefresh();
	if(noReload!=1)
	processPage(currentPage,1);
	if(typeof isComment != 'undefined')
	{
	if($('.recentComments').hasClass('active'))
	{
		loadCommentManage();
	}
	}
	//hideCommentCount();
}

*/
