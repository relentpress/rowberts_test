<?php

/*
slug: manageComments
version: 1.0.1
*/

class addonManageComments{
	
	private static $version = '1.0.1';

	public static function version(){
		return self::version;	
	}
	
	public static function init(){
		require_once(APP_ROOT."/addons/manageComments/controllers/func.php");	
		require_once(APP_ROOT."/addons/manageComments/controllers/manageClientsManageComments.php");
		panelRequestManager::addFunctions('manageCommentsGet', 'manageCommentsReplyGet', 'manageCommentsGetRecent');
		regHooks('addonMenus', 'responseProcessors', 'taskTitleTemplate', 'getStatsRequestParams');
	}	
}

?>