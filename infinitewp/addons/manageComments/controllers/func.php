<?php

function manageCommentsAddonMenus(&$menus){
	//$menus[] = '<li><a class="navLinkUpdate navLinks optionSelect commentClass" page="comments"><span class="float-left">Comments</span><span class="update_count float-left droid700 commentCountClass" id="totalCommentCount">0</span></a></li>';
	$menus['manage']['subMenus'][] = array('page' => 'comments', 'displayName' => '<span class="float-left">Comments</span><span class="update_count float-left droid700 commentCountClass" id="totalCommentCount">0</span><div class="clear-both"></div>');
}

function manageCommentsGetRecent(){
	$sitesStats = panelRequestManager::getRawSitesStats();
	
	$recentComments = array();
	foreach($sitesStats as $siteID){
		$siteID['stats']['comments'] = (array)$siteID['stats']['comments'];	
			
		if(is_array($siteID['stats']['comments']['pending'])){
			foreach($siteID['stats']['comments']['pending'] as $key => $value){
				$comments = get_object_vars($value);
				$comments['comment_date'] = @date(Reg::get('dateFormatLong'), strtotime($comments['comment_date']));
				$recentComments['siteView'][$siteID['siteID']][$comments['comment_ID']] = $comments;
			}
		}
	}
	
	return $recentComments;
}

function manageCommentsGet(){
	
	$actionID = Reg::get('currentRequest.actionID');
		
	$commentDatas = DB::getFields("?:temp_storage", "data", "type = 'getComments' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'getComments' AND paramID = '".$actionID."'");
		
	if(empty($commentDatas)){
		return array();
	}
	$finalData = array();
	foreach($commentDatas as $data){
		$finalData = array_merge_recursive($finalData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($finalData);
		
	return $finalData;
}

function manageCommentsReplyGet(){
	
	$actionID = Reg::get('currentRequest.actionID');
		
	$commentDatas = DB::getFields("?:temp_storage", "data", "type = 'replyComments' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'replyComments' AND paramID = '".$actionID."'");
		
	if(empty($commentDatas)){
		return array();
	}
	$finalData = array();
	foreach($commentDatas as $data){
		$finalData = array_merge_recursive($finalData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($finalData);
	
	return $finalData;
}

function manageCommentsResponseProcessors(&$responseProcessor){
	$responseProcessor['comments']['get'] = 'getComments';
	$responseProcessor['comments']['edit'] = 'editComments';
	$responseProcessor['comments']['reply'] = 'replyComments';
	$responseProcessor['comments']['bulkEdit'] = 'bukEditComments';
}

function manageCommentsTaskTitleTemplate(&$template){
	//$template['comments']['add']['']		= "Create comments in <#sitesCount#> site<#sitesCountPlural#>";
	$template['comments']['get']['']				= "Load comments from <#sitesCount#> site<#sitesCountPlural#>";
	$template['comments']['reply']['']				= "Reply a comment";
	
	$template['comments']['bulkEdit']['approve']	= "Comments approved in <#sitesCount#> site<#sitesCountPlural#>";
	$template['comments']['bulkEdit']['trash']		= "Comments moved to trash in <#sitesCount#> site<#sitesCountPlural#>";
	$template['comments']['bulkEdit']['spam']		= "Comments marked as spam in <#sitesCount#> site<#sitesCountPlural#>";
	
	$template['comments']['edit']['approve'] 		= "Comment approved";
	$template['comments']['edit']['trash'] 			= "Comment moved to trash";
	$template['comments']['edit']['spam'] 			= "Comment marked as spam";
	
}

function manageCommentsGetStatsRequestParams(&$requestParams){
	$requestParams['item_filter']['get_stats'][] = array(
													   '0' => 'comments',
													   '1' => array
														   (
															   'numberposts' => ''
														   )
												   );
}


?>