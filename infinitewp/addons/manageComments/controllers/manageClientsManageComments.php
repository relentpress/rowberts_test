<?php
class manageClientsManageComments{

	public static function getCommentsProcessor($siteIDs, $params){//to after backup now completed, to upload backup file to FTP/S3/DropBox this method is used
		
		$type = "comments";
		$action = "get";
		$requestAction = "get_comments"; 
		
		foreach($siteIDs as $siteID){
			$siteData = getSiteData($siteID);
				
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => 'getComments', 'detailedAction' => $type);
			$events=1;
			
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $params;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			prepareRequestAndAddHistory($PRP);
	   }
	}
	
	public static function getCommentsResponseProcessor($historyID, $responseData){
		 
		responseDirectErrorHandler($historyID, $responseData);
		if(empty($responseData['success'])){
			return false;
		}
		
		$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
		$type = $historyData['type'];
		$actionID = $historyData['actionID'];
		$siteID = $historyData['siteID'];
		
		$data = array();
		if(!empty($responseData['success'][$type])){
			$items = $responseData['success'][$type];
			
			$siteView = array();
			$typeView = array();
			foreach($items as $key => $value){
				$items[$key]['comment_date'] = @date(Reg::get('dateFormatLong'), strtotime($value['comment_date']));
			}
						
			$data['siteView']['_'.$siteID] = $items;
			
			DB::insert("?:temp_storage", array('type' => 'getComments', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($data)));
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID." AND uniqueName = 'getComments'");
			return;
		}
		elseif($responseData['success']['total'] == 0){
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID." AND uniqueName = 'getComments'");
		}
		else{
			DB::update("?:history_additional_data", array('status' => 'error'), "historyID=".$historyID." AND uniqueName = 'getComments'");
		}
	}
	
	public static function editCommentsProcessor($siteIDs, $params){
		
		$type = "comments";
		$action = "edit";
		$requestAction = "action_comment"; 
		
		$requestParams = array('comment_id' => $params['commentID'], 'docomaction' => $params['action']);
		
		foreach($siteIDs as $siteID){
			$siteData = getSiteData($siteID);
				
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => 'editComments', 'detailedAction' => $params['action']);
			$events=1;
			
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			prepareRequestAndAddHistory($PRP);
	   }
	}
	
	public static function editCommentsResponseProcessor($historyID, $responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		if(empty($responseData['success'])){
			return false;
		}
		
		if(!empty($responseData['success'])){
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID." AND uniqueName = 'editComments'");
		}
	}
	
	public static function replyCommentsProcessor($siteIDs, $params){
		
		$type = "comments";
		$action = "reply";
		$requestAction = "replyto_comment"; 
		
		$requestParams = array('comment_id' => $params['commentID'], 'post_id' => $params['postID'], 'reply_text' => $params['replyText']);
		
		foreach($siteIDs as $siteID){
			$siteData = getSiteData($siteID);
				
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => 'replyComments', 'detailedAction' => $type);
			$events=1;
			
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			prepareRequestAndAddHistory($PRP);
	   }
		
	}
	
	public static function replyCommentsResponseProcessor($historyID, $responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		if(empty($responseData['success'])){
			return false;
		}
		
		$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
		$type = $historyData['type'];
		$actionID = $historyData['actionID'];
		$siteID = $historyData['siteID'];
		
		$data = array();
		
		if(!empty($responseData['success'])){
			$items = $responseData['success'];
						
			$data['siteView']['_'.$siteID] = $items;
			
			DB::insert("?:temp_storage", array('type' => 'replyComments', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($data)));
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID." AND uniqueName = 'replyComments'");
			return;
		}
		DB::update("?:history_additional_data", array('status' => 'error'), "historyID=".$historyID." AND uniqueName = 'replyComments'");
	}
	
	public static function bulkEditCommentsProcessor($siteIDs, $params){
				
		$type = "comments";
		$action = "bulkEdit";
		$requestAction = "bulk_action_comments"; 
		
		$events=1;
		foreach($params['siteID'] as $siteID => $value){
			$requestParams = $value;
			$requestParams['docomaction'] = $params['docomaction'];
						
			$siteData = getSiteData($siteID);
			
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => 'bulkEdit', 'detailedAction' => $params['docomaction']);
						
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			prepareRequestAndAddHistory($PRP);
	   }
	   
	}
	
	public static function bukEditCommentsResponseProcessor($historyID, $responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		if(empty($responseData['success'])){
			return false;
		}
		if(!empty($responseData['success'])){
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID." AND uniqueName = 'bulkEdit'");
		}
	}
}
manageClients::addClass('manageClientsManageComments');

?>