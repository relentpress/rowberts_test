<link rel="stylesheet" href="addons/bulkPublish/css/bulk_publish_styles.css?<?php echo $addon_version; ?>" type="text/css" />
<link rel="stylesheet" href="addons/bulkPublish/js/zebra/css/zebra_datepicker.css?<?php echo $addon_version; ?>" type="text/css" />

<script src="addons/bulkPublish/js/tiny_mce/tiny_mce.js?<?php echo $addon_version; ?>" type="text/javascript"></script>
<script src="addons/bulkPublish/js/tiny_mce/jquery.tinymce.js?<?php echo $addon_version; ?>" type="text/javascript"></script>
<script src="addons/bulkPublish/js/zebra/javascript/zebra_datepicker.js?<?php echo $addon_version; ?>" type="text/javascript"></script>
<script src="addons/bulkPublish/js/functions.js?<?php echo $addon_version; ?>" type="text/javascript"></script>
<script src="addons/bulkPublish/js/init.js?<?php echo $addon_version; ?>" type="text/javascript"></script>
<script src="addons/bulkPublish/js/load.js?<?php echo $addon_version; ?>" type="text/javascript"></script>
<script src="addons/bulkPublish/js/wp-langs-en.js?<?php echo $addon_version; ?>" type="text/javascript"></script>
<script src="addons/bulkPublish/js/wp-tinymce-schema.js?<?php echo $addon_version; ?>" type="text/javascript"></script>