<?php

function bulkPublishAddonMenus(&$menus){
	//$menus[] = '<li><a class="navLinks optionSelect" page="posts">Publish</a></li>';
	$menus['manage']['subMenus'][] = array('page' => 'posts', 'displayName' => 'Posts, Pages &amp; Links');
}

function bulkPublishGetLinks(){
	
	$actionID = Reg::get('currentRequest.actionID');
		
	$getDatas = DB::getFields("?:temp_storage", "data", "type = 'getLinks' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'getLinks' AND paramID = '".$actionID."'");
		
	if(empty($getDatas)){
		return array();
	}
	$finalData = array();
	foreach($getDatas as $data){
		$finalData = array_merge_recursive($finalData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($finalData);
		
	return $finalData;
}

function bulkPublishGetPosts(){
	
	$actionID = Reg::get('currentRequest.actionID');
		
	$getDatas = DB::getFields("?:temp_storage", "data", "type = 'getPosts' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'getPosts' AND paramID = '".$actionID."'");
		
	if(empty($getDatas)){
		return array();
	}
	$finalData = array();
	foreach($getDatas as $data){
		$finalData = array_merge_recursive($finalData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($finalData);
	
	return $finalData;
}

function bulkPublishGetPages(){
	
	$actionID = Reg::get('currentRequest.actionID');
		
	$getDatas = DB::getFields("?:temp_storage", "data", "type = 'getPages' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'getPages' AND paramID = '".$actionID."'");
		
	if(empty($getDatas)){
		return array();
	}
	$finalData = array();
	foreach($getDatas as $data){
		$finalData = array_merge_recursive($finalData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($finalData);
		
	return $finalData;
}

function bulkPublishResponseProcessors(&$responseProcessor){
	$responseProcessor['links']['add'] 			= 'addLinks';
	$responseProcessor['links']['get'] 			= 'getLinks';
	$responseProcessor['links']['delete'] 		= 'deleteLinks';
	$responseProcessor['links']['bulkDelete']	= 'bulkDeleteLinks';
	
	$responseProcessor['post']['add'] 			= 'addPostPage';
	$responseProcessor['posts']['get']			= 'getPosts';
	$responseProcessor['posts']['delete'] 		= 'deletePosts';
	$responseProcessor['posts']['bulkDelete'] 	= 'bulkDeletePosts';
	
	$responseProcessor['page']['add']			= 'addPostPage';
	$responseProcessor['pages']['get'] 			= 'getPages';
	$responseProcessor['pages']['delete'] 		= 'deletePages';
	$responseProcessor['pages']['bulkDelete']	= 'bulkDeletePages';
	
}

function bulkPublishTaskTitleTemplate(&$template){
	$template['links']['add']['']			= "Add links in <#sitesCount#> site<#sitesCountPlural#>";
	$template['links']['get']['']			= "Get links from <#sitesCount#> site<#sitesCountPlural#>";
	$template['links']['delete']['']		= "Delete a links";
	$template['links']['bulkDelete']['']	= "Delete links from <#sitesCount#> site<#sitesCountPlural#>";
	
	$template['post']['add']['']			= "Add a post in <#sitesCount#> site<#sitesCountPlural#>";	
	$template['posts']['get']['']			= "Get posts from <#sitesCount#> site<#sitesCountPlural#>";
	$template['posts']['delete'][''] 		= "Delete a post";
	$template['posts']['bulkDelete'][''] 	= "Delete posts from <#sitesCount#> site<#sitesCountPlural#>";
	
	$template['page']['add']['']			= "Add a page in <#sitesCount#> site<#sitesCountPlural#>";
	$template['pages']['get'][''] 			= "Get pages from <#sitesCount#> site<#sitesCountPlural#>";
	$template['pages']['delete'][''] 		= "Delete a page";
	$template['pages']['bulkDelete'][''] 	= "Delete pages from <#sitesCount#> site<#sitesCountPlural#>";
	
}

?>