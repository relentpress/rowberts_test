<?php
class manageClientsBulkPublish{
	
	public static function addLinksProcessor($siteIDs,$params){
		  
		  $type = "links";
		  $action = "add";
		  $requestAction = "add_link";
			  
			  $requestParams = array('name' => $params['linkName'], 'url' => $params['URL'], 'description' => $params['description'], 'link_target' => $params['linkTarget'], 'link_category' => explode(',', $params['linkCategory']));	
			  
			  $historyAdditionalData = array();
			  $historyAdditionalData[] = array('uniqueName' => $params['URL'], 'detailedAction' => $type);
			  
			  foreach($siteIDs as $siteID){
			  $siteData = getSiteData($siteID);
			  $events=1;
			  
			  $PRP = array();
			  $PRP['requestAction'] 	= $requestAction;
			  $PRP['requestParams'] 	= $requestParams;
			  $PRP['siteData'] 		= $siteData;
			  $PRP['type'] 			= $type;
			  $PRP['action'] 			= $action;
			  $PRP['events'] 			= $events;
			  $PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			 prepareRequestAndAddHistory($PRP);
		  }
	  }
	  public static function addLinksResponseProcessor($historyID,$responseData){
		  
		  responseDirectErrorHandler($historyID, $responseData);

		  if( empty($responseData['success'])  ){
			  return false;
		  }
		  
		  $updateLink = array();
		  if( !empty($responseData['success']) ){
			  $updateLink['resultID'] = $responseData['success'];
			  $updateLink['status'] = 'success';
			  DB::update("?:history_additional_data", $updateLink, "historyID=".$historyID);	
		  }
	  }
	  
	  public static function getLinksProcessor($siteIDs,$params){
		  
		  $type = "links";
		  $action = "get";
		  $requestAction = "get_links";
			  
		  $requestParams = array('filter_links' => $params['filterLinks']);	
		  
		  $historyAdditionalData = array();
		  $historyAdditionalData[] = array('uniqueName' => 'getLinks', 'detailedAction' => $type);
		  
		  foreach($siteIDs as $siteID){
			  $siteData = getSiteData($siteID);
			  $events=1;
			  
			  $PRP = array();
			  $PRP['requestAction'] 	= $requestAction;
			  $PRP['requestParams'] 	= $requestParams;
			  $PRP['siteData'] 		= $siteData;
			  $PRP['type'] 			= $type;
			  $PRP['action'] 			= $action;
			  $PRP['events'] 			= $events;
			  $PRP['sendAfterAllLoad'] = true;
			  $PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			  prepareRequestAndAddHistory($PRP);
		  }
	  }
	  
	  public static function getLinksResponseProcessor($historyID,$responseData){
		  
		 
		  
		  responseDirectErrorHandler($historyID, $responseData);
		if(empty($responseData['success'])){
			return false;
		}
		
		$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
		$type = $historyData['type'];
		$actionID = $historyData['actionID'];
		$siteID = $historyData['siteID'];
		
		$data = array();
		if(!empty($responseData['success'][$type])){
			$items = $responseData['success'][$type];
			
			$typeView = array();
	
			$data['siteView']['_'.$siteID] = $items;
						 
			DB::insert("?:temp_storage", array('type' => 'getLinks', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($data)));
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID." AND uniqueName = 'getLinks'");
			return;
		}
		DB::update("?:history_additional_data", array('status' => 'error'), "historyID=".$historyID." AND uniqueName = 'getLinks'");
	  }
	  
	  public static function deleteLinksProcessor($siteIDs,$params){
		  
		  $type = "links";
		  $action = "delete";
		  $requestAction = "delete_link";
			  
			  $requestParams = array('link_id' => $params['linkID']);	
			  
			  $historyAdditionalData = array();
			  $historyAdditionalData[] = array('uniqueName' => "deleteLink", 'detailedAction' => $type);
			  
			  foreach($siteIDs as $siteID){
			  $siteData = getSiteData($siteID);
			  $events=1;
			  
			  $PRP = array();
			  $PRP['requestAction'] 	= $requestAction;
			  $PRP['requestParams'] 	= $requestParams;
			  $PRP['siteData'] 		= $siteData;
			  $PRP['type'] 			= $type;
			  $PRP['action'] 			= $action;
			  $PRP['events'] 			= $events;
			  $PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			  return prepareRequestAndAddHistory($PRP);
		  }
	  }
	  
	  public static function deleteLinksResponseProcessor($historyID,$responseData){
		  responseDirectErrorHandler($historyID, $responseData);
		  
		  if( empty($responseData['success'])  ){
			  return false;
		  }
		  
		  $updateLink = array();
		  if( !empty($responseData['success']) ){
			  //$updateLink['resultID'] = $responseData['success'];
			  $updateLink['status'] = 'success';
			  DB::update("?:history_additional_data", $updateLink, "historyID=".$historyID);	
		  }
	  }
	  
	  public static function bulkDeleteLinksProcessor($siteIDs, $params){
				
		$type = "links";
		$action = "bulkDelete";
		$requestAction = "delete_links"; 
	
		foreach($params['siteID'] as $siteID => $value){
			$requestParams = $value;
			$requestParams['deleteaction'] = $params['docomaction'];
						
			$siteData = getSiteData($siteID);
			
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => 'bulkDelete', 'detailedAction' => $type);
			$events=1;
			
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			prepareRequestAndAddHistory($PRP);
	   }
	   
	}
	
	public static function bulkDeleteLinksResponseProcessor($historyID, $responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		if(empty($responseData['success'])){
			return false;
		}
		if(!empty($responseData['success'])){
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID." AND uniqueName = 'bulkDelete'");
		}
	}
	
	// Add new post and page .
	  public static function addPostProcessor($siteIDs, $params){
		  $type = "post";
		  self::addPostPageProcessor($siteIDs, $params, $type); 
		  
	  }
	  public static function addPageProcessor($siteIDs, $params){
		  $type = "page";
		  self::addPostPageProcessor($siteIDs, $params, $type); 
		  
	  }
	  
	  public static function addPostPageProcessor($siteIDs, $params, $type){
		  
		

		$html = $params['postContent'];
		
	 			
		  $action = "add";
		  $requestAction = "create_post";
		  
		  $requestParams =  array
							   ( 'post_data' => array
									   (
										   'post_data' => array
											   (
												   'post_title' => $params['postTitle'],
												   
												   'post_content' => $params['postContent'],
												   
												   'post_date' => $params['postDate'],
												   
												   'post_date_gmt' => (!empty($params['postDate']) ? gmdate("Y-m-d H:i:s", strtotime($params['postDate'])) : ''),
												   
												   'post_status' => $params['type'],
												   
												   'comment_status' => ($params['discussion']['allowComments'] == 1) ? 'open' : 'closed' ,
												   
												   'post_password' => $params['postPassword'],
												   
												   /*'to_ping' => $params['postTitle'],
												   
												   'pinged' => $params['postTitle'],*/
												   
												   'post_name' => $params['editPostSlug'],
												   
												   'iwp_post_edit' => $params['overWritePost'],
												   
       											   'iwp_force_publish' => '1',
												   
        										   'iwp_match_by' => $params['postMatch'],
												   
												   'post_type' => $type,
												   
												   'post_excerpt' => $params['excerpt'],
												   
												   'tags_input' => $params['tags'],
											   ),
						
										  'post_extras' => array
											   (
												   'post_meta' => array
													   (/*
														   '_edit_last' => array
															   (
																   '0' => ''
															   ),
						
														   '_edit_lock' => array
															   (
																   '0' => ''
															   )*/
															
															$params['custom']['field'] => array
																	 (															
																		0 => $params['custom']['data'],
																	  ),
													  	),
					  
												   /*'post_atta_images' => $img,
						
												  'post_upload_dir' => array
													   (
														   'path' => '/sites/wordpress/wp-content/uploads/2011/12',
														   'url' => 'http://revmakx.com/wordpress/wp-content/uploads/2011/12',
														   'subdir' => '/2011/12',
														   'basedir' => '/sites/wordpress/wp-content/uploads',
														   'baseurl' => 'http://revmkax.com/wordpress/uploads',
														   'error' => 'false',
													   ),*/
						 /*
												   'post_checksum' => 'b0f44adfc13fd8f58078dfa51940a5f8',*/
												   'featured_img' => $params['featuredImg'],
												   'post_categories' => $params['categories']
											   )
									  ));
									   
			  $historyAdditionalData = array();
			  $historyAdditionalData[] = array('uniqueName' => 'create', 'detailedAction' => $type);
			  
			  foreach($siteIDs as $siteID){
			  $siteData = getSiteData($siteID);
			  $events=1;
			  
			  $PRP = array();
			  $PRP['requestAction'] 	= $requestAction;
			  $PRP['requestParams'] 	= $requestParams;
			  $PRP['siteData'] 			= $siteData;
			  $PRP['type'] 				= $type;
			  $PRP['action'] 			= $action;
			  $PRP['events'] 			= $events;
			  $PRP['historyAdditionalData'] 	= $historyAdditionalData;
			  
			  prepareRequestAndAddHistory($PRP);
			  
			  }
	  }
	  
	  public static function addPostPageResponseProcessor($historyID,$responseData){	  
		  
		  responseDirectErrorHandler($historyID, $responseData);
		  
		  if( empty($responseData['success']) ){
			  return false;
		  }
		  
		  if( !empty($responseData['success']) ){
			  DB::update("?:history_additional_data", array("status" => "success", "resultID" => $responseData['success']), "historyID=".$historyID);	
		  }
	  }
	  
	   public static function getPostsProcessor($siteIDs,$params){
		   		  
		  $type = "posts";
		  $action = "get";
		  $requestAction = "get_posts";
		  
		  if(!empty($params['dates'])){
			$dates 		= explode('-', $params['dates']);
			$fromDate 	= date('Y-m-d',strtotime(trim($dates[0])));
			$toDate		= date('Y-m-d',strtotime(trim($dates[1])));
			
		  }
		
			  $requestParams = array('filter_posts' => $params['filterPosts'], 'iwp_get_posts_date_from' => (!empty($fromDate) ? $fromDate : ''), 'iwp_get_posts_date_to' => (!empty($toDate) ? $toDate : ''), 'iwp_get_posts_publish' => $params['publish'],'iwp_get_posts_pending' => $params['pending'],'iwp_get_posts_private' => $params['private'],'iwp_get_posts_future' => $params['future'],'iwp_get_posts_draft' => $params['draft'],'iwp_get_posts_trash' => $params['trash'], 'iwp_get_posts_range' => 0);
			  
			  $historyAdditionalData = array();
			  $historyAdditionalData[] = array('uniqueName' => 'getPosts', 'detailedAction' => $type);
			  
			  foreach($siteIDs as $siteID){
			  $siteData = getSiteData($siteID);
			  $events=1;
			  
			  $PRP = array();
			  $PRP['requestAction'] 	= $requestAction;
			  $PRP['requestParams'] 	= $requestParams;
			  $PRP['siteData'] 		= $siteData;
			  $PRP['type'] 			= $type;
			  $PRP['action'] 			= $action;
			  $PRP['events'] 			= $events;
			  $PRP['sendAfterAllLoad'] = true;
			  $PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			  prepareRequestAndAddHistory($PRP);
		  }
	  }
	  
	  public static function getPostsResponseProcessor($historyID,$responseData){
		  
		  responseDirectErrorHandler($historyID, $responseData);
		if(empty($responseData['success'])){
			return false;
		}
		
		$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
		$type = $historyData['type'];
		$actionID = $historyData['actionID'];
		$siteID = $historyData['siteID'];
		
		$data = array();
		
		if(!empty($responseData['success'][$type])){
	
			$items = $responseData['success'][$type];
			$siteView = array();
			
			foreach($items as $key => $postData){
				$postData['post_date'] = @date(Reg::get('dateFormatLong'), strtotime($postData['post_date']));
				$siteView[$postData['post_status']][] = $postData;
			}
			$data['siteView']['_'.$siteID] = $siteView;
		
			DB::insert("?:temp_storage", array('type' => 'getPosts', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($data)));
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID." AND uniqueName = 'getPosts'");
		}
		elseif($responseData['success']['total']['total_num'] == 0){
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID." AND uniqueName = 'getPosts'");
		}
		else{
			DB::update("?:history_additional_data", array('status' => 'error'), "historyID=".$historyID." AND uniqueName = 'getPosts'");
		}
	  }

//Delete, Delete perm, Restore . . .
	   public static function deletePostsProcessor($siteIDs,$params){
		   		  
		  $type = "posts";
		  $action = "delete";
		  $requestAction = "delete_post";
			  
			  $requestParams = array('post_id' => $params['postID'], 'action' => $params['action']);	
			  
			  $historyAdditionalData = array();
			  $historyAdditionalData[] = array('uniqueName' => "deletePost", 'detailedAction' => $type);
			  
			  foreach($siteIDs as $siteID){
				  $siteData = getSiteData($siteID);
				  $events=1;
				  
				  $PRP = array();
				  $PRP['requestAction'] 	= $requestAction;
				  $PRP['requestParams'] 	= $requestParams;
				  $PRP['siteData'] 			= $siteData;
				  $PRP['type'] 				= $type;
				  $PRP['action'] 			= $action;
				  $PRP['events'] 			= $events;
				  $PRP['historyAdditionalData'] 	= $historyAdditionalData;
							
				  return prepareRequestAndAddHistory($PRP);
			  }
	  }
	  
	  public static function deletePostsResponseProcessor($historyID,$responseData){
		  responseDirectErrorHandler($historyID, $responseData);
		  
		  if( empty($responseData['success'])  ){
			  return false;
		  }
		
		  if( !empty($responseData['success']) ){
			  DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID);	
		  }
	  }

//bulk trash, bulk delete perm.	  - for posts & pages
	  public static function bulkDeletePostsProcessor($siteIDs, $params){
		  
		$type = "posts";
		$action = "bulkDelete";
		$requestAction = "delete_posts";
	
		foreach($params['siteID'] as $siteID => $value){
			
			$requestParams = $value;
			$requestParams['deleteaction'] = $params['action'];
						
			$siteData = getSiteData($siteID);
			
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => $params['action'], 'detailedAction' => $type);
			$events=1;
			
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			prepareRequestAndAddHistory($PRP);
	   }
	   
	}
	
	public static function bulkDeletePostsResponseProcessor($historyID, $responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		if(empty($responseData['success'])){
			return false;
		}
				  
		if(!empty($responseData['success'])){
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID);
		}
	}

//Get Pages
	public static function getPagesProcessor($siteIDs,$params){
		   		  
		  $type = "pages";
		  $action = "get";
		  $requestAction = "get_pages";
			  
		  $requestParams = array('filter_pages' => $params['filterPages'], 'iwp_get_pages_date_from' => $params['pagesDateFrom'], 'iwp_get_pages_date_to' => $params['pagesDateTo'], 'iwp_get_pages_publish' => $params['publish'],'iwp_get_pages_pending' => $params['pending'],'iwp_get_pages_private' => $params['private'],'iwp_get_pages_future' => $params['future'],'iwp_get_pages_draft' => $params['draft'],'iwp_get_pages_trash' => $params['trash'], 'iwp_get_pages_range' => 0);
		  
		  $historyAdditionalData = array();
		  $historyAdditionalData[] = array('uniqueName' => 'getPages', 'detailedAction' => $type);
		  
		  foreach($siteIDs as $siteID){
			  $siteData = getSiteData($siteID);
			  $events=1;
			  
			  $PRP = array();
			  $PRP['requestAction'] 	= $requestAction;
			  $PRP['requestParams'] 	= $requestParams;
			  $PRP['siteData'] 		= $siteData;
			  $PRP['type'] 			= $type;
			  $PRP['action'] 			= $action;
			  $PRP['events'] 			= $events;
			  $PRP['sendAfterAllLoad'] = true;
			  $PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			  prepareRequestAndAddHistory($PRP);
		  }
	  }
	  
	  public static function getPagesResponseProcessor($historyID,$responseData){
		  
		  responseDirectErrorHandler($historyID, $responseData);
		  
		if(empty($responseData['success'])){
			return false;
		}
		
		$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
		$type = $historyData['type'];
		$actionID = $historyData['actionID'];
		$siteID = $historyData['siteID'];
		
		$data = array();
		if(!empty($responseData['success'][$type])){
			$items = $responseData['success'][$type];
			
			$siteView = array();
			
			foreach($items as $key => $pageData){
				$pageData['post_date'] = @date(Reg::get('dateFormatLong'), strtotime($pageData['post_date']));
				$siteView[$pageData['post_status']][] = $pageData;
			}
			
			$data['siteView']['_'.$siteID] = $siteView;
						
			DB::insert("?:temp_storage", array('type' => 'getPages', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($data)));
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID." AND uniqueName = 'getPages'");
		}
		elseif($responseData['success']['total']['total_num'] == 0){
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID." AND uniqueName = 'getPages'");
		}
		else{
			DB::update("?:history_additional_data", array('status' => 'error'), "historyID=".$historyID." AND uniqueName = 'getPages'");
		}
	  }

//Delete, Delete perm, Restore.  - for pages
	   public static function deletePagesProcessor($siteIDs,$params){
	  
		  $type = "pages";
		  $action = "delete";
		  $requestAction = "delete_page";
			  
		  $requestParams = array('post_id' => $params['postID'], 'action' => $params['action']);	
		  
		  $historyAdditionalData = array();
		  $historyAdditionalData[] = array('uniqueName' => "delete_pages", 'detailedAction' => $type);
		  
		  foreach($siteIDs as $siteID){
			  $siteData = getSiteData($siteID);
			  $events=1;
			  
			  $PRP = array();
			  $PRP['requestAction'] 	= $requestAction;
			  $PRP['requestParams'] 	= $requestParams;
			  $PRP['siteData'] 		= $siteData;
			  $PRP['type'] 			= $type;
			  $PRP['action'] 			= $action;
			  $PRP['events'] 			= $events;
			  $PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			  return prepareRequestAndAddHistory($PRP);
		  }
	  }
	  
	  public static function deletePagesResponseProcessor($historyID,$responseData){
		  responseDirectErrorHandler($historyID, $responseData);
		  
		  if( empty($responseData['success'])  ){
			  return false;
		  }
		  
		  $updateLink = array();
		  if( !empty($responseData['success']) ){
			  //$updateLink['resultID'] = $responseData['success'];
			  $updateLink['status'] = 'success';
			  DB::update("?:history_additional_data", $updateLink, "historyID=".$historyID);	
		  }
	  }
	  
	  public static function bulkDeletePagesProcessor($siteIDs, $params){
		  
		$type = "pages";
		$action = "bulkDelete";
		$requestAction = "delete_posts"; 
	
		foreach($params['siteID'] as $siteID => $value){
			
			$requestParams = $value;
			$requestParams['deleteaction'] = $params['action'];
						
			$siteData = getSiteData($siteID);
			
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => $params['action'], 'detailedAction' => $type);
			$events=1;
			
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
						
			prepareRequestAndAddHistory($PRP);
	   }
	   
	}
	
	public static function bulkDeletePagesResponseProcessor($historyID, $responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		if(empty($responseData['success'])){
			return false;
		}
				  
		if(!empty($responseData['success'])){
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID);
		}
	}

	
	  
}
manageClients::addClass('manageClientsBulkPublish');

?>