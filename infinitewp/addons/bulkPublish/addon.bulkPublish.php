<?php

/*
slug: bulkPublish
version: 1.0.3
*/

class addonBulkPublish{
	
	private static $version = '1.0.3';

	public static function version(){
		return self::version;	
	}
	
	public static function init(){
		require_once(APP_ROOT."/addons/bulkPublish/controllers/func.php");	
		require_once(APP_ROOT."/addons/bulkPublish/controllers/manageClientsBulkPublish.php");
		panelRequestManager::addFunctions('bulkPublishGetLinks', 'bulkPublishGetPosts', 'bulkPublishGetPages');
		regHooks('addonMenus', 'responseProcessors', 'taskTitleTemplate');
	}	
}

?>