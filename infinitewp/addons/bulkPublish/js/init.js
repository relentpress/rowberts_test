
$(".managePosts").live('click',function() {
activeItem=$('.typeSelectorPosts.active').attr('utype');	  
$("#processType").text('MANAGE');
if(activeItem!='links')
	 {
$(".optionsContent").html('<div style="padding:10px 0;"> <table width="70%" border="0" class="bulk_post_form_table history"> <tr> <td width="27%"><div class="float-right" style="text-align:right; color:#737987; padding-top: 10px;"><span class="droid700" style="font-size:11px;">SEARCH</span> <span class="itemName itemUpper" style="text-transform:uppercase; font-weight:700; font-size:11px;">'+activeItem+'</span><br /> Leave blank to load all <span class="itemName">'+activeItem.toTitleCase()+'</span></div></td> <td width="73%"><div class="float-left" style="margin-right:10px"> <input name="" type="text" onenterbtn=".fetchposts" class="fetchPostTxt txt onenter " style="width: 300px; height: 17px; margin: 5px;" /> </div></td> </tr> <tr> <td align="right"><span class="droid700" style="font-size:11px;">FILTER BY DATE</span></td> <td><div id="widget" class="txt float-right"> <div id="widgetField" > <span id="dateContainer"class="fieldDate">Select Date Range<div class="cal_arrow"></div> </span> </div> <div id="widgetCalendar" style="height:0"> </div> </div><span class="refreshDateField reset_date rep_sprite" aria-describedby="ui-tooltip-1"><span class="rep_sprite_backup"></span></span></td> </tr> <tr> <td align="right"><span class="droid700" style="font-size:11px;">LOAD ONLY</span></td> <td><ul class="checkbox_filters float-right"> <li><a class="rep_sprite selectPublishedPosts selPosts active" status="on">Published</a></li> <li><a class="rep_sprite selectPendingPosts selPosts "status="">Pending</a></li> <li><a class="rep_sprite selectPrivatePosts selPosts " status="">Private</a></li> <li><a class="rep_sprite selectScheduledPosts selPosts" status="">Scheduled</a></li> <li><a class="rep_sprite selectDraftPosts selPosts" status="">Draft</a></li> <li><a class="rep_sprite selectTrashPosts selPosts " status="">Trash</a></li> </ul></td> </tr> <tr> <td>&nbsp;</td> <td><div class="btn_action float-left fetchPostCont" ><a class="rep_sprite fetchPosts " style="margin: 8px 30px 8px 0px; width: 66px; text-align:center;">Load <span class="itemName">'+activeItem+'</span></a></div></td> </tr> </table> <div class="clear-both"></div> <div class="actionContent siteSearch selPosts" style="display:none;margin-top:10px "></div> </div> ');    }
else
{
	$(".optionsContent").html('<div style="padding:10px 0;"> <div class="btn_action float-right fetchPostCont"><a class="rep_sprite fetchPosts"style="margin: 8px 30px 8px 0px; width: 60px;" >Load <span class="itemName">'+activeItem+'</span></a></div> <div class="float-right" style="margin-right:10px"> <input name="" type="text" onenterbtn=".fetchposts" class="fetchPostTxt txt onenter " style="width: 300px; height: 17px; margin: 5px;" /> </div> <div class="float-right" style="text-align:right; color:#737987; padding-top: 10px;"><span class="droid700" style="font-size:11px;">SEARCH <span class="itemName itemUpper">'+activeItem.toUpperCase()+'</span><br /> Leave blank to load all <span class="itemName">'+activeItem.toTitleCase()+'</span></div><div class="clear-both"></div><div class="actionContent siteSearch selPosts" style="display:none;margin-top:10px "></div> </div>');
}
$(".optionsContent").addClass('result_block_noborder').hide();
$(".advancedInstallOptions").hide();
$(".siteSelectorContainer").html(siteSelectorVar);
if($(".siteSelectorContainer").hasClass('recent_type'))
{
$(".siteSelectorContainer").removeClass('recent_type');
$(".siteSelectorContainer").show();
}
				var now3 = new Date();
				now3.addDays(-4);
				var now4 = new Date();
$('#widgetCalendar').DatePicker({
					flat: true,
					format: 'b d, Y',
					date: [new Date(now3), new Date(now4)],
					calendars:1,
					mode: 'range',
					starts: 1,
					onChange: function(formated)
					 {
							$('#widgetField #dateContainer').get(0).innerHTML = formated.join(' - ');
							$("#widgetField #dateContainer").append('<div class="cal_arrow"></div>');
					 }
				});
siteSelectorNanoReset();
});


$(".fetchPosts").live('click',function() {

if(!$(this).hasClass('disabled'))
{
$(this).addClass('disabled');	
var tempArray={};
params={};
var requireDataArray={};
tempArray['args']={};
searchText=$(".fetchPostTxt").val();
if(activeItem=='posts')
{
params['filterPosts']='';
params['postsDateFrom']='';
params['postsDateTo']='';
params['filterPosts']=searchText;
tempArray['action']="getPosts";
requireDataArray['bulkPublishGetPosts']=1;
}
else if(activeItem=='pages')
{
params['filterPages']='';
params['pagesDateFrom']='';
params['pagesDateTo']='';
params['filterPages']=searchText;
tempArray['action']="getPages";
requireDataArray['bulkPublishGetPages']=1;
}
else if(activeItem=='links')
{
params['filterLinks']='';
params['linksDateFrom']='';
params['linksDateTo']='';
params['filterLinks']=searchText;
tempArray['action']="getLinks";
requireDataArray['bulkPublishGetLinks']=1;
}
params['publish']=$('.selectPublishedPosts').attr("status");
params['pending']=$('.selectPendingPosts').attr("status");
params['private']=$('.selectPrivatePosts').attr("status");
params['future']=$('.selectScheduledPosts').attr("status");
params['draft']=$('.selectDraftPosts').attr("status");
params['trash']=$('.selectTrashPosts').attr("status");
var dateField=$('.fieldDate').text();
if(dateField=='Select Date Range ')
dateField='';
else
dateField=$('.fieldDate').text();
params['dates']=dateField;
siteArray=getSelectedSites();
$(this).addClass('active');
$(this).attr('tempname',$(this).text());
$(this).append('<div class="btn_loadingDiv"></div>');
tempArray['args']['siteIDs']=siteArray;
tempArray['args']['params']=params;

tempArray['requiredData']= requireDataArray;
if(activeItem=='links')
{
	doCall(ajaxCallPath,tempArray,'formArrayLoadLinks','json',"none");
}
else
{
doCall(ajaxCallPath,tempArray,'formArrayLoadPosts','json',"none");
}
$(this).addClass('active');
}

});

$("#save_group_changes_post").live('click',function() {
	var postTitle,postContent,excerpt,customFields,publish={},postsByTag={},editPostSlug,categories,tags,img,overWriteFlag,requireDataArray={},tempArray={},params={},action;
	params['discussion']={};
	params['publish']={};
	params['postsByTag']={};
	params['custom']={};
	siteArray=getSelectedSites();
	tempArray['args']={};
	requireDataArray={};
	$(this).addClass('disabled');
	if(activeItem=='posts')
	action='addPost';
	else
	action='addPage';
	
	params['postTitle']=$('.post_title').val();
	if($('.visualForm').hasClass('active'))
	{   
	    testVar = tinyMCE.get('content');
	    var editVal = testVar.getContent();
		params['postContent']=editVal;
	}
	else
	{
	params['postContent']=$('#content').val();
	}
	params['excerpt']=$('.op_cont.excerpt textarea').val();
	if($('.allow_comments').hasClass('active'))
	params['discussion']['allowComments']=1;
	if($('.allow_trackbacks').hasClass('active'))
	params['discussion']['allowTrackbacks']=1;
	params['publish']['status']= $('.status .radio_btn.active').attr('val');
	params['publish']['visibility']=$('.visibility .radio_btn.active').attr('val');
	params['publish']['publish']=$('.publish_post .radio_btn.active').attr('val');
	params['postsByTag']['widget_title']=$('.widget_title').val();
	params['postsByTag']['widget_tags']=$('.widget_tags').val();
	params['editPostSlug']=$('.editPostSlug').val();
	params['categories']=$('.categories').val();
	params['tags']=$('.tags_posts').val();
	params['featuredImg']=$('.post_img').val();
	params['postMatch']=$('.postMatch .radio_btn.active').attr('val');
	params['postPassword']=$('.passVal').val();
	params['custom']['field']='';
	params['custom']['data']=$('.custom_fields textarea').val();
	    if(($('.customFieldsDate').val()!='')&&($('.postHours').val())&&($('.postMins').val()))
		{
			params['postDate']=$('.customFieldsDate').val()+' '+$('.postHours').val()+':'+$('.postMins').val()+':'+'00';
		}
		else
		{
			params['postDate']='';
		}
	if($('.overWrite_post').hasClass('active'))
	params['overWritePost']='1';
	else
	params['overWritePost']='0';
	if((params['publish']['visibility']=='Password protected')||(params['publish']['visibility']=='Public'))
	{
	params['type']='publish';
	}
	else if(params['publish']['visibility']=='Private')
	{
		params['type']='private';
	}
	if(params['publish']['publish']=='Schedule')
	{
		params['type']='future';
	}
	$(this).addClass('active');
	tempArray['action']=action;
	tempArray['args']['siteIDs']=siteArray;
    tempArray['args']['params']=params;
	tempArray['requiredData']= requireDataArray;
	doHistoryCall(ajaxCallPath,tempArray,'processPublishPost');
    return false;
});

$(".addLink").live('click',function() {
	$(this).addClass('disabled');
	var tempArray={},params={},siteArray;
	tempArray['args']={};
	siteArray=getSelectedSites();
	params['linkName']=$('.linkName').val();
	params['URL']=$('.linkURL').val();
	params['description']=$('.linkDes').val();
	params['linkCategory']=$('.linkCat').val();
	params['linkTarget']=$('.itemPost.active').text();
	tempArray['action']='addLinks';
	tempArray['args']['siteIDs']=siteArray;
    tempArray['args']['params']=params;
	tempArray['requiredData']= {};
	doHistoryCall(ajaxCallPath,tempArray,'processPublishPost');
	return false;
});

$(".typePosts").live('click',function() {
if(!$(this).hasClass('active'))
{
optionSelect(this);
$
activeItem=$(this).attr('utype');
$('.itemName').text(activeItem.toTitleCase());
$('.itemNameLower').text(activeItem.toLowerCase());
$('.itemUpper').text(activeItem.toUpperCase());
if($(".managePosts").hasClass('active'))
{
$(".managePosts").removeClass('active')
$(".managePosts").click();
}
if($(".addNew").hasClass('active'))
{
$(".addNew").removeClass('active')
$(".addNew").click();
}
}
return false;
});

$(".typePages").live('click',function() {
if(!$(this).hasClass('active'))
{
optionSelect(this);
activeItem=$(this).attr('utype');
$('.itemName').text(activeItem.toTitleCase());
$('.itemNameLower').text(activeItem.toLowerCase());
$('.itemUpper').text(activeItem.toUpperCase());
if($(".managePosts").hasClass('active'))
{
$(".managePosts").removeClass('active')
$(".managePosts").click();
}
if($(".addNew").hasClass('active'))
{
$(".addNew").removeClass('active')
$(".addNew").click();
}
}
return false;
});

$(".typeLinks").live('click',function() {
if(!$(this).hasClass('active'))
{
optionSelect(this);
activeItem=$(this).attr('utype');
$('.itemName').text(activeItem.toTitleCase());
$('.itemNameLower').text(activeItem.toLowerCase());
$('.itemUpper').text(activeItem.toUpperCase());
if($(".managePosts").hasClass('active'))
{
$(".managePosts").removeClass('active')
$(".managePosts").click();
}
if($(".addNew").hasClass('active'))
{
$(".addNew").removeClass('active')
$(".addNew").click();
}
}
return false;
});
$(".addNew").live('click',function(obj) {
//$(".siteSelectorContainer").html(siteSelectorRestrictVar);
obj.stopPropagation();
$('.website_cont').each(function(){
	$(this).removeClass('active');
});
siteSelectorNanoReset();
$("#processType").text('ADD');
$(".optionsContent").removeClass('result_block_noborder');
loadAddNewPanel(activeItem);
$(".optionsContent").hide();
$(".advancedInstallOptions").hide();
});

$(".selectPostsAdd").live('click',function() {

	if($(this).hasClass('active'))
	{
	$(this).removeClass('active');
	
	}
	else
	{
	$(this).addClass('active');
	
	}
	

});
$(".allow_trackbackss").live('click',function() {
	$(this).addClass('active');
});
$(".status .radio_btn").live('click',function() {
/*	var par=$(this).closest('td');
	var rad=$('.radio_btn');*/
	$('.status .radio_btn').each(function(){
		$(this).removeClass('active');
	});
	$(this).addClass('active');
	var status_val=$(this).attr('val');
	$('.status.current_value').html(status_val);
});

$(".visibility .radio_btn").live('click',function() {
	$('.visibility .radio_btn').each(function(){
		$(this).removeClass('active');
	});
	$(this).addClass('active');
	var visibility_val=$(this).attr('val');
	if(visibility_val=='Password protected')
	{
		$('.passVal').show();
	}
	else
	{
		$('.passVal').hide();
	}
	if(visibility_val=='Private')
	{
		$(".schedPost").hide();
	}
	else
	{
		$(".schedPost").show();
	}
	$('.visibility.current_value').html(visibility_val);
});

$(".publish_post .radio_btn").live('click',function() {
	$('.publish_post .radio_btn').each(function(){
		$(this).removeClass('active');
	});
	$(this).addClass('active');
	var publish_post_val=$(this).attr('val');
	if(publish_post_val == 'Schedule')
	{
		$(".dateChooser").show();
	}
	else
	{
		$(".dateChooser").hide();
	}
	$('.current_value.publish_post').html(publish_post_val);
});

$(".postMatch .radio_btn").live('click',function() {
	$('.postMatch .radio_btn').each(function(){
		$(this).removeClass('active');
	});
	$(this).addClass('active');
	
});


$(".op_open").live('click',function(obj) {
	resetBottomToolbar();
	obj.stopPropagation();
});

$(".op_edit.post").live('click',function() {
    var pid = $(this).attr('pid');
	var sid =  $(this).attr('sid');
	if(pid!=undefined)
	{
    loadAdminHere(sid,3,pid);
	}
	return false;
});
$(".op_edit.link").live('click',function() {
    var lid = $(this).attr('lid');
	var sid =  $(this).attr('sid');
	if(lid!=undefined)
	{
    loadAdminHere(sid,4,lid);
	}
	return false;
	
});
$(".op_trash").live('click',function() {
	if(($(this).attr('type')=='posts')||($(this).attr('type')=='pages'))
	{   
	    
		$(this).addClass('disabled');
        var siteID,dID,type,action,name,valArray,act,requestData,cidi;
    	var siteI=$(this).attr('siteid');
	    cidi=$(this).attr('postID');
		action=$(this).attr('type');
		if(action=='posts')
		{
			act='deletePosts';
		}
		else
		{
			act='deletePages';
		}
    	siteID={};
    	siteID['0']=siteI;
     	valArray={};
	    valArray['postID'] =cidi ;
        valArray['action'] ='delete';

	    requestData={};
	    requestData['args']={};
	    requestData['action'] =act;	
		requestData['args']['siteIDs'] = siteID;
		requestData['args']['params'] = valArray;

	   requestData['requiredData'] = {};
      doHistoryCall(ajaxCallPath,requestData,'');
	  hideBlockPosts(this);

return false;
	}
});

$(".op_del_forever.link").live('click',function() {

    var lid = $(this).attr('lid');
	var sid =  $(this).attr('sid');
	var tempArray={},params={};
	siteArray=getSelectedSites();
	tempArray['args']={};
	siteID={};
    siteID['0']=sid;
	params['linkID']=lid;
	tempArray['action']='deleteLinks';
	tempArray['args']['siteIDs']=siteID;
    tempArray['args']['params']=params;
	tempArray['requiredData']= {};
	doHistoryCall(ajaxCallPath,tempArray,'');
	hideBlockLinks(this);
	return false;
	
});

$(".js_changes_posts").live('click',function() {
	$(this).addClass('disabled');
	if(activeItem!='links')
	applyChangesPosts(this,activeItem);
	else if(activeItem=='links')
	applyChangesLinks(this);
	
	return false;
});

$(".link_link").live('click',function(obj) {
	
	obj.stopPropagation();
	});

$("textarea.tinymce").live('click',function() {
	
});

$(".editAsHtml").live('click',function() {
	isVisual='html';
	testVar = tinyMCE.get('content');
	var editVal = testVar.getContent();
	tinyMCE.execCommand('mceRemoveControl', false, 'content');	
    $('#content').val(editVal);
});

$(".visualForm").live('click',function() {
    isVisual='visual';                 
   tinyMCE.execCommand('mceRemoveControl', false, 'content');

tinyMCE.init({
        // General options
        mode : "specific_textareas",
        theme : "advanced",
        plugins : "inlinepopups, spellchecker, tabfocus, paste, media, fullscreen, wpeditimage, wpgallery, wplink, wpdialogs ,wordcount,wordpress",

        // Theme options
        theme_advanced_buttons1 : "bold, italic, strikethrough, bullist, numlist, blockquote, justifyleft, justifycenter, justifyright, link, unlink, wp_more, spellchecker, fullscreen, wp_adv",
        theme_advanced_buttons2 : "formatselect, underline, justifyfull, forecolor, pastetext, pasteword, removeformat, charmap, outdent, indent, undo, redo",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
        editor_selector : "tinyText",
		width : "695",
		height : "340",
		relative_urls : false,
        remove_script_host : false,
});
tinyMCE.execCommand('mceAddControl', false, 'content');
});
$(".mce_code").live('click',function() {
	
});
$(".uploadInsert").live('click',function() {
	if($('.visualForm').hasClass('active'))
		{
		$('.imageValues').dialog({ width: 522 });
        isVisual='visual';
		}
	if($('.htmlForm').hasClass('active'))
		{
			$('.imageValues').dialog({ width: 522 });
			isVisual='html';
		}
});
$(".cancel.imgDiag").live('click',function() {
	$('.imageValues').dialog('close');
	if($('.visualForm').hasClass('active'))
		{
			$('.visualForm').click();
		}
		else
		{
			$('.htmlForm').addClass('active');
		}
});
$(".dropMeTab").live('click',function() {
	$('.dropMe').hide();
	
});

$(".go_button").live('click',function() {
		var imgURL=$('.imgSrc').val();
		var imgTitle=$('.imgTitle').val();
		var imgAlign=$('.postRadio.active').attr('value');
		if(isVisual=='visual')
		{
		testVar = tinyMCE.get('content');
	    var editVal = testVar.getContent();
		editVal = editVal+'<img alt="" src='+imgURL+' title='+imgTitle+' align='+imgAlign+' width="300" height="220" />';
		tinyMCE.activeEditor.setContent(editVal);
		
		}
		else
	    {
			editVal = $('#content').val();
			editVal = editVal+'<img alt="" src='+imgURL+' title='+imgTitle+' align='+imgAlign+' width="300" height="220" />';
		}
		$('#content').val(editVal);
		$('.imageValues').dialog('close');
});
$(".overWrite_post").live('click',function() {
	if($(this).hasClass('active'))
	{
		$(".overPost").show();
	}
	else
	{
	   $(".overPost").hide();
	}
});

$(".showStatusEdit").live('click',function() {
	if(!($(this).hasClass('active')))
	{
		$(this).addClass('active');
		$(".current_value.status").hide();
		$(".editing_value.status").show();
		$(this).parent().hide();
	}
	else
	{
		$(this).removeClass('active');
		$(".editing_value.status").hide();
	}
});

$(".showVisibEdit").live('click',function() {
	if(!($(this).hasClass('active')))
	{
		$(this).addClass('active');
		$(".current_value.visibility").hide();
		$(".editing_value.visibility").show();
		$(this).parent().hide();
	}
	else
	{
		$(this).removeClass('active');
		$(".editing_value.visibility").hide();
	}

});

$(".showPublishEdit").live('click',function() {
	if(!($(this).hasClass('active')))
	{
		$(this).addClass('active');
		$(".current_value.publish_post").hide();
		$(".editing_value.publish_post").show();
		$(".dateChooser").show();
		$(".customFieldsDate").Zebra_DatePicker({
        direction: 1 ,
        offset:[-104,206],   
        });
		$(this).parent().hide();
		$('.immed').click();
	}
	else
	{
		$(this).removeClass('active');
		$(".editing_value.publish_post").hide();
	}

});

$(".dropCustomClick").live('click',function() {
	

	if(!($(this).hasClass('active')))
	{
		$(this).addClass('active');
		if(!($('.dropMe').is(":visible")))
		$(".dropMe").show();
		else
		$(".dropMe").hide();
	}
	else
	{
		$(this).removeClass('active');
		if(!($('.dropMe').is(":visible")))
		$(".dropMe").show();
		else
		$(".dropMe").hide();
	}
});

/*$('.title_collapse').live('click',function(){
	if(!($(this).hasClass('active')))
	{
		$(this).addClass('active');
	}
	else
	{
		$(this).removeClass('active');
	}
		if($(this).hasClass('active'))
	{
		$(this).next('.op_cont').hide();
	}
	else
	{
	   $(this).next('.op_cont').show();
	}
	
});*/
$('.customFieldsOption li a').live('click',function(){
	$(".mc_dropdown_btn span.dropdown_btn_val").text($(this).text());
	 $(".dropMe").hide();
});
$('.cancel.dropMe').live('click',function(){
	$(".dropMe").hide();
	$(".dropCustomClick").removeClass('active');
	
});

$(".onEnterField").live('keypress',function(e) {
	 var code = (e.keyCode ? e.keyCode : e.which);
	 repText = $(this).val();
	 if(code == 13) { 
     if(repText.length>0)
	 $(".mc_dropdown_btn span.dropdown_btn_val").text(repText);
     $(".dropMe").hide();
     }
});

$(".onEnterField").live('click',function(e) {
	return false;
});

$(".fieldDate").live('click',function() {
	if(!$(this).hasClass('active'))
	{
		$(this).addClass('active');
		if($('#widgetCalendar').is(":visible"))
		{
			$('#widgetCalendar').hide();
		}
		else
		{
			$('#widgetCalendar').show();
		}
	}
	else
	{
		$(this).removeClass('active');
		if($('#widgetCalendar').is(":visible"))
		{
			$('#widgetCalendar').hide();
		}
		else
		{
			$('#widgetCalendar').show();
		}
	}

});

$(".selPosts").live('click',function() {
		if(!($(this).hasClass('active')))
	{
		$(this).addClass('active');
		$(this).attr('status','on');
	}
	else
	{
		$(this).removeClass('active');
		$(this).attr('status','');
	}
});

$(".itemPost").live('click',function() {
			if(!($(this).hasClass('active')))
	{
		$(this).addClass('active');
	}
	else
	{
		$(this).removeClass('active');
	}
});

$('.field .radio_btn').live('click',function() {
	$('.field .radio_btn.active').each(function(){
		$(this).removeClass('active');
	});
	if(!$(this).hasClass('active'))
	{
		$(this).addClass('active');
	}
	else
	{
		$(this).removeClass('active');
	}
});

$('#pageContent').live('click',function() {
	if($('.managePosts').hasClass('active'))
	{
    if($('.website_cont').hasClass('active'))
	{
		if($('#widgetCalendar').is(":visible"))
	    $('.fieldDate').click();
	}
	}
});
$('.move_to_trash,.delete_completely').live('click',function() {
	if($('.selectPosts').hasClass('active'))
{
	$('.js_changes_posts').removeClass('disabled');
}
else
{
	$('.js_changes_posts').addClass('disabled');
}
});

$('.op_cont.excerpt').live('click',function(){
	
});

$('.main_post').live('click',function(){
	//var mainParent = $(this).closest('.ind_post_same_status');
	if(!$(this).hasClass('active'))
	 {
		 $(this).addClass('active');
	     $('.selectPosts',this).addClass('active');
		 //$(mainParent).addClass('active');
	 }
	 else
	 {
		 $(this).removeClass('active');
	     $('.selectPosts',this).removeClass('active');
		 //$(mainParent).removeClass('active');
	 }
	 var sameStausParent = $(this).closest('.main_same_post');
	 var postCount=0,postActiveCount=0;
	 $('.main_post',sameStausParent).each(function(){
		 postCount++;
	 });
	 $('.main_post.active',sameStausParent).each(function(){
		 postActiveCount++;
	 });
	 if(postCount == postActiveCount)
	 {
		 $(sameStausParent).addClass('active');
	 }
	 else
	 {
		 $(sameStausParent).removeClass('active');
	 }
	 var mainCount=0,mainActiveCount=0;
	 var mainRealParent = $(this).closest('.rd');
	 $('.main_same_post',mainRealParent).each(function(){
		 mainCount++;
	 });
	 $('.main_same_post.active',mainRealParent).each(function(){
		 mainActiveCount++;
	 });
	 if(mainCount == mainActiveCount)
	 {
	 $(mainRealParent).addClass('active');
	 $(mainRealParent.siblings('.rh')).find('.checkSitePost').addClass('active');
	 }
	 else
	 {
	 $(mainRealParent).removeClass('active');
	 $(mainRealParent.siblings('.rh')).find('.checkSitePost').removeClass('active');
	 }
	 enableApplyChanges();
});



$('.row_ind_link_cont').live('click',function(){
	if(!$(this).hasClass('active'))
	 {
		 $(this).addClass('active');
	     $(this).find('.selectPosts').addClass('active');
	 }
	 else
	 {
		 $(this).removeClass('active');
	     $(this).find('.selectPosts').removeClass('active');
	 }
	 var mainParent = $(this).closest('.rd');
	 var linkCount=0,linkActiveCount=0;
	 $('.row_ind_link_cont',mainParent).each(function(){
		 linkCount++;
	 });
	 $('.row_ind_link_cont.active',mainParent).each(function(){
		 linkActiveCount++;
	 });
	 if(linkCount == linkActiveCount)
	 {
	 	$(mainParent).addClass('active');
		$(mainParent.siblings('.rh')).find('.checkSitePost').addClass('active');
	 }
	 else
	 {
		 $(mainParent).removeClass('active');
		 $(mainParent.siblings('.rh')).find('.checkSitePost').removeClass('active');
	 }
	 enableApplyChanges();
});

$('.refreshDateField').live('click',function(){
	$('.fieldDate').text('Select Date Range ');
});

$('.postRadio').live('click',function(){
	$('.postRadio').each(function(){
		if($(this).hasClass('active'))
		$(this).removeClass('active');
	});
	$(this).addClass('active');
});

$(".searchPosts").live('keyup',function() {
	searchSites(this,4);	
});

$('.checkSitePost').live('click',function(){
	var row_detailed = $(this).closest('.row_detailed');
	var activeCont = $('.typeSelectorPosts.active').attr('utype');
	if(!($(this).hasClass('active')))
	{   
	    $(this).addClass('active');
		if(activeCont == "links")
		{
			$('.row_ind_link_cont',row_detailed).each(function(){
				if(!($(this).hasClass('active')))
				{
					$(this).click();
				}
			});
		}
		else
		{
			$('.main_post',row_detailed).each(function(){
				if(!($(this).hasClass('active')))
				{
					$(this).click();
				}
			});
		}
	}
	else
	{	
		$(this).removeClass('active');
		if(activeCont == "links")
		{
			$('.row_ind_link_cont',row_detailed).each(function(){
				if($(this).hasClass('active'))
				{
					$(this).click();
				}
			});
		}
		else
		{
			$('.main_post',row_detailed).each(function(){
				if($(this).hasClass('active'))
				{
					$(this).click();
				}
			});
		}
	}
	return false;
});
$('.userRoles_post').live('click',function(){
	$('.userRoles_post').removeClass('active');
	$(this).addClass('active');
});