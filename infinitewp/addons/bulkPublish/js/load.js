var isVisual;
function loadPostsManage()
{
	var content='<div class="site_nav_sub"> <ul> <li><a class="managePosts optionSelect">MANAGE</a></li> <li><a class="addNew optionSelect">ADD NEW</a></li> <div class="clear-both"></div> </ul> </div> <ul class="btn_rounded_gloss"> <li><a class="rep_sprite typeSelectorPosts optionSelect typePosts active" utype="posts" onclick="">Posts</a></li><li><a class="rep_sprite typeSelectorPosts optionSelect typePages" utype="pages" onclick="">Pages</a></li> <li><a class="rep_sprite typeSelectorPosts optionSelect typeLinks" utype="links" onclick="">Links</a></li> </ul><div class="steps_hdr">Select websites to <span id="processType">Manage</span> <span class="itemUpper">Posts</span></div><div class="siteSelectorContainer">'+siteSelectorVar+'</div><div class="advancedInstallOptions" style="display:none"><div class="steps_hdr">INSTALLATION OPTIONS</div> <div style="width:220px;"> <div class="checkbox generalSelect activateItem" onclick="">Activate  <span class="itemNameLower">'+activeItem+'</span> after install</div> <div class="checkbox generalSelect overwriteItem" onclick="">Overwrite, if <span class="itemNameLower">'+activeItem+'</span> already exist.</div> </div></div> <div class="result_block shadow_stroke_box siteSearch bulk_publish_page_post" style="border:0;box-shadow: 0 0 0  rgba(0, 0, 0, 0)"><div class="optionsContent "></div>';
	$("#pageContent").html(content);
	$(".managePosts").click();
	siteSelectorNanoReset();
}

function loadManagePostsPanel(data)
 {   

	 $(".fetchPosts").text($(".fetchPosts").attr('tempname')).removeClass('disabled');
	 $(".btn_loadingDiv").remove();
	 manage=data;
	 var content='';
	 content=' <div class="th rep_sprite"> <div class="title"><span class="droid700">Search Sites</span></div> <ul class="btn_radio_slelect float-left" style="display:none"> <li><a class="active rep_sprite manage_posts_view itemName optionSelect">'+activeItem.toTitleCase()+'</a></li> <li><a class="rep_sprite manage_websites_view optionSelect">Websites</a></li> </ul> <div class="type_filter "> <input name="" id="manageFilter" type="text" class="input_type_filter searchPosts manageFilter " value="type to filter" /> <div class="clear_input rep_sprite_backup" onclick=""></div></div> <div class="btn_action float-right"><a class="rep_sprite status_applyChangesCheck js_changes_posts float-right disabled">Apply Changes</a></div><ul class="th_sub_nav float-right user_management" style="padding: 3px 0 4px 2px;"><li><a class="rep_sprite userRoles_post move_to_trash" changeclass="changeUserRole" func="move_to_trash">MOVE TO TRASH</a></li><li><a class="rep_sprite userRoles_post delete_completely" changeclass="changeUserDelete" func="delete">DELETE PERMANENTLY</a></li></ul> </div> <div id="view_content"></div>';
	
	$(".actionContent").html(content).show();
	postsListPanel('posts',activeItem);
	text=$(".fetchPostsTxt").val();
 }
 
 function postsListPanel(view,activeItem)
 {   
	 $(".status_applyChangesCheck").addClass('disabled');
	 var content='',statusVar,firstKey,actionVar,actionLi,siteID,dID,contName,delCont,extraDeactivateClass,styleSheetTmp,dLink,totalPosts,noContentText,posts={},m=0,postsTemp={},newpost,tempObject,jsontemp={},newPid={},cLenInit=0,cLen={},conts='',conTemp={},urlCont='p';
	 postsTemp={};
	 if(!($('.move_to_trash').is(':visible')))
	 {
		$('.move_to_trash').show(); 
	 }
     if(activeItem=='posts')
	 var json=manage.data.bulkPublishGetPosts.siteView;
	 if(activeItem=='pages')
	 var json=manage.data.bulkPublishGetPages.siteView;
	 if(activeItem=='links')
	 {
	 var json=manage.data.bulkPublishGetLinks.siteView;
	 $('.move_to_trash').hide();
	 }
	 if(json!=undefined)
     var totalPosts = objLen(json);
	 if(json!=null && json!=undefined && getPropertyCount(json)>0 && totalPosts>0)
 {   
     $.each(json, function(i, object) 
	 {
		 
		 if(object!=false){
		 var j=i;
		 cLenInit = 0;
		 tempObject=object;
		 $.each(object, function(k, v) 
		 {   
		     if(activeItem == 'links')
			 {
				 cLenInit=cLenInit+1;
			 }
			 else
			 {
			 $.each(v,function(ke,va){				 
			 cLenInit=cLenInit+1;
			 });
			 }
	     });
		 cLen[i]=cLenInit;
		 content=content+'<div class="ind_row_cont posts ">';
		 content=content+'<div class="row_summary" ><div class="row_arrow"></div><div class="count_cont ">'+maxLen(cLen[j])+'</div><div class="row_name searchable">'+site[j].name+'</div> <div class="clear-both"></div></div>';
		 content=content+'<div class="row_detailed" style="display:none">';
		 content=content+'<div class="rh"> <div class="row_arrow"></div><div class="checkbox checkSitePost" style="padding-bottom:11px;"></div><div class="count_cont"><span>'+cLen[j]+'</span></div><div class="row_name">'+site[j].name+'</div> <div class="clear-both"></div></div>';
		 content=content+'<div class="rd"><div class="row_ind_post_cont links" > <div class="clear-both"></div>';
		 if(activeItem!='links')
		 {
		 $.each(object,function(k,v){
			 if(k=='publish')
			 {
			 var postStatus='published';
			 }
			 else if(k=='future')
			 {
			  var postStatus='scheduled';	 
			 }
			 else
			 {
				var postStatus=k; 
			 }
			 content=content+'<div class="row_ind_post_page_cont main_same_post"><div class="post_status"><span class="'+postStatus+'">'+postStatus+'</span></div><div class="post_same_status_cont ">';
			 $.each(v,function(ke,va){
			 var catg='',tagsPost=''; 
			 if(activeItem=='posts')
			 {
		 $.each(va.cats,function(k1,v1){
				 $.each(v1,function(k2,v2){
			     if(catg=='')
				 catg=v2;
				 else		 
				 catg=catg+', '+v2;
				 });
			 });
			 $.each(va.tags,function(kee,vaa){
				 $.each(vaa,function(x,y){
			     if(tagsPost=='')
				 tagsPost=y;
				 else		 
				 tagsPost=tagsPost+', '+y;
				 });
			 });
			 }
			if(activeItem=='pages')
			{
				urlCont = 'p';
			}
content=content+'<div class="ind_post_same_status main_post postStatus='+ke+'"><div class="post_same_status_cont"><div class="ind_post_same_status"><div class="checkbox selectPosts" postid='+va.post_id+' siteid='+j+'></div><div class="row_post">'+va.post_title+'</div><div class="clear-both"></div><div class="comment_who icon_sprite_WPposts">'+va.post_author.author_name+'</div><div class="comment_when rep_sprite_backup">'+va.post_date+'</div><div class="post_categ icon_sprite_WPposts">'+catg+'</div><div class="post_tag icon_sprite_WPposts">'+tagsPost+'</div><ul class="post_page_ops"><li class="rep_sprite"><a class="op_open icon_sprite_WPposts link" title="Open Post" sid='+j+' pid='+va.post_id+' href='+site[j].URL+'/?'+urlCont+'='+va.post_id+' target="_blank"></a></li><li class="rep_sprite"><a class="op_edit icon_sprite_WPposts post" title="Edit Post" sid='+j+' pid='+va.post_id+'></a></li><li class="rep_sprite"><a class="op_trash rep_sprite_backup" title="Trash" type='+activeItem+' siteID='+j+' postID='+va.post_id+'></a></li></ul></div></div><div class="clear-both"></div></div>';
			 });
			 content=content+'</div><div class="clear-both"></div></div>';
		 });
		 }
		 else
		 {
			 $.each(object,function(k,v){
				 conts='';
				 if(v.link_visible=="Y")
				 var vi='visible';
				 $.each(v.link_cats,function(k,v){
					 if(conts=='')
				     conts=v;
					 else
					 conts=conts+', '+v;
					 });
				 content=content+' <div class="row_ind_link_cont"><div class="checkbox selectPosts" postid='+k+' siteid='+j+' ></div><div class="row_link">'+v.link_name+'<a class="icon_sprite_WPlinks link_link" href="'+v.link_url+'" target="_blank">'+v.link_url+'</a></div> <div class="clear-both"></div><div class="link_categ icon_sprite_WPlinks">'+conts+'</div> <div class="link_visible icon_sprite_WPlinks">'+vi+'</div> <div class="link_rating icon_sprite_WPlinks">'+v.link_rating+'</div><div class="link_relationship icon_sprite_WPlinks">'+v.link_rel+'</div><div class="clear-both"></div><ul class="link_ops"><li class="rep_sprite"><a class="op_edit icon_sprite_WPlinks link" title="Edit" sid='+j+' lid='+k+'></a></li> <li class="rep_sprite"><a class="op_del_forever rep_sprite_backup link" title="Delete Permanently" sid='+j+' lid='+k+'></a></li></ul> </div>';
		    });
		 } content=content+'</div></div></div></div>';
		 }
		  });
		  }
		else 
		content=content+'<div class="ind_row_cont" ><div class="noShow" ><div class="row_name"> No '+activeItem+' to Show </div><div class="clear-both"></div></div></div>';
		if(view=="sites")
		noContentText='websites';
		else
		noContentText = 'websites';
		content='<div class="no_match hiddenCont" style="display:none">Bummer, there are no '+noContentText+' that match.<br />Try typing lesser characters.</div>'+content;
		 $("#view_content").html(content);
	    hideTagsButton();
 }
 
 function loadAddNewPanel(view)
 {
	 $(".btn_loadingDiv").remove();
	 var content2='';
	 var content='';
	 content=content+'<div class="th rep_sprite"><div class="title"><span class="droid700">BULK ADD NEW '+view.toUpperCase()+'</span></div></div>';
	 if(view=='links')
	 {
		 content=content+'<div class="add_post_left_cont"><input name="" type="text" class="txt post_title linkName" placeholder="Name" /> <input name="" type="text" class="txt post_title linkURL" placeholder="Link - http://" /><input name="" type="text" class="txt post_title linkDes" placeholder="Description" style="margin-bottom:10px;" /></div>';
		 content=content+'<div class="add_post_right_cont"><div class="op_cont_outer"> <div class="title_collapse rep_sprite"><div class="collapse_arrow">CATEGORIES</div> <div class="clear-both"></div></div><div class="op_cont"> <input name="" type="text" class="txt full linkCat" placeholder="Separate categories with commas" /></div> </div><div class="op_cont_outer"> <div class="title_collapse rep_sprite"> <div class="collapse_arrow">TARGET FRAME</div><div class="clear-both"></div></div> <div class="op_cont"><ul class="btn_radio_slelect link_target float-left" style="margin: 10px 38px;"> <li><a class="rep_sprite  itemPost optionSelect  active">_blank</a><div class="tip center">Open in a new window or tab.</div></li> <li><a class="rep_sprite optionSelect itemPost ">_top</a><div class="tip center">Open in the current window or tab, with no frames.</div></li><li><a class="rep_sprite itemPost optionSelect ">_none</a><div class="tip center">Open in the same window or tab.</div></li> </ul></div> </div><div class="op_cont_outer"></div><div class="op_cont_outer"> <div class="op_cont"></div></div> </div>';
		 content=content+'<div class="clear-both"></div>';
		 content=content+'<div class="th rep_sprite" style="border-top: 1px solid #D2D5D7; border-bottom:0; height:35px;"> <div class="btn_action float-right"><a class="rep_sprite addLink" id="" style="margin: 4px;">Add Link</a></div> </div>';
	 }
	 else
	 {
	 content=content+' <div class="add_post_left_cont"><input name="" type="text" class="txt post_title" placeholder="Enter '+activeItem+' title here" />';
	 content=content+' <div class="txt_editor post_content"> <div class="toolbar"> <a class="rep_sprite uploadInsert icon_sprite_WPposts" changeclass="changeUserRole" func="change-role">Insert Image</a> <ul class="th_sub_nav float-right wysiwyg_mode" style="margin: 3px;"> <li><a class="rep_sprite active userRoles_post visualForm" changeclass="changeUserRole" func="change-role">VISUAL</a></li> <li><a class="rep_sprite userRoles_post editAsHtml htmlForm" changeclass="changeUserDelete" func="delete-user">HTML</a></li> </ul> </div> <textarea class="wp-editor-area tinyText tinymce" name="content" id="content" cols="" rows="" style="height: 260px;"></textarea> </div>';

	 content=content+'<ul id="sortableLeft"><li> <div class="op_cont_outer excerpt"> <div class="title_collapse rep_sprite"><span class="collapse_arrow">EXCERPT</span> <span style="float:right;">An excerpt is an optional summary or description of a post; in short, a post summary. <a href="http://codex.wordpress.org/Excerpt" target="_blank">Learn more</a>.</span>  <div class="clear-both"></div> </div> <div class="op_cont excerpt disabled">  <textarea name="" cols="" rows="" style="height: 60px;"></textarea> </div>  </div></li> <li> <div class="op_cont_outer">  <div class="title_collapse rep_sprite"> <div class="collapse_arrow">CUSTOM FIELDS</div> <span style="float:right;">Custom fields can be used to add extra metadata to a post that you can <a href="http://codex.wordpress.org/Using_Custom_Fields" target="_blank">use in your theme</a>.</span> <div class="clear-both"></div>  </div> <div class="op_cont custom_fields">  <div style="float:left; width:150px; margin:10px;"><div class="mc_dropdown_cont custom_fields float-left "> <div class="mc_dropdown_btn dropCustomClick" id=""><span id="profileDropName" class="dropdown_btn_val">Select or Enter new</span><span class="arrow_down"></span></div><div class="mc_dropdown custom_profile dropMe" style="display:none"> <div class="th rep_sprite dropMeTab"> <input name="" type="text" class="txt onEnterField" placeholder="Enter new" /> <a class="cancel rep_sprite_backup dropMe">close</a></div> <ul id="selectCustomFieldOptions" class="customFieldsOption">  <li><a>additional_settings</a></li>  <li><a>form</a></li>  <li><a>hometumb</a></li>  <li><a>page-subtitle</a></li><li><a>tags_custom</a></li><li><a>var_1</a></li><li><a>var_2</a></li> <li><a>additional_messages_top</a></li><li><a>additional_messages_bottom</a></li><li><a>seo_option</a></li><li><a>sharebox</a></li><li><a>test</a></li> </ul> </div></div></div><div style="float:left;"><textarea name="" cols="" rows="" style="width: 515px;"></textarea> </div><div class="clear-both"></div></div></div></li> <li><div class="op_cont_outer"><div class="title_collapse rep_sprite"><div class="collapse_arrow">DISCUSSION</div><div class="clear-both"></div></div><div class="op_cont discussion"><div class="checkbox allow_comments selectPostsAdd">Allow comments</div> <div class="checkbox allow_trackbacks selectPostsAdd" >Allow trackbacks and pingbacks on this page.</div></div></div></li></ul></div>';
	 content=content+'<div class="add_post_right_cont"><ul id="sortableRight"><li><div class="op_cont_outer publish"><div class="title_collapse rep_sprite"><div class="collapse_arrow">PUBLISH</div> <div class="clear-both"></div></div> <div class="op_cont"><table width="100%" border="0"><tr><td width="35%" align="right" valign="top"><div style="padding-top:5px;" class="label">STATUS</div><div class="edit_publish rep_sprite"><a class="rep_sprite_backup showStatusEdit"></a></div></td><td><div class="current_value status">Draft</div><div class="editing_value status" style="display:none"><div class="radio_btn active" val="Draft">Draft</div><div class="radio_btn" val="Pending review">Pending review</div></div></td> </tr> </table><table width="100%" border="0"><tr><td width="35%" align="right" valign="top"><div style="padding-top:5px;" class="label">VISIBILITY</div> <div class="edit_publish rep_sprite"><a class="rep_sprite_backup showVisibEdit"></a></div></td><td><div class="current_value visibility">Public</div><div class="editing_value visibility" style="display:none"> <div class="radio_btn active" val="Public">Public</div> <div class="radio_btn" val="Password protected">Password protected</div><input type="text" class="passVal txt" placeholder="enter password" style="display:none"></input> <div class="radio_btn pvtPost" val="Private">Private</div></div></td> </tr> </table> <table width="100%" border="0"><tr><td width="35%" align="right" valign="top"><div style="padding-top:5px;" class="label">PUBLISH</div> <div class="edit_publish rep_sprite"><a class="rep_sprite_backup showPublishEdit"></a></div></td> <td><div class="current_value publish_post">Immediately</div><div class="editing_value publish_post" style="display:none" > <div class="radio_btn active immed" val="Immediately">Immediately</div><div class="radio_btn schedPost" val="Schedule">Schedule</div></div></td> </tr> <tr class="dateChooser" style="display:none" > <td colspan="2" align="right" valign="top"><div class="schedule_post schedPost"><div class="dropdown_cont profile_cont float-left" style="margin-left: 10px;width: 100px;"><div class="text widget"><input type="text" class="customFieldsDate"  style="width: 100px ;text-align:left"></input></div> <div class="dropdown_btn" id="selectProfileBtn" style="width: 74px; display:none"><span id="profileDropName" class="dropdown_btn_val customFieldsDate">19 Dec, 2012</span><span class="arrow_down"></span></div> <ul id="selectProfileOptions" class="dropdownToggle" style="display: none;"><li class="emptyCont"><a>No profile created yet.</a></li></ul> </div><div class="datepicker_cont float-right"></div><div style="float:right; width:100px; margin: 2px 8px 0 0;"><span style="padding:0 5px; float:left; margin-top: 6px;">@</span><input name="" type="text" class="txt float-left postHours" /><span style="padding:0 5px; float:left; margin-top: 6px;">:</span><input name="" type="text" class="txt float-left postMins" /></div></div></td></tr> </table> <div class="checkbox overWrite_post selectPostsAdd">Overwrite existing post</div><table class="overPost" style="display:none; border-bottom:0;" width="100%" border="0" style="border-bottom:0; margin-top: -10px;"><tbody class="postMatch"><tr><td align="center">Match by</td> <td><div class="radio_btn active" val="title">Title</div></td><td><div class="radio_btn" val="slug">Slug</div></td></tr> <tr><td colspan="3" align="center" style="font-size:11px;">If no match is found, the post will be published</td></tr></tbody></table><div style="border-top: 1px solid #D2D5D7;"><div class="btn_action float-right"><a class="rep_sprite btn_blue" id="save_group_changes_post" style="" onclick="">Publish</a></div><div class="clear-both"></div></div></div> </div></li><li><div class="op_cont_outer custom_table"><div class="title_collapse rep_sprite"><div class="collapse_arrow">'+view.toUpperCase()+' BY TAG PAGE FIELDS</div><div class="clear-both"></div> </div><div class="op_cont"><table width="100%" border="0" style="margin:5px 0;"><tr><td width="40%" align="right" class="label">WIDGET TITLE</td> <td><input name="" type="text" class="txt widget_title" /></td></tr> <tr><td width="40%" align="right" class="label">WIDGET TAGS</td> <td><input name="" type="text" class="txt widget_tags" /></td> </tr></table></div> </div></li><li> <div class="op_cont_outer"> <div class="title_collapse rep_sprite"><div class="collapse_arrow">EDIT POST SLUG</div> <div class="clear-both"></div> </div><div class="op_cont"><input name="" type="text" class="txt full editPostSlug" placeholder="Add post slug here" /> </div></div></li><li><div class="op_cont_outer"> <div class="title_collapse rep_sprite"> <div class="collapse_arrow">CATEGORIES</div><div class="clear-both"></div></div> <div class="op_cont"><input name="" type="text" class="txt full categories" placeholder="Separate categories with commas" /></div> </div></li><li> <div class="op_cont_outer"><div class="title_collapse rep_sprite"> <div class="collapse_arrow">TAGS</div><div class="clear-both"></div></div> <div class="op_cont"> <input name="" type="text" class="txt full tags_posts" placeholder="Separate tags with commas" /></div> </div></li><li> <div class="op_cont_outer"><div class="title_collapse rep_sprite"> <div class="collapse_arrow">FEATURED IMAGE</div> <div class="clear-both"></div> </div> <div class="op_cont"> <input name="" type="text" class="txt full post_img" placeholder="Enter URL to the image" /></div> </div></li></ul></div>';
	 content=content+'<div class="clear-both"></div>';

    content=content+'<div class="dialog_cont imageValues ui-dialog-content ui-widget-content" style="display:none;" > <div class="th rep_sprite " id="addSiteSprite"> <div class="title droid700">INSERT AN IMAGE</div> <a class="cancel rep_sprite_backup imgDiag">cancel</a></div> <div class="insert_image form_cont " style="width:478"> <div class="tr" style="width:96%"> <div class="tl ">Image URL</div> <div class="td"> <input name="" type="text" id="imgURL" class="onEnter imgSrc" onenterbtn=".addSiteButton"> </div> <div class="clear-both"></div> </div> <div class="tr" style="width:96%"> <div class="tl ">Image Title</div> <div class="td"> <input name="" type="text" id="username" class="onEnter imgTitle" onenterbtn=".addSiteButton"> </div> <div class="clear-both"></div> </div> <div class="tr"> <div class="tl">ALIGN</div> <div class="td align_options"> <div name="align" class="radio_btn postRadio active float-left" id="align-none" value="none" onClick="" >None</div> <div name="align" id="align-left" value="left" onClick="" type="radio" class="radio_btn postRadio float-left"> Left </div> <div name="align" id="align-center" value="center" onClick="" type="radio" class="radio_btn postRadio float-left">Center </div> <div name="align" id="align-right" value="right" onClick="" type="radio" class="radio_btn postRadio float-left">Right </div> </div> <div class="clear-both"></div> </div> </div> <div class="clear-both"></div> <div class="th_sub rep_sprite" style="border-top: 1px solid #D2D5D7;"> <div class="btn_action float-right "><a class="rep_sprite go_button " style="color: #6C7277;">Insert Image</a></div> </div> <div class="clear-both"></div> </div> ';
	 
	 }
	 $(".optionsContent").html(content).show();
		$('#dateField').DatePickerShow();
	if($('.visualForm').hasClass('active'))
	{
		tinyMCE.execCommand('mceRemoveControl', false, 'content');
		tinyMCE.init({
        // General options
        mode : "specific_textareas",
        theme : "advanced",
        plugins : "inlinepopups, spellchecker, tabfocus, paste, media, fullscreen, wpeditimage, wpgallery, wplink, wpdialogs ,wordcount,wordpress",

        // Theme options
        theme_advanced_buttons1 : "bold, italic, strikethrough, bullist, numlist, blockquote, justifyleft, justifycenter, justifyright, link, unlink, wp_more, spellchecker, fullscreen, wp_adv",
        theme_advanced_buttons2 : "formatselect, underline, justifyfull, forecolor, pastetext, pasteword, removeformat, charmap, outdent, indent, undo, redo",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
        editor_selector : "tinyText",
		width : "695",
		height : "340",
		relative_urls : false,
        remove_script_host : false,
});

	}


 }




function loadImgDialog()
{
	 
	var content='';
	var extra='';
	if(group!=null && group!=undefined && group.length!=0)
	{
	extra='<div class="tr assignGroupItem addSiteToggleDiv" id="addSiteGroupsPanel"><div class="tl">Existing Groups</div><div class="td"><div class="addSiteGroups">'+groupGenerate(2)+'</div></div><div class="clear-both"></div></div>';
	}
 	content=content+'<div class="dialog_cont add_site"> <div class="th rep_sprite " id="addSiteSprite"> <div class="title droid700">ADD AN IMAGE</div> <a class="cancel rep_sprite_backup">cancel</a></div> <div class=" form_cont " style="border:0;"> <div class="tr"> <div class="tl ">Image URL</div> <div class="td"> <input name="" type="text" id="imgURL" class="onEnter imgSrc" onenterbtn=".addSiteButton" /> </div> <div class="clear-both"></div> </div>  <div class="tr"> <div class="tl ">Image Title</div> <div class="td"> <input name="" type="text" id="username" class="onEnter imgTitle" onenterbtn=".addSiteButton" /> </div> <div class="clear-both"></div> </div> <div class="tr websiteURLCont" style="display:none"> <div class="tl">Website URL</div> <div class="td"> <input name="website" type="text" id="websiteURL" class="txtHelp onEnter" onenterbtn=".addSiteButton" value="http://" helptxt="http://" /> </div> <div class="clear-both"></div> </div> <div class="tr activationKeyDiv" style="display:none"> <div class="tl ">Activation Key</div> <div class="td"> <input name="" type="text" id="activationKey" class="onEnter" style="display:none" onenterbtn=".addSiteButton" /> <div style="color: #737987;line-height: 16px;">The Activation Key will be displayed every time you activate the IWP Client Plugin on your website.</div> </div> <div class="clear-both"></div> </div> <div class="tr" style="margin-top: 25px; margin-bottom: 15px; display:none"> <div class="tl"></div> <div class="td"><span class="toggle_link"><span class="addSiteToggleAction assignToggleAction">Assign to groups</span></span><span class="toggle_link"><span class="addSiteToggleAction folderToggleAction">Folder protection</span><span class="addSiteToggleAction advancedContentTypeAction">Advanced</span></span></div> <div class="clear-both"></div> </div> <div class="tr folderProtectionItem addSiteToggleDiv" style="display:none"> <div class="tl"></div> <div class="td"> <input name="" type="text" class="txtHelp folder_protect" id="addSiteAuthUsername" helptxt="username" value="username" style="color:#AAAAAA"/> <input name="" type="password" id="addSiteAuthUserPassword" class="txtHelp folder_protect" helptxt="password" value="password" style="color:#AAAAAA"/> </div> <div class="clear-both"></div> </div> <div class="tr advancedContentTypeItem addSiteToggleDiv" style="display:none"> <div class="tl">CONTENT TYPE</div> <div class="td"> <div class="c_radio cTypeRadio active">application/x-www-form-urlencoded</div> <div class="c_radio cTypeRadio">multipart/form-data</div> <div class="c_radio cTypeRadio">text/plain</div> <div class="c_radio cTypeRadio customTxt"> <input name="" type="text" style="width:200px; height:10px; margin-top: -4px;" class="txtHelp customTxtVal" value="custom type" helptxt="custom type"> </div> </div> <div class="clear-both"></div> <div class="tl">CONNECT USING</div> <div class="td"> <ul class="btn_radio_slelect float-left" style="margin-left:10px;"> <li><a class="rep_sprite optionSelect connectURLClass active" def="default">Default</a></li> <li><a class="rep_sprite optionSelect connectURLClass" def="siteURL">Site URL</a></li> <li><a class="rep_sprite optionSelect connectURLClass"  def="adminURL">wp-admin URL</a></li> </ul> </div> <div class="clear-both"></div> </div> <div class="tr assignGroupItem addSiteToggleDiv" style="display:none"> <div class="tl two_liner">New Group<br /> <span style="text-transform:none; font-size:12px;">(separate by comma)</span></div> <div class="td"> <input name="" type="text" class="txtHelp" helptxt="eg. group1, group2" value="eg. group1, group2" id="groupText" style="color:#AAAAAA"/> </div> <div class="clear-both"></div> </div> '+extra+'</div> <div class="clear-both"></div> <div class="th_sub rep_sprite"><span class="rep_sprite_backup info float-left" id="clientPluginDescription">The IWP Client Plugin should be installed on the sites before adding them.</span> <div class="btn_action float-right "><a class="rep_sprite go_button">Add Site</a></div> </div> <div class="clear-both"></div> </div>';
	$("#modalDiv").dialog("close");
	$("#modalDiv").dialog("destroy");
	$('#modalDiv').html(content).dialog({width:'auto', modal:true,position: 'center',resizable: false,open: function(event, ui) {bottomToolBarHide(); },close: function(event, ui) {bottomToolBarShow(); } });
	$(".assignGroupItem").hide();
	
	
}

function hidePostStatus(){
	var m=0,n=0,l=0,x=0,y=0,z=0;
	$('.post_status .trash').each(function(){
       m=m+1;
	   if(m>1)
	   {   $(this).wrap('<div class="new" />');
		   $(this).removeClass('trash');
	   }
	});
	$('.post_status .draft').each(function(){
       n=n+1;
	   if(n>1)
	   {
		   $(this).removeClass('draft');
	   }
	});
	$('.post_status .pending').each(function(){
       l=l+1;
	   if(l>1)
	   {
		   $(this).removeClass('pending');
	   }
	});
	$('.post_status .private').each(function(){
       x=z+1;
	   if(z>1)
	   {
		   $(this).removeClass('private');
	   }
	});
	$('.post_status .scheduled').each(function(){
       y=y+1;
	   if(y>1)
	   {
		   $(this).removeClass('scheduled');
	   }
	});
	$('.post_status .published').each(function(){
       z=z+1;
	   if(z>1)
	   {
		   $(this).removeClass('published');
	   }
	});
}