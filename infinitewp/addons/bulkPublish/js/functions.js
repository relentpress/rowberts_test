
function showPostsOptions()
{
	var countDate = 0;
	if($('.managePosts').hasClass('active'))
	  {
		if(!isSiteSelected())
		{
		$(".optionsContent").hide();
		$(".advancedInstallOptions").hide();
		}
	 else
		 {  
			$(".optionsContent").show();// For manage / install panel
			$('#widgetCalendar').hide().css("overflow","visible");		
		 }
	}
	if($('.addNew').hasClass('active'))
	{
		if(!isSiteSelected())
	{
	$(".optionsContent").hide();
	$(".advancedInstallOptions").hide();
	}
	else
 {
	$('.optionsContent').show();   
 }
	
	}
	$(".actionContent").html('').hide();
	
}
function formArrayLoadPosts(data)
{
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="loadPosts";
		
}

function formArrayLoadLinks(data)
{
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="loadLinks";
		
}
function enableApplyChanges()
{
	if($('.userRoles').hasClass('active'))
	{
		if($('.selectPosts').hasClass('active'))
	{
		$('.status_applyChangesCheck').removeClass('disabled');
	}
	else
	{
		$('.status_applyChangesCheck').addClass('disabled');
	}
	
}
else
{
	$('.status_applyChangesCheck').addClass('disabled');
}
}

function applyChangesPosts(object,activeElement)
{
	var changeArray={};
	var arrayCounter=0, siteID, dID, type, action, name, valArray={},siteIDS={},n=0,sites={};
	closestVar=$(object).closest('.siteSearch');
	if($(".move_to_trash").hasClass('active'))
	{	
	action='trash';
	$(".move_to_trash").removeClass('active');
	}
    if($(".delete_completely").hasClass('active'))
	{
	action='delete';
	$(".delete_completely").removeClass('active');
	}
	$(".selectPosts.active").each(function () {
		
		siteID = $(this).attr('siteid');
		if(valArray[siteID]==undefined)
		{
		valArray[siteID]={};
		}
		cid=$(this).attr('postid');
		valArray[siteID][arrayCounter]=cid;
		arrayCounter++;
		$(this).closest('.main_post').fadeOut('slow',function(){
			$(this).remove();
	    });
		//hideBlockPosts(this);
	});
	//var main = $(obj).closest('.main_same_post');
	$('.main_same_post.active').each(function(){
		$(this).fadeOut('slow',function(){
	    });
	});
	$('.rd.active').each(function(){
		$(this).parent().fadeOut('slow',function(){
	    });
	});
	var tempArray={};
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['args']['params']['siteID']={};
	if(activeElement == 'posts')
	tempArray['action']='bulkDeletePosts';
	else
	tempArray['action']='bulkDeletePages';
	tempArray['args']['siteIDs']=siteArray;
	tempArray['args']['params']['siteID'] = valArray;
	tempArray['args']['params']['action'] = action;
	tempArray['requiredData']={};
	doHistoryCall(ajaxCallPath,tempArray,'processApplyChangesPost');
    
}

function applyChangesLinks(object)
{
	var changeArray={};
	var arrayCounter=0, siteID, dID, type, action, name, valArray={},siteIDS={},n=0,sites={};
	closestVar=$(object).closest('.siteSearch');
	if($(".move_to_trash").hasClass('active'))
	{	
	action='delete';
	$(".move_to_trash").removeClass('active');
	}
    if($(".delete_completely").hasClass('active'))
	{
	action='delete';
	$(".delete_completely").removeClass('active');
	}
	$(".selectPosts.active").each(function () {
		
		siteID = $(this).attr('siteid');
		if(valArray[siteID]==undefined)
		{
		valArray[siteID]={};
		}
		cid=$(this).attr('postid');
		valArray[siteID][arrayCounter]=cid;
		arrayCounter++;
		$(this).closest('.row_ind_link_cont').fadeOut('slow',function(){
			$(this).remove();
	    });
	});
	$('.rd.active').each(function(){
		$(this).parent().fadeOut('slow',function(){
	    });
	});
	var tempArray={};
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['args']['params']['siteID']={};
	tempArray['action']='bulkDeleteLinks';
	tempArray['args']['siteIDs']=siteArray;
	tempArray['args']['params']['docomaction']='delete';
	tempArray['args']['params']['siteID'] = valArray;
	tempArray['args']['params']['action'] = 'delete';
	tempArray['requiredData']={};
	doHistoryCall(ajaxCallPath,tempArray,'processApplyChangesPost');

}

function hideTagsButton(){
   $(".post_tag").each(function (){
	   if($(this).text()=='')
	   {
		   $(this).hide();
	   }
	   else
	   {
		   $(this).show();
	   }
   });
   $(".post_categ").each(function (){
	   if($(this).text()=='')
	   {
		   $(this).hide();
	   }
	   else
	   {
		   $(this).show();
	   }
   });
}

function hideBlockPosts(obj) {
	var count=0;
	$(obj).closest('.main_post').fadeOut('slow',function(){
		$(this).remove();
	});
	var main = $(obj).closest('.main_same_post');
	var full = $(obj).closest('.row_ind_post_page_cont');
	$('.main_post:visible',main).each(function(object){
		count++;
	});
	if(count<2)
	{
		$(main).fadeOut('slow',function(){
	});
	}
	hideFullPost(obj,count);
}

function hideFullPost(obj,prevCount) {
	var count = 0;
	var main = $(obj).closest('.ind_row_cont');
	//var full = $(obj).closest('.row_detailed');
	$('.main_same_post:visible',main).each(function(){
		count++;
	});
	if((count<2)&&(prevCount<2))
	{
		$(main).fadeOut('slow',function(){  });
	}
}


function hideBlockLinks(obj) {
	var count=0;
	$(obj).closest('.row_ind_link_cont').fadeOut('slow',function(){
		$(this).remove();
	});
	var main = $(obj).closest('.row_ind_post_cont');
	var full = $(obj).closest('.row_detailed');
	$('.row_ind_link_cont:visible',main).each(function(){
		count++;
	});
	if(count<2)
	{
		$(full).fadeOut('slow',function(){
	});
	}
}

/*function myDate(dateCont)
{
	var d=new Date(dateCont);
	var mon=d.getMonth();
	var date=d.getDate();
	var t=d.getHours();
	if(t>12)
	{
	var p='pm';
	t= t-12;
	}
	else 
	var p='am';
	t= t+':'+d.getMinutes()+p;
	switch (mon)
{
case 0:
  mon="Jan";
  break;
case 1:
  mon="Feb";
  break;
case 2:
  mon="Mar";
  break;
case 3:
  mon="Apr";
  break;
case 4:
  mon="May";
  break; 
case 5:
  mon="Jun";
  break;
case 6:
  mon="Jul";
  break;
case 7:
  mon="Aug";
  break;
case 8:
  mon="Sep";
  break;
case 9:
  mon="Oct";
  break;             
case 10:
  mon="Nov";
  break;
case 11:
  mon="Dec";
  break;
}

	return mon+' '+date+' @ '+t;
	
}*/
function processPublishPost()
{   
    if($('#save_group_changes_post').hasClass('disabled'))
	$('#save_group_changes_post').removeClass('disabled');
    if($('.addLink').hasClass('disabled'))
	$('.addLink').removeClass('disabled');
}

function processApplyChangesPost()
{
	$('.js_changes_posts').removeClass('disabled');
}

