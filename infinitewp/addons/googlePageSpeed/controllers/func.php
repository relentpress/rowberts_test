<?php

function googlePageSpeedAddonMenus(&$menus){
	$menus['monitor']['subMenus'][] = array('page' => 'gPageSpeed', 'displayName' => 'Google PageSpeed');
}


function googlePageSpeedGrantAccess(){
	$result = getOption('googlePageSpeedAccessStatus');
	if(!empty($result)){
		return $result;
	}
	return false;
}


function googlePageSpeedGetData($params){
	try{
		googlePageSpeedHelper::setPathsAndKeys();
		if(!googlePageSpeedHelper::authAndSetToken()){ return false; }
		$result=array();
		foreach ($params['siteIDs'] as $siteID) {
	        $result_temp = getSiteData($siteID);
			$result[$siteID] = $result_temp['URL'];
			$rez = googlePageSpeedHelper::getGooglePageSpeedData(googlePageSpeedHelper::$client,$result[$siteID]);
			$rez = str_replace(array("\n","\t"),array("",""),$rez);
			$rez = json_decode($rez);
			$result[$siteID] = $rez;
		}
		return $result;
	}catch(Google_ServiceException $e){
		$cd = $e->getCode();
		$errors = $e->getErrors();
		addNotification($type='E', $title='Google PageSpeed Service Exception (Error#'.$cd.')', $message=$errors[0]['message'], $state='U', $callbackOnClose='', $callbackReference='');
	}
	catch(Google_Exception $e){
		$cd = $e->getCode();
		$msg = $e->getMessage();
		addNotification($type='E', $title='Google PageSpeed Exception (Error#'.$cd.')', $message=$msg, $state='U', $callbackOnClose='', $callbackReference='');
	}
}

class googlePageSpeedHelper{
	
	public static $client;
	public static $authHelper;
	public static $storage;
	private static $GPSThisPage;
	private static $redirectURL;
	private static $isSetPathsAndKeysComplete = false;
	
	public static function setPathsAndKeys(){
		
		if(self::$isSetPathsAndKeysComplete) return true;
		
		include_once(APP_ROOT.'/lib/googleAPIs/src/Google_Client.php');
		include_once(APP_ROOT.'/lib/googleAPIs/storage.php');
		include_once(APP_ROOT.'/lib/googleAPIs/authHelper.php');
		
		self::$GPSThisPage = APP_URL.'addons/googlePageSpeed/lib/googlePageSpeed.php';
		self::$redirectURL = APP_URL.'addons/googlePageSpeed/lib/googlePageSpeed.php';
		
		
		$googlePageSpeedAPIKeys = getOption('googleAPIKeys');
		if(!empty($googlePageSpeedAPIKeys)){
			$googlePageSpeedAPIKeys = unserialize($googlePageSpeedAPIKeys);
		}
	
		// Build a new client object to work with authorization.
		self::$client = new Google_Client();
		self::$client->setClientId($googlePageSpeedAPIKeys['clientID']);

		self::$client->setClientSecret($googlePageSpeedAPIKeys['clientSecretKey']);
		self::$client->setRedirectUri(self::$redirectURL);
		self::$client->setApplicationName('InfiniteWP Google PageSpeed');
		self::$client->setScopes(array('openid'));
		
		// Magic. Returns objects from the Analytics Service
		// instead of associative arrays.
		self::$client->setUseObjects(true);
		
		
		// Build a new storage object to handle and store tokens in sessions.
		// Create a new storage object to persist the tokens across sessions.
		self::$storage = new apiSessionStorage(); //googleAnalyticsStorageHelper();
		
		self::$authHelper = new AuthHelper(self::$client, self::$storage, self::$GPSThisPage);
		
		self::$isSetPathsAndKeysComplete = true;

	}
	
	public static function authAndSetToken(){
		
		$getAccessToken = getOption('googlePageSpeedAccessToken');
		$accessToken = unserialize($getAccessToken);
		
		if (isset($accessToken)) {
		  self::$client->setAccessToken($accessToken);
		}
			
		return self::$authHelper->isAuthorized();
	}

	public function getGooglePageSpeedData($client,$url)
	{
		$url = urlencode($url);
		$reqURL = "https://www.googleapis.com/pagespeedonline/v1/runPagespeed?url=".$url."&screenshot=true";
		$req = new Google_HttpRequest($reqURL);
		$res = self::$client->getIo()->authenticatedRequest($req);
		$respBody = $res->getResponseBody();
		return $respBody;
	}
	
}