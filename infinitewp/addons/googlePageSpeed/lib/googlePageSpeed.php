<?php

include '../../../includes/app.php';
try{
	//Main controller logic.

	googlePageSpeedHelper::setPathsAndKeys();

	if ($_GET['action'] == 'revoke') {
	 googlePageSpeedHelper::$authHelper->revokeToken();
	 deleteOption('googlePageSpeedAccessToken');
	 
	 updateOption('googlePageSpeedAccessStatus', 'revoke');
	 
	 addNotification($type='N', $title='Google PageSpeed Revoked', $message="Your Google PageSpeed account has been revoked.", $state='U', $callbackOnClose='', $callbackReference='');
	  
	} else if ($_GET['action'] == 'auth' || $_GET['code']) {
	  googlePageSpeedHelper::$authHelper->authenticate();

	  if($_GET['code']){
		  sleep(1);
		  $getAccessToken = googlePageSpeedHelper::$storage->get($accessToken);
		  updateOption('googlePageSpeedAccessToken', serialize($getAccessToken));
	  }
	}else {
		if(googlePageSpeedHelper::$storage->get($accessToken) ){
			googlePageSpeedHelper::$authHelper->setTokenFromStorage();


				addNotification($type='N', $title='Google PageSpeed Connected', $message="Your Google PageSpeed account has been connected.", $state='U', $callbackOnClose='', $callbackReference='');

				updateOption('googlePageSpeedAccessStatus', 'grant');

				header("Location:".APP_URL); 

		}else{
			header("Location:".APP_URL); 
		}

	}
}catch(Google_ServiceException $e){
	$cd = $e->getCode();
	$errors = $e->getErrors();
	addNotification($type='E', $title='Google PageSpeed Service Exception (Error#'.$cd.')', $message=$errors[0]['message'], $state='U', $callbackOnClose='', $callbackReference='');
	header("Location:".APP_URL); 
}
catch(Google_Exception $e){
	$cd = $e->getCode();
	$msg = $e->getMessage();
	addNotification($type='E', $title='Google PageSpeed Exception (Error#'.$cd.')', $message=$msg, $state='U', $callbackOnClose='', $callbackReference='');
	header("Location:".APP_URL); 
}


?>