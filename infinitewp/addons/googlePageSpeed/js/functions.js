var grantRevokeGPSData,isGooglePS=1;

function loadGooglePageSpeedData(data) {
	data = data.data.googlePageSpeedGetData;
	if(data != null){
		$('.PageSpeedContent').html(loadGooglePageSpeedHTML(data)).show();
		if($('.show_screenshot').length){
			$.each($('.show_screenshot'),function(){
				var siteID = $(this).closest('.row_detailed').attr('sid');
				imageData = data[siteID]['screenshot']['data'];
				imageData = imageData.replace(/-/g,'+');
				imageData = imageData.replace(/_/g,'/');
				$(this).qtip({
					content: '<img style="height:210px;" src="data:text/html;charset=utf-8;base64,'+imageData+'" />',
					position: { target: 'mouse', my: 'top right', at: 'left bottom' },
					style: { classes: 'ui-tooltip-shadow ui-tooltip-dark gps-custom-qtip-class'}
				});
			});
		}
	}else{
		$('.loadPageSpeedBtn').removeClass('disabled');
	}
}