function loadGooglePageSpeedPage()
{   
	var content='<div class="steps_hdr"><span id="processType">SELECT</span> <span class="itemUpper">Websites to analyze and get pagespeed insights</span></div><div class="siteSelectorContainer">'+siteSelectorVar+'</div><div class="btn_action float-right loadPageSpeedBtn disabled" ><a class="rep_sprite">Analyze</a></div><div class="PageSpeedContent" style="display:none;clear:both;"></div>';
	$("#pageContent").html(content);
	currentPage="gPageSpeed";

	if(grantRevokeGPSData == 'grant')
	{
		siteSelectorNanoMultiGoogle();
	}
	else
	{
		$('#pageContent').html('<div class="empty_data_set"><div class="line2">You have not connected your Google PageSpeed account to the admin panel. Go to <a class="googlePSSettingsClick" id="">Settings</a> to do it now.</div></div>');
	}

	$(".PageSpeedContent").hide();
	loadGooglePageSpeedContent();
	siteSelectorNanoReset();
	siteSelectorNanoMultiGoogle();
}
// function loadGooglePageSpeedGrantRevokeButton(data)						//available in base
// {
// 	$('.grantVal').val(data);
// 	var grantMe = data.data.googleServicesGetAPIKeys ;
// 	if((grantMe != null)&&(typeof grantMe != 'undefined'))
// 	{
// 	$('#clientID').val(grantMe.clientID);
// 	$('#clientSecretKey').val(grantMe.clientSecretKey);
// 	}
// }

function siteSelectorNanoMultiGoogle()
{
	
	$(".single_website_items_cont.nano").nanoScroller({stop: true});
	$(".single_website_items_cont").css('height',$(".single_website_items_cont").height()).addClass('nano');
	$(".single_website_items_cont.nano").nanoScroller();	
}

function processGooglePageSpeedForm(data)
{
    var mainData=data;
	$("#saveSettingsBtn").removeClass('disabled');
	$(".settings_cont .btn_loadingDiv").remove();
	if(data.data.googleServicesSaveAPIKeys == true)
	{
		$("#saveSuccess").show();
		setTimeout(function () {	/*$("#settings_cont").hide();*/ $("#saveSuccess").hide();},1000);
		$("#settings_btn").removeClass('active');
		window.location.href = ''+systemURL+'addons/googlePageSpeed/lib/googlePageSpeed.php?action=auth';
	}
}

function loadGooglePageSpeedContent(data)
{
	var tempArray={};
	tempArray['requiredData']={};
	
	tempArray['requiredData']['googlePageSpeedGrantAccess']=1;
	
	doCall(ajaxCallPath,tempArray,'storeGooglePageSpeedData');
}

function storeGooglePageSpeedData(data){
	grantRevokeGPSData=data.data.googlePageSpeedGrantAccess;
	// if(grantRevokeGPSData == 'grant')
	// {
	// 	$('.gps_grant').hide();
	// 	$('.gps_revoke').show();
	// }
	// else
	// {
	// 	$('.gps_grant').show();
	// 	$('.gps_revoke').hide();
		
	// }
}

function loadGooglePageSpeedPageContent(){
	var activeSites =$('.website_cont.active').length;
	if(activeSites>0){
		$('.loadPageSpeedBtn').removeClass('disabled');
	}else{
		$('.loadPageSpeedBtn').addClass('disabled');
		
	}
}

function getGooglePageSpeedHyperLinkedHeaderData(linkValue,data){
	if(data.indexOf(' of ')>=0){
		data = "<a class='droid700' href='"+linkValue+"' target='_blank'>"+data.replace(' of ','<\/a> of ');
	}else if(data.indexOf(' to ')>=0){
		data = "<a class='droid700' href='"+linkValue+"' target='_blank'>"+data.replace(' to ','<\/a> to ');
	}else if(data.indexOf(' for ')>=0){
		data = "<a class='droid700' href='"+linkValue+"' target='_blank'>"+data.replace(' for ','<\/a> for ');
	}else if(data.indexOf(' about ')>=0){
		data = data.replace(' about '," about <a class='droid700' href='"+linkValue+"' target='_blank'>")+'<\/a>';
	}else{
		data = "<a class='droid700' href='"+linkValue+"' target='_blank'>"+data+"<\/a>";
	}
	return data;
}

function googlePageSpeedFindAndReplaceArgs(headerData,dataType){
	if (typeof headerData != 'undefined' && objLen(headerData) > 0 ) {
		var returnFormat = headerData['format'];
		if (typeof headerData['args'] != 'undefined') {
			for(var i=0;i<headerData['args'].length;i++ ){
				if(headerData['args'][i]['type'] == 'HYPERLINK' && dataType == 'headers'){
					returnFormat = getGooglePageSpeedHyperLinkedHeaderData(headerData['args'][i]['value'],returnFormat);
				}else{
					var pattern = new RegExp('\\$'+(i+1), 'g');
					returnFormat = returnFormat.replace(pattern, headerData['args'][i]['value']);
				}
			}
			if(dataType=='result'){
				returnFormat = "<span class='resource_link'>"+returnFormat.replace(' (', "</span> <span class='saving_stats'>(")+"</span>";
				returnFormat = "<li>"+returnFormat+"</li>"
			}
		}
		return returnFormat;
	}
}


function getGooglePageSpeedUrlBlocksHTML(localizedRuleName,urlBlocks){
	var urlBlocksHTML = '';
	if(typeof urlBlocks != 'undefined' && objLen(urlBlocks) > 0 ){
		$.each(urlBlocks,function(index,urlBlocks_data){
			urlBlocksHTML += "<div class='headerData'>"+googlePageSpeedFindAndReplaceArgs(urlBlocks_data['header'],'headers')+"</div>";
			if(typeof urlBlocks_data['urls']!= 'undefined'){
				urlBlocksHTML += "<ul class='resource_list_cont'>";
				$.each(urlBlocks_data['urls'],function(index2,urls_data){
					urlBlocksHTML += googlePageSpeedFindAndReplaceArgs(urls_data['result'],'result');
				});
				urlBlocksHTML += "</ul>";
			}
		});
	}
	return urlBlocksHTML;
}


function loadGooglePageSpeedHTML(data){
	if(typeof data != 'undefined' && objLen(data) > 0 ){
		var HTMLData = '<div class="actionContent result_block siteSearch pagespeed" id=""><div class="th rep_sprite"> <div class="type_filter"> <input name="filter" type="text" class="input_type_filter searchSitePS" value="type to filter" /> <div class="clear_input rep_sprite_backup" onclick=""></div> </div> <div class="toggle_passed_rules_block float-right"><div class="checkbox toggle_passed_rules active"> Show passed rules </div></div> </div><div class="no_match hiddenCont" style="display:none">Bummer, there are no backups that match.<br>Try typing fewer characters.</div><div class="rows_cont">';
		var HTMLInnerData = '';
		$.each(data,function(siteID,pageSpeedData){
			var siteURL = sitesList['s'+siteID]['URL'];
			if(typeof pageSpeedData['formattedResults'] != 'undefined'){
				var shouldfix_tabs = considerfix_tabs = passedrules_tabs = '';
				var shouldfix_block = considerfix_block = passedrules_block = scoreType = '';
				var PStabs = [0,0,0];
				if( typeof pageSpeedData['formattedResults']['ruleResults'] != 'undefined' && objLen(pageSpeedData['formattedResults']['ruleResults']) > 0 ){
					$.each(pageSpeedData['formattedResults']['ruleResults'],function(ruleName,ruleDetails){
						if(ruleDetails['ruleImpact']>=10){
							shouldfix_tabs += "<div class='row_updatee'><div class='toggle_details float-right'><div class='show_details'>See how to fix this</div><div class='hide_details'>Hide Details</div></div><div class='localizedRuleName'>"+ruleDetails['localizedRuleName']+"</div><div class='urlBlocks'>"+getGooglePageSpeedUrlBlocksHTML(ruleDetails['localizedRuleName'],ruleDetails['urlBlocks'])+"</div><div class='clear-both'></div></div>";
							PStabs[0] += 1;
						}else if (ruleDetails['ruleImpact']>0 && ruleDetails['ruleImpact']<10){
							considerfix_tabs += "<div class='row_updatee'><div class='toggle_details float-right'><div class='show_details'>See how to fix this</div><div class='hide_details'>Hide Details</div></div><div class='localizedRuleName'>"+ruleDetails['localizedRuleName']+"</div><div class='urlBlocks'>"+getGooglePageSpeedUrlBlocksHTML(ruleDetails['localizedRuleName'],ruleDetails['urlBlocks'])+"</div><div class='clear-both'></div></div>";
							PStabs[1] += 1;
						}else{
							passedrules_tabs += "<div class='row_updatee'><div class='toggle_details float-right'><div class='show_details'>Learn More</div><div class='hide_details'>Hide Details</div></div><div class='localizedRuleName'>"+ruleDetails['localizedRuleName']+"</div><div class='urlBlocks'>"+getGooglePageSpeedUrlBlocksHTML(ruleDetails['localizedRuleName'],ruleDetails['urlBlocks'])+"</div><div class='clear-both'></div></div>";
							PStabs[2] += 1;
						}
					});
				}
				if(PStabs[0] != 0) shouldfix_block = "<div class='ps_result_classification'><div class='ps_result shouldfix'></div><div class='row_updatee'><span class='ps_result_classification_title droid700'>SHOULD FIX</span><div class='clear-both'></div></div>"+shouldfix_tabs+"</div>";
				if(PStabs[1] != 0) considerfix_block = "<div class='ps_result_classification'><div class='ps_result considerfix'></div><div class='row_updatee'><span class='ps_result_classification_title droid700'>CONSIDER FIXING</span><div class='clear-both'></div></div>"+considerfix_tabs+"</div>";
				if(PStabs[2] != 0) passedrules_block = "<div class='ps_result_classification'><div class='ps_result passed'></div><div class='row_updatee'><span class='ps_result_classification_title droid700'>PASSED RULES</span><div class='clear-both'></div></div>"+passedrules_tabs+"</div>";
				if(pageSpeedData['score'] >= 85) scoreType = 'excellent';
				if(pageSpeedData['score'] >= 65 && pageSpeedData['score'] < 85) scoreType = 'warning';
				if(pageSpeedData['score'] < 65) scoreType = 'error';

				HTMLInnerData = HTMLInnerData+"<div class='ind_row_cont'><div class='row_summary' sid='"+siteID+"'><div class='row_arrow'></div><div class='row_name searchable'><span class='analyse_score "+scoreType+"'>"+pageSpeedData['score']+"/100</span>"+siteURL+"</div><div class='clear-both'></div></div><div class='row_detailed' sid='"+siteID+"' style='display:none;clear:both;'><div class='rh'><div class='row_arrow'></div><div class='row_name'><span class='analyse_score "+scoreType+"'>"+pageSpeedData['score']+"/100</span>"+siteURL+"</div><span class='show_screenshot float-right'>View site screenshot</span><div class='clear-both'></div></div><div class='rd'>"+shouldfix_block+" "+considerfix_block+" "+passedrules_block+"</div></div></div>";
			}
			if(typeof pageSpeedData['error'] != 'undefined'){
				HTMLInnerData += "<div class='ind_row_cont'><div class='row_summary' sid='"+siteID+"'><div class='row_arrow'></div><div class='row_name searchable'><span class='analyse_score error'>-/100</span>"+siteURL+"</div><div class='clear-both'></div></div><div class='row_detailed' sid='"+siteID+"' style='display:none;clear:both;'><div class='rh'><div class='row_arrow'></div><div class='row_name'><span class='analyse_score error'>-/100</span>"+siteURL+"</div><div class='clear-both'></div></div><div class='rd'><div class='row_updatee'><div class='localizedRuleName'>"+pageSpeedData['error']['message']+"</div><div class='clear-both'></div></div></div></div></div>"
			}
		});
		HTMLData = HTMLData+HTMLInnerData+'</div></div>';
		return HTMLData;
	}
}
