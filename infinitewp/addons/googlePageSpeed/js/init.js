$(function(){

	googleSettings = '<li>Google</li>';
	googlePageSpeedAccess = '<div id="gPageSpeedAccess" class="tr"> <div class="padding" style="font-size:12px;"><div class="label" style="padding: 10px 0;">CONNECT YOUR GOOGLE PageSpeed ACCOUNT<a href="http://infinitewp.com/knowledge-base/how-to-connect-your-google-apis-account/?utm_source=application&utm_medium=userapp&utm_campaign=kb" style="padding:0; float:right; display:inline; text-transform:none;" target="_blank">See instructions</a></div> <div class="tl no_text_transform" style="width:475px;"> <div><div class="form_label">Authorized Redirect URI</div><input type="text" class="selectOnText" style="width:466px;" value="'+systemURL+'addons/googlePageSpeed/lib/googlePageSpeed.php" readonly="true" /></div> <div class="clear-both"></div></div> <div class="clear-both"></div><div class="btn_action float-right gps_grant" ><a class="rep_sprite btn_blue" id="gps_grant_revoke" href="'+systemURL+'addons/googlePageSpeed/lib/googlePageSpeed.php?action=auth" >Grant Access</a></div> <div class="btn_action float-right gps_revoke" style="display:none" ><a class="rep_sprite btn_blue" id="gps_grant_re" style="" href="'+systemURL+'addons/googlePageSpeed/lib/googlePageSpeed.php?action=revoke">Revoke Access</a></div> <div class="clear-both"></div> </div> </div> ';
	// $('#googleTab').append(googlePageSpeedAccess);
	

	tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['googlePageSpeedGrantAccess']=1;
	// tempArray['requiredData']['googleServicesGetAPIKeys']=1;				//available in base
	doCall(ajaxCallPath,tempArray);
	loadGooglePageSpeedContent();
	
});

$(".googlePSSettingsClick").live('click',function() {
	openSettingsPage('Google');
    return false;
	
});

$(".gps_grant_revoke_click").live('click',function() {
	$('#gps_grant_revoke').click();
});

$('#gps_grant_revoke').live('click',function(){
	var tempArray={};
	tempArray['action']='';
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['requiredData']={};
	var paramsArray={};
	$("#clientCreds .formVal").each(function () {
		paramsArray[$(this).attr('id')]=$(this).val();
	});
	paramsArray['redirect']=1;
	tempArray['requiredData']['googleServicesSaveAPIKeys']=paramsArray;
	grant_revoke_click = ".gps_grant_revoke_click";
	doCall(ajaxCallPath,tempArray,'processGooglePageSpeedForm');
});


$(".loadPageSpeedBtn").live('click',function() {
	
	    $(this).addClass('disabled');
	    $('.PageSpeedContent').html('').hide();
		var siteIDs=[];
		$.each($('.website_cont.active'),function(){
			siteIDs.push($(this).attr('sid'));
		});
	    var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['googlePageSpeedGetData']={};
		tempArray['requiredData']['googlePageSpeedGetData']['siteIDs']=siteIDs;
		doCall(ajaxCallPath,tempArray,'loadGooglePageSpeedData');
		
});

$('.toggle_passed_rules').live('click',function(){
	if($(this).hasClass('active')){
		$(this).removeClass('active');
		$('.ps_result.passed').closest('.ps_result_classification').hide();
	}else{
		$(this).addClass('active');
		$('.ps_result.passed').closest('.ps_result_classification').show();
	}

});

$('.show_screenshot').live('click',function(){
	return false;
});

$('.show_details').live('click',function(){
	$(this).hide();
	$(this).parent().find('.hide_details').show();
	$(this).closest('.row_updatee').find('.urlBlocks').show();
	$(this).closest('.row_updatee').find('.localizedRuleName').addClass('droid700');
});

$('.hide_details').live('click',function(){
	$(this).hide();
	$(this).parent().find('.show_details').show();
	$(this).closest('.row_updatee').find('.urlBlocks').hide();
	$(this).closest('.row_updatee').find('.localizedRuleName').removeClass('droid700');
});

$(".searchSitePS").live('keyup',function() {
	searchSites(this,4);
});