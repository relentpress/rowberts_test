<?php

/*
slug: googlePageSpeed
version: 1.0.5
*/


class addonGooglePageSpeed{
	
	private static $version = '1.0.5';

	public static function version(){
		return self::version;
	}

	public static function init(){
		require_once(APP_ROOT."/addons/googlePageSpeed/controllers/func.php");	
		panelRequestManager::addFunctions('googlePageSpeedGrantAccess','googlePageSpeedGetAPIKeys','googlePageSpeedSaveAPIKeysAndRedirect','googlePageSpeedGetData');
		regHooks('addonMenus');
	}

	public static function update(){
		
		if(version_compare(getOldAddonVersion('googlePageSpeed'), '1.0.0') === -1){
			//from v1.0.0 Google API Lib is kept common in base app, so deleting common API files here
			
			if(!is_object($GLOBALS['FileSystemObj'])){
				if(!initFileSystem()){
					appUpdateMsg('Unable to initiate file system.', true);
					exit;
				}
			}
			
			$panelLocation = $GLOBALS['FileSystemObj']->findFolder(APP_ROOT);
			$panelLocation  = removeTrailingSlash($panelLocation);
			$GLOBALS['FileSystemObj']->delete($panelLocation.'/addons/googlePageSpeed/lib/authHelper.php', false, 'f');
			$GLOBALS['FileSystemObj']->delete($panelLocation.'/addons/googlePageSpeed/lib/storage.php', false, 'f');
			$GLOBALS['FileSystemObj']->delete($panelLocation.'/addons/googlePageSpeed/lib/src', true);
		}
		return true;
	}

}