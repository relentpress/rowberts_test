<?php

/*
slug: codeSnippets
version: 1.0.3
*/

class addonCodeSnippets{
	
	private static $version = '1.0.3';
	
	public static function version(){
		return self::version;	
	}
	
	public static function init(){
		require_once(APP_ROOT."/addons/codeSnippets/controllers/func.php");	
		require_once(APP_ROOT."/addons/codeSnippets/controllers/manageClientsCodeSnippets.php");
		panelRequestManager::addFunctions('codeSnippetsAdd', 'codeSnippetsGet', 'codeSnippetsGetAll', 'codeSnippetsDelete','codeSnippetsGetAllHTML','codeSnippetsResult');
		regHooks('responseProcessors', 'taskTitleTemplate', 'addonMenus');
	}	
	
	public static function install(){
		$Q1 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:code_snippets` (
							`cmdID` INT UNSIGNED NULL AUTO_INCREMENT ,
							`cmdName` varchar(50) NOT NULL ,
							`cmd` TEXT NOT NULL ,
							PRIMARY KEY (  `cmdID` ),
							UNIQUE KEY `cmdName` (`cmdName`)
							) ENGINE = MYISAM DEFAULT CHARSET=latin1;");
		if($Q1){
			return true;	
		}
		return false;
	}
}

?>