$(function () {


	$(".loadSnippet").live('click',function() {
		$("#duplicateSnippet").remove();
		$(".run_code_help_text").fadeOut(200);
	$(".loadSnippet").removeClass('active');
	$(this).addClass('active');
	var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['codeSnippetsGet']={};
	tempArray['requiredData']['codeSnippetsGet']['cmdID']=$(this).attr('cmdid');
	doCall(ajaxCallPath,tempArray,'loadSingleSnippet');
	});
	$(".run_code_help_text").live('click',function() {
		$(".run_code_terminal").focus();
		$(this).fadeOut(200);
	});
	$(".run_code_terminal").live('focus',function() {
		$(".run_code_help_text").fadeOut(200);
	}).live('blur',function() { 
	if($(".run_code_terminal").val()=='')
	$(".run_code_help_text").fadeIn(200);
	}).live('keyup',function() {
		if($(this).val()!='')
		$("#runSnippetCode").removeClass('disabled');
		else
		$("#runSnippetCode").addClass('disabled');
	});
	$(".save_snippet").live('click',function() {
		$("#duplicateSnippet").remove();
		if($(".new_snippet_name").val()=='')
		{
		$(".save_snippet").before('<div id="duplicateSnippet">Enter a snippet name</div>');
		return false;
		}
		$(this).addClass('disabled');
		$(this).append('<div class="btn_loadingDiv"></div>');
		var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['codeSnippetsAdd']={};
		tempArray['requiredData']['codeSnippetsAdd']['cmdName']=$(".new_snippet_name").val();
		tempArray['requiredData']['codeSnippetsAdd']['cmd']=$(".run_code_terminal").val();
		tempArray['requiredData']['codeSnippetsGetAllHTML']=1;
		doCall(ajaxCallPath,tempArray,'addNewSnippet');
		
	});
	$(".loadSnippet .remove_bg").live('mouseenter',function() {
		$(this).closest(".loadSnippet").addClass('delWarn');
	}).live('mouseleave',function() {
		$(this).closest(".loadSnippet").removeClass('delWarn');
	});
	$(".delSnippet").live('click',function() {
		var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['codeSnippetsDelete']={};
		tempArray['requiredData']['codeSnippetsDelete']['cmdID']=$(this).attr('cmdid');
		doCall(ajaxCallPath,tempArray,'');
		$(this).closest(".loadSnippet").remove();
		return false;
	});
	$("#runSnippetCode").live('click',function() {
		var tempArray={};
		tempArray['action']="codeSnippetsRunCmd";
		$(this).addClass('disabled');
		$(this).prepend('<div class="btn_loadingDiv left"></div>');
		tempArray['args']={};
		tempArray['args']['params']={};
		tempArray['args']['params']['cmd']=$(".run_code_terminal").val();
		tempArray['requiredData']={};
		tempArray['requiredData']['codeSnippetsResult']=1;
		var siteArray=getSelectedSites();
		tempArray['args']['siteIDs']=siteArray;
		doCall(ajaxCallPath,tempArray,'formArrayRunCode','json',"none");
		return false;
	});
	});