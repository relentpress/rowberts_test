function loadSnippetPage(data)
{

	var content='<div class="steps_hdr">Select websites to <span id="processType">RUN</span> <span class="itemUpper">Code</span></div><div class="siteSelectorContainer">'+siteSelectorVar+'</div><div class="result_block shadow_stroke_box siteSearch itemPanel" style="border:0;box-shadow: 0 0 0  rgba(0, 0, 0, 0)"><div class="snippetDataContent" style="display:none"><div class="shadow_stroke_box" style="width:630px; float:left; margin-right:20px; position:relative;"> <div class="th rep_sprite"><div class="title"><span class="droid700">Run Code Snippets</span></div></div> <textarea name="" cols="" rows="" class="run_code_terminal"></textarea> <div class="run_code_help_text">Type in or copy & paste your code here. <br /> You can also load saved snippets →</div> <div class="th rep_sprite" style="border-top: 1px solid #D2D5D7; border-bottom:0;"><div class="save_snippet_form"><input name="" type="text" class="new_snippet_name onEnter" onenterbtn=".save_snippet" /> <div class="save_snippet rep_sprite float-left" >Save</div></div> <div class="btn_action float-right"><a class="rep_sprite disabled" id="runSnippetCode" >Run Code</a></div> </div> </div><div class="shadow_stroke_box run_code_saved_snippets" style="width:304px; float:left;"> <div class="th rep_sprite"><div class="title"><span class="droid700">Saved Code Snippets</span></div></div> <div class="rows_cont codeSnippetList nano" style="height: 272px;"><div class="content"></div> </div>    </div></div><div class="clear-both"></div><div class="shadow_stroke_box response responseFullDiv" style="display:none"> <div class="th rep_sprite"><div class="title"><span class="droid700">Response</span></div></div><div id="responseCont"></div></div>	';
	$("#pageContent").html(content);
	currentPage="codeSnippet";
	$(".navLinks").removeClass('active');
	if(data==undefined || data=='')
	{
	var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['codeSnippetsGetAllHTML']=1;
	doCall(ajaxCallPath,tempArray,'loadSnippetPage');
	}
	else
	$(".codeSnippetList .content").html(data.data.codeSnippetsGetAllHTML);
	
	
	siteSelectorNanoReset();
  
}

