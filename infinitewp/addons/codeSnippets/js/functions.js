function showCodeSnippetOptions()
{
	if(!isSiteSelected())
	$(".snippetDataContent").hide();
	else
	{
		$(".codeSnippetList").nanoScroller({stop: true});
	$(".snippetDataContent").show();
	$(".codeSnippetList").nanoScroller();
	}
}

function loadSingleSnippet(data)
{
	$(".run_code_terminal").val(data.data.codeSnippetsGet.cmd);
	if(data.data.codeSnippetsGet.cmd!='')
	$("#runSnippetCode").removeClass('disabled');
	else
	$("#runSnippetCode").addClass('disabled');
}

function addNewSnippet(data)
{
	$(".btn_loadingDiv").remove();
	$(".save_snippet").removeClass('disabled');
	if($.isNumeric(data.data.codeSnippetsAdd))
	$(".codeSnippetList .content").html(data.data.codeSnippetsGetAllHTML);
	else
	$(".save_snippet").before('<div id="duplicateSnippet">'+data.data.codeSnippetsAdd+'</div>');
	
}

function processSnippetResponse(data)
{
	$("#runSnippetCode").removeClass('disabled');
	$(".btn_loadingDiv").remove();
	var content='';
	 $.each(data.data.codeSnippetsResult, function(siteID, val) {
		content=content+'<div class="site_response_cont"><div class="site_name">'+site[siteID].name+'</div><div class="response">'+val+'</div></div>';
	 });
	$(".responseFullDiv").show();
	$("#responseCont").html(content);
}

function formArrayRunCode(data)
{
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="runCode";
	
	
}
