<?php

function codeSnippetsAddonMenus(&$menus){
	$menus['tools']['subMenus'][] = array('page' => 'codeSnippets', 'displayName' => 'Code Snippets');
}

function codeSnippetsAdd($params){
	if(empty($params)){
		return false;
	}
	
	$exists = DB::getExists("?:code_snippets", "cmdName", "cmdName='".$params['cmdName']."'");
	if(!$exists){
		$cmd = $params['cmd'];
		$query = DB::insert("?:code_snippets", array("cmdName" => $params['cmdName'], "cmd" => $cmd));
		if(!empty($query)){
			return $query;
		}
		else{
			return "Oops! Try again.";
		}
	}
	else{
		return "Snippet already exists.";
	}
}

function codeSnippetsGet($params){
	if(empty($params)){
		return false;
	}
	$data = DB::getRow("?:code_snippets", "*", "cmdID='".$params['cmdID']."'");
	if(!empty($data)){
		$data['cmd'] = $data['cmd'];
		return $data;
	}
	else{
		return false;
	}
}

function codeSnippetsGetAll(){
	$snippets = DB::getArray("?:code_snippets", "*", "1 ORDER BY cmdID DESC");
	
	if(!empty($snippets)){
		return $snippets;
	}
	else{
		return false;
	}
}

function codeSnippetsGetAllHTML(){
	$snippets = codeSnippetsGetAll();
	$HTML = TPL::get('/addons/codeSnippets/templates/view.tpl.php', array('snippets' => $snippets));
	return $HTML;
	
}

function codeSnippetsDelete($params){
	if(empty($params)){
		return false;
	}
	
	return DB::delete("?:code_snippets", "cmdID = '".$params['cmdID']."'");
	
}

function codeSnippetsResult(){
	
	$actionID = Reg::get('currentRequest.actionID');
		
	$responseDatas = DB::getFields("?:temp_storage", "data", "type = 'codeSnippets' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'getUsers' AND paramID = '".$actionID."'");	
		
	if(empty($responseDatas)){
		return array();
	}
	$finalData = array();
	foreach($responseDatas as $data){
		$finalData = array_merge_recursive($finalData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($finalData);
	return $finalData;
}


function codeSnippetsResponseProcessors(&$responseProcessor){
	$responseProcessor['codeSnippets']['execute'] = 'codeSnippetsRunCmd';
}

function codeSnippetsTaskTitleTemplate(&$template){
	$template['codeSnippets']['execute']['']	= "Run code in <#sitesCount#> site<#sitesCountPlural#>";
}

?>