<?php
class manageClientsCodeSnippets{
	
	public static function codeSnippetsRunCmdProcessor($siteIDs, $params){
				
		$type = "codeSnippets";
		$action = "execute";
		$requestAction = "execute_php_code";
		$cmd = $params['cmd'];

		$requestParams = array('code' => $cmd);	
		
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => $action, 'detailedAction' => $type);
		
		foreach($siteIDs as $siteID){
			$siteData = getSiteData($siteID);
			
			$events = 1;
			
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
			$PRP['doNotExecute'] 			= false;
			$PRP['sendAfterAllLoad'] = true;
					  
			prepareRequestAndAddHistory($PRP);
		}
		
	}
	
	public static function codeSnippetsRunCmdResponseProcessor($historyID, $responseData){
		responseDirectErrorHandler($historyID, $responseData);
		
		$response = array();
		if(isset($responseData['success'])){
			$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
			$actionID = $historyData['actionID'];
			$siteID = $historyData['siteID'];
			$response['_'.$siteID] = $responseData['success'];
			
			DB::insert("?:temp_storage", array('type' => 'codeSnippets', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
			DB::update("?:history_additional_data", "status='success'", "historyID=".$historyID."");
			return;
		}
	}
}

manageClients::addClass('manageClientsCodeSnippets');

?>