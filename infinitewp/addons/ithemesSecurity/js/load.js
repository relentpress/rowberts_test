/************************************************************
* InfiniteWP Admin panel - iThemes Security Plugin									*
* Copyright (c) 2014 Revmakx								*
* www.revmakx.com											*
*															*
************************************************************/
/*
 * Load Site selector
 */
function loadIthemesSecurityMainPage() {
        var content='<div class="steps_hdr"><span id="processType">SELECT WEBSITE TO MANAGE</span> <span class="itemUpper">iThemes Security</span><div id="ithemes_security_installation_bulk" class="float-right"><a class="siteSelectorSelect">Install & activate iThemes Security plugin in all sites</a></div></div><div class="siteSelectorContainer">'+siteSelectorVar+'</div><div class="btn_action float-right load_itheames_security_result disabled" ><a class="rep_sprite" id="LoadIthemesSecurity">Load Results</a></div><div id="ithemes_security_content" style="display:none;clear:both;"></div>';
	$("#pageContent").html(content);
	$(".website_cont").addClass('hidden_its disabled');
	currentPage="ithemesSecurity";
	ithemesSecurityGetRecentPluginsStatusAndCheck();//getRecentPluginsStatusAndCheck(); //change this later
	var tempArray = {};
	tempArray['requiredData'] = {};
	tempArray['requiredData']['fetchRecentPluginsStatus'] = 1;
	doCall(ajaxCallPath,tempArray,'ithemesSecurityGetRecentPluginsStatusAndCheck');//getRecentPluginsStatusAndCheck() //change this later
}

function classifyIthemesSecurityResult(data) {
	var siteIDs   = data['siteIDs'];
	var classified = {};
	$.each(siteIDs,function(index,siteID){
		var siteData = [];
		var siteURL = data['siteURLs'][index];
		if (typeof data[siteID].high != 'undefined') {
			siteData['high'] = data[siteID].high;
		} 
                if(typeof data[siteID].low != 'undefined') {
			siteData['low'] = data[siteID].low;
		}
                if(typeof data[siteID].medium != 'undefined') {
			siteData['medium'] = data[siteID].medium;
		}
		classified[siteID] = {'high':[],'low':[],'medium':[]};
		classified[siteID]['totalCount'] = classified[siteID]['highCount'] = classified[siteID]['lowCount'] = classified[siteID]['mediumCount'] = 0;
                

		classified[siteID]['siteURL'] = siteURL;
		if (typeof siteData['high'] != 'undefined') {
			$.each(siteData['high'],function(resIndex,resData){
                                if(typeof resData.advanced != 'undefined')
                                    var page = "advanced";
                                else 
                                    var page = "settings";
				var issues = {'text':resData.text, 'link':resData.link, 'page':page}; 
				classified[siteID]['high'].push(issues);
				classified[siteID]['highCount']++;
                                classified[siteID]['totalCount']++;
			});
		}
                if (typeof siteData['low'] != 'undefined') {
			$.each(siteData['low'],function(resIndex,resData){
                                if(typeof resData.advanced != 'undefined')
                                    var page = "advanced";
                                else 
                                    var page = "settings";
				var issues = {'text':resData.text, 'link':resData.link, 'page':page};
				
				classified[siteID]['low'].push(issues);
				classified[siteID]['lowCount']++;
                                classified[siteID]['totalCount']++;
			});
		}
                if (typeof siteData['medium'] != 'undefined') {
			$.each(siteData['medium'],function(resIndex,resData){
				if(typeof resData.advanced != 'undefined')
                                    var page = "advanced";
                                else 
                                    var page = "settings";
				var issues = {'text':resData.text, 'link':resData.link, 'page':page};
				classified[siteID]['medium'].push(issues);
				classified[siteID]['mediumCount']++;
                                classified[siteID]['totalCount']++;
			});
		}
	});
	return classified;
}

function loadIthemesSecurityContentHTML(contentData) {
	var HTMLData='<div class="actionContent result_block siteSearch" id=""><div class="th rep_sprite"><div class="type_filter"> <input name="filter" type="text" class="input_type_filter searchSiteBL" value="type to filter sites" /> </div> </div><div class="no_match hiddenCont" style="display:none">Bummer, there are no scan result that match.<br>Try typing fewer characters.</div><div class="rows_cont">';
	//For display the issue
	$.each(contentData,function(siteID,siteData){
		var issueResult = '';
		var totalCount = siteData.totalCount;
		var highCount = siteData.highCount;
		var lowCount = siteData.lowCount;
		var mediumCount = siteData.mediumCount;
		if(totalCount!=0) {
                        if(highCount!=0) {
                            issueResult += '<div class="row_updatee"> <div class="row_updatee_ind"> <div class="label_updatee  float-left"> <div class="label droid700 float-left"><span class="high">High</span></div> <div class="count float-left"><span>'+highCount+'</span></div> <div class="clear-both"></div> </div><div class="items_cont float-left">';
                            $.each(siteData.high,function(issueId,issueData){
                            issueResult += '<div class="item_ind float-left"> <div class="item float-left">'+issueData.text+'</div> <div class="select_operation"> <ul class="btn_radio_slelect small float-left"><li><a  sid="'+siteID+'" pageid="'+issueData.page+'" linkid="'+issueData.link+'" class="rep_sprite actionButton loadIthemesSecurityFix actionButtonRounded ">Fix</a></li></ul> </div></div>';

                            });
                            issueResult += '</div><div class="clear-both"></div></div> </div>'
                        }
                        if(mediumCount!=0) {
                            issueResult += '<div class="row_updatee"> <div class="row_updatee_ind"> <div class="label_updatee  float-left"> <div class="label droid700 float-left"><span class="medium">Medium</span></div> <div class="count float-left"><span>'+mediumCount+'</span></div> <div class="clear-both"></div> </div><div class="items_cont float-left">';
                            $.each(siteData.medium,function(issueId,issueData){
                            issueResult += '<div class="item_ind float-left"> <div class="item float-left">'+issueData.text+'</div> <div class="select_operation"> <ul class="btn_radio_slelect small float-left"><li><a sid="'+siteID+'" pageid="'+issueData.page+'" linkid="'+issueData.link+'" class="rep_sprite actionButton loadIthemesSecurityFix actionButtonRounded ">Fix</a></li></ul> </div></div>';

                            });
                            issueResult += '</div><div class="clear-both"></div></div> </div>'
                        }
                        if(lowCount!=0) {
                            issueResult += '<div class="row_updatee"> <div class="row_updatee_ind"> <div class="label_updatee  float-left"> <div class="label droid700 float-left"><span class="low">Low</span></div> <div class="count float-left"><span>'+lowCount+'</span></div> <div class="clear-both"></div> </div><div class="items_cont float-left">';
                            $.each(siteData.low,function(issueId,issueData){
                            issueResult += '<div class="item_ind float-left"> <div class="item float-left">'+issueData.text+'</div> <div class="select_operation"> <ul class="btn_radio_slelect small float-left"><li><a sid="'+siteID+'" pageid="'+issueData.page+'" linkid="'+issueData.link+'" class="rep_sprite actionButton loadIthemesSecurityFix actionButtonRounded ">Fix</a></li></ul> </div></div>';

                            });
                            issueResult += '</div><div class="clear-both"></div></div> </div>'
                        }
                        
			if(totalCount!=0) {
			HTMLData = HTMLData+"<div class='ind_row_cont'><div class='row_summary' sid='"+siteID+"'><div class='row_arrow'></div><div style='padding:9px;'><div class='count_cont float-left'>"+totalCount+"</div></div><div class='row_name searchable'>"+siteData.siteURL+"</div><div class='row_action float-right'><a class='adminIthemesSecurityPopout' sid='"+siteID+"'><span class='statusSpan'>View details</span></a></div><div class='clear-both'></div></div><div class='row_detailed' sid='"+siteID+"' style='display:none;clear:both;'><div class='rh'><div class='row_arrow'></div><div style='padding:9px;'><div class='count_cont float-left'>"+totalCount+"</div></div><div class='row_name'>"+siteData.siteURL+"</div><div class='row_action float-right'><a class='adminIthemesSecurityPopout' sid='"+siteID+"'><span class='statusSpan'>View details</span></a></div><div class='clear-both'></div></div><div class='rd'>"+issueResult+"</div></div></div>";
			} else {
				HTMLData = HTMLData+"<div class='ind_row_cont'><div class='row_no_detail' sid='"+siteID+"'><div class='row_name searchable'>"+siteData.siteURL+"</div><div class='item_result success'>No issues found</div><div class='clear-both'></div></div></div>";
			}
		}
	});
	
	
	
	HTMLData = HTMLData+'</div></div>';
	return HTMLData;
}

function loadIthemesSecurityAdminPopout(object,sid) {

	where='&where=admin';
	var_0='var_0=page__IWPVAR__itsec';
	
	var processLink=ajaxCallPath+'?action=loadSite&siteID='+sid+where+'&'+var_0;
	$(object).attr('href',processLink);
	$(object).attr('target','_blank');
	$(object).attr('clicked','1');
	
	resetBottomToolbar();
	
}

function loadIthemesSecurityFixAdminPopout(object,sid,pageid,linkid) {

    where='&where=admin';
    var_0='var_0=page__IWPVAR__toplevel_page_itsec_'+pageid+linkid;

    var processLink=ajaxCallPath+'?action=loadSite&siteID='+sid+where+'&'+var_0;
    $(object).attr('href',processLink);
    $(object).attr('target','_blank');
    $(object).attr('clicked','1');

    resetBottomToolbar();
}

function installIthemesSecurity(data){
   
	var data = data.data.installNotInstalledPlugin;
	var dLink = data[0].download_link;
	var tempArray={};
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['args']['siteIDs']={};
	var params={}; 
	tempArray['action'] = 'installPlugins';
	params['plugins']={};
	tempArray['args']['siteIDs'][0] = data[1];
	params['plugins']['0']=dLink;
	params['activate'] = true;
	tempArray['args']['params'] = params;
	doHistoryCall(ajaxCallPath,tempArray,'ithemesSecurityPluginInitialization');
}

function installIthemesSecurityMultiSites(data){
	var data = data.data.installNotInstalledPlugin;
	var dLink = data[0].download_link;
	var tempArray={};
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['args']['siteIDs']=data[1];
	var params={}; 
	tempArray['action'] = 'installPlugins';
	params['plugins']={};
	params['plugins']['0']=dLink;
	params['activate'] = true;
	tempArray['args']['params'] = params;
	doHistoryCall(ajaxCallPath,tempArray,'ithemesSecurityPluginInitialization');
}