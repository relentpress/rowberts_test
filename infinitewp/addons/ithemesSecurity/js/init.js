/************************************************************
* InfiniteWP Admin panel - iThemes Security Plugin									*
* Copyright (c) 2014 Revmakx								*
* www.revmakx.com											*
*															*
************************************************************/
/*
 * Document On loads
 */
$(function(){
    
    $(".load_itheames_security_result").live('click',function() {
            var siteIDs = new Array();
            $("#pageContent").find('#ithemes_security_content').html('').hide();
            $('.website_cont.active').each(function(){
                    siteIDs.push($(this).attr('sid'));
            });

            var tempArray={};
            var action = 'loadScan';
            $(".load_itheames_security_scan").addClass('disabled');

            tempArray['action']='ithemesSecurityLoad';
            tempArray['args']={};
            tempArray['args']['siteIDs']=siteIDs;
            tempArray['args']['params']={};
            tempArray['args']['params']['action'] = action;
            tempArray['requiredData'] = {};
            tempArray['requiredData']['ithemesSecurityLoad'] = 1;
            doCall(ajaxCallPath,tempArray,'formArrayIthemesSecurityLoad');

    });
    
    $(".adminIthemesSecurityPopout").live('click',function(e) {
		if($(this).attr('clicked')!=1)
		{
			loadIthemesSecurityAdminPopout(this,$(this).attr('sid'));
			$(this).attr('clicked','0');
		}
		
		e.stopImmediatePropagation();

	});
        
        $(".loadIthemesSecurityFix").live('click',function(e) {
		
            loadIthemesSecurityFixAdminPopout(this,$(this).attr('sid'),$(this).attr('pageid'),$(this).attr('linkid'));
            $(this).attr('clicked','0');
            e.stopImmediatePropagation();

	});
        
        $('.website_cont.hidden_its').live('hover',function(e){
		if($(this).hasClass('disabled'))
		{
			$(this).removeClass('disabled');
			$(this).find('.tips').show();
		}
		else if($(this).find('.tips').length)
		{
			$(this).addClass('disabled');
			$(this).find('.tips').hide();
		}
		
	});

	$('.website_cont.hidden_its .tips').live('click',function(){
		if(typeof $(this).attr('code') !== 'undefined' && $(this).attr('code') !== false){
		var action = $(this).attr('code') == '1'?'activate':'install' ; 
		}else{
			return false;
		}
		var siteID = $(this).closest('.website_cont.hidden_its').attr('sid');
                
		if(action == 'install'){
			var tempArray={};
			tempArray['requiredData']={};
			var valArray={};
			valArray['type']='plugins';
			valArray['siteID']=siteID;
			valArray['plugin_slug']='better-wp-security';
			tempArray['requiredData']['installNotInstalledPlugin']=valArray;
			doCall(ajaxCallPath,tempArray,"installIthemesSecurity","json","none");
		}else if(action == 'activate'){

			var changeArray={};
			var dID, type, action, name, valArray;
			dID="better-wp-security/better-wp-security.php";
			type = 'plugins';
			action="activate";
			name="iThemes Security";
			changeArray[siteID]={};
			changeArray[siteID][type]={};
			valArray={};
			valArray['name']=name;
			valArray['path']=dID;
			valArray['action']=action;
			changeArray[siteID][type][0]=valArray;

			var tempArray={};
			tempArray['args']={};
			tempArray['args']['params']={};
			tempArray['action']='managePlugins';
			tempArray['args']['params'] = changeArray;
			doHistoryCall(ajaxCallPath,tempArray,'ithemesSecurityPluginInitialization');
		}
		$(this).closest('.website_cont.hidden_its').removeClass('disabled');
		$(this).remove();
	});
        
        $('#ithemes_security_installation_bulk').live('click',function(){
		var activationSiteIDs = [],installationSiteIDs = [],isActing=1;
		$('.website_cont.hidden_its .tips').each(function(){
			var siteID = $(this).closest('.website_cont.hidden_its').attr('sid');
			if(typeof $(this).attr('code') !== 'undefined' && $(this).attr('code') !== false){
			if($(this).attr('code') == '1'){
				activationSiteIDs.push(siteID);
				}else if($(this).attr('code') == '0'){
				installationSiteIDs.push(siteID);
			}
			}else{
				isActing = 0;
				return false;
			}
		});
		// activation bulk starts
		if(activationSiteIDs.length>0){
			var changeArray={};
			var dID, type, action, name, valArray;
			dID="better-wp-security/better-wp-security.php";
			type = 'plugins';
			action="activate";
			name="iThemes Security";
			$.each(activationSiteIDs,function(index,siteID){
				changeArray[siteID]={};
				changeArray[siteID][type]={};
				valArray={};
				valArray['name']=name;
				valArray['path']=dID;
				valArray['action']=action;
				changeArray[siteID][type][0]=valArray;
			});

			var tempArray={};
			tempArray['args']={};
			tempArray['args']['params']={};
			tempArray['action']='managePlugins';
			tempArray['args']['params'] = changeArray;
			doHistoryCall(ajaxCallPath,tempArray,'ithemesSecurityPluginInitialization');
		}
		// activation bulk ends

		// installation bulk starts
		if(installationSiteIDs.length>0){
			
			var tempArray={};
			tempArray['requiredData']={};
			var valArray={};
			valArray['type']='plugins';
			valArray['siteID']=installationSiteIDs;
			valArray['plugin_slug']='better-wp-security';
			tempArray['requiredData']['installNotInstalledPlugin']=valArray;
			doCall(ajaxCallPath,tempArray,"installIthemesSecurityMultiSites","json","none");
		}
		// installation bulk ends
		if(isActing){	
		$('.website_cont.disabled .tips').remove();
		$('.website_cont.disabled').removeClass('disabled');
		}
	});
    
});