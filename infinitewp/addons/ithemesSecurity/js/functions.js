/************************************************************
* InfiniteWP Admin panel - iThemes Security Plugin									*
* Copyright (c) 2014 Revmakx								*
* www.revmakx.com											*
*															*
************************************************************/
function showIthemesSecurityOptions() {
	var activeSites =$('.website_cont.active').length;
	if(activeSites>0){
		$('.load_itheames_security_result').removeClass('disabled');
	}else{
		$('.load_itheames_security_result').addClass('disabled');
	}	
}

function formArrayIthemesSecurityLoad(data) {
    formArrayVar[data.actionResult.actionID]={};
    formArrayVar[data.actionResult.actionID]['function']="ithemesSecurityLoad";
}

function ithemesSecurityLoad(data) {
        
	var content = data.data.ithemesSecurityLoad;
	var contentData = classifyIthemesSecurityResult(content);
        
	content = loadIthemesSecurityContentHTML(contentData);
	$("#pageContent").find('#ithemes_security_content').append(content).show();
	showIthemesSecurityOptions();
}

function ithemesSecurityPluginInitialization(data) {
	$("#historyQueue").show();
	var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['pluginInintializationReload']='iThemes Security';
	doCall(ajaxCallPath,tempArray);
}

function ithemesSecurityGetRecentPluginsStatusAndCheck(data){//temporary function which is similar to getRecentPluginsStatusAndCheck(data) in base apps.js
	if(typeof data != 'undefined' && typeof data.data.getRecentPluginsStatus != 'undefined')
		pluginsStatus = data.data.getRecentPluginsStatus; 
	ithemesSecurityHideUninstallandInactiveSites(pluginsStatus,currentPage);
	siteSelectorNanoReset();
}


function ithemesSecurityHideUninstallandInactiveSites(pluginsData,checkPlugin) {//temporary function which is similar to hideUninstallandInactiveSites(pluginsData,checkPlugin) in base apps.js, this func code will replace the base one soon
	data = {};
	plugin_main_files = {};
	
	if(checkPlugin == 'brokenLinks'){
		plugin_main_files[0] = 'broken-link-checker/broken-link-checker.php';
		pluginName = 'Broken Link Checker'
	}else if(checkPlugin == 'wordFence'){
		plugin_main_files[0] = 'wordfence/wordfence.php';
		pluginName = 'WordFence'
	}else if(checkPlugin == 'ithemesSecurity'){
		plugin_main_files[0] = 'better-wp-security/better-wp-security.php';
		plugin_main_files[1] = 'ithemes-security-pro/ithemes-security-pro.php';
		pluginName = 'iThemes Security'
	}else if(checkPlugin == 'yoastWpSeo'){
		plugin_main_files[0] = 'wordpress-seo-premium/wp-seo-premium.php';
		pluginName = 'WP SEO by Yoast'
	}
	
	$.each(plugin_main_files,function(indexDummy, plugin_main_file){
		$.each(pluginsData,function(siteID,pluginData){
			if(typeof data[siteID] == 'undefined' || data[siteID][0] == false){
				if(plugin_main_file in pluginData){
					if(typeof pluginData[plugin_main_file] != 'undefined' && pluginData[plugin_main_file] != null){
						data[siteID] = [ pluginData[plugin_main_file]['isInstalled'],pluginData[plugin_main_file]['isActivated'] ];
					}else{
						data[siteID] = [null,null];
					}
				}else{
					data[siteID] = [false,false];
				}
			}
		});
	});
		

	$(".website_cont").each(function(){
		var id = $(this).attr('sid');
		if(typeof data[id] != 'undefined' && data[id] != null && typeof data[id][0] != 'undefined' && data[id][1] != 'undefined' && data[id][0] != null && data[id][1] != null){
			if (data[id][0] && !(data[id][1]) ) {
				if(checkPlugin == 'yoastWpSeo'){
					if($(this).find('.tips').length == 0)	$(this).append('<div class="tips" >Please Activate Premium '+pluginName+'</div>');
				}else{
					if($(this).find('.tips').length == 0)	$(this).append('<div class="tips" code="1">Click to Activate '+pluginName+'</div>');
				}
				if(!($(this).hasClass('disabled')))		$(this).addClass('disabled');
			}
			else if (!data[id][0] && !(data[id][1]) ) {
				if(checkPlugin == 'yoastWpSeo'){
					if($(this).find('.tips').length == 0)	$(this).append('<div class="tips" >Please Install & Activate Premium '+pluginName+'</div>');
				}else{
					if($(this).find('.tips').length == 0)	$(this).append('<div class="tips"  code="0">Click to Install & Activate '+pluginName+'</div>');
				}
				if(!($(this).hasClass('disabled')))		$(this).addClass('disabled');
			}else if(data[id][0] && (data[id][1]) ){
				$(this).removeClass('disabled');
				$(this).find('.tips').remove();
			}
		}else{
			if($(this).find('.tips').length == 0)	$(this).append('<div class="tips">Not communicable. Please reload data.</div>');
			if(!($(this).hasClass('disabled')))		$(this).addClass('disabled');
		}
	});
}