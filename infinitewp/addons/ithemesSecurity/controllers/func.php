<?php 

function ithemesSecurityAddonMenus(&$menus){
	$menus['protect']['subMenus'][] = array('page' => 'ithemesSecurity', 'displayName' => 'iThemes Security');
}

function ithemesSecurityResponseProcessors(&$responseProcessor){
	// $responseProcessor[<<type>>][<<action>>] = <<initial_part_of_response_processor_function_in_func.php>>;
        $responseProcessor['ithemesSecurity']['load'] = 'ithemesSecurityLoad';

}

function ithemesSecurityTaskTitleTemplate(&$template){
	$template['ithemesSecurity']['load']['']	= "Loading security scan results from <#sitesCount#> site<#sitesCountPlural#>";
}

function ithemesSecurityLoad() {
	$actionID = Reg::get('currentRequest.actionID');
	$linksData = DB::getFields("?:temp_storage", "data", "type = 'ithemesSecurity' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'ithemesSecurity' AND paramID = '".$actionID."'");
		
	if(empty($linksData)){
		return array();
	}
	$refinedData = array();

	foreach($linksData as $data){
		$refinedData = array_merge_recursive($refinedData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($refinedData);
	foreach($refinedData as $siteID => $links){
		$siteData = DB::getRow("?:sites","URL","siteID=".$siteID."");
		$refinedData['siteURLs'][] = $siteData['URL'];
		$refinedData['siteIDs'][] = $siteID;
	}

        return $refinedData;
}


?>