<?php 
/************************************************************
* InfiniteWP Admin panel - iThemes Security Plugin					*
* Copyright (c) 2014 Revmakx								*
* www.revmakx.com											*
*															*
************************************************************/
class manageClientsIthemesSecurity{
        /*
	 * Kick start the Scan on iThemes
	 */
	public static function ithemesSecurityLoadProcessor($siteIDs, $params){
		$type = "ithemesSecurity";
		$action = "load";
		$requestAction = "ithemes_security_load";
		
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => 'ithemesSecurityLoad', 'detailedAction' => 'Fetch the results');
		$events=1;
		foreach ($siteIDs as $siteID) {
			$siteData = getSiteData(intval($siteID));
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['directExecute'] 	= false;
			$PRP['events'] 			= $events;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
			prepareRequestAndAddHistory($PRP);
		}
	}
	
	/*
	 * Response for iThemes Scan start from client side
	 */
	public static function ithemesSecurityLoadResponseProcessor($historyID, $responseData){
		responseDirectErrorHandler($historyID, $responseData);
		
		$response = array();
		if(isset($responseData['success'])){
			$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
			$actionID = $historyData['actionID'];
			$siteID = $historyData['siteID'];
			$response['_'.$siteID] = $responseData['success'];
			
			DB::insert("?:temp_storage", array('type' => 'ithemesSecurity', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
			DB::update("?:history_additional_data", "status='success'", "historyID=".$historyID."");
			return;
		}
	}
}

manageClients::addClass('manageClientsIthemesSecurity');
?>