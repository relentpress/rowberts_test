<?php

/*
slug: ithemesSecurity
version: 1.0.0
*/


class addonIthemesSecurity{
	
	private static $version = '1.0.0';

	public static function version(){
		return self::version;
	}

	public static function init(){
		require_once(APP_ROOT."/addons/ithemesSecurity/controllers/func.php");	
		require_once(APP_ROOT."/addons/ithemesSecurity/controllers/manageClientsIthemesSecurity.php");	
		panelRequestManager::addFunctions('ithemesSecurityLoad');
		regHooks('addonMenus','responseProcessors', 'taskTitleTemplate');
	}	

}
?>