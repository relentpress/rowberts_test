<link rel="stylesheet" href="addons/googleAnalytics/css/googleAnalytics.css?<?php echo $addon_version; ?>" type="text/css" media="all" />
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="addons/googleAnalytics/js/functions.js?<?php echo $addon_version; ?>" type="text/javascript"></script>
<script src="addons/googleAnalytics/js/init.js?<?php echo $addon_version; ?>" type="text/javascript"></script>
<script src="addons/googleAnalytics/js/load.js?<?php echo $addon_version; ?>" type="text/javascript"></script>
