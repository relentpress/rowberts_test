$(function () {

	googleSettings = '<li>Google</li>';
	googleAnalyticsAccess = '<div id="gAnalyticsAccessTab" class="tr"> <div class="padding" style="font-size:12px;"><div class="label" style="padding: 10px 0;">CONNECT YOUR GOOGLE ANALYTICS ACCOUNT<a href="http://infinitewp.com/knowledge-base/how-to-connect-your-google-apis-account/?utm_source=application&utm_medium=userapp&utm_campaign=kb" style="padding:0; float:right; display:inline; text-transform:none;" target="_blank">See instructions</a></div> <div class="tl no_text_transform" style="width:475px;"> <div><div class="form_label">Authorized Redirect URI</div><input type="text" class="selectOnText" style="width:466px;" value="'+systemURL+'addons/googleAnalytics/lib/googleAnalytics.php" readonly="true" /></div> <div class="clear-both"></div></div> <div class="clear-both"></div> <div class="btn_action float-right g_grant" ><a class="rep_sprite btn_blue" id="grant_revoke" href="'+systemURL+'addons/googleAnalytics/lib/googleAnalytics.php?action=auth" >Grant Access</a></div> <div class="btn_action float-right g_revoke" style="display:none" ><a class="rep_sprite btn_blue" id="grant_re" style="" href="'+systemURL+'addons/googleAnalytics/lib/googleAnalytics.php?action=revoke">Revoke Access</a></div> <div class="clear-both"></div> </div> </div>';
	// $('#googleTab').append(googleAnalyticsAccess);
	
	//loadGrantRevokeButton();
	tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['googleAnalyticsGrantAccess']=1;
	// tempArray['requiredData']['googleServicesGetAPIKeys']=1;							//available in base
	//tempArray['requiredData']['googleAnalyticsGetProfileData']['view']='week';
	doCall(ajaxCallPath,tempArray);
	loadGoogleContent();
	});

$(".selectTwoG").live('click',function() {
	var attr=$(this).attr("attr");
	if(!($(this).hasClass('active')))
	{  
	   $('.selectTwoG.active').each(function(){
		   $(this).removeClass('active');
	   });
       $(this).addClass('active');
	   loadGoogleVar(attr);
	}
	else
	{
		$(this).removeClass('active');
	}
});
$(".manage_month_view").live('click',function() {
	/*if(!$(this).hasClass('active'))
	{
	$('.analyticsContent').hide();
	$(this).addClass('active');
	if($('.manage_week_view').hasClass('active'))
	$('.manage_week_view').removeClass('active');
	}
	else
	{
		$(this).removeClass('active');
	}*/
	if($(".manage_week_view").hasClass('active'))
	 {
	 $(".manage_week_view").removeClass('active');
	 }
	$('.analyticsContent').show();
		var siteID=$('.single_website_cont.active').attr('sid');
	    var tempArray={};
		tempArray['requiredData']={};
		//tempArray['requiredData']['googleAnalyticsGetProfileID']=1;
		tempArray['requiredData']['googleAnalyticsGetProfileData']={};
		tempArray['requiredData']['googleAnalyticsGetProfileData']['siteID']=siteID;
		//activeGoogle=$('.activeGoogle.active').attr("attr");
		tempArray['requiredData']['googleAnalyticsGetProfileData']['view']='month';
		doCall(ajaxCallPath,tempArray,'loadAnalyticsData');
});

$(".manage_week_view").live('click',function() {
	/*if(!$(this).hasClass('active'))
	{
	//$('.analyticsContent').hide();
	$(this).addClass('active');
	if($('.manage_month_view').hasClass('active'))
	$('.manage_month_view').removeClass('active');
	}
	else
	{
		$(this).removeClass('active');
	}*/
	 if($(".manage_month_view").hasClass('active'))
	 {
	 $(".manage_month_view").removeClass('active');
	 }
	 $('.analyticsContent').show();
		var siteID=$('.single_website_cont.active').attr('sid');
	    var tempArray={};
		tempArray['requiredData']={};
		//tempArray['requiredData']['googleAnalyticsGetProfileID']=1;
		tempArray['requiredData']['googleAnalyticsGetProfileData']={};
		tempArray['requiredData']['googleAnalyticsGetProfileData']['siteID']=siteID;
		//activeGoogle=$('.activeGoogle.active').attr("attr");
		tempArray['requiredData']['googleAnalyticsGetProfileData']['view']='week';
	    doCall(ajaxCallPath,tempArray,'loadAnalyticsData');
	
});

$(".googleSites .single_website_cont").live('click',function() {
	$(".single_website_cont").removeClass('active');
		//$(this).addClass('active');
	if(!($(this).hasClass('active')))
	{
		if(!($(this).children('.tip').is(':visible')))
		{
			$(this).addClass('active');
			$(".googleBtnView").show();
			//$('.googleView').show();
			$('.googleView').hide();
			$('.analyticsContent').html('');
		}
		
	}
	
	return false;
});

$(".loadAnalyticsBtn").live('click',function() {
	
	//$('.googleView').hide();
	    if($('.manage_month_view').hasClass('active'))
		{
			$('.manage_month_view').removeClass('active');
			$('.manage_week_view').addClass('active');
		}
	    $(this).addClass('disabled');
	    $('.analyticsContent').show();
		var siteID=$('.single_website_cont.active').attr('sid');
	    var tempArray={};
		tempArray['requiredData']={};
		//tempArray['requiredData']['googleAnalyticsGetProfileID']=1;
		tempArray['requiredData']['googleAnalyticsGetProfileData']={};
		tempArray['requiredData']['googleAnalyticsGetProfileData']['siteID']=siteID;
		//activeGoogle=$('.activeGoogle.active').attr("attr");
		tempArray['requiredData']['googleAnalyticsGetProfileData']['view']='week';
		 doCall(ajaxCallPath,tempArray,'loadAnalyticsData');
		/*$('.googleView').show();
		$(".manage_week_view").addClass('active');*/
});

$(".weekView").live('click',function() {
	$('.googleView').show();
});

$(".activeGoogle").live('click',function() {
	$('.activeGoogle').each(function(){
		$(this).removeClass('active');
	});
	if($(this).hasClass('active'))
	{
		$(this).removeClass('active');
	}
	else
	{
		$(this).addClass('active');
	}
});


$(".googleSettingsClick").live('click',function() {
	openSettingsPage('Google');
    return false;
	
});

$('#grant_revoke').live('click',function(){
	var tempArray={};
			tempArray['action']='';
			tempArray['args']={};
			tempArray['args']['params']={};
			tempArray['requiredData']={};
			var paramsArray={};
			$("#clientCreds .formVal").each(function () {
				paramsArray[$(this).attr('id')]=$(this).val();
			});
			paramsArray['redirect']=1;
			//tempArray['requiredData']['googleAnalyticsSaveAPIKeys']=1;
			tempArray['requiredData']['googleServicesSaveAPIKeys']=paramsArray;
			grant_revoke_click = ".grant_revoke_click";
			// console.log(tempArray);
			doCall(ajaxCallPath,tempArray,'processGoogleForm');
});

$('.single_website_cont.hidden_gg').live('hover',function(e){
	if($(this).hasClass('disabled'))
	{
		$(this).removeClass('disabled');
		$(this).children('.tip').show();
		$(this).children('.tip').text('Click to assign Analytics profile');
	}
	else
	{
		$(this).addClass('disabled');
		$(this).children('.tip').hide();
	}
	
});
$('.single_website_cont.hidden_gg').live('click',function(){
	$(".googleBtnView").hide();
	$('.googleView').hide();
	$('.analyticsContent').html('');
});

$(".grant_revoke_click").live('click',function() {
$('#grant_revoke').click();
});
	
$('.googleSites .tip').live('click',function(e){
	//e.preventDefault();
	e.stopImmediatePropagation();
	var sid = $(this).closest('.single_website_cont').attr('sid');
	loadAddSite(sid);
	$('#gg').select2('open');
	if(typeof isGoogleWM != 'undefined')
	{
		if(isGoogleWM=='1')
		{
			$('.googleWMEditOptions').hide();
		}
	}
	$(".dialog_cont .title").text("EDIT SITE DETAILS");
	$(".dialog_cont .activationKeyDiv").hide();
	$(".dialog_cont .websiteURLCont").show();
	
	$(".dialog_cont .addSiteButton").text('Save Changes').addClass('editSite').attr('sid',$(this).closest('.single_website_cont').attr('sid'));
	$(".dialog_cont #gg").css("visibility", "visible");
	$(".dialog_cont #gg").css("top", "auto");
	$(".dialog_cont #websiteURL").val(site[$(this).closest('.single_website_cont').attr('sid')].URL);
	$(".dialog_cont #adminURL").val(site[$(this).closest('.single_website_cont').attr('sid')].adminURL);
	$(".dialog_cont #username").val(site[$(this).closest('.single_website_cont').attr('sid')].adminUsername).focus();
	$(".dialog_cont .connectURLClass").removeClass('active');
	$(".dialog_cont [def='"+site[$(this).closest('.single_website_cont').attr('sid')].connectURL+"']").addClass('active');
	$("#clientPluginDescription").hide();
	if(site[$(this).closest('.single_website_cont').attr('sid')].httpAuth!=undefined && site[$(this).closest('.single_website_cont').attr('sid')].httpAuth.username!=undefined)
	$(".dialog_cont #addSiteAuthUsername").val(site[$(this).closest('.single_website_cont').attr('sid')].httpAuth.username);
	if(site[$(this).closest('.single_website_cont').attr('sid')].httpAuth!=undefined && site[$(this).closest('.single_website_cont').attr('sid')].httpAuth.password!=undefined)
	$(".dialog_cont #addSiteAuthUserPassword").val(site[$(this).closest('.single_website_cont').attr('sid')].httpAuth.password);
	if(getPropertyCount(site[$(this).closest('.single_website_cont').attr('sid')].groupIDs)>0)
	{
	$.each(site[$(this).closest('.single_website_cont').attr('sid')].groupIDs, function(i, object) {
		$(".addSiteGroups .g"+object).addClass('active');
	});
	}
	if(site[$(this).closest('.single_website_cont').attr('sid')].callOpt!= undefined && (site[$(this).closest('.single_website_cont').attr('sid')].callOpt.contentType!=''))
	{
	var radionSiteID=site[$(this).closest('.single_website_cont').attr('sid')];
	
	$(".cTypeRadio").removeClass('active');
	var checkCustomType=0;
	$(".cTypeRadio").each (function () {
	if($(this).text()==radionSiteID.callOpt.contentType)
	{
	$(this).addClass('active');
	checkCustomType=1;
	}
	});
	if(checkCustomType==0)
	{
	$(".cTypeRadio .customTxtVal").val(site[$(this).closest('.single_website_cont').attr('sid')].callOpt.contentType);
	$(".cTypeRadio.customTxt").addClass('active');
	}
	}
});

