function loadGooglePage(data)
{   
	var content=''; content=content+singleSiteSelectorGoogle();
	content=content+'<div class="btn_action float-right googleBtnView"><a class="rep_sprite loadAnalyticsBtn" style="margin: 8px 30px 8px 0px; width: 79px;">Load Analytics</a></div><div class="clear-both"></div>';
	content=content+'<div class="result_block shadow_stroke_box bulk_publish_page_post googleView"><div class="th rep_sprite"> <ul class="btn_radio_slelect float-left" style="margin-left: 7px;"> <li><a class="rep_sprite manage_week_view active activeGoogle" attr="week">Weekly</a></li> <li><a class="rep_sprite manage_month_view  activeGoogle" attr="month">Monthly</a></li> </ul> </div>';
	content=content+'<div class="analyticsContent"></div></div>';
	$("#pageContent").html(content);
	if(grantRevokeData == 'grant')
	{
		$('.singleSiteGoogle').show();
		siteSelectorNanoSingleGoogle();
	}
	else
	{
		$('#pageContent').prepend('<div class="empty_data_set"><div class="line2">You have not connected your Google Analytics account to the admin panel. Go to <a class="googleSettingsClick" id="">Settings</a> to do it now.</div></div>');
		$('.singleSiteGoogle').hide();
	}
	hideInactiveGoogleSites(data.data.googleAnalyticsActiveSites);
	$(".googleBtnView").hide();
	$(".googleView").hide();
	loadGoogleContent();
	//siteSelectorNanoReset();
	//siteSelectorNanoSingleGoogle();
}
function loadAnalyticsData(data)
{    
	activeGoogle=$('.activeGoogle.active').attr("attr");	
	if(typeof activeGoogle=='undefined')	
	{
		activeGoogle = 'week';
	}
	var gData=data.data.googleAnalyticsGetProfileData;
	if(gData != null){
		if(activeGoogle=='week')
		{
			thisMonthData=data.data.googleAnalyticsGetProfileData.currentWeek;
			prevMonthData=data.data.googleAnalyticsGetProfileData.previousWeek;
		}
		else if(activeGoogle=='month')
		{
			thisMonthData=data.data.googleAnalyticsGetProfileData.currentMonth;
			prevMonthData=data.data.googleAnalyticsGetProfileData.previousMonth; 
		}
		content='';
		content=content+'<div id="chart_div" style="min-width: 400px; height: 400px; margin: 0 auto"></div>';
		content=content+'<div class="ga_cont selectTwoG  active" attr="ga:visits"> <div class="label">VISITS</div><div class="value">'+gData.visitsAvg+'</div><div class="change good">'+gData.visitsPercent+'%</div></div> <div class="ga_cont selectTwoG "attr="ga:visitors"> <div class="label">UNIQUE VISITORS</div> <div class="value">'+gData.uniqueVisitorAvg+'</div><div class="change good">'+gData.uniqueVisitorPercent+'%</div></div><div class="ga_cont selectTwoG " attr="ga:pageviews"> <div class="label">PAGEVIEWS</div><div class="value">'+gData.pageViewsAvg+'</div><div class="change good">'+gData.pageViewsPercent+'%</div> </div> <div class="ga_cont selectTwoG " attr="ga:pageviewsPerVisit"><div class="label">PAGES/VISIT</div><div class="value">'+gData.pageVisitsAvg+'</div><div class="change good">'+gData.pageVisitsAvgPercent+'%</div></div> <div class="ga_cont selectTwoG " attr="ga:avgTimeOnSite"><div class="label">AVG. VISIT DURATION</div><div class="value">'+gData.visitDurationAvg+'</div><div class="change good">'+gData.visitDurationPercent+'%</div> </div> <div class="ga_cont selectTwoG " attr="ga:visitBounceRate" style="margin-left:310px;"><div class="label">BOUNCE RATE</div><div class="value">'+gData.bounceRateAvg+'</div> <div class="change good bounce">'+gData.bounceRatePercent+'%</div> </div><div class="ga_cont selectTwoG " attr="ga:newVisits"><div class="label">% NEW VISITS</div><div class="value">'+gData.newVisitsAvg+'</div><div class="change good">'+gData.newVisitsPercent+'%</div></div><div class="clear-both"></div> </div>';

		/*	content=content+'<div class="btn_action dbActionBtn googleAnalytics"><a class="rep_sprite btn_blue" style="margin: 20px 55px;" href="'+systemURL+'addons/googleAnalytics/lib/googleAnalytics.php?action=auth" target="_blank" id="dbConnected">Grant Access</a></div>';*/
		$('.googleView').show();
		$(".analyticsContent").html(content).show();
		loadBadClass();
		loadGoogleVar("ga:visits");

		if($(".activeGoogle").hasClass('active'))
		{

		}
		else
		{
			$(".manage_week_view").addClass('active');
		}
		$('.loadAnalyticsBtn').removeClass('disabled');

	}else{
		$('.loadAnalyticsBtn').removeClass('disabled');
	}

}

function loadGoogleContent(data)
{
	var tempArray={};
	tempArray['requiredData']={};
	
	tempArray['requiredData']['googleAnalyticsEditSiteOptions']=1;
	tempArray['requiredData']['googleAnalyticsGrantAccess']=1;
	tempArray['requiredData']['googleAnalyticsMappedSites']=1;
	
	doCall(ajaxCallPath,tempArray,'storeGoogleData');
}


	function drawVisualization(testVar,header,attrx) {
		if(header == 'week')
		displayHAxis = 'Week';
		else if(header == 'month')
		displayHAxis = 'Month';
       data=testVar;
        // Create and draw the visualization.
        var ac = new google.visualization.ComboChart(document.getElementById('chart_div'));
        ac.draw(data, {
          title : attrx+' by '+header,
          //isStacked: true,
          //width: 600,
          height: 400,
          vAxis: {title: attrx, titleTextStyle: {italic: false}},
          hAxis: {title: displayHAxis, titleTextStyle: {italic: false}},
		  fontName: '"Droid Sans",sans-serif',
		  seriesType: "area",
          series: {1: {type: "line"}},
        });
		}
function loadGoogleVar(attr){
	
	//var attr= $(".selectTwoG.active").attr("attr");
  var data = new google.visualization.DataTable(),attrx,attry,header,thisDate,PrevDate,mainDate,mainPrevDate,dateNum;
  header=$('.activeGoogle.active').attr('attr');
  if(attr=="ga:newVisits")
  attrx = " % New Visits";
   if(attr=="ga:visits")
  attrx = " Visits";
    if(attr=="ga:visitors")
  attrx = " Unique Visitors";
  if(attr=="ga:pageviews")
  attrx = " Pageviews";
    if(attr=="ga:pageviewsPerVisit")
  attrx = " Pages/Visit";
  if(attr=="ga:visitBounceRate")
  attrx = " Bounce Rate";
  if(attr=="ga:avgTimeOnSite")
  attrx = " Avg.Visit Duration";
  
var testx="this "+header;
var testy="prev "+header;
var xData={};
var yData={};
var num;
$.each(thisMonthData[0],function(k,v){
	if(attr==v)
	{
	num=k;
	}
});
data.addColumn('number', 'dates');
data.addColumn('number', testx);
data.addColumn({type: 'string', role: 'tooltip'});
data.addColumn('number', testy);
data.addColumn({type: 'string', role: 'tooltip'});
$.each(thisMonthData,function(k,v){	
if(k>0)
{	
data.addRows([ [k, parseInt(v[num]),v[0]+' -\n '+attrx+':'+parseFloat(v[num]).toFixed(2),parseInt(prevMonthData[k][num]),prevMonthData[k][0]+' -\n '+attrx+':'+parseFloat(prevMonthData[k][num]).toFixed(2) ]]);
}

});
      //drawVisualization(dt);
	  drawVisualization(data,header,attrx);

	
}

function loadBadClass(){
	$(".change").each(function(){
	 var test=$(this).text(),test2;
	 test2=parseInt(test);
	 if(test2<0)
	 {
		 $(this).removeClass('good');
		 $(this).addClass('bad');
	 }
	 
 });
 if($('.change.bounce').hasClass('bad'))
	 {
		 $('.change.bounce').removeClass('bad');
			 $('.change.bounce').addClass('good');
	 }
	 else    // if($('.change.bounce').hasClass('good'))
	 {
		 $('.change.bounce').removeClass('good');
			 $('.change.bounce').addClass('bad');
	 }
	
}

function hideInactiveGoogleSites(data){
	
	$(".single_website_cont").each(function(){
			   $(this).addClass('disabled');
			   $(this).addClass('hidden_gg');
		  });
	 $.each(data,function(k,v){
		  var isMatch=0;
		  
	  $(".single_website_cont").each(function(){
             if($(this).attr('sid')==v)
			  {
				isMatch=isMatch+2;
				  if(isMatch>0)
				  {
					  $(this).removeClass('disabled');
				      $(this).removeClass('hidden_gg');
				  }
			  }
	     });
	 });
}

function storeGoogleData(data){
	gData = data.data.googleAnalyticsEditSiteOptions;
	grantRevokeData=data.data.googleAnalyticsGrantAccess;
	gSites = data.data.googleAnalyticsMappedSites;
	// if(grantRevokeData == 'grant')
	// {
	// 	$('.g_grant').hide();
	// 	$('.g_revoke').show();
	// }
	// else
	// {
	// 	$('.g_grant').show();
	// 	$('.g_revoke').hide();
		
	// }
	
	
}

function singleSiteSelectorGoogle()
{
	var sContent ='';
	if(sitesList!=null && sitesList!=undefined &&  getPropertyCount(sitesList)>0)
	 {
	$.each(sitesList, function(key,value) {
	
	sContent=sContent+'<div class="single_website_cont searchable "  id="s'+value.siteID+'" sid="'+value.siteID+'" onclick=""><a title="'+value.name+'">'+value.name+'</a><div class="tip">Assign a Google Analytics profile</div></div>';
		
		});
		 sContent= '<div class="single_site_selector shadow_stroke_box siteSearch singleSiteGoogle"><div class="th rep_sprite"><input name="" value="type to filter" type="text" class="input_type_filter search_site googleSearchFilter" style="color: rgb(170, 170, 170); margin: 8px;"></div><div class="single_website_items_cont googleSites"><div class="content">'+sContent+'<div class="no_match hiddenCont" style="display:none">Bummer, there are no websites that match.<br />Try typing lesser characters.</div> <div class="clear-both"></div></div> <div class="clear-both"></div></div></div>';
	 }
	 else
	 {
	 sContent='<div class="single_website_items_cont"><div class="no_match hiddenCont">No websites added yet.</div> <div class="clear-both"></div> <div class="clear-both"></div></div>';
	 }
	
	 return sContent;
}
function siteSelectorNanoSingleGoogle()
{
	/*if ( $.browser.msie && $.browser.version=='8.0') {
		$(".site_selector1 .bywebsites .website_items_cont .website_cont:nth-child(3n+3)").css({"width":"235px", "border-right":"0"});
	}*/
	//$(".siteSelectorContainer .single_website_items_cont").addClass('nano');
	$(".single_website_items_cont.nano").nanoScroller({stop: true});
	//$(".siteSelectorContainer .group_items_cont").css('height',$(".siteSelectorContainer .group_items_cont").height()).addClass('nano');
	$(".single_website_items_cont").css('height',$(".single_website_items_cont").height()).addClass('nano');
	$(".single_website_items_cont.nano").nanoScroller();	
}
// function loadGrantRevokeButton(data)								//available in base
// {
// 	$('.grantVal').val(data);
// 	var grantMe = data.data.googleServicesGetAPIKeys ;
// 	if((grantMe != null)||(grantMe != undefined))
// 	{
// 	$('#clientID').val(grantMe.clientID);
// 	$('#clientSecretKey').val(grantMe.clientSecretKey);
// 	}
// }

function processGoogleForm(data)
{
    var mainData=data;
	//data=data.data.updateAccountSettings;
	//$('#save_before_grant').click();
	$("#saveSettingsBtn").removeClass('disabled');
	$(".settings_cont .btn_loadingDiv").remove();
	if(data.data.googleServicesSaveAPIKeys == true)
	{
		$("#saveSuccess").show();
		setTimeout(function () {	/*$("#settings_cont").hide();*/ $("#saveSuccess").hide();},1000);
		$("#settings_btn").removeClass('active');
		window.location.href = ''+systemURL+'addons/googleAnalytics/lib/googleAnalytics.php?action=auth';
	}
	
}
