<?php

/*
slug: googleAnalytics
version: 1.0.15
*/

class addonGoogleAnalytics{
	
	private static $version = '1.0.15';

	public static function version(){
		return self::version;	
	}
	
	public static function init(){
		require_once(APP_ROOT."/addons/googleAnalytics/controllers/func.php");	
		panelRequestManager::addFunctions('googleAnalyticsGetProfileData', 'googleAnalyticsFetchProfiles', 'googleAnalyticsEditSiteOptions', 'googleAnalyticsInstallPopulateSites','googleAnalyticsSetProfilesAndSites','googleAnalyticsActiveSites', 'googleAnalyticsGrantAccess', 'googleAnalyticsSaveAPIKeys', 'googleAnalyticsSaveAPIKeysAndRedirect', 'googleAnalyticsGetAPIKeys','googleAnalyticsMappedSites');
		regHooks('addonMenus', 'responseProcessors', 'taskTitleTemplate','postAddSite', 'runOffBrowserLoad');
	}	
	
	public static function install(){
		
	
		$Q1 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:google_analytics_profiles` (
	  `userID` int(10) unsigned DEFAULT NULL,
	  `gaUsername` varchar(100)  DEFAULT NULL,
	  `gaAccName` text NOT NULL,
	  `gaSiteURL` varchar(255) NOT NULL,
	  `gaProfileName` varchar(255) NOT NULL,
	  `gaProfileID` int(10) unsigned DEFAULT NULL,
	  `gaParams` text NOT NULL,
	  `gaDefault` enum('0','1') NOT NULL DEFAULT '0',
	  UNIQUE KEY `ga_profileID` (`gaProfileID`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1");
	
	
	$Q2 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:google_analytics_profiles_sites` (
	  `siteID` int(10) unsigned DEFAULT NULL,
	  `gaProfileID` varchar(15) DEFAULT NULL,
	  UNIQUE KEY `siteID` (`siteID`,`gaProfileID`),
	  UNIQUE KEY `siteID_2` (`siteID`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
	
	if($Q1 && $Q2){
			return true;	
		}
		return false;
	}

	public static function update(){
		
		if(version_compare(getOldAddonVersion('googleAnalytics'), '1.0.8') === -1){
			//from v1.0.8 Google API Lib is kept common in base app, so deleting common API files here
			
			if(!is_object($GLOBALS['FileSystemObj'])){
				if(!initFileSystem()){
					appUpdateMsg('Unable to initiate file system.', true);
					exit;
				}
			}
			
			$panelLocation = $GLOBALS['FileSystemObj']->findFolder(APP_ROOT);
			$panelLocation  = removeTrailingSlash($panelLocation);
			$GLOBALS['FileSystemObj']->delete($panelLocation.'/addons/googleAnalytics/lib/authHelper.php', false, 'f');
			$GLOBALS['FileSystemObj']->delete($panelLocation.'/addons/googleAnalytics/lib/storage.php', false, 'f');
			$GLOBALS['FileSystemObj']->delete($panelLocation.'/addons/googleAnalytics/lib/src', true);
		}
		return true;
	}
}

?>