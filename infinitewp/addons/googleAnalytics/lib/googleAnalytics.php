<?php

	include '../../../includes/app.php';
	try{
		//Main controller logic.

		googleAnalyticsHelper::setPathsAndKeys();

		if ($_GET['action'] == 'revoke') {
			googleAnalyticsHelper::$authHelper->revokeToken();
			 //DB::delete("?:google_analytics_auth", "1");
			deleteOption('googleAnalyticsAccessToken');
			DB::delete("?:google_analytics_profiles", "1");
			DB::delete("?:google_analytics_profiles_sites", "1");
			updateOption('gaMapProfilesAndSitesInitialRun', "N");
			 
			updateOption('googleAnalyticsAccessStatus', 'revoke');
			 
			addNotification($type='N', $title='Google Analytics Revoked', $message="Your Google Analytics account has been revoked.", $state='U', $callbackOnClose='', $callbackReference='');
		  
		} else if ($_GET['action'] == 'auth' || $_GET['code']) {
		  googleAnalyticsHelper::$authHelper->authenticate();
		  
		  	if($_GET['code']){
			  sleep(1);
			  $getAccessToken = googleAnalyticsHelper::$storage->get($accessToken);
			  //DB::replace("?:google_analytics_auth", array('ID' => 1, 'userID' => $_SESSION['userID'], 'auth_data' => serialize($getAccessToken))); //using ID in insert will restrict to 1, because ID is primary
			  updateOption('googleAnalyticsAccessToken', serialize($getAccessToken));
		  	}
		}
		else {
			if(googleAnalyticsHelper::$storage->get($accessToken) ){ 
				googleAnalyticsHelper::$authHelper->setTokenFromStorage();
				 
				googleAnalyticsInstallPopulateSites();
				 
				if(googleAnalyticsFetchProfiles()){
					 
					addNotification($type='N', $title='Google Analytics Connected', $message="Your Google Analytics account has been connected.", $state='U', $callbackOnClose='', $callbackReference='');
					 
					updateOption('googleAnalyticsAccessStatus', 'grant');
					 
					header("Location:".APP_URL); 
				}
				else{
					echo 'some error here';//CLEARIT
				}
			}else{
				header("Location:".APP_URL); 
			}
			 

		}

	}catch(Google_ServiceException $e){
		$cd = $e->getCode();
		$errors = $e->getErrors();
		addNotification($type='E', $title='Google Analytics Service Exception (Error#'.$cd.')', $message=$errors[0]['message'], $state='U', $callbackOnClose='', $callbackReference='');
		header("Location:".APP_URL); 
	}
	catch(Google_Exception $e){
		$cd = $e->getCode();
		$msg = $e->getMessage();
		addNotification($type='E', $title='Google Analytics Exception (Error#'.$cd.')', $message=$msg, $state='U', $callbackOnClose='', $callbackReference='');
		header("Location:".APP_URL); 
	}
		

?>