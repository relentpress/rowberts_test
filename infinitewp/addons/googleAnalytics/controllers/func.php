<?php

function googleAnalyticsAddonMenus(&$menus){
	//$menus[] = '<li><a class="navLinks optionSelect" page="googleAnalytics">Google Analytics</a></li>';
	$menus['monitor']['subMenus'][] = array('page' => 'googleAnalytics', 'displayName' => 'Google Analytics');
}

function googleAnalyticsFetchProfiles(){
	try{
		googleAnalyticsHelper::setPathsAndKeys();
		if(!googleAnalyticsHelper::authAndSetToken()){ return false; }
		$analytics = new Google_AnalyticsService(googleAnalyticsHelper::$client);
			
		$accounts = $analytics->management_accounts->listManagementAccounts();
		
		$getAccounts = googleAnalyticsHelper::getAccounts($accounts); //&$accounts

		$freshProfilesInsert = array();	
		

		if(!empty($getAccounts)){
			foreach($getAccounts as $getaccount){
				
				$webproperties = $analytics->management_webproperties->listManagementWebproperties($getaccount['accID']);
				
				foreach($webproperties->items as $item){
					sleep(1);
					$profiles = $analytics->management_profiles->listManagementProfiles($item->accountId, $item->id);
					
					if (count($profiles->getItems()) > 0) {
					  $items = $profiles->getItems();
					  $firstProfileId = $items[0]->getId();
					} 
					$userID = (isset($GLOBALS['userID']) && !empty($GLOBALS['userID']))?$GLOBALS['userID']:$_SESSION['userID'];
					if(!empty($profiles->items) && is_array($profiles->items)){
						foreach($profiles->items as $profile){
							$freshProfilesInsert[] = array('userID' => $userID, 'gaUsername' => $profiles->username, 'gaAccName' => $getaccount['accName'], 'gaSiteURL' => $item->websiteUrl, 'gaProfileName' => $profile->name, 'gaProfileID' => $profile->id, 'gaParams' => serialize($profile), 'gaDefault' => (($firstProfileId == $profile->id) ? 1 : 0));
						}
					}
				}
			}
		}
	}catch(Google_ServiceException $e){
		$cd = $e->getCode();
		$errors = $e->getErrors();
		addNotification($type='E', $title='Google Analytics Service Exception (Error#'.$cd.')', $message=$errors[0]['message'], $state='U', $callbackOnClose='', $callbackReference='');
	}
	catch(Google_Exception $e){
		$cd = $e->getCode();
		$msg = $e->getMessage();
		addNotification($type='E', $title='Google Analytics Exception (Error#'.$cd.')', $message=$msg, $state='U', $callbackOnClose='', $callbackReference='');
	}
	
	DB::delete("?:google_analytics_profiles", "1"); //delete all rows
	if(!empty($freshProfilesInsert)){
		foreach($freshProfilesInsert as $freshProfile){
			DB::insert("?:google_analytics_profiles", $freshProfile);
		}
	}
	
	updateOption('gaLastProfilesFetch', time());
	
	$run = getOption('gaMapProfilesAndSitesInitialRun');
	if(empty($run) || $run == 'N'){
		googleAnalyticsPostAddSite();
		updateOption('gaMapProfilesAndSitesInitialRun', 'Y');
	}
	return true;
}

function googleAnalyticsGrantAccess(){
	$result = getOption('googleAnalyticsAccessStatus');
	if(!empty($result)){
		return $result;
	}
	return false;
}

function googleAnalyticsPostAddSite($siteID=false){
	
	googleAnalyticsInstallPopulateSites();
	
	$successMaps = 0;
	$whereJoin = '';
	if(!empty($siteID)){
		$whereJoin = " AND S.siteID = '".$siteID."'";
	}
	
	$sites = DB::getFields("?:sites S, ?:google_analytics_profiles_sites GAPS", "S.URL, S.siteID", "S.siteID = GAPS.siteID AND (GAPS.gaProfileID IS NULL OR GAPS.gaProfileID = '')".$whereJoin, "siteID");
	if(!empty($sites)){
		foreach($sites as $siteID => $siteAddress){
			$siteAddress = str_ireplace(array('http://', 'https://'), '', $siteAddress);
			$siteAddress = rtrim($siteAddress, '/');
			$gaProfileID = DB::getField("?:google_analytics_profiles", "gaProfileID", "gaProfileID NOT IN(SELECT gaProfileID FROM ?:google_analytics_profiles_sites WHERE (gaProfileID IS NULL AND gaProfileID = '')) AND gaSiteURL LIKE '%".$siteAddress."%'");
			if(!empty($gaProfileID)){
				DB::update("?:google_analytics_profiles_sites", array('gaProfileID' => $gaProfileID), "siteID = ".$siteID);
				$successMaps++;
			}
		}
	}
	if($successMaps > 0){
		addNotification($type='N', $title='Google Analytics site'.(($successMaps > 1) ? 's' : '').' mapped', $message=$successMaps.' site'.(($successMaps > 1) ? 's' : '').' mapped with google analytics.', $state='U', $callbackOnClose='', $callbackReference='');
	}
}

//active analytic profile
function googleAnalyticsActiveSites(){
	return DB::getFields("?:google_analytics_profiles_sites", "siteID", "gaProfileID IS NOT NULL");
}

//Mapped site and profileID analytic profile
function googleAnalyticsMappedSites(){
	$mappedSites = DB::getArray("?:google_analytics_profiles_sites", "siteID,gaProfileID", "gaProfileID IS NOT NULL");
	foreach($mappedSites as $key => $value){
		$option[$value['siteID']] = array('gaProfileID' => $value['gaProfileID']); 
	}
	return $option;
}

//Update 'site' with 'profileID' in 'ga_profiles_sites' table.
function googleAnalyticsSetProfilesAndSites($params){
		if(empty($params['googleProfileID'])) $params['googleProfileID'] = array('NULL');
		DB::update("?:google_analytics_profiles_sites", array('gaProfileID' => $params['googleProfileID']), "siteID = ".$params['siteID']);
}

function googleAnalyticsInstallPopulateSites(){
	
	$siteIDs = DB::getFields("?:sites", "siteID", "1");
	if(!empty($siteIDs)){
		foreach($siteIDs as $siteID){
			if(!DB::getExists("?:google_analytics_profiles_sites", "siteID", "siteID = ".$siteID)){
				DB::insert("?:google_analytics_profiles_sites", array('siteID' => $siteID));
			}
		}
	}
}

function googleAnalyticsEditSiteOptions(){
	
	$gaDatas = DB::getArray("?:google_analytics_profiles", "gaSiteURL, gaProfileName, gaProfileID", "1");
	if(!empty($gaDatas)){
		foreach($gaDatas as $key => $gaData ){
			$gaSiteURL = str_replace(array('http://', 'https://'), '', $gaData["gaSiteURL"]);
			$gaSiteURL = $gaSiteURL.(substr($gaSiteURL, -1) == '/' ? '' : '');
			$googleAnalyticsProfileData[] = array("gaSiteURL" => $gaSiteURL, "gaProfileName" => $gaData["gaProfileName"], "gaProfileID" => $gaData["gaProfileID"]);
		}
	}
	
	return $googleAnalyticsProfileData;
}

function googleAnalyticsGetProfileData($params){ //attribute $profileID, $view (month or week)
	
	try{
		googleAnalyticsHelper::setPathsAndKeys();
		if(!googleAnalyticsHelper::authAndSetToken()){ return false; }
			
		$profileID = DB::getField("?:google_analytics_profiles_sites", "gaProfileID", "siteID = '".$params['siteID']."'");
		
		//For client Report
		
		if(isset($params['clientReportAnalyticsDate']) && !empty($params['clientReportAnalyticsDate'])){
			$previousPeriod['startDate'] = $params['clientReportAnalyticsDate']['startDate'];
			$previousPeriod['endDate'] = $params['clientReportAnalyticsDate']['endDate'];
			
			$periods =  array($previous => $previousPeriod);
		}//
		else{
			if($params['view'] == 'month'){
				$periodDays = 30;
				$current = 'currentMonth';
				$previous = 'previousMonth';
			}
			else{ //week
				$periodDays = 7;
				$current = 'currentWeek';
				$previous = 'previousWeek';
			}
			
			$previousPeriod['startDate']	= date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-($periodDays*2)+1, date("Y")));
			$previousPeriod['endDate'] 		= date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-($periodDays*1), date("Y")));
			$thisPeriod['startDate'] 		= date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-($periodDays*1)+1, date("Y")));
			$thisPeriod['endDate'] 			= date('Y-m-d', mktime(0, 0, 0, date("m"), date("d"), date("Y")));
			
			$periods =  array($previous => $previousPeriod, $current => $thisPeriod);
		}
		
		$analytics = new Google_AnalyticsService(googleAnalyticsHelper::$client);
		    	
		foreach($periods as $period => $dates){
			$startDate = $dates['startDate'];
			$endDate = $dates['endDate'];
			
			$results = googleAnalyticsHelper::queryCoreReportingApi($analytics, 'ga:'.$profileID, $startDate, $endDate);
			
			//For client Report 
			if(isset($params['clientReportAnalyticsDate']) && !empty($params['clientReportAnalyticsDate'])){
				$totalsForAllResults = $results->totalsForAllResults;
				return $totalsForAllResults;
			}
			// 
			
			$totalsForAllResults[$period] = $results->totalsForAllResults;
			$columnHeader = $results->columnHeaders;
			
			$test = array();
			$gaData[$period] = array();
			
			foreach($columnHeader as $header){
				$test[] = $header->name;
			}
			
			$gaData[$period][] = $test;
			foreach($results->rows as $row){			
				$row[0] = date('M d, Y', strtotime($row[0]));
				array_push($gaData[$period], $row);
			}
		}
		 	 
		$gaData['visitsAvg'] = $totalsForAllResults[$current]['ga:visits'];
		$gaData['visitsPercent'] = differencePercent($totalsForAllResults[$current]['ga:visits'], $totalsForAllResults[$previous]['ga:visits']);
		 
		$gaData['uniqueVisitorAvg'] = $totalsForAllResults[$current]['ga:visitors'];
		$gaData['uniqueVisitorPercent'] = differencePercent($totalsForAllResults[$current]['ga:visitors'], $totalsForAllResults[$previous]['ga:visitors']);
		 
		$gaData['pageViewsAvg'] = round($totalsForAllResults[$current]['ga:pageviews'], 2);
		$gaData['pageViewsPercent'] = differencePercent($totalsForAllResults[$current]['ga:pageviews'], $totalsForAllResults[$previous]['ga:pageviews']);
		 
		$gaData['pageVisitsAvg'] = round($totalsForAllResults[$current]['ga:pageviewsPerVisit'], 2);
		$gaData['pageVisitsAvgPercent'] = differencePercent($totalsForAllResults[$current]['ga:pageviewsPerVisit'], $totalsForAllResults[$previous]['ga:pageviewsPerVisit']);
		 
		$gaData['visitDurationAvg'] = round($totalsForAllResults[$current]['ga:avgTimeOnSite'], 2);
		$gaData['visitDurationPercent'] = differencePercent($totalsForAllResults[$current]['ga:avgTimeOnSite'], $totalsForAllResults[$previous]['ga:avgTimeOnSite']);
		 
		$gaData['bounceRateAvg'] = round($totalsForAllResults[$current]['ga:visitBounceRate'], 2);
		$gaData['bounceRatePercent'] = differencePercent($totalsForAllResults[$current]['ga:visitBounceRate'], $totalsForAllResults[$previous]['ga:visitBounceRate']);
		 
		$gaData['newVisitsAvg'] = round($totalsForAllResults[$current]['ga:percentNewVisits'], 2);
		$gaData['newVisitsPercent'] = differencePercent($totalsForAllResults[$current]['ga:percentNewVisits'], $totalsForAllResults[$previous]['ga:percentNewVisits']);
		 
		return $gaData;
	}catch(Google_ServiceException $e){
		$cd = $e->getCode();
		$errors = $e->getErrors();
		addNotification($type='E', $title='Google Analytics Service Exception (Error#'.$cd.')', $message=$errors[0]['message'], $state='U', $callbackOnClose='', $callbackReference='');
	}
	catch(Google_Exception $e){
		$cd = $e->getCode();
		$msg = $e->getMessage();
		addNotification($type='E', $title='Google Analytics Exception (Error#'.$cd.')', $message=$msg, $state='U', $callbackOnClose='', $callbackReference='');
	}
}


function differencePercent($current, $previous){
	$previous = (float)$previous;
	if($previous == 0){//fix me
		return round(-100, 2);
	}
	return @round(((($current / $previous) - 1) * 100), 2);
}

function googleAnalyticsRunOffBrowserLoad(){
	$userID = (isset($GLOBALS['userID']) && !empty($GLOBALS['userID']))?$GLOBALS['userID']:$_SESSION['userID'];
	if(empty($userID)){//fix for RunOffBrowserLoad in fsock mode
		$userID = DB::getField("?:users", "userID", "1 ORDER BY userID ASC LIMIT 1");
		if(version_compare(APP_VERSION, '2.4.0beta1') == -1){
			$_SESSION['userID'] = $userID;			
		} else {
			$GLOBALS['userID'] = $userID;			
		}
	}

	$googleAPIKeys = getOption('googleAPIKeys');
	$result = getOption('googleAnalyticsAccessStatus');
	if(!empty($googleAPIKeys)){
		$APIKeys = unserialize($googleAPIKeys);
	}
	if(empty($APIKeys['clientID']) || empty($APIKeys['clientSecretKey']) || $result != 'grant'){
		return false;
	}
	googleAnalyticsFetchProfiles();
}

class googleAnalyticsHelper{
	
	public static $client;
	public static $authHelper;
	public static $storage;
	private static $GAThisPage;
	private static $redirectURL;
	private static $isSetPathsAndKeysComplete = false;
	
	public static function setPathsAndKeys(){

		
		if(self::$isSetPathsAndKeysComplete) return true;
		
		include_once(APP_ROOT.'/lib/googleAPIs/src/Google_Client.php');
		include_once(APP_ROOT.'/lib/googleAPIs/src/contrib/Google_AnalyticsService.php');
		include_once(APP_ROOT.'/lib/googleAPIs/storage.php');
		include_once(APP_ROOT.'/lib/googleAPIs/authHelper.php');
		
		self::$GAThisPage = APP_URL.'addons/googleAnalytics/lib/googleAnalytics.php';
		self::$redirectURL = APP_URL.'addons/googleAnalytics/lib/googleAnalytics.php';
		

		$googleAnalyticsAPIKeys = getOption('googleAPIKeys');
		if(!empty($googleAnalyticsAPIKeys)){
			$googleAnalyticsAPIKeys = unserialize($googleAnalyticsAPIKeys);
		}
	
		// Build a new client object to work with authorization.
		self::$client = new Google_Client();
		self::$client->setClientId($googleAnalyticsAPIKeys['clientID']);
		self::$client->setClientSecret($googleAnalyticsAPIKeys['clientSecretKey']);
		self::$client->setRedirectUri(self::$redirectURL);
		self::$client->setApplicationName('InfiniteWP Google Analytics');
		self::$client->setScopes(array('https://www.googleapis.com/auth/analytics.readonly'));
		
		// Magic. Returns objects from the Analytics Service
		// instead of associative arrays.
		self::$client->setUseObjects(true);

		
		// Build a new storage object to handle and store tokens in sessions.
		// Create a new storage object to persist the tokens across sessions.
		self::$storage = new apiSessionStorage(); //googleAnalyticsStorageHelper();
		
		self::$authHelper = new AuthHelper(self::$client, self::$storage, self::$GAThisPage);
		
		self::$isSetPathsAndKeysComplete = true;


	}
	
	public static function authAndSetToken(){
		
		$getAccessToken = getOption('googleAnalyticsAccessToken');
		$accessToken = unserialize($getAccessToken);
		
		if (isset($accessToken)) {
		  self::$client->setAccessToken($accessToken);
		}
			
		return self::$authHelper->isAuthorized();
	}
	
	public static function getAccounts(&$accounts) {
		 $items = $accounts->getItems();
		 
		 $accDetail = array();
		if (count($items) > 0){
		  foreach($items as &$account) {
			  $accDetail[] = array('accID' => $account->getId(), 'kind' => $account->getKind(), 'selfLink' => $account->getSelfLink(), 'accName' => $account->getName(), 'created' => $account->getCreated(), 'updated' => $account->getUpdated());
		  }
		}
		return $accDetail;
  	}
	
	  public static function queryCoreReportingApi($analytics, $tableId,$startDate,$endDate) {
		  
		  $optParams = array('dimensions' => 'ga:date');
		  
		  return $analytics->data_ga->get(
			urldecode($tableId),
			$startDate,
			$endDate,
			'ga:newVisits,ga:visits,ga:visitors,ga:percentNewVisits,ga:pageviews,ga:pageviewsPerVisit,ga:visitBounceRate,ga:avgTimeOnSite',
			$optParams);
	  }
}

/*class googleAnalyticsStorageHelper{//runOffBrowserLoad doesnt depend on session or cookie
  public function set($value) {
    //$_SESSION['access_token'] = $value;
	updateOption('googleAnalyticsAccessTokenSession', serialize($value));
  }

  public function get() {
    //return $_SESSION['access_token'];
	return unserialize(getOption('googleAnalyticsAccessTokenSession'));
  }

  public function delete() {
    //unset($_SESSION['access_token']);
	updateOption('googleAnalyticsAccessTokenSession', '');
  }
}*/

?>