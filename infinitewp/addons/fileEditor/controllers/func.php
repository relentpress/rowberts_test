<?php

function fileEditorAddonMenus(&$menus){
	$menus['tools']['subMenus'][] = array('page' => 'fileEditor', 'displayName' => 'File Uploader');
}

function fileEditorResponseProcessors(&$responseProcessor){
	$responseProcessor['fileEditor']['fileUpload'] = 'fileEditorUpload';
}

function fileEditorTaskTitleTemplate(&$template){
	$template['fileEditor']['fileUpload']['']	= "Uploading 1 file to <#sitesCount#> site<#sitesCountPlural#>";
}

function fileEditorUploader(){
	$actionID = Reg::get('currentRequest.actionID');
		
	$linkData = DB::getFields("?:temp_storage", "data", "type = 'fileEditor' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'fileEditor' AND paramID = '".$actionID."'");
		
	if(empty($linkData)){
		return array();
	}
	$refinedData = array();

	foreach($linkData as $data){
		$refinedData = array_merge_recursive($refinedData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($refinedData);
	foreach($refinedData as $siteID => $links){
		$siteData = DB::getRow("?:sites","URL","siteID=".$siteID."");
		$refinedData['siteURLs'][] = $siteData['URL'];
		$refinedData['siteIDs'][] = $siteID;
	}

	return $refinedData;
}