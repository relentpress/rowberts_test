<?php
	class manageClientsFileEditor
	{
		
		public static function fileEditorUploadProcessor($siteIDs,$params){
			$type = "fileEditor";
			$action = $params['action'];
			$requestAction = "file_editor_upload";
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => 'FileEditorUpload', 'detailedAction' => 'uploadFile');
			$events=1;
			$file = APP_ROOT."/uploads/".$params['uploadedFiles'][0];
			$fileOrig = substr($file,0,-4);
			$ext = substr($fileOrig,strrpos($fileOrig, '.')+1);
			$fileContent = '';
			if(file_exists($file)){
				$fileHandler = fopen($file,r);
				if (!function_exists('gzdeflate')) {
					addNotification($type='E', $title='Gzip library missing', 'Gzip library functions are not available.', $state='U', $callbackOnClose='', $callbackReference='');
					return false;
		        }else{
					$fileContent = gzdeflate(fread($fileHandler, filesize($file)));
		        }
				fclose($fileHandler);
			}else{
				addNotification($type='E', $title='Uploaded file corrupt', 'Uploaded file is corrupt/missing.', $state='U', $callbackOnClose='', $callbackReference='');
				return false;
			}
			foreach ($siteIDs as $index => $siteID) {
				$siteData = getSiteData(intval($siteID));
				$requestParams = array('filePath'=>$params['filePath'],'folderPath'=>$params['folderPath'],'fileContent'=>$fileContent,'ext'=>$ext);
				$PRP = array();
				$PRP['requestAction'] 	= $requestAction;
				$PRP['siteData'] 		= $siteData;
				$PRP['type'] 			= $type;
				$PRP['action'] 			= $action;
				$PRP['requestParams'] 	= $requestParams;
				$PRP['directExecute'] 	= false;
				$PRP['events'] 			= $events;
				$PRP['sendAfterAllLoad'] = true;
				$PRP['historyAdditionalData'] 	= $historyAdditionalData;
				prepareRequestAndAddHistory($PRP);
			}
		}
		
		public static function fileEditorUploadResponseProcessor($historyID, $responseData){
			responseDirectErrorHandler($historyID, $responseData);
			
			$response = array();
			if(isset($responseData['success'])){
				$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
				$actionID = $historyData['actionID'];
				$siteID = $historyData['siteID'];
				$response['_'.$siteID] = $responseData['success'];

				DB::insert("?:temp_storage", array('type' => 'fileEditor', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
				DB::update("?:history_additional_data", array('status'=>'success'), "historyID=".$historyID."");
				return;
			}
		}
	
	}
	manageClients::addClass('manageClientsFileEditor');