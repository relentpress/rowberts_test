<?php

/*
slug: fileEditor
version: 1.0.0
*/


class addonFileEditor{
	
	private static $version = '1.0.0';

	public static function version(){
		return self::version;
	}

	public static function init(){
		require_once(APP_ROOT."/addons/fileEditor/controllers/func.php");	
		require_once(APP_ROOT."/addons/fileEditor/controllers/manageClientsFileEditor.php");	
		panelRequestManager::addFunctions('fileEditorUploader');
		regHooks('addonMenus','responseProcessors','taskTitleTemplate');
	}	

}