function loadFileEditorContent(){
	var activeSites =$('.website_cont.active').length;
	if(activeSites>0){
		getFileEditorCont();
	}else{
		$("#pageContent").find('#fle_content').html('').hide();
	}
}
function getFileEditorCont(){
		var siteIDs = new Array();
		$('.website_cont.active').each(function(){
			siteIDs.push($(this).attr('sid'));
		});
		$(this).addClass('disabled');
		var content_block = $("#pageContent").find('#fle_content');
		content_block.html('<div id="FileUploaderBlock"></div><div class="fileuploader_options"></div><div class="fileprocessor_block"></div>');
		createFileEditorUploadOptions(content_block.find('.fileuploader_options'));
		$('.folderSelector').select2({
			width:'135px',
		});
		$('.folderSelector').find(".select2-search, .select2-focusser").remove();
		createFileEditorUploader('FileUploaderBlock');
		createFileEditorProcessor(content_block.find('.fileprocessor_block'));
		content_block.show();
	}

function createFileEditorUploader(block){
	var uploader = new qq.FileUploader({
		element: document.getElementById(block),
		action: systemURL+'uploadScript.php',
		multiple:false,
		params:{'allWPFiles':1},
		debug: true
	}); 
	$('#'+block).prepend('<div class="uploader_text">Upload file to replace\/insert</div>')
}
function createFileEditorUploadOptions (block) {
	var content = '<div class="fileLocation_text">Enter path to replace\/insert file</div><div class="fileLocation"><select class="folderSelector"><option value="admin">wp-admin</option><option value="content" selected>wp-content</option><option value="plugins" >plugins</option><option value="themes" >themes</option><option value="uploads" >uploads</option><option value="includes">wp-includes</option><option value="root">/</option></select><span style="font-size: 15px;padding: 2px;"> / </span><input type="text-box" class="fileSelector" placeholder="(optional)" /><span style="font-size: 15px;padding: 2px;"> / </span><input type="text-box" class="fileNameClient" /></div>'
	block.html(content);
}

function createFileEditorProcessor (block) {
	var content = "<div class='fileProcessor'><div class='fe_note'><ul>Note:<li>File that already exists with the same name in the same folder will be over-written.</li><li>Any error in an uploaded executable file may result in a broken site.</li></ul></div><div class='checkbox fe_riskchecker'>I understand the risks.</div><div class='processFile btn_action disabled'><a class='rep_sprite'>Process File</a></div></div>";
	block.html(content);
}


function loadFileEditorConfirmationPopup(object) {
	var e = '<div class="dialog_cont take_tour" style="width: 360px;"> <div class="th rep_sprite"> <div class="title droid700">ARE YOU SURE?</div></div> <div style="padding:20px;"><div style="text-align:center; line-height: 22px;" id="removeSiteCont">Are you sure you want to proceed with this?</div><table style="width:320px; margin:20px auto;"><tr><td><div class="btn_action float-right"></div></td><td><div class="btn_action float-right" style="margin-right: 40px;"><a class="rep_sprite closeUpdateChangeNotification cancelUpload" style="color: #6C7277;">No! Don\'t.</a></div><div class="btn_action float-right" style="margin-right: 30px; cursor:pointer;"><a class="rep_sprite btn_blue closeUpdateChangeNotification confirmUploadProcess" style="color: #6C7277;  cursor:pointer;">Yes! Go ahead.</a></div></td></tr></table></div> <div class="clear-both"></div></div>';
	tempConfirmObject = object;
	$("#modalDiv").dialog("destroy");
	$("#modalDiv").html(e).dialog({ width: "auto",modal: true,position: "center",resizable: false,open: function (e, t) {bottomToolBarHide()},close: function (e, t) {bottomToolBarShow()} }); 
	
}
function validateFileEditorParams(content_block,folder,file){
	if(!(folder == 'admin' || folder == 'content' || folder == 'plugins' || folder == 'themes' || folder == 'uploads' || folder == 'includes' || folder == 'root'))
		return [0,"Only Admin, Content, Plugins, Themes, Uploads, Includes and \/ are allowed"];
	else if(file == '' || typeof file == 'undefined')
		return [0,"Please give the file location"];
	else if (file == 'wp_config.php' && folder == 'root')
		return [0,"Config file cannot be altered"];
	else if(content_block.find('.qq-upload-list').html() == '' && content_block.find('.upload_progress.finished').length == 0){
		return [0,"No successful upload or please wait for upload to complete"];
	}else{
		return [1,"Success"];
	}
}

function FileEditorUploader(data){
	var content_block = $("#pageContent").find('#fle_content');
	content_block.find('.qq-upload-list').html('');
	content_block.find('select.folderSelector').val('content');
	content_block.find('.select2-container.folderSelector').find('a span').html('wp-content');
	content_block.find('.fileSelector,.fileNameClient').val('');
}

function formArrayFileEditorUploader(data){
	$("#historyQueue").show();
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="FEUploaderResult";
}