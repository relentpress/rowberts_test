$(function(){
	
	
	$('.processFile').live('click',function(){
		$("#pageContent").find('#fle_content').find('.processFile').addClass('disabled');
		$("#pageContent").find('#fle_content').find('.fe_riskchecker').removeClass('active');
		loadFileEditorConfirmationPopup($(this));
		return false;
	});

	$(".cancelUpload").live('click',function() {
		$("#modalDiv").dialog('close');
	});

	$(".fe_riskchecker").live('click',function() {
		var content_block = $("#pageContent").find('#fle_content');
		var folder = content_block.find('select.folderSelector').val();
		var folders_intermediate = content_block.find('.fileSelector').val();
		var file = content_block.find('.fileNameClient').val();
		var validity = validateFileEditorParams(content_block,folder,file);
		if(validity[0]){
			if($(this).hasClass('active')){
				$(this).removeClass('active');
				$('.processFile').addClass('disabled');
			}else{
				$(this).addClass('active');
				$('.processFile').removeClass('disabled');
			}
		}
		return false;
	});

	$(".confirmUploadProcess").live('click',function() {
		$("#modalDiv").dialog('close');
		var content_block = $("#pageContent").find('#fle_content');
		var folder = content_block.find('select.folderSelector').val();
		var folders_intermediate = content_block.find('.fileSelector').val();
		var file = content_block.find('.fileNameClient').val();
		var validity = validateFileEditorParams(content_block,folder,file);
		if(validity[0]){
			var uploadedFiles = [];
			$.each(content_block.find('.upload_progress.finished'),function(){
				uploadedFiles.push($(this).find('.installFileNames').html());
			});

			var siteIDs = new Array();
			$('.website_cont.active').each(function(){
				siteIDs.push($(this).attr('sid'));
			});
			var tempArray={};
			tempArray['action']='fileEditorUpload';
			tempArray['args']={};
			tempArray['args']['siteIDs']=siteIDs;
			tempArray['args']['params']={};
			tempArray['args']['params']['action'] = 'fileUpload';
			tempArray['args']['params']['filePath'] = [folders_intermediate,file];
			tempArray['args']['params']['folderPath'] = folder;
			tempArray['args']['params']['uploadedFiles'] = uploadedFiles;
			tempArray['requiredData'] = {};
			tempArray['requiredData']['fileEditorUploader'] = 1;
			doCall(ajaxCallPath,tempArray,'formArrayFileEditorUploader');
		}else{
			return false;
		}
	});
});