$(function(){

	$('.load_bls_main').live('click',function(){
		contentData='';
		var siteIDs = new Array();
		$("#pageContent").find('#bl_content').html('').hide();
		$('.website_cont.active').each(function(){
			siteIDs.push($(this).attr('sid'));
		});

		var tempArray={};
		var action = 'getAll';
		$(this).addClass('disabled');

		tempArray['action']='brokenLinksGetAllLinks';
		tempArray['args']={};
		tempArray['args']['siteIDs']=siteIDs;
		tempArray['args']['params']={};
		tempArray['args']['params']['action'] = action;
		tempArray['requiredData'] = {};
		tempArray['requiredData']['brokenLinksGetAllLinks'] = 1;
		doCall(ajaxCallPath,tempArray,'formArraybrokenLinksGetAllLinks');

	});

	$('.row_updatee,.link_actions').live('mouseover',function(){
		$(this).closest('.row_updatee').find('.link_actions').show();
	});

	$('.row_updatee,.link_actions').live('mouseout',function(){
		$(this).closest('.row_updatee').find('.link_actions').hide();
	});


$(".brokenlinkchecker .row_updatee").live('click',function(e) {
	var rdParent = $(this).closest('.rd');
	if($(e.target).attr('type') != 'textbox'){
		if ($(this).hasClass('active') && ($(this).closest('.ind_row_cont').find('.row_detailed').find('.bulkselect_blinks.active').length == $(this).closest('.ind_row_cont').find('.row_detailed').find('.bulkselect_blinks').length)) {
	 		$(this).closest('.ind_row_cont').removeClass('active').find('.select_all_blc_links').removeClass('active');
	 	}else if( !($(this).hasClass('active')) && ($(this).closest('.ind_row_cont').find('.row_detailed').find('.bulkselect_blinks.active').length == $(this).closest('.ind_row_cont').find('.row_detailed').find('.bulkselect_blinks').length-1) ){
	 		$(this).closest('.ind_row_cont').addClass('active').find('.select_all_blc_links').addClass('active');
	 	}
	 	if(!$(this).hasClass('active'))
	 	{
	 		$(this).addClass('active');
	 		$('.checkbox.bulkselect_blinks',this).addClass('active');
	 	}
	 	else
	 	{
	 		$(this).removeClass('active');
	 		$('.checkbox.bulkselect_blinks',this).removeClass('active');
	 	}
	 	if($('.bulkselect_blinks.active').length && $('.bl_userSelect.active').length){
	 		$('.blstatus_applyChangesCheck').removeClass('disabled');
	 	}else{
	 		$('.blstatus_applyChangesCheck').addClass('disabled');
	 	}
	}	 
});


	$('.load_bls').live('click',function(){
		var classification = $(this).attr('action');
		changeBrokenLinksContentHTML(classification);
	});

	$('.bl_action').live('click',function(){
		
		var row_updatee = $(this).closest('.row_updatee');
		var siteID = $(this).closest('.row_detailed').attr('sid');
		var linkID = row_updatee.attr('lid');
		var linkType = $('.load_bls.active').attr('action');
		if (linkType == 'all') {
			linkType = $(this).closest('.link_type_wrap').find('.link_type span').attr('class');
		}
		if($(this).attr('act')=='edit'){
			var displayurl = row_updatee.find('.bl_links .displayurl');
			var linktext = row_updatee.find('.bl_linktext .display_linktext');
			displayurl.hide();
			linktext.hide();
			row_updatee.find('.bl_links .editurl').show().find('input').val(displayurl.html());
			row_updatee.find('.bl_linktext .edit_bl_linktext').show().css('display','inline-block').find('input').val(linktext.html());
			row_updatee.find('.link_actions .link_acts').hide();
			row_updatee.find('.link_actions .editlink_acts').show();
		}
		if($(this).attr('act')=='edit_cancel'){
			row_updatee.find('.bl_links .editurl').hide();
			row_updatee.find('.bl_links .displayurl').show();
			row_updatee.find('.bl_linktext .edit_bl_linktext').hide();
			row_updatee.find('.bl_linktext .display_linktext').show();
			row_updatee.find('.link_actions .editlink_acts').hide();
			row_updatee.find('.link_actions .link_acts').show();
		}
		if($(this).attr('act')=='edit_update'){
			var newLink = row_updatee.find('.bl_links .editurl input').val();
			var newText = row_updatee.find('.bl_linktext .edit_bl_linktext input').val();
			var tempArray={};
			tempArray['action']='brokenLinksUpdateLink';
			tempArray['args']={};
			tempArray['args']['siteIDs']=siteID;
			tempArray['args']['params']={};
			tempArray['args']['params']['action'] = 'updateLink';
			tempArray['args']['params']['linktype'] = linkType;
			tempArray['args']['params']['linkid'] = linkID;
			tempArray['args']['params']['newlink'] = newLink;
			tempArray['args']['params']['newtext'] = newText;
			tempArray['requiredData'] = {};
			tempArray['requiredData']['brokenLinksUpdateLink'] = 1;
			doCall(ajaxCallPath,tempArray,'formArrayBrokenLinksUpdateLink');
		}

		if($(this).attr('act')=='dismiss' || $(this).attr('act')=='notbroken' || $(this).attr('act')=='unlink'){
			var tempArray={};
			var BLinfo = {};
			BLinfo[siteID] = [[linkID,linkType]];
			tempArray['action']='brokenLinksBulkActions';
			tempArray['args']={};
			tempArray['args']['siteIDs']=[siteID];
			tempArray['args']['params']={};
			tempArray['args']['params']['action'] = 'bulk_'+$(this).attr('act');
			if($(this).attr('act')=='notbroken') tempArray['args']['params']['action'] = 'bulk_mark_not_broken';
			tempArray['args']['params']['linkData'] = BLinfo;
			tempArray['requiredData'] = {};
			tempArray['requiredData']['brokenLinksBulkActions'] = 1;
			doCall(ajaxCallPath,tempArray,'formArrayBrokenLinksBulkActions');
		}

		if($(this).attr('act')=='undismiss'){
			var tempArray={};
			tempArray['action']='brokenLinksUndismissLink';
			tempArray['args']={};
			tempArray['args']['siteIDs']=siteID;
			tempArray['args']['params']={};
			tempArray['args']['params']['action'] = 'undismissBroken';
			tempArray['args']['params']['linkid'] = linkID;
			tempArray['args']['params']['linktype'] = linkType;
			tempArray['requiredData'] = {};
			tempArray['requiredData']['brokenLinksUndismissLink'] = 1;
			doCall(ajaxCallPath,tempArray,'formArrayBrokenLinksUndismissLink');
		}
		return false;
	});

	$(".searchSiteBL").live('keyup',function() {
		searchSites(this,4);
	});

	

	$('.bl_userSelect').live('click',function(){
		$('.bl_userSelect').removeClass('active');
		$(this).addClass('active');
		if($('.bulkselect_blinks.active').length && $('.bl_userSelect.active').length){
			$('.blstatus_applyChangesCheck').removeClass('disabled');
		}else{
			$('.blstatus_applyChangesCheck').addClass('disabled');
		}
	});

	$('.blstatus_applyChangesCheck').live('click',function(){
		$(this).addClass('disabled');
		var action = $('.bl_userSelect.active').attr('act');
		var cat = $('.load_bls.active').attr('action');
		var brokenCount = redirectedCount = dismissedCount = othersCount = 0;
		if(cat == 'all'){
			brokenCount = $('.link_type .broken').closest('.link_type_wrap').find('.bulkselect_blinks.active').length;
			redirectedCount = $('.link_type .redirected').closest('.link_type_wrap').find('.bulkselect_blinks.active').length;
			dismissedCount = $('.link_type .dismissed').closest('.link_type_wrap').find('.bulkselect_blinks.active').length;
			othersCount = $('.link_type .others').closest('.link_type_wrap').find('.bulkselect_blinks.active').length;
			if(action == 'bulk_mark_not_broken' && (redirectedCount || dismissedCount || othersCount) ){
				loadBrokenLinksProcessConfirmationPopup(action,cat);
			}else if(action == 'bulk_dismiss' && (dismissedCount || othersCount) ){
				loadBrokenLinksProcessConfirmationPopup(action,cat);
			}else{
				initiateBrokenLinksBulkProcess(action,cat);
			}

		}else if(cat == 'broken'){
			brokenCount = $('.bulkselect_blinks.active').length;
			initiateBrokenLinksBulkProcess(action,cat);
		}else if(cat == 'redirected'){
			redirectedCount = $('.bulkselect_blinks.active').length;
			if(action == 'bulk_mark_not_broken'){
				loadBrokenLinksProcessConfirmationPopup(action,cat);
			}else{
				initiateBrokenLinksBulkProcess(action,cat);
			}
		}else if(cat == 'dismissed'){
			dismissedCount = $('.bulkselect_blinks.active').length;
			if(action == 'bulk_mark_not_broken' || action == 'bulk_dismiss'){
				loadBrokenLinksProcessConfirmationPopup(action,cat);
			}else{
				initiateBrokenLinksBulkProcess(action,cat);
			}
		}

	});

	$('.cancelBLProcess').live('click',function(){
		$("#modalDiv").dialog('close');
	});

	$('.confirmBLProcess').live('click',function(){
		$("#modalDiv").dialog('close');
		var action = $(this).attr('act');
		var cat = $('.load_bls.active').attr('action');
		if(brokenLinksUnselectNotEligibleLinks(action,cat)){
			initiateBrokenLinksBulkProcess(action,cat);
		}
	});
			
	$('.website_cont.hidden_blc').live('hover',function(e){
		if($(this).hasClass('disabled'))
		{
			$(this).removeClass('disabled');
			$(this).find('.tips').show();
		}
		else if($(this).find('.tips').length)
		{
			$(this).addClass('disabled');
			$(this).find('.tips').hide();
		}
		
	});

	$('.website_cont.hidden_blc .tips').live('click',function(){
		if(typeof $(this).attr('code') !== 'undefined' && $(this).attr('code') !== false){
		var action = $(this).attr('code') == '1'?'activate':'install' ; 
		}else{
			return false;
		}
		var siteID = $(this).closest('.website_cont.hidden_blc').attr('sid');
		$(this).closest('.website_cont').removeClass('disabled');
		$(this).remove();
		if(action == 'install'){
			var tempArray={};
			tempArray['requiredData']={};
			var valArray={};
			valArray['type']='plugins';
			valArray['siteID']=siteID;
			valArray['plugin_slug']='broken-link-checker';
			tempArray['requiredData']['installNotInstalledPlugin']=valArray;
			doCall(ajaxCallPath,tempArray,"installBrokenLinkChecker","json","none");

		}else if(action == 'activate'){
			var changeArray={};
			var dID, type, action, name, valArray;
			dID="broken-link-checker/broken-link-checker.php";
			type = 'plugins';
			action="activate";
			name="Broken Link Checker";
			changeArray[siteID]={};
			changeArray[siteID][type]={};
			valArray={};
			valArray['name']=name;
			valArray['path']=dID;
			valArray['action']=action;
			changeArray[siteID][type][0]=valArray;

			var tempArray={};
			tempArray['args']={};
			tempArray['args']['params']={};
			tempArray['action']='managePlugins';
			tempArray['args']['params'] = changeArray;
			doHistoryCall(ajaxCallPath,tempArray,'brokenLinksCheckPluginInitialization');
		}
	});

	$('#blc_installation_bulk').live('click',function(){
		var activationSiteIDs = [],installationSiteIDs = [],isActing=1;
		$('.website_cont.hidden_blc .tips').each(function(){
			var siteID = $(this).closest('.website_cont.hidden_blc').attr('sid');
			if(typeof $(this).attr('code') !== 'undefined' && $(this).attr('code') !== false){
			if($(this).attr('code') == '1'){
				activationSiteIDs.push(siteID);
				}else if($(this).attr('code') == '0'){
				installationSiteIDs.push(siteID);
			}
			}else{
				isActing =0;
				return false;
			}
		});
		// activation bulk starts
		if(activationSiteIDs.length>0){
			var changeArray={};
			var dID, type, action, name, valArray;
			dID="broken-link-checker/broken-link-checker.php";
			type = 'plugins';
			action="activate";
			name="Broken Link Checker";
			$.each(activationSiteIDs,function(index,siteID){
				changeArray[siteID]={};
				changeArray[siteID][type]={};
				valArray={};
				valArray['name']=name;
				valArray['path']=dID;
				valArray['action']=action;
				changeArray[siteID][type][0]=valArray;
			});

			var tempArray={};
			tempArray['args']={};
			tempArray['args']['params']={};
			tempArray['action']='managePlugins';
			tempArray['args']['params'] = changeArray;
			doHistoryCall(ajaxCallPath,tempArray,'brokenLinksCheckPluginInitialization');
		}
		// activation bulk ends

		// installation bulk starts
		if(installationSiteIDs.length>0){

			var tempArray={};
			tempArray['requiredData']={};
			var valArray={};
			valArray['type']='plugins';
			valArray['siteID']=installationSiteIDs;
			valArray['plugin_slug']='broken-link-checker';
			tempArray['requiredData']['installNotInstalledPlugin']=valArray;
			doCall(ajaxCallPath,tempArray,"installBrokenLinkCheckerMultiSites","json","none");
		}
		// installation bulk ends
		if(isActing){	
		$('.website_cont.disabled .tips').remove();
		$('.website_cont.disabled').removeClass('disabled');
		}
	});
		
		
	$('.select_all_blc_links').live('click',function(){
		if ($(this).hasClass('active')) {
			$(this).closest('.ind_row_cont').find('.row_detailed').find('.bulkselect_blinks').removeClass('active');
			$(this).closest('.ind_row_cont').find('.row_detailed').find('.row_updatee').removeClass('active');
			$(this).closest('.ind_row_cont').removeClass('active');
			$(this).closest('.ind_row_cont').find('.select_all_blc_links').removeClass('active');
		}else{
			$(this).closest('.ind_row_cont').find('.row_detailed').find('.bulkselect_blinks').addClass('active');
			$(this).closest('.ind_row_cont').find('.row_detailed').find('.row_updatee').addClass('active');
			$(this).closest('.ind_row_cont').addClass('active');
			$(this).closest('.ind_row_cont').find('.select_all_blc_links').addClass('active');
		}
		if($('.bulkselect_blinks.active').length && $('.bl_userSelect.active').length){
			$('.blstatus_applyChangesCheck').removeClass('disabled');
		}else{
			$('.blstatus_applyChangesCheck').addClass('disabled');
		}
		return false;
	});
	


});