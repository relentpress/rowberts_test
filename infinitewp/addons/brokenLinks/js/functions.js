			
var contentData='';
function loadBrokenLinksContent(e){
	var activeSites =$('.website_cont.active').length;
	if(activeSites>0 ){
		if(typeof e == 'undefined'){
			$('.load_bls_main').removeClass('disabled');
		}else{
			if( !($( e.target ).closest('.website_cont').find('.tips').is(':visible')) ){
				$('.load_bls_main').removeClass('disabled');
			}else{
				$( e.target ).closest('.website_cont').removeClass('active');
			}
		}
	}else{
		$('.load_bls_main').addClass('disabled');
		$("#pageContent").find('.link_categories').hide();
		$("#pageContent").find('.link_categories span').removeAttr('class');
		$("#pageContent").find('#bl_content').html('').hide();
	}
}


function formArraybrokenLinksGetAllLinks(data){
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="loadbrokenLinksGetAllLinksResult";	
}

function brokenLinksGetAllLinks(data){
	var content = data.data.brokenLinksGetAllLinks;
	contentData = classifyBrokenLinksResultContent(content);
	content = loadBrokenLinksContentHTML();
	$("#pageContent").find('#bl_content').append(content).show();
}

function formArrayBrokenLinksUpdateLink(data){
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="getBrokenLinksUpdateLink";	
}

function brokenLinksNoLinksContent(){
	var brokenCount = 0;var redirectedCount = 0;var dismissedCount = 0;var othersCount = 0;
	$.each(contentData,function(siteID,siteData){
		brokenCount += siteData['broken'].length;
		redirectedCount += siteData['redirected'].length;
		dismissedCount += siteData['dismissed'].length;
		othersCount += siteData['others'].length;
	});
	var cat = $('.load_bls.active').attr('action');
	if(cat == 'all'){
		if(brokenCount+redirectedCount+dismissedCount+othersCount == 0)
			$('.rows_cont').html('<div class="gwmt_no_issues">No links in the selected sites.</div>');
	}else{
		if(eval(cat+'Count') == 0)
			$('.rows_cont').html('<div class="gwmt_no_issues">No '+cat+' links in the selected sites.</div>');
	}
}

function brokenLinksUpdateLink(data){
	data = data.data.brokenLinksUpdateLink;
	var siteID = data['siteID'];
	var linkData = data[siteID];
	var linkType = linkData['linkType'];
	var newlink = linkData['new_link']['url'];
	var newLinkID = linkData['new_link_id'];
	var newText = linkData['new_text'];
	var oldLinkID = linkData['old_link_id'];
	var oldlinkDOM = $('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.row_updatee[lid="'+oldLinkID+'"]');

	$.each(contentData[siteID][linkType],function(index,link){
		if(link['link_id'] == oldLinkID){
			link['url'] = newlink;
			link['link_text'] = newText;
		}
	});

	oldlinkDOM.find('.bl_links .editurl').hide();
	oldlinkDOM.find('.bl_links .displayurl').html(newlink).show();
	oldlinkDOM.find('.bl_linktext .edit_bl_linktext').hide();
	oldlinkDOM.find('.bl_linktext .display_linktext').html(newText).show();
	oldlinkDOM.find('.link_actions .editlink_acts').hide();
	oldlinkDOM.find('.link_actions .link_acts').show();
	oldlinkDOM.attr('lid',newLinkID);
}

function brokenLinksUnlink(siteID,linkData){
	if(linkData['link_deleted']){
		var oldLinkID = linkData['old_link_id'];
		var linkType = linkData['linkType'];
		var oldlinkDOM = $('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.row_updatee[lid="'+oldLinkID+'"]');
		if($('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.link_type .'+linkType).closest('.link_type_wrap').find('.row_updatee').length <= 1){
			oldlinkDOM.closest('.link_type_wrap').hide();
		}
		oldlinkDOM.remove();
		$.each(contentData[siteID][linkType],function(index,link){
			if(link['link_id'] == oldLinkID){
				contentData[siteID][linkType].splice(index,1);
				return false;
			}
		});
		brokenLinksNoLinksContent();
	}
}

function brokenLinksMarkAsNotBroken(siteID,linkData){
	if(linkData['marked']){
		var oldLinkID = linkData['old_link_id'];
		var linkType = linkData['linkType'];
		var oldlinkDOM = $('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.row_updatee[lid="'+oldLinkID+'"]');
		if ($('.load_bls.active').attr('action') == 'all') {
			var clone  = oldlinkDOM.clone();
			$('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.link_type .others').closest('.link_type_wrap').append('<div class="row_updatee" lid="'+oldLinkID+'">'+clone.html()+'</div>');
			$('.row_updatee[lid="'+oldLinkID+'"]').find('.link_acts').find('.bl_action[act="notbroken"]').remove();
			$('.row_updatee[lid="'+oldLinkID+'"]').find('.link_acts').find('.bl_action[act="dismiss"]').remove();
		}
		if($('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.link_type .'+linkType).closest('.link_type_wrap').find('.row_updatee').length <= 1){
			oldlinkDOM.closest('.link_type_wrap').hide();
		}
		oldlinkDOM.remove();
		$.each(contentData[siteID][linkType],function(index,link){
			if(link['link_id'] == oldLinkID){
				link['broken'] = 0;
				contentData[siteID]['others'].push(link);
				contentData[siteID][linkType].splice(index,1);
				return false;
			}
		});
		brokenLinksNoLinksContent();
	}
}

function brokenLinksDismiss(siteID,linkData){
	if(linkData['dismissvalue_set']){
		var oldLinkID = linkData['old_link_id'];
		var linkType = linkData['linkType'];
		var oldlinkDOM = $('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.row_updatee[lid="'+oldLinkID+'"]');
		if ($('.load_bls.active').attr('action') == 'all') {
			var clone  = oldlinkDOM.clone();
			$('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.link_type .dismissed').closest('.link_type_wrap').append('<div class="row_updatee" lid="'+oldLinkID+'">'+clone.html()+'</div>').show();
			$('.row_updatee[lid="'+oldLinkID+'"]').find('.link_acts').find('.bl_action[act="notbroken"]').remove();
			$('.row_updatee[lid="'+oldLinkID+'"]').find('.link_acts').find('.bl_action[act="dismiss"]').attr('act','undismiss').html('<a class="undismiss" title="Undismiss"></a>');
		}
		if($('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.link_type .'+linkType).closest('.link_type_wrap').find('.row_updatee').length <= 1){
			oldlinkDOM.closest('.link_type_wrap').hide();
		}
		oldlinkDOM.remove();
		$.each(contentData[siteID][linkType],function(index,link){
			if(link['link_id'] == oldLinkID){
				link['dismissed'] = 1;
				contentData[siteID]['dismissed'].push(link);
				contentData[siteID][linkType].splice(index,1);
				return false;
			}
		});
		brokenLinksNoLinksContent();
	}
}

function formArrayBrokenLinksUndismissLink(data){
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="getBrokenLinksUndismissLink";	
}

function brokenLinksUndismissLink(data){
	data = data.data.brokenLinksUndismissLink;
	var siteID = data['siteID'];
	var linkData = data[siteID];
	if(linkData['dismissvalue_set']){
		var oldLinkID = linkData['old_link_id'];
		var linkType = linkData['linkType'];
		var oldlinkDOM = $('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.row_updatee[lid="'+oldLinkID+'"]');
		if ($('.load_bls.active').attr('action') == 'all') {
			oldlinkDOM.find('.link_acts').find('.bl_action[act="undismiss"]').attr('act','dismiss').html('<a class="dismiss" title="Dismiss"></a>');
			var clone  = oldlinkDOM.clone();
		}
		if($('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.link_type .'+linkType).closest('.link_type_wrap').find('.row_updatee').length <= 1){
			oldlinkDOM.closest('.link_type_wrap').hide();
		}
		oldlinkDOM.remove();
		$.each(contentData[siteID][linkType],function(index,link){
			if(link['link_id'] == oldLinkID){
				link['dismissed'] = 0;
				if(link['false_positive'] == 1){
					contentData[siteID]['others'].push(link);
					if ($('.load_bls.active').attr('action') == 'all'){
						$('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.link_type .others').closest('.link_type_wrap').append('<div class="row_updatee" lid="'+oldLinkID+'">'+clone.html()+'</div>').show();
					}
				}
				else if(link['broken'] == 1)	{
					contentData[siteID]['broken'].push(link);
					if ($('.load_bls.active').attr('action') == 'all'){
						$('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.link_type .broken').closest('.link_type_wrap').append('<div class="row_updatee" lid="'+oldLinkID+'">'+clone.html()+'</div>').show();
						$("<li class='bl_action rep_sprite' act='notbroken'><a class='mark_not_broken' title='Mark as Not Broken'></a></li>").insertBefore($('.row_updatee[lid="'+oldLinkID+'"]').find('.link_acts').find('.bl_action[act="dismiss"]'));
					}
				}
				else if(link['redirect_count'] == 1){
					contentData[siteID]['redirected'].push(link);
					if ($('.load_bls.active').attr('action') == 'all'){
						$('#bl_content').find('.row_detailed[sid="'+siteID+'"]').find('.link_type .redirected').closest('.link_type_wrap').append('<div class="row_updatee" lid="'+oldLinkID+'">'+clone.html()+'</div>').show();
					}
				}
				contentData[siteID][linkType].splice(index,1);
				return false;
			}
		});
		brokenLinksNoLinksContent();
	}
}

function loadBrokenLinksProcessConfirmationPopup(action,cat) {
	var m = s = n = '';
	var act='';
	if(action == 'bulk_mark_not_broken'){
		act = 'Mark as Not Broken';
	}else if(action == 'bulk_dismiss'){
		act = 'Dismiss';
	}
	if(cat == 'all'){
		m = 'Some of the selected links are not eligible for \''+act+'\'. Do you want to proceed removing the ineligible links and continue with \''+act+'\'?';
		s = 'Yes! Go ahead.';
		n = 'No! Don\'t.';
	}else{
		m = '\''+act+'\' option is not applicable for \''+cat+'\' category. Please choose another action.';
		s = 'Remove my selections';
		n = 'OK';
	}
	var e = '<div class="dialog_cont take_tour" style="width: 360px;"> <div class="th rep_sprite"> <div class="title droid700">ARE YOU SURE?</div></div> <div style="padding:20px;"><div style="text-align:center; line-height: 22px;" id="removeSiteCont">'+m+'</div><table style="width:320px; margin:20px auto;"><tr><td><div class="btn_action float-right"></div></td><td><div class="btn_action float-right" style="margin-right: 40px;"><a class="rep_sprite closeUpdateChangeNotification cancelBLProcess" style="color: #6C7277;">'+n+'</a></div><div class="btn_action float-right" style="margin-right: 30px; cursor:pointer;"><a class="rep_sprite btn_blue closeUpdateChangeNotification confirmBLProcess" act="'+action+'" style="color: #6C7277;  cursor:pointer;">'+s+'</a></div></td></tr></table></div> <div class="clear-both"></div></div>';
	
	$("#modalDiv").dialog("destroy");
	$("#modalDiv").html(e).dialog({ width: "auto",modal: true,position: "center",resizable: false,open: function (e, t) {bottomToolBarHide()},close: function (e, t) {bottomToolBarShow()} }); 
	
}

function brokenLinksUnselectNotEligibleLinks (action,cat) {
	if(cat == 'all'){
		if(action == 'bulk_mark_not_broken'){
			$('.link_type .redirected,.link_type .dismissed,.link_type .others').closest('.link_type_wrap').find('.bulkselect_blinks').removeClass('active').closest('.row_updatee').removeClass('active');
		}else if(action == 'bulk_dismiss'){
			$('.link_type .dismissed,.link_type .others').closest('.link_type_wrap').find('.bulkselect_blinks').removeClass('active').closest('.row_updatee').removeClass('active');
		}
	}else if(cat == 'redirected'){
		if(action == 'bulk_mark_not_broken'){
			$('.bulkselect_blinks').removeClass('active').closest('.row_updatee').removeClass('active');
		}
	}else if(cat == 'dismissed'){
		if(action == 'bulk_mark_not_broken' || action == 'bulk_dismiss'){
			$('.bulkselect_blinks').removeClass('active').closest('.row_updatee').removeClass('active');
		}
	}
	$.each($('.row_detailed.active'),function(){
		if(!($(this).find('.bulkselect_blinks.active').length)){
			$(this).removeClass('active').find('.select_all_blc_links').removeClass('active');
		}
	});
	return $('.bulkselect_blinks.active').length;
}

function initiateBrokenLinksBulkProcess (action,cat) {
	var BLinfo = {};
	var BLsites = [];
	$.each($('.row_detailed.active'),function(){
		var siteID = $(this).attr('sid');
		BLsites.push(siteID);
		BLinfo[siteID] = [];
		$.each($(this).find('.bulkselect_blinks.active'),function(){
			var blinfo = $(this).closest('.row_updatee');
			var linkType = $('.load_bls.active').attr('action');
			if (linkType == 'all') {
				linkType = blinfo.closest('.link_type_wrap').find('.link_type span').attr('class');
			}
			BLinfo[siteID].push([blinfo.attr('lid'),linkType]);
		});
	});
	var tempArray = {};
	tempArray['action']='brokenLinksBulkActions';
	tempArray['args']={};
	tempArray['args']['siteIDs']=BLsites;
	tempArray['args']['params']={};
	tempArray['args']['params']['action'] = action;
	tempArray['args']['params']['linkData'] = BLinfo;
	tempArray['requiredData'] = {};
	tempArray['requiredData']['brokenLinksBulkActions'] = 1;
	doCall(ajaxCallPath,tempArray,'formArrayBrokenLinksBulkActions');
}

function formArrayBrokenLinksBulkActions(data){
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="loadbrokenLinksBulkActionsResult";	
}
	
function brokenLinksBulkActions(data){
	data = data.data.brokenLinksBulkActions;
	$.each(data,function(siteID,linkData){
		if(linkData[1] == 'bulk_dismiss'){
			$.each(linkData[0],function(index,link){
				link = link;
				brokenLinksDismiss(siteID,link);
			});
		}else if (linkData[1] == 'bulk_mark_not_broken') {
			$.each(linkData[0],function(index,link){
				brokenLinksMarkAsNotBroken(siteID,link);
			});
		}else if (linkData[1] == 'bulk_unlink') {
			$.each(linkData[0],function(index,link){
				brokenLinksUnlink(siteID,link);
			});
		}
	});
	$('.bulkselect_blinks,.row_detailed,.bl_userSelect').removeClass('active');
}

function brokenLinksCheckPluginInitialization(data) {
	$("#historyQueue").show();
	var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['pluginInintializationReload']='Broken Link Checker';
	doCall(ajaxCallPath,tempArray);
}

function installBrokenLinkChecker(data){
	var data = data.data.installNotInstalledPlugin;
	var dLink = data[0].download_link;
	var tempArray={};
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['args']['siteIDs']={};
	var params={}; 
	tempArray['action'] = 'installPlugins';
	params['plugins']={};
	tempArray['args']['siteIDs'][0] = data[1];
	params['plugins']['0']=dLink;
	params['activate'] = true;
	tempArray['args']['params'] = params;
	doHistoryCall(ajaxCallPath,tempArray,'brokenLinksCheckPluginInitialization');
}

function installBrokenLinkCheckerMultiSites(data){
	var data = data.data.installNotInstalledPlugin;
	var dLink = data[0].download_link;
	var tempArray={};
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['args']['siteIDs']=data[1];
	var params={}; 
	tempArray['action'] = 'installPlugins';
	params['plugins']={};
	params['plugins']['0']=dLink;
	params['activate'] = true;
	tempArray['args']['params'] = params;
	doHistoryCall(ajaxCallPath,tempArray,'brokenLinksCheckPluginInitialization');
}