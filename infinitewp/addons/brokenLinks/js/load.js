function loadBrokenLinksPage(data)
{
	
	var content='<div class="steps_hdr"><span id="processType">MANAGE</span> <span class="itemUpper">Broken Links</span><div id="blc_installation_bulk" class="float-right"><a class="siteSelectorSelect">Install &amp; activate Broken Link Checker plugin in all sites</a></div></div><div class="siteSelectorContainer">'+siteSelectorVar+'</div><div class="btn_action float-right load_bls_main disabled" ><a class="rep_sprite">Check for Broken Links</a></div><div id="bl_content" style="display:none;clear:both;"></div>';
	$("#pageContent").html(content);
	$(".website_cont").addClass('hidden_blc');
	$(".website_cont.hidden_blc").addClass('disabled');
	currentPage="brokenLinks";
	getRecentPluginsStatusAndCheck();
	var tempArray = {};
	tempArray['requiredData'] = {};
	tempArray['requiredData']['getRecentPluginsStatus'] = 1;
	doCall(ajaxCallPath,tempArray,'getRecentPluginsStatusAndCheck');
}

function classifyBrokenLinksResultContent(data){
	var siteIDs   = data['siteIDs'];
	var classified = {};
	if(typeof siteIDs != 'undefined' && siteIDs.length > 0){
		$.each(siteIDs,function(index,siteID){
			var siteURL = data['siteURLs'][index];
			var siteData = data[siteID];
			if(typeof siteData != 'undefined' && objLen(siteData) > 0){
				if(siteData != 'nolinks'){
					classified[siteID] = {'broken':[] , 'redirected':[], 'dismissed':[], 'others':[] };
					$.each(siteData,function(SDindex,link){
						if(link['dismissed'] == 1){
							classified[siteID]['dismissed'].push(link);
						}else if(link['broken'] == 1 && link['false_positive'] != 1){
							classified[siteID]['broken'].push(link);
						}else if(link['redirect_count'] == 1){
							classified[siteID]['redirected'].push(link);
						}else{
							classified[siteID]['others'].push(link);
						}
						classified[siteID]['siteURL'] = siteURL;
					});
				}else{
					classified[siteID] = siteData;
				}
			}
		});
	}
	return classified;
}


function loadBrokenLinksContentHTML() {
	var HTMLData ='';
	if(typeof contentData != 'undefined' && objLen(contentData) > 0){
		HTMLData='<div class="actionContent result_block siteSearch brokenlinkchecker" id=""><div class="th rep_sprite"> <ul class="btn_radio_slelect float-left" style="margin-left: 10px;"> <li><a class="rep_sprite load_bls optionSelect" action="all">All</a> </li> <li><a class=" rep_sprite optionSelect load_bls active" action="broken">Broken</a> </li> <li><a class="rep_sprite load_bls optionSelect" action="redirected">Redirected</a> </li> <li><a class="rep_sprite load_bls optionSelect" action="dismissed">Dismissed</a> </li> </ul> <div class="type_filter"> <input name="filter" type="text" class="input_type_filter searchSiteBL" value="type to filter" /> <div class="clear_input rep_sprite_backup" onclick=""></div> </div> <div class="btn_action float-right"><a class="rep_sprite blstatus_applyChangesCheck disabled">Apply Changes</a></div><ul class="th_sub_nav" style="margin-top: 3px; float: right;"><li><a class="rep_sprite bl_userSelect" act="bulk_mark_not_broken">Mark as Not Broken</a></li><li><a class="rep_sprite bl_userSelect" act="bulk_unlink">Unlink</a></li><li><a class="rep_sprite bl_userSelect" act="bulk_dismiss">Dismiss</a></li> </ul> </div><div class="no_match hiddenCont" style="display:none">Bummer, there are no backups that match.<br>Try typing fewer characters.</div><div class="rows_cont">';
		var HTMLInnerData = '';
		var noLinks = true;
		$.each(contentData,function(siteID,data){
			var siteURL = data['siteURL'];
			var linkTabs = '';
			if(data != 'nolinks') noLinks = false;
			if(typeof data['broken'] != 'undefined' && data['broken'].length ){
				$.each(data['broken'],function(index,link){
					if(link['http_code']==0 && link['status_text']==null) link['status_text'] = 'Not checked';
					var linkBrokenCheck = '';
					var linkDismissCheck = '';
					if(link['broken']==1){linkBrokenCheck = "<li class='bl_action rep_sprite' act='notbroken'><a class='mark_not_broken' title='Mark as Not Broken'></a></li>";}
					if(link['dismissed']==1){linkDismissCheck = "<li class='bl_action rep_sprite' act='undismiss'><a class='undismiss' title='Undismiss'></a></li>";}
					if(link['dismissed']==0 && link['type']!='all' && (link['broken']==1 || link['redirect_count']>0)){linkDismissCheck = "<li class='bl_action rep_sprite' act='dismiss'><a class='dismiss' title='Dismiss'></a></li>";}
					var linkActions = "<li class='bl_action rep_sprite' act='edit'><a class='edit_url' title='Edit URL'></a></li> <li class='bl_action rep_sprite' act='unlink'><a class='unlink' title='Unlink'></a></li> "+linkBrokenCheck+" "+linkDismissCheck+" </ul><div class='editlink_acts'><a class='bl_action save_edit rep_sprite' act='edit_update'>Save</a> <a class='bl_action' act='edit_cancel'>Cancel</a>";
					linkTabs = linkTabs+"<div class='row_updatee' lid='"+link['link_id']+"'><div class='checkbox bulkselect_blinks'></div><div class='link_can_edits'><div class='bl_linktext'><div class='display_linktext'>"+link['link_text']+"</div><div class='edit_bl_linktext'><input type='textbox' value='"+link['link_text']+"'/></div></div><div class='bl_links'><div class='float-left displayurl'>"+link['url']+"</div><div class='droid700 float-left editurl'><input type='textbox' value='"+link['url']+"'/></div></div></div><div class='link_data'><div class='link_stats bl_source'>"+link['source']+"</div><div class='link_stats bl_status'>"+link['status_text']+"</div><div class='link_stats bl_httpcode'>"+link['http_code']+"</div><div class='link_actions'><ul class='link_acts'>"+linkActions+"</div></div></div><div class='clear-both'></div></div>";
				});
			}else{
				linkTabs = 'none';
			}
			if(linkTabs != 'none')
				HTMLInnerData += "<div class='ind_row_cont'><div class='row_summary' sid='"+siteID+"'><div class='row_arrow'></div><div class='checkbox select_all_blc_links'></div><div class='row_name searchable'>"+siteURL+"</div><div class='clear-both'></div></div><div class='row_detailed' sid='"+siteID+"' style='display:none;clear:both;'><div class='rh'><div class='row_arrow'></div><div class='checkbox select_all_blc_links'></div><div class='row_name'>"+siteURL+"</div><div class='link_details'><span class='link_details_text droid700' style='margin-right: 88px;'>Source</span><span class='link_details_text droid700' style='margin-right: 103px;'>Status</span><span class='link_details_text droid700'>HTTP Code</span></div><div class='clear-both'></div></div><div class='rd'>"+linkTabs+"</div></div></div>";

		});
		if(HTMLInnerData == '' && noLinks == false) HTMLInnerData = '<div class="gwmt_no_issues">Yipee! No broken links in the selected sites.</div>';
		if(HTMLInnerData == '' && noLinks == true) HTMLInnerData = '<div class="gwmt_no_issues">No links found in the selected sites.</div>';

		HTMLData = HTMLData+HTMLInnerData+'</div></div>';
	}
	return HTMLData;
}

function changeBrokenLinksContentHTML(filter){
	$('.rows_cont').html('');
	var content ='';
	if(typeof contentData != 'undefined' && objLen(contentData) > 0){
		if(filter != 'all'){
			var noLinks = true;
			if (filter == 'redirected') {
				$('.bl_userSelect[act="bulk_mark_not_broken"]').parent().hide();
				$('.bl_userSelect[act="bulk_dismiss"]').parent().show();
			}else if(filter == 'dismissed'){
				$('.bl_userSelect[act="bulk_mark_not_broken"]').parent().hide();
				$('.bl_userSelect[act="bulk_dismiss"]').parent().hide();
			}else if(filter == 'broken'){
				$('.bl_userSelect').parent().show();
			}
			$.each(contentData,function(siteID,data){
				var siteURL = data['siteURL'];
				var linkTabs = '';
				if(data != 'nolinks')	noLinks = false;
				if(typeof data[filter] != 'undefined' && data[filter].length){
					$.each(data[filter],function(index,link){
						if(link['http_code']==0 && link['status_text']==null) link['status_text'] = 'Not checked';
						var linkBrokenCheck = '';
						var linkDismissCheck = '';
						if(link['broken']==1){linkBrokenCheck = "<li class='bl_action rep_sprite' act='notbroken'><a class='mark_not_broken' title='Mark as Not Broken'></a></li>";}
						if(link['dismissed']==1){linkDismissCheck = "<li class='bl_action rep_sprite' act='undismiss'><a class='undismiss' title='Undismiss'></a></li>";}
						if(link['dismissed']==0 && link['type']!='all' && (link['broken']==1 || link['redirect_count']>0)){linkDismissCheck = "<li class='bl_action rep_sprite' act='dismiss'><a class='dismiss' title='Dismiss'></a></li>";}
						var linkActions = "<li class='bl_action rep_sprite' act='edit'><a class='edit_url' title='Edit URL'></a></li> <li class='bl_action rep_sprite' act='unlink'><a class='unlink' title='Unlink'></a></li> "+linkBrokenCheck+" "+linkDismissCheck+" </ul><div class='editlink_acts'><a class='bl_action save_edit rep_sprite' act='edit_update'>Save</a> <a class='bl_action' act='edit_cancel'>Cancel</a>";
						linkTabs = linkTabs+"<div class='row_updatee' lid='"+link['link_id']+"'><div class='checkbox bulkselect_blinks'></div><div class='link_can_edits'><div class='bl_linktext'><div class='display_linktext'>"+link['link_text']+"</div><div class='edit_bl_linktext'><input type='textbox' value='"+link['link_text']+"'/></div></div><div class='bl_links'><div class='float-left displayurl'>"+link['url']+"</div><div class='droid700 float-left editurl'><input type='textbox' value='"+link['url']+"'/></div></div></div><div class='link_data'><div class='link_stats bl_source'>"+link['source']+"</div><div class='link_stats bl_status'>"+link['status_text']+"</div><div class='link_stats bl_httpcode'>"+link['http_code']+"</div><div class='link_actions'><ul class='link_acts'>"+linkActions+"</div></div></div><div class='clear-both'></div></div>";
					});
				}else{
					// linkTabs = 'No '+filter+' links';
					linkTabs = "none";
				}
				if(linkTabs != 'none'){
					content = content+ "<div class='ind_row_cont'><div class='row_summary' sid='"+siteID+"'><div class='row_arrow'></div><div class='checkbox select_all_blc_links'></div><div class='row_name searchable'>"+siteURL+"</div><div class='clear-both'></div></div><div class='row_detailed' sid='"+siteID+"' style='display:none;clear:both;'><div class='rh'><div class='row_arrow'></div><div class='checkbox select_all_blc_links'></div><div class='row_name'>"+siteURL+"</div><div class='link_details'><span class='link_details_text droid700' style='margin-right: 88px;'>Source</span><span class='link_details_text droid700' style='margin-right: 103px;'>Status</span><span class='link_details_text droid700'>HTTP Code</span></div><div class='clear-both'></div></div><div class='rd'>"+linkTabs+"</div></div></div>";
				}
			});
			if(content == ''){
				if(noLinks){
					content = '<div class="gwmt_no_issues">No links found in the selected sites.</div>';
				}else{
					content = '<div class="gwmt_no_issues">No '+filter+' links in the selected sites.</div>';
				}
			}
		}else{
			var noLinks = true;
			$('.bl_userSelect').parent().show();
			$.each(contentData,function(siteID,data){
				var siteURL = data['siteURL'];
				var linkTabs = '';
				var content_check = 0;
				var filters = ['broken','redirected','dismissed','others'];
				for(i=0;i<filters.length;i++){
					if(data != 'nolinks')	noLinks = false;
					if(typeof data[filters[i]] != 'undefined' && data[filters[i]].length){
						linkTabs = linkTabs+'<div class="link_type_wrap"><div class="link_type"><span class="'+filters[i]+'">'+filters[i]+'</span></div>';
						$.each(data[filters[i]],function(index,link){
							if(link['http_code']==0 && link['status_text']==null) link['status_text'] = 'Not checked';
							var linkBrokenCheck = '';
							var linkDismissCheck = '';
							if(link['broken']==1){linkBrokenCheck = "<li class='bl_action rep_sprite' act='notbroken'><a class='mark_not_broken' title='Mark as Not Broken'></a></li>";}
							if(link['dismissed']==1){linkDismissCheck = "<li class='bl_action rep_sprite' act='undismiss'><a class='undismiss' title='Undismiss'></a></li>";}
							if(link['dismissed']==0 && link['type']!='all' && (link['broken']==1 || link['redirect_count']>0)){linkDismissCheck = "<li class='bl_action rep_sprite' act='dismiss'><a class='dismiss' title='Dismiss'></a></li>";}
							var linkActions = "<li class='bl_action rep_sprite' act='edit'><a class='edit_url' title='Edit URL'></a></li> <li class='bl_action rep_sprite' act='unlink'><a class='unlink' title='Unlink'></a></li> "+linkBrokenCheck+" "+linkDismissCheck+" </ul><div class='editlink_acts'><a class='bl_action save_edit rep_sprite' act='edit_update'>Save</a> <a class='bl_action' act='edit_cancel'>Cancel</a>";
							linkTabs = linkTabs+"<div class='row_updatee' lid='"+link['link_id']+"'><div class='checkbox bulkselect_blinks'></div><div class='link_can_edits'><div class='bl_linktext'><div class='display_linktext'>"+link['link_text']+"</div><div class='edit_bl_linktext'><input type='textbox' value='"+link['link_text']+"'/></div></div><div class='bl_links'><div class='float-left displayurl'>"+link['url']+"</div><div class='droid700 float-left editurl'><input type='textbox' value='"+link['url']+"'/></div></div></div><div class='link_data'><div class='link_stats bl_source'>"+link['source']+"</div><div class='link_stats bl_status'>"+link['status_text']+"</div><div class='link_stats bl_httpcode'>"+link['http_code']+"</div><div class='link_actions'><ul class='link_acts'>"+linkActions+"</div></div></div><div class='clear-both'></div></div>";
						});
						linkTabs = linkTabs+'</div>';
						content_check = content_check || 1;
					}else{
						linkTabs = linkTabs+'<div class="link_type_wrap" style="display:none;"><div class="link_type"><span class="'+filters[i]+'">'+filters[i]+'</span></div></div>';
						content_check = content_check || 0;
					}
				}
				if(content_check){
					content = content+ "<div class='ind_row_cont'><div class='row_summary' sid='"+siteID+"'><div class='row_arrow'></div><div class='checkbox select_all_blc_links'></div><div class='row_name searchable'>"+siteURL+"</div><div class='clear-both'></div></div><div class='row_detailed' sid='"+siteID+"' style='display:none;clear:both;'><div class='rh'><div class='row_arrow'></div><div class='checkbox select_all_blc_links'></div><div class='row_name'>"+siteURL+"</div><div class='link_details'><span class='link_details_text droid700' style='margin-right: 88px;'>Source</span><span class='link_details_text droid700' style='margin-right: 103px;'>Status</span><span class='link_details_text droid700'>HTTP Code</span></div><div class='clear-both'></div></div><div class='rd'>"+linkTabs+"</div></div></div>";
				}
			});
			if(content == ''){
				if(noLinks){
					content = '<div class="gwmt_no_issues">No links found in the selected sites.</div>';
				}else{
					content = '<div class="gwmt_no_issues">No links in the selected sites.</div>';
				}
			}
		}
	}
	$('.rows_cont').html(content);
}
