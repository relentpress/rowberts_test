<?php

/*
slug: brokenLinks
version: 1.0.2
*/


class addonBrokenLinks{
	
	private static $version = '1.0.2';

	public static function version(){
		return self::version;
	}

	public static function init(){
		require_once(APP_ROOT."/addons/brokenLinks/controllers/func.php");	
		require_once(APP_ROOT."/addons/brokenLinks/controllers/manageClientsBrokenLinks.php");	
		panelRequestManager::addFunctions('brokenLinksGetAllLinks','brokenLinksUpdateLink','brokenLinksUndismissLink','brokenLinksBulkActions');
		regHooks('addonMenus','responseProcessors','getStatsRequestParams', 'taskTitleTemplate');
	}	

}