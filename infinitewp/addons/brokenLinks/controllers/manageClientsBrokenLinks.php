<?php
	class manageClientsBrokenLinks
	{
		public static function brokenLinksGetAllLinksProcessor($siteIDs,$params){
			$type = "brokenLinks";
			$action = "getAllLinks";
			$requestAction = "get_all_links";
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => 'blgetAllLinks', 'detailedAction' => 'getAll');
			$events=1;
			foreach ($siteIDs as $siteID) {
				$siteData = getSiteData(intval($siteID));
				$PRP = array();
				$PRP['requestAction'] 	= $requestAction;
				$PRP['siteData'] 		= $siteData;
				$PRP['type'] 			= $type;
				$PRP['action'] 			= $action;
				$PRP['directExecute'] 	= false;
				$PRP['events'] 			= $events;
				$PRP['sendAfterAllLoad'] = true;
				$PRP['historyAdditionalData'] 	= $historyAdditionalData;
				prepareRequestAndAddHistory($PRP);
			}
		}
		
		public static function brokenLinksGetAllLinksResponseProcessor($historyID, $responseData){
			responseDirectErrorHandler($historyID, $responseData);
			
			$response = array();
			if(isset($responseData['success'])){
				$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
				$actionID = $historyData['actionID'];
				$siteID = $historyData['siteID'];
				$response['_'.$siteID] = $responseData['success'];

				DB::insert("?:temp_storage", array('type' => 'brokenLinks', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
				DB::update("?:history_additional_data", array('status'=>'success'), "historyID=".$historyID."");
				return;
			}
		}
		public static function brokenLinksUpdateLinkProcessor($siteID,$params){
			$type = "brokenLinks";
			$action = $params['action'];
			$requestAction = "update_broken_link";
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => 'blupdateLink', 'detailedAction' => 'updateLink');
			$events=1;
			$siteData = getSiteData(intval($siteID));
			$requestParams = array('newLink'=>$params['newlink'],'newText'=>$params['newtext'],'linkID'=>$params['linkid'],'linkType'=>$params['linktype']);
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['directExecute'] 	= false;
			$PRP['events'] 			= $events;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
			prepareRequestAndAddHistory($PRP);
		}
		public static function brokenLinksUpdateLinkResponseProcessor($historyID, $responseData){
			responseDirectErrorHandler($historyID, $responseData);
			
			$response = array();
			if(isset($responseData['success'])){
				$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
				$actionID = $historyData['actionID'];
				$siteID = $historyData['siteID'];
				$response['_'.$siteID] = $responseData['success'];

				DB::insert("?:temp_storage", array('type' => 'brokenLinks', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
				DB::update("?:history_additional_data", array('status'=>'success'), "historyID=".$historyID."");
				return;
			}
		}

		public static function brokenLinksUndismissLinkProcessor($siteID,$params){
			$type = "brokenLinks";
			$action = $params['action'];
			$requestAction = "undismiss_broken_link";
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => 'blundismissLink', 'detailedAction' => 'undismissLink');
			$events=1;
			$siteData = getSiteData(intval($siteID));
			$requestParams = array('linkID'=>$params['linkid'],'linkType'=>$params['linktype']);
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['directExecute'] 	= false;
			$PRP['events'] 			= $events;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
			prepareRequestAndAddHistory($PRP);
		}


		public static function brokenLinksUndismissLinkResponseProcessor($historyID, $responseData){
			responseDirectErrorHandler($historyID, $responseData);
			
			$response = array();
			if(isset($responseData['success'])){
				$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
				$actionID = $historyData['actionID'];
				$siteID = $historyData['siteID'];
				$response['_'.$siteID] = $responseData['success'];

				DB::insert("?:temp_storage", array('type' => 'brokenLinks', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
				DB::update("?:history_additional_data", array('status'=>'success'), "historyID=".$historyID."");
				return;
			}
		}

		public static function brokenLinksBulkActionsProcessor($siteIDs,$params){
			$type = "brokenLinks";
			$action = 'bulkActions';
			$act = substr($params['action'], 5);
			if($act == 'mark_not_broken')	$act = 'markNotBroken';
			$requestAction = "bulk_actions_processor";
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => $act, 'detailedAction' => $act);
			$events=1;
			foreach ($siteIDs as $siteID) {
				$siteData = getSiteData(intval($siteID));
				$requestParams = array('linkData'=>$params['linkData'][$siteID],'action'=>$params['action']);
				$PRP = array();
				$PRP['requestAction'] 	= $requestAction;
				$PRP['siteData'] 		= $siteData;
				$PRP['type'] 			= $type;
				$PRP['action'] 			= $action;
				$PRP['requestParams'] 	= $requestParams;
				$PRP['directExecute'] 	= false;
				$PRP['events'] 			= $events;
				$PRP['sendAfterAllLoad'] = true;
				$PRP['historyAdditionalData'] 	= $historyAdditionalData;
				prepareRequestAndAddHistory($PRP);
			}
		}

		public static function brokenLinksBulkActionsResponseProcessor($historyID, $responseData){
			responseDirectErrorHandler($historyID, $responseData);
			
			$response = array();
			if(isset($responseData['success'])){
				$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
				$actionID = $historyData['actionID'];
				$siteID = $historyData['siteID'];
				$response['_'.$siteID] = $responseData['success'];

				DB::insert("?:temp_storage", array('type' => 'brokenLinks', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
				DB::update("?:history_additional_data", array('status'=>'success'), "historyID=".$historyID."");
				return;
			}
		}
	}
	manageClients::addClass('manageClientsBrokenLinks');