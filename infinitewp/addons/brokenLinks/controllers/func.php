<?php

function brokenLinksAddonMenus(&$menus){
	$menus['monitor']['subMenus'][] = array('page' => 'brokenLinks', 'displayName' => 'Broken Link Checker');
}

function brokenLinksResponseProcessors(&$responseProcessor){
	$responseProcessor['brokenLinks']['getAllLinks'] = 'brokenLinksGetAllLinks';
	$responseProcessor['brokenLinks']['updateLink'] = 'brokenLinksUpdateLink';
	$responseProcessor['brokenLinks']['undismissBroken'] = 'brokenLinksUndismissLink';
	$responseProcessor['brokenLinks']['bulkActions'] = 'brokenLinksBulkActions';
}

function brokenLinksTaskTitleTemplate(&$template){
	$template['brokenLinks']['getAllLinks']['']	= "Getting broken links from <#sitesCount#> site<#sitesCountPlural#>";
	$template['brokenLinks']['updateLink']['']	= "Updating 1 link in <#sitesCount#> site<#sitesCountPlural#>";
	$template['brokenLinks']['undismissBroken']['']	= "Undimissing 1 link in <#sitesCount#> site<#sitesCountPlural#>";
	$template['brokenLinks']['bulkActions']['markNotBroken']	= "Marking link(s) as not broken in <#sitesCount#> site<#sitesCountPlural#>";
	$template['brokenLinks']['bulkActions']['unlink']	= "Unlinking link(s) in <#sitesCount#> site<#sitesCountPlural#>";
	$template['brokenLinks']['bulkActions']['dismiss']	= "Dismissing link(s) in <#sitesCount#> site<#sitesCountPlural#>";
}

function brokenLinksGetAllLinks(){
	$actionID = Reg::get('currentRequest.actionID');
	$linksData = DB::getFields("?:temp_storage", "data", "type = 'brokenLinks' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'brokenLinks' AND paramID = '".$actionID."'");
		
	if(empty($linksData)){
		return array();
	}
	$refinedData = array();

	foreach($linksData as $data){
		$refinedData = array_merge_recursive($refinedData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($refinedData);
	foreach($refinedData as $siteID => $links){
		$siteData = DB::getRow("?:sites","URL","siteID=".$siteID."");
		$refinedData['siteURLs'][] = $siteData['URL'];
		$refinedData['siteIDs'][] = $siteID;
	}

	return $refinedData;

}

function brokenLinksUpdateLink(){
	$actionID = Reg::get('currentRequest.actionID');
		
	$linkData = DB::getFields("?:temp_storage", "data", "type = 'brokenLinks' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'brokenLinks' AND paramID = '".$actionID."'");
		
	if(empty($linkData)){
		return array();
	}
	$refinedData = array();

	foreach($linkData as $data){
		$refinedData = array_merge_recursive($refinedData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($refinedData);
	foreach($refinedData as $siteID => $links){
		$siteData = DB::getRow("?:sites","URL","siteID=".$siteID."");
		$refinedData['siteURL'] = $siteData['URL'];
		$refinedData['siteID'] = $siteID;
	}

	return $refinedData;
}

function brokenLinksUndismissLink(){
	$actionID = Reg::get('currentRequest.actionID');
		
	$linkData = DB::getFields("?:temp_storage", "data", "type = 'brokenLinks' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'brokenLinks' AND paramID = '".$actionID."'");
		
	if(empty($linkData)){
		return array();
	}
	$refinedData = array();

	foreach($linkData as $data){
		$refinedData = array_merge_recursive($refinedData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($refinedData);
	foreach($refinedData as $siteID => $links){
		$siteData = DB::getRow("?:sites","URL","siteID=".$siteID."");
		$refinedData['siteURL'] = $siteData['URL'];
		$refinedData['siteID'] = $siteID;
	}
	return $refinedData;
}

function brokenLinksBulkActions($params){
	$actionID = Reg::get('currentRequest.actionID');
		
	$linkData = DB::getFields("?:temp_storage", "data", "type = 'brokenLinks' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'brokenLinks' AND paramID = '".$actionID."'");
		
	if(empty($linkData)){
		return array();
	}
	$refinedData = array();

	foreach($linkData as $data){
		$refinedData = array_merge_recursive($refinedData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($refinedData);
	foreach($refinedData as $siteID => $links){
		$siteData = DB::getRow("?:sites","URL","siteID=".$siteID."");
	}
	return $refinedData;
}