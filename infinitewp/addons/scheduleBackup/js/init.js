
$(function () {
	
	$(".whenType").live('click',function() {
		$(".whenArgs").hide();
		$("."+$(this).text().toLowerCase()).show();
	});

	$(".selectDate").live('click',function() {
		$(".selectDate").removeClass('active');
		$(this).addClass('active');
	});

	$("#timeSelectBtn").live('click',function() {
		$(".time_select_options").toggle();
		return false;
	});
	$(".time_select_options a").live('click',function() {
		$("#timeSelectBtn a").text($(this).text()+" "+$(this).attr('std')).attr('timeval',$(this).attr('timeval'));
		$(".time_select_options").hide();
		
	});
	$("#normalBackup").live('click',function() { 

		var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['getSitesBackupsHTML']=1;
		doCall(ajaxCallPath,tempArray,'loadBackupPage');
	});
	$("#scheduledBackup").live('click',function() { 

		var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['scheduleBackupGetBackupsHTML']=1;
		doCall(ajaxCallPath,tempArray,'loadScheduleBackupPage');
	});
	$(".scheduleBackupNow").live('click',function() { 
		loadBackup(1,'',1);
	});
	$(".editScheduleBackup").live('click',function() { 
		var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['scheduleBackupEdit']={};
		tempArray['requiredData']['scheduleBackupEdit']['scheduleKey']=$(this).attr("schedulekey");
		
		doCall(ajaxCallPath,tempArray,'loadEditScheduleBackupPage');
		return false;
	});
	$(".removeSchedule").live('click',function() { 

		var closestVar=$(this).closest('.ind_row_cont');
		$(".row_summary",closestVar).addClass('del');
		$(".row_detailed",closestVar).addClass('del');
		$(".row_summary .delConfHide,.row_detailed .rh .delConfHide ",closestVar).hide();
		return false;
		
	});
	$(".run_now").live('click',function() { 
		var tempArray={};
		tempArray['action']='scheduleBackupRunTask';
		tempArray['args']={};
		tempArray['args']['params']={};
		tempArray['args']['params']['scheduleKey']=$(this).attr("schedulekey");
		doHistoryCall(ajaxCallPath,tempArray,'');
		return false;
	});
	$(".cc_schedule_mask").live('click',function() { 

		var closest=$(this).closest(".ind_row_cont");
		var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['scheduleBackupPause']={};
		tempArray['requiredData']['scheduleBackupPause']['scheduleKey']=$(this).attr('schedulekey');
		if($(".cc_schedule_img",this).hasClass('on'))
		tempArray['requiredData']['scheduleBackupPause']['value']=1;
		else
		tempArray['requiredData']['scheduleBackupPause']['value']=0;
		doCall(ajaxCallPath,tempArray,'');
		$(".cc_schedule_img",closest).toggleClass("on off");
		return false;
	});
	$(".restoreScheduleBackup").live('click',function() {
		if($(this).hasClass("needConfirm"))
		{
			loadConfirmationPopup($(this));
			return false;
		}
		var tempArray={};
		tempArray['args']={};
		tempArray['action']='restoreBackup';
		tempArray['args']['params']={};
		tempArray['args']['params']['taskName']=$(this).attr('taskname');
		tempArray['args']['params']['resultID']=$(this).attr('referencekey');
		tempArray['args']['siteIDs']=[$(this).attr('sid')];
		$(this).addClass('disabled');
		$(this).text('Queued..');
		doHistoryCall(ajaxCallPath,tempArray,"");
		return false;
		
		
	});
});