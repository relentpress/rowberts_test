function loadScheduleBackupPage(data)
{
	var content='';
	data=data.data.scheduleBackupGetBackupsHTML;
		content=content+'<div class="site_nav_sub"> <ul> <li><a class="optionSelect" id="normalBackup">Manual</a></li> <li><a class="optionSelect active" id="scheduledBackup">Scheduled</a></li> <div class="clear-both"></div> </ul> </div>';
	content=content+'<div class="result_block backup  shadow_stroke_box siteSearch" id="backupPageMainCont"><div class="th rep_sprite"><div class="type_filter "><input name="" type="text" class="input_type_filter searchItems" value="type to filter"><div class="clear_input rep_sprite_backup"  onclick=""></div></div><div class="btn_action float-right"><a class="rep_sprite scheduleBackupNow" >Create New Backup Schedule</a></div></div><div class="no_match hiddenCont" style="display:none">Bummer, there are no backups that match.<br />Try typing fewer characters.</div>'+data+'</div>';
	$("#pageContent").html(content);
 $(".removeBackup").qtip({content: { text: 'Delete Backup' }, position: { my: 'left center', at: 'right center' }, show: { event: 'mouseenter' }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 5, height:8} } });
  $(".removeSchedule").qtip({content: { text: 'Delete Schedule' }, position: { my: 'left center', at: 'right center' }, show: { event: 'mouseenter' }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 5, height:8} } });

  $(".run_now").qtip({content: { text: 'Run Now' }, position: { my: 'left center', at: 'right center' }, show: { event: 'mouseenter' }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 5, height:8} } });
  /* $(".download").qtip({content: { text: 'Download Backup' }, position: { my: 'right center', at: 'left center' }, show: { event: 'mouseenter' }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 5, height:8} } }); */
  addQTipForDownlaodLinks();
}

function loadEditScheduleBackupPage(data)
{
	data=data.data.scheduleBackupEdit.scheduleData;
	loadBackup(1,"",1,data);
}



function scheduleBackupResponse(data)
{
	var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['scheduleBackupGetBackupsHTML']=1;
	doCall(ajaxCallPath,tempArray,'loadScheduleBackupPage');
}