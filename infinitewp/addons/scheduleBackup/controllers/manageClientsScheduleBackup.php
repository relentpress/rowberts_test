<?php
class manageClientsScheduleBackup{
	
	public static function scheduleBackupProcessor($siteIDs, $params){
			
		$accountInfo = array('account_info' => $params['accountInfo']);
		if((!empty($accountInfo['account_info']['iwp_gdrive'])))
		{
			//$accountInfo['account_info']['iwp_gdrive']['gDriveEmail'] = unserialize(getOption('googleDriveAccessToken'));
			$repoID = $accountInfo['account_info']['iwp_gdrive']['gDriveEmail'];
			if(function_exists('backupRepositorySetGoogleDriveArgs')){
				$accountInfo['account_info']['iwp_gdrive'] = backupRepositorySetGoogleDriveArgs($accountInfo['account_info']['iwp_gdrive']);
			}else{
				addNotification($type='E', $title='Cloud backup Addon Missing', $message="Check if cloud backup addon exists and is active", $state='U', $callbackOnClose='', $callbackReference='');
				return false;
			}
		}
		$config = $params['config'];
	
		if($config['mechanism'] == 'undefined'){ $config['mechanism'] = 'singleCall'; }
		
		self::scheduleBackup($siteIDs, array('accountInfo' => $accountInfo, 'config' => $config, 'scheduleKey' => $params['schedulekey'], 'action' => 'schedule') );
		
	}
		
	  private static function scheduleBackup($siteIDs, $params){
		  		  
		  $accountInfo = $params['accountInfo'];
		  $config = $params['config'];
		  $scheduleKey = $params['scheduleKey'];
		  $timeout = (20 * 60);
		  
		  $type = "scheduleBackup";
		  $action = $params['action'];
	  
		  $requestAction = "scheduled_backup";
		  
		  //$config['taskName'] = $config['backupName'];
		  foreach($params['accountInfo'] as $accInfo => $info){
			  if(!empty($info) && is_array($info)){
				  foreach($info as $accType => $infotype){
					  $info = $infotype;
					  $info['type'] = $accType;
				  }
			  }
		  }
		  
		  $exclude = explode(',', $config['exclude']);
		  $include = explode(',', $config['include']);	
		  array_walk($exclude, 'trimValue');
		  array_walk($include, 'trimValue');
		
		
		  $additionalDatas = array('task_name' => $config['taskName'],'mechanism' => $config['mechanism'], 'args' => array('type' => $type, 'action' => $action, 'what' => $config['what'], 'optimize_tables' => $config['optimizeDB'], 'exclude' => $exclude, 'exclude_file_size' => (int)$config['excludeFileSize'], 'exclude_extensions' => $config['excludeExtensions'], 'include' => $include, 'del_host_file' => $config['delHostFile'], 'disable_comp' => $config['disableCompression'], 'fail_safe_db' => $config['failSafeDB'], 'fail_safe_files' => $config['failSafeFiles'], 'limit' => $config['limit'], 'backup_name' => $config['backupName']), 'secure' => $accountInfo);
				  		  		  
		  $additionalDatas = serialize($additionalDatas);
		   
		   if(empty($scheduleKey)){
			    $scheduleID = DB::insert("?:backup_schedules", array("backupName" => $config['backupName'], "type" => $config['type'], "schedule" => $config['scheduleHR'], "what" => $config['what'], "additionalDatas" => $additionalDatas));
			    $key = md5($scheduleID.$siteID.microtime()); 
				DB::update("?:backup_schedules", "scheduleKey = '".$key."'", "scheduleID = '".$scheduleID."'");
		   }
		   
		   if(!empty($scheduleKey)){
			   $scheduleID = DB::getField("?:backup_schedules", "scheduleID", "scheduleKey = '".$scheduleKey."'");
			   DB::update("?:backup_schedules", array("backupName" => $config['backupName'], "type" => $config['type'], "schedule" => $config['scheduleHR'], "what" => $config['what'], "additionalDatas" => $additionalDatas), "scheduleKey = '".$scheduleKey."'");
			   $key = $scheduleKey;	 
			   DB::delete("?:backup_schedules_link", "scheduleID='".$scheduleID."'");
		   }
			
			$nextSchedule = scheduleBackupScheduleNext($config['type'], $config['scheduleHR']);
			DB::update("?:backup_schedules", array('scheduleStatus'=>'scheduled' ,'nextSchedule'=>$nextSchedule), "scheduleKey='".$key."'");
			
			foreach($siteIDs as $siteID){			
				$scheduled = DB::insert("?:backup_schedules_link", array('siteID' => $siteID ,'scheduleID'=>$scheduleID ,'nextSchedule'=>$nextSchedule, 'status'=>'scheduled'));
			}
			
			return $scheduled;
	}
	
	public static function scheduleBackupResponseProcessor($historyID, $responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		
		if(empty($responseData['success'])){
			return false;
		}
		
		if(!empty($responseData['success']['error'])){
			DB::update("?:history_additional_data", "status='error' ,errorMsg = '".$responseData['success']['error']."'", "historyID=".$historyID);	
			return false;
		}
		
		$args = $responseData['success']['task_args'];
		$historyData = DB::getRow("?:history", "*", "historyID=".$historyID);
		$siteID = DB::getField("?:history", "siteID", "historyID=".$historyID);
		
		$scheduleData = DB::getRow("?:backup_schedules", "*", "scheduleKey='".$args['key']."'");
								
		if(!empty($args) && $historyData['action'] == 'schedule' && !empty($scheduleData)){
			DB::update("?:history_additional_data", "status='success'", "historyID=".$historyID);
			
			if($scheduleData['scheduleKey'] == $args['key']){
				$nextSchedule = scheduleBackupScheduleNext($args['type'], $args['schedule']);
				
				DB::update("?:backup_schedules", array('scheduleStatus'=>'scheduled' ,'nextSchedule'=>$nextSchedule), "scheduleKey='".$args['key']."'");			
				DB::insert("?:backup_schedules_link", array('siteID'=>$args['site_id'] ,'scheduleID'=>$scheduleData['scheduleID'] ,'nextSchedule'=>$nextSchedule, 'status'=>'scheduled'));
			}
		}
		
		//---------------------------post process------------------------>
			
		$allParams = array('action' => 'getStats', 'args' => array('siteIDs' => array($siteID), 'extras' => array('sendAfterAllLoad' => false, 'doNotShowUser' => true)));		
		panelRequestManager::handler($allParams);
	}	
	
	public static function scheduleBackupRunTaskProcessor($siteIDs=array(), $params){
		$type = "scheduleBackup";
		
		$requestAction = "run_task";
		$timeout = (20 * 60);
				
		$siteIDs = DB::getFields("?:backup_schedules as BS, ?:backup_schedules_link as BL", "BL.siteID", "BL.scheduleID=BS.scheduleID AND scheduleKey='".$params['scheduleKey']."'");
		$additionalDatas = DB::getField("?:backup_schedules", "additionalDatas", "scheduleKey='".$params['scheduleKey']."'");
				
		$requestParams = unserialize($additionalDatas);
		$requestParams['task_name'] = $params['scheduleKey'];
		
		$requestParams['mechanism'] = (!empty($params['mechanism']) ? $params['mechanism'] : $requestParams['mechanism']);
		$action = ($requestParams['mechanism'] == 'multiCall') ? "multiCallRunTask" : "runTask";
		
		if($action == "multiCallRunTask")
		{
			//this function set the multicall options value from config.php if available
			setMultiCallOptions($requestParams);
		}
		
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => $params['scheduleKey'], 'detailedAction' => $action);
		
		$incTime = 10 * 60;//10 mins		
		$i = 0;
		$lastHistoryID = '';
		if(empty($params['timeScheduled'])){ $params['timeScheduled'] = time(); }
		foreach($siteIDs as $siteID){
			$siteData = getSiteData($siteID);
			
			$events=1;
			
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
			$PRP['timeout'] 		= $timeout;
			$PRP['status'] 			= 'pending';
			

			$PRP['timeScheduled'] = $params['timeScheduled'];
			
						
			if($lastHistoryID){
				$runCondition = 	array();
				$runCondition['satisfyType'] = 'OR';
				$runCondition['query'] = array('table' => "history_additional_data", 'select' => 'historyID', 'where' => "historyID = ".$lastHistoryID." AND status IN('success', 'error', 'netError')");
				//$runCondition['maxWaitTime'] = $params['timeScheduled'] + $incTime * $i;
				$PRP['runCondition'] = serialize($runCondition);
				$PRP['status'] = 'scheduled';
			}
				
			$lastHistoryID = prepareRequestAndAddHistory($PRP);
			$i++;
		}
	}
	
	public static function scheduleBackupRunTaskResponseProcessor($historyID, $responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		
		if(empty($responseData['success'])){
			return false;
		}
		
		$historyData = DB::getRow("?:history", "*", "historyID=".$historyID);
		$siteID = $historyData['siteID'];
		
		if(!empty($responseData['success']['error']) && is_string($responseData['success']['error'])){		
			DB::update("?:history_additional_data", array('status' => 'error', 'errorMsg' => $responseData['success']['error']), "historyID=".$historyID);	
			return false;
		}
		
		else{
			if($historyData['type'] == 'scheduleBackup' && $historyData['action'] == 'multiCallRunTask'){
				$historyResponseStatus[$historyID] = "multiCallWaiting";
				Reg::set("historyResponseStatus", $historyResponseStatus);
				
				updateHistory(array('status' => "multiCallWaiting"), $historyID);
				manageClientsBackup::triggerRecheck($responseData, $siteID);
				
			}else{
			
				DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID);

				//---------------------------post process------------------------>
				$siteID = DB::getField("?:history", "siteID", "historyID=".$historyID);
			
				$allParams = array('action' => 'getStats', 'args' => array('siteIDs' => array($siteID), 'extras' => array('sendAfterAllLoad' => false, 'doNotShowUser' => true)));
				
				panelRequestManager::handler($allParams);
			}
		}	
	}
	
	public static function scheduleBackupDeleteTaskProcessor($siteIDs=array(), $params){
		
		$type = "scheduleBackup";
		$action = "deleteTask";
		$requestAction = "delete_schedule_task";
		$timeout = (20 * 60);
				
		$schedule = DB::getRow("?:backup_schedules", "scheduleID,backupName", "scheduleKey='".$params['scheduleKey']."'");
		$sites = DB::getArray("?:backup_schedules_link", "siteID", "scheduleID='".$schedule['scheduleID']."'");
		
		$requestParams = array('task_name' => $params['scheduleKey']);
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => $schedule['backupName'], 'detailedAction' => $action);
		
		//if(empty($sites)){
			$scheduleID = DB::getField("?:backup_schedules", "scheduleID", "scheduleKey='".$params['scheduleKey']."'");
			DB::delete("?:backup_schedules_link", "scheduleID='".$scheduleID."'");
			DB::delete("?:backup_schedules", "scheduleKey='".$params['scheduleKey']."'");
		//}
		
		foreach($sites as $site){
			$sIDs[] = $site['siteID'];
		}
		
		foreach($sIDs as $siteID){
			$siteData = getSiteData($siteID);		
			
			$events=1;
			
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
			$PRP['timeout'] 		= $timeout;
			
			if($params['scheduleType'] == 'cron'){
				$PRP['scheduleTime'] 		= $params['scheduleTime'];
			}
				
			prepareRequestAndAddHistory($PRP);
		}
	}
	
	public static function scheduleBackupDeleteTaskResponseProcessor($historyID, $responseData){
		
		if(!empty($responseData['error'])){
			DB::update("?:history_additional_data", "status='error' ,errorMsg = '".$responseData['success']['error']."'", "historyID=".$historyID);	
		}
		
		if(!empty($responseData['success'])){
			DB::update("?:history_additional_data", "status='success'", "historyID=".$historyID);
			$scheduleID = DB::getField("?:backup_schedules", "scheduleID", "scheduleKey='".$responseData['success']."'");
			DB::delete("?:backup_schedules_link", "scheduleID='".$scheduleID."'");
			DB::delete("?:backup_schedules", "scheduleKey='".$responseData['success']."'");
		}
		//---------------------------post process------------------------>
		$siteID = DB::getField("?:history", "siteID", "historyID=".$historyID);	
		$allParams = array('action' => 'getStats', 'args' => array('siteIDs' => array($siteID), 'extras' => array('sendAfterAllLoad' => false, 'doNotShowUser' => true)));		
		panelRequestManager::handler($allParams);
	}	
}

manageClients::addClass('manageClientsScheduleBackup');
?>