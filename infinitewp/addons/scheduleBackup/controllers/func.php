<?php

function scheduleBackupGetNextSchedule(&$nextScheduleTime){

	$nextTime = DB::getField("?:backup_schedules", "MIN(nextSchedule)", "paused='0' ");
	if(!empty($nextTime)){
		$nextScheduleTime[] = $nextTime;
	}
}

function scheduleBackupCreate($datas){
	
	$result = manageClientsScheduleBackup::scheduleBackupProcessor($datas['siteIDs'], $datas['params']);
	
	if($result){
		return true;
	}
	
	return false;
}

function scheduleBackupCronRun(){
	$settings = DB::getArray("?:backup_schedules", "*", "nextSchedule <= '".time()."' AND paused='0'");
	if(!empty($settings)){
		foreach($settings as $key => $value){
			$sites = DB::getFields("?:backup_schedules_link", "siteID", "scheduleID='".$value['scheduleID']."'");
			if(empty($sites)){ continue; }
			
			//----------------------------------------------------decide a task need to run now or not---------------------------------------------------------
			$tempAdditionalDatas = unserialize($value['additionalDatas']);
			$mechanism = $tempAdditionalDatas['mechanism'];

			if(CRON_MODE == 'easyCronTask' && $mechanism == 'singleCall'){
				if(getSystemCronRunningFrequency() == 0){
					$mechanism = 'multiCall';
					addNotification($type='E', $title='Schedule Backup Method Changed', '<strong>'.$value['backupName'].'</strong> schedule task method has been temporarily changed to MultiCall because system cron was not trigger for last 90 mins.', $state='U');//FINALISE ERROR MESSAGE
				}
				else{
					continue;//systemCron is active, lets wait for it to trigger this singleCall Backup
				}
			}
			elseif(CRON_MODE == 'systemCronDefault' && $mechanism == 'multiCall'){
				if(!manageEasyCron::isActive()){
					$mechanism = 'singleCall';
					addNotification($type='E', $title='Schedule Backup Method Changed', '<strong>'.$value['backupName'].'</strong> schedule task method has been temporarily changed to old method because easyCron was not active.', $state='U');//FINALISE ERROR MESSAGE
				}
				else{
					continue;//easyCron is active, lets wait for it to trigger this multiCall Backup
				}
			}
			//----------------------------------------------------decide a task need to run now or not------------ENDS HERE-------------------------------
			
			$next = scheduleBackupScheduleNext($value['type'], $value['schedule']);	
			DB::update("?:backup_schedules", array('nextSchedule' => $next), "scheduleID='".$value['scheduleID']."' AND scheduleKey = '".$value['scheduleKey']."'");
			
			$data = array(	'action' => 'scheduleBackupRunTask', 
							'args' => array(
								'params' => array('backupName' => $value['backupName'], 'scheduleKey' => $value['scheduleKey'], 'timeScheduled' => $value['nextSchedule'], 'scheduleType' => 'cron', 'mechanism' => $mechanism), 
								'siteIDs' => $sites
							)
						);
			panelRequestManager::handler($data);
		}
	}
}

function scheduleBackupPause($params){ //panelRequestManager
	if(empty($params)){
			return false;
	}
	else{
		DB::update("?:backup_schedules", array('paused' => $params['value']), "scheduleKey = '".$params['scheduleKey']."'");
	}
}

function scheduleBackupEdit($params){
	if(empty($params)){
		return false;
	}
	else{
		$data = array();
		$data['scheduleData'] = DB::getRow("?:backup_schedules", "scheduleID, backupName, type, schedule, scheduleKey, additionalDatas, paused, repository", "scheduleKey='".$params['scheduleKey']."'");
		$data['scheduleData']['additionalDatas'] = unserialize($data['scheduleData']['additionalDatas']);
		
		$data['scheduleData']['additionalDatas']['args']['exclude'] = @implode(',', $data['scheduleData']['additionalDatas']['args']['exclude']);
		$data['scheduleData']['additionalDatas']['args']['include'] = @implode(',', $data['scheduleData']['additionalDatas']['args']['include']);

		$data['scheduleData']['siteName']['siteIDs'] = $siteIDs = DB::getFields("?:backup_schedules_link", "siteID", "scheduleID='".$data['scheduleData']['scheduleID']."'");
	
		if($data['scheduleData']['type'] == 'daily'){
			$data['scheduleData']['time'] = $data['scheduleData']['schedule'];
			unset($data['scheduleData']['schedule']);
		}
		
		if($data['scheduleData']['type'] == 'weekly' || $data['scheduleData']['type'] == 'monthly'){
			$a = explode("|", $data['scheduleData']['schedule']);
			$data['scheduleData']['time'] = $a[0];
			$data['scheduleData']['day'] = $a[1];
			unset($data['scheduleData']['schedule']);		
		}
				
		foreach($siteIDs as $siteID){
			Reg::tplSet('sitesData', panelRequestManager::getSites());
            $sitesData = Reg::tplGet('sitesData'); 
			$data['scheduleData']['siteName'][$siteID] = $sitesData[$siteID]['name'];
		}
	}
	return $data;
}

function scheduleBackupScheduleNext($type, $schedule){
	$schedule = explode("|", $schedule);
	if (empty($schedule))
		return false;
	switch ($type) {
		
		case 'daily':
			
			if (isset($schedule[1]) && $schedule[1]) {
				$delay_time = $schedule[1] * 60;
			}
			
			$current_hour  = date("H");
			$schedule_hour = $schedule[0];
			if ($current_hour >= $schedule_hour){
				$time = mktime($schedule_hour, 0, 0, date("m"), date("d") + 1, date("Y"));
			}
			else{
				$time = mktime($schedule_hour, 0, 0, date("m"), date("d"), date("Y"));
			}
					
			break;
		
		
		case 'weekly':
			if (isset($schedule[2]) && $schedule[2]) {
				$delay_time = $schedule[2] * 60;
			}
			$current_weekday  = date('w');
			$schedule_weekday = $schedule[1];
			$current_hour     = date("H");
			$schedule_hour    = $schedule[0];
			
			if ($current_weekday > $schedule_weekday)
				$weekday_offset = 7 - ($current_weekday - $schedule_weekday);
			else
				$weekday_offset = $schedule_weekday - $current_weekday;
			
			
			if (!$weekday_offset) { //today is scheduled weekday
				if ($current_hour >= $schedule_hour)
					$time = mktime($schedule_hour, 0, 0, date("m"), date("d") + 7, date("Y"));
				else
					$time = mktime($schedule_hour, 0, 0, date("m"), date("d"), date("Y"));
			} else {
				$time = mktime($schedule_hour, 0, 0, date("m"), date("d") + $weekday_offset, date("Y"));
			}
			
			break;
		
		case 'monthly':
			if (isset($schedule[2]) && $schedule[2]) {
				$delay_time = $schedule[2] * 60;
			}
			$current_monthday  = date('j');
			$schedule_monthday = $schedule[1];
			$current_hour      = date("H");
			$schedule_hour     = $schedule[0];
			
			if ($current_monthday > $schedule_monthday) {
				$time = mktime($schedule_hour, 0, 0, date("m") + 1, $schedule_monthday, date("Y"));
			} else if ($current_monthday < $schedule_monthday) {
				$time = mktime($schedule_hour, 0, 0, date("m"), $schedule_monthday, date("Y"));
			} else if ($current_monthday == $schedule_monthday) {
				if ($current_hour >= $schedule_hour)
					$time = mktime($schedule_hour, 0, 0, date("m") + 1, $schedule_monthday, date("Y"));
				else
					$time = mktime($schedule_hour, 0, 0, date("m"), $schedule_monthday, date("Y"));
				break;
			}
			
			break;
		default:
			break;
	}
	
	if (isset($delay_time) && $delay_time) {
		$time += $delay_time;
	}
	
	return $time;
}


function scheduleBackupGetBackups(){
		
	$schedules = DB::getArray("?:backup_schedules", "*","1");
	if(!empty($schedules)){						
		foreach($schedules as $key => $value){
			$sites = DB::getArray("?:backup_schedules_link", "siteID", "scheduleID='".$value['scheduleID']."'");
			foreach($sites as $site){
				$sIDs[] = $site['siteID'];
			}
			$backupNames[] = $value['scheduleKey'];
		}
	}
		if(!empty($sIDs))
		$siteIDs = array_unique($sIDs);
		$sitesStats = panelRequestManager::getRawSitesStats($siteIDs);	
		$sitesBackups = array();
			
		foreach($sitesStats as $siteID => $siteStats){
			if(!empty($backupNames))

			foreach($backupNames as $backupName){
			
				$sitesBackups[$backupName]['data'] = DB::getRow("?:backup_schedules", "*","scheduleKey='".$backupName."'");
				$sitesBackups[$backupName]['data']['count'] = $count= DB::getField("?:backup_schedules_link", "count(DISTINCT siteID)", "scheduleID='".$sitesBackups[$backupName]['data']['scheduleID']."'");
				
				if(empty($siteStats['stats']['iwp_backups'][$backupName])) { continue; }
				
				$siteBackupsTemp = $siteStats['stats']['iwp_backups'][$backupName];
				$siteBackups[$siteID] = array();
				krsort( $siteBackupsTemp );
				
				foreach($siteBackupsTemp as $referenceKey => $siteBackupTemp){
					if(!empty($siteBackupTemp['error'])){ continue; }
					
					$otherParts = '';
					if(empty($siteBackupTemp['server']['file_url']) && !empty($siteBackupTemp['ftp'])){
						$otherParts = $siteBackupTemp['ftp'];
					}
					if(empty($siteBackupTemp['server']['file_url']) && !empty($siteBackupTemp['amazons3'])){
						$otherParts = $siteBackupTemp['amazons3'];
					}
					if(empty($siteBackupTemp['server']['file_url']) && !empty($siteBackupTemp['dropbox'])){
						$otherParts = $siteBackupTemp['dropbox'];
					}
					if(empty($siteBackupTemp['server']['file_url']) && !empty($siteBackupTemp['gDriveOrgFileName'])){
						$otherParts = $siteBackupTemp['gDriveOrgFileName'];
					}
					
					if((is_array($siteBackupTemp['server']['file_url']))||(is_array($otherParts)))
					{
						$fileURLParts = explode('/', $siteBackupTemp['server']['file_url'][0] ? $siteBackupTemp['server']['file_url'][0] : $otherParts[0]);
					}
					else
					{
						$fileURLParts = explode('/', $siteBackupTemp['server']['file_url'] ? $siteBackupTemp['server']['file_url'] : $otherParts);
					}
					
					$fileName = array_pop($fileURLParts);
					$fileNameParts = explode('_', $fileName);
					$what = $fileNameParts[2];
					
					$repo = '';
					if(array_key_exists('server', $siteBackupTemp)){
						$repo = "Server";
					}
					if(array_key_exists('ftp', $siteBackupTemp)){
						$repo = "FTP";
					}
					if(array_key_exists('amazons3', $siteBackupTemp)){
						$repo = "Amazon S3";
					}
					if(array_key_exists('dropbox', $siteBackupTemp)){
						$repo = "Dropbox";
					}
					if(array_key_exists('gDriveOrgFileName', $siteBackupTemp)){
						$repo = "G Drive";
					}
					
					$sitesBackups[$backupName]['data']['repo'] = $repo;
					$sitesBackups[$backupName]['sites'][$siteID][$referenceKey] = array('backupName' => $backupName,
																						'sitesCount' => $count,
																						'type' => $value['type'],
																						'schedule' => $value['schedule'],
																						'what' => $what,
																						'time' => $siteBackupTemp['time'],
																						'downloadURL' => $siteBackupTemp['server']['file_url'],
																						'size' => $siteBackupTemp['size'],
																						'referenceKey' => $referenceKey,
																						'backupName' => $value['backupName'],
																						'siteID' => $siteID,
																						'repo' => $repo);	
																	
				}
			}
		}
	
	return $sitesBackups;
	
}
	
function scheduleBackupGetBackupsHTML(){
	$scheduleBackups = scheduleBackupGetBackups();
	$HTML = TPL::get('/addons/scheduleBackup/templates/view.tpl.php', array('scheduleBackups' => $scheduleBackups));
	return $HTML;
}

function scheduleBackupResponseProcessors(&$responseProcessor){
	$responseProcessor['scheduleBackup']['schedule'] = 'scheduleBackup';
	$responseProcessor['scheduleBackup']['runTask'] = 'scheduleBackupRunTask';
	$responseProcessor['scheduleBackup']['multiCallRunTask'] = 'scheduleBackupRunTask';
	$responseProcessor['scheduleBackup']['deleteTask'] = 'scheduleBackupDeleteTask';
}

function scheduleBackupFailedNotification($historyID){
	//to show notification if it run by cron then offline notification	
	$HAD = DB::getRow("?:history_additional_data", "*", "historyID=".$historyID);
	if($HAD['status'] == 'error' || $HAD['status'] == 'netError'){
		
		$backupTask = DB::getRow("?:backup_schedules", "*", "scheduleKey = '".$HAD['uniqueName']."'");
		$siteID = DB::getField("?:history", "siteID", "historyID=".$historyID);
		$siteData = getSiteData($siteID);
		
		$hr = '';
		if($backupTask['schedule'] == 0){
			$hr = '12 am';
		}
		elseif($backupTask['schedule'] >= 12){
			if($backupTask['schedule'] > 12){
				$hr = ($backupTask['schedule'] - 11).' pm';
			}
			else{
				$hr = '12 pm';
			}
		}
		else{
			$hr = $backupTask['schedule'].' am';
		}
		
		$message = $backupTask['backupName'].' '.$backupTask['type'].' @'.$hr.' Error:'.$HAD['errorMsg'].' for '.$siteData['name'];
		addNotification($type='E', $title='Schedule Backup Failed', $message, $state='U', $callbackOnClose='', $callbackReference='');
	}
}

function scheduleBackupTaskTitleTemplate(&$template){
	$template['scheduleBackup']['schedule']['']	= "Schedule backup in <#sitesCount#> site<#sitesCountPlural#>";
	$template['scheduleBackup']['runTask']['']	= "Run backup schedule in <#sitesCount#> site<#sitesCountPlural#>";
	$template['scheduleBackup']['deleteTask']['']	= "Delete backup schedule with <#sitesCount#> site<#sitesCountPlural#>";
	$template['scheduleBackup']['multiCallRunTask'][''] = "Run backup schedule in <#sitesCount#> site<#sitesCountPlural#>";
}

function scheduleBackupRemoveSite($siteID){
	
	$scheduleIDs = DB::getFields("?:backup_schedules_link", "scheduleID", "siteID = '".$siteID."'");			
	DB::delete("?:backup_schedules_link", "siteID = '".$siteID."'" );
	
	if(!empty($scheduleIDs)){
		foreach($scheduleIDs as $scheduleID){
			$scheduleIDExists = DB::getExists("?:backup_schedules_link", "scheduleID", "scheduleID = '".$scheduleID."'");
			if(!$scheduleIDExists){
				DB::delete("?:backup_schedules", "scheduleID = '".$scheduleID."'" );
			}
		}
	}
}

function scheduleBackupCronRequiredAlert($requiredFor){
	$requiredFor[] = 'Schedule Backups';
}

?>