<?php  
Reg::tplSet('sitesData', panelRequestManager::getSites());
$sitesData = Reg::tplGet('sitesData'); 


if(!empty($d['scheduleBackups'])){ ?>
<div class="rows_cont">
<?php foreach($d['scheduleBackups'] as $backupName => $taskData){ ?>
 <div class="ind_row_cont">
	<div class="row_summary">
	<?php TPL::captureStart('sitesBackupsRowSummary'); ?>
	<div class="row_arrow"></div>
	<div class="row_checkbox on_off"> 
		<?php if($taskData['data']['paused'] == '0') { ?>
        <div class="cc_mask cc_schedule_mask" schedulekey="<?php echo $taskData['data']['scheduleKey']; ?>"><div class="cc_img cc_schedule_img on"></div></div>
        <?php } if($taskData['data']['paused'] == '1') { ?>
        <div class="cc_mask cc_schedule_mask" schedulekey="<?php echo $taskData['data']['scheduleKey']; ?>"><div class="cc_img cc_schedule_img off"></div></div>
        <?php } ?>
	  </div>
	<div class="row_backup_action rep_sprite"><a class="run_now rep_sprite_backup" schedulekey="<?php echo $taskData['data']['scheduleKey']; ?>"></a></div>
	<div class="row_name searchable"><?php echo $taskData['data']['backupName']; ?></div>
	<div class="row_backup_action rep_sprite" style="float:right;"><a class="trash rep_sprite_backup removeSchedule" taskName="<?php echo $taskData['data']['scheduleKey']; ?>"></a>
	 <div class="del_conf" style="display:none;">
			  <div class="label">Sure?</div>
			  <div class="yes deleteBackup scheduleDelete" schedulekey="<?php echo $taskData['data']['scheduleKey']; ?>">Yes</div>
			  <div class="no deleteBackup scheduleDelete">No</div>
			</div>
			</div>
	<div class="row_action float-left"><a class="editScheduleBackup" schedulekey="<?php echo $taskData['data']['scheduleKey']; ?>">Edit</a></div>
	<div class="files_db_bu rep_sprite_backup delConfHide"><?php if($taskData['data']['what'] == 'full'){ ?>Files + DB<?php } elseif($taskData['data']['what'] == 'db'){ ?>DB<?php } elseif($taskData['data']['what'] == 'files'){ ?> Files <?php } ?></div>
	<div class="time_bu rep_sprite_backup delConfHide"><?php echo ucfirst($taskData['data']['type']); ?></div>
	<div class="sites_bu rep_sprite_backup delConfHide"><?php echo $taskData['data']['count']; ?></div>
	<div class="clear-both"></div>
	<?php TPL::captureStop('sitesBackupsRowSummary'); ?>
	<?php echo TPL::captureGet('sitesBackupsRowSummary'); ?>
  </div> 
  
  <div class="row_detailed" style="display:none;">
	<div class="rh">
	<?php echo TPL::captureGet('sitesBackupsRowSummary'); ?>
	  </div>
		 <div class="rd">
		<div class="row_updatee">
        
  <?php 
  if(!empty($taskData['sites']))
  		foreach($taskData['sites'] as $siteID => $refKey){ ?>
	 
		<div class="row_updatee_ind">
			<div class="label_updatee float-left">
			<div class="label droid700 float-right"><?php echo $sitesData[$siteID]['name'] ?></div>
		  </div>
			<div class="items_cont float-left">
			<?php foreach($refKey as $key => $datas) { ?>
			<div class="item_ind float-left  topBackup">
				<div class="rep_sprite_backup stats repository delConfHide"><?php echo $datas['repo']; ?></div>
                <div class="rep_sprite_backup stats files delConfHide"><?php if($datas['what'] == 'full'){ ?>Files + DB<?php } elseif($datas['what'] == 'db'){ ?>DB<?php } else { ?> Files <?php } ?></div>
				<div class="rep_sprite_backup stats size delConfHide"><?php echo $datas['size']; ?></div>
				<div class="rep_sprite_backup stats time delConfHide"><?php echo @date(Reg::get('dateFormatLong'), $datas['time']); ?></div>
				
			  <div class="row_backup_action rep_sprite" style="float:right;"><a class="trash rep_sprite_backup removeBackup" sid="<?php echo $datas['siteID']; ?>" taskName="<?php echo $taskData['data']['scheduleKey']; ?>" referencekey="<?php echo $datas['referenceKey']; ?>"></a>
				<div class="del_conf" style="display:none;">
			  <div class="label">Sure?</div>
			  <div class="yes deleteBackup" schedulekey="<?php echo $taskData['data']['scheduleKey']; ?>" >Yes</div>
			  <div class="no deleteBackup">No</div>
			</div></div>
				
				<?php if(!empty($datas['downloadURL'])){ if(!is_array($datas['downloadURL'])) {$urlArray = array(); $urlArray[] = $datas['downloadURL'];} else { $urlArray = $datas['downloadURL']; } $urlIndex = count($urlArray)-1;  ?><div class="row_backup_action rep_sprite delConfHide" style="float:right;"><a class="download rep_sprite_backup" href="<?php echo $urlArray[$urlIndex]; ?>"></a></div> <?php  } ?>
				<div class="row_action float-left delConfHide"><a class="restoreScheduleBackup needConfirm" sid="<?php echo $siteID; ?>" taskName="<?php echo $taskData['data']['scheduleKey']; ?>" referencekey="<?php echo $datas['referenceKey']; ?>">Restore</a></div>
			  </div>
			  <?php } ?>
		  </div>
			<div class="clear-both"></div>
		  </div>
		  
<?php  }?></div>
	  </div></div></div><!--ind_row_cont Ends-->
<?php }?>
	
  </div>

<?php }//End if
else{ ?>
<div class="empty_data_set"><div class="line2">Looks like there are <span class="droid700">no backups scheduled now</span>. Create a <a class="scheduleBackupNow">Backup Schedule</a>.</div></div>
<script>$(".searchItems","#backupPageMainCont").hide();</script>
<?php } ?>
