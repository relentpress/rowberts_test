<?php

/*
slug: scheduleBackup
version: 1.3.1
*/

class addonScheduleBackup{
	
	private static $version = '1.3.1';
	
	public static function version(){
		return self::version;	
	}
	
	public static function init(){	
		require_once(APP_ROOT."/addons/scheduleBackup/controllers/func.php");
		require_once(APP_ROOT."/addons/scheduleBackup/controllers/manageClientsScheduleBackup.php");	
		panelRequestManager::addFunctions('scheduleBackupPause', 'scheduleBackupEdit', 'scheduleBackupGetBackupsHTML', 'scheduleBackupCreate');
		regHooks('responseProcessors', 'cronRun', 'taskTitleTemplate', 'removeSite', 'cronRequiredAlert', 'getNextSchedule');
	}
	
	
	public static function install(){
		
		$Q1 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:backup_schedules` (
  `scheduleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `backupName` varchar(50) NOT NULL,
  `type` varchar(15) NOT NULL,
  `schedule` varchar(15) NOT NULL,
  `scheduleKey` varchar(255) NOT NULL DEFAULT '',
  `additionalDatas` text,
  `nextSchedule` bigint(20) unsigned DEFAULT NULL,
  `scheduleStatus` varchar(50) NOT NULL DEFAULT '',
  `lastRun` int(10) unsigned DEFAULT NULL,
  `paused` enum('0','1') NOT NULL DEFAULT '0',
  `what` varchar(25) NOT NULL DEFAULT '',
  `repository` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`scheduleID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");

	$Q2 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:backup_schedules_link` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(10) unsigned NOT NULL,
  `scheduleID` int(10) unsigned DEFAULT NULL,
  `nextSchedule` bigint(20) DEFAULT NULL,
  `delayedTime` int(10) unsigned DEFAULT '1',
  `finalTime` bigint(20) DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");
		if($Q1 && $Q2){
			return true;	
		}
		return false;
	}
	
	public static function update(){
		
		if(version_compare(getOldAddonVersion('scheduleBackup'), '1.0.4') === -1){
			echo '<br><br><div style="font-weight:bold;">Please reschedule all your scheduled backups.</div>';
		}
		if(version_compare(getOldAddonVersion('scheduleBackup'), '1.3.0') === -1){
			$ok = true;
			$backupSchedules = DB::getArray("?:backup_schedules", "scheduleID, additionalDatas", "1");
			if(!empty($backupSchedules)){
				foreach($backupSchedules as $backupSchedule){
					$additionalDatas = unserialize($backupSchedule['additionalDatas']);
					$additionalDatas['mechanism'] = 'singleCall';
					$isUpdated = DB::update("?:backup_schedules", array('additionalDatas' => serialize($additionalDatas)), "scheduleID = ".$backupSchedule['scheduleID']."");
					if(!$isUpdated) $ok = false;
				}
			}
			if(!$ok) return false;
		}
		if(version_compare(getOldAddonVersion('scheduleBackup'), '1.3.0beta3') === -1){
			DB::update("?:history", array('status' => 'error', 'error' => 'bugFix'), "type = 'scheduleBackup' AND action = 'runTask' AND status = 'multiCallWaiting'");//this where condition shouldn't hv occured, so fixing it, by marking it as error
		}
		if(version_compare(getOldAddonVersion('scheduleBackup'), '1.3.0beta5') === -1){
			
			$schedules = DB::getArray("?:backup_schedules", "additionalDatas, scheduleID", 1);
			
			if(!empty($schedules)){
			
				foreach($schedules as $key => $backupDatas){
				$isChange = false;
				
				$additionalDatas = unserialize($backupDatas['additionalDatas']);
				
				if(isset($additionalDatas['args']) && $additionalDatas['mechanism'] == 'undefined'){
					$additionalDatas['mechanism'] = "singleCall";
					$isChange = true;
				}
				if(!isset($additionalDatas['args'])){
					
					
					 $exclude = explode(',', $additionalDatas['exclude']);
					 $include = explode(',', $additionalDatas['include']);	
					 array_walk($exclude, 'trimValue');
					 array_walk($include, 'trimValue');
					 
					 /*$additionalDatas['exclude'] = $exclude;
					 $additionalDatas['include'] = $include;
					 
					 $additionalDatas['secure'] = array('accountInfo' => array($additionalDatas['accountInfo']['type'] => $additionalDatas['accountInfo']));
					 unset($additionalDatas['accountInfo']);
					 
					 $mechanism = ($additionalDatas['mechanism'] == 'undefined' || empty($additionalDatas['mechanism'])) ? 'singleCall' : $additionalDatas['mechanism'];
					 
					 $additionalDatas = array('task_name' => $additionalDatas['taskName'], 'mechanism' => $mechanism, 'args' => $additionalDatas, 'secure' => $additionalDatas['secure']);
					 unset($additionalDatas['args']['secure']);*/
					 
					 $mechanism = ($additionalDatas['mechanism'] == 'undefined' || empty($additionalDatas['mechanism'])) ? 'singleCall' : $additionalDatas['mechanism'];
					 
					 $additionalDatas['args']['type'] = 'scheduleBackup';
					 $additionalDatas['args']['action'] = 'schedule';
					 
					 $additionalDatas['secure'] = array('account_info' => array($additionalDatas['accountInfo']['type'] => $additionalDatas['accountInfo']));
					 unset($additionalDatas['accountInfo']);
					 
					 $additionalDatas = array('task_name' => $additionalDatas['taskName'],'mechanism' => $mechanism, 'args' => array('type' => 'scheduleBackup', 'action' => 'schedule', 'what' => $additionalDatas['what'], 'optimize_tables' => $additionalDatas['optimizeDB'], 'exclude' => $exclude, 'include' => $include, 'del_host_file' => $additionalDatas['delHostFile'], 'disable_comp' => $additionalDatas['disableCompression'], 'fail_safe_db' => $additionalDatas['failSafeDB'], 'fail_safe_files' => $additionalDatas['failSafeFiles'], 'limit' => $additionalDatas['limit'], 'backup_name' => $additionalDatas['backupName']), 'secure' => $additionalDatas['secure']);
					 
					 $isChange = true;
					 
				}
				
				if($isChange){
					DB::update("?:backup_schedules", array('additionalDatas' => serialize($additionalDatas)), "scheduleID = '".$backupDatas['scheduleID']."' ");
				}
			}
			
			}
		}
		
		if(version_compare(getOldAddonVersion('scheduleBackup'), '1.3.0') === -1){
			
			$schedules = DB::getArray("?:backup_schedules", "additionalDatas, scheduleID", 1);

			if(!empty($schedules)){

				foreach($schedules as $key => $backupDatas){
				
					$isChange = false;

					$additionalDatas = unserialize($backupDatas['additionalDatas']);
					
					if(!empty($additionalDatas['secure']['account_info']))
					{
						foreach($additionalDatas['secure']['account_info'] as $key => $value){
							if(empty($key) || empty($value))
							{
								$additionalDatas['secure']['account_info'] = null;
							}
							$isChange = true;
						}
					}
					
					if($isChange){
						DB::update("?:backup_schedules", array('additionalDatas' => serialize($additionalDatas)), "scheduleID = '".$backupDatas['scheduleID']."' ");
					}
				}
			
			}
		}
		return true;
	}
}



?>