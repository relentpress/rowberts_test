<?php

/*
slug: googleWebMasters
version: 1.0.7
*/


class addonGoogleWebMasters{
	
	private static $version = '1.0.7';

	public static function version(){
		return self::version;
	}

	public static function init(){
		require_once(APP_ROOT."/addons/googleWebMasters/controllers/func.php");	
		require_once(APP_ROOT."/addons/googleWebMasters/controllers/manageClientsGoogleWebMasters.php");	
		panelRequestManager::addFunctions('googleWebMastersActiveSites','googleWebMastersSaveAPIKeysAndRedirect','googleWebMastersGrantAccess','googleWebMastersGetAPIKeys','googleWebMastersMappedSites','googleWebMastersEditSiteOptions','googleWebMastersInstallPopulateSites','googleWebMastersFetchProfiles','googleWebMastersSetProfilesAndSites','googleWebMastersGetProfileData','googleWebMastersIgnoreValue','googleWebMastersRedirect','googleWebMastersBulkIgnores','googleWebMastersRedirectAgain','googleWebMastersRedirectedAgain','googleWebMastersReconsiderIssue');
		regHooks('addonMenus','responseProcessors','postAddSite', 'runOffBrowserLoad', 'taskTitleTemplate');
	}	

	public static function install(){
		
		$Q1 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:google_webmasters_ignored` (
			  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `crawl` varchar(80) NOT NULL,
			  `issue` varchar(255) NOT NULL,
			  `redirect` varchar(255) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

		$Q2 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:google_webmasters_profiles` (
			  `gwtSiteURL` varchar(255) NOT NULL,
			  `gwtProfileID` varchar(70) DEFAULT NULL,
			  UNIQUE KEY `ga_profileID` (`gwtProfileID`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

		$Q3 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:google_webmasters_profiles_sites` (
			  `siteID` int(10) unsigned DEFAULT NULL,
			  `gwtProfileID` varchar(70) DEFAULT NULL,
			  UNIQUE KEY `siteID` (`siteID`,`gwtProfileID`),
			  UNIQUE KEY `siteID_2` (`siteID`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

		if($Q1 && $Q2 && $Q3){
			return true;	
		}
		return false;
	}

	public static function update(){
		
		if(version_compare(getOldAddonVersion('googleWebMasters'), '1.0.0') === -1){
			//from v1.0.0 Google API Lib is kept common in base app, so deleting common API files here
			
			if(!is_object($GLOBALS['FileSystemObj'])){
				if(!initFileSystem()){
					appUpdateMsg('Unable to initiate file system.', true);
					exit;
				}
			}
			
			$panelLocation = $GLOBALS['FileSystemObj']->findFolder(APP_ROOT);
			$panelLocation  = removeTrailingSlash($panelLocation);
			$GLOBALS['FileSystemObj']->delete($panelLocation.'/addons/googleWebMasters/lib/authHelper.php', false, 'f');
			$GLOBALS['FileSystemObj']->delete($panelLocation.'/addons/googleWebMasters/lib/storage.php', false, 'f');
			$GLOBALS['FileSystemObj']->delete($panelLocation.'/addons/googleWebMasters/lib/src', true);
		}
		return true;
	}
	

}