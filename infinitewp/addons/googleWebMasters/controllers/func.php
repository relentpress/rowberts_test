<?php

function googleWebMastersAddonMenus(&$menus){
	$menus['monitor']['subMenus'][] = array('page' => 'googleWebMasters', 'displayName' => 'Google Webmasters');
}

function googleWebMastersResponseProcessors(&$responseProcessor){
	$responseProcessor['googleWebMasters']['webmastersRedirect'] = 'googleWebMastersRedirect';
	$responseProcessor['googleWebMasters']['webmastersRedirectAgain'] = 'googleWebMastersRedirectAgain';
}

function googleWebMastersTaskTitleTemplate(&$template){
	$template['googleWebMasters']['webmastersRedirect']['']	= "Redirecting 1 link in <#sitesCount#> site<#sitesCountPlural#>";
	$template['googleWebMasters']['webmastersRedirectAgain']['']	= "Redirecting 1 link in <#sitesCount#> site<#sitesCountPlural#>";
}

function googleWebMastersFetchProfiles(){
	try{
		googleWebMastersHelper::setPathsAndKeys();
		if(!googleWebMastersHelper::authAndSetToken()){ return false; }
		$getSites = googleWebMastersHelper::getSites(googleWebMastersHelper::$client);
	}catch(Google_ServiceException $e){
		$cd = $e->getCode();
		$errors = $e->getErrors();
		addNotification($type='E', $title='Google Webmasters Service Exception (Error#'.$cd.')', $message=$errors[0]['message'], $state='U', $callbackOnClose='', $callbackReference='');
	}
	catch(Google_Exception $e){
		$cd = $e->getCode();
		$msg = $e->getMessage();
		addNotification($type='E', $title='Google Webmasters Exception (Error#'.$cd.')', $message=$msg, $state='U', $callbackOnClose='', $callbackReference='');
	}

	DB::delete("?:google_webmasters_profiles", "1"); //delete all rows
	if(!empty($getSites)){
		foreach ($getSites as $site) {
			DB::insert("?:google_webmasters_profiles",array('gwtSiteURL'=>$site,'gwtProfileID'=>md5($site)));
		}
	}
	
	updateOption('gwtLastProfilesFetch', time());
	
	$run = getOption('gwtMapProfilesAndSitesInitialRun');
	if(empty($run) || $run == 'N'){
		googleWebMastersPostAddSite();
		updateOption('gwtMapProfilesAndSitesInitialRun', 'Y');
	}
	return true;
}

function googleWebMastersActiveSites(){
	return DB::getFields("?:google_webmasters_profiles_sites", "siteID", "gwtProfileID IS NOT NULL");
}


function googleWebMastersSetProfilesAndSites($params){
	if(empty($params['googleProfileID'])) $params['googleProfileID'] = array('NULL');
	DB::update("?:google_webmasters_profiles_sites", array('gwtProfileID' => $params['googleProfileID']), "siteID = ".$params['siteID']);
}

function googleWebMastersGetProfileData($params){
	try{
		googleWebMastersHelper::setPathsAndKeys();

		if(!googleWebMastersHelper::authAndSetToken()){
			return false;
		}
		foreach ($params['siteIDs'] as $siteID) {
			$profileID = DB::getField("?:google_webmasters_profiles_sites", "gwtProfileID", "siteID = '".$siteID."'");
			$encSiteURL= urlencode(DB::getField("?:google_webmasters_profiles","gwtSiteURL","gwtProfileID='".$profileID."'"));
			$getCrawlData[$siteID] = googleWebMastersHelper::getCrawlData(googleWebMastersHelper::$client,$encSiteURL,$siteID);
	                $getCrawledSitestemp = getSiteData(intval($siteID));
			$getCrawledSites[$siteID] = $getCrawledSitestemp['URL'];
		}
		$result = array($getCrawledSites,$getCrawlData);
		return $result;
	}catch(Google_ServiceException $e){
		$cd = $e->getCode();
		$errors = $e->getErrors();
		addNotification($type='E', $title='Google Webmasters Service Exception (Error#'.$cd.')', $message=$errors[0]['message'], $state='U', $callbackOnClose='', $callbackReference='');
	}
	catch(Google_Exception $e){
		$cd = $e->getCode();
		$msg = $e->getMessage();
		addNotification($type='E', $title='Google WebMasters Exception (Error#'.$cd.')', $message=$msg, $state='U', $callbackOnClose='', $callbackReference='');
	}

}

function googleWebMastersGrantAccess(){
	$result = getOption('googleWebMastersAccessStatus');
	if(!empty($result)){
		return $result;
	}
	return false;
}

function googleWebMastersPostAddSite($siteID=false){
	
	googleWebMastersInstallPopulateSites();
	
	$successMaps = 0;
	$whereJoin = '';
	if(!empty($siteID)){
		$whereJoin = " AND S.siteID = '".$siteID."'";
	}
	
	$sites = DB::getFields("?:sites S, ?:google_webmasters_profiles_sites GWMPS", "S.URL, S.siteID", "S.siteID = GWMPS.siteID AND (GWMPS.gwtProfileID IS NULL OR GWMPS.gwtProfileID = '')".$whereJoin, "siteID");
	if(!empty($sites)){
		foreach($sites as $siteID => $siteAddress){
			$siteAddress = str_ireplace(array('http://', 'https://'), '', $siteAddress);
			$siteAddress = rtrim($siteAddress, '/');
			$gwtProfileID = DB::getField("?:google_webmasters_profiles", "gwtProfileID", "gwtProfileID NOT IN(SELECT gwtProfileID FROM ?:google_webmasters_profiles_sites WHERE (gwtProfileID IS NULL AND gwtProfileID = '')) AND gwtSiteURL LIKE '%".$siteAddress."%'");
			if(!empty($gwtProfileID)){
				DB::update("?:google_webmasters_profiles_sites", array('gwtProfileID' => $gwtProfileID), "siteID = ".$siteID);
				$successMaps++;
			}
		}
	}
	if($successMaps > 0){
		addNotification($type='N', $title='Google Webmasters site'.(($successMaps > 1) ? 's' : '').' mapped', $message=$successMaps.' site'.(($successMaps > 1) ? 's' : '').' mapped with google webmasters.', $state='U', $callbackOnClose='', $callbackReference='');
	}
}

//Mapped site and profileID analytic profile
function googleWebMastersMappedSites(){
	$mappedSites = DB::getArray("?:google_webmasters_profiles_sites", "siteID,gwtProfileID", "gwtProfileID IS NOT NULL");
	foreach($mappedSites as $key => $value){
		$option[$value['siteID']] = array('gwtProfileID' => $value['gwtProfileID']); 
	}
	return $option;
}

function googleWebMastersEditSiteOptions(){
	
	$gwtDatas = DB::getArray("?:google_webmasters_profiles", "gwtSiteURL, gwtProfileID", "1");
	if(!empty($gwtDatas)){
		foreach($gwtDatas as $key => $gwtData ){
			$gwtSiteURL = str_replace(array('http://', 'https://'), '', $gwtData["gwtSiteURL"]);
			$gwtSiteURL = $gwtSiteURL.(substr($gwtSiteURL, -1) == '/' ? '' : '');
			$googleWebMastersProfileData[] = array("gwtSiteURL" => $gwtSiteURL, "gwtProfileID" => $gwtData["gwtProfileID"]);
		}
	}
	
	return $googleWebMastersProfileData;
}


function googleWebMastersInstallPopulateSites(){
	
	$siteIDs = DB::getFields("?:sites", "siteID", "1");
	if(!empty($siteIDs)){
		foreach($siteIDs as $siteID){
			if(!DB::getExists("?:google_webmasters_profiles_sites", "siteID", "siteID = ".$siteID)){
				DB::insert("?:google_webmasters_profiles_sites", array('siteID' => $siteID));
			}
		}
	}
}


function googleWebMastersRunOffBrowserLoad(){
	
	
	if(empty($_SESSION['userID'])){
		$userID = DB::getField("?:users", "userID", "1 ORDER BY userID ASC LIMIT 1");
		$_SESSION['userID'] = $userID;
	}

	$googleAPIKeys = getOption('googleAPIKeys');
	$result = getOption('googleWebMastersAccessStatus');
	if(!empty($googleAPIKeys)){
		$APIKeys = unserialize($googleAPIKeys);
	}
	if(empty($APIKeys['clientID']) || empty($APIKeys['clientSecretKey']) || $result != 'grant'){
		return false;
	}
	
	googleWebMastersFetchProfiles();
}


function googleWebMastersIgnoreValue($params){
	$crawl = md5($params['siteID'].$params['issueType'].$params['detail'].$params['url'].$params['dateDetected']);
	if($params['type'] == 'redirect'){
		$query = DB::insert("?:google_webmasters_ignored",array('crawl'=>$crawl,'issue'=>$params['url'],'redirect'=>$params['new_link']));
		if($query)
			addNotification($type='N', $title='Redirection Added', $message="Your link has been redirected to ".$params['new_link'].".", $state='U', $callbackOnClose='', $callbackReference='');
	}else if($params['type'] == 'ignore'){
		$query = DB::insert("?:google_webmasters_ignored",array('crawl'=>$crawl,'issue'=>$params['url']));
		if($query)
			addNotification($type='N', $title='Ignoring this link', $message="Your link has been ignored and will not be shown further.", $state='U', $callbackOnClose='', $callbackReference='');
	}else if($params['type'] == 'bulk_ignore'){
		$query = DB::insert("?:google_webmasters_ignored",array('crawl'=>$crawl,'issue'=>$params['url']));
		if($query)
			addNotification($type='N', $title='Ignoring this link', $message="Your links have been ignored and will not be shown further.", $state='U', $callbackOnClose='', $callbackReference='');
	}
	return $params;
}

function googleWebMastersRedirectedAgain($params){
	$query = DB::update( "?:google_webmasters_ignored", array( "redirect" => $params['new_link'] ), "crawl='".$params['id']."'" );
	if($query)
		addNotification($type='N', $title='Redirection Edited', $message="Your link has been redirected to ".$params['new_link'].".", $state='U', $callbackOnClose='', $callbackReference='');
	return $params;
}

function googleWebMastersBulkIgnores($params){
	$returnArray = array();
	foreach ($params as $siteID => $issues) {
		for ($i=0; $i < count($issues); $i++) { 
			$redefinedParams = array(
										'siteID' => $siteID,
										'id' => $issues[$i][0],
										'issueType' => $issues[$i][1],
										'detail' => $issues[$i][2],
										'url' => $issues[$i][3],
										'dateDetected' => $issues[$i][4],
										'type' => 'bulk_ignore',
									);
			$extra = googleWebMastersIgnoreValue($redefinedParams);
			if(!empty($extra))
				array_push($returnArray, $extra);
		}
	}
	return $returnArray;
}

function googleWebMastersReconsiderIssue($params){
	$query = DB::delete("?:google_webmasters_ignored","crawl='".$params."'");
	if($query)
		addNotification($type='W', $title='Issue Reconsidered', $message="Your link has been reconsidered.<br>Please reload webmaster tools ", $state='U', $callbackOnClose='', $callbackReference='');
	return $params;
}


function googleWebMastersRedirect($params){
	$actionID = Reg::get('currentRequest.actionID');
		
	$linkData = DB::getFields("?:temp_storage", "data", "type = 'googleWebMasters' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'googleWebMasters' AND paramID = '".$actionID."'");
		
	if(empty($linkData)){
		return array();
	}
	$refinedData = array();

	foreach($linkData as $data){
		$refinedData = array_merge_recursive($refinedData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($refinedData);
	foreach($refinedData as $siteID => $links){
		$siteData = DB::getRow("?:sites","URL","siteID=".$siteID."");
		$refinedData['siteURL'] = $siteData['URL'];
		$refinedData['siteID'] = $siteID;
		$refinedData['issueID'] = $params;

	}
	return $refinedData;
}


function googleWebMastersRedirectAgain($params){
	$actionID = Reg::get('currentRequest.actionID');
		
	$linkData = DB::getFields("?:temp_storage", "data", "type = 'googleWebMasters' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'googleWebMasters' AND paramID = '".$actionID."'");
		
	if(empty($linkData)){
		return array();
	}
	$refinedData = array();

	foreach($linkData as $data){
		$refinedData = array_merge_recursive($refinedData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($refinedData);
	foreach($refinedData as $siteID => $links){
		$siteData = DB::getRow("?:sites","URL","siteID=".$siteID."");
		$refinedData['siteURL'] = $siteData['URL'];
		$refinedData['siteID'] = $siteID;
		$refinedData['issueID'] = $params;

	}
	return $refinedData;
}

class googleWebMastersHelper{
	
	public static $client;
	public static $authHelper;
	public static $storage;
	private static $GWTThisPage;
	private static $redirectURL;
	private static $isSetPathsAndKeysComplete = false;
	
	public static function setPathsAndKeys(){
		
		if(self::$isSetPathsAndKeysComplete) return true;
		
		include_once(APP_ROOT.'/lib/googleAPIs/src/Google_Client.php');
		include_once(APP_ROOT.'/lib/googleAPIs/storage.php');
		include_once(APP_ROOT.'/lib/googleAPIs/authHelper.php');
		
		self::$GWTThisPage = APP_URL.'addons/googleWebMasters/lib/googleWebMasters.php';
		self::$redirectURL = APP_URL.'addons/googleWebMasters/lib/googleWebMasters.php';
		
		
		$googleWebMastersAPIKeys = getOption('googleAPIKeys');
		if(!empty($googleWebMastersAPIKeys)){
			$googleWebMastersAPIKeys = unserialize($googleWebMastersAPIKeys);
		}
	
		// Build a new client object to work with authorization.
		self::$client = new Google_Client();
		self::$client->setClientId($googleWebMastersAPIKeys['clientID']);

		self::$client->setClientSecret($googleWebMastersAPIKeys['clientSecretKey']);
		self::$client->setRedirectUri(self::$redirectURL);
		self::$client->setApplicationName('InfiniteWP Google WebMasters');
		self::$client->setScopes(array('https://www.google.com/webmasters/tools/feeds'));
		
		// Magic. Returns objects from the Analytics Service
		// instead of associative arrays.
		self::$client->setUseObjects(true);
		
		
		// Build a new storage object to handle and store tokens in sessions.
		// Create a new storage object to persist the tokens across sessions.
		self::$storage = new apiSessionStorage(); //googleAnalyticsStorageHelper();
		
		self::$authHelper = new AuthHelper(self::$client, self::$storage, self::$GWTThisPage);
		
		self::$isSetPathsAndKeysComplete = true;

	}
	
	public static function authAndSetToken(){
		
		$getAccessToken = getOption('googleWebMastersAccessToken');
		$accessToken = unserialize($getAccessToken);

		if (isset($accessToken)) {
		  	self::$client->setAccessToken($accessToken);
		}
			
		return self::$authHelper->isAuthorized();
	}

	public static function getSites($client){
		$req = new Google_HttpRequest("https://www.google.com/webmasters/tools/feeds/sites/");
		$feed = self::$client->getIo()->authenticatedRequest($req);
		$respBody = $feed->getResponseBody();
		$doc  = new DOMDocument();
		@$doc->loadXML($respBody);
		$sites = array();
		foreach ($doc->getElementsByTagName('entry') as $node) {
			array_push($sites,$node->getElementsByTagName('title')->item(0)->nodeValue);
		}
		return $sites;
	}

	public static function getCrawlData($client,$encUri,$siteID){
		$issues = array();
		$redirects = array();
		$dismissed = array();
		$url = "https://www.google.com/webmasters/tools/feeds/{$encUri}/crawlissues?start-index=1";//&max-results=1
		$req = new Google_HttpRequest($url);
		$feed = self::$client->getIo()->authenticatedRequest($req);
		$respBody = $feed->getResponseBody();
		$doc  = new DOMDocument();
		$doc->loadXML($respBody);
		foreach ($doc->getElementsByTagName('entry') as $node) {
			$issueId = str_replace(
			    "https://www.google.com/webmasters/tools/feeds/{$encUri}/crawlissues/",
			    '',
			    $node->getElementsByTagName('id')->item(0)->nodeValue
			);
			$crawlType    = $node->getElementsByTagNameNS('http://schemas.google.com/webmasters/tools/2007', 'crawl-type')->item(0)->nodeValue;
			$issueType    = $node->getElementsByTagNameNS('http://schemas.google.com/webmasters/tools/2007', 'issue-type')->item(0)->nodeValue;
			$detail       = $node->getElementsByTagNameNS('http://schemas.google.com/webmasters/tools/2007', 'detail')->item(0)->nodeValue;
			$url          = $node->getElementsByTagNameNS('http://schemas.google.com/webmasters/tools/2007', 'url')->item(0)->nodeValue;
			$dateDetected = date('M d, Y', strtotime($node->getElementsByTagNameNS('http://schemas.google.com/webmasters/tools/2007', 'date-detected')->item(0)->nodeValue));
			$updated      = date('M d, Y', strtotime($node->getElementsByTagName('updated')->item(0)->nodeValue));

			$value = md5($siteID.$issueType.$detail.$url.$dateDetected);

			$isIgnored = intval(DB::getField("?:google_webmasters_ignored", "count(id) AS igCount", "crawl = '".$value."' "));
			// add issue data to results array
			if(!$isIgnored){
				array_push($issues,
				    array($value, $crawlType, $issueType, $detail, $url, $dateDetected, $updated)
				);
			}else{
				$ignored = DB::getArray("?:google_webmasters_ignored", "redirect", "crawl = '".$value."' ");
				if ($ignored[0]['redirect'] == NULL) {
					array_push($dismissed,array($value, $crawlType, $issueType, $detail, $url, $dateDetected, $updated));
				}else{
					array_push($redirects, array($value,$url,$ignored[0]['redirect']));
				}
			}
		}
		
		$result = array($issues,$redirects,$dismissed);
		return $result;
	}

}