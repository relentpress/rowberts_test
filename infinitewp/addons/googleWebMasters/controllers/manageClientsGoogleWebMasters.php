<?php
	class manageClientsGoogleWebMasters
	{
		public static function googleWebMastersRedirectProcessor($siteID,$params){
			$type = "googleWebMasters";
			$action = $params['action'];
			$requestAction = "put_redirect_url";
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => 'wmtredirectURL', 'detailedAction' => 'redirectLink');
			$events=1;
			$siteData = getSiteData(intval($siteID));
			$requestParams = array('oldLink'=>$params['oldLink'],'newLink'=>$params['newLink'],'location'=>$params['location']);
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['directExecute'] 	= false;
			$PRP['events'] 			= $events;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
			prepareRequestAndAddHistory($PRP);
		}

		public static function googleWebMastersRedirectResponseProcessor($historyID, $responseData){
			responseDirectErrorHandler($historyID, $responseData);
			
			$response = array();
			if(isset($responseData['success'])){
				$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
				$actionID = $historyData['actionID'];
				$siteID = $historyData['siteID'];
				$response['_'.$siteID] = $responseData['success'];

				DB::insert("?:temp_storage", array('type' => 'googleWebMasters', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
				DB::update("?:history_additional_data", array('status'=>'success'), "historyID=".$historyID."");
				return;
			}
		}

		public static function googleWebMastersRedirectAgainProcessor($siteID,$params){
			$type = "googleWebMasters";
			$action = $params['action'];
			$requestAction = "put_redirect_url_again";
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => 'wmtredirectURLagain', 'detailedAction' => 'redirectLinkAgain');
			$events=1;
			$siteData = getSiteData(intval($siteID));
			$requestParams = array('originalLink'=>$params['originalLink'],'oldLink'=>$params['oldLink'],'newLink'=>$params['newLink'],'location'=>$params['location']);
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['directExecute'] 	= false;
			$PRP['events'] 			= $events;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
			prepareRequestAndAddHistory($PRP);
		}

		public static function googleWebMastersRedirectAgainResponseProcessor($historyID, $responseData){
			responseDirectErrorHandler($historyID, $responseData);
			
			$response = array();
			if(isset($responseData['success'])){
				$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
				$actionID = $historyData['actionID'];
				$siteID = $historyData['siteID'];
				$response['_'.$siteID] = $responseData['success'];

				DB::insert("?:temp_storage", array('type' => 'googleWebMasters', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($response)));
				DB::update("?:history_additional_data", array('status'=>'success'), "historyID=".$historyID."");
				return;
			}
		}




	}
	manageClients::addClass('manageClientsGoogleWebMasters');