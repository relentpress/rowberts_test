<?php

include '../../../includes/app.php';
try{
	//Main controller logic.

	googleWebMastersHelper::setPathsAndKeys();

	if ($_GET['action'] == 'revoke') {
	 googleWebMastersHelper::$authHelper->revokeToken();
	 //DB::delete("?:google_analytics_auth", "1");
	 deleteOption('googleWebMastersAccessToken');
	 DB::delete("?:google_webmasters_profiles", "1");
	 DB::delete("?:google_webmasters_profiles_sites", "1");
	 updateOption('gwtMapProfilesAndSitesInitialRun', "N");
	 
	 updateOption('googleWebMastersAccessStatus', 'revoke');
	 
	 addNotification($type='N', $title='Google WebMasters Revoked', $message="Your Google WebMasters account has been revoked.", $state='U', $callbackOnClose='', $callbackReference='');
	  
	} else if ($_GET['action'] == 'auth' || $_GET['code']) {
	  googleWebMastersHelper::$authHelper->authenticate();

	  if($_GET['code']){
		  sleep(1);
		  $getAccessToken = googleWebMastersHelper::$storage->get($accessToken);
		  //DB::replace("?:google_analytics_auth", array('ID' => 1, 'userID' => $_SESSION['userID'], 'auth_data' => serialize($getAccessToken))); //using ID in insert will restrict to 1, because ID is primary
		  updateOption('googleWebMastersAccessToken', serialize($getAccessToken));
	  }
	}else {
		if(googleWebMastersHelper::$storage->get($accessToken) ){
			googleWebMastersHelper::$authHelper->setTokenFromStorage();

			googleWebMastersInstallPopulateSites();

			if(googleWebMastersFetchProfiles()){

				addNotification($type='N', $title='Google WebMasters Connected', $message="Your Google WebMasters account has been connected.", $state='U', $callbackOnClose='', $callbackReference='');

				updateOption('googleWebMastersAccessStatus', 'grant');

				header("Location:".APP_URL); 
			}
			else{
				echo 'some error here';//CLEARIT
			}
		}else{
			header("Location:".APP_URL); 
		}

	}

}catch(Google_ServiceException $e){
	$cd = $e->getCode();
	$errors = $e->getErrors();
	addNotification($type='E', $title='Google Webmasters Service Exception (Error#'.$cd.')', $message=$errors[0]['message'], $state='U', $callbackOnClose='', $callbackReference='');
	header("Location:".APP_URL); 
}
catch(Google_Exception $e){
	$cd = $e->getCode();
	$msg = $e->getMessage();
	addNotification($type='E', $title='Google Webmasters Exception (Error#'.$cd.')', $message=$msg, $state='U', $callbackOnClose='', $callbackReference='');
	header("Location:".APP_URL); 
}
		

?>