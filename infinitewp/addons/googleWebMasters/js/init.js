$(function(){

	googleSettings = '<li>Google</li>';
	googleWebMastersAccess = '<div id="gWebMastersAccess" class="tr"> <div class="padding" style="font-size:12px;"><div class="label" style="padding: 10px 0;">CONNECT YOUR GOOGLE WebMasters ACCOUNT<a href="http://infinitewp.com/knowledge-base/how-to-connect-your-google-apis-account/?utm_source=application&utm_medium=userapp&utm_campaign=kb" style="padding:0; float:right; display:inline; text-transform:none;" target="_blank">See instructions</a></div> <div class="tl no_text_transform" style="width:475px;"> <div><div class="form_label">Authorized Redirect URI</div><input type="text" class="selectOnText" style="width:466px;" value="'+systemURL+'addons/googleWebMasters/lib/googleWebMasters.php" readonly="true" /></div> <div class="clear-both"></div></div> <div class="clear-both"></div> <div class="btn_action float-right gwmt_grant" ><a class="rep_sprite btn_blue" id="wmt_grant_revoke" href="'+systemURL+'addons/googleWebMasters/lib/googleWebMasters.php?action=auth" >Grant Access</a></div> <div class="btn_action float-right gwmt_revoke" style="display:none" ><a class="rep_sprite btn_blue" id="wmt_grant_re" style="" href="'+systemURL+'addons/googleWebMasters/lib/googleWebMasters.php?action=revoke">Revoke Access</a></div> <div class="clear-both"></div></div> </div> ';
	// $('#googleTab').append(googleWebMastersAccess);

	tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['googleWebMastersGrantAccess']=1;
	// tempArray['requiredData']['googleServicesGetAPIKeys']=1;			//available in base
	doCall(ajaxCallPath,tempArray);
	getGoogleWebMastersContent();
});

$(".loadWebMastersBtn").live('click',function() {
	    $(this).addClass('disabled');
	    $('.WebMastersContent').html('').hide();
		var siteIDs=[];
		$.each($('.website_cont.active'),function(){
			siteIDs.push($(this).attr('sid'));
		});
	    var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['googleWebMastersGetProfileData']={};
		tempArray['requiredData']['googleWebMastersGetProfileData']['siteIDs']=siteIDs;
		 doCall(ajaxCallPath,tempArray,'loadGoogleWebMastersData');
});

$('.ci_ignore').live('click',function(){
	var crawlInfo = $(this).closest('.row_updatee');
	var crawledInfo = {
		'id':crawlInfo.attr('civ'),
		'siteID':crawlInfo.closest('.row_detailed').attr('sid'),
		'issueType':crawlInfo.find('.ci_details[cont="issueType"]').html(),
		'detail':crawlInfo.find('.ci_details[cont="detail"]').html(),
		'url':crawlInfo.find('.ci_url').html(),
		'dateDetected':crawlInfo.find('.ci_details[cont="dateDetected"]').html(),
		'type':'ignore'
	};
    var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['googleWebMastersIgnoreValue']=crawledInfo;
	doCall(ajaxCallPath,tempArray,'loadWebMastersRemoveIgnored');
	return false;
});

$(".wmt_redirect").live('click',function() {
	$(this).closest('.row_updatee').find('.ci_details_tab').hide();
	$(this).closest('.row_updatee').find('.ci_redirect_tab').show();
	return false;
});

$(".wmt_crawl_redirect_url_cancel").live('click',function() {
	$(this).closest('.row_updatee').find('.ci_redirect_tab').hide();
	$(this).closest('.row_updatee').find('.ci_details_tab').show();
	return false;
});

$(".wmt_crawl_redirect_url_ok").live('click',function() {
	var Parent = $(this).closest('.row_updatee');
	var old_link = Parent.find('.ci_tab').find('.ci_url').html();
	var new_link = Parent.find('.ci_redirect_tab').find('.ci_redirect_url').val();
	if(old_link == new_link){
		Parent.find('.ci_actions_panel').find('.wmt_crawl_redirect_url_cancel').click();
	}else{
		var siteID = Parent.closest('.row_detailed').attr('sid');
		var issueID = Parent.attr('civ');
		var tempArray={};
		tempArray['action']='googleWebMastersRedirect';
		tempArray['args']={};
		tempArray['args']['siteIDs'] = siteID;
		tempArray['args']['params'] = {};
		tempArray['args']['params']['action'] = 'webmastersRedirect';
		tempArray['args']['params']['location'] = 'database';
		tempArray['args']['params']['oldLink'] = old_link;
		tempArray['args']['params']['newLink'] = new_link;
		tempArray['requiredData'] = {};
		tempArray['requiredData']['googleWebMastersRedirect'] = issueID;
		doCall(ajaxCallPath,tempArray,'formArrayGoogleWebMastersRedirect');
	}
	return false;
});

$(".googleWTSettingsClick").live('click',function() {
	openSettingsPage('Google');
    return false;
	
});
$('.website_cont.hidden_ggwmt').live('hover',function(e){
	if($(this).hasClass('disabled'))
	{
		$(this).removeClass('disabled');
		$(this).children('.tip').show();
		$(this).children('.tip').text('Click to assign WebMasters profile');
	}
	else
	{
		$(this).addClass('disabled');
		$(this).children('.tip').hide();
	}
	
});
$('.website_cont.hidden_ggwmt').live('click',function(){
	$(".googleWMTBtnView").hide();
	$('.WebMastersContent').html('');
});
$(".wmt_grant_revoke_click").live('click',function() {
	$('#wmt_grant_revoke').click();
});

$('#wmt_grant_revoke').live('click',function(){
	var tempArray={};
	tempArray['action']='';
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['requiredData']={};
	var paramsArray={};
	$("#clientCreds .formVal").each(function () {
		paramsArray[$(this).attr('id')]=$(this).val();
	});
	paramsArray['redirect']=1;
	tempArray['requiredData']['googleServicesSaveAPIKeys']=paramsArray;
	grant_revoke_click = ".wmt_grant_revoke_click";
	doCall(ajaxCallPath,tempArray,'processGoogleWebMastersForm');
});


$('.googleWMTSites .tip').live('click',function(e){
	e.stopImmediatePropagation();
	var sid = $(this).closest('.website_cont').attr('sid');
	loadAddSite(sid);
	$('#ggwm').select2('open');
	if(typeof isGoogle != 'undefined')
	{
		if(isGoogle=='1')
		{
			$('.googleEditOptions').hide();
		}
	}
	$(".dialog_cont .title").text("EDIT SITE DETAILS");
	$(".dialog_cont .activationKeyDiv").hide();
	$(".dialog_cont .websiteURLCont").show();
	
	$(".dialog_cont .addSiteButton").text('Save Changes').addClass('editSite').attr('sid',$(this).closest('.website_cont').attr('sid'));
	$(".dialog_cont #gg").css("visibility", "visible");
	$(".dialog_cont #gg").css("top", "auto");
	$(".dialog_cont #websiteURL").val(site[$(this).closest('.website_cont').attr('sid')].URL);
	$(".dialog_cont #adminURL").val(site[$(this).closest('.website_cont').attr('sid')].adminURL);
	$(".dialog_cont #username").val(site[$(this).closest('.website_cont').attr('sid')].adminUsername).focus();
	$(".dialog_cont .connectURLClass").removeClass('active');
	$(".dialog_cont [def='"+site[$(this).closest('.website_cont').attr('sid')].connectURL+"']").addClass('active');
	$("#clientPluginDescription").hide();
	if(typeof site[$(this).closest('.website_cont').attr('sid')].httpAuth!='undefined' && typeof site[$(this).closest('.website_cont').attr('sid')].httpAuth.username!='undefined')
		$(".dialog_cont #addSiteAuthUsername").val(site[$(this).closest('.website_cont').attr('sid')].httpAuth.username);
	if(typeof site[$(this).closest('.website_cont').attr('sid')].httpAuth!='undefined' && typeof site[$(this).closest('.website_cont').attr('sid')].httpAuth.password!='undefined')
		$(".dialog_cont #addSiteAuthUserPassword").val(site[$(this).closest('.website_cont').attr('sid')].httpAuth.password);
	if(getPropertyCount(site[$(this).closest('.website_cont').attr('sid')].groupIDs)>0)
	{
		$.each(site[$(this).closest('.website_cont').attr('sid')].groupIDs, function(i, object) {
			$(".addSiteGroups .g"+object).addClass('active');
		});
	}
	if(typeof site[$(this).closest('.website_cont').attr('sid')].callOpt!= 'undefined' && (site[$(this).closest('.website_cont').attr('sid')].callOpt.contentType!=''))
	{
		var radionSiteID=site[$(this).closest('.website_cont').attr('sid')];
		
		$(".cTypeRadio").removeClass('active');
		var checkCustomType=0;
		$(".cTypeRadio").each (function () {
			if($(this).text()==radionSiteID.callOpt.contentType)
			{
				$(this).addClass('active');
				checkCustomType=1;
			}
		});
		if(checkCustomType==0)
		{
			$(".cTypeRadio .customTxtVal").val(site[$(this).closest('.website_cont').attr('sid')].callOpt.contentType);
			$(".cTypeRadio.customTxt").addClass('active');
		}
	}
});


$('.load_gwmt').live('click',function(){
	var act = $(this).attr('action');
	$('.gwm_status_applyChangesCheck').addClass('disabled');
	changeGoogleWebMastersHTML(act,$(this));
});


$('.select_all_gwm_issues').live('click',function(){
	if ($(this).hasClass('active')) {
		$(this).closest('.ind_row_cont').find('.row_detailed').find('.select_ci').removeClass('active');
		$(this).closest('.ind_row_cont').find('.row_detailed').find('.row_updatee').removeClass('active');
		$(this).closest('.ind_row_cont').removeClass('active');
		$(this).closest('.ind_row_cont').find('.select_all_gwm_issues').removeClass('active');
	}else{
		$(this).closest('.ind_row_cont').find('.row_detailed').find('.select_ci').addClass('active');
		$(this).closest('.ind_row_cont').find('.row_detailed').find('.row_updatee').addClass('active');
		$(this).closest('.ind_row_cont').addClass('active');
		$(this).closest('.ind_row_cont').find('.select_all_gwm_issues').addClass('active');
	}
	if($('.select_ci.active').length && $('.select_gwmtignore_links.active').length){
		$('.gwm_status_applyChangesCheck').removeClass('disabled');
	}else{
		$('.gwm_status_applyChangesCheck').addClass('disabled');
	}
	return false;
});




			

$('.WebMastersContent .row_updatee').live('click',function(e){
	if($(e.target).attr('type') != 'textbox' && $('.load_gwmt.active').attr('action')=='all'){
		if ($(this).hasClass('active') ) {
			if($(this).closest('.row_detailed').find('.select_ci.active').length==$(this).closest('.row_detailed').find('.select_ci').length){
				$(this).closest('.ind_row_cont').removeClass('active').find('.select_all_gwm_issues').removeClass('active');
			}
			$(this).removeClass('active');
			$('.checkbox.select_ci',this).removeClass('active');
		}else if( !($(this).hasClass('active')) ){
			if($(this).closest('.row_detailed').find('.select_ci.active').length == $(this).closest('.row_detailed').find('.select_ci').length-1){
				$(this).closest('.ind_row_cont').addClass('active').find('.select_all_gwm_issues').addClass('active');;
			}
			$(this).addClass('active');
			$('.checkbox.select_ci',this).addClass('active');
		}
		if($('.select_ci.active').length && $('.select_gwmtignore_links.active').length){
			$('.gwm_status_applyChangesCheck').removeClass('disabled');
		}else{
			$('.gwm_status_applyChangesCheck').addClass('disabled');
		}
	}
});


$('.select_gwmtignore_links').live('click',function(){
	$('.select_gwmtignore_links').removeClass('active');
	$(this).addClass('active');
	if($('.select_ci.active').length && $('.select_gwmtignore_links.active').length){
		$('.gwm_status_applyChangesCheck').removeClass('disabled');
	}else{
		$('.gwm_status_applyChangesCheck').addClass('disabled');
	}

});

$('.gwm_status_applyChangesCheck').live('click',function(){
	$(this).addClass('disabled');
	$(this).parent().parent().find('.select_gwmtignore_links').removeClass('active')
	var crawledInfo = {};
	$.each($('.row_detailed.active'),function(){
		var siteID = $(this).attr('sid');
		crawledInfo[siteID] = [];
		$.each($(this).find('.select_ci.active'),function(){
			var crawlInfo = $(this).closest('.row_updatee');
			crawledInfo[siteID].push([crawlInfo.attr('civ'),crawlInfo.find('.ci_details[cont="issueType"]').html(),crawlInfo.find('.ci_details[cont="detail"]').html(),crawlInfo.find('.ci_url').html(),crawlInfo.find('.ci_details[cont="dateDetected"]').html()]);
		});
	});
	var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['googleWebMastersBulkIgnores']=crawledInfo;
	doCall(ajaxCallPath,tempArray,'loadWebMastersBulkRemoveIgnored');
});

$('.wmt_redirect_again').live('click',function(){
	$(this).closest('.row_updatee').find('.ci_redirected_tab').hide();
	$(this).closest('.row_updatee').find('.ci_edit_redirected_tab').show();
    return false;
});

$('.wmt_redirect_again_cancel').live('click',function(){
	$(this).closest('.row_updatee').find('.ci_edit_redirected_tab').hide();
	$(this).closest('.row_updatee').find('.ci_redirected_tab').show();
    return false;
});

$('.wmt_redirect_again_ok').live('click',function(){
	var Parent = $(this).closest('.row_updatee');
	var original_link = Parent.find('.ci_tab').find('.ci_url').html();
	var old_link = Parent.find('.ci_redirected_tab').find('.ci_redirected_url').html();
	var new_link = Parent.find('.ci_edit_redirected_tab').find('.ci_edit_redirected_url').val();
	if(old_link == new_link){
		Parent.find('.ci_actions_panel').find('.wmt_redirect_again_cancel').click();
	}else{
		var siteID = Parent.closest('.row_detailed').attr('sid');
		var issueID = Parent.attr('civ');
		var tempArray={};
		tempArray['action']='googleWebMastersRedirectAgain';
		tempArray['args']={};
		tempArray['args']['siteIDs'] = siteID;
		tempArray['args']['params'] = {};
		tempArray['args']['params']['action'] = 'webmastersRedirectAgain';
		tempArray['args']['params']['location'] = 'database';
		tempArray['args']['params']['originalLink'] = original_link;
		tempArray['args']['params']['oldLink'] = old_link;
		tempArray['args']['params']['newLink'] = new_link;
		tempArray['requiredData'] = {};
		tempArray['requiredData']['googleWebMastersRedirectAgain'] = issueID;
		doCall(ajaxCallPath,tempArray,'formArrayGoogleWebMastersRedirectAgain');
	}
    return false;
});
	

$('.wmt_reconsider').live('click',function(){
	var id = $(this).closest('.row_updatee').attr('civ');

	var tempArray={};
	tempArray['requiredData']={};
	
	tempArray['requiredData']['googleWebMastersReconsiderIssue']=id;
	doCall(ajaxCallPath,tempArray,'googleWebMastersReconsiderIssue');
    return false;
});