function loadGoogleWebMastersPage(data)
{   
	var content='<div class="steps_hdr"><span id="processType">MANAGE</span> <span class="itemUpper">Google Webmasters Crawl Errors</span></div><div class="siteSelectorContainer">'+siteSelectorVar+'</div><div class="btn_action float-right loadWebMastersBtn disabled" ><a class="rep_sprite">Load Webmasters Crawl Errors</a></div><div class="WebMastersContent" style="display:none;clear:both;"></div>';
	$("#pageContent").html(content);
	currentPage="googleWebMasters";


	if(grantRevokeWMTData == 'grant')
	{
		$('.singleSiteGoogle').show();
		siteSelectorNanoMultiGoogle();
	}
	else
	{
		$('#pageContent').html('<div class="empty_data_set"><div class="line2">You have not connected your Google WebMasters account to the admin panel. Go to <a class="googleWTSettingsClick" id="">Settings</a> to do it now.</div></div>');
		$('.singleSiteGoogle').hide();
	}

	hideInactiveGoogleWebMastersSites(data.data.googleWebMastersActiveSites);
	$(".googleWMTBtnView").hide();
	$(".WebMastersContent").hide();
	getGoogleWebMastersContent();
	siteSelectorNanoReset();
	siteSelectorNanoMultiGoogle();
}
function getGoogleWebMastersContent(data)
{
	var tempArray={};
	tempArray['requiredData']={};
	
	tempArray['requiredData']['googleWebMastersEditSiteOptions']=1;
	tempArray['requiredData']['googleWebMastersGrantAccess']=1;
	tempArray['requiredData']['googleWebMastersMappedSites']=1;
	
	doCall(ajaxCallPath,tempArray,'storeGoogleWebMastersData');
}

function loadGoogleWebMastersData(data){
	gwmcontentData = data.data.googleWebMastersGetProfileData;
	if(gwmcontentData != null){
		var HTMLData='<div class="actionContent result_block siteSearch" id=""><div class="th rep_sprite"> <ul class="btn_radio_slelect float-left" style="margin-left: 7px;"> <li><a class="rep_sprite load_gwmt optionSelect active" action="all">All</a> </li> <li><a class="rep_sprite load_gwmt optionSelect" action="redirected">Redirected</a> </li> <li><a class="rep_sprite load_gwmt optionSelect" action="dismissed">Dismissed</a> </li> </ul> <div class="type_filter"> <input name="filter" type="text" class="input_type_filter searchSiteBL" value="type to filter" /> <div class="clear_input rep_sprite_backup" onclick=""></div> </div> <div class="btn_action float-right"><a class="rep_sprite gwm_status_applyChangesCheck disabled">Apply Changes</a></div><ul class="th_sub_nav float-right" style="padding: 3px 0 4px 2px;"><li><a class="rep_sprite select_gwmtignore_links">Dismiss</a></li> </ul> </div><div class="no_match hiddenCont" style="display:none">Bummer, there are no backups that match.<br>Try typing fewer characters.</div><div class="rows_cont">';
		var HTMLInnerData = '';
		if(typeof gwmcontentData[1] != 'undefined' && objLen(gwmcontentData[1]) > 0){
			$.each(gwmcontentData[1],function(siteID,crawlIssues){
				var siteURL = gwmcontentData[0][siteID];
				var server_error_tabs = not_found_tabs = like_content_tabs = '';
				var server_error_block = not_found_block = like_content_block = '';
				var GWMtabs = [0,0,0];
				var linkTabs = '';
				var links_count = crawlIssues[0].length;
				if (crawlIssues[0].length) {
					$.each(crawlIssues[0],function(index,issue){
						var redirect_option='';
						if(issue[3].indexOf('robots.txt')<0) redirect_option = '<li class="wmt_redirect rep_sprite"><a title="Redirect"></a></li>';
						var issueText = "<div class='row_updatee' civ='"+issue[0]+"'><div class=' checkbox select_ci'></div><div class='ci_tab'><div class='ci_url'>"+issue[4]+"</div><div class='ci_details_tab'><span class='ci_details' cont='issueType'>"+issue[2]+"</span>;<span class='ci_details' cont='detail'>"+issue[3]+"</span><span class='empty_block'></span> Issue detected on <span class='ci_details' cont='dateDetected'>"+issue[5]+"</span><span class='empty_block'></span> Last crawl on <span class='ci_details' cont='updated'>"+issue[6]+"</span><div class='ci_actions_panel'><ul>"+redirect_option+"<li class='ci_ignore rep_sprite'><a title='Dismiss'></a></li></ul></div></div><div class='ci_redirect_tab'><span class='redirect_from_to_arrow'>&#x21b3;</span><input class='ci_redirect_url' value='"+issue[4]+"' /><div class='ci_actions_panel'><a class='wmt_crawl_redirect_url_ok rep_sprite' title='Redirect'>Redirect</a><a class='wmt_crawl_redirect_url_cancel' title='Cancel'>Cancel</a></div></div></div><div class='clear-both'></div></div>";
						if(issue[3] == '404-like content'){
							like_content_tabs += issueText;
							GWMtabs[0] += 1;
						}else if(issue[3] == '404 \(Not found\)'){
							not_found_tabs += issueText;
							GWMtabs[1] += 1;
						}else{
							server_error_tabs += issueText;
							GWMtabs[2] += 1;
						}
					});
					if(GWMtabs[0] != 0) like_content_block = "<div class='gwm_result_classification'><div class=''><span class='gwm_result_classification_title droid700'>404 Like-Content</span><div class='clear-both'></div></div>"+like_content_tabs+"</div>";
					if(GWMtabs[1] != 0) not_found_block = "<div class='gwm_result_classification'><div class=''><span class='gwm_result_classification_title droid700'>404 Not-Found</span><div class='clear-both'></div></div>"+not_found_tabs+"</div>";
					if(GWMtabs[2] != 0) server_error_block = "<div class='gwm_result_classification'><div class=''><span class='gwm_result_classification_title droid700'>Server Error</span><div class='clear-both'></div></div>"+server_error_tabs+"</div>";
					linkTabs = server_error_block+" "+like_content_block+" "+not_found_block;
				}else{
					linkTabs = "none";
				}
				if(linkTabs != 'none')
					HTMLInnerData = HTMLInnerData+"<div class='ind_row_cont'><div class='row_summary' sid='"+siteID+"'><div class='row_arrow'></div><div class='checkbox select_all_gwm_issues'></div><div class='row_name searchable'><span class='links_count'>"+links_count+"</span>"+siteURL+"</div><div class='clear-both'></div></div><div class='row_detailed' sid='"+siteID+"' style='display:none;clear:both;'><div class='rh'><div class='row_arrow'></div><div class='checkbox select_all_gwm_issues'></div><div class='row_name'><span class='links_count'>"+links_count+"</span>"+siteURL+"</div><div class='clear-both'></div></div><div class='rd'>"+linkTabs+"</div></div></div>";
			});
		}
		if(HTMLInnerData == '') HTMLInnerData = '<div class="gwmt_no_issues">Yipee! No crawl errors in the selected sites.</div>';

		HTMLData = HTMLData+HTMLInnerData+'</div></div>';

		$('.WebMastersContent').html(HTMLData).show();
	}else{
		$('.loadWebMastersBtn').removeClass('disabled');
	}
}

function googleWebMastersNoLinksContent(){
	var redirectedCount = 0;var dismissedCount = 0;var othersCount = 0;
	$.each(gwmcontentData[1],function(siteID,siteData){
		redirectedCount += siteData[1].length;
		dismissedCount += siteData[2].length;
		othersCount += siteData[0].length;
	});
	var cat = $('.load_gwmt.active').attr('action');
	if(cat == 'all'){
		if(redirectedCount+dismissedCount+othersCount == 0)
			$('.rows_cont').html('<div class="gwmt_no_issues">No links in the selected sites.</div>');
	}else{
		if(eval(cat+'Count') == 0)
			$('.rows_cont').html('<div class="gwmt_no_issues">No '+cat+' links in the selected sites.</div>');
	}
}

function googleWebMastersPostActions(ignoreData){
	if(typeof ignoreData != 'undefined' && objLen(ignoreData) > 0){
		var siteID = ignoreData['siteID'];
		var issueId = ignoreData['id'];
		var issueType = ignoreData['type'];
		var issueDOM = $('.row_detailed[sid="'+siteID+'"]').find('.row_updatee[civ="'+issueId+'"]');
		var civ = issueDOM.attr('civ');
		issueDOM.closest('.row_detailed').find('.links_count').html(parseInt(issueDOM.closest('.row_detailed').find('.links_count').html())-1);
		issueDOM.closest('.ind_row_cont').find('.row_summary').find('.links_count').html(parseInt(issueDOM.closest('.ind_row_cont').find('.row_summary').find('.links_count').html())-1);
		if(typeof gwmcontentData[1][siteID][0] != 'undefined' && objLen(gwmcontentData[1][siteID][0]) > 0){
			$.each(gwmcontentData[1][siteID][0],function(index,issue){
				if(issue[0] == civ) {
					if (issueType == 'ignore' || issueType == 'bulk_ignore') {
						gwmcontentData[1][siteID][2].push(issue);
						gwmcontentData[1][siteID][0].splice(index,1);
					}else if(issueType == 'redirect'){
						gwmcontentData[1][siteID][1].push([civ,issue[4],ignoreData['new_link']]);
						gwmcontentData[1][siteID][0].splice(index,1);
					}
					return false;
				}
			});
		}
		$('.row_detailed').removeClass('active');
		issueDOM.remove();
		googleWebMastersNoLinksContent();
	}
}

function loadWebMastersRemoveIgnored(data){
	var ignoreData = data.data.googleWebMastersIgnoreValue;
	googleWebMastersPostActions(ignoreData);
}

function loadWebMastersBulkRemoveIgnored(data){
	var bulkIgnoreData = data.data.googleWebMastersBulkIgnores;
	$.each(bulkIgnoreData,function(index,ignoreData){
		googleWebMastersPostActions(ignoreData);
	});
}
function loadWebMastersEditRedirected(data){
	var data = data.data.googleWebMastersRedirectedAgain;
	$('.row_updatee[civ="'+data['id']+'"]').find('.ci_redirected_url').html(data['new_link']);
	$('.row_updatee[civ="'+data['id']+'"]').find('.ci_edit_redirected_url').val(data['new_link']);
	$('.ci_edit_redirected_tab').hide();
	$('.ci_redirected_tab').show();
}

function storeGoogleWebMastersData(data){
	gwmData = data.data.googleWebMastersEditSiteOptions;
	grantRevokeWMTData=data.data.googleWebMastersGrantAccess;
	gwmSites = data.data.googleWebMastersMappedSites;
	// if(grantRevokeWMTData == 'grant')
	// {
	// 	$('.gwmt_grant').hide();
	// 	$('.gwmt_revoke').show();
	// }
	// else
	// {
	// 	$('.gwmt_grant').show();
	// 	$('.gwmt_revoke').hide();
		
	// }
}

function googleWebMastersReconsiderIssue(data){
	var civ = data.data.googleWebMastersReconsiderIssue;
	var siteID = $('.row_updatee[civ="'+civ+'"]').closest('.row_detailed').attr('sid');
	var issueDOM = $('.row_detailed[sid="'+siteID+'"]').find('.row_updatee[civ="'+civ+'"]');
	issueDOM.closest('.row_detailed').find('.links_count').html(parseInt(issueDOM.closest('.row_detailed').find('.links_count').html())-1);
	issueDOM.closest('.ind_row_cont').find('.row_summary').find('.links_count').html(parseInt(issueDOM.closest('.ind_row_cont').find('.row_summary').find('.links_count').html())-1);
	$('.row_updatee[civ="'+civ+'"]').remove();
	if(typeof gwmcontentData[1][siteID][2] != 'undefined' && objLen(gwmcontentData[1][siteID][2]) > 0){
		$.each(gwmcontentData[1][siteID][2],function(index,issue){
			if(issue[0] == civ) {
				gwmcontentData[1][siteID][2].splice(index,1);
				return false;
			}
		});
	}
	googleWebMastersNoLinksContent();
}
// function loadGoogleWebMastersGrantRevokeButton(data)						//available in base
// {
// 	$('.grantVal').val(data);
// 	var grantMe = data.data.googleServicesGetAPIKeys ;
// 	if((grantMe != null) && (typeof grantMe != 'undefined'))
// 	{
// 	$('#clientID').val(grantMe.clientID);
// 	$('#clientSecretKey').val(grantMe.clientSecretKey);
// 	}
// }

function siteSelectorNanoMultiGoogle()
{
	$(".single_website_items_cont.nano").nanoScroller({stop: true});
	$(".single_website_items_cont").css('height',$(".single_website_items_cont").height()).addClass('nano');
	$(".single_website_items_cont.nano").nanoScroller();	
}


function hideInactiveGoogleWebMastersSites(data){
	$(".website_cont").each(function(){
			   $('.website_items_cont').addClass('googleWMTSites');
			   $(this).addClass('disabled');
			   $(this).addClass('hidden_ggwmt');
			   $(this).append('<div class="tip">Assign a Google WebMasters profile</div>');
		  });

	 if(typeof data != 'undefined'){
		$.each(data,function(k,v){
			var isMatch=0;
			$(".website_cont").each(function(){
				if($(this).attr('sid')==v)
				{
					isMatch=isMatch+2;
					if(isMatch>0)
					{
						$(this).removeClass('disabled');
						$(this).removeClass('hidden_ggwmt');
					}
				}
			});
		});
	}
}


function processGoogleWebMastersForm(data)
{
    var mainData=data;
	$("#saveSettingsBtn").removeClass('disabled');
	$(".settings_cont .btn_loadingDiv").remove();
	if(data.data.googleServicesSaveAPIKeys == true)
	{
		$("#saveSuccess").show();
		setTimeout(function () {	/*$("#settings_cont").hide();*/ $("#saveSuccess").hide();},1000);
		$("#settings_btn").removeClass('active');
		window.location.href = ''+systemURL+'addons/googleWebMasters/lib/googleWebMasters.php?action=auth';
	}
	
}