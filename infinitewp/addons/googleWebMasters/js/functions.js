var thisMonthData={},prevMonthData={},manageGData,activeGoogle,isGoogleWM=1,gwmData='',googleWMJson,gwmSites,grantRevokeWMTData,grant_revoke_click = '',gwmcontentData;

function formArrayGoogleWebMastersRedirect(data){
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="getGoogleWebMastersRedirect";	
}

function googleWebMastersRedirect(data){
	var data = data.data.googleWebMastersRedirect;
	if(typeof data != 'undefined' && objLen(data) > 0){
		var issueLinks = data[data['siteID']];
		var issueID = data['issueID'];
		var crawlInfo = $('.row_updatee[civ="'+issueID+'"]');
		var crawledInfo = {
			'id':issueID,
			'siteID':data['siteID'],
			'issueType':crawlInfo.find('.ci_details[cont="issueType"]').html(),
			'detail':crawlInfo.find('.ci_details[cont="detail"]').html(),
			'url':crawlInfo.find('.ci_url').html(),
			'dateDetected':crawlInfo.find('.ci_details[cont="dateDetected"]').html(),
			'type':'redirect',
			'new_link':issueLinks['newLink']
		};
	    var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['googleWebMastersIgnoreValue']=crawledInfo;
		doCall(ajaxCallPath,tempArray,'loadWebMastersRemoveIgnored');
	}
}

		
function loadGoogleWebMastersContent(e) {
	if($(".googleWMTSites .website_cont.active").length){
		if(typeof e != 'undefined'){
			if(!($( e.target ).closest('.website_cont').find('.tip').is(':visible'))){
				$('.loadWebMastersBtn').removeClass('disabled');
			}else{
				$( e.target ).closest('.website_cont').removeClass('active');
			}
		}else{
			$('.loadWebMastersBtn').removeClass('disabled');
		}
	}else{
		$('.loadWebMastersBtn').addClass('disabled');
		$('.WebMastersContent').html('').hide();
	}
}

function formArrayGoogleWebMastersRedirectAgain(data){
	formArrayVar[data.actionResult.actionID]={};
	formArrayVar[data.actionResult.actionID]['function']="getGoogleWebMastersRedirectAgain";	
}
			
function googleWebMastersRedirectAgain(data){
	var data = data.data.googleWebMastersRedirectAgain;
	if(typeof data != 'undefined' && objLen(data) > 0){
		var issueLinks = data[data['siteID']];
		var issueID = data['issueID'];
		var crawlInfo = $('.row_updatee[civ="'+issueID+'"]');
		var crawledInfo = {
			'id':issueID,
			'original_link':issueLinks['originalLink'],
			'old_link':issueLinks['oldLink'],
			'new_link':issueLinks['newLink']
		};
	    var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['googleWebMastersRedirectedAgain']=crawledInfo;
		doCall(ajaxCallPath,tempArray,'loadWebMastersEditRedirected');
	}
}
	
function changeGoogleWebMastersHTML(act,thisObj){
	var content = '';
	var issueType = act;
	var issue_no_message;
	var parent = thisObj.closest('.actionContent').find('div:first');
	$('.rows_cont').html(content);
	if(typeof gwmcontentData[1] != 'undefined' && objLen(gwmcontentData[1]) > 0){
		if(act == 'all'){
			parent.find('.select_gwmtignore_links').removeClass('active').closest('ul').show();
			parent.find('.gwm_status_applyChangesCheck ').closest('div').show();
				$.each(gwmcontentData[1],function(siteID,crawlIssues){
					var siteURL = gwmcontentData[0][siteID];
					var linkTabs = '';
					var server_error_tabs = not_found_tabs = like_content_tabs = '';
					var server_error_block = not_found_block = like_content_block = '';
					var GWMtabs = [0,0,0];
					var links_count = crawlIssues[0].length;
					if (crawlIssues[0].length) {
						$.each(crawlIssues[0],function(index,issue){
							var redirect_option='';
							if(issue[3].indexOf('robots.txt')<0) redirect_option = '<li class="wmt_redirect rep_sprite"><a title="Redirect"></a></li>';
							var issueText = "<div class='row_updatee' civ='"+issue[0]+"'><div class=' checkbox select_ci'></div><div class='ci_tab'><div class='ci_url'>"+issue[4]+"</div><div class='ci_details_tab'><span class='ci_details' cont='issueType'>"+issue[2]+"</span>;<span class='ci_details' cont='detail'>"+issue[3]+"</span><span class='empty_block'></span> Issue detected on <span class='ci_details' cont='dateDetected'>"+issue[5]+"</span><span class='empty_block'></span> Last crawl on <span class='ci_details' cont='updated'>"+issue[6]+"</span><div class='ci_actions_panel'><ul>"+redirect_option+"<li class='ci_ignore rep_sprite'><a title='Dismiss'></a></li></ul></div></div><div class='ci_redirect_tab'><span class='redirect_from_to_arrow'>&#x21b3;</span><input class='ci_redirect_url' value='"+issue[4]+"' /><div class='ci_actions_panel'><a class='wmt_crawl_redirect_url_ok rep_sprite' title='Redirect'>Redirect</a><a class='wmt_crawl_redirect_url_cancel' title='Cancel'>Cancel</a></div></div></div><div class='clear-both'></div></div>";
							if(issue[3] == '404-like content'){
								like_content_tabs += issueText;
								GWMtabs[0] += 1;
							}else if(issue[3] == '404 \(Not found\)'){
								not_found_tabs += issueText;
								GWMtabs[1] += 1;
							}else{
								server_error_tabs += issueText;
								GWMtabs[2] += 1;
							}
						});
						if(GWMtabs[0] != 0) like_content_block = "<div class='gwm_result_classification'><div class=''><span class='gwm_result_classification_title droid700'>404 Like-Content</span><div class='clear-both'></div></div>"+like_content_tabs+"</div>";
						if(GWMtabs[1] != 0) not_found_block = "<div class='gwm_result_classification'><div class=''><span class='gwm_result_classification_title droid700'>404 Not-Found</span><div class='clear-both'></div></div>"+not_found_tabs+"</div>";
						if(GWMtabs[2] != 0) server_error_block = "<div class='gwm_result_classification'><div class=''><span class='gwm_result_classification_title droid700'>Server Error</span><div class='clear-both'></div></div>"+server_error_tabs+"</div>";
						linkTabs = server_error_block+" "+like_content_block+" "+not_found_block;
					}else{
						linkTabs = "none";
					}
					if(linkTabs != 'none')
						content = content+"<div class='ind_row_cont'><div class='row_summary' sid='"+siteID+"'><div class='row_arrow'></div><div class='checkbox select_all_gwm_issues'></div><div class='row_name searchable'><span class='links_count'>"+links_count+"</span>"+siteURL+"</div><div class='clear-both'></div></div><div class='row_detailed' sid='"+siteID+"' style='display:none;clear:both;'><div class='rh'><div class='row_arrow'></div><div class='checkbox select_all_gwm_issues'></div><div class='row_name'><span class='links_count'>"+links_count+"</span>"+siteURL+"</div><div class='clear-both'></div></div><div class='rd'>"+linkTabs+"</div></div></div>";
				});
		}else if(act == 'redirected'){
			parent.find('.select_gwmtignore_links').closest('ul').hide();
			parent.find('.gwm_status_applyChangesCheck ').closest('div').hide();
			$.each(gwmcontentData[1],function(siteID,crawlIssues){
				var siteURL = gwmcontentData[0][siteID];
				var linkTabs = '';
				var links_count = crawlIssues[1].length;
				if (crawlIssues[1].length) {
					
					$.each(crawlIssues[1],function(index,issue){
						linkTabs = linkTabs+"<div class='row_updatee' civ='"+issue[0]+"'><div class='ci_tab'><div class='ci_url'>"+issue[1]+"</div></div><div class='ci_redirected_tab'><span class='redirect_from_to_arrow'>&#x21b3;</span><div class='ci_redirected_url' >"+issue[2]+"</div> <div class='ci_actions_panel'><ul><li class='wmt_redirect_again rep_sprite'><a title='Edit redirect'></a></li></ul></div></div> <div class='ci_edit_redirected_tab'><span class='redirect_from_to_arrow'>&#x21b3;</span><input class='ci_edit_redirected_url' value='"+issue[2]+"'/><div class='ci_actions_panel'><a class='wmt_redirect_again_ok rep_sprite' title='Save'>Save</a><a class='wmt_redirect_again_cancel' title='Cancel'>Cancel</a></div></div>  <div class='clear-both'></div></div>";
					});
				}else{
					linkTabs = "none";
				}
				if(linkTabs != 'none')
					content = content+"<div class='ind_row_cont'><div class='row_summary' sid='"+siteID+"'><div class='row_arrow'></div><div class='row_name searchable'><span class='links_count'>"+links_count+"</span>"+siteURL+"</div><div class='clear-both'></div></div><div class='row_detailed' sid='"+siteID+"' style='display:none;clear:both;'><div class='rh'><div class='row_arrow'></div><div class='row_name'><span class='links_count'>"+links_count+"</span>"+siteURL+"</div><div class='clear-both'></div></div><div class='rd'>"+linkTabs+"</div></div></div>";
			});
		}else if(act == 'dismissed'){
			parent.find('.select_gwmtignore_links').closest('ul').hide();
			parent.find('.gwm_status_applyChangesCheck ').closest('div').hide();
			$.each(gwmcontentData[1],function(siteID,crawlIssues){
				var siteURL = gwmcontentData[0][siteID];
				var linkTabs = '';
				var links_count = crawlIssues[2].length;
				if (crawlIssues[2].length) {
					
					$.each(crawlIssues[2],function(index,issue){
						linkTabs = linkTabs+"<div class='row_updatee' civ='"+issue[0]+"'><div class='ci_tab'><div class='ci_url'>"+issue[4]+"</div></div> <div class='ci_details_tab'><span class='ci_details' cont='issueType'>"+issue[2]+"</span>;<span class='ci_details' cont='detail'>"+issue[3]+"</span><span class='empty_block'></span> Issue detected on <span class='ci_details' cont='dateDetected'>"+issue[5]+"</span><span class='empty_block'></span> Last crawl on <span class='ci_details' cont='updated'>"+issue[6]+"</span><div class='ci_actions_panel'><ul><li class='wmt_reconsider rep_sprite'><a title='Consider'></a></li></ul></div></div> <div class='clear-both'></div></div>";
					});
				}else{
					linkTabs = "none";
				}
				if(linkTabs != 'none')
					content = content+"<div class='ind_row_cont'><div class='row_summary' sid='"+siteID+"'><div class='row_arrow'></div><div class='row_name searchable'><span class='links_count'>"+links_count+"</span>"+siteURL+"</div><div class='clear-both'></div></div><div class='row_detailed' sid='"+siteID+"' style='display:none;clear:both;'><div class='rh'><div class='row_arrow'></div><div class='row_name'><span class='links_count'>"+links_count+"</span>"+siteURL+"</div><div class='clear-both'></div></div><div class='rd'>"+linkTabs+"</div></div></div>";
			});
		}
	}
	if (issueType == 'all') {
		issue_no_message = 'Yipee! No crawl errors in the selected sites.';
	}else{
		issue_no_message = 'There are no '+issueType+' links in the selected sites.';
	}
	if(content == '') content = '<div class="gwmt_no_issues">'+issue_no_message+'</div>'
	$('.rows_cont').html(content);
}