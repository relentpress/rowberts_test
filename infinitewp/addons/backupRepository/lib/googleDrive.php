<?php

include '../../../includes/app.php';

try{
	//Main controller logic.
	googleDriveHelper::setPathsAndKeys();

	if ($_GET['action'] == 'revoke') {
		googleDriveHelper::$authHelper->revokeToken();

		//saving the details in profile
		$params = array();
		$params['repositoryID'] = $_GET['repositoryID'];				//getting the repository ID from $_GET
		backupRepositoryDeleteProfile($params);
		
		addNotification($type='N', $title='Google Drive Revoked', $message="Your Google Drive account has been revoked.", $state='U', $callbackOnClose='', $callbackReference='');
	}
	else if ($_GET['action'] == 'auth' || $_GET['code']) {
		googleDriveHelper::$authHelper->authenticate();

		if($_GET['code']){
		  sleep(1);
		  $getAccessToken = googleDriveHelper::$storage->get($accessToken);
		  
		  $getGoogleEmail = googleDriveHelper::getGoogleEmail();
		  
		  //saving the details in profile
		  $params['profileName'] = $getGoogleEmail;
		  $params['type'] = 'iwp_gdrive';
		  $params['status'] = 'grant';
		  $params['accountInfo'] = (array)json_decode($getAccessToken);
		  // backupRepositorySaveProfile($params);//no need to do here. its done below in else part
		}
	}
	else {
		if(googleDriveHelper::$storage->get($accessToken) ){
			googleDriveHelper::$authHelper->setTokenFromStorage();

			$getAccessToken = googleDriveHelper::$storage->get($accessToken);
			$getGoogleEmail = googleDriveHelper::getGoogleEmail();

			addNotification($type='N', $title='Google Drive Connected', $message='Your Google Drive account <span class="droid700">'.$getGoogleEmail.'</span> has been connected.', $state='U', $callbackOnClose='', $callbackReference='');
			
			//saving the details in profile
			$params['profileName'] = $getGoogleEmail;
			$params['type'] = 'iwp_gdrive';
			$params['status'] = 'grant';
			$params['accountInfo'] = (array)json_decode($getAccessToken);
			backupRepositorySaveProfile($params);

			echo '<div style="text-align:center"><div style="padding:50px 0 20px; font-size: 14px; font-family: Helvetica, Arial, sans-serif;">Go to your panel and confirm that you have connected your Google Drive account</div><img src="images/googleDrive_howto_confirm.png"></div>';
		}else{
			echo 'Request completed. You can close this window now.';
		}
	}
}
catch(Google_ServiceException $e){
	$cd = $e->getCode();
	$errors = $e->getErrors();
	addNotification($type='E', $title='Google Drive Service Exception (Error#'.$cd.')', $message=$errors[0]['message'], $state='U', $callbackOnClose='', $callbackReference='');
	//header("Location:".APP_URL); 
}
catch(Google_Exception $e){
	$cd = $e->getCode();
	$msg = $e->getMessage();
	addNotification($type='E', $title='Google Drive Exception (Error#'.$cd.')', $message=$msg, $state='U', $callbackOnClose='', $callbackReference='');
	//header("Location:".APP_URL); 
}