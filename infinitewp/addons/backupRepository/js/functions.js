scheduleRepoEditData='';
repositoryAddonFlag=1;
dpConsumerKey='p0kr57ithemrzj4';
dpConsumerSecret='j49zdyzxjebb0t2';
function loadProfiles(type)
{
	$(".dropdown_cont, .save_new_profile").show();
	$("#backupNow").removeClass('disabled');
	$("#selectProfileBtn").show();
var tempArray={},profileVar='';
tempArray['requiredData']={};
tempArray['requiredData']['backupRepositoryGetAllProfiles']={};
tempArray['requiredData']['backupRepositoryGetAllProfiles']['type']=type;
if(type=="iwp_ftp")
profileVar='FTP';
else if(type=="iwp_amazon_s3")
profileVar='Amazon S3';
	else if(type=="iwp_dropbox")
profileVar='Dropbox';
	else if(type=="iwp_gdrive")
	profileVar='Google Drive';
$(".dialog_cont #profileDropName").text('Select '+profileVar+' profile');
doCall(ajaxCallPath,tempArray,'loadProfileDropDown');
}
function loadDropbox(data)
{
if(data==undefined || data=='')
{
$(".dropdown_cont, .save_new_profile").hide();
$("#dropboxFolderInfo").show();
var tempArray={};
tempArray['requiredData']={};
tempArray['requiredData']['backupRepositoryGetDropBoxKeys']=1;
doCall(ajaxCallPath,tempArray,'loadDropbox');
}
else
{
	$("#dbTitle .dbActionBtn").remove();
	
	data=data.data.backupRepositoryGetDropBoxKeys;
	
	if(data==false)
	{
$("#dropboxRepo #dbTitle").append('<div class="btn_action dbActionBtn"><a class="rep_sprite btn_blue" style="margin: 20px 55px;" href="'+systemURL+'addons/backupRepository/lib/dropbox.php" target="_blank" id="dbConnected">Connect to my Dropbox &amp; Authorize</a></div>');
$("#backupNow").addClass('disabled');
	}
	else
	{
	$("#dropboxRepo #dbTitle").append('<div style="text-transform: none;  padding: 10px; line-height: 14px; font-weight: 400; font-size: 12px;" class="dbActionBtn">Your <span style="font-weight: 700;">'+data.email+'</span> account has been connected.<br />To connect a different account, <a href="'+systemURL+'addons/backupRepository/lib/dropbox.php" target="_blank" id="dbConnected">click here</a>.</div> <input type="hidden" id="oauth_token" class="required formVal" value="'+data.token+'"><input type="hidden" id="oauth_token_secret" class="required formVal" value="'+data.token_secret+'">');
	$("#backupNow").removeClass('disabled');
	}
	
}
}
function loadServer()
{
	$(".dropdown_cont, .save_new_profile").hide();
	$(".backupRepoTestFooter").hide();
	$("#backupNow").removeClass('disabled');
}
function loadProfileDropDown(data)
{
	data=data.data.backupRepositoryGetAllProfiles;
	var content='';
	if(getPropertyCount(data)>0)
	{
		$.each(data, function(val,object){
		content=content+'<li><a class="loadProfile dropOption" dropopt="'+object.repositoryID+'">'+object.profileName+'<div class="rep_sprite remove_bg"><span class="rep_sprite_backup del delProfile" repoid="'+object.repositoryID+'"></span></div> </a></li>';
		});
	}
	if(content=='')
	content='<li class="emptyCont"><a>No profile created yet.</a></li>';
	$(".dialog_cont #selectProfileOptions").html(content).hide();
	
}
function getRepoDetails(repoID)
{
	var tempArray={};
	tempArray['requiredData']={};
tempArray['requiredData']['backupRepositoryGetProfile']={};
tempArray['requiredData']['backupRepositoryGetProfile']['repositoryID']=repoID;
$(".dialog_cont .updateProfile").attr('repoid',repoID).show();
doCall(ajaxCallPath,tempArray,'populateRepoFields');

}
function populateRepoFields(data)
{
	data=data.data.backupRepositoryGetProfile;
	addRepoFields(data);
}
function addRepoFields(data)
{
	if(getPropertyCount(data)>0)
	{
		$.each(data, function(val,object){
		if(val!='ProfileName' && val!='type')
		{
		
						if($(".dialog_cont #"+val).hasClass('dropdown'))
			{
				
				$(".dialog_cont #"+val).text($(".dialog_cont #"+val+"Options [dropopt='"+object+"']").text()).attr('dropopt',object);
			}
			else if($(".dialog_cont #"+val).hasClass('checkbox'))
			{
				if(object==1)
				$(".dialog_cont #"+val).addClass('active');
			}
			else
			$(".dialog_cont #"+val).val(object);
			
					
		}
		
		});
	}
}

function processProfileAction(data)
{
	$(".dialog_cont .profileStatusDiv").remove();
	$('.dialog_cont .profileActionBtn').removeClass('disabled');
	if(data.data.backupRepositorySaveProfile!=undefined)
	{
		if((data.data.backupRepositorySaveProfile.status!=undefined && data.data.backupRepositorySaveProfile.status=="success") || data.data.backupRepositorySaveProfile==true )
		{
		if(data.data.backupRepositorySaveProfile.status=="success")
		{
			$(".dialog_cont #selectProfileOptions").append('<li><a class="loadProfile dropOption" dropopt="'+data.data.backupRepositorySaveProfile.repositoryID+'">'+data.data.backupRepositorySaveProfile.profileName+'<div class="rep_sprite remove_bg"><span class="rep_sprite_backup del delProfile" repoid="'+data.data.backupRepositorySaveProfile.repositoryID+'"></span></div> </a></li>');
			$(".dialog_cont #selectProfileOptions .emptyCont").remove();
			$(".dialog_cont #profileDropName").text(data.data.backupRepositorySaveProfile.profileName);
		}
		$(".dialog_cont .profile_cont").prepend('<div class="create_profile_status success profileStatusDiv">&nbsp;</div>');
		}
		else
		{
		var tempErrorMsg;
		if(data.data.backupRepositorySaveProfile.status!=undefined && data.data.backupRepositorySaveProfile.status=="error")
		tempErrorMsg=data.data.backupRepositorySaveProfile.errorMsg;
		else
		tempErrorMsg=data.data.backupRepositorySaveProfile;
		$(".dialog_cont .profile_cont").prepend('<div class="create_profile_status error profileStatusDiv">'+tempErrorMsg+'</div>');
		
		}
	}
	
}
function processRepoTestConnection(data)
{
	processTestConnection(data,"completeRepository","testConnection");
}

function closeProfileDropDown()
{
$(".dialog_cont #selectProfileOptions").hide();
$(".dialog_cont #selectProfileBtn").removeClass('open');
}

function processGoogleCredsSave(data)
{
	if((typeof data.data.backupRepositoryGetGoogleDriveCreds != 'undefined')&&(data.data.backupRepositoryGetGoogleDriveCreds != false))
	{
		var googleKeys = data.data.backupRepositoryGetGoogleDriveCreds;
		$("#gdrive_access_key").val(googleKeys['clientID']);
		$("#gdrive_secure_key").val(googleKeys['clientSecretKey']);
		$("#gdrive_redirect_url").val(googleKeys['redirectURL']);
	}
	$(".gDriveCredsPage").hide();
	loadGoogleDrive(data)
}

function loadGoogleDrive(data)
{
	$("#selectProfileBtn").hide();
	$(".save_new_profile").hide();
	if((typeof data.data.backupRepositoryGetAllProfiles != 'undefined')&&(data.data.backupRepositoryGetAllProfiles != false))
	{
		gDrivePopulateProfiles(data);
	}else{
		$(".gDriveProfilePage").hide();
		$("#backupNow").addClass('disabled');
		if((typeof data.data.backupRepositoryGetGoogleDriveCreds != 'undefined')&&(data.data.backupRepositoryGetGoogleDriveCreds != false))
		{
			var googleKeys = data.data.backupRepositoryGetGoogleDriveCreds;
		$("#gdrive_access_key").val(googleKeys['clientID']);
		$("#gdrive_secure_key").val(googleKeys['clientSecretKey']);
			$("#gdrive_redirect_url").val(googleKeys['redirectURL']);//http://localhost/mainPanel/addons/backupRepository/lib/googleDrive.php
		
		$(".gDriveConnectPage").show();
		$("#afterConnectGDrive").hide();
		$("#connectGDrive").show();
		}else{
			$(".gDriveCredsPage").show();
	}
	}
	centerDialog();
	//for schedule backup Edit
	if((typeof scheduleRepoEditData != "undefined")&&(scheduleRepoEditData.type == "iwp_gdrive"))
	{
		$(".gDriveLoadProfile").each(function(){
			var thisObj = $(this);
			if($(this).attr("dropopt") == scheduleRepoEditData.gDriveEmail)
			{
				thisObj.click();
			}
		});
	}
}

function gDrivePopulateProfiles(data)
{
	$(".gDriveConnectPage").hide();
	$(".gDriveProfilePage").show();
	$(".gDriveDropdown").show();
	data=data.data.backupRepositoryGetAllProfiles;
	var content='';
	if(getPropertyCount(data)>0)
	{
		$.each(data, function(val,object){
		// content=content+'<li><a class="gDriveLoadProfile dropOption" style="width: 220px;"dropopt="'+object.repositoryID+'">'+object.profileName+'<div class="rep_sprite remove_bg"><span class="rep_sprite_backup del delProfile" repoid="'+object.repositoryID+'"></span></div> </a></li>';
		content=content+'<li><a class="gDriveLoadProfile dropOption" style="width: 220px;"dropopt="'+object.repositoryID+'">'+object.profileName+' </a></li>';
		$("#gDriveEmail").text(object.profileName).attr("dropopt", object.repositoryID);
		$("#gDriveDisconnect").attr("href", systemURL+"addons/backupRepository/lib/googleDrive.php?action=revoke&repositoryID="+object.repositoryID);
		});
	}
	if(content=='')
	content='<li class="emptyCont"><a>No profile connected yet.</a></li>';
	$(".dialog_cont #gDriveEmailOptions").html(content).hide();
	$("#backupNow").removeClass('disabled');
}


