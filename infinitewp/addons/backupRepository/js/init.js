$(".repBtn").live('click',function() { 
$("#dropboxFolderInfo").hide();
$(".updateProfile,.profileStatusDiv",".dialog_cont ").hide();
$(".backupRepoTestFooter").show();
$("#testConnection").removeClass("testing success error");
if($(this).attr('rep')=='server')
loadServer();
else if($(this).attr('rep')=='FTP')
loadProfiles('iwp_ftp');
else if($(this).attr('rep')=='amazon')
loadProfiles('iwp_amazon_s3');
else if($(this).attr('rep')=='dropbox')
loadDropbox();
else if($(this).attr('rep')=='gDrive')
{	
	var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['backupRepositoryGetGoogleDriveCreds']={};
	tempArray['requiredData']['backupRepositoryGetGoogleDriveCreds']=1;
	tempArray['requiredData']['backupRepositoryGetAllProfiles']={};
	tempArray['requiredData']['backupRepositoryGetAllProfiles']['type']="iwp_gdrive";
	doCall(ajaxCallPath,tempArray,'processGoogleCredsSave');
}
$(".dialog_cont .repoForms").hide();
$(".dialog_cont #"+$(this).attr('rep')+"Repo").show();
centerDialog();
});

$(".createProfile").live('click',function() {  //Modified for update


if(($(".dialog_cont #newProfileName").val()=='' || $(".dialog_cont #newProfileName").val()=='new profile') && !$(this).hasClass('updateProfile'))
{
$(".dialog_cont #newProfileName").addClass('error');
return false;
}

var checkForm=validateForm($(".dialog_cont .repBtn.active").attr('rep')+"Repo");
if(checkForm!=false)
{

var tempArray={};
tempArray['requiredData']={};

tempArray['requiredData']['backupRepositorySaveProfile']={};
tempArray['requiredData']['backupRepositorySaveProfile']['type']=$(".dialog_cont .repBtn.active").attr('repType');
tempArray['requiredData']['backupRepositorySaveProfile']['profileName']=$(".dialog_cont #newProfileName").val();
tempArray['requiredData']['backupRepositorySaveProfile']['accountInfo']=checkForm;
if($(this).hasClass('updateProfile'))
{
tempArray['requiredData']['backupRepositorySaveProfile']['repositoryID']=$(this).attr('repoid');
tempArray['requiredData']['backupRepositorySaveProfile']['profileName']=$(".dialog_cont #profileDropName").text();
}

tempArray['requiredData']['backupRepositorySaveProfile']['status']="active";
$('.dialog_cont .profileActionBtn').addClass('disabled');
$(".dialog_cont .profile_cont").prepend('<div class="create_profile_status loading profileStatusDiv"></div>');
doCall(ajaxCallPath,tempArray,'processProfileAction');
}

});
$(".loadProfile").live('click',function() { 
getRepoDetails($(this).attr('dropopt'));

});
$(".delProfile").live('click',function() { 
tempArray={};
tempArray['requiredData']={};
tempArray['requiredData']['backupRepositoryDeleteProfile']={};
tempArray['requiredData']['backupRepositoryDeleteProfile']['repositoryID']=$(this).attr('repoid');
if($(this).closest('.loadProfile').text()==$(".dialog_cont #profileDropName").text())
$(".dialog_cont #profileDropName").text("Select "+$(".dialog_cont .repBtn.active").attr('rep')+" profile");
$(this).closest('li').remove();
if($(".dialog_cont #selectProfileOptions li").length==0)
{
$(".dialog_cont #profileDropName").text("Select "+$(".dialog_cont .repBtn.active").attr('rep')+" profile");
$(".dialog_cont #selectProfileOptions").html('<li class="emptyCont"><a>No profile created yet.</a></li>');
}
doCall(ajaxCallPath,tempArray,'processProfileAction');
return false;
}).live('mouseenter',function() {
	$(this).closest("a").addClass('delWarn');
}).live('mouseleave',function() {
	$(this).closest("a").removeClass('delWarn');
});
$("#testConnection").live('click',function() { 
$(".dialog_cont #completeRepository .inner_cont .conn_test_error_cont").remove();
if(!$(this).hasClass('testing'))
{
$(this).removeClass('error success');
var checkForm=validateForm($(".dialog_cont .repBtn.active").attr('rep')+"Repo");
if(checkForm!=false)
{
tempArray={};
tempArray['requiredData']={};
tempArray['requiredData']['repositoryTestConnection']={};
tempArray['requiredData']['repositoryTestConnection'][$(".dialog_cont .repBtn.active").attr('repType')]=checkForm;
doCall(ajaxCallPath,tempArray,'processRepoTestConnection');
$(this).addClass('testing');
}
}

});
$(".formVal, .validateField").live('focus',function() { 
$(this).removeClass('error');
});
$(".disabled #newProfileName").live('focus',function() { 
$(this).blur();
});
$(".e_close").live('click',function() { 
$(this).closest('.conn_test_error_cont').remove();
});

$("#dbConnected").live('click',function() { 
setTimeout(function () { $("#dbTitle .dbActionBtn").remove();$("#dropboxRepo #dbTitle").append('<div class="btn_action dbActionBtn"><a class="rep_sprite btn_blue" style="margin: 20px 70px;" id="dbConfirm">Yes, I have authorized this panel</a></div>')},100);
});
$("#dbConfirm").live('click',function() { 
loadDropbox();
});

$("#google_drive_save").live('click',function(){
	var params = {};
	params['clientID'] = $("#gdrive_access_key").val();
	params['clientSecretKey'] = $("#gdrive_secure_key").val();
	params['redirectURL'] = $("#gdrive_redirect_url").val();
	var tempArray={};
	tempArray['requiredData']={};
	tempArray['requiredData']['backupRepositorySaveGoogleDriveCreds']={};
	tempArray['requiredData']['backupRepositorySaveGoogleDriveCreds']=params;
	tempArray['requiredData']['backupRepositoryGetGoogleDriveCreds']={};
	tempArray['requiredData']['backupRepositoryGetGoogleDriveCreds']=1;
	tempArray['requiredData']['backupRepositoryGetAllProfiles']={};
	tempArray['requiredData']['backupRepositoryGetAllProfiles']['type']="iwp_gdrive";
	
	doCall(ajaxCallPath, tempArray, 'processGoogleCredsSave');
});

$("#gdrive_redirect_url").live('click',function(){
	$(this).select();
});

$("#connectGDrive").live('click',function() {
	setTimeout(function () { $("#afterConnectGDrive").show(); },200);
	setTimeout(function () { $("#connectGDrive").hide() },200);
});

$("#afterConnectGDrive").live('click',function(){
	var tempArray={},profileVar='';
	tempArray['requiredData']={};
	tempArray['requiredData']['backupRepositoryGetAllProfiles']={};
	tempArray['requiredData']['backupRepositoryGetAllProfiles']['type']="iwp_gdrive";
	doCall(ajaxCallPath,tempArray,'gDrivePopulateProfiles');
});

$(".gDriveLoadProfile").live('click',function(){
	var repID = $(this).attr("dropopt");
	$("#gDriveDisconnect").attr("href", systemURL+"addons/backupRepository/lib/googleDrive.php?action=revoke&repositoryID="+repID);
});

$(".gDriveAddAnotherAccount").live('click',function(){
	$(".gDriveCredsPage").hide();
	$(".gDriveProfilePage").hide();
	$(".gDriveConnectPage").show();
	$("#afterConnectGDrive").hide();
	$("#connectGDrive").show();
});

$(".gDriveEditAPI").live('click',function(){
	$(".gDriveConnectPage").hide();
	$(".gDriveProfilePage").hide();
	$(".gDriveCredsPage").show();
});

$("#gDriveDisconnect").live('click',function(){
	var thisObj = $(".repBtn.active");
	setTimeout(function () { $(thisObj).removeClass("active"); $(thisObj).click() },200);
});
$(function () {
    $("#use_sftp").live('click',function() {
            var hostPort = $("#ftp_port").val();
            if($("#use_sftp").hasClass('active')) {
                if(parseInt(hostPort)==21) {
                    $("#ftp_port").val('22');
                }
            } else {
                if(parseInt(hostPort)==22) {
                    $("#ftp_port").val('21');
                }
            }
    });
});
