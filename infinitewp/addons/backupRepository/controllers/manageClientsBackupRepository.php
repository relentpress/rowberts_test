<?php
class manageClientsBackupRepository{

	public static function backupRepositoryProcessor($siteIDs, $params){//to after backup now completed, to upload backup file to FTP/S3/DropBox this method is used
		
		$type = "backupRepository";
		$action = "upload";
		$requestAction = "backup_repository";  //"remote_backup_now";
		$timeout = (20 * 60);
		
		$requestparams = array();
		
		foreach($siteIDs as $siteID){
			  $siteData = getSiteData($siteID);
			  $events=1;
		
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => $params['repository'], 'detailedAction' => $type);
		$events=1;
		
		$PRP = array();
		$PRP['requestAction'] 	= $requestAction;
		$PRP['requestParams'] 	= $requestParams;
		$PRP['siteData'] 		= $siteData;
		$PRP['type'] 			= $type;
		$PRP['action'] 			= $action;
		$PRP['events'] 			= $events;
		$PRP['historyAdditionalData'] 	= $historyAdditionalData;
		$PRP['timeout'] 		= $timeout;
		
		prepareRequestAndAddHistory($PRP);
	   }
	}
	
	public static function backupRepositoryResponseProcessor($historyID, $responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		
		if(empty($responseData['success'])){
			return false;
		}

		if(!empty($responseData['success'])){
			DB::update("?:history_additional_data", "status='success'", "historyID=".$historyID."");
		}
		
		$siteID = DB::getField("?:history", "siteID", "historyID=".$historyID);
	
		$allParams = array('action' => 'getStats', 'args' => array('siteIDs' => array($siteID), 'extras' => array('sendAfterAllLoad' => false, 'doNotShowUser' => true)));
		
		panelRequestManager::handler($allParams);
	}
}
manageClients::addClass('manageClientsBackupRepository');

?>