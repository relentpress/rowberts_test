<?php

function backupRepositorySaveProfile($params){
	if(empty($params)){
		return false;
	}
	$accountInfo = serialize($params['accountInfo']);
	if(empty($params['repositoryID'])){
		$query = DB::insert("?:backup_repository", array('profileName' => $params['profileName'], 'type' => $params['type'], 'credentials' => $accountInfo, 'status' => $params['status']));
		if(!empty($query)){
			$data = array();
			$data['status'] = 'success';
			$data['repositoryID'] = $query;
			$data['profileName'] = DB::getField("?:backup_repository","profileName", "repositoryID='".$query."'"); 
			return $data;
		}
		else{
			return array('status' => 'error', 'errorMsg' => 'Oops! Try again.');
		}
	}
	else{
		$query = DB::update("?:backup_repository", array('profileName' => $params['profileName'], 'type' => $params['type'], 'credentials' => $accountInfo, 'status' => $params['status']), "repositoryID='".$params['repositoryID']."'");
		if(!empty($query)){
			return $query;
		}
		else{
			return "Profile already exists.";
		}
	}
}

function backupRepositoryGetProfile($params){
	if(empty($params)){
		return false;
	}
	
	$datas = array();
	$data = DB::getRow("?:backup_repository", "*", "repositoryID='".$params['repositoryID']."'");
	$datas = unserialize($data['credentials']);
	$datas['ProfileName'] = $data['profileName'];
	$datas['type'] = $data['type']; 
	
	if(!empty($datas)){
		return $datas;
	}
	else{
		return false;
	}
}

function backupRepositoryGetAllProfiles($params){
	$profiles = DB::getArray("?:backup_repository", "repositoryID, profileName","type='".$params['type']."'");
	if(!empty($profiles)){
		return $profiles;
	}
	else{
		return false;
	}
}

function backupRepositoryDeleteProfile($params){
	return DB::delete("?:backup_repository", "repositoryID='".$params['repositoryID']."'");
}

function backupRepositoryResponseProcessors(&$responseProcessor){
	$responseProcessor['backupRepository']['upload'] = 'backupRepository';
}

function backupRepositoryTaskTitleTemplate(&$template){
	$template['backupRepository']['upload']['']	= "Upload backup to <#uniqueName#>";
}

function backupRepositoryGetDropBoxKeys(){
	
	$DropBoxTokens = getOption('DropBoxTokens');
	
	if(empty($DropBoxTokens)){
		return false;
	}
	return @unserialize($DropBoxTokens);
}

function backupRepositorySaveGoogleDriveCreds($params){
	updateOption('googleDriveCredentials', serialize($params));
}

function backupRepositoryGetGoogleDriveCreds(){
	$params = getOption('googleDriveCredentials');
	return unserialize($params);
}

function backupRepositorySetGoogleDriveArgs($initialArgs)
{
	$googleArgs = array();
	$googleCreds = array();
	$params['repositoryID'] = $initialArgs['gDriveEmail'];
	$g_token = backupRepositoryGetProfile($params);
	$googleCreds = backupRepositoryGetGoogleDriveCreds();
	$googleArgs['token'] = $g_token;
	$googleArgs['clientID'] = $googleCreds['clientID'];
	$googleArgs['clientSecretKey'] = $googleCreds['clientSecretKey'];
	$googleArgs['redirectURL'] = $googleCreds['redirectURL'];
	$googleArgs['gdrive_site_folder'] = $initialArgs['gdrive_site_folder'];
	return $googleArgs;
}

class googleDriveHelper{
	
	public static $client;
	public static $authHelper;
	public static $storage;
	private static $GDThisPage;
	private static $redirectURL;
	private static $isSetPathsAndKeysComplete = false;
	private static $oauth;
	
	public static function setPathsAndKeys(){
		
		if(self::$isSetPathsAndKeysComplete) return true;
		
		include_once(APP_ROOT.'/lib/googleAPIs/src/Google_Client.php');
		include_once(APP_ROOT.'/lib/googleAPIs/src/contrib/Google_DriveService.php');
		include_once(APP_ROOT.'/lib/googleAPIs/src/contrib/Google_Oauth2Service.php');
		//include_once(APP_ROOT.'/lib/googleAPIs/src/service/Drive.php');
		//include_once(APP_ROOT.'/lib/googleAPIs/src/service/Resource.php');
		include_once(APP_ROOT.'/lib/googleAPIs/storage.php');
		include_once(APP_ROOT.'/lib/googleAPIs/authHelper.php');
		
		self::$GDThisPage = APP_URL.'addons/backupRepository/lib/googleDrive.php';
		self::$redirectURL = APP_URL.'addons/backupRepository/lib/googleDrive.php';
		
		
		$googleKeys = getOption('googleDriveCredentials');							//get the creds from option table.
		$googleKeys = unserialize($googleKeys);
		
		$googleAPIKeys = array();
		$googleAPIKeys = backupRepositoryGetGoogleDriveCreds();
		/* $googleAPIKeys['clientID'] = '657972506832.apps.googleusercontent.com';
		$googleAPIKeys['clientSecretKey'] = 'FQ5fS20QmyGLE-3UpJisHo8F'; */
		/* $googleAPIKeys['clientID'] = $googleKeys['clientID'];
		$googleAPIKeys['clientSecretKey'] = $googleKeys['clientSecretKey']; */
		
		
		/* if(!empty($googleAPIKeys)){
			$googleAPIKeys = unserialize($googleAPIKeys);
		} */
	
		// Build a new client object to work with authorization.
		self::$client = new Google_Client();
		self::$client->setClientId($googleAPIKeys['clientID']);

		self::$client->setClientSecret($googleAPIKeys['clientSecretKey']);
		self::$client->setRedirectUri(self::$redirectURL);
		self::$client->setApplicationName('InfiniteWP Google Drive');
		self::$client->setScopes(array(
		  'https://www.googleapis.com/auth/drive',
		  'https://www.googleapis.com/auth/userinfo.email'));
		
		// Magic. Returns objects from the Analytics Service
		// instead of associative arrays.
		self::$client->setUseObjects(true);
		
		self::$oauth = new Google_Oauth2Service(self::$client);
		
		
		// Build a new storage object to handle and store tokens in sessions.
		// Create a new storage object to persist the tokens across sessions.
		self::$storage = new apiSessionStorage(); //googleAnalyticsStorageHelper();
		
		self::$authHelper = new AuthHelper(self::$client, self::$storage, self::$GDThisPage);
		
		self::$isSetPathsAndKeysComplete = true;

	}
	
	public static function testConnectionGoogle($args){
		
		include_once(APP_ROOT.'/lib/googleAPIs/src/Google_Client.php');
		include_once(APP_ROOT.'/lib/googleAPIs/src/contrib/Google_DriveService.php');
		include_once(APP_ROOT.'/lib/googleAPIs/src/contrib/Google_Oauth2Service.php');
		include_once(APP_ROOT.'/lib/googleAPIs/storage.php');
		include_once(APP_ROOT.'/lib/googleAPIs/authHelper.php');
		
		$gDriveArgs = backupRepositorySetGoogleDriveArgs($args);
		$accessToken = $gDriveArgs['token'];
		
		$client = new Google_Client();
		$client->setClientId($gDriveArgs['clientID']);

		$client->setClientSecret($gDriveArgs['clientSecretKey']);
		$client->setRedirectUri($gDriveArgs['redirectURL']);
		$client->setScopes(array(
		  'https://www.googleapis.com/auth/drive',
		  'https://www.googleapis.com/auth/userinfo.email'));
		
		$accessToken = $gDriveArgs['token'];
		$refreshToken = $accessToken['refresh_token'];
		
		try
		{
			$client->refreshToken($refreshToken);
			return array('status' => 'success');
		}
		catch(Exception $e)
		{	
			echo 'google Error ',  $e->getMessage(), "\n";
			return array('status' => 'error', 'errorMsg' => $e->getMessage());
		}
	}
	
	public static function getGoogleEmail(){
	
		
		
		$get = self::$oauth->userinfo->get();
		$getEmail = $get->email;
			
		return $getEmail;
		
	}
	
}



?>