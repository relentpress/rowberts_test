<?php

/*
slug: backupRepository
version: 1.0.5
*/

class addonBackupRepository{
	
	private static $version = '1.0.5';

	public static function version(){
		return self::version;	
	}
	
	public static function init(){
		require_once(APP_ROOT."/addons/backupRepository/controllers/func.php");	
		require_once(APP_ROOT."/addons/backupRepository/controllers/manageClientsBackupRepository.php");
		panelRequestManager::addFunctions('backupRepositoryTestConnection', 'backupRepositorySaveProfile', 'backupRepositoryGetProfile', 'backupRepositoryGetAllProfiles', 'backupRepositoryDeleteProfile', 'backupRepositoryGetDropBoxKeys', 'backupRepositoryUpdateDropBoxKeys', 'backupRepositorySaveGoogleDriveCreds', 'backupRepositoryGetGoogleDriveCreds');
		regHooks('responseProcessors', 'taskTitleTemplate');
	}	
	
	public static function install(){
		
		$Q1 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:backup_repository` (
  `repositoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profileName` varchar(50) NOT NULL,
  `type` varchar(255) NOT NULL,
  `credentials` text NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  `errorNo` int(10) unsigned DEFAULT NULL,
  `errorMsg` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`repositoryID`),
  UNIQUE KEY `profileName` (`profileName`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");

		if($Q1){
			return true;	
		}
		return false;
	}
}

?>