<?php

function manageUsersAddonMenus(&$menus){
	//$menus[] = '<li><a class="navLinks optionSelect" page="userManagement">Users</a></li>';
	$menus['manage']['subMenus'][] = array('page' => 'userManagement', 'displayName' => 'Users');
}

function manageUsersResponseProcessors(&$responseProcessor){
	$responseProcessor['manageUsers']['add'] = 'manageUsersAdd';
	$responseProcessor['manageUsers']['get'] = 'manageUsersGet';
	$responseProcessor['manageUsers']['changeRole'] = $responseProcessor['manageUsers']['changePassword'] = $responseProcessor['manageUsers']['delete'] = 'manageUsersUpdate';
	
}

function manageUsersTaskTitleTemplate(&$template){
	$template['manageUsers']['add']['']	= "Create user in <#sitesCount#> site<#sitesCountPlural#>";
	$template['manageUsers']['get']['']	= "Load users from <#sitesCount#> site<#sitesCountPlural#>";
	$template['manageUsers']['changeRole']['']	= "Change role for <#detailedActionCount#> user<#typePlural#> in <#sitesCount#> site<#sitesCountPlural#>";
	$template['manageUsers']['changePassword']['']	= "Change password for <#detailedActionCount#> user<#typePlural#> in <#sitesCount#> site<#sitesCountPlural#>";
	$template['manageUsers']['delete']['']	= "Delete <#detailedActionCount#> user<#typePlural#> in <#sitesCount#> site<#sitesCountPlural#>";
}

function manageUsersGet(){
	
	$actionID = Reg::get('currentRequest.actionID');
		
	$userDatas = DB::getFields("?:temp_storage", "data", "type = 'getUsers' AND paramID = '".$actionID."'");
		
	DB::delete("?:temp_storage", "type = 'getUsers' AND paramID = '".$actionID."'");
		
	if(empty($userDatas)){
		return array();
	}
	$finalData = array();
	foreach($userDatas as $data){
		$finalData = array_merge_recursive($finalData, (array)unserialize($data));	
	}
	arrayMergeRecursiveNumericKeyHackFix($finalData);
	
	return $finalData;
}

?>