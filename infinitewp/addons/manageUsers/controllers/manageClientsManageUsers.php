<?php
//add new user to site.
class manageClientsManageUsers{
		
	public static function manageUsersAddProcessor($siteIDs, $params){
		
		$type = "manageUsers";
		$action = "add";
		$requestAction = "add_user";
			
			$requestParams = array('user_login' => $params['userLogin'],'user_pass' => $params['userPass'],'first_name' => $params['firstName'],'last_name' => $params['lastName'],'user_email' => $params['userEmail'],'role' => $params['role'],'user_url' => $params['userURL'],'email_notify' => $params['emailNotify']);	
			
			$historyAdditionalData = array();
			$historyAdditionalData[] = array('uniqueName' => $params['userLogin'], 'detailedAction' => $type);
			
			foreach($siteIDs as $siteID){
			$siteData = getSiteData($siteID);
			
			$events = 1;
			
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
		  
			prepareRequestAndAddHistory($PRP);
		}
	}
	
	public static function manageUsersAddResponseProcessor($historyID, $responseData){
	
		responseDirectErrorHandler($historyID, $responseData);
		
		if(empty($responseData['success'])){
			return false;
		}
		
		$updateData = array();
		if( !empty($responseData['success']) ){
			$updateData['resultID'] = $responseData['success'];
			$updateData['status'] = 'success';
			DB::update("?:history_additional_data", $updateData, "historyID=".$historyID);
		}		
	}
	
	//get users from site.
	public static function manageUsersGetProcessor($siteIDs, $params){ //FIX ME: add historyAdditionalData
		
		$type = "manageUsers";
		$action = "get";
		$requestAction = "get_users";
		$requestParams = array('user_roles' => $params['userRole']);
		
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => $type, 'detailedAction' => $action);
	   
		foreach($siteIDs as $siteID){
			$siteData = getSiteData($siteID);
			
			$events=1;
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['doNotExecute'] 	= false;
			$PRP['sendAfterAllLoad'] = true;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
		  
		  prepareRequestAndAddHistory($PRP);
		}          		
	}
	
	public static function manageUsersGetResponseProcessor($historyID,$responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		
		if(empty($responseData['success'])){
			return false;
		}
		if( !empty($responseData['success']['users']) ){
			
			$historyData = DB::getRow("?:history", "type, actionID, siteID", "historyID=".$historyID);
			$siteID = $historyData['siteID'];
			$actionID = $historyData['actionID'];
			$users = array();
			
			
			foreach($responseData['success']['users'] as $key => $value){
				foreach($value as $k => $v){
					if(!empty($v) && is_array($v)){
						foreach($v as $a){
							foreach($responseData['success']['users']['request_roles'] as $roles){
								if($a != $roles){ continue; }
								$users['siteView']['_'.$siteID][$a][$value['user_login']] = $value;
								$users['userView'][$value['user_login']][$a]['_'.$siteID] = $value;
							}
						}
					}
				}
			}
			
			DB::insert("?:temp_storage", array('type' => 'getUsers', 'paramID' => $actionID, 'time' => time(), 'data' =>  serialize($users)));
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID);
			return;
		}
		DB::update("?:history_additional_data", array('status' => 'error'), "historyID=".$historyID);
	}
	
	// manage users role and password .
	public static function manageUsersUpdateProcessor($siteIDs, $params){
		
		$actionString = array('change-role' => 'changeRole', 'change-password' => 'changePassword', 'delete-user' => 'delete');
		
		$type = "manageUsers";
		$action = $actionString[ $params['userEditAction'] ];
		$requestAction = "edit_users";
		$events = 0;
		
		foreach($params as $param){
			foreach($param as $siteID => $value){
				$historyAdditionalData = array();
				$users = array();
				foreach($value as $user){
					$users[] = $user;
					$events++;
					$historyAdditionalData[] = array('uniqueName' => $user, 'detailedAction' => $actionString[ $params['userEditAction'] ]);
				}
				
				$requestParams = array('users' => $users, 'user_edit_action' => $params['userEditAction'], 'new_role' => $params['newRole'], 'new_password' => $params['newPassword'], 'reassign_user' => $params['reassignUser']);
				$siteData = getSiteData($siteID);
				$PRP = array();
				$PRP['requestAction'] 	= $requestAction;
				$PRP['requestParams'] 	= $requestParams;
				$PRP['siteData'] 		= $siteData;
				$PRP['type'] 			= $type;
				$PRP['action'] 			= $action;
				$PRP['events'] 			= $events;
				$PRP['historyAdditionalData'] 	= $historyAdditionalData;
				
				prepareRequestAndAddHistory($PRP);
			}
		}
	}
	
	public static function manageUsersUpdateResponseProcessor($historyID, $responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		if(empty($responseData['success'])){
			return false;
		}
		
		$action = DB::getField("?:history", "action", "historyID=".$historyID);
		
		foreach($responseData['success'] as $userLogin => $result){
			$updateData = array();
			if(!empty($result['error'])){
				$updateData['status'] = 'error';
				$updateData['errorMsg'] = $result['error'];
			}
			else{
				$updateData['status'] = 'success';
				if($action != 'delete'){
					$updateData['resultID'] = $result;
				}
			}
			DB::update("?:history_additional_data", $updateData, "historyID = ".$historyID." AND uniqueName = '".$userLogin."'");
		}
	}
}
manageClients::addClass('manageClientsManageUsers');

?>