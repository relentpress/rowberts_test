<?php

/*
slug: manageUsers
version: 1.0.3
*/

class addonManageUsers{
	
	private static $version = '1.0.3';
	
	public static function version(){
		return self::version;	
	}
	
	public static function init(){
		require_once(APP_ROOT."/addons/manageUsers/controllers/func.php");	
		require_once(APP_ROOT."/addons/manageUsers/controllers/manageClientsManageUsers.php");	
		panelRequestManager::addFunctions('manageUsersGet');
		regHooks('addonMenus', 'responseProcessors', 'taskTitleTemplate');
	}	
}

?>