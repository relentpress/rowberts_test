var manageUsers;
function loadUserManagement(data)
{
	var content='<div class="site_nav_sub"> <ul> <li><a class="manage optionSelect active">MANAGE</a></li> <li><a id="addNewUser">CREATE NEW USER</a></li> <div class="clear-both"></div> </ul> </div> <div class="steps_hdr">Select websites to <span id="processType">Manage</span> <span class="itemUpper">Users</span></div><div class="siteSelectorContainer">'+siteSelectorVar+'</div><div class="result_block shadow_stroke_box siteSearch itemPanel" style="border:0;box-shadow: 0 0 0  rgba(0, 0, 0, 0)"><div class="usersDataContent" style="display:none"></div>';
	$("#pageContent").html(content);
	loadUserManagementData();
	siteSelectorNanoReset();
  
}

function loadUserManagementData()
{
	var content='';
	content = '<div style="padding:20px 0;"> <div class="btn_action float-right" style="margin-right:30px"><a class="rep_sprite loadUsersBtn">Load Users<span class="itemName"></span></a></div> <ul class="checkbox_filters float-right" style="margin-top:7px;"><li><a class="userRoleType active">Administrator</a></li><li><a class="userRoleType active">Editor</a></li><li><a class="userRoleType active">Author</a></li><li><a class="userRoleType active">Contributor</a></li><li><a class="userRoleType">Subscriber</a></li></ul> <div class="clear-both"></div><div class="actionContent siteSearch user_management_action" style="display:none;margin-top:10px "></div> </div>';
	
	 $(".usersDataContent").html(content);
	 
}
function loadProcessedUserData(data)
{
	$(".loadUsersBtn").removeClass('disabled');
	$(".btn_loadingDiv").remove();
	checkLoadUsersBtn=0;

	var content=' <div class="th rep_sprite"> <div class="title"><span class="droid700">Show by</span></div> <ul class="btn_radio_slelect float-left"> <li><a class="rep_sprite manage_user_userview itemName optionSelect">User</a></li> <li><a class="rep_sprite manage_user_websitesview optionSelect active">Websites</a></li> </ul> <div class="type_filter " style="border-right: 1px solid #D4D7D9; padding: 8px 7px 8px 7px;"> <input name="" id="manageFilter" type="text" class="input_type_filter searchItems manageFilter " value="type to filter" /> <div class="clear_input rep_sprite_backup" onclick=""></div></div><ul class="th_sub_nav float-left user_management" style="border-left: 1px solid #F6F6F6;padding: 3px 0 4px 2px;"><li><a class="rep_sprite active userRoles" changeclass="changeUserRole" func="change-role">CHANGE ROLE</a></li><li><a class="rep_sprite userRoles" changeclass="changeUserPassword" func="change-password">CHANGE PASSWORD</a></li><li><a class="rep_sprite userRoles" changeclass="changeUserDelete" func="delete-user">DELETE</a></li></ul><div class="dropdown_cont users_select_role float-left changeUserRole changeUserClasses"><div class="dropdown_btn" id="selectRoleBtn" style=""><span id="roleDropName" class="dropdown_btn_val">Select Role</span><span class="arrow_down"></span></div><ul id="selectRoleOptions" class="dropdownToggle" style="display:none;"><li><a class="loadRole dropOption" dropopt="1">Administrator</a></li><li><a class="loadRole dropOption" dropopt="2">Editor</a></li><li><a class="loadRole dropOption" dropopt="3">Author</a></li><li><a class="loadRole dropOption" dropopt="4">Contributor</a></li><li><a class="loadRole dropOption" dropopt="5">Subscriber</a></li></ul></div><input name="" type="text" class="changeUserPassword changeUserClasses txtHelp" id="userChangePassword" helptxt="new password" value="new password" style="display:none"/><div class="delete_user_post_ressign float-left changeUserDelete changeUserClasses" style="display:none"><input name="" type="text"  id="userChangeDelete" class="txtHelp" helptxt="reassign posts to" value="reassign posts to"/><div class="delete_user_post_ressign_tip">Enter username to reassign posts to, or leave blank to delete user posts.</div></div>	 <div class="btn_action float-right"><a class="rep_sprite userManageConfirmAction disabled">Apply Changes</a></div> </div> <div id="view_content" ></div>';
	$(".actionContent").html(content).show();
	 manageUsers=data;
	 loadUserData("sites");
}


 function loadUserData(view)
 {
	 $(".status_applyChangesCheck").addClass('disabled');
	 var content='',statusVar,firstKey,actionVar,actionLi,siteID,dID,contName,delCont,extraDeactivateClass,styleSheetTmp,dLink,noContentText;
	 if(view=='sites')
	var json=manageUsers.data.manageUsersGet.siteView;
	 else
	 var json=manageUsers.data.manageUsersGet.userView;
	if(json!=null &&  json!=undefined && getPropertyCount(json)>0)
 {
	 $.each(json, function(i, object) {
		 
		 if(view!='sites')
		 {
		 firstKey=i;
		
		 }
		 else
		 {
	
		firstKey=site[i].name;
		 }
		
		
		 content=content+'<div class="ind_row_cont "> <div class="row_summary" style="display:none" > <div class="row_arrow"></div><div class="row_checkbox userMainCheck parent_'+i+'" checkvar="'+i+'"></div>	  <div class="row_name searchable">'+firstKey+'</div> <div class="clear-both"></div> </div>';
	     content=content+'<div class="row_detailed" > <div class="rh"> <div class="row_arrow"></div><div class="row_checkbox userMainCheck parent_'+i+'" checkvar="'+i+'"></div>	<div class="row_name">'+firstKey+'</div> <div class="clear-both"></div> </div><div class="rd">';
		  $.each(object, function(status, value) {
			  
			
		 content=content+'<div class="row_updatee"> <div class="row_updatee_ind"> <div class="label_updatee  float-left"> <div class="label droid700 float-left"><span class="'+status+'">'+status.toTitleCase()+'</span></div> <div class="count float-left"><span>'+getPropertyCount(value)+'</span></div> <div class="clear-both"></div> </div><div class="items_cont float-left">';
		  $.each(value, function(id, array) {
			
			   if(view=='sites')
			  {
				  siteID=i;
				  username=id;
				  contName=id;
			  }
			  else
			  {
				  siteID=id;
				  username=i;
				  
				contName=site[id].name;
			  }
			 
			
			  
			  content=content+' <div class="item_ind float-left userSubCheck child_'+i+'" checkvar="'+i+'" sid="'+siteID+'" username="'+username+'"><div class="row_checkbox"></div>	 <div class="item float-left">'+contName+'</div><div class="result_area"></div></div>  ';
		  });
		  content=content+' </div><div class="clear-both"></div></div> </div>';
		 
		  });
		
		 content=content+'</div></div></div>';
		  });
		  }
		if(view=="sites")
		noContentText='websites';
		else
		noContentText = "users";
		if(content=='')
		content='<div style="padding:20px; text-align:center">No users found.</div>';
		else
		content='<div class="no_match hiddenCont" style="display:none">Bummer, there are no '+noContentText+' that match.<br />Try typing lesser characters.</div>'+content;
		 $("#view_content").html(content);
	 
 }
 
 function loadNewUser()
 {
	 var extra,siteName,bkBtn,bkNowBtn;
	
	 extra='<div class="th_sub rep_sprite"><ul class="two_steps"><li><a class="current rep_sprite_backup next" id="selectWebsitesUserTab">SELECT WEBSITES</a></li><li class="line"></li> <li><a id="enterDetailsUserTab" class="clickNone rep_sprite_backup next">ENTER DETAILS</a></li></ul> </div><div class="siteSelectorContainer backupTab">'+siteSelectorRestrictVar+'</div> <div id="addUserOptions" class="backupTab add_user_form" style="display:none">';
	
	bkBtn='<div class="btn_next_step float-right rep_sprite disabled backupTab next" id="enterUserDetails">Enter Details<div class="taper"></div></div><div class="btn_action float-right"><a class="rep_sprite" id="addUser" class="backupTab" style="display:none">Create User</a></div></div>';
	 enterDetailsCont='<div class="float-left left" style="padding:20px 20px 0; width:45%;"> <div class="float-left" style="width: 47%; margin-right: 20px;"> <div class="label">USERNAME</div> <input name="" type="text" id="userLogin"> <div class="label">EMAIL</div> <input name="" type="text" id="userEmail"> <div class="clear-both"></div> </div> <div class="float-left" style="width: 47%;"> <div class="label">ROLE</div> <div class="dropdown_cont users_set_role float-left changeUserRole changeUserClasses" style="margin-top:-2px"> <div class="dropdown_btn" id="selectRoleBtn" style=""><span id="roleDropName" class="dropdown_btn_val">Select Role</span><span class="arrow_down"></span></div> <ul id="selectRoleOptions" class="dropdownToggle" style="display: none; "> <li><a class="loadRole dropOption" dropopt="1">Administrator</a></li> <li><a class="loadRole dropOption" dropopt="2">Editor</a></li> <li><a class="loadRole dropOption" dropopt="3">Author</a></li> <li><a class="loadRole dropOption" dropopt="4">Contributor</a></li> <li><a class="loadRole dropOption" dropopt="5">Subscriber</a></li> </ul> </div><div class="clear-both"></div><div class="label" style="margin-top:14px">PASSWORD</div><input name="" type="text" id="userPass"> <div class="clear-both"></div> </div><div class="clear-both"></div><div class="checkbox" id="sendPassEmail">Send this password to the new user by email.</div> </div> <div class="float-left right" style="padding:20px; width:45%;"> <div class="float-left" style="width:100%"> <div class="float-left" style="width:45%; margin-right:20px"> <div class="label">FIRST NAME</div> <input name="" type="text" id="firstName"> </div> <div class="float-left" style="width:49%;"> <div class="label">LAST NAME</div> <input name="" type="text" id="lastName"> </div> <div class="clear-both"></div> <div class="label">WEBSITE</div> <input name="" type="text" id="userURL" style="width:371px;"> <div class="clear-both"></div> </div> </div> <div class="clear-both"></div> </div> <div class="th rep_sprite" style="border-top:1px solid #c6c9ca; height: 35px;">';

	content='<div class="dialog_cont create_backup create_backup_sitewise"> <div class="th rep_sprite"> <div class="title droid700">CREATE A NEW USER</div> <a class="cancel rep_sprite_backup">cancel</a></div>  '+extra+enterDetailsCont+bkBtn+'</div>';
	$("#modalDiv").dialog("close");
	$("#modalDiv").dialog("destroy");
	$('#modalDiv').html(content).dialog({width:'auto',modal:true,position: 'center',resizable: false, open: function(event, ui) {bottomToolBarHide();  addNewUserOn = 1; },close: function(event, ui) {bottomToolBarShow(); addNewUserOn=0; $("#modalDiv").html(''); }});
	$(".siteSelectorContainer .nano").nanoScroller({stop: true});
	$(".siteSelectorContainer .group_items_cont").css('height',$(".siteSelectorContainer .group_items_cont").height()).addClass('nano');
	$(".siteSelectorContainer .website_items_cont").css('height',$(".siteSelectorContainer .website_items_cont").height()).addClass('nano');
	$(".siteSelectorContainer .nano").nanoScroller();
 }
 