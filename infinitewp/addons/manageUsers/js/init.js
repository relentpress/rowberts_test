$(function () {
$(".userRoles").live('click',function() {
	$(".userRoles").removeClass('active');
	$(this).addClass('active');
	$(".changeUserClasses").hide();
	$("."+$(this).attr('changeclass')).show();
	checkUserManageConfirm();
});
$(".userRoleType").live('click',function() {
	makeSelection(this);
	if($(".userRoleType.active").length<1 && checkLoadUsersBtn==0)
	$(".loadUsersBtn").addClass('disabled');
	else if($(".userRoleType.active").length>0 && checkLoadUsersBtn==0)
	$(".loadUsersBtn").removeClass('disabled');
});
$(".loadUsersBtn").live('click',function() {

if(!$(this).hasClass('disabled'))
{
	checkLoadUsersBtn=1;
	var tempArray={};
	$(this).addClass('disabled');
	$(this).append('<div class="btn_loadingDiv"></div>');
	tempArray['action']="manageUsersGet";
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['args']['params']['userRole']={};
	tempArray['requiredData']={};
	tempArray['requiredData']['manageUsersGet']=1;
	var tempCheck=0;
	tempArray['args']['params']['userRole']={};
	$(".userRoleType.active").each(function () { 
	tempArray['args']['params']['userRole'][tempCheck] = $(this).text().toLowerCase();
	tempCheck++;
	});
	var siteArray=getSelectedSites();
	tempArray['args']['siteIDs']=siteArray;
	
	doCall(ajaxCallPath,tempArray,'formArrayLoadUsers','json',"none");
}
});

$(".manage_user_userview").live('click',function() {
	loadUserData("user");
});

$(".manage_user_websitesview").live('click',function() {
	loadUserData("sites");
});

$(".userMainCheck").live('click',function() {
	if($(this).hasClass('active'))
	{
		$(".child_"+$(this).attr('checkvar')).removeClass('active');
		$(this).removeClass('active').closest(".ind_row_cont").removeClass('active');
		
	}
	else
	{
		$(".child_"+$(this).attr('checkvar')).addClass('active'); 
		$(this).addClass('active').closest(".ind_row_cont").addClass('active');
		
	}
	checkUserManageConfirm();
	return false;
	
});

$(".userSubCheck").live('click',function() {
	if($(this).hasClass('active'))
	$(this).removeClass('active').children(".row_checkbox").removeClass('active');
	else
	$(this).addClass('active').children(".row_checkbox").addClass('active');
	if($(".child_"+$(this).attr("checkvar")+".active").length==$(".child_"+$(this).attr("checkvar")).length)
	$(".parent_"+$(this).attr("checkvar")).addClass('active').closest(".ind_row_cont").addClass('active');
	else
	$(".parent_"+$(this).attr("checkvar")).removeClass('active').closest(".ind_row_cont").removeClass('active');
	checkUserManageConfirm();
	return false;
	
});
$(".loadRole").live('click',function() {
checkUserManageConfirm();
});
$(".userManageConfirmAction").live('click',function() {
	if(!$(this).hasClass('disabled'))
	{
	var func = $(".userRoles.active").attr("func");
	var changeArray={},arrayCounter=0;
	$(this).addClass('disabled');
	$(".userSubCheck.active").each(function () {
		siteID=$(this).attr('sid');
		username=$(this).attr('username');
		if(changeArray[siteID]==undefined)
		changeArray[siteID]={};
		changeArray[siteID][arrayCounter]=username;
		arrayCounter++;
		$(this).children('.result_area').html('<div class="queued_single">Queued..</div>');
		$(this).addClass('complete').removeClass('active').children('.row_checkbox').remove();
	});
	completeUserActiveCheck();
	var tempArray={};
	tempArray['action']='manageUsersUpdate';
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['args']['params']['userEditAction']=func;
	if(func=='change-role')
	tempArray['args']['params']['newRole'] = $("#roleDropName").text().toLowerCase();
	else if(func=='change-password' && $("#userChangePassword").val()!="new password")
	tempArray['args']['params']['newPassword'] = $("#userChangePassword").val();
	else if(func=='delete-user' && $("#userChangeDelete").val()!="reassign posts to")
	tempArray['args']['params']['reassignUser'] = $("#userChangeDelete").val();
	tempArray['args']['params']['users'] = changeArray;
	doHistoryCall(ajaxCallPath,tempArray,'');
		$(".loadUsersBtn").removeClass('disabled');
	}
	return false;
});
$("#addNewUser").live('click',function() {
loadNewUser();
});

$("#enterUserDetails").live('click',function() {
	$("#enterDetailsUserTab").removeClass('clickNone').click();
});
$("#enterDetailsUserTab").live('click',function() {
	
	if(!$(this).hasClass('clickNone'))
	{
		$(".dialog_cont .th_sub.rep_sprite .current").removeClass('current');
			$(".dialog_cont .backupTab").hide();
		
		
		$(this).addClass('current').removeClass('completed');
	$("#selectWebsitesUserTab").removeClass('current').addClass('completed');
	$("#addUserOptions,#addUser").show();
	$(".dialog_cont .siteSelectorContainer,#enterUserDetails").hide();
	}
});
$("#selectWebsitesUserTab").live('click',function() {
	$(".dialog_cont .th_sub.rep_sprite .current").removeClass('current');
	$(".dialog_cont .backupTab").hide();
	showAddUserOptions();
	$(this).addClass('current').removeClass('completed');
	$("#enterDetailsUserTab").removeClass('current completed').addClass('clickNone');
	$(".dialog_cont .siteSelectorContainer,#enterUserDetails").show();
	$("#addUserOptions,#addUser").hide();
		
});
$("#sendPassEmail").live('click',function() {
	makeSelection(this);
});
$("#selectRoleBtn").live('click',function() {
	$(this).removeClass('error');
});

$("#addUser").live('click',function() {
	var checkVar=true;
	$(".emailError").remove();
	$(".dialog_cont .error").removeClass('error');
	if($("#userLogin").val()=='')
	{
	$("#userLogin").addClass('error');
	checkVar=false;
	}
	if($("#userPass").val()=='')
	{
	$("#userPass").addClass('error');
	checkVar=false;
	}
	if($("#userEmail").val()=='')
	{
	$("#userEmail").addClass('error');
	checkVar=false;
	}
	if(!echeck($("#userEmail").val()))
	{
	$("#userEmail").prev(".label").append("<div class='emailError error'>Enter a valid email.</div>");
	checkVar = false;
	}
	if($(".dialog_cont #roleDropName").text()=="Select Role")
	{
		$(".dialog_cont #selectRoleBtn").addClass('error');
	checkVar = false;
	}
	if(checkVar == false)
	return false;
	var tempArray={};
	tempArray['action']="manageUsersAdd";
	tempArray['args']={};
	tempArray['args']['params']={};
	tempArray['args']['params']['userLogin']=$("#userLogin").val();
	tempArray['args']['params']['userPass']=$("#userPass").val();
	if($("#firstName").val()!='')
	tempArray['args']['params']['firstName']=$("#firstName").val();
	if($("#lastName").val()!='')
	tempArray['args']['params']['lastName']=$("#lastName").val();
	if($("#userURL").val()!='')
	tempArray['args']['params']['userURL']=$("#userURL").val();
	tempArray['args']['params']['userEmail']=$("#userEmail").val();
	tempArray['args']['params']['role']=$(".dialog_cont #roleDropName").text().toLowerCase();
	if($("#sendPassEmail").hasClass("active"))
	tempArray['args']['params']['emailNotify']=1;
	else
	tempArray['args']['params']['emailNotify']=0;
	tempArray["args"]["siteIDs"]=getSelectedSites(".dialog_cont");
	doHistoryCall(ajaxCallPath,tempArray,"");
	$("#modalDiv").dialog("close");
	$("#modalDiv").html('');
	return false;
	
});
$("#userEmail").live('click',function() {
	$(".emailError").remove();
});
});