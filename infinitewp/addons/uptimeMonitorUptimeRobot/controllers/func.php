<?php

function uptimeMonitorUptimeRobotAddonMenus(&$menus){
	//$menus[] = '<li><a class="navLinks optionSelect" page="upTimeMonitor">Uptime Monitor</a></li>';
	$menus['tools']['subMenus'][] = array('page' => 'upTimeMonitor', 'displayName' => 'Uptime Monitor');
}

function uptimeMonitorUptimeRobotSaveApiKey($params){
	if(!empty($params)){
		$saveApiKey = updateOption("uptimeRobotApiKey", serialize(trim($params['uptimeRobotApiKey'])));
		if($saveApiKey){
			return trim($params['uptimeRobotApiKey']);
		}
		return false;
	}
}

function uptimeMonitorUptimeRobotGetApiKey(){
	$getApiKey = getOption("uptimeRobotApiKey");
	if(!empty($getApiKey)){
		$apiKey = unserialize($getApiKey);
		if($apiKey){
			return $apiKey;
		}
	}
	return false;
}


//new monitor.
function uptimeMonitorUptimeRobotAddMonitor($params){
	$apiKey = uptimeMonitorUptimeRobotGetApiKey();
	
	if(empty($apiKey)){ return false; }
	
		if(!empty($params)){
			$monitorName = urlencode($params['monitorName']);
			if(!empty($params['emailAlerts'])){
				$alertContactID = setAlertsContacts($params['emailAlerts']);
			}
			$decodedData = array();
			foreach($params['siteIDs'] as $siteIDs => $siteID){
				$siteData = DB::getRow("?:sites", "URL, httpAuth", "siteID = '".$siteID."'");
				$monitorHttpAuth = '';
				if(!empty($siteData['httpAuth'])){
					$siteHttpAuth = unserialize($siteData['httpAuth']);
					$monitorHttpAuth = "&monitorHTTPUsername=".$siteHttpAuth['username']."&monitorHTTPPassword=".$siteHttpAuth['password']."";
				}
				switch($params['type']){
					case 'http':
							$upTimeMonitorAddURL = "http://api.uptimerobot.com/newMonitor?apiKey=".$apiKey."&monitorFriendlyName=".$monitorName."&monitorURL=".$siteData['URL']."&monitorType=1&format=json".$alertContactID.$monitorHttpAuth;
						break;
						
					case 'keywordChecking':
							$upTimeMonitorAddURL = "http://api.uptimerobot.com/newMonitor?apiKey=".$apiKey."&monitorFriendlyName=".$monitorName."&monitorURL=".$siteData['URL']."&monitorType=2&monitorKeywordType=".$params['keywordChecking']['alertWhen']."&monitorKeywordValue=".$params['keywordChecking']['keyword']."&format=json".$alertContactID.$monitorHttpAuth;
						break;
						
					case 'ping':
							$upTimeMonitorAddURL = "http://api.uptimerobot.com/newMonitor?apiKey=".$apiKey."&monitorFriendlyName=".$monitorName."&monitorURL=".$siteData['URL']."&monitorType=3&format=json".$alertContactID.$monitorHttpAuth;
						break;
						
					case 'TCP':
							$upTimeMonitorAddURL = "http://api.uptimerobot.com/newMonitor?apiKey=".$apiKey."&monitorFriendlyName=".$monitorName."&monitorURL=".$siteData['URL']."&monitorType=4&monitorSubType=".$params['TCP']['port']."&monitorPort=".$params['TCP']['customPortValue']."&format=json".$alertContactID.$monitorHttpAuth;
						break;
				}
				
				$decodedData[$siteID] = apiCallAndResponse($upTimeMonitorAddURL);				
			}
		}
		return $decodedData;
}

  /*1 - down
  2 - up
  99 - paused
  98 - started
  */
  function uptimeMonitorUptimeRobotGetMonitor(){
	
	$apiKey = uptimeMonitorUptimeRobotGetApiKey();
	
	if(empty($apiKey)){ return false; }
	
		$upTimeMonitorGetURL = "http://api.uptimerobot.com/getMonitors?apiKey=".$apiKey."&logs=1&alertContacts=1&showMonitorAlertContacts=1&format=json&showTimeZone=1&customUptimeRatio=1";
		$decodedData = apiCallAndResponse($upTimeMonitorGetURL);
					
		$logType[1] = 'down';
		$logType[2] = 'up';
		$logType[99] = 'pause';
		$logType[98] = 'started';
		
		$logsData = array();
		if($decodedData->stat == 'fail'){
			$resultData = array('stat' => $decodedData->stat, 'message' => $decodedData->message);
		}else{
			$monitors = $decodedData->monitors->monitor;
			
			if(!empty($monitors) && is_array($monitors))
			foreach($monitors as $monitor){
				$logsData = array();
				if(!empty($monitor->log)){
					$i = 1;
					foreach($monitor->log as $key => $logs){
						
						$monitorTimeStamp = strtotime($logs->datetime);
						$monitorTime = date('H.is', $monitorTimeStamp);
						
						@date_default_timezone_set('GMT'); //gmdate(time());
						$gmt = time();
						
						$currentTimeStamp = strtotime($decodedData->timezone."minutes", $gmt);
						$currentTime = date('m/d/Y H:i:s', $currentTimeStamp);
					
						$todayStartHourTimeStamp = strtotime("-24 hours", $currentTimeStamp);
						$todayStartHour =  date('H', $todayStartHourTimeStamp);
						
						$diff = ($currentTimeStamp - $todayStartHourTimeStamp)/60/60;
						
						if($monitorTimeStamp >= $todayStartHourTimeStamp){
							$logsData[] = array('time' => $logs->datetime, 'type' => $logType[$logs->type], 'hour' => $monitorTime, 'currentTime' => $currentTime, 'less24' => 'yes', 'todayStartHour' => $todayStartHour, 'diff' => $diff);
						}
						else{
							if(empty($logsData) || $i == 1){
								$logsData[] = array('time' => $logs->datetime, 'type' => (($logs->type == 1) ? 'down' : 'up'), 'hour' => $monitorTime, 'currentTime' => $currentTime, 'less24' => 'no', 'todayStartHour' => $todayStartHour, 'diff' => $diff);
								$i = 0;
								continue;
							}
						}
					}
				}
				
				$resultData[$monitor->friendlyname][] = array('stat' => $decodedData->stat, 'playPauseStatus' => $monitor->status, 'monitorID' => $monitor->id, 'monitorURL' => $monitor->url, 'currentTime' => date('H.i', strtotime($decodedData->timezone."minutes")), 'monitorName' => $monitor->friendlyname, 'alltimeuptimeratio' => $monitor->alltimeuptimeratio, 'customUpTimeRatio' => $monitor->customuptimeratio, 'logs' => $logsData);
			}
		}
	
		return $resultData;
}

function uptimeMonitorUptimeRobotDeleteMonitor($params){
	$apiKey = uptimeMonitorUptimeRobotGetApiKey();
	
	if(empty($apiKey)){ return false; }
	
	if(is_array($params['monitorIDs'])){
		$upTimeMonitorDeleteResult = array();
		foreach($params['monitorIDs'] as $key => $monitorID){
			$upTimeMonitorDeleteURL = "http://api.uptimerobot.com/deleteMonitor?apiKey=".$apiKey."&monitorID=".$monitorID."&format=json";
			$upTimeMonitorDeleteResult[] = apiCallAndResponse($upTimeMonitorDeleteURL);
		}
		
	}
		
	return $upTimeMonitorDeleteResult;
}

function uptimeMonitorUptimeRobotChangeStatus($params){
	
	$apiKey = uptimeMonitorUptimeRobotGetApiKey();
	if(empty($apiKey)){ return false; }
	
	$upTimeMonitorChangeStatusURL = "http://api.uptimerobot.com/editMonitor?apiKey=".$apiKey."&monitorID=".$params['monitorID']."&monitorStatus=".$params['status']."&format=json";
	$resultDecodedData = apiCallAndResponse($upTimeMonitorChangeStatusURL);
	
	return $resultDecodedData;
}

function setAlertsContacts($email){
	$apiKey = uptimeMonitorUptimeRobotGetApiKey();
	if(empty($apiKey)){ return false; }
	
	$getAlertContactsURL = "http://api.uptimerobot.com/getAlertContacts?apiKey=".$apiKey."&format=json";
	$getAlertContactsDecodedData = apiCallAndResponse($getAlertContactsURL);
	$alertContactID = '';		
	if($getAlertContactsDecodedData->stat == 'ok'){
		$alertContacts = $getAlertContactsDecodedData->alertcontacts->alertcontact;
		foreach($alertContacts as $contacts){
			if($email == $contacts->value){
				$alertContactID = "&monitorAlertContacts=".$contacts->id;
			}
			else{
				
				$newAlertContactURL = "http://api.uptimerobot.com/newAlertContact?apiKey=".$apiKey."&alertContactType=2&alertContactValue=".$email."&format=json";
				$newAlertContactDecodedData = apiCallAndResponse($newAlertContactURL);
				if($newAlertContactDecodedData->stat == 'ok'){
					$alertContactID = "&monitorAlertContacts=".$newAlertContactDecodedData->alertContact->id;
				}
			}
		}
	}
	
	return $alertContactID;
}

function apiCallAndResponse($URL){
	$json_encap = "jsonUptimeRobotApi()";
	$response = doCall($URL, '', '');
	return json_decode(substr ($response[0], strlen($json_encap) - 1, strlen ($response[0]) - strlen($json_encap)));
}

/*
function jsonDecode($response){
	$json_encap = "jsonUptimeRobotApi()";
	return json_decode(substr ($response, strlen($json_encap) - 1, strlen ($response) - strlen($json_encap)));
}
*/

?>