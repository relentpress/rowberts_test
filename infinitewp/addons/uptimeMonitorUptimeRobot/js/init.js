$(function () {
	// $("#settings_cont .btn_radio_slelect.mainSettingsTab").prepend('<li><a class="rep_sprite optionSelect settingsButtons" item="uptimeTab">Uptime Monitor</a></li>');
	uptimeMonitoringSettings = '<li>Uptime Monitor</li>';
        uptime_monitor_content = '<div class="settings settingsItem branding" id="uptimeTab" style="border: 0px; padding: 0px; "> <div class="tr"> <div class="padding" style="font-size:12px;"><div class="label" style="padding: 10px 0;">CONNECT YOUR UPTIME ROBOT ACCOUNT<a href="http://infinitewp.com/knowledge-base/how-to-connect-your-uptimerobot-account/?utm_source=application&utm_medium=userapp&utm_campaign=kb" style="padding:0; float:right; display:inline; text-transform:none;" target="_blank">See instructions</a></div> <div class="tl no_text_transform" style="width:475px;"> <div><div class="form_label">UPTIME ROBOT API KEY</div><input type="text" class="uptimeAPIKey formVal"  id="uptimeRobotApiKey" placeholder="Enter API Key Here" style="width: 238px;"></div> <div class="clear-both"></div></div> <div class="clear-both"></div> </div> </div>  </div> <div class="th_sub rep_sprite" ><div class="success rep_sprite_backup float-left" id="saveSuccess" style="display:none">Saved successfully!</div><div class="btn_action float-right"><a class="rep_sprite" id="saveSettingsBtn" page="uptimeTab">Save Changes</a></div></div>';
	// $("#settings_cont #appSettingsTab").after('<div class="settings settingsItem branding" id="uptimeTab" style="border: 0px; padding: 0px; display: none;"> <div class="tr"> <div class="padding" style="font-size:12px;"><div class="label" style="padding: 10px 0;">CONNECT YOUR UPTIME ROBOT ACCOUNT<a href="http://infinitewp.com/knowledge-base/how-to-connect-your-uptimerobot-account/?utm_source=application&utm_medium=userapp&utm_campaign=kb" style="padding:0; float:right; display:inline; text-transform:none;" target="_blank">See instructions</a></div> <div class="tl no_text_transform" style="width:475px;"> <div><div class="form_label">UPTIME ROBOT API KEY</div><input type="text" class="uptimeAPIKey formVal"  id="uptimeRobotApiKey" style="width: 238px;"></div> <div class="clear-both"></div></div> <div class="clear-both"></div> </div> </div>  </div> ');
	
	//loadGrantRevokeButton();
	tempArray={};
	tempArray['requiredData']={};
	//tempArray['requiredData']['googleAnalyticsGrantAccess']=1;
	tempArray['requiredData']['uptimeMonitorUptimeRobotGetApiKey']=1;
	//tempArray['requiredData']['googleAnalyticsGetProfileData']['view']='week';
	doCall(ajaxCallPath,tempArray,'processAPIKeySet');
	//loadGoogleContent();
	});


$('.scheduleUptime').live('click',function(){
	loadUptimePage();
});
/*$('.uptimeSites .website_cont').live('click',function(){
	
	if($('.website_cont').hasClass('active'))
	{
		if($('#enterUptimeDetails').hasClass('disabled'))
		{
			$('#enterUptimeDetails').removeClass('disabled');
		}
		
	}
	
});*/
$('#enterUptimeDetails').live('click',function(){
	$('.uptimeSites').hide();
	$('.uptimeOptions').show();
	$('#selectWebsitesTab_uptime').removeClass('current');
	$('#selectWebsitesTab_uptime').addClass('completed');
	$('#enterDetailsTab_uptime').addClass('current');
	$('#addMonitorFinal').show();
	$(this).hide();
});
$('.uptimeType').live('click',function(){
	$('.keywordChecking:visible').hide();
	$('.TCP_options:visible').hide();
	if($(this).attr('id') == 'keywordChecking')
	{
		$('.keywordChecking').show();
	}
	if($(this).attr('id') == 'TCP')
	{
		$('.TCP_options').show();
	}
});

$('#newPortMonitorPort').live('change',function(){
	var selected = $(this).val();
	if(selected == '99')
	{   
	  
		$('.customPortClass').show();
		$( ".customPortClass" ).show(  );
	}
	else
	{
		$('.customPortClass:visible').hide();
	}
});

$('#selectWebsitesTab_uptime.completed').live('click',function(){
	if($('#enterDetailsTab_uptime').hasClass('completed'))
	{
		$('#enterDetailsTab_uptime').removeClass('completed');
	}
	if($('#enterDetailsTab_uptime').hasClass('current'))
	{
		$('#enterDetailsTab_uptime').removeClass('current');
	}
	if($('#uptimeResultsTab').hasClass('current'))
	{
		$('#uptimeResultsTab').removeClass('current');
	}
	$(this).removeClass('completed');
	$(this).addClass('current');
	$('.uptimeSites').show();
	$('.uptimeOptions').hide();
	$('.uptimeResultOptions').hide();
	$('#addMonitorFinal').hide();
	$('#enterUptimeDetails').show();
	$('#addMonitorClose').hide();
	
});

$('#enterDetailsTab_uptime.completed').live('click',function(){
	if($(this).hasClass('completed'))
	$(this).removeClass('completed');
	if($('#uptimeResultsTab').hasClass('current'))
	{
		$('#uptimeResultsTab').removeClass('current');
	}
	$(this).addClass('current');
	$('.uptimeSites').hide();
	$('.uptimeOptions').show();
	$('.uptimeResultOptions').hide();
	$('#addMonitorFinal').show();
	$('#enterUptimeDetails').hide();
	$('#addMonitorClose').hide();
});


$('#uptimeResultsTab.current').live('click',function(){
	$('.uptimeSites').hide();
	$('.uptimeOptions').hide();
	$('.uptimeResultOptions').show();
	$('#addMonitorFinal').hide();
	$('#enterUptimeDetails').hide();
});

$('.alertWhenClass').live('click',function(){
	$('.alertWhenClass').removeClass('active');
	$(this).addClass('active');

});
$('#addMonitorFinal').live('click',function(){
	var tempArray={},siteArray;;
	var requireDataArray={};
	siteArray=getSelectedSites();
	$(this).addClass('disabled');
	/*tempArray['args']={};
	tempArray['action']="get"+activeItem.toTitleCase();
	tempArray['args']['siteIDs']=siteArray;*/
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']={};
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['action'] = monitorAction;
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['monitorID'] = monitorEditID;
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['type'] = $('.uptimeType.active').attr('id');
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['monitorName'] = $('#monitorName').val();
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['emailAlerts'] = $('#uptimeEmailAlerts').val();
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['URL'] = $('#uptimeURL').val();
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['HTTP'] = {};
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['ping'] = {};
	//requireDataArray['upTimeMonitorSet']['HTTP']['URL'] = 
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['keywordChecking'] = {};
	//requireDataArray['upTimeMonitorSet']['keywordChecking']['URL'] = 
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['keywordChecking']['alertWhen'] = $('.alertWhenClass.active').attr("var");
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['keywordChecking']['keyword'] = $('.keywordInput').val();
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['TCP'] = {};
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['TCP']['port'] = $('#newPortMonitorPort').val();
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['TCP']['customPortValue'] = $('#customPortValue').val();
	requireDataArray['uptimeMonitorUptimeRobotAddMonitor']['siteIDs'] = siteArray;
	tempArray['requiredData']= requireDataArray;
	doCall(ajaxCallPath,tempArray,'processMonitorAdd','json',"none");
});

$('.pauseMonitor').live('click',function(){
	var tempArray={},siteArray;;
	var requireDataArray={};
	requireDataArray['uptimeMonitorUptimeRobotChangeStatus']={};
	requireDataArray['uptimeMonitorUptimeRobotChangeStatus']['status']=0;
	requireDataArray['uptimeMonitorUptimeRobotChangeStatus']['monitorID']=$(this).attr("mid");
	//requireDataArray['uptimeMonitorUptimeRobotChangeStatus']['monitorName']=$(this).attr("mName");
	tempArray['requiredData']= requireDataArray;
	doCall(ajaxCallPath,tempArray,'processPauseMonitor','json',"none");
	return false;
});

$('.deleteMonitor').live('click',function(){
	var tempArray={},siteArray;;
	var requireDataArray={};
	requireDataArray['uptimeMonitorUptimeRobotDeleteMonitor']={};
	requireDataArray['uptimeMonitorUptimeRobotDeleteMonitor']['monitorIDs'] = {};
	//requireDataArray['uptimeMonitorUptimeRobotDeleteMonitor']['status']=0;
	requireDataArray['uptimeMonitorUptimeRobotDeleteMonitor']['monitorIDs']['0']=$(this).attr("mid");
	//requireDataArray['uptimeMonitorUptimeRobotChangeStatus']['monitorName']=$(this).attr("mName");
	tempArray['requiredData']= requireDataArray;
	if($(this).hasClass('yes'))
	{
		$("#loadingDiv").show();
		
		$(this).closest('.row_updatee_ind').hide("blind", {}, 500);
		doCall(ajaxCallPath,tempArray,'processDelMonitor','json',"none");
	}
	else
	{
		var row_update = $(this).closest('.row_updatee_ind').removeClass('del');
		$('.del_conf_uptime',row_update).hide();
		$('.delConfHide',row_update).show();
		
	}
	return false;
});

$('.editMonitor').live('click',function(){
	var tempArray={},siteArray;
	moniorAction = 'editMonitor';
	monitorEditID = $(this).attr("mid");
	var requireDataArray={};
	requireDataArray['uptimeMonitorUptimeRobotGetSingleMonitor']={};
	//requireDataArray['uptimeMonitorUptimeRobotDelete']['status']=0;
	requireDataArray['uptimeMonitorUptimeRobotGetSingleMonitor']['monitorID']=$(this).attr("mid");
	//requireDataArray['upTimeMonitorChangeStatus']['monitorName']=$(this).attr("mName");
	tempArray['requiredData']= requireDataArray;
	doCall(ajaxCallPath,tempArray,'processEditMonitor','json',"none");
	return false;

});

$('.monitorON_OFF').live('click',function(){
	var tempArray={},monitorIDs={},m=0,monStatus;
	if($(this).hasClass('off'))
	{
		$(this).removeClass('off');
		$(this).addClass('on');
		monStatus = 1;
	}
	else
	{
		$(this).removeClass('on');
		$(this).addClass('off');
		monStatus = 0;
	}
	
	/*var parentRowSummary = $(this).closest('.row_summary');
	var parentRowDetailed = $(this).closest('.row_summary').siblings('.row_detailed');
	if($(parentRowSummary).is(':visible'))
	{
		//var parentRD = $(this).closest('.rh').siblings(".rd");
		$('.deleteMonitor',parentRowDetailed).each(function(){
		monitorIDs[m] = $(this).attr("mid");
		m++;
	});
	}
	else
	{
		var parentRD = $(this).closest('.rh').siblings(".rd");
		$('.deleteMonitor',parentRD).each(function(){
			monitorIDs[m] = $(this).attr("mid");
			m++;
		});
	}*/
	monitorEditID = $(this).attr("mid");
	var requireDataArray={};
	requireDataArray['uptimeMonitorUptimeRobotChangeStatus']={};
	//requireDataArray['uptimeMonitorUptimeRobotDelete']['status']=0;
	requireDataArray['uptimeMonitorUptimeRobotChangeStatus']['status']=monStatus;
	requireDataArray['uptimeMonitorUptimeRobotChangeStatus']['monitorID'] = monitorEditID;
	//requireDataArray['uptimeMonitorUptimeRobotChangeStatus']['monitorName']=$(this).attr("mName");
	tempArray['requiredData']= requireDataArray;
	doCall(ajaxCallPath,tempArray,'processBulkPause','json',"none");
	return false;
});

$('.bulkDeleteMonitor').live('click',function(parentDom){
	parentDom.stopImmediatePropagation();
	var tempArray={},monitorIDs={},m=0,monStatus;
	var parentRowSummary = $(this).closest('.row_summary');
	var parentRowDetailed = $(this).closest('.row_summary').siblings('.row_detailed');
	if($(this).hasClass('opened'))
	{
		var parentRD = $(this).closest('.rh').siblings(".rd");
		$('.monitorON_OFF',parentRD).each(function(){
			monitorIDs[m] = $(this).attr("mid");
			m++;
		});
	}
	else
	{
		$('.monitorON_OFF',parentRowDetailed).each(function(){
		monitorIDs[m] = $(this).attr("mid");
		m++;
		});
	}
	var requireDataArray={};
	requireDataArray['uptimeMonitorUptimeRobotDeleteMonitor']={};
	//requireDataArray['uptimeMonitorUptimeRobotDeleteMonitor']['status']=0;
	//requireDataArray['uptimeMonitorUptimeRobotChangeStatus']['status']=monStatus;
	requireDataArray['uptimeMonitorUptimeRobotDeleteMonitor']['monitorIDs'] = monitorIDs;
	requireDataArray['uptimeMonitorUptimeRobotDeleteMonitor']['action'] = "bulkDelete";
	//requireDataArray['uptimeMonitorUptimeRobotChangeStatus']['monitorName']=$(this).attr("mName");
	tempArray['requiredData']= requireDataArray;
	if($(this).hasClass('no'))
	{
		$(parentRowSummary).removeClass('del');
		$('.del_conf_uptime',parentRowSummary).hide();	
		var row_detailed = $(this).closest('.row_detailed');
			if($(row_detailed).is(':visible'))
				{
					$(row_detailed).removeClass('del');
					$('.del_conf_uptime.opened',row_detailed).hide();
				}
	}
	else
	{
		$("#loadingDiv").show();
	
		var row_detailed = $(this).closest('.row_detailed');
		$(parentRowSummary).hide("blind", {}, 500);
		$(row_detailed).hide("blind", {}, 500);
		doCall(ajaxCallPath,tempArray,'processBulkDelete','json',"none");
	}
	return false;
});

$('.deleteMonitor_conf').live('click',function(){
	var row_updatee_ind = $(this).closest('.row_updatee_ind');
	$(row_updatee_ind).addClass('del');
	$('.delConfHide',row_updatee_ind).hide();
	$('.del_conf_uptime',row_updatee_ind).show();
	
	//$('.del_conf_uptime').show();
});

$('.bulkDeleteMonitor_conf').live('click',function(parentDom){
	var row_summary = $(this).closest('.row_summary');
	var row_detailed = $(this).closest('.row_detailed');
	if($(row_detailed).is(':visible'))
	{
		$(row_detailed).addClass('del');
		$('.del_conf_uptime.opened',row_detailed).show();
	}
	$(row_summary).addClass('del');
	$('.del_conf_uptime',row_summary).show();
	return false;
});
$(".uptimeSettingsClick").live('click',function() {
	openSettingsPage('Uptime Monitor');
    return false;
	
});
$('#addMonitorClose').live('click',function(){
	var m="";
	$("#modalDiv").dialog("close");
	$.each(resultsData_uptime,function(key,val){
		if((typeof val != 'undefined')&&(val != null))
		{
		if(val['stat'] == 'ok')
		m = "ok";
		}
	});
	if(m == "ok")
	loadupTimeMonitorMainPage();
});