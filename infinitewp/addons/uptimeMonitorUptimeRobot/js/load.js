var monitorAction="addMonitor",monitorEditID='',resultsData_uptime="";
function loadupTimeMonitorMainPage()
{
	/*var content='<div class="steps_hdr">Select websites to <span id="processType">Monitor</span> <span class="itemUpper">Uptime</span></div><div class="siteSelectorContainer">'+siteSelectorVar+'</div></div> <div class="optionsContent" style="display:block"></div>';*/
	var tempArray={};
	var requireDataArray={};
	$('.navLinks').addClass('disabled');
	$("#loadingDiv").show();
	$('#pageContent').html('');
	requireDataArray['uptimeMonitorUptimeRobotGetApiKey']=1;
	requireDataArray['uptimeMonitorUptimeRobotGetMonitor']=1;
	tempArray['requiredData']= requireDataArray;
	doCall(ajaxCallPath,tempArray,'processGetMonitor','json',"none");
	/*var content='<div class="result_block backup  shadow_stroke_box siteSearch uptime" id="backupPageMainCont"><div class="rows_cont"><div class="ind_row_cont"> <div class="row_summary"><div class="row_arrow"></div><div class="row_name">spark blog</div> <div class="row_backup_action rep_sprite" style="float:right;"><a class="trash bulkDeleteMonitor rep_sprite_backup"></a></div> <div class="clear-both"></div> </div> <div class="row_detailed"> <div class="rh"> <div class="row_arrow"></div> <div class="row_name">spark blog</div> <div class="row_backup_action rep_sprite" style="float:right;"><a class="trash bulkDeleteMonitor rep_sprite_backup"></a></div> <div class="clear-both"></div>  </div> <div class="rd"><div class="row_updatee"><div class="row_updatee_ind"><div class="timeline_hour_labels_cont"> <div style="width:50px; margin-right: 5px;">Last 24h</div><div>00</div> <div>01</div> <div>02</div> <div>03</div> <div>04</div>  <div>05</div> <div>06</div> <div>07</div> <div>08</div> <div>09</div> <div>10</div> <div>11</div> <div>12</div><div>13</div> <div>14</div> <div>15</div>  <div>16</div> <div>17</div> <div>18</div> <div>19</div> <div>20</div> <div>21</div> <div>22</div> <div>23</div><div class="clear-both"></div> </div> </div> <div class="row_updatee_ind"> <div class="label_updatee float-left"><div class="label droid700 float-right">backup01</div></div><div class="uptime_percentage">100%</div> <div class="items_cont float-left"> <div class="timeline_cont"><div class="timeline_up"></div> <div class="timeline_down"></div> <div class="timeline_up"></div>  <div class="clear-both"></div> </div> </div>  <div class="clear-both"></div> </div>   <div class="row_updatee_ind"><div class="label_updatee float-left"><div class="label droid700 float-right">backup03</div> </div><div class="uptime_percentage down">60%</div> <div class="items_cont float-left"> <div class="timeline_cont"><div class="timeline_up"></div><div class="timeline_down"></div><div class="timeline_up"></div> <div class="clear-both"></div> </div> </div><div class="clear-both"></div> </div> </div></div> </div> </div></div><div class="th rep_sprite"><div class="type_filter "><input name="" type="text" class="input_type_filter searchItems" value="type to filter" style="display: none;"><div class="clear_input rep_sprite_backup" onclick=""></div></div><div class="btn_action float-right"><a class="rep_sprite scheduleUptime">Create New Uptime Schedule</a></div></div><div class="no_match hiddenCont" style="display:none">Bummer, there are no backups that match.<br>Try typing lesser characters.</div><div class="empty_data_set"><div class="line2">Looks like there are <span class="droid700">no uptime Monitors scheduled now</span>. Create a <a class="scheduleUptime">Schedule Uptime</a>.</div></div></div>';*/
	//$("#pageContent").html(content);
	/*$(".timeline_up").qtip({id:"toggleGroupQtip",content: { text: 'Status Up &ltbr/&gt hola' }, position: { my: 'bottom center', at: 'top center',  adjust:{ y: -6} }, show: { event: 'mouseenter', effect: { type: 'slide' } }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 10, height:5} } });*/
	/*$(".optionsContent").html('<div class="btn_action float-right  recent_type"><a class="rep_sprite loadUptime ">Load <span class="itemName">Uptime</span></a></div><div class="clear-both"></div><div class="UptimeContent"></div>');*/
}
function loadUptimePage()
{
	var content='<div class="uptimeDialog dialog_cont create_backup"><div class="th rep_sprite"> <div class="title droid700">CREATE A NEW UPTIME MONITOR</div> <a class="cancel rep_sprite_backup">cancel</a></div><div class="th_sub rep_sprite"><ul class=""><li><a class="current rep_sprite_backup next" id="selectWebsitesTab_uptime">SELECT WEBSITES</a></li><li class="line"></li> <li><a id="enterDetailsTab_uptime" class="clickNone rep_sprite_backup next">ENTER DETAILS</a></li><li class="line"></li><li><a id="uptimeResultsTab" class="clickNone rep_sprite_backup next">RESULTS</a></li></ul> </div><div class="siteSelectorContainer uptimeSites">'+siteSelectorVar+'</div><div class="uptimeOptions"> <div class="left float-left"> <div class="label">MONITOR NAME</div> <input name="" type="text" id="monitorName">  <div class="label">EMAIL ALERTS TO</div> <input name="" type="text" id="uptimeEmailAlerts">     </div> <div class="right float-left"> <div class="backup_what" style="padding-bottom:0;"> <div class="label">WHAT?</div> <ul class="btn_radio_slelect" style="margin-bottom:20px"> <li><a class="rep_sprite active optionSelect uptimeType" id="http">HTTP(s)</a></li><li><a class="rep_sprite optionSelect uptimeType" id="keywordChecking">Keyword Checking</a></li><li><a class="rep_sprite optionSelect uptimeType" id="ping">Ping</a></li><li><a class="rep_sprite optionSelect uptimeType" id="TCP">TCP Ports</a></li> </ul><div class="clear-both"></div><div class="" style="display:none"><div class="label">URL</div> <input name="" type="text" id="uptimeURL"></div> <div class="TCP_options"><select id="newPortMonitorPort" name="newPortMonitorPort" style="margin-bottom: 14px;" class="smallInput "><option value="0" selected="selected">Please select</option><option value="1">HTTP (80)</option><option value="2">HTTPS (443)</option><option value="3">FTP (21)</option> <option value="4">SMTP (25)</option> <option value="5">POP3 (110)</option> <option value="6">IMAP (143)</option><option value="99">Custom Port</option> </select><div class="customPortClass" style="display:none;margin-top:13px"><div class="label">PORT</div> <input name="" type="text" id="customPortValue"></div></div><div class="keywordChecking" style="display:none;"><div class="label">ALERT WHEN</div><div class="radio_btn float-left alertWhenClass uptimeAlertExist" var="1" >Exists</div><div class="radio_btn float-left alertWhenClass uptimeAlertNotExist" var="2">Not exists</div><div class="clear-both"></div><div class="label">KEYWORD</div><textarea class="keywordInput"></textarea></div><div class="clear-both"></div>   </div>    </div> <div class="clear-both"></div> </div><div class="uptimeResultOptions" style="display:none"></div><div class="th rep_sprite" style="border-top:1px solid #c6c9ca; height:35px"><div class="btn_next_step float-right rep_sprite disabled next" id="enterUptimeDetails">Enter Details<div class="taper"></div></div><div class="float-right btn_action"  ><a class="rep_sprite"  id="addMonitorFinal" style="display:none" >Add Monitor</a></div><div class="float-right btn_action" id="addMonitorClose" style="display: none;"><a class="rep_sprite">Close</a></div></div></div>';
	//$("#modalDiv").dialog("close");
	//$("#modalDiv").dialog("destroy");
	//$('#modalDiv').html(content).dialog();
	$('#modalDiv').html(content).dialog({width:'auto',modal:true,position: 'center',resizable: false, open: function(event, ui) {bottomToolBarHide(); },close: function(event, ui) {bottomToolBarShow(); forceBackup=0; $("#modalDiv").html(''); }});
	
	$('#newPortMonitorPort').select2();
	$('.keywordChecking').hide();
	$('.TCP_options').hide();
	$('.uptimeOptions').hide();
	siteSelectorNanoReset();
}

function processMonitorAdd(data)
{
	resultsData_uptime = data.data.uptimeMonitorUptimeRobotAddMonitor,content = '';
	$.each(resultsData_uptime,function(key,val){
		if((typeof val != 'undefined')&&(val != null))
		{
		if(val['stat'] == 'ok')
		content = content+'<div class="monitor_results_site float-left" style="width:45%">'+site[key].name+'</div><div class="monitor_results_message float-left">Monitor is added successfully</div>';
		else
		content = content+'<div class="monitor_results_site float-left" style="width:45%"">'+site[key].name+'</div><div class="monitor_results_message float-left">'+val['message']+'</div>';
		}
		else
		{
			content = content+'<div class="monitor_results_site float-left" style="width:45%"">'+site[key].name+'</div><div class="monitor_results_message float-left">Error Adding Monitor</div>';
		}
		content = content + '<div class="clear-both"></div>';
	});
	
	$('.uptimeResultOptions').show();
	$('.uptimeResultOptions').html(content);
	$('.uptimeOptions').hide();
	//$('.uptimeSites').hide();
	//$('.uptimeOptions').show();
	$('#addMonitorClose').show();
	$('#selectWebsitesTab_uptime').removeClass('current');
	$('#selectWebsitesTab_uptime').addClass('completed');
	$('#enterDetailsTab_uptime').removeClass('current');
	$('#enterDetailsTab_uptime').addClass('completed');
	$('#uptimeResultsTab').addClass('current');
	$('#addMonitorFinal').hide();
	$('#addMonitorFinal').removeClass('disabled');
	//$(this).hide();
}
function processGetMonitor(data){
	//var siteArray = site['1'].URL;
	var content='',inner='';
	var logsVal={};
	var getMonitorData = data.data.uptimeMonitorUptimeRobotGetMonitor;
	if((getMonitorData != null)&&(getMonitorData['stat'] != 'fail'))
		{
		$.each(getMonitorData,function(k,v){
			$.each(v,function(ke,va){
				logsVal[va.monitorID]={};
				var nextVal = 0,prevNextVal = 0;
				$.each(va,function(key,val){
					if(key == 'logs')
					{
						var y=0;
						$.each(val,function(keyy,value){
							y++;
							
							logsVal[va.monitorID][keyy]={};
							logsVal[va.monitorID][keyy]['hour'] = value.hour;
							logsVal[va.monitorID][keyy]['time'] = value.time;
							logsVal[va.monitorID][keyy]['type'] = value.type;
							if (typeof va['logs'][keyy+1] != 'undefined')
							{
								if(y == '1')
								{
									var monitorDiff =  (new Date(value.time) - new Date(value.currentTime));
									var monitorDiff = monitorDiff/-3600000;
									/*monitorDiff = (monitorDiff * 27)+27;
									monitorDiff = monitorDiff/27;*/
									if(monitorDiff < 1)
									{ 
										monitorDiff = 0;
									}
								}
								/*else if(y>=va['logs'].length)
								{
									logsVal[va.monitorID][keyy]['width'] = 638-nextVal;
								}*/
								else
								{
									var monitorDiff = new Date(value.time) - new Date(va['logs'][keyy-1]['time']);
									
									var monitorDiff = monitorDiff/-3600000;
								}
								logsVal[va.monitorID][keyy]['width'] = (monitorDiff*27);
								prevNextVal = nextVal;
								nextVal = nextVal+(monitorDiff*27);
								if(nextVal > 638)
								{
									logsVal[va.monitorID][keyy]['width'] = nextVal - prevNextVal;
								}
							}
							else
							{    
								if(value.less24 == 'no')
								{
									if(y>=va['logs'].length)
									{
										logsVal[va.monitorID][keyy]['width'] = 647-nextVal;
									}
									else
									{
										logsVal[va.monitorID][keyy]['width'] = (24-( va.currentTime - value.todayStartHour))*27;
									}
								
								}
								else
								{
									if(y == '1')
									{
										var monitorDiff = new Date(value.currentTime) - new Date(value.time);
										monitorDiff = monitorDiff/3600000;
										//logsVal[va.monitorID][keyy]['width'] = (24+( va.currentTime - value.hour))*27;
										nextVal = nextVal+(monitorDiff*27);
										if(nextVal > 638)
										{
											logsVal[va.monitorID][keyy]['width'] = 647;
										}
										else
										{
											logsVal[va.monitorID][keyy]['width'] = monitorDiff*27;
										}
									}
									else
									{
										var monitorDiff = new Date(va['logs'][keyy-1]['time']) - new Date(value.time) ; 
										monitorDiff = monitorDiff/3600000;
										//var monitorDiff = 648-nextVal;
										
										prevNextVal = nextVal;
										nextVal = nextVal + (monitorDiff*27);
										if(nextVal > 638)
										{   
											monitorDiff =   nextVal - prevNextVal;
											logsVal[va.monitorID][keyy]['width'] = monitorDiff;
										}
										else
										{
											logsVal[va.monitorID][keyy]['width'] = monitorDiff*27;
										}
									}
								
								}
								
							}
						});
					}
				});
			});
		});
		}
	var getApiKeyData = data.data.uptimeMonitorUptimeRobotGetApiKey;
	var booFalse = new Boolean(false);
	if(getApiKeyData)
	{
		if((getMonitorData != null)&&(getMonitorData['stat'] != 'fail'))
		{
		$.each(getMonitorData,function(k,v){
			var subContent = '',innerTimeContent='',minContent='',y=parseInt(v['0']['currentTime']),x=y+1;
			content = content + '<div class="ind_row_cont"> <div class="row_summary"> <div class="row_arrow"></div><div class="row_name">'+k+'</div> <div class="row_backup_action rep_sprite" style="float:right;"><a class="trash bulkDeleteMonitor_conf rep_sprite_backup" title="Delete All Monitors"></a></div> <div class="del_conf_uptime float-right" style=" margin: 4px 0px 0px 514px;display:none"><div class="label">Sure?</div><div class="yes bulkDeleteMonitor" mid="775727613">Yes</div><div class="no bulkDeleteMonitor">No</div></div><div class="clear-both"></div> </div>  <div class="row_detailed "style="display:none"> <div class="rh"> <div class="row_arrow"></div><div class="row_name">'+k+'</div> <div class="row_backup_action rep_sprite" style="float:right;"><a class="trash bulkDeleteMonitor_conf rep_sprite_backup" title="Delete All Monitors"></a></div><div class="del_conf_uptime opened float-right" style="margin: 4px 0px 0px 514px;display:none "><div class="label">Sure?</div><div class="yes bulkDeleteMonitor opened" mid="775727613">Yes</div><div class="no bulkDeleteMonitor opened">No</div></div> <div class="clear-both"></div></div> <div class="rd"> <div class="row_updatee"> <div class="row_updatee_ind"><div class="timeline_hour_labels_cont"> <div style="width:50px; margin-right: 5px;">Last 24h</div>';
			for(var d=0;d < 24;d++)
			{ 
			var curVal = parseInt(v['0']['currentTime']);
			//curVal = curVal+x;
			
			minContent = minContent+'  <div>'+x+'</div> ';
			if(x>22)
			{   
				x=x-24;
			}
			x++;
			}
			content =content+minContent+' <div class="clear-both"></div></div></div>';
					$.each(v,function(key,val){
						var playPauseStatus = val.playPauseStatus;
						if((typeof val['logs'] != 'undefined')&&(val['logs'] != null))
						{
							if((typeof val['logs'][0] != 'undefined')&&(val['logs'][0] != null))
							var uptimeCurrentType = val['logs'][0]['type'];
						}
						if(playPauseStatus == '0')
						{
							playPauseStatus = 'off';
						}
						else
						{
							playPauseStatus = 'on';
						}
						subContent = subContent+'<div class="row_updatee_ind"><div class="label_updatee float-left" ><a class="label droid700 monName float-right" style="color: #666;" title = "'+val.monitorURL+'">'+val.monitorURL+'</a></div><div class="row_action float-left "><a class="needConfirm editMonitor" style="display:none" mid="'+val.monitorID+'" mName="'+val.monitorName+'">Edit</a></div><div class="row_backup_action rep_sprite" style="float:right;"><a class=" rep_sprite_backup deleteMonitor_conf float-right" mid="'+val.monitorID+'" title="Delete Monitor"></a></div><div class="del_conf_uptime" style="display:none"><div class="label">Sure?</div><div class="yes deleteMonitor" mid="'+val.monitorID+'">Yes</div><div class="no deleteMonitor">No</div></div><div class="row_checkbox float-left on_off delConfHide"  ><div class="cc_mask "><div class="cc_img monitorON_OFF '+playPauseStatus+'" mid="'+val.monitorID+'"></div></div></div><div class="uptime_percentage timeline_uptime_'+uptimeCurrentType+' delConfHide">'+val.alltimeuptimeratio+'%</div> <div class="items_cont uptimePercCont delConfHide float-left"><div class="timeline_cont">'; 
						  innerTimeContent = '';  
						 var n = 0,endTimeMonitor; 
						$.each(val.logs,function(keyy,value){ 
						if(keyy == 0)
						{
							endTimeMonitor = value.currentTime;
						}
						else
						{
							endTimeMonitor = val['logs'][keyy-1]['time'];
						}
						 innerTimeContent = innerTimeContent+' <div class="timeline_'+value.type+' timeQtip" timeType="'+value.type+'" timeStart="'+value.time+'" timeEnd="'+endTimeMonitor+'" timeCurrent="'+value.currentTime+'" style="width:'+logsVal[val.monitorID][keyy]['width']+'px"></div> ';
						});
						subContent = subContent+innerTimeContent+'<div class="clear-both"></div></div> </div> <div class="clear-both"></div></div>';
					});
					
				 content = content+subContent+'</div> </div> </div></div>';
		});
		
		content = '<div class="result_block backup  shadow_stroke_box siteSearch uptime" id="backupPageMainCont"><div class="rows_cont"></div><div class="th rep_sprite"><div class="type_filter "><input name="" type="text" class="input_type_filter searchItems" value="type to filter" style="display: none;"><div class="" style="margin-top: 7px;"><span class="droid700">Uptime Monitor</span></div><div class="clear_input rep_sprite_backup" onclick=""></div></div><div class="btn_action float-right"><a class="rep_sprite scheduleUptime">Create New Monitor</a></div></div><div class="no_match hiddenCont" style="display:none">Bummer, there are no backups that match.<br>Try typing lesser characters.</div>'+content+'<div class="empty_data_set" style="display:none"><div class="line2">Looks like there are <span class="droid700">no uptime Monitors scheduled now</span>. Create a <a class="scheduleUptime">Schedule Uptime</a>.</div></div>';
		}
		else 
		{
			content = '<div class="result_block backup  shadow_stroke_box siteSearch uptime" id="backupPageMainCont"><div class="rows_cont"></div><div class="th rep_sprite"><div class="type_filter "><input name="" type="text" class="input_type_filter searchItems" value="type to filter" style="display: none;"><div class="" style=" margin-top: 7px;"><span class="droid700">Uptime Monitor</span></div><div class="clear_input rep_sprite_backup" onclick=""></div></div><div class="btn_action float-right"><a class="rep_sprite scheduleUptime">Create New Monitor</a></div></div><div class="no_match hiddenCont" style="display:none">Bummer, there are no backups that match.<br>Try typing lesser characters.</div><div class="empty_data_set" ><div class="line2">Looks like there are <span class="droid700">no monitors set up</span>. Create a <a class="scheduleUptime">new monitor</a>.</div></div>';
			if((getMonitorData != null)&&(getMonitorData['stat'] == 'fail')&&(getMonitorData['message'] == 'apiKey is wrong'))
			{
				content = '<div class="empty_data_set"><div class="line2">Your UptimeRobot account API key is incorrect. Go to <a class="uptimeSettingsClick" id="">Settings</a> to do it now.</div></div>';
			}
		}
	}
	else
	{
		content = '<div class="empty_data_set"><div class="line2">You have not connected your Uptime Robot account using your API key. Go to <a class="uptimeSettingsClick" id="">Settings</a> to do it now.</div></div>';
	}
	$("#pageContent").html(content);
	$(".timeQtip").each(function(){
		$(this).qtip({id:"toggleGroupQtip",content: { text: 'Status - '+$(this).attr("timeType").toUpperCase()+'<br />Start Time - '+$(this).attr("timestart")+'<br />End Time - '+$(this).attr("timeEnd")+'' }, position: { my: 'bottom center', at: 'top center',  adjust:{ y: -6} }, show: { event: 'mouseenter', effect: { type: 'slide' } }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 10, height:5} } });
	});
	$(".deleteMonitor_conf").qtip({content: { text: 'Delete Monitor' }, position: { my: 'left center', at: 'right center' }, show: { event: 'mouseenter' }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 5, height:8} } });
	$(".bulkDeleteMonitor_conf ").qtip({content: { text: 'Delete All Monitors' }, position: { my: 'left center', at: 'right center' }, show: { event: 'mouseenter' }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 5, height:8} } });
	$("#loadingDiv").hide();
	$('.navLinks').removeClass('disabled');
	/*$(".timeline_down").each(function(){
		$(".timeline_up").qtip({id:"toggleGroupQtip",content: { text: 'Status Down ---Start Time='+$(this).attr("timestart")+'' }, position: { my: 'bottom center', at: 'top center',  adjust:{ y: -6} }, show: { event: 'mouseenter', effect: { type: 'slide' } }, hide: { event: 'mouseleave' }, style: { classes: 'ui-tooltip-shadow ui-tooltip-dark',  tip: {  corner: true, width: 10, height:5} } });
	});*/
	//var pos = $('.time22').width();

}
function processPauseMonitor(data){
	
}
function processDelMonitor(data){
	$("#loadingDiv").hide();
}
function processEditMonitor(data){
	var editMonitorData = data.data.uptimeMonitorUptimeRobotGetSingleMonitor;
	//var type = editMonitorData.type;
	loadUptimePage();
	if(editMonitorData.type == 'http')
	{
	$('#http').click();
	$('.uptimeType').addClass('disabled');
	}
	else if(editMonitorData.type == 'keywordChecking')
	{
	$('#keywordChecking').click();
	$('.uptimeType').addClass('disabled');
	}
	else if(editMonitorData.type == 'ping')
	{
		$('#ping').click();
		$('.uptimeType').addClass('disabled');
	}
	else if(editMonitorData.type == 'tcp')
	{
		$('#TCP').click();
		$('.uptimeType').addClass('disabled');
	}
$('#monitorName').val(editMonitorData.friendlyname);
$('#uptimeUserName').val(editMonitorData.httpusername);
$('#uptimePassword').val(editMonitorData.httppassword);
$('.keywordInput').val(editMonitorData.keywordvalue);
if(editMonitorData.keywordtype == '2')
$('.uptimeAlertNotExist').addClass('active');
if(editMonitorData.keywordtype == '1')
$('.uptimeAlertExist').addClass('active');


}

function processBulkPause(data){
	
}
function processBulkDelete(data){
	
	$("#loadingDiv").hide();
}
function processUptimeAPIKeySave(data){
	$("#saveSettingsBtn").removeClass('disabled');
	$(".settings_cont .btn_loadingDiv").remove();
	var mainData = data.data;
	var apiKey = mainData.uptimeMonitorUptimeRobotSaveApiKey;
	if(typeof apiKey != 'undefined' && apiKey != null && apiKey != false){
		$("#saveSuccess").show();
		setTimeout(function () { $("#saveSuccess").hide();},1000);
		$("#settings_btn").removeClass('active');
		settingsData['data']['getSettingsAll']['settings']['uptimeRobot']['apiKey']=apiKey;
	}
}
function processAPIKeySet(data)
{
	
	var booFalse = new Boolean(false);
	settingsData['data']['getSettingsAll']['settings']['uptimeRobot'] = {};
	if((data.data.uptimeMonitorUptimeRobotGetApiKey))
	{
		// $('#uptimeRobotApiKey').val(data.data.uptimeMonitorUptimeRobotGetApiKey);
		settingsData['data']['getSettingsAll']['settings']['uptimeRobot']['apiKey'] = data.data.uptimeMonitorUptimeRobotGetApiKey;
	}
	else
	{
		// $('#uptimeRobotApiKey').val('');
		settingsData['data']['getSettingsAll']['settings']['uptimeRobot']['apiKey'] = '';
		// $('#uptimeRobotApiKey').attr('placeholder',"Enter Api key here");
	}
}

/*function timeDiff(startTime,endTime){
	startTime = parseFloat(startTime);
	endTime = parseFloat(endTime);
	startTimeInt = parseInt(startTime);
	endTimeInt = parseInt(endTime);
	var countTime = 0;
	for(var i = 0;i<23;i++)
	{
		if(startTimeInt!=endTimeInt)
		{
			countTime++;
		}
		if(startTimeInt > 23)
		{
			startTimeInt = 0;
		}
		if(startTimeInt==endTimeInt)
		{
			return countTime;
		}
		startTimeInt++;
	}
}*/