<?php

/*
slug: uptimeMonitorUptimeRobot
version: 1.0.5
*/

class addonUptimeMonitorUptimeRobot{
	
	private static $version = '1.0.5';

	public static function version(){
		return self::version;	
	}
	
	public static function init(){
		require_once(APP_ROOT."/addons/uptimeMonitorUptimeRobot/controllers/func.php");
		panelRequestManager::addFunctions('uptimeMonitorUptimeRobotAddMonitor', 'uptimeMonitorUptimeRobotGetMonitor', 'uptimeMonitorUptimeRobotDeleteMonitor', 'uptimeMonitorUptimeRobotChangeStatus', 'uptimeMonitorUptimeRobotSaveApiKey', 'uptimeMonitorUptimeRobotGetApiKey');
		regHooks('addonMenus');
	}	
}

?>