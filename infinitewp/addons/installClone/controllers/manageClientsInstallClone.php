<?php
class manageClientsInstallClone{
	
	public static function installCloneNewSiteProcessor($siteIDs=array(), $params){

		$type = "installClone";
		$action = "newSite";
		
		if($params['backupURL'] != 'localPackage' && empty($params['cloneFromURL']) && empty($params['sourceSiteID']) ){//exsiting site to new location
			addNotification($type='E', $title='Install/Clone error', 'Param sourceSiteID missing.', $state='U', $callbackOnClose='', $callbackReference='');
		 	return;
		}
		
		set_time_limit(0);
		
		$userID = (isset($GLOBALS['userID']) && !empty($GLOBALS['userID']))?$GLOBALS['userID']:$_SESSION['userID'];
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => 'newSite', 'detailedAction' => $type);
		$historyData = array('siteID' => 0, 'actionID' => Reg::get('currentRequest.actionID'), 'userID' => $userID, 'type' => $type, 'action' => $action, 'events' => 1, 'status' => 'pending', 'URL' => $params['newSiteURL'], 'timeout' => 1200, 'isPluginResponse' => '0');
		
		$historyID = addHistory($historyData, $historyAdditionalData);
		
		$compactVars = compact('params');
		DB::insert("?:temp_storage", array('type' => 'installCloneNewSite', 'paramID' => $historyID, 'time' => time(), 'data' =>  serialize($compactVars)));
		//rest of functionality will happen in method installCloneNewSitePreProcessor()
	}
	
	public static function installCloneNewSitePreProcessor($historyID){
		
		$compactVars = DB::getField("?:temp_storage", "data", "type = 'installCloneNewSite' AND paramID = ".$historyID);
		DB::delete("?:temp_storage", "type = 'installCloneNewSite' AND paramID = ".$historyID);
		
		$compactVars = unserialize($compactVars);
		extract($compactVars);
		
		
		$hostName = trim($params['hostName']);
		$hostUserName = trim($params['hostUserName']); 
		$hostPassword = trim($params['hostPassword']);
		$hostPort = trim($params['hostPort']);
		$hostSSL = trim($params['hostSSL']);
		$hostPassive = trim($params['hostPassive']);
                $use_sftp = trim($params['use_sftp']);
				
		$parts = parse_url($params['newSiteURL']);
		
                if(isset($use_sftp) && $use_sftp==1) {
                    $path = APP_ROOT.'/lib/phpseclib';
                    set_include_path(get_include_path() . PATH_SEPARATOR . $path);
                    include_once('Net/SFTP.php');
                    
                    $sftp = new Net_SFTP($hostName);
                        if(!$sftp) {
                            updateHistory(array('status' => 'error'), $historyID);
                            DB::update("?:history_additional_data", array('status' => 'error', 'errorMsg' => 'Connection to the SFTP Host failed. Check your Hostname.'), "historyID=".$historyID);
                            return false; //array('error' => 'Connect to the Host failed, Check your hostName');
                        }
                    if (!$sftp->login($hostUserName, $hostPassword)) {
                        updateHistory(array('status' => 'error'), $historyID);
                        DB::update("?:history_additional_data", array('status' => 'error', 'errorMsg' => 'Could not login to SFTP. Please check the credentials.'), "historyID=".$historyID);
                        return false; //array('error' => 'Connect to the Host failed, Check your hostName');
                    } else {
                            $uploadPath = '/'.trim($params['remoteFolder'], '/').'/clone_controller';
                            $cloneTempPath = $uploadPath.'/clone_temp';

                            $file_bridge 		= "/bridge.php";  
                            $file_fileSystem 	= "/fileSystem.php";  
                            $file_pclzip 		= "/class-pclzip.php";
                            
                            $sftp->mkdir($uploadPath,-1,true);
                            $sftp->mkdir($cloneTempPath,-1,true);
                            $sftp->chdir($uploadPath);
                            
                            @$sftp->put(basename($file_bridge), APP_ROOT."/addons/installClone/bridge".$file_bridge, NET_SFTP_LOCAL_FILE);
                            @$sftp->put(basename($file_fileSystem), APP_ROOT."/addons/installClone/bridge".$file_fileSystem, NET_SFTP_LOCAL_FILE);
                            @$sftp->put(basename($file_pclzip), APP_ROOT."/addons/installClone/bridge".$file_pclzip, NET_SFTP_LOCAL_FILE);
                            
                            /*
                             * PHP Lib Upload Start here
                             */
                            
                            $sftp->mkdir($uploadPath.'/phpseclib/Crypt',-1,true);
                            $sftp->mkdir($uploadPath.'/phpseclib/File',-1,true);
                            $sftp->mkdir($uploadPath.'/phpseclib/Math',-1,true);
                            $sftp->mkdir($uploadPath.'/phpseclib/Net/SFTP',-1,true);
                            $sftp->mkdir($uploadPath.'/phpseclib/System',-1,true);
                            
                            $sftp->chdir($uploadPath.'/phpseclib/Crypt');
                            @$sftp->put('AES.php', APP_ROOT."/lib/phpseclib/Crypt/AES.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('Base.php', APP_ROOT."/lib/phpseclib/Crypt/Base.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('Blowfish.php', APP_ROOT."/lib/phpseclib/Crypt/Blowfish.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('DES.php', APP_ROOT."/lib/phpseclib/Crypt/DES.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('Hash.php', APP_ROOT."/lib/phpseclib/Crypt/Hash.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('Random.php', APP_ROOT."/lib/phpseclib/Crypt/Random.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('RC2.php', APP_ROOT."/lib/phpseclib/Crypt/RC2.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('RC4.php', APP_ROOT."/lib/phpseclib/Crypt/RC4.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('Rijndael.php', APP_ROOT."/lib/phpseclib/Crypt/Rijndael.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('RSA.php', APP_ROOT."/lib/phpseclib/Crypt/RSA.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('TripleDES.php', APP_ROOT."/lib/phpseclib/Crypt/TripleDES.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('Twofish.php', APP_ROOT."/lib/phpseclib/Crypt/Twofish.php", NET_SFTP_LOCAL_FILE);
                            
                            $sftp->chdir($uploadPath.'/phpseclib/File');
                            @$sftp->put('ANSI.php', APP_ROOT."/lib/phpseclib/File/ANSI.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('ASN1.php', APP_ROOT."/lib/phpseclib/File/ASN1.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('X509.php', APP_ROOT."/lib/phpseclib/File/X509.php", NET_SFTP_LOCAL_FILE);
                            
                            $sftp->chdir($uploadPath.'/phpseclib/Math');
                            @$sftp->put('BigInteger.php', APP_ROOT."/lib/phpseclib/Math/BigInteger.php", NET_SFTP_LOCAL_FILE);
                            
                            $sftp->chdir($uploadPath.'/phpseclib/Net');
                            @$sftp->put('SCP.php', APP_ROOT."/lib/phpseclib/Net/SCP.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('SFTP.php', APP_ROOT."/lib/phpseclib/Net/SFTP.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('SSH1.php', APP_ROOT."/lib/phpseclib/Net/SSH1.php", NET_SFTP_LOCAL_FILE);
                            @$sftp->put('SSH2.php', APP_ROOT."/lib/phpseclib/Net/SSH2.php", NET_SFTP_LOCAL_FILE);
                            
                            $sftp->chdir($uploadPath.'/phpseclib/Net/SFTP');
                            @$sftp->put('Stream.php', APP_ROOT."/lib/phpseclib/Net/SFTP/Stream.php", NET_SFTP_LOCAL_FILE);
                            
                            $sftp->chdir($uploadPath.'/phpseclib/System');
                            @$sftp->put('SSH_Agent.php', APP_ROOT."/lib/phpseclib/System/SSH_Agent.php", NET_SFTP_LOCAL_FILE);
                            
                            $sftp->chdir($uploadPath.'/phpseclib');
                            @$sftp->put('openssl.cnf', APP_ROOT."/lib/phpseclib/openssl.cnf", NET_SFTP_LOCAL_FILE);
                            
                            $sftp->chdir($uploadPath);
                            
                            /*
                             * PHP Lib Upload End here
                             */
                            
                            if($params['backupURL'] == 'localPackage'){
                                    $packagePath = installCloneGetWPPackage();
                                    if(empty($packagePath)){
                                            updateHistory(array('status' => 'error'), $historyID);
                                            DB::update("?:history_additional_data", array('status' => 'error', 'errorMsg' => 'Unable to locate WP package.'), "historyID=".$historyID);	
                                            return false; //array('error' => 'Unable to locate WP package.!'); 
                                    }
                                    //$uploadWP = @ftp_put($connection, $uploadPath.'/WPPackage.zip', $packagePath, FTP_BINARY);
                                    @$sftp->put('WPPackage.zip', $packagePath, NET_SFTP_LOCAL_FILE);
                                    $uploadWP = 1;

                                    $oldUser = '';
                                    $oldURL = '';
                            }
                            elseif(!empty($params['cloneFromURL'])){
                                    $oldUser = '';
                                    $oldURL = '';
                            }
                            else{
                                    $sourceSiteData = getSiteData($params['sourceSiteID']);
                                    $oldUser = $sourceSiteData['adminUsername'];
                                    $oldURL = $sourceSiteData['URL'];
                            }

                            
                            
                            //----------------------------------
                    }
                    
                } else {
		
		if(!empty($hostSSL) && function_exists('ftp_ssl_connect')){
			$connection = @ftp_ssl_connect($hostName, $hostPort);
		}
		else{
			$connection = @ftp_connect($hostName, $hostPort);
		}
	
		
		if (!$connection ){
			updateHistory(array('status' => 'error'), $historyID);
			DB::update("?:history_additional_data", array('status' => 'error', 'errorMsg' => 'Connection to the Host failed. Check your Hostname.'), "historyID=".$historyID);
			return false; //array('error' => 'Connect to the Host failed, Check your hostName');
		}		
		
		
		$login = @ftp_login($connection, $hostUserName, $hostPassword);	
		
		
		if (!$login) { 
			updateHistory(array('status' => 'error'), $historyID);
			DB::update("?:history_additional_data", array('status' => 'error', 'errorMsg' => 'Could not login to FTP. Please check the credentials.'), "historyID=".$historyID);
			return false; //array('error' => 'Connection attempt failed!');
		}		
		else{
		
			if(!empty($hostPassive)){
				@ftp_pasv($connection, true);
			}
			
			$uploadPath = '/'.trim($params['remoteFolder'], '/').'/clone_controller';
			$cloneTempPath = $uploadPath.'/clone_temp';
			
			$parts = explode("/", $cloneTempPath);
			$countParts = count($parts);
			
			$return = true;
			$fullpath = "";						
			$i = 0;
			foreach($parts as $part){				
				$i++;
				if(empty($part)){
					$fullpath .= "/";
					continue;
				}
				$fullpath .= $part."/";
				if(@ftp_chdir($connection, $fullpath)){
					ftp_chdir($connection, $fullpath);
				}else{
					if(@ftp_mkdir($connection, $part)){
						ftp_chdir($connection, $part);						
						if($part == 'clone_temp' && $countParts == $i){//$countParts == $i to make sure it is last folder	
							if (function_exists('ftp_chmod') ){
								@ftp_chmod($connection, 0777, $fullpath);
							}
							else{
								@ftp_site($connection, sprintf('CHMOD %o %s', 0777, $fullpath));
							}
						}
					}else{
						$return = false;
					}
				}
			}
			
			if($return == false){
				updateHistory(array('status' => 'error'), $historyID);
				DB::update("?:history_additional_data", array('status' => 'error', 'errorMsg' => 'Unable to create a directory using the FTP credentials.'), "historyID=".$historyID);
				return false; //array('error' => 'Error creating the directory'); 
			}
			
			$file_bridge 		= "/bridge.php";  
			$file_fileSystem 	= "/fileSystem.php";  
			$file_pclzip 		= "/class-pclzip.php";
				
			$uploadBridge 	= @ftp_put($connection, $uploadPath.$file_bridge, APP_ROOT."/addons/installClone/bridge".$file_bridge, FTP_ASCII);
			$uploadFS 		= @ftp_put($connection, $uploadPath.$file_fileSystem, APP_ROOT."/addons/installClone/bridge".$file_fileSystem, FTP_ASCII);
			$uploadPCL 		= @ftp_put($connection, $uploadPath.$file_pclzip, APP_ROOT."/addons/installClone/bridge".$file_pclzip, FTP_ASCII);
			
			if($params['backupURL'] == 'localPackage'){
				$packagePath = installCloneGetWPPackage();
				if(empty($packagePath)){
					updateHistory(array('status' => 'error'), $historyID);
					DB::update("?:history_additional_data", array('status' => 'error', 'errorMsg' => 'Unable to locate WP package.'), "historyID=".$historyID);	
					return false; //array('error' => 'Unable to locate WP package.!'); 
				}
				$uploadWP = @ftp_put($connection, $uploadPath.'/WPPackage.zip', $packagePath, FTP_BINARY);
				$uploadWP = 1;
				
				$oldUser = '';
				$oldURL = '';
			}
			elseif(!empty($params['cloneFromURL'])){
				$oldUser = '';
				$oldURL = '';
			}
			else{
				$sourceSiteData = getSiteData($params['sourceSiteID']);
				$oldUser = $sourceSiteData['adminUsername'];
				$oldURL = $sourceSiteData['URL'];
			}
				
			if (!$uploadBridge || !$uploadFS || !$uploadPCL || ($params['backupURL'] == 'localPackage' && !$uploadWP)) {
				updateHistory(array('status' => 'error'), $historyID);
				DB::update("?:history_additional_data", array('status' => 'error', 'errorMsg' => 'FTP upload failed.'), "historyID=".$historyID);
				return false; //array('error' => 'FTP upload failed!'); 
			}
		}
		
		@ftp_close($connection);
		
                }
		
		$destinationURL = removeTrailingSlash($params['newSiteURL'])."/clone_controller".$file_bridge;
		
		$newUserPassword = !empty($params['newUserPassword']) ? md5($params['newUserPassword']) : '';
			  
		$requestParams = array('dbHost' => $params['dbHost'], 'dbUser' => $params['dbUser'], 'dbPassword' => $params['dbPassword'], 'dbName' => $params['dbName'], 'oldSite' => $oldURL, 'oldUser' => $oldUser, 'newSiteURL' => $params['newSiteURL'], 'newUser' => $params['newUserName'], 'newPassword' => $newUserPassword, 'admin_email' => $params['adminEmail'], 'memorySize' => '256', 'toIWP' => $params['toIWP'], 'backupURL' => $params['backupURL'], 'db_table_prefix' => $params['dbPrefix'] , 
		'ftpHost' => $params['hostName'], 
		'ftpPort' => $params['hostPort'],
		'ftpUser' => $params['hostUserName'],
		'ftpPass' => $params['hostPassword'],		
		'ftpBase' => '/'.trim($params['remoteFolder'], '/'),
		'ftpSSL' => $params['hostSSL'],
		'ftpPassive' => $params['hostPassive'],
		/*, 'owner' => 'infinitewp.com'*/);
		//
                if(isset($params['use_sftp'])) {
                    $requestParams['ftpUseSftp'] = $params['use_sftp'];
                }
				
		$updateHistoryData = array('param1' => base64_encode(serialize($requestParams)), 'param2' => $params['newSiteURL'], 'status' => 'pending', 'URL' => $destinationURL);
		
		updateHistory($updateHistoryData, $historyID);
				
		DB::insert("?:history_raw_details", array('historyID' => $historyID, 'request' => base64_encode(serialize($requestParams)), 'panelRequest' => serialize($_REQUEST)));
	}
	
	public static function installCloneNewSiteResponseProcessor($historyID, $responseData){
		
		$start = '#Status(';
		$end = ')#';
		
		$strBetArray = getStrBetAll($responseData,$start,$end);
		//$statusData = unserializeArray($strBetArray);
		$statusData = unserializeBase64DecodeArray($strBetArray);
		
		$finalStateReached = false;
		$responseDataReadable = false;
		
		foreach($statusData as $d1){
			$responseDataReadable = true;
			foreach($d1 as $d2 => $d3){
				//echo "$d2 : $d3<br />\n";
				if($d2 == "error"){
					$finalStateReached = true;
					DB::update("?:history_additional_data", array('status' => 'error', 'error' => 'error', 'errorMsg' => $d3), "historyID=".$historyID);
				}
				elseif($d2 == "success" && $d3 == 'clone_completed'){
					$finalStateReached = true;
					DB::update("?:history_additional_data", "status='success'", "historyID=".$historyID);
				}
				elseif($d2 == "options" && (!empty($d3))){				
						//Add site Function.
						$_POST = array('action' => 'addSite', 
						'args' => array(
						'params' => array('URL' => $d3['URL'], 'username' => $d3['userName'], 'activationKey' => $d3['activationKey']), 
						'siteIDs' => array()
						)
						);
						panelRequestManager::handler($_POST);				
				}	
			}
		}//end of foreach($statusData as $d1)
		if($responseDataReadable === true && $finalStateReached === false){
			DB::update("?:history_additional_data", array('status' => 'error', 'error' => 'error', 'errorMsg' => 'An unknown error occured in Install/Clone process.'), "historyID=".$historyID);
		}
	}
	
	public static function installCloneExistingSiteProcessor($siteIDs, $params){
		
		
		$type = "installClone";
		$action = "existingSite";
		$requestAction = "restore";
		$timeout = (20 * 60);
		
		$maintainOldKey = false;
		
		$historyAdditionalData = array();
	    $historyAdditionalData[] = array('uniqueName' => 'existingSite', 'detailedAction' => $type);
		
		
		if($params['backupURL'] == 'localPackage'){//FreshWP to Existing site
			$params['backupURL'] = installCloneGetWPPackage($URLRequired=true);
			$oldUser = '';
			$params['iwpClone'] = '1';
		}
		elseif(!empty($params['cloneFromURL'])){//BackURL to Existing site
			$oldUser = '';
		}
		else{//Site Backup to Existing site
			if(!empty($params['sourceSiteID'])){
				$sourceSiteData = getSiteData($params['sourceSiteID']);
				$oldUser = $sourceSiteData['adminUsername'];
				$maintainOldKey = true;
			}
			else{
				addNotification($type='E', $title='Install/Clone error', 'Param sourceSiteID missing.', $state='U', $callbackOnClose='', $callbackReference='');
				return;	
			}
		}
		
		
		
		foreach($siteIDs as $siteID){
			$siteData = getSiteData($siteID);
			
			$requestParams = array('backup_url' => $params['backupURL'], 'overwrite' => '1', 'new_user' => $params['newUserName'], 'new_password' => $params['newUserPassword'], 'old_user' => $oldUser, 'clone_from_url' => $params['cloneFromURL'], 'iwp_clone' => $params['iwpClone'], 'admin_email' => $params['adminEmail']);

			if($maintainOldKey){
				$requestParams['maintain_old_key'] = 1;
			}
			
			$events=1;
			$PRP = array();
			$PRP['requestAction'] 	= $requestAction;
			$PRP['requestParams'] 	= $requestParams;
			$PRP['siteData'] 		= $siteData;
			$PRP['type'] 			= $type;
			$PRP['action'] 			= $action;
			$PRP['events'] 			= $events;
			$PRP['historyAdditionalData'] 	= $historyAdditionalData;
			$PRP['timeout'] 		= $timeout;
			
		   prepareRequestAndAddHistory($PRP);
		}
	}
	
	public static function installCloneExistingSiteResponseProcessor($historyID, $responseData){
		
		if($responseData['success'] && empty($responseData['success'])){
			return false;
		}
		
		if(!empty($responseData['error'])){
			DB::update("?:history_additional_data", array('status' => 'error', 'errorMsg' => $responseData['error']), "historyID=".$historyID);	
		}
		
		if(!empty($responseData['success'])){
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID);	
			
			if($responseData['success'] != 1){
				$historyData = DB::getRow("?:history", "*", "historyID=".$historyID);
				$siteID = $historyData['siteID'];
				DB::update("?:sites", array('adminUsername' => $responseData['success']), "siteID=".$siteID);
			}
		}
	}
	
	public static function installCloneBackupNowProcessor($siteIDs, $params){
		
		$type = "installClone";
		$action = 'installCloneBackupNow';
		$config = $params['config'];
		$timeout = (20 * 60);//20 mins
		$requestAction = "scheduled_backup";
		
		$exclude = explode(',', $config['exclude']);
		$include = explode(',', $config['include']);			
		array_walk($exclude, 'trimValue');
		array_walk($include, 'trimValue');
		
		$siteData = getSiteData($siteIDs);
		$requestParams = array('task_name' => 'Backup Now', 'mechanism' => 'singleCall', 'args' => array('type' => $type, 'action' => $action, 'what' => 'full', 'optimize_tables' => '', 'exclude' => $exclude, 'include' => $include, 'del_host_file' => '', 'disable_comp' => '', 'limit' => '5', 'backup_name' => 'Backup for Clone'));
			 		 			  
		$historyAdditionalData = array();
		$historyAdditionalData[] = array('uniqueName' => 'Backup clone', 'detailedAction' => $type);
			  
		$events=1;
		$PRP = array();
		$PRP['requestAction'] 	= $requestAction;
		$PRP['requestParams'] 	= $requestParams;
		$PRP['siteData'] 		= $siteData;
		$PRP['type'] 			= $type;
		$PRP['action'] 			= $action;
		$PRP['events'] 			= $events;
		$PRP['historyAdditionalData'] 	= $historyAdditionalData;
		$PRP['timeout'] 		= $timeout;
		$PRP['directExecute'] 		= true;
		
		prepareRequestAndAddHistory($PRP);
		
	
	}
	
	public static function installCloneBackupNowResponseProcessor($historyID, $responseData){
		
		responseDirectErrorHandler($historyID, $responseData);
		
		if(empty($responseData['success'])){
			return false;
		}
		
		if(!empty($responseData['success']['error'])){
			DB::update("?:history_additional_data", array('status' => 'error', 'errorMsg' => $responseData['success']['error']), "historyID=".$historyID);	
			return array('status' => 'error', 'errorMsg' => ' backup error ');
		}
		
		$historyData = DB::getRow("?:history", "*", "historyID=".$historyID);
		$siteID = $historyData['siteID'];
		
		if(!empty($responseData['success']['task_results']) && $historyData['action'] == 'installCloneBackupNow'){
			DB::update("?:history_additional_data", array('status' => 'success'), "historyID=".$historyID);
			$siteBackupsTemp = $responseData['success']['task_results'];	
			
			$siteStats = DB::getRow("?:site_stats", "stats", "siteID=".$siteID);
			$siteStats = unserialize(base64_decode($siteStats['stats']));
			$siteStats["iwp_backups"]["Backup Now"] = $siteBackupsTemp;
			
			$siteStats = base64_encode(serialize($siteStats));
			
			DB::update("?:site_stats", array('stats' => $siteStats), "siteID=".$siteID);
		}
	}
}
manageClients::addClass('manageClientsInstallClone');

?>