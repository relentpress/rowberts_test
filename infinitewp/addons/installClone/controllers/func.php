<?php

function installCloneAddonMenus(&$menus){
	//$menus[] = '<li><a class="navLinks optionSelect" page="installClone">Install / Clone WP</a></li>';
	$menus['tools']['subMenus'][] = array('page' => 'installClone', 'displayName' => 'Install / Clone WP');
}

function installCloneResponseProcessors(&$responseProcessor){
	$responseProcessor['installClone']['newSite'] = 'installCloneNewSite';
	$responseProcessor['installClone']['existingSite'] = 'installCloneExistingSite';
	$responseProcessor['installClone']['installCloneBackupNow'] = 'installCloneBackupNow';
}

function cloneSiteBackupsHTML($siteID){
	$sitesBackups = panelRequestManager::getSitesBackups(array($siteID));
	$HTML = TPL::get('/addons/installClone/templates/cloneView.tpl.php', array('siteBackups' => reset($sitesBackups), 'siteID' => $siteID));
	return $HTML;
}

function installCloneTaskTitleTemplate(&$template){
	$template['installClone']['newSite'][''] = "Install WordPress in 1 site";
	$template['installClone']['existingSite'][''] = "Clone 1 site";
	$template['installClone']['installCloneBackupNow'][''] = "Backup 1 site for cloning";
}

function installCloneGetWPPackage($URLRequired=false){
	$WPInstallPackage = getOption('WPInstallPackage');
	$WPInstallPackage = unserialize($WPInstallPackage);
	
	if(empty($WPInstallPackage)){
		return false;	
	}
	
	$customPackagePath = APP_ROOT.'/addons/installClone/bridge/WP_custom.zip';
	if(file_exists($packagePath)){
		return $customPackagePath;
	}
	
	$packagePath = APP_ROOT.'/addons/installClone/bridge/WP_'.$WPInstallPackage['version'].'.zip';
	
	
	if(!$URLRequired){
		$finalPackage = $packagePath;
	}
	else{
		$appURL = APP_URL;
		$settings = Reg::get('settings');
		if(!empty($settings['httpAuth']['username'])){
			$appURL = str_replace('://', '://'.$settings['httpAuth']['username'].':'.$settings['httpAuth']['password'].'@', $appURL);
		}
		$finalPackage = str_replace(APP_ROOT.'/', $appURL, $packagePath);
	}
	
	if(file_exists($packagePath)){
		return $finalPackage;
	}
	else{
		if(installCloneDownloadWPPackage($WPInstallPackage)){
			return $finalPackage;
		}
	}
	return false;
}

function installCloneDownloadWPPackage($WPInstallPackage){
	$isDone = false;
	if(!empty($WPInstallPackage['downloadURL'])){
		//delete old one
		$lastVersion = getOption('lastWPInstallPackageDownloaded');
		@unlink(APP_ROOT.'/addons/installClone/bridge/WP_'.$lastVersion.'.zip');
		
		//new download
		addNotification($type='N', $title='DOWNLOADING WORDPRESS '.$WPInstallPackage['display_version'], $message='The latest WordPress '.$WPInstallPackage['display_version'].' package is being downloaded.', $state='U', $callbackOnClose='', $callbackReference='');
		$isDone = downloadURL($WPInstallPackage['downloadURL'], APP_ROOT.'/addons/installClone/bridge/WP_'.$WPInstallPackage['version'].'.zip');
		if($isDone && file_exists(APP_ROOT.'/addons/installClone/bridge/WP_'.$WPInstallPackage['version'].'.zip')){
			addNotification($type='N', $title='WORDPRESS '.$WPInstallPackage['display_version'].' DOWNLOAD COMPLETE', $message='The latest WordPress '.$WPInstallPackage['display_version'].' package has been downloaded. This will be used for all subsequent WordPress installations until an updated version is available.', $state='U', $callbackOnClose='', $callbackReference='');
			updateOption('lastWPInstallPackageDownloaded', $WPInstallPackage['version']);
		}
		else{
			addNotification($type='E', $title='WORDPRESS '.$WPInstallPackage['display_version'].' DOWNLOAD FAILED', $message='The latest WordPress '.$WPInstallPackage['display_version'].' package download failed. <br>Make sure '.APP_ROOT.'/addons/installClone/bridge/ is writable.', $state='U', $callbackOnClose='', $callbackReference='');	
		}
	}
	return $isDone;
}


function installCloneUpdateCheck($result){
	
	if(!empty($result['WPInstallPackage'])){
		updateOption('WPInstallPackage', serialize($result['WPInstallPackage']));
	}
}

function installCloneSaveProfile($params){
	if(!empty($params)){
		if(!($params['installCloneProfileID'])){
			$save = DB::insert("?:install_clone_repository", array("profileName" => $params['profileName'], "accountInfo" => serialize($params['accountInfo'])));
			if($save){
				return true;
			}
		}
		else
		{
			$query = DB::update("?:install_clone_repository", array("profileName" => $params['profileName'], "accountInfo" => serialize($params['accountInfo'])), "installCloneProfileID='".$params['installCloneProfileID']."'");
			if(!empty($query)){
				return $query;
			}
			else{
				return "Profile already exists.";
			}
		}
	}
}

function installCloneDeleteProfile($params){
	return DB::delete("?:install_clone_repository", "installCloneProfileID='".$params['installCloneProfileID']."'");
}

function installCloneGetAllProfiles(){
	$get = DB::getArray("?:install_clone_repository", "profileName, installCloneProfileID", "1", "installCloneProfileID");
	return $get;
}
function installCloneGetProfile($params){
	if(!empty($params)){
		$get = DB::getRow("?:install_clone_repository", "*", "installCloneProfileID = '".$params['profileID']."'");
		$get['accountInfo'] = unserialize($get['accountInfo']);
	}
	
	return $get;
}
if(!function_exists('unserializeBase64DecodeArray')){

	function unserializeBase64DecodeArray($strBetArray){
		if(empty($strBetArray) || !is_array($strBetArray)){ return false; }
		$newArray = array();
		foreach($strBetArray as $key => $value){
			$newArray[$key] = unserialize(base64_decode($value));
		}
		return $newArray;
	}

}

?>