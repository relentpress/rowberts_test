<?php
$GLOBALS['IWP_MMB_PROFILING']['ACTION_START'] = microtime(1);
register_shutdown_function("bridge_shutdown");

error_reporting(E_ALL ^ E_NOTICE);
@ini_set("display_errors", 1);
@ignore_user_abort(true);

require_once 'fileSystem.php';

$HTTP_RAW_POST_DATA = file_get_contents('php://input');

$http_data = unserialize(base64_decode($HTTP_RAW_POST_DATA));

$_REQUEST = $http_data;

define('DB_HOST',$_REQUEST['dbHost']);
define('DB_USER',$_REQUEST['dbUser']);
define('DB_PASSWORD',$_REQUEST['dbPassword']);
define('DB_NAME',$_REQUEST['dbName']);


define('APP_FTP_HOST',$_REQUEST['ftpHost']);
define('APP_FTP_PORT',$_REQUEST['ftpPort']);
define('APP_FTP_USER',$_REQUEST['ftpUser']);
define('APP_FTP_PASS',$_REQUEST['ftpPass']);
define('APP_FTP_BASE',$_REQUEST['ftpBase']);
define('APP_FTP_SSL',$_REQUEST['ftpSSL']);
define('APP_FTP_USE_SFTP',$_REQUEST['ftpUseSftp']);

$GLOBALS['downloadPossibleError'] = '';


//$old = $_REQUEST['oldSite'];
$old_url = removeTrailingSlash($_REQUEST['oldSite']);
$old_user = $_REQUEST['oldUser'];
$newUser = $_REQUEST['newUser'];
$newPassword = $_REQUEST['newPassword'];
$new_url = removeTrailingSlash($_REQUEST['newSiteURL']);

$new_file_path = dirname(dirname(__FILE__));

$db_table_prefix = (isset($_REQUEST['db_table_prefix'])) ? $_REQUEST['db_table_prefix'] : false;

// For unzipping
require_once 'class-pclzip.php';

//Check access
@ini_set('memory_limit', "-1"); // For big uploads
@ini_set("max_execution_time", 0);
@set_time_limit(0);

// Check MySQL
$check=mysql_connect(DB_HOST,DB_USER,DB_PASSWORD) or die (status("Error establishing DB: (".mysql_errno().") ".mysql_error(), $success=false, $return=true));
status("DB Connected", $success=true, $return=false);
@mysql_close($check);



//$bkfile='temp.zip';
if($_REQUEST['backupURL'] == "localPackage"){
	$bkfile = "WPPackage.zip";
	
	if(!file_exists($bkfile)) {
   		die(status("Couldn't Find the backup file.", false ,true));
	}
	
	if(file_exists($bkfile)){
		status("Backup File Exist.", true, false);
	}

}
else{
	$this_backup_URL = get_files_array_from_iwp_part($_REQUEST['backupURL']);
	
	if(!is_array($this_backup_URL))
	{
		$this_temp_backup_URL = $this_backup_URL;
		$this_backup_URL = array();
		$this_backup_URL[] = $this_temp_backup_URL;
	}
	$bkfile = array();
	foreach($this_backup_URL as $key => $single_backup_URL)
	{
		iwp_mmb_auto_print('download');
		$bkfile[] = downloadURL($single_backup_URL, 'WPPackage.zip');
		if (!file_exists($bkfile[$key])) {
			 die(status("Couldn't Download the backup file.", false ,true));
		}
		if(file_exists($bkfile[$key])){
			status("Backup File Downloaded", true, false);
		}
	}
}
iwp_mmb_auto_print('download');

transfer($bkfile, $new_url, $newUser, $newPassword, $old_user, $db_table_prefix);

function transfer($backup_file, $new_url, $newUser, $newPassword, $old_user, $db_table_prefix)
{
	// ***************************************** Extract the ZIP  Starts {*************************
	$temp_unzip_dir = getTempDir();
	$temp_unzip_dir = removeTrailingSlash($temp_unzip_dir);
	
	//if(removeTrailingSlash($temp_unzip_dir) !== removeTrailingSlash(dirname(dirname(__FILE__)))){
		$temp_uniq = md5(microtime(1));
		while (is_dir($temp_unzip_dir .'/'. $temp_uniq )) {
			$temp_uniq = md5(microtime(1));
		}
		$temp_unzip_dir = $temp_unzip_dir .'/'. $temp_uniq;
		mkdir($temp_unzip_dir);
	//}

	status("Using temp working dir:".$temp_unzip_dir, $success=true, $return=false);
	
	if(!is_array($backup_file))
	{
		$temp_backup_file = $backup_file;
		$backup_file = array();
		$backup_file[] = $temp_backup_file;
		
	}
	@ini_set('memory_limit', '-1');
	foreach($backup_file as $key => $single_backup_file)
	{
		iwp_mmb_auto_print('unzipping');
		$unzip = cmdExec('which unzip', true);
		if (!$unzip) $unzip = "unzip";   
		$command = "$unzip -d $temp_unzip_dir -o $single_backup_file";
		$result = cmdExec($command);

		if (!$result) {
			$archive   = new IWPPclZip($single_backup_file);
			$extracted = $archive->extract(PCLZIP_OPT_PATH, $temp_unzip_dir, PCLZIP_OPT_TEMP_FILE_THRESHOLD, 1);
			if (!$extracted || $archive->error_code) {
				die(status('Error: Failed to extract backup file (' . $archive->error_string . ').'.$GLOBALS['downloadPossibleError'], $success=false, $return=true));
			}
		}
		else{
			status('Native zip is used to unzip.', $success=true, $return=false);
		}
	}
	appendSplitFiles($temp_unzip_dir);
	
	if(file_exists($temp_unzip_dir.'/iwp_db/index.php')){
		include	$temp_unzip_dir.'/iwp_db/index.php';//this will overwrite few global variables $old_url and $old_file_path
		global $old_url, $old_file_path;
		if(isset($old_file_path)){
			$old_file_path = removeTrailingSlash($old_file_path);
		}
		if(isset($old_url)){
			$old_url = removeTrailingSlash($old_url);
		}
	}
	
	// ***************************************** }Extract the ZIP  Ends ****************************
	
	// ***************************************** Replace DB Starts{*********************************
	global $old_url, $new_url, $old_table_prefix, $table_prefix;
	$old_table_prefix = trim(get_table_prefix($temp_unzip_dir));
	//$new_table_prefix = trim(get_table_prefix());
	if ($db_table_prefix && $old_table_prefix != $db_table_prefix) {
		$has_new_prefix = true;
		$table_prefix = $db_table_prefix;
	} else {
		$has_new_prefix = false;
		$table_prefix = $old_table_prefix;
	}
	
	$changed_prefix_config = change_table_prefix_config_file($temp_unzip_dir, $table_prefix);
	if ($changed_prefix_config) {
		status("Table prefix changed in Config file.", $success=true, $return=false);
	} else {
	    die(status("Error: Couldn't change wp-config.php file.", $success=false, $return=true));
	}
	
	$sqlConnect = mysql_connect(DB_HOST,DB_USER,DB_PASSWORD) or die (status('Error Connecting to the MySQL Server', $success=false, $return=true));
	
	@chmod($temp_unzip_dir.'/iwp_db',0755);
	
    @mysql_select_db(DB_NAME) or die(status('Error selecting MySQL database,: ' . mysql_error(), $success=false, $return=true));
	
	$paths     = check_mysql_paths();
	$db_file_path = $temp_unzip_dir.'/iwp_db';
	$file_name = glob($db_file_path . '/*.sql');
	
	
	/*-----Replace URL--------*/
	$db_file = $file_name[0];
    $db_file = $db_file_path  . "/" . basename($db_file);
		
    if(modify_db_dump($db_file, $has_new_prefix)) {
		status("Database dump modified url and prefix.", $success=true, $return=false);
    } else {
		status("Error: Database dump cannot be modified.", $success=false, $return=true);
    }
	/*-----Replace URL-ends--------*/

	$brace     = (substr(PHP_OS, 0, 3) == 'WIN') ? '"' : '';
	$command   = $brace . $paths['mysql'] . $brace . ' --host="' . DB_HOST . '" --user="' . DB_USER . '" --password="' . DB_PASSWORD . '" --default-character-set="utf8" ' . DB_NAME . ' < ' . $brace . $db_file . $brace;
	
	$result = cmdExec($command);	
	
        iwp_mmb_auto_print('sql_import');
        
	
	if ($result){
		status("Database dump executed using command.", $success=true, $return=false);
	}
	else{
		mysql_query("SET NAMES 'utf8'");
        //Else PHP db dump
        // $filename      = glob($file_path . '/*.sql');
        // $db_file       = $filename[0];
        // // Temporary variable, used to store current query
        // $current_query = '';
        // Read in entire file
        $lines         = file($db_file);
        // Loop through each line
        if (count($lines) && !empty($lines)) {
            foreach ($lines as $line) {
                iwp_mmb_auto_print('php_sql_import');
                // Skip it if it's a comment
                if (substr($line, 0, 2) == '--' || $line == '')
                    continue;
                $current_query .= $line;
                // If it has a semicolon at the end, it's the end of the query
                if (substr(trim($line), -1, 1) == ';') {
                    // Perform the query
                    $result = mysql_query($current_query);
                    if (!$result) {
						//------------Due to big query, error msg is not getting saved in IWP Panel DB due to max packet length and other issues-- this is a fix for it------
						$temp_error_replace_text = '...[Big text removed for error]...';
						$max_error_query_length = 1500 + strlen($temp_error_replace_text);
						$temp_current_query = $current_query;
						if(strlen($current_query) > $max_error_query_length){
							$temp_current_query = substr_replace($temp_current_query, '...[Big text removed for error]...', 750, -750);
						}
						$temp_current_query = htmlentities($temp_current_query);
						//------------Due to big query, error msg is not getting saved in IWP Panel DB due to max packet length and other issues-- this is a fix for it------
                        $db_error = 'Error performing query "<strong>' . $temp_current_query . '</strong>": ' . mysql_error();
                        break;
                    }
                    // Reset temp variable to empty
                    $current_query = '';
                }
            }
        } else {
            $db_error = 'Cannot open database file.';
        }
        
        if ($db_error) {
			die(status("Failed to restore: "  . $db_error, $success=false, $return=true));
           //die(status('Error: ' . $db_error, $success=false, $return=true));
        }
    	status("Database dump executed using PHP.", $success=true, $return=false);
	}
	
	if ($has_new_prefix) {
    	$query = "
    		UPDATE {$table_prefix}options
    		SET option_name = '{$table_prefix}user_roles'
    		WHERE option_name = '{$old_table_prefix}user_roles'
    		LIMIT 1";
			
    	mysql_query($query) or die(status('Error replacing options values - ' . mysql_error(), $success=false, $return=true));
    	$query = "
	    	UPDATE {$table_prefix}usermeta
	    	SET meta_key = CONCAT('{$table_prefix}', SUBSTR(meta_key, CHAR_LENGTH('{$old_table_prefix}') + 1))
	    	WHERE meta_key LIKE '{$old_table_prefix}%'";
    	mysql_query($query) or die(status('Error replacing usermeta values - ' . mysql_error(), $success=false, $return=true));
    }
	
	
	status("DB restored", $success=true, $return=false);
	
	@unlink($db_file);
	@unlink(dirname($db_file).'/index.php');
	@clearstatcache();
	@rmdir(dirname($db_file));
	
	//}***************************************** Replace DB Ends*********************************
	
	//*********************************** Write the Config File Starts { *************************
	
	$lines = @file($temp_unzip_dir.'/wp-config.php');
    if(empty($lines)){
		$lines = @file($temp_unzip_dir.'/wp-config-sample.php');
	}
	@unlink($temp_unzip_dir.'/wp-config.php'); // Unlink if a config already exists
	if(empty($lines)){ die(status("Please replace wp-config.php. It seems missing", $success=false, $return=true)); }
	foreach ($lines as $line) {
		if (strstr($line, 'DB_NAME')){
			$line = "define('DB_NAME', '".DB_NAME."');\n";
		}
		if (strstr($line, 'DB_USER')){
			$line = "define('DB_USER', '".DB_USER."');\n";
		}
		if (strstr($line, 'DB_PASSWORD')){
			$line = "define('DB_PASSWORD', '".DB_PASSWORD."');\n";
		}
		if (strstr($line, 'DB_HOST')){
			$line = "define('DB_HOST', '".DB_HOST."');\n";
		}
		if (strstr($line, 'WP_HOME') || strstr($line, 'WP_SITEURL')){
			$line = "";
		}
		if(!file_put_contents($temp_unzip_dir.'/wp-config.php', $line, FILE_APPEND))
			die(status("Permission denied to write the config file.", $success=false, $return=true));
	}
			
	// }*********************************** Write the Config File Ends **********************************
			
	replace_htaccess($new_url, $temp_unzip_dir);
			
	if (!$old_url) {//$old_url - old site url
		$query =  "SELECT option_value FROM " . $table_prefix . "options  WHERE option_name = 'siteurl' LIMIT 1";
		$result = mysql_query($query) or die(status('Error getting old site URL' . mysql_error(), $success=false, $return=true));
		$info = mysql_fetch_assoc($result);
		$old_url = removeTrailingSlash($info['option_value']);
	}
				
	// Update the Home / Site URL
	$query = "UPDATE " . $table_prefix . "options SET option_value = '".$new_url."' WHERE option_name = 'home'";
	mysql_query($query) or die(status("Error updating the home URL", $success=false, $return=true));
	status("Home URL updated", $success=true, $return=false);
	
	$query = "UPDATE " . $table_prefix . "options  SET option_value = '".$new_url."' WHERE option_name = 'siteurl'";
	mysql_query($query) or die(status("Error updating the site URL", $success=false, $return=true));
	status("Site URL updated", $success=true, $return=false);
	
	//Set the new admin password  
	if ($newUser && $newPassword && $old_user) {//$newPassword -> md5ed password
		$query = "UPDATE " . $table_prefix . "users SET user_login = '".$newUser."', user_pass = '".$newPassword."' WHERE user_login = '".$old_user."'";
		mysql_query($query) or die(status("Error setting up credentials.", $success=false, $return=true));
		status("Credentials updated.", $success=true, $return=false);
	}


	//Send the variables of new_user, new_password and old_user

	//Replace the post contents
	$query = "UPDATE " . $table_prefix . "posts SET post_content = REPLACE (post_content, '$old_url','$new_url') WHERE post_content REGEXP 'src=\"(.*)$old_url(.*)\"' OR post_content REGEXP 'href=\"(.*)$old_url(.*)\"'";
	mysql_query($query) or die(status("Error updating the post content", $success=false, $return=true));
	status("Post content updated.", $success=true, $return=false);
	 
	 
	 //$table = $GLOBALS['table_prefix'].'iwp_backup_status';
	 
	 //mysql_num_rows(mysql_query("SHOW TABLES LIKE ".$table_prefix."iwp_backup_status"))
	 $queryTestBS = mysql_query("SHOW TABLES LIKE '".$table_prefix."iwp_backup_status'");
		if(!$queryTestBS) echo mysql_error();
	 $queryTestBSRows = mysql_num_rows($queryTestBS);
		
	 if($queryTestBSRows){
		$delete = mysql_query("TRUNCATE TABLE ".$table_prefix."iwp_backup_status ")or die(status('Failed to clear old IWP backup status table.' . mysql_error(), $success=false, $return=true));
		status("IWP backup status table cleared", $success=true, $return=false);
	 }
	 
	 
	//clearing iwp-client plugin iwp_client_public_key, iwp_client_action_message_id, iwp_client_nossl_key
	$query = "DELETE FROM " . $table_prefix . "options WHERE option_name = 'iwp_client_public_key' OR option_name = 'iwp_client_action_message_id' OR option_name = 'iwp_client_nossl_key'";
	mysql_query($query) or die(status('Failed to clear old IWP Client Plugin details.' . mysql_error(), $success=false, $return=true));
	status("Cleared old IWP Client Plugin details.", $success=true, $return=false);
	
	//Remove the iwp-client plugin old data // Need to change these
	$query = "DELETE FROM " . $table_prefix . "options WHERE option_name IN ('iwp_backup_tasks', 'iwp_notifications', 'iwp_client_brand', 'user_hit_count', 'iwp_pageview_alerts')";
	mysql_query($query) or die(status('Error deleting client settings' . mysql_error(), $success=false, $return=true));
	status("IWP settings Deleted", $success=true, $return=false);  
		   
    if (!empty($_REQUEST['toIWP'])) {
		$iwp_client_activation_key = sha1( rand(1, 99999). uniqid('', true) .$new_url);
		$query = "REPLACE INTO " . $table_prefix . "options(option_name, option_value) VALUES('iwp_client_activate_key', '$iwp_client_activation_key')";
		mysql_query($query) or die(status("Failed to create Activation Key", $success=false, $return=true));
		status("Activation Key Created", $success=true, $return=false);
    }
	else{
		//deactivate iwp-client plugin
		$query = "SELECT option_value FROM " . $table_prefix . "options WHERE option_name='active_plugins'";
		$result = mysql_query($query) or die(status("Failed to get active plugins", $success=false, $return=true));
		$row = mysql_fetch_array($result);
		$active_plugins = @unserialize($row['option_value']);
		$key = array_search('iwp-client/init.php', $active_plugins);
		if($key !== false && $key !== NULL){
			unset($active_plugins[$key]);
		}
		$active_plugins = @serialize($active_plugins);
		$query = "UPDATE " . $table_prefix . "options SET option_value = '$active_plugins' WHERE option_name='active_plugins'";
		$result = mysql_query($query) or die(status("Failed to deactivate client plugin", $success=false, $return=true));
	}
		   
	$admin_email = trim($_REQUEST['admin_email']);
    if(trim($old_user) == ''){
    	if ($admin_email) {
			//Clean Install
			$query = "UPDATE " . $table_prefix . "options SET option_value = '$admin_email' WHERE option_name = 'admin_email'";
			mysql_query($query) or die(status('Error setting admin email - ' . mysql_error(), $success=false, $return=true));
			status("Admin Email created", $success=true, $return=false);
			
			$query = "SELECT * FROM " . $table_prefix ."users LIMIT 1";
			$temp_user_result = mysql_query($query) or die(status('Error: user to replace not found - ' . mysql_error(), $success=false, $return=true));
			
			if($temp_user = mysql_fetch_assoc($temp_user_result)){
				$query        = "UPDATE " . $table_prefix . "users SET user_email='$admin_email', user_login = '$newUser', user_pass = '$newPassword' WHERE user_login = '$temp_user[user_login]'";
				mysql_query($query) or die(status('Error setting new user - ' . mysql_error(), $success=false, $return=true));
				status("New User Created", $success=true, $return=false);
			}
		} else {
    		//Clone from url
    		if($newUser && $newPassword){
				$query = "UPDATE " . $table_prefix . "users SET user_pass = '$newPassword' WHERE user_login = '$newUser'";
				mysql_query($query) or die(status('Error setting new password - ' . mysql_error(), $success=false, $return=true));
				status("New Password Created", $success=true, $return=false);
			}
    	}
    }
	
	//Reset media upload settings
    $query = "UPDATE " . $table_prefix . "options SET option_value = '' WHERE option_name = 'upload_path' OR option_name = 'upload_url_path'";
    mysql_query($query) or die(status('Error setting media upload settings - ' . mysql_error(), $success=false, $return=true));
	
	@mysql_close($sqlConnect);
	status("DB Modifications done", $success=true, $return=false);
	
	/*if(removeTrailingSlash($temp_unzip_dir) === removeTrailingSlash(dirname(dirname(__FILE__)))){// this can create files in some other ownership
		//no need to do any copy, files are already required destination
		status("No need to to copy files and folders.", $success=true, $return=false);
	}
	else{*/
		//copy to required destination
		initFileSystem(false, dirname(dirname(__FILE__)));
		$FSCopyResult = directToAnyFSCopyDir($temp_unzip_dir, dirname(dirname(__FILE__)));
		if($FSCopyResult !== true){
			die(status("Error in file system copy.", $success=false, $return=true));
		}
		
		$directFSObj = new filesystemDirect('');
		$directFSObj->delete(dirname(__FILE__), true);//dirname(__FILE__) => clone_controller folder
		
		
		$GLOBALS['FileSystemObj']->delete(removeTrailingSlash(APP_FTP_BASE).'/clone_controller', true);//for those files and folders not delete with direct file system
		status("Copy through file system done.", $success=true, $return=false);
	//}
		 			
	echo "<h1>Clone Completed</h1>";
	status("clone_completed", $success=true, $return=false);//changing success or status will affect result processing in addon controller
	
	if (!empty($_REQUEST['toIWP'])) {
		status("Datas", $success=true, $return=false, array('URL' => $new_url, 'userName' => $newUser, 'activationKey' => $iwp_client_activation_key));
	}
	
	
	
	//@unlink('class-pclzip.php');	
	//@unlink('bridge.php');
	//@unlink('fileSystem.php');
	//@unlink($backup_file);
	
	//if(file_exists('error_log')) @unlink('error_log');
	//@clearstatcache();
	//@rmdir('../clone_controller');
		
	return true;
}

function get_files_array_from_iwp_part($backup_file)
{
	$backup_files_array = array();
	if(strpos($backup_file, '_iwp_part') !== false)
	{
		$orgName = substr($backup_file, 0, strpos($backup_file, '_iwp_part_'));
		$totalParts = substr($backup_file, strpos($backup_file, '_iwp_part_')+10);
		$totalParts = substr($totalParts, 0, strlen($totalParts)-4);
		for($i=0; $i<=$totalParts; $i++)
		{
			iwp_mmb_auto_print('get_files_array_from_iwp_part');
			if($i == 0)
			{
				$backup_files_array[] = $orgName.'.zip';
			}
			else
			{
				$backup_files_array[] = $orgName.'_iwp_part_'.$i.'.zip';
			}
		}
		return $backup_files_array;
	}
	else
	{
		$backup_files_array[] = $backup_file;
		return $backup_file;
	}
}

function appendSplitFiles($fileToAppend)
{
	// function to join the split files during multicall backup
	$directory_tree = get_all_files_from_dir($fileToAppend);
	usort($directory_tree, "sortString");
	$joinedFilesArray = array();
	$orgHashValues = array();
	$hashValue = '';
		
	foreach($directory_tree as $k => $v)
	{
		$contents = '';
			$orgFileCount = 0;
		/* $subject = $v;
		$pattern = '/iwp_part/i';
		preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE);
		print_r($matches); */
		$pos = strpos($v, 'iwp_part');
		if($pos !== false)
		{
			$currentFile = explode(".",$v);
				$currentFileSize = count($currentFile);
			 foreach($currentFile as $key => $val)
				{
					iwp_mmb_auto_print('appendSplitFiles');
					if(($key == ($currentFileSize-2))||($currentFileSize == 1))
					{
						$insPos = strpos($val, '_iwp_part');
						$rest = substr_replace($val, '', $insPos);
						$currentFile[$key] = $rest;
						
						$insPos2 = strpos($rest, '_iwp_hash');
						if($insPos2 != false)
						{
							$hashValue = substr($rest, -32);
							$rest = substr_replace($rest, '', $insPos2);
							$currentFile[$key] = $rest;
					}
				}
				}
				$orgFileCount++;	
			$orgFileName = implode(".", $currentFile);
			$handle = fopen($v,"r");
			$contents = fread($handle, filesize($v));
			fclose($handle);
				if($orgFileCount == 1)
				{
					//clearing contents of file intially to prevent appending to already existing file
				}
			file_put_contents($orgFileName,$contents,FILE_APPEND);
			$joinedFilesArray[$orgFileName] = 'hash';
			$orgHashValues[$orgFileName] = $hashValue;
			echo " orgFileName - ".$orgFileName;
			$file_to_ulink = realpath($v);
			$resultUnlink = unlink($file_to_ulink);
			$resultUnlink = error_get_last();
			if(!$resultUnlink)
			{
				if(is_file($v))
				{
					unlink($file_to_ulink);
				}
			}
		}
	}
	$hashValues = array();
	foreach($joinedFilesArray as $key => $value)
	{
		$hashValues[$key] = md5_file($key);
	}
		$totalHashValues = array();
		$totalHashValues['orgHash'] = $orgHashValues;
		$totalHashValues['afterSplitHash'] = $hashValues;
		return $totalHashValues;
}

function sortString($a, $b)
{
	// the uSort CallBack Function used in the appendSplitFiles function
	$stringArr = array();
	$stringArr[0] = $a;
	$stringArr[1] = $b;
	$strA = '';
	$strB = '';
	foreach($stringArr as $strKey => $strVal)
	{
		$mystring = $strVal;
		$findme = '_iwp_part';																		//fileNameSplit logic
		$pos = strpos($mystring, $findme);
		$rest = substr($mystring, $pos);
		$pos2 = strrpos($rest, $findme);
		$len = strlen($rest);
		$actLen = $pos2+strlen($findme);
		$actPos = $len - $actLen -1;
		$actPartNum = substr($rest, -($actPos));
			$actPartNumArray = explode(".",$actPartNum);
			foreach($actPartNumArray as $key => $val)
			{
				if($key == 0)
				$actPartNum = $val;
			}
		if($strKey == 0)
		$strA = intval($actPartNum);
		else
		$strB = intval($actPartNum);
	}
	if ($strA == $strB){return 0;}
	return ($strA < $strB) ? -1 : 1;	
}

function get_all_files_from_dir($path, $exclude = array()) 
{
	if ($path[strlen($path) - 1] === "/") $path = substr($path, 0, -1);
	global $directory_tree, $ignore_array;
	$directory_tree = array();
	foreach ($exclude as $file) {
		if (!in_array($file, array('.', '..'))) {
			if ($file[0] === "/") $path = substr($file, 1);
			$ignore_array[] = "$path/$file";
		}
	}
	get_all_files_from_dir_recursive($path);
	return $directory_tree;
}

function get_all_files_from_dir_recursive($path, $ignore_array=array()) {
	if ($path[strlen($path) - 1] === "/") $path = substr($path, 0, -1);
	global $directory_tree, $ignore_array;
	$directory_tree_temp = array();
	$dh = @opendir($path);
	if(empty($ignore_array))
	{
		$ignore_array = array();
	}
	while (false !== ($file = @readdir($dh))) {
		if (!in_array($file, array('.', '..'))) {
			if (!in_array("$path/$file", $ignore_array)) {
				if (!is_dir("$path/$file")) {
					$directory_tree[] = "$path/$file";
				} else {
                                        iwp_mmb_auto_print('appendSplitFiles');
					get_all_files_from_dir_recursive("$path/$file");
				}
			}
		}
	}
	@closedir($dh);
}

function change_table_prefix_config_file($file_path, $db_table_prefix) {
	//select wp-config-sample.php
	$wp_config_file = glob($file_path . '/wp-config.php');
	if (@rename($wp_config_file[0], $file_path.'/wp-config-temp.php')) {
		$lines = file($file_path.'/wp-config-temp.php');
		@unlink($file_path.'/wp-config-temp.php');
	} else {
		$lines = @file($file_path.'/wp-config-sample.php');
	}

	@unlink($file_path.'/wp-config.php');

	if (empty($lines))
		die(status('Error: Cannot recreate wp-config.php file.', $success=false, $return=true));

	$file_success = false;

	foreach ($lines as $line) {
		if ($db_table_prefix && strstr($line, '$table_prefix')) {
			$line         = "\$table_prefix = '$db_table_prefix';\n";
			$file_success = true;
		}

		if (file_put_contents($file_path.'/wp-config.php', $line, FILE_APPEND) === FALSE)
			die(status('Error: Cannot write wp-config.php file.', $success=false, $return=true));
	}
	return $file_success;
}


// function restore_db_php($file_name){
	// $current_query = '';
	// // Read in entire file
	// $lines = file($file_name);
		// // Loop through each line
	// foreach ($lines as $line) {
		// // Skip it if it's a comment
		// if (substr($line, 0, 2) == '--' || $line == '')
			// continue;
		
		// // Add this line to the current query
		// $current_query .= $line;
		// // If it has a semicolon at the end, it's the end of the query
		// if (substr(trim($line), -1, 1) == ';') {
			// // Perform the query
			// $result = mysql_query($current_query);
			// if ($result === false)
				// return false;
			// // Reset temp variable to empty
			// $current_query = '';
		// }
	// }
	
	// @unlink($file_name);
	// return true;
// }

function cmdExec()
{
 if ($command == '')
        return false;
    
    if (checkFunctionExists('exec')) {
        $log = @exec($command, $output, $return);
        
        if ($string)
            return $log;
        return $return ? false : true;
    } elseif (checkFunctionExists('system')) {
        $log = @system($command, $return);
        
        if ($string)
            return $log;
        return $return ? false : true;
    } elseif (checkFunctionExists('passthru') && !$string) {
        $log = passthru($command, $return);
        
        return $return ? false : true;
    } else {
        return false;
    }
}
function checkFunctionExists($function_callback){
	
	if(!function_exists($function_callback))
		return false;
		
	$disabled = explode(', ', @ini_get('disable_functions'));
	if (in_array($function_callback, $disabled))
		return false;
		
	if (extension_loaded('suhosin')) {
		$suhosin = @ini_get("suhosin.executor.func.blacklist");
		if (empty($suhosin) == false) {
			$suhosin = explode(',', $suhosin);
			$blacklist = array_map('trim', $suhosin);
			$blacklist = array_map('strtolower', $blacklist);
			if(in_array($function_callback, $blacklist))
				return false;
		}
	}
	return true;
}

function get_table_prefix($temp_unzip_dir)
{
	$lines = file($temp_unzip_dir.'/wp-config.php');
	foreach ($lines as $line) {
		if (strstr($line, '$table_prefix')) {
			$pattern = "/(\'|\")[^(\'|\")]*/";
			preg_match($pattern, $line, $matches);
			$prefix = substr($matches[0], 1);
			return $prefix;
			break;
		}
	}
	return 'wp_'; //default
}

if (!function_exists('file_put_contents')) {
function file_put_contents($filename, $data) {
	$f = @fopen($filename, 'w');
	if (!$f) {
	  die(status("Error - Fopen needs to be enabled in your server", $success=false, $return=true));
	} else {
		$bytes = fwrite($f, $data);
		fclose($f);
		return $bytes;
	}
}
}

function check_mysql_paths()
{
	 $paths = array(
		'mysql' => '',
		'mysqldump' => ''
	);
	if (substr(PHP_OS, 0, 3) == 'WIN') {
		$mysql_install =mysql_query("SHOW VARIABLES LIKE 'basedir'");
		if ($mysql_install) {
			$install_path       = str_replace('\\', '/', $mysql_install->Value);
			$paths['mysql']     = $install_path . 'bin/mysql.exe';
			$paths['mysqldump'] = $install_path . 'bin/mysqldump.exe';
		} else {
			$paths['mysql']     = 'mysql.exe';
			$paths['mysqldump'] = 'mysqldump.exe';
		}
	} else {
		$paths['mysql'] = cmdExec('which mysql', true);
		if (empty($paths['mysql']))
			$paths['mysql'] = 'mysql'; // try anyway
		
		$paths['mysqldump'] = cmdExec('which mysqldump', true);
		if (empty($paths['mysqldump']))
			$paths['mysqldump'] = 'mysqldump'; // try anyway         
		
	}
	
	return $paths;
}

 function check_sys()
{
	if ($this->mmb_function_exists('exec'))
		return 'exec';
	
	if ($this->mmb_function_exists('system'))
		return 'system';
	
	if ($this->mmb_function_exists('passhtru'))
		return 'passthru';
	
	return false;
	
}

function replace_htaccess($url, $temp_unzip_dir){
	$file = @file_get_contents($temp_unzip_dir.'/.htaccess');
    if ($file && strlen($file)) {
        $args    = parse_url($url);        
        $string  = rtrim($args['path'], "/");
        $regex   = "/BEGIN WordPress(.*?)RewriteBase(.*?)\n(.*?)RewriteRule \.(.*?)index\.php(.*?)END WordPress/sm";
        $replace = "BEGIN WordPress$1RewriteBase " . $string . "/ \n$3RewriteRule . " . $string . "/index.php$5END WordPress";
        $file    = preg_replace($regex, $replace, $file);
        @file_put_contents($temp_unzip_dir.'/.htaccess', $file);
    }
}

function modify_db_dump($db_file, $has_new_prefix) {
	$lines = file($db_file);
	@unlink($db_file);
	// Loop through each line
	
	if (count($lines) && !empty($lines)) {
		foreach ($lines as $i => $line) {
                        iwp_mmb_auto_print('modify_db_dump');
			// Skip it if it's a comment
			if (substr($line, 0, 2) == '--' || $line == '')
				continue;
			
			
			
			$line = preg_replace_callback('/\\\'(.*?[^\\\\])\\\'[\\,\\)]/', 'search_and_replace_url', $line);//old - /\'([^\']+)\'[\,\)]/ new - /\'(.*?[^\\])\'[\,\)]/
			if ($has_new_prefix) {
				$line = preg_replace_callback("/(TABLE[S]?|INSERT\ INTO|DROP\ TABLE\ IF\ EXISTS) [`]?([^`\;\ ]+)[`]?/", 'search_and_replace_prefix', $line);				
			}
			// Add this line to the current segment
			if (file_put_contents($db_file, $line, FILE_APPEND) === FALSE)
            	die(status('Error: Cannot write wp-config.php file.', $success=false, $return=true));
		}
		
		return true;
	} else {
		return false;
	}
}

function is_serialized( $data ) {
	
	// if it isn't a string, it isn't serialized
	if ( ! is_string( $data ) )
		return false;
	$data = trim( $data );
	if ( 'N;' == $data )
		return true;
	$length = strlen( $data );
	if ( $length < 4 )
		return false;
	if ( ':' !== $data[1] )
		return false;
	$lastc = $data[$length-1];
	if ( ';' !== $lastc && '}' !== $lastc )
		return false;
	$token = $data[0];
	switch ( $token ) {
		case 's' :
			if ( '"' !== $data[$length-2] )
				return false;
		case 'a' :
		case 'O' :
			return (bool) preg_match( "/^{$token}:[0-9]+:/s", $data );
		case 'b' :
		case 'i' :
		case 'd' :
			return (bool) preg_match( "/^{$token}:[0-9.E-]+;\$/", $data );
	}
	return false;
}


function apply_replaces($subject, $is_serialized = false) {
	global $old_url, $new_url, $old_file_path, $new_file_path;
	
	$search = array();
	$replace = array();
	
	//all these values with untrailed slashes will be good
	
	$search[0] = $old_url;
	$replace[0] = $new_url;
	
	if(!empty($old_file_path)){
		$search[1] = $old_file_path;
		$replace[1] = $new_file_path;
	}

	return str_replace($search, $replace, $subject);
}

function replace_array_values(&$value, $key) {
	if (!is_string($value)) return;
	$value = apply_replaces($value, true);
}

function search_and_replace_url($matches) {

	global $old_url, $old_file_path;
	$replace = $search = $matches[1];
	$subject = $matches[0];
	
	if (($old_url && strpos($replace, $old_url) !== false) || ($old_file_path && strpos($replace, $old_file_path) !== false)) {//URL and file path IWP improvement
		if (is_serialized(stripcslashes($replace)) && false !== ($data = @unserialize(stripcslashes($replace)))) {
			if ( is_array( $data ) ) {
				array_walk_recursive($data, 'replace_array_values');
			} elseif (is_string($data)) {
				$data = apply_replaces($data, true);
			}
			$replace = addslashes(serialize($data));
			$replace = str_replace(array("\r\n", "\n"), '\r\n', $replace);
		} else {
			$replace = apply_replaces($replace);
		}
	}
	return str_replace($search, $replace, $subject);
}

function search_and_replace_prefix($matches) {
	global $old_table_prefix, $table_prefix;
	$subject = $matches[0];
	$old_table_name = $matches[2];

	//$new_table_name = str_replace($old_table_prefix, $table_prefix, $old_table_name);
	
	$new_table_name = preg_replace('/'.$old_table_prefix.'/',  $table_prefix, $old_table_name, 1);
	
	return str_replace($old_table_name, $new_table_name, $subject);
}

function status($status, $success=true, $return=true, $options=''){
	
	if($success && !empty($options)){  echo '#Status('.base64_encode(serialize(array('success' => $status, 'options' => $options))).')#'; echo "\n".serialize(array('success' => $status, 'options' => $options)); }
	
	if($success){ echo '#Status('.base64_encode(serialize(array('success' => $status))).')#';  echo "\n".serialize(array('success' => $status)); }
	
	if($return){ echo '#Status('.base64_encode(serialize(array('error' => $status))).')#';  echo "\n".serialize(array('error' => $status)); }
	
	ob_flush(); flush();
	
        $GLOBALS['IWP_MMB_PROFILING']['LAST_PRINT'] = $current_time;
	
}

function bridge_shutdown(){
	$isError = false;

    if ($error = error_get_last()){
    switch($error['type']){
		/*case E_PARSE:*/
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
            $isError = true;
            break;
        }
    }

    if ($isError){
		$status = 'PHP Fatal error occured: '.$error['message'].' in '.$error['file'].' on line '.$error['line'].'.';
		status($status, $success=false, $return=true);
		
    }
}

function appUpdateMsg($msg, $isError=0){

	if($isError){
		die(status($msg, $success=false, $return=true, $options=''));
	}
	else{
		status($msg, $success=true, $return=false, $options='');
	}
}


function iwp_mmb_auto_print($unique_task, $task_desc=''){// this will help responding web server, will keep alive the script execution
    $print_every_x_secs = 20;

    $current_time = microtime(1);
    if(!$GLOBALS['IWP_MMB_PROFILING']['TASKS'][$unique_task]['START']){
            $GLOBALS['IWP_MMB_PROFILING']['TASKS'][$unique_task]['START'] = $current_time;	
    }

    if(!$GLOBALS['IWP_MMB_PROFILING']['LAST_PRINT'] || ($current_time - $GLOBALS['IWP_MMB_PROFILING']['LAST_PRINT']) > $print_every_x_secs){

            //$print_string = "TT:".($current_time - $GLOBALS['IWP_MMB_PROFILING']['ACTION_START'])."\n";
            if(!empty($task_desc)){
                $print_string = $unique_task."  Task Desc :".$task_desc." TT:".($current_time - $GLOBALS['IWP_MMB_PROFILING']['TASKS'][$unique_task]['START']);
            }else {
                $print_string = $unique_task." TT:".($current_time - $GLOBALS['IWP_MMB_PROFILING']['TASKS'][$unique_task]['START']);
            }
            iwp_mmb_print_flush($print_string);            		
    }
}

function iwp_mmb_print_flush($print_string){// this will help responding web server, will keep alive the script execution
		
    echo $print_string." ||| ";
    echo "TT:".(microtime(1) - $GLOBALS['IWP_MMB_PROFILING']['ACTION_START'])."\n";
    $GLOBALS['IWP_MMB_PROFILING']['LAST_PRINT'] = $current_time;
    ob_flush();
    flush();
}

?>