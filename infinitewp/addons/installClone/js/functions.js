installCloneAddonFlag=1;
function singleSiteSelector()
{
	var sContent ='';
	if(sitesList!=null && sitesList!=undefined &&  getPropertyCount(sitesList)>0)
	 {
	$.each(sitesList, function(key,value) {
	
	sContent=sContent+'<div class="single_website_cont searchable"  id="s'+value.siteID+'" sid="'+value.siteID+'" onclick=""><a title="'+value.name+'">'+value.name+'</a></div>';
		
		});
		 sContent= '<div class="single_site_selector shadow_stroke_box siteSearch"><div class="th rep_sprite"><input name="" value="type to filter" type="text" class="input_type_filter search_site" style="color: rgb(170, 170, 170); "></div><div class="single_website_items_cont"><div class="content">'+sContent+'<div class="no_match hiddenCont" style="display:none">Bummer, there are no websites that match.<br />Try typing fewer characters.</div><div class="clear-both"></div></div><div class="pane"></div> <div class="clear-both"></div> <div class="clear-both"></div></div></div>';
	 }
	 else
	 {
	 sContent='<div class="single_website_items_cont"><div class="no_match hiddenCont">No websites added yet.</div> <div class="clear-both"></div> <div class="clear-both"></div></div>';
	 }
	
	 return sContent;
}
function siteSelectorNanoSingle()
{
	if ( $.browser.msie && $.browser.version=='8.0') {
		$(".single_website_items_cont .single_website_cont:nth-child(3n+3)").css({"width":"235px", "border-right":"0"});
	}
	$(".single_website_items_cont.nano").nanoScroller({stop: true});
	$(".single_website_items_cont").css('height',$(".single_website_items_cont").height()).addClass('nano');
	$(".single_website_items_cont.nano").nanoScroller();	
}

function processCloneTestConnect(data)
{
	processTestConnection(data,"selectDestination","cloneTestConnection");
}

function installCloneProcess(data)
{
	if(data!=undefined)
	{
		$("#totalBtnAction").removeClass('disabled');
		return false;
	}
	var checkForm = validateForm("clonePanel");
	if(checkForm!=false)
	{
		var tempArray={};
	tempArray['action']='installCloneNewSite';
	tempArray['args']={};
	tempArray['args']['params']={};
	if(checkForm.sourceID!=undefined && checkForm.sourceID!='')
	{
		tempArray['args']['siteIDs']={};
		tempArray['args']['siteIDs'][0]=checkForm.sourceID;
		checkForm.sourceID={};
		tempArray['action']="installCloneExistingSite";
	}
	tempArray['args']['params']=checkForm;
	if(checkForm.sourceSiteID!=undefined && checkForm.sourceSiteID!='')
	tempArray['args']['params']['sourceSiteID']=checkForm.sourceSiteID;
	if($('#hostPassive').hasClass("active"))
	var hostPassive = 1;
	else
	var hostPassive = '';
	if($('#hostSSL').hasClass("active"))
	var hostSSL = 1;
	else
	var hostSSL = '';
	tempArray['args']['params']['hostPassive']=hostPassive;
	tempArray['args']['params']['hostSSL']=hostSSL;
	doHistoryCall(ajaxCallPath,tempArray,"installCloneProcess");
	$("#totalBtnAction").addClass('disabled');
	}

}

function processProfileAction_IC(data)
{
	loadProfiles_IC();
}