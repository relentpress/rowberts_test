var generalBackupURL = 'localPackage';
function installDestinationData(hide)
{
	var extraCont='';
	if(hide==1)
	extraCont = 'style="display:none"';
	var content = '<div id="selectDestination" '+extraCont+' class="subtabData"> <div class="th_sub rep_sprite"> <ul class="th_sub_nav"> <li><a class="rep_sprite active optionSelect cloneOptionTab" function="installCloneFTPForm">NEW SITE</a></li> <li><a class="rep_sprite optionSelect cloneOptionTab existingSite" function="siteSelectorWithForm">EXISTING SITE</a></li> </ul> </div> <div class="inner_cont">'+installCloneFTPForm()+' <div class="clear-both"></div></div> </div><div class="clear-both"></div>';
	return content;
}
function installCloneLoad()
{
	var content='';
	content=content+'<div class="site_nav_sub"> <ul> <li><a class="optionSelect installCloneTab active" id="installCloneFromWP">INSTALL FRESH WP</a></li> <li><a class="optionSelect installCloneTab " id="installCloneFromSite">CLONE AN EXISTING SITE</a></li> <li><a class="optionSelect installCloneTab" id="installCloneFromBackup">CLONE FROM BACKUP</a></li> <div class="clear-both"></div> </ul> </div><div id="clonePanel"></div>';
	
	$("#pageContent").html(content);
	installCloneFromWP();
}
function installCloneFromWP()
{
	var content='';
	content ='<div class="steps_container install_wp"> <div class="th rep_sprite"> <div class="title droid700">INSTALL A FRESH COPY OF WP</div> </div> <div id="cloneFromSites"><input type="hidden" id="backupURL" class="formVal" value="'+generalBackupURL+'">'+installDestinationData()+' </div> <div class="clear-both"></div> <div class="th_sub rep_sprite finalClonePageData cloneTestConnectionTh" style="border-top:1px solid #c6c9ca; border-bottom:0;"> <div class="test_conn_cont float-right"> <div class="test_conn" id="cloneTestConnection">Test connection</div> </div> </div><div class="th_sub th_sub_btm rep_sprite" style=""> <div class="save_new_profile float-left profileActionBtn_IC" style="display: block;"> <input name="" type="text" class="float-left validateField onEnter txtHelp" onenterbtn=".btn_new_profile" value="new profile" helptxt="new profile" id="newProfileName_IC" style="color: rgb(170, 170, 170);"> <div class="rep_sprite btn_new_profile float-left createProfile_IC ">Create</div><a class="rep_sprite_backup updateProfile_IC createProfile_IC float-left profileActionBtn" style="display:none" profile_id="">Update Profile</a> </div><div class="btn_action float-right"><a class="rep_sprite finalStep" id="totalBtnAction" function="installCloneProcess" >Install WP</a></div> <div class="checkbox generalSelect float-right finalClonePageData formVal active" id="toIWP">Add to InfiniteWP</div> </div> </div>';
	$("#clonePanel").html(content);
	$(".install_wp #newProfileName_IC").val('new profile');
	loadProfiles_IC();
	
}
function installCloneFromSite()
{
	var content='';
	content ='<div class="steps_container clone_existing"> <div class="th rep_sprite"> <div class="title droid700">CLONE AN EXISTING SITE</div> </div> <div class="th_sub rep_sprite"> <ul> <li><a class="current rep_sprite_backup next subtabSteps" function="loadSingleSiteSelectorSource" step="1">SELECT SOURCE</a></li> <li class="line"></li> <li><a class="subtabSteps step1 rep_sprite_backup next" function="displayCloneLoadFromBackup " step="2">SELECT BACKUP</a></li> <li class="line"></li> <li><a function="loadSingleSiteSelectorDestination" class="subtabSteps step1 step2 rep_sprite_backup next" step="3">SELECT DESTINATION</a></li> </ul> </div> <div id="cloneFromSites"> <div id="selectSource" class="subtabData"> <div class="inner_cont useSourceSiteID">'+singleSiteSelector()+'</div> </div> <div id="selectFromBackup" style="display:none" class="subtabData"> <div class="inner_cont"> <div class="left_col"> <div class="sub_title">CREATE A BACKUP NOW TO CLONE</div> <div class="sub_title_hint">Backup will include your DB + Files within your site root.</div> <div class="label">FILES & FOLDERS TO EXCLUDE</div> <input name="" class="txtHelp" type="text" id="excludeFolders" value="eg., old-backup.zip, wp-content/old-backups" helptxt="eg., old-backup.zip, wp-content/old-backups" style="color: rgb(170, 170, 170); "/> <div class="label">FOLDERS TO INCLUDE</div> <input name="" type="text" id="includeFolders" /> <div class="label_hint">in addition to the default <span>wp-admin, wp-content and wp-includes</span> folders.</div> <div class="btn_backup_now" id="onDemandBackup"><a>CREATE A BACKUP NOW TO CLONE</a></div> </div> <div class="right_col"> <div class="sub_title">SELECT A BACKUP TO CLONE</div> <ul class="backup_list" id="prevBackups"> </ul><div class="backup_empty_cont" style="display:none">No backups were found.<br />Create a backup now to clone.</div>  </div> </div> </div>'+installDestinationData(1)+'</div> <div class="clear-both"></div> <div class="th_sub rep_sprite finalClonePageData cloneTestConnectionTh" style="border-top:1px solid #c6c9ca; border-bottom:0; display:none"> <div class="save_new_profile float-left profileActionBtn_IC" style="display: block;"> <input name="" type="text" class="float-left validateField onEnter txtHelp" onenterbtn=".btn_new_profile" value="new profile" helptxt="new profile" id="newProfileName_IC" style="color: rgb(170, 170, 170);"> <div class="rep_sprite btn_new_profile float-left createProfile_IC ">Create</div><a class="rep_sprite_backup updateProfile_IC createProfile_IC float-left profileActionBtn" style="display:none" profile_id="">Update Profile</a> </div><div class="test_conn_cont float-right"> <div class="test_conn" id="cloneTestConnection">Test connection</div> </div> </div><div class="th_sub th_sub_btm rep_sprite"> <div class="btn_next_step float-right rep_sprite disabled "><a id="totalBtnAction">Select Backup <div class="taper"></div></a> </div><div class="checkbox generalSelect float-right finalClonePageData formVal active" id="toIWP" style="display:none">Add to InfiniteWP</div> </div> </div>';
	$("#clonePanel").html(content);
	loadSingleSiteSelectorSource();
	siteSelectorNanoSingle();
	loadProfiles_IC();
}
function installCloneFromBackup()
{
	var content="";
	content = '<div class="steps_container clone_existing backup_file"> <div class="th rep_sprite"> <div class="title droid700">CLONE FROM A BACKUP FILE</div> </div> <div class="th_sub rep_sprite"> <ul> <li><a class="current rep_sprite_backup next subtabSteps" function="loadBackupForm" step="1">BACKUP FILE</a></li> <li class="line"></li> <li><a class="subtabSteps step1 rep_sprite_backup next" function="loadBackupDestination" step="2">SELECT DESTINATION</a></li> </ul> </div> <div id="cloneFromSites"> <div id="backupForm" class="subtabData"> <div class="inner_cont"> <div class="single_col"> <div class="label">LINK TO BACKUP FILE</div> <input name="" type="text" id="backupURLTemp"  /> </div> </div> </div> '+installDestinationData(1)+' </div> <div class="th_sub rep_sprite finalClonePageData cloneTestConnectionTh" style="border-top:1px solid #c6c9ca; border-bottom:0; display:none"> <div class="test_conn_cont float-right"> <div class="test_conn" id="cloneTestConnection">Test connection</div> </div></div> <div class="th_sub th_sub_btm rep_sprite" style=""> <div class="save_new_profile float-left profileActionBtn_IC" style="display: none;"> <input name="" type="text" class="float-left validateField onEnter txtHelp" onenterbtn=".btn_new_profile" value="new profile" helptxt="new profile" id="newProfileName_IC" style="color: rgb(170, 170, 170);"> <div class="rep_sprite btn_new_profile float-left createProfile_IC ">Create</div><a class="rep_sprite_backup updateProfile_IC createProfile_IC float-left profileActionBtn" style="display:none" profile_id="">Update Profile</a> </div><div class="btn_next_step rep_sprite float-right" ><a  id="totalBtnAction">Select Destination<div class="taper"></div></a></div> <div class="checkbox generalSelect float-right finalClonePageData formVal active" id="toIWP" style="display:none">Add to InfiniteWP</div> </div> </div>';
	$("#clonePanel").html(content);
	loadBackupForm();
	
}

var cloneExtraContent='<div class="sub_title">ADMIN CREDENTIALS</div> <div class="label">USERNAME</div> <input name="" type="text" id="newUserName" class="formVal required" /> <div class="label">PASSWORD</div> <input name="" type="text" id="newUserPassword" class="formVal required" /><div class="label">EMAIL</div> <input name="" type="text" id="adminEmail" class="formVal required" />'
function installCloneFTPForm()
{
	var content='';
	content = '<div class="left_col"> <div class="sub_title">&nbsp;</div> <div class="label">WEBSITE URL - <span style="text-transform:none; font-size:12px">Path to the installation directory</span></div> <input name="" type="text" id="newSiteURL" class="formVal required txtHelp" value="http://yourdomain.com/path" helpTxt="http://yourdomain.com/path" style="color:#AAA" /> <div class="sub_title">FTP DETAILS</div> <div class="label">FTP HOST</div> <input name="" type="text" id="hostName" class="formVal required"/> <div class="label">FTP PORT</div><input name="" type="text" id="hostPort" value="21" class="formVal required"><div class="label">FTP USERNAME</div> <input name="" type="text" id="hostUserName" class="formVal required"/> <div class="label">FTP PASSWORD</div> <input name="" type="text" id="hostPassword" class="formVal required" /> <div class="label">FTP INSTALLATION FOLDER PATH</div> <input name="" type="text" id="remoteFolder" class="formVal required" /><div style="text-transform:none; text-transform: none;color: #9398A2;margin-top: -14px;"> Should point to same directory as WEBSITE URL</div><div id="use_sftp" class="checkbox generalSelect label">SFTP Connection</div><div class="checkbox generalSelect label" id="hostSSL">FTP SSL Connection</div><div class="checkbox generalSelect label active" id="hostPassive" >Use passive mode</div> </div> <div class="right_col"> <div class="sub_title">DATABASE DETAILS</div> <div class="label">DB HOST</div> <input name="" type="text" id="dbHost" class="formVal required" /> <div class="label">DB NAME</div> <input name="" type="text" id="dbName" class="formVal required" /> <div class="label">DB USERNAME</div> <input name="" type="text" id="dbUser" class="formVal required"/> <div class="label">DB PASSWORD</div> <input name="" type="text" id="dbPassword" class="formVal required" /><div class="label">DB PREFIX</div> <input name="" value="wp_" type="text" id="dbPrefix" class="formVal required" />'+cloneExtraContent+' </div>';
	return '<div class="dropdown_cont profile_cont float-left" style="margin: 10px 170px;float: right;"> <div class="dropdown_btn" id="selectProfileBtn_IC"><span id="profileDropName" class="dropdown_btn_val">Select Profile</span><span class="arrow_down"></span></div> <ul id="selectProfileOptions_IC" class="dropdownToggle" style="display: none;"></ul> </div><form id="installClone" method="post">'+content+'</form>';
}

function installCloneLoadFromBackup(data)
{
	
	if(data==undefined || data=='')
	{
		var tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['cloneSiteBackupsHTML']=$("#selectSource .single_website_cont.active").attr('sid');
		doCall(ajaxCallPath,tempArray,'installCloneLoadFromBackup');
		$("#selectFromBackup #prevBackups").html('');
		$("#selectFromBackup .backup_empty_cont").hide();
		$("#selectFromBackup .backup_empty_cont, #selectFromBackup #onDemandStatus").hide();
		displayCloneLoadFromBackup();

		$("#totalBtnAction").closest("div").addClass('disabled');
	}
	else
	{
		$("#selectFromBackup .backup_empty_cont").hide();
		if(data.data.cloneSiteBackupsHTML!='')
		{

			$("#selectFromBackup #prevBackups").html(data.data.cloneSiteBackupsHTML);

		}
		else
		$("#selectFromBackup .backup_empty_cont").show();
		
	}
}
function displayCloneLoadFromBackup()
{
	$("#totalBtnAction").closest("div").addClass('btn_next_step').removeClass('btn_action').addClass('rep_sprite');
	$("#totalBtnAction").html('Select Destination<div class="taper"></div>').attr("function","loadSingleSiteSelectorDestination").removeClass('rep_sprite'); 
	$("#selectFromBackup").show();
}

function loadSingleSiteSelectorSource()
{
	$("#totalBtnAction").closest("div").addClass('btn_next_step').removeClass('btn_action').addClass('rep_sprite');
	$("#totalBtnAction").html('Select Backup<div class="taper"></div>').attr("function","installCloneLoadFromBackup").removeClass('rep_sprite'); 
	$("#selectSource").show();
	
}

function loadSingleSiteSelectorDestination()
{
	
	$("#totalBtnAction").closest("div").addClass('btn_action').removeClass('btn_next_step').removeClass('rep_sprite');
	$("#totalBtnAction").html('Clone').attr("function","installCloneProcess").addClass('finalStep rep_sprite'); 
	$("#selectDestination").show();
	$("#clonePanel .finalClonePageData").show();
	
}
function createBackupAndLoad(data)
{
	
	
	if(data.actionResult.detailedStatus[0].status=="success")
	{
		$("#selectFromBackup #onDemandStatus").html('<span class="success_icon"></span>Done. Proceed with selecting the destination.').addClass('btn_backup_now_done').removeClass('btn_backup_now_loading');
		if(data.data.cloneSiteBackupsHTML!='')
		$("#selectFromBackup .backup_empty_cont").hide();
		$("#selectFromBackup #prevBackups").html(data.data.cloneSiteBackupsHTML);
		$("#selectFromBackup #prevBackups li:first a").click();
	}
	else
	{
		$("#selectFromBackup #onDemandStatus").html('<span class="fail_icon"></span>'+data.actionResult.detailedStatus[0].errorMsg).addClass('btn_backup_now_error').removeClass('btn_backup_now_loading');
	}
	$("#selectFromBackup #onDemandBackup").removeClass('disabled');
	
	
	
	
}


function siteSelectorWithForm()
{
	var content=singleSiteSelector();
	content = content+'<div class="single_col">'+cloneExtraContent+'</div>';
	return content;
}



function loadBackupForm()
{
	$("#totalBtnAction").closest("div").addClass('btn_next_step').removeClass('btn_action').addClass('rep_sprite');
	$("#totalBtnAction").html('Select Destination<div class="taper"></div>').attr("function","loadBackupDestination").removeClass('rep_sprite');
	$("#backupForm").show();
	$('.profileActionBtn_IC').hide();
}

function loadBackupDestination()
{
	$("#totalBtnAction").closest("div").addClass('btn_action').removeClass('btn_next_step').removeClass('rep_sprite');
	$("#totalBtnAction").html('Clone').attr("function","installCloneProcess").addClass('finalStep rep_sprite'); 
	$("#cloneFromURL").remove();
	$("#cloneFromSites").prepend("<input type='hidden' id='backupURL' class='formVal' value='"+$("#backupURLTemp").val()+"'>");
	$("#cloneFromURL").remove();
	$("#cloneFromSites").prepend("<input type='hidden' id='cloneFromURL' class='formVal' value='1'>");
	$("#selectDestination").show();
	$("#clonePanel .finalClonePageData").show();
	loadProfiles_IC();
}

function loadProfiles_IC(profileID)
{
	$(".dropdown_cont, .save_new_profile,").show();
	var tempArray={},profileVar='';
	tempArray['requiredData']={};
	tempArray['requiredData']['installCloneGetAllProfiles']={};
	tempArray['requiredData']['installCloneGetAllProfiles']=1;
	//tempArray['requiredData']['installCloneGetProfile']['profileID'] = profileID;
	/* profileVar='InstallClone';
	$(".install_wp #profileDropName").text('Select '+profileVar+' profile'); */
	doCall(ajaxCallPath,tempArray,'loadProfileDropDown_IC');
}

function loadProfileDropDown_IC(data)
{
	data=data.data.installCloneGetAllProfiles;
	var content='';
	if(getPropertyCount(data)>0)
	{
		$.each(data, function(val,object){
			content=content+'<li><a class="loadProfile_IC dropOption" dropopt="'+object.installCloneProfileID+'">'+object.profileName+'<div class="rep_sprite remove_bg"><span class="rep_sprite_backup del delProfile_IC" profile_id="'+object.installCloneProfileID+'"></span></div> </a></li>';
		});
	}
	if(content=='')
	content='<li class="emptyCont"><a>No profile created yet.</a></li>';
	$("#selectProfileOptions_IC").html(content).hide(); 
	
}

function populateProfileDropDown_IC(data)
{
	var populateValues = data.data.installCloneGetProfile.accountInfo;
	$('#installClone #adminEmail').val(populateValues.adminEmail);
	$('#installClone #dbHost').val(populateValues.dbHost);
	$('#installClone #dbName').val(populateValues.dbName);
	$('#installClone #dbPassword').val(populateValues.dbPassword);
	$('#installClone #dbPrefix').val(populateValues.dbPrefix);
	$('#installClone #dbUser').val(populateValues.dbUser);
	$('#installClone #hostName').val(populateValues.hostName);
	$('#installClone #hostPort').val(populateValues.hostPort);
	$('#installClone #hostPassword').val(populateValues.hostPassword);
	$('#installClone #hostUserName').val(populateValues.hostUserName);
	$('#installClone #newSiteURL').val(populateValues.newSiteURL);
	$('#installClone #newUserName').val(populateValues.newUserName);
	$('#installClone #newUserPassword').val(populateValues.newUserPassword);
	$('#installClone #remoteFolder').val(populateValues.remoteFolder);
	if(populateValues.hostPassive == "1")
	$('#installClone #hostPassive').addClass('active');
	else
	$('#installClone #hostPassive').removeClass('active');
	if(populateValues.hostSSL == "1")
	$('#installClone #hostSSL').addClass('active');
	else
	$('#installClone #hostSSL').removeClass('active');
    
        if(populateValues.use_sftp == "1")
	$('#installClone #use_sftp').addClass('active');
	else
	$('#installClone #use_sftp').removeClass('active');
	//validateForm("installClone");
}