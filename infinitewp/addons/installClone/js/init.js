$(function () {
	$(".installCloneTab").live('click',function() {
		eval($(this).attr("id")+"()");
	});
	$(".subtabSteps").live('click',function() {
		if($(this).hasClass('completed'))
		{
			processContentFromTabs(this);
			$("#totalBtnAction").closest("div").removeClass('disabled');
			var currentStep = parseInt($(this).attr("step"));
			$(".subtabSteps.step"+currentStep).removeClass('completed');
			$(this).removeClass("completed").addClass('current');
			
		}
		
	});
	$("#totalBtnAction").live('click',function() {
		if(!$(this).hasClass('disabled') )
		{
			if($(this).hasClass('finalStep'))
			processContentFromTabs(this,1,1);
			else
			{
				var checkForm = validateForm("cloneFromSites");
				if(checkForm!=false)
				processContentFromTabs(this,1);
			}
			
		}

		
		return false;
	});
	$(".single_website_cont").live('click',function() {
		$(".single_website_cont").removeClass('active');
		$(this).addClass('active');
		$("#totalBtnAction").closest("div").removeClass('disabled');;
		if($(this).closest('.inner_cont').hasClass('useSiteID'))
		{
			$("#sourceID").remove();
			$("#cloneFromSites").prepend("<input type='hidden' id='sourceID' class='formVal' value='"+$(this).attr('sid')+"'>");
		}
		if($(this).closest('.inner_cont').hasClass('useSourceSiteID'))
		{
			$("#sourceSiteID").remove();
			$("#cloneFromSites").prepend("<input type='hidden' id='sourceSiteID' class='formVal' value='"+$(this).attr('sid')+"'>");
		}
		
	});
	$("#onDemandBackup").live('click',function() {
		$("#onDemandStatus").remove();
		var tempArray={};
		tempArray['action']='installCloneBackupNow';
		tempArray['args']={};
		tempArray['requiredData']={};
		tempArray['args']['params']={};
		tempArray['args']['params']['config']={};
		var backupExcludeFiles= $("#selectFromBackup #excludeFolders").val();
		if(backupExcludeFiles=='eg., old-backup.zip, wp-content/old-backups')
		var backupExcludeFiles='';
		tempArray['args']['params']['config']['exclude']=backupExcludeFiles;
		tempArray['args']['params']['config']['include']=$("#selectFromBackup #includeFolders").val();
		tempArray['args']['siteIDs']=$("#selectSource .single_website_cont.active").attr('sid');
		tempArray['requiredData']['cloneSiteBackupsHTML']=$("#selectSource .single_website_cont.active").attr('sid');
		$(this).after('<div class="btn_backup_now_loading" id="onDemandStatus">Backing up.. Jus\' a moment..</div>');
		$(this).addClass('disabled');
		doCall(ajaxCallPath,tempArray,'createBackupAndLoad');
		
	});
	$(".selectBackupFromList").live('click',function() {
		$(".selectBackupFromList").removeClass('active');
		$(this).addClass('active');
		$("#totalBtnAction").closest("div").removeClass('disabled');
		$("#backupURL").remove();
		$("#cloneFromSites").prepend("<input type='hidden' id='backupURL' class='formVal' value='"+$(this).attr("backupurl")+"'>");
	});
	$(".cloneOptionTab").live('click',function() {
		
		$("#selectDestination .inner_cont").removeClass('useSiteID');
		$("#totalBtnAction").closest("div").removeClass('disabled');
		tempReturn=eval($(this).attr("function")+"()");
		$("#selectDestination .inner_cont").html(tempReturn);
		$(".install_wp #newProfileName_IC").val('');
		loadProfiles_IC();
		$(".updateProfile_IC").hide();
		if($(this).hasClass('existingSite'))
		{
			$(".cloneTestConnectionTh").hide();
			$("#totalBtnAction").closest("div").addClass('disabled');
			$("#selectDestination .inner_cont").addClass('useSiteID');
			// $("#selectDestination .single_col .formVal").removeClass('required');
			$("#clonePanel #toIWP").hide();
			siteSelectorNanoSingle();
			$('.profileActionBtn_IC').hide();
		}
		else
		{
			$("#cloneFromSites #sourceID").remove();
			$(".cloneTestConnectionTh").show();
			$("#clonePanel #toIWP").show();
		}
		
	});
	$("#cloneTestConnection").live('click',function() { 
		$("#selectDestination .inner_cont .conn_test_error_cont").remove();
		if(!$(this).hasClass('testing'))
		{
			$(this).removeClass('error success');
			var checkForm=validateForm("clonePanel");

			if(checkForm!=false)
			{
				tempArray={};
				tempArray['requiredData']={};
				tempArray['requiredData']['repositoryTestConnection']={};
				tempArray['requiredData']['repositoryTestConnection']["iwp_ftp"]=checkForm;
				doCall(ajaxCallPath,tempArray,'processCloneTestConnect');
				$(this).addClass('testing');
			}
		}

	});
	$(".createProfile_IC").live('click',function() {  //Modified for update


		if((($(".install_wp #newProfileName_IC").val()=='')&&(!$(this).hasClass('updateProfile_IC'))) || (($(".install_wp #newProfileName_IC").val()=='new profile') && !$(this).hasClass('updateProfile_IC')))
		{
			$(".install_wp #newProfileName_IC").addClass('error');
			return false;
		}

		var checkForm=validateForm("installClone");
		if(checkForm!=false)
		{

			var tempArray={};
			tempArray['requiredData']={};

			tempArray['requiredData']['installCloneSaveProfile']={};
			//tempArray['requiredData']['backupRepositorySaveProfile']['type']=$(".dialog_cont .repBtn.active").attr('repType');
			tempArray['requiredData']['installCloneSaveProfile']['profileName']=$("#newProfileName_IC").val();
			tempArray['requiredData']['installCloneSaveProfile']['accountInfo']=checkForm;
			if($('#hostPassive').hasClass("active"))
			var hostPassive = 1;
			else
			var hostPassive = '';
			if($('#hostSSL').hasClass("active"))
			var hostSSL = 1;
			else
			var hostSSL = '';
			tempArray['requiredData']['installCloneSaveProfile']['accountInfo']['hostPassive']=hostPassive;
			tempArray['requiredData']['installCloneSaveProfile']['accountInfo']['hostSSL']=hostSSL;
			if($(this).hasClass('updateProfile_IC'))
			{
				tempArray['requiredData']['installCloneSaveProfile']['installCloneProfileID']=$(this).attr('profile_id');
				tempArray['requiredData']['installCloneSaveProfile']['profileName']=$("#profileDropName").text();
			}
			else
			{
				//tempArray['requiredData']['backupRepositorySaveProfile']['status']="active";
				//$('.install_wp .profileActionBtn').addClass('disabled');
			}
			//$(".install_wp .profile_cont").prepend('<div class="create_profile_status loading profileStatusDiv"></div>');
			doCall(ajaxCallPath,tempArray,'processProfileAction_IC');
		}

	});
	$(".loadProfile_IC").live('click',function() { 
		//loadProfiles_IC($(this).attr('dropopt'));
		var profileID = $(this).attr('dropopt');
		$(".dropdown_cont, .save_new_profile, .updateProfile_IC").show();
		$(".updateProfile_IC").attr("profile_id",profileID);
		$(".install_wp #newProfileName_IC").val('new profile');
		var tempArray={},profileVar='';
		tempArray['requiredData']={};
		tempArray['requiredData']['installCloneGetProfile']={};
		//tempArray['requiredData']['installCloneGetProfile']=1;
		tempArray['requiredData']['installCloneGetProfile']['profileID'] = profileID;
		/* profileVar='InstallClone';
		$(".install_wp #profileDropName").text('Select '+profileVar+' profile'); */
		doCall(ajaxCallPath,tempArray,'populateProfileDropDown_IC','json');

	});

	$(".delProfile_IC").live('click',function() { 
		tempArray={};
		tempArray['requiredData']={};
		tempArray['requiredData']['installCloneDeleteProfile']={};
		tempArray['requiredData']['installCloneDeleteProfile']['installCloneProfileID']=$(this).attr('profile_id');
		if($(this).closest('.loadProfile').text()==$(".install_wp #profileDropName").text())
		$(".install_wp #profileDropName").text("Select profile");
		$(this).closest('li').remove();
		if($(".install_wp #selectProfileOptions li").length==0)
		{
			$(".install_wp #profileDropName").text("Select profile");
			$(".install_wp #selectProfileOptions").html('<li class="emptyCont"><a>No profile created yet.</a></li>');
		}
		doCall(ajaxCallPath,tempArray,'processProfileAction');
		return false;
	}).live('mouseenter',function() {
		$(this).closest("a").addClass('delWarn');
	}).live('mouseleave',function() {
		$(this).closest("a").removeClass('delWarn');
	});

        $("#use_sftp").live('click',function() {
                var hostPort = $("#hostPort").val();
		if($(this).hasClass('active')) {
                    if(parseInt(hostPort)==21) {
                        $("#hostPort").val('22');
                    }
                } else {
                    if(parseInt(hostPort)==22) {
                        $("#hostPort").val('21');
                    }
                }
        });


	
});

function processContentFromTabs(object,btnClick,finalStep)
{
	if(finalStep!=1)
	{
		$("#totalBtnAction").removeClass('finalStep');
		$(".subtabData").hide();
		$(".finalClonePageData").hide();
		var currentStep = parseInt($(".subtabSteps.current").attr("step"));
		var stepData=currentStep+1;
		if(btnClick==1)
		$(".subtabSteps.current").addClass('completed');
		$(".subtabSteps").removeClass('current');
		if(btnClick==1)
		$(".subtabSteps[step='"+stepData+"']").addClass('current');
		
	}
	eval($(object).attr("function")+"()");
}