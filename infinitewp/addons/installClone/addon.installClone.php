<?php

/*
slug: installClone
version: 1.3.1
*/

class addonInstallClone{
	
	private static $version = '1.3.1';
	
	public static function version(){
		return self::version;	
	}
	
	public static function init(){
		require_once(APP_ROOT."/addons/installClone/controllers/func.php");	
		require_once(APP_ROOT."/addons/installClone/controllers/manageClientsInstallClone.php");	
		panelRequestManager::addFunctions('cloneSiteBackupsHTML', 'installCloneSaveProfile', 'installCloneGetProfile', 'installCloneGetAllProfiles','installCloneDeleteProfile');
		regHooks('addonMenus', 'responseProcessors', 'taskTitleTemplate', 'updateCheck');
	}	
	
	public static function install(){
		
		$Q1 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:install_clone_repository` (
  `installCloneProfileID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profileName` varchar(255) DEFAULT NULL,
  `accountInfo` text,
  PRIMARY KEY (`installCloneProfileID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");
		if($Q1){
			return true;	
		}
		return false;
	}
	
	public static function update(){
		
		$oldVersion = getOldAddonVersion('installClone');
		
		if(version_compare($oldVersion, '1.1.0') === -1){
			$Q1 = DB::doQuery("CREATE TABLE IF NOT EXISTS `?:install_clone_repository` (
	  `installCloneProfileID` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `profileName` varchar(255) DEFAULT NULL,
	  `accountInfo` text,
	  PRIMARY KEY (`installCloneProfileID`)
	) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");
			if($Q1){
				return true;	
			}
			return false;
		}
		return true;
	}
}

?>