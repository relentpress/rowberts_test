<?php
  if(!empty($d['siteBackups'])){
  foreach($d['siteBackups'] as $key => $value){ 
  	foreach($value as $siteBackup){
		if($siteBackup['what'] == 'full' && !empty($siteBackup['downloadURL'])){ ?>
        	<li><a class="selectBackupFromList" backupURL="<?php echo getFullWPURL($d['siteID'], $siteBackup['downloadURL']); ?>"><span class="check rep_sprite_backup"></span><span class="bu_name"><?php echo $siteBackup['backupName']; ?></span><span class="bu_time rep_sprite_backup"><?php echo @date(Reg::get('dateFormatLong'), $siteBackup['time']); ?></span><div class="clear-both"></div></a></li>
<?php }
  	}
  }
}
?>