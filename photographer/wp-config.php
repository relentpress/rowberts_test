<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', false); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '' ); //Added by WP-Cache Manager
define('DB_NAME', 'rowberts_photographer');

	define('DB_USER', 'rowberts_staging');
	define('DB_PASSWORD', 'Ma&7HZXuxR');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 */
define('AUTH_KEY',         'DfKVgb8$o?FC/d|h(|=5=F/RRRm3$STJd&V~27!h~ZG+~{3A&@ooD*K**-W5`Sh8');
define('SECURE_AUTH_KEY',  '{|d7;Iev4X=ufitQh;<nJOJM[rR%gtu2l+F&=LDsbB}F*}cI2- <Dy}p{F2DJMZK');
define('LOGGED_IN_KEY',    ']LE@j(FZ;P$P c,Q:3o*ara0W[%=d-K)6gA Ac*Y E><Gf+yq<+Wu`_;Q|$I{vld');
define('NONCE_KEY',        'R+H~Dg|1@/:-ul<D$q5!en8%gT!/0=k,<cG7n-f(K0UF(^vaR1i,zD`#{Ne.ZnSi');
define('AUTH_SALT',        'u?:Y<&1|o*mvf9lOr0P;TW)tnhXfg8PfN2|DNl975INsfX&G|dJ$Ux~J[0FlRy~u');
define('SECURE_AUTH_SALT', '73nF/)]K2^McO>c<UVIaUka2~$n3mf[9&0s5O)bTmqf/4E~KKJ+0:^R=ICS%m,7`');
define('LOGGED_IN_SALT',   '#F LqNPI:JJMrgV]|q~r)=+ZrJf/T+@CV]CgcZ|+T8yqupJ Zk<oFc`|{po<ZzY<');
define('NONCE_SALT',       '&8RG|tvdviDQsu=;RD2A1ju]dG|SB&sxZ`%{-#l+ChGOav@T+(XEs}(+{RNj},AP');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/*Enable Auto Updates for WP Core (Major), Themes & Plugins by CER */
define('WP_AUTO_UPDATE_CORE', true);
add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );
