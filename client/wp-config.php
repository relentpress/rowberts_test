<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */


/** VARIABLE CONNECTION STRINGS BASED ON SERVER **/

/** Developer One **/
if( stristr( $_SERVER['SERVER_NAME'], "client.local.dev" ) ) {

	// Dev Environment
	define( 'DB_NAME', 'client' );
	define( 'DB_USER', 'root' );
	define( 'DB_PASSWORD', 'root' );
	define( 'DB_HOST', 'localhost' );
	
	//define( 'WP_HOME', 'http://template.local.dev');
	//define( 'WP_SITEURL', WP_HOME);
	
	// Dev will always want debug on and caching off
	define( 'WP_DEBUG', true );
	define( 'WP_CACHE', false );	 
} 

/** Developer Two **/
elseif( stristr( $_SERVER['SERVER_NAME'], "localhost" ) ) {
 
	// Staging Environment
	define( 'DB_NAME', 'template' );
	define( 'DB_USER', 'root' );
	define( 'DB_PASSWORD', '' );
	define( 'DB_HOST', 'localhost' );
	
	//define( 'WP_HOME', 'http://localhost/template'); 
	//define( 'WP_SITEURL', WP_HOME);
	
	// Dev will always want debug on and caching off
	define( 'WP_DEBUG', true );
	define( 'WP_CACHE', false );		 
} 

/** Staging **/
elseif( stristr( $_SERVER['SERVER_NAME'], "client.relentpress.com" ) ) {
 
	// Staging Environment
	define( 'DB_NAME', 'rowberts_client' );
	define('DB_USER', 'rowberts_staging');
	define('DB_PASSWORD', 'Ma&7HZXuxR');
	define( 'DB_HOST', 'localhost' );
	
	//define( 'WP_HOME', 'http://template.sg.rowberts.com.au'); 
	//define( 'WP_SITEURL', WP_HOME);
	
	// UAT Environment will always be the same as production so turn off debug and turn on caching
	define( 'WP_DEBUG', false );
	define( 'WP_CACHE', true );		 
} 

/** Live **/
else {
 
	// Production Environment
	define( 'DB_NAME', 'rowberts_client' );
	define( 'DB_USER', 'rowberts' );
	define( 'DB_PASSWORD', 'Pf7&k&^ctg' );
	define( 'DB_HOST', 'localhost' );
	
	//define( 'WP_HOME', 'http://project.com'); 
	//define( 'WP_SITEURL', WP_HOME); 
	
	// Live Environment will always be the same as production so turn off debug and turn on caching
	define( 'WP_DEBUG', false );
	define( 'WP_CACHE', true );
		
}


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rRHKb66%Wrf[P?#%Cg&BH-AS>bTZ=he9o:-J.kO+Z|=7h]~ra TJFD:q5~dFOfm?');
define('SECURE_AUTH_KEY',  'PW_FU4__`-4*VeBPxf=CwR+CiA]`F ,y~k[*+-4k$E|n(eX>5%Cq{aEYGF)Q&yi6');
define('LOGGED_IN_KEY',    'R;Faie^)uN<;X1-/q_T5Z$Vhk,;d0Nj+Cs~nW`_8]xe[/:K[mp&L1=S%d&3G%@Tz');
define('NONCE_KEY',        ';q3MD|LT]+dl/*6-KqZk1Xum4-ZXOZp=YhG~gBwt9u/uR2dU{CbI%]=#GF;|49 T');
define('AUTH_SALT',        '2-z#(HQ_;<?i@-_`X7t{Xn!_-R|Q![(ZKh66|;QQO,_MJ5x;Y!_|<W`;85]-Qe,|');
define('SECURE_AUTH_SALT', '.rri_15Rwq$Ql7F^gDd|m>@$#uPj1Kn2*,|8axNGy-ug*<Ra6C-utc|mbf+zT-<l');
define('LOGGED_IN_SALT',   'mDQX0jILM20oK$d[gzlr$gsK]sNXY``.SK%WV|GTMg]*B:hwd&h6&Yb/r+I}1o}0');
define('NONCE_SALT',       '/yI<(0(s.<-R!;(^%N[5Xho}dp+C09Zgql4,Sc U?vFw$t(Z9+Jp6n-Z2:*jj2gA');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'client.relentpress.com');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');



