var FLThemeSettings;

(function($){

    /**
     * @class FLThemeSettings
     */ 
    FLThemeSettings = {
    
    	/**
	     * @property _photoSelector
	     */ 
    	_photoSelector: null,
    
	    /**
	     * @method init
	     */ 
        init: function()
        {
            this._bind();
            this._renderPresets();
            this._initColorPickers();
            this._initNav();
            this._initEditors();
        },
        
        /**
	     * @method _bind
	     */
        _bind: function()
        {
            $('.fl-settings-nav a').on('click', this._navClicked);
            $('.fl-settings-nav .fl-settings-save input').on('click', this._saveSettings);
            $('.fl-tooltip-icon').on('mouseover', this._showTooltip);
            $('.fl-tooltip-icon').on('mouseout', this._hideTooltip);
            $('.fl-img-select').on('click', this._selectPhoto);
            $('.fl-img-replace').on('click', this._replaceImage);
            $('.fl-font-select[data-weight]').on('change', this._fontChange);
            $('input[name=export]').on('click', this._exportSettings);
            $('input[name=import]').on('click', this._importSettings);
            $('select').on('change', this._selectChanged);
            $('select').trigger('change');
        },
        
        /**
	     * @method _initNav
	     */
        _initNav: function()
        {
            var links  = $('.fl-settings-nav a'),
                hash   = window.location.hash,
                active = links.filter('[href~="'+ hash +'"]');
                
            $('a.fl-active').removeClass('fl-active');
            $('.fl-settings-form').hide();
                
            if(hash == '' || active.length === 0) {
                active = links.eq(0);
            }
            
            active.addClass('fl-active');
            $('#fl-'+ active.attr('href').split('#').pop() +'-form').fadeIn();
        },
        
        /**
	     * @method _navClicked
	     */
        _navClicked: function()
        {
            $('a.fl-active').removeClass('fl-active');
            $('.fl-settings-form').hide();
            $(this).addClass('fl-active');
            $('#fl-'+ $(this).attr('href').split('#').pop() +'-form').fadeIn();
        },
        
        /**
	     * @method _saveSettings
	     */
        _saveSettings: function()
        {
            $('.fl-settings-content form').submit();
        },
        
        /**
	     * @method _clearSettingsWarning
	     */
        _clearSettingsWarning: function()
        {
            var result = confirm('WARNING!!! This will clear all of the theme settings for your entire site. Are you sure you want to do this?');
            return result;
        },
        
        /**
	     * @method _renderPresets
	     */
        _renderPresets: function()
        {
        	var wrap 			= $('<div class="fl-preset-thumbs"></div>'),
        		thumbsUrl 		= FLThemeSettings.presetThumbUrl,
        		chooseClass 	= '',
        		chooseText		= '',
        		preset 			= null,
        		presets			= FLThemeSettings.presets,
        		selectedPreset 	= $('#fl-presets-form input[name=preset]').val();
        	
        	for(preset in presets) {
        	
        		if(preset == selectedPreset) {
	        		chooseClass = 'fl-preset-choose fl-preset-active';
	        		chooseText  = FLThemeSettings.strings.selected;
        		}
        		else {
	        		chooseClass = 'fl-preset-choose';
	        		chooseText  = FLThemeSettings.strings.chooseSkin;
        		}
        	
	        	wrap.append('<div class="fl-preset-thumb" data-skin="' + presets[preset].skin + '" data-preset="' + preset + '"><img src="' + thumbsUrl + preset + '.jpg" /><div class="fl-preset-actions"><a href="#" class="' + chooseClass + '">' + chooseText + '</a><a href="' + FLThemeSettings.homeUrl + '?fl-preview=' + preset + '" class="fl-preset-preview" target="_blank">' + FLThemeSettings.strings.preview + '</a></div><div style="clear:both;"></div></div>');
        	}
        	
        	wrap.append('<div style="clear:both;"></div>');
        	
        	$('#fl-presets-form .fl-settings-section').append(wrap);
        	$('.fl-preset-choose').on('click', FLThemeSettings._choosePreset);
        },
        
        /**
	     * @method _choosePreset
	     */
        _choosePreset: function(e)
        {
	        var link 		= $(this),
	        	thumb 		= link.closest('.fl-preset-thumb'),
	        	skinId	 	= thumb.data('skin'),
	        	presetId 	= thumb.data('preset'),
	        	preset		= FLThemeSettings.presets[presetId];
        
			if(typeof preset.settings !== 'undefined') {
				FLThemeSettings._changePreset(preset.settings);
			}
			
			$('.fl-preset-choose.fl-preset-active').removeClass('fl-preset-active').html(FLThemeSettings.strings.chooseSkin);
			link.addClass('fl-preset-active').html(FLThemeSettings.strings.selected);
			
			$('#fl-presets-form input[name=skin]').val(skinId);
			$('#fl-presets-form input[name=preset]').val(presetId);
			$('.fl-settings-nav a[href~="#design"]').trigger('click');
			
			window.location.hash = 'design';	
			window.scrollTo(0, 1);
			e.preventDefault();
        },
        
        /**
	     * @method _changePreset
	     */
        _changePreset: function(settings)
        {
        	var prop  		= null,
        		field 		= null,
        		colorPicker = null;
        	
        	for(prop in settings) {
        	
	        	field = $('[name=' + prop + ']');
	        	
	        	if(field.length === 0) {
		        	continue;
	        	}
	        	
	        	field.val(settings[prop]);
	        	
	        	if(field.is('select')) {
		        	field.trigger('change');
	        	}
	        	if(field.hasClass('fl-color-picker-value')) {
	        		if(settings[prop] == '') {
		        		field.siblings('.fl-color-picker-color').css('background-color', 'transparent');
						field.parent().addClass('fl-empty');
	        		}
					else {
						colorPicker = field.siblings('.fl-color-picker-color');
			        	colorPicker.ColorPickerSetColor(settings[prop]);
			        	colorPicker.css('background-color', '#' + settings[prop]);
			        	colorPicker.parent().removeClass('fl-empty');
					}
	        	}
        	}
        },
        
        /**
	     * @method _showTooltip
	     */
        _showTooltip: function()
        {
            $(this).siblings('.fl-tooltip-text').fadeIn();
        },
        
        /**
	     * @method _hideTooltip
	     */
        _hideTooltip: function()
        {
            $(this).siblings('.fl-tooltip-text').fadeOut();
        },
        
        /**
         * @method _selectPhoto
         */ 
        _selectPhoto: function()
        {
            if(FLThemeSettings._photoSelector === null) {
                FLThemeSettings._photoSelector = wp.media({
                    title: FLThemeSettings.strings.selectPhoto,
                    button: { text: FLThemeSettings.strings.selectPhoto },
                    library : { type : 'image' },
                    multiple: false
                });
            }
            
            FLThemeSettings._photoSelector.once('select', $.proxy(FLThemeSettings._photoSelected, this));
            FLThemeSettings._photoSelector.open();
        },
        
        /**
         * @method _photoSelected
         */ 
        _photoSelected: function()
        {
        	var photo 	= FLThemeSettings._photoSelector.state().get('selection').first().toJSON(),
        		button 	= $(this),
        		wrap 	= button.closest('.fl-img-wrap'),
        		img 	= button.siblings('.fl-img'),
        		field 	= button.siblings('.fl-img-value');
        		
        	wrap.removeClass('fl-img-empty');
        	img.attr('src', photo.url);
        	field.val(photo.url);
        },
        
        /**
	     * @method _replaceImage
	     */
        _replaceImage: function()
        {
        	var button = $(this);
        	
        	button.siblings('.fl-img-value').val('');
            button.parent().addClass('fl-img-empty');
        },
        
        /**
	     * @method _fontChange
	     */
        _fontChange: function()
        {
            var val          = $(this).val(),
                weightName   = $(this).attr('data-weight'),
                weight       = $('select[name='+ weightName +']'),
                saved        = weight.attr('data-saved-setting'),
                variants     = FLFontFamilies.system[val],
                i            = 0,
                html         = '',
                selected     = '',
                map          = {
                    '100': 'Thin 100',
                    '200': 'Extra-Light 200',
                    '300': 'Light 300',
                    '400': 'Normal 400',
                    '500': 'Medium 500',
                    '600': 'Semi-Bold 600',
                    '700': 'Bold 700',
                    '800': 'Extra-Bold 800',
                    '900': 'Ultra-Bold 900'
                };
            
            saved = saved == '' ? '400' : saved;
            
            if(typeof variants === 'undefined') {
                variants = FLFontFamilies.google[val];
            }
            for( ; i < variants.length; i++) {
                selected = variants[i] == saved ? ' selected="selected"' : '';
                html += '<option value="'+ variants[i] +'"'+ selected +'>'+ map[variants[i]] +'</option>';
            }
            
            weight.html(html);
            weight.attr('data-saved-setting', '');
        },
        
        /**
	     * @method _initColorPickers
	     */
        _initColorPickers: function()
        {
            $('.fl-color-picker').each(function(){
            
                var wrapper  = $(this),
                    picker   = wrapper.find('.fl-color-picker-color'),
                    startHex = '#' + wrapper.find('.fl-color-picker-value').val();
                    
                picker.css('background-color', startHex);
            
                picker.ColorPicker({
        			color: startHex,
        			onShow: function (dialog) {
        				$(dialog).fadeIn(500);
        				return false;
        			},
        			onHide: function (dialog) {
        				$(dialog).fadeOut(500);
        				return false;
        			},
        			onChange: function (hsb, hex, rgb) {
                        wrapper.removeClass('fl-empty');
        				wrapper.find('.fl-color-picker-value').val(hex);
        				picker.css('background-color', '#' + hex);			
        			}
        		});
        		
        		wrapper.find('.fl-color-picker-clear').on('click', FLThemeSettings._clearColorPicker);
            });
        },
        
        /**
	     * @method _clearColorPicker
	     */
        _clearColorPicker: function()
        {
            var button = $(this);
                
            button.siblings('.fl-color-picker-color').css('background-color', 'transparent');
            button.siblings('.fl-color-picker-value').val('');
            button.parent().addClass('fl-empty');
        },
        
        /**
         * @method _initEditors
         */ 
        _initEditors: function()
        {
	        $('textarea[data-editor]').each(function() {
	        
	        	var textarea = $(this), 
	        		mode     = textarea.data('editor'), 
	        		editDiv  = $('<div>', {
		                position: 	'absolute',
		                height: 	parseInt(textarea.attr('rows'), 10) * 20
		            }), 
	        		editor = null;

				editDiv.insertBefore(textarea);
	            textarea.css('display', 'none');
	 
	            editor = ace.edit(editDiv[0]);
	            editor.getSession().setValue(textarea.val());
	            editor.getSession().setMode('ace/mode/' + mode);
	            
	            textarea.closest('form').submit(function() {
	                textarea.val(editor.getSession().getValue());
	            });
	        });
        },
        
        /**
         * @method _settingsSelectChanged
         */ 
        _selectChanged: function()
        {
            var val    = null,
                toggle = $(this).attr('data-toggle'),
                i      = 0,
                k      = 0;
            
            if(typeof toggle !== 'undefined') {
                
                val    = $(this).val();
                toggle = JSON.parse(toggle);
                
                for(i in toggle) {
                    if(typeof toggle[i].fields !== 'undefined') {
                        for(k = 0; k < toggle[i].fields.length; k++) {
                            $('#fl-field-' + toggle[i].fields[k]).hide();
                        }
                    }
                    if(typeof toggle[i].sections !== 'undefined') {
                        for(k = 0; k < toggle[i].sections.length; k++) {
                            $('#fl-' + toggle[i].sections[k] + '-settings-section').hide();
                        }
                    }
                }
                
                if(typeof toggle[val] !== 'undefined') {
                    if(typeof toggle[val].fields !== 'undefined') {
                        for(i = 0; i < toggle[val].fields.length; i++) {
                            $('#fl-field-' + toggle[val].fields[i]).show();
                        }
                    }
                    if(typeof toggle[val].sections !== 'undefined') {
                        for(i = 0; i < toggle[val].sections.length; i++) {
                            $('#fl-' + toggle[val].sections[i] + '-settings-section').show();
                        }
                    }
                }
            }
        },
        
        /**
         * @method _exportSettings
         */ 
        _exportSettings: function()
        {
        	var href = window.location.href.split('#').shift() + '&fl-export';
        	
        	window.location.href = href;
        },
        
        /**
         * @method _importSettings
         */ 
        _importSettings: function()
        {
        	if($('input[name=import_file]').val() == '') {
	        	alert('Please select an import file.');
        	}
        	else {
	        	FLThemeSettings._saveSettings();
        	}
        }
    };

})(jQuery);