<?php
/*
Template Name: Proposal Summary
*/

?>
<?php
    function builder_scripts() {
    	wp_enqueue_script(
    		'fl-script',
    		'http://client.relentpress.com/template/wp-content/uploads/sites/2/fl-builder/77-layout.js?ver=fd15488c57a529130364418e844cbe48',
    		array( 'jquery' )
    	);
    }
    
    add_action( 'wp_enqueue_scripts', 'builder_scripts' );
    
    function my_global_builder_posts($post_ids) {
        $post_ids[] = '77';
        $post_ids[] = '28';
        $post_ids[] = '79';
        $post_ids[] = '81';
        $post_ids[] = '83';
        $post_ids[] = '85';
        return $post_ids;
        
        
    }
    
    add_filter('fl_builder_global_posts', 'my_global_builder_posts');
    
    
?>



<?php get_header(); ?>

<script type="text/javascript" src="http://client.relentpress.com/template/wp-content/uploads/sites/2/fl-builder/77-layout.js?ver=a77681aeb3cedd203a4fcb85efb6c81f"></script>
<link rel="stylesheet" id="fl-builder-layout-77-css" href="http://client.relentpress.com/template/wp-content/uploads/sites/2/fl-builder/77-layout.css?ver=a77681aeb3cedd203a4fcb85efb6c81f" type="text/css" media="all">
<div class="fl-content-full container">
    <div class="row">
        <div class="fl-content col-md-12">
        <?php if(function_exists('pf_show_link')){echo pf_show_link();} ?>
        <?php
            
            $query = new WP_Query('page_id=28' );
            
            if ( $query->have_posts() ) :
            
            	
            	 while ( $query->have_posts() ) : $query->the_post();
            		?>	
            		<article class="fl-post" id="fl-post-<?php the_ID(); ?>" itemscope="itemscope" itemtype="http://schema.org/CreativeWork">

                	
                    
                    <div class="fl-post-content clearfix" itemprop="text">
                        
                        <?php 
                            
                            the_content(); 
                        
                			wp_link_pages(array(
                                'before' => '<div class="fl-post-page-nav">' . __('Pages:', 'fl-automator'), 
                                'after' => '</div>', 
                                'next_or_number' => 'number'
                            ));
                		?>
                    </div><!-- .fl-post-content -->
                    
                    <?php comments_template(); ?>
                    
                </article>
                <!-- .fl-post -->
                <?php
            	 endwhile; 
            	
            	wp_reset_postdata();
            
            endif;
            
            $exclude_ids = array( 100 );
            $args = array(
	
            	 'post_type' => 'page',
            	 'post_parent' => 28,
            	 'post__not_in' => $exclude_ids,
            	 'orderby' => 'menu_order', 'order' => 'ASC'
            );
            
            $the_query = new WP_Query( $args );
            
            if ( $the_query->have_posts() ) :
            
            	
            	 while ( $the_query->have_posts() ) : $the_query->the_post();
            	?>	
            		<article class="fl-post" id="fl-post-<?php the_ID(); ?>" itemscope="itemscope" itemtype="http://schema.org/CreativeWork">

                	
                    <div class="fl-post-content clearfix" itemprop="text">
                        <?php 
                            the_content(); 
                        
                			wp_link_pages(array(
                                'before' => '<div class="fl-post-page-nav">' . __('Pages:', 'fl-automator'), 
                                'after' => '</div>', 
                                'next_or_number' => 'number'
                            ));
                		?>
                    </div><!-- .fl-post-content -->
                    
                    <?php comments_template(); ?>
                    
                </article>
                <!-- .fl-post -->
                <?php
            	 endwhile; 
            	
            	wp_reset_postdata();
            
            else :
            
                echo '<p>Sorry, no posts matched your criteria.</p>';
            
            endif;
        ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>