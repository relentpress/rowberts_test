
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;
INSERT INTO `wp_usermeta` VALUES (1,1,'first_name','Christopher'),(2,1,'last_name','Roberts'),(3,1,'nickname','relentpress'),(4,1,'description',''),(5,1,'rich_editing','true'),(6,1,'comment_shortcuts','false'),(7,1,'admin_color','fresh'),(8,1,'use_ssl','0'),(9,1,'show_admin_bar_front','true'),(10,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(11,1,'wp_user_level','10'),(12,1,'dismissed_wp_pointers','wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),(13,1,'show_welcome_panel','0'),(14,1,'wp_user-settings','editor=tinymce&hidetb=0&wplink=1&libraryContent=browse'),(15,1,'wp_user-settings-time','1416221634'),(16,1,'wp_dashboard_quick_press_last_post_id','15'),(17,1,'source_domain','client.relentpress.com'),(18,1,'primary_blog','1'),(19,2,'first_name',''),(20,2,'last_name',''),(21,2,'nickname','template'),(22,2,'description',''),(23,2,'rich_editing','true'),(24,2,'comment_shortcuts','false'),(25,2,'admin_color','fresh'),(26,2,'use_ssl','0'),(27,2,'show_admin_bar_front','true'),(30,2,'dismissed_wp_pointers','wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),(31,2,'show_welcome_panel','2'),(32,2,'primary_blog',''),(33,2,'source_domain',''),(36,1,'wp_2_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(37,1,'wp_2_user_level','10'),(38,1,'wp_2_user-settings','editor=tinymce&hidetb=0&wplink=1&libraryContent=browse'),(39,1,'wp_2_user-settings-time','1416221673'),(40,1,'wp_2_dashboard_quick_press_last_post_id','208'),(41,1,'managenav-menuscolumnshidden','a:4:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),(42,1,'metaboxhidden_nav-menus','a:4:{i:0;s:8:\"add-post\";i:1;s:13:\"add-portfolio\";i:2;s:12:\"add-post_tag\";i:3;s:11:\"add-frm_tag\";}'),(43,1,'nav_menu_recently_edited','4'),(44,1,'closedpostboxes_dashboard-network','a:0:{}'),(45,1,'metaboxhidden_dashboard-network','a:2:{i:0;s:27:\"network_dashboard_right_now\";i:1;s:17:\"dashboard_primary\";}'),(46,1,'closedpostboxes_page','a:2:{i:0;s:22:\"genesis_inpost_seo_box\";i:1;s:25:\"genesis_inpost_layout_box\";}'),(47,1,'metaboxhidden_page','a:7:{i:0;s:16:\"acf_acf_overview\";i:1;s:18:\"acf_acf_milestones\";i:2;s:12:\"revisionsdiv\";i:3;s:11:\"postexcerpt\";i:4;s:10:\"postcustom\";i:5;s:7:\"slugdiv\";i:6;s:9:\"authordiv\";}'),(48,3,'first_name','Nick'),(49,3,'last_name','Bown'),(50,3,'nickname','customer'),(51,3,'description',''),(52,3,'rich_editing','true'),(53,3,'comment_shortcuts','false'),(54,3,'admin_color','fresh'),(55,3,'use_ssl','0'),(56,3,'show_admin_bar_front','false'),(59,3,'dismissed_wp_pointers','wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),(60,3,'primary_blog','2'),(61,3,'source_domain','client.relentpress.com'),(62,3,'wp_2_capabilities','a:1:{s:10:\"subscriber\";b:1;}'),(63,3,'wp_2_user_level','0'),(64,3,'genesis_admin_menu','1'),(65,3,'genesis_seo_settings_menu','1'),(66,3,'genesis_import_export_menu','1'),(67,3,'genesis_author_box_single',''),(68,3,'genesis_author_box_archive',''),(69,3,'headline',''),(70,3,'intro_text',''),(71,3,'doctitle',''),(72,3,'meta_description',''),(73,3,'meta_keywords',''),(74,3,'noindex',''),(75,3,'nofollow',''),(76,3,'noarchive',''),(77,3,'layout',''),(78,3,'googleplus',''),(79,3,'LatestComments','a:2:{i:0;s:6:\"2, 107\";i:1;s:5:\"2, 11\";}'),(82,1,'wp_3_user-settings','editor=tinymce&hidetb=0&wplink=1&libraryContent=browse'),(83,1,'wp_3_user-settings-time','1409562689'),(84,1,'wp_3_dashboard_quick_press_last_post_id','153'),(85,1,'panorama_acf_ignore','true'),(86,1,'panorama_ignore_notice','true'),(87,1,'session_tokens','a:1:{s:64:\"7314b693923234ff9a1dd31c266000221baf92dcc23a49abd133c7b2d2ed7986\";i:1418448381;}'),(90,1,'wp_4_user-settings','editor=tinymce&hidetb=0&wplink=1&libraryContent=browse'),(91,1,'wp_4_user-settings-time','1413107219'),(92,1,'wp_4_dashboard_quick_press_last_post_id','3'),(93,1,'genesis_admin_menu','1'),(94,1,'genesis_seo_settings_menu','1'),(95,1,'genesis_import_export_menu','1'),(96,1,'genesis_author_box_single',''),(97,1,'genesis_author_box_archive',''),(98,1,'headline',''),(99,1,'intro_text',''),(100,1,'doctitle',''),(101,1,'meta_description',''),(102,1,'meta_keywords',''),(103,1,'noindex',''),(104,1,'nofollow',''),(105,1,'noarchive',''),(106,1,'layout',''),(107,1,'googleplus','');
/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

