
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_2_frm_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_2_frm_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_key` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `ip` text,
  `form_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parent_item_id` int(11) DEFAULT NULL,
  `is_draft` tinyint(1) DEFAULT '0',
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_key` (`item_key`),
  KEY `form_id` (`form_id`),
  KEY `post_id` (`post_id`),
  KEY `user_id` (`user_id`),
  KEY `parent_item_id` (`parent_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_2_frm_items` WRITE;
/*!40000 ALTER TABLE `wp_2_frm_items` DISABLE KEYS */;
INSERT INTO `wp_2_frm_items` VALUES (1,'g2m6o4','Test','a:2:{s:7:\"browser\";s:76:\"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0\";s:8:\"referrer\";s:60:\"http://client.relentpress.com/template/content/add-a-page/\r\n\";}','112.198.82.132',6,49,1,NULL,0,1,'2014-07-27 07:49:54','2014-07-27 08:05:36'),(2,'joa44d','test 2','a:2:{s:7:\"browser\";s:76:\"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0\";s:8:\"referrer\";s:60:\"http://client.relentpress.com/template/content/add-a-page/\r\n\";}','112.198.82.132',6,55,1,NULL,0,1,'2014-07-27 08:01:53','2014-07-27 08:01:53'),(3,'haud70','haud70','a:2:{s:7:\"browser\";s:76:\"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0\";s:8:\"referrer\";s:62:\"http://client.relentpress.com/template/content/add-document/\r\n\";}','112.198.82.132',14,0,1,NULL,0,1,'2014-07-27 08:25:22','2014-07-27 08:25:22'),(4,'kyc2hb','test 3','a:2:{s:7:\"browser\";s:76:\"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0\";s:8:\"referrer\";s:58:\"http://client.relentpress.com/template/content/add-page/\r\n\";}','112.198.82.132',6,76,1,NULL,0,1,'2014-07-27 09:39:32','2014-07-28 05:25:05'),(5,'nbvv5p','test pdf','a:2:{s:7:\"browser\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36\";s:8:\"referrer\";s:71:\"http://client.relentpress.com/template/project-overview/add-document/\r\n\";}','222.127.118.53',14,0,1,NULL,0,1,'2014-07-28 05:20:31','2014-07-28 05:20:31'),(6,'8fukf9','Test Post','a:2:{s:7:\"browser\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36\";s:8:\"referrer\";s:67:\"http://client.relentpress.com/template/project-overview/add-post/\r\n\";}','222.127.118.53',7,120,1,NULL,0,1,'2014-07-28 06:46:19','2014-07-28 06:46:19'),(8,'proposal-summary','Proposal Summary','Copied from Post',NULL,6,181,1,NULL,0,NULL,'2014-11-12 08:53:43','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `wp_2_frm_items` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

