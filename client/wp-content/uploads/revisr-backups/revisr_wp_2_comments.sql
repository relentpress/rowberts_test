
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_2_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_2_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_2_comments` WRITE;
/*!40000 ALTER TABLE `wp_2_comments` DISABLE KEYS */;
INSERT INTO `wp_2_comments` VALUES (2,107,'Nick Bown','test@relentpress.com','','222.127.118.53','2014-07-28 15:00:07','2014-07-28 05:00:07','Test discusion lkdj lkja ds;lfnasfd dsf',0,'1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:31.0) Gecko/20100101 Firefox/31.0','',0,3),(3,107,'Nick Bown','test@relentpress.com','','222.127.118.53','2014-07-28 15:01:02','2014-07-28 05:01:02','Aesthetic skateboard direct trade, bitters fanny pack Williamsburg seitan Echo Park authentic readymade bespoke normcore. Flannel plaid food truck YOLO cardigan Wes Anderson, photo booth kale chips viral. Farm-to-table cardigan flannel flexitarian. Wolf fingerstache brunch Odd Future umami Austin. Gentrify direct trade Wes Anderson tousled aesthetic tofu. Blog Vice synth mlkshk Brooklyn next level. YOLO kale chips American Apparel VHS cred church-key, flexitarian photo booth locavore Truffaut tofu.\r\n\r\nSriracha organic flannel, slow-carb paleo four loko bespoke VHS +1 plaid butcher. Pinterest food truck slow-carb, yr ethnic locavore Carles lomo dreamcatcher occupy 90\'s cliche. Butcher McSweeney\'s wayfarers fashion axe keffiyeh Portland. Bicycle rights post-ironic cred, Tonx Kickstarter Godard chillwave retro mixtape. Vice seitan squid tattooed mumblecore fashion axe, selvage drinking vinegar authentic actually occupy. American Apparel Schlitz Cosby sweater distillery, normcore YOLO disrupt 8-bit paleo Tonx drinking vinegar art party 3 wolf moon squid gastropub. Fanny pack church-key tattooed dreamcatcher.',0,'1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:31.0) Gecko/20100101 Firefox/31.0','',0,3);
/*!40000 ALTER TABLE `wp_2_comments` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

