
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_revisr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_revisr` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message` text,
  `event` varchar(42) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_revisr` WRITE;
/*!40000 ALTER TABLE `wp_revisr` DISABLE KEYS */;
INSERT INTO `wp_revisr` VALUES (1,'2014-11-19 06:10:33','Successfully backed up the database.','backup'),(2,'2014-11-19 06:10:36','Successfully pushed 2 commits to origin/master.','push'),(3,'2014-11-19 06:10:36','Commmitted <a href=\"http://client.relentpress.com/wp-admin/post.php?post=10&action=edit\">#7b82594</a> to the local repository.','commit'),(4,'2014-11-19 06:10:36','Successfully pushed 0 commits to origin/master.','push'),(5,'2014-11-19 07:58:45','Commmitted <a href=\"http://client.relentpress.com/wp-admin/post.php?post=12&action=edit\">#d3d8f3f</a> to the local repository.','commit'),(6,'2014-11-19 07:58:47','Successfully pushed 1 commit to origin/master.','push'),(7,'2014-11-24 08:34:05','Successfully backed up the database.','backup'),(8,'2014-11-24 08:34:08','Successfully pushed 2 commits to origin/master.','push'),(9,'2014-11-24 08:34:08','Commmitted <a href=\"http://client.relentpress.com/wp-admin/post.php?post=13&action=edit\">#c62b9d3</a> to the local repository.','commit'),(10,'2014-11-24 08:34:09','Successfully pushed 0 commits to origin/master.','push'),(11,'2014-11-24 08:42:30','Successfully backed up the database.','backup'),(12,'2014-11-24 08:42:32','Successfully pushed 2 commits to origin/master.','push'),(13,'2014-11-24 08:42:32','Commmitted <a href=\"http://client.relentpress.com/wp-admin/post.php?post=14&action=edit\">#3a16696</a> to the local repository.','commit'),(14,'2014-11-24 08:42:33','Successfully pushed 0 commits to origin/master.','push');
/*!40000 ALTER TABLE `wp_revisr` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

