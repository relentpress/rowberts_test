
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` VALUES (1,1,'2014-07-27 06:21:22','2014-07-27 06:21:22','Welcome to WordPress. This is your first post. Edit or delete it, then start blogging!','Hello world!','','publish','open','open','','hello-world','','','2014-07-27 06:21:22','2014-07-27 06:21:22','',0,'http://client.relentpress.com/?p=1',0,'post','',0),(2,1,'2014-07-27 06:21:22','2014-07-27 06:21:22','This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my blog. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://client.relentpress.com/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!','Sample Page','','publish','open','open','','sample-page','','','2014-07-27 06:21:22','2014-07-27 06:21:22','',0,'http://client.relentpress.com/?page_id=2',0,'page','',0),(4,1,'2014-07-07 04:20:12','2014-07-07 04:20:12','Add content here if you would like to use this as a listing page.','Single page','Used for the single post page','publish','closed','closed','','single-page','','','2014-07-07 04:20:12','2014-07-07 04:20:12','',0,'http://client.relentpress.com/blog/frm_display/single-page/',0,'frm_display','',0),(5,1,'2014-07-07 04:42:00','2014-07-07 04:42:00','<tr style=\"border-bottom: solid 1px #E3E3E3\">\n<td width=\"30%\" class=\"dirheadline\"><a href=\"[detaillink]\">[87]</a></td>\n<td width=\"20%\" class=\"dirstatus\">[88]</td>\n<td width=\"40%\" class=\"dirdate\">[updated-at]</td>\n<td width=\"10%\" class=\"diredit\">[editlink location=\"front\" label=\"Edit\" page_id=10]</td>\n</tr>','Content List','','publish','closed','closed','','content-list','','','2014-07-07 04:42:00','2014-07-07 04:42:00','',0,'http://client.relentpress.com/blog/frm_display/content-list/',0,'frm_display','',0),(6,1,'2014-07-07 23:26:44','2014-07-07 11:26:44','<tr style=\"border-bottom: solid 1px #E3E3E3\">\n<td width=\"30%\" class=\"dirheadline\"><a href=\"[detaillink]\">[89]</a></td>\n<td width=\"20%\" class=\"dirstatus\">[94]</td>\n<td width=\"40%\" class=\"dirdate\">[updated-at]</td>\n<td width=\"10%\" class=\"diredit\">[editlink location=\"front\" label=\"Edit\" page_id=76]</td>\n</tr>','Article List','Article List','publish','closed','closed','','content-list-2','','','2014-07-07 23:26:44','2014-07-07 11:26:44','',0,'http://client.relentpress.com/blog/frm_display/content-list-2/',0,'frm_display','',0),(7,1,'2014-07-08 00:24:41','2014-07-07 12:24:41','[95]\n\n<div class=\"editbuttons\">\n[editlink location=\"front\" label=\"Edit\" page_id=98]  \n</div>\n<hr />','Categories List','','publish','closed','closed','','93','','','2014-07-08 00:24:41','2014-07-07 12:24:41','',0,'http://client.relentpress.com/blog/frm_display/93/',0,'frm_display','',0),(8,1,'2014-07-08 10:33:13','2014-07-07 22:33:13','Add content here if you would like to use this as a listing page.','Single post','Used for the single post page','publish','closed','closed','','single-post-2','','','2014-07-08 10:33:13','2014-07-07 22:33:13','',0,'http://client.relentpress.com/blog/frm_display/single-post-2/',0,'frm_display','',0),(10,1,'2014-11-19 17:10:31','2014-11-19 06:10:31','','Update Config','','publish','closed','closed','','update-config','','','2014-11-19 17:10:31','2014-11-19 06:10:31','',0,'http://client.relentpress.com/?post_type=revisr_commits&#038;p=10',0,'revisr_commits','',0),(12,1,'2014-11-19 18:58:45','2014-11-19 07:58:45','','New Tools','','publish','closed','closed','','new-tools','','','2014-11-19 18:58:45','2014-11-19 07:58:45','',0,'http://client.relentpress.com/?post_type=revisr_commits&#038;p=12',0,'revisr_commits','',0),(13,1,'2014-11-24 19:33:52','2014-11-24 08:33:52','','WP Core Update','','publish','closed','closed','','wp-core-update','','','2014-11-24 19:33:53','2014-11-24 08:33:53','',0,'http://client.relentpress.com/?post_type=revisr_commits&#038;p=13',0,'revisr_commits','',0),(14,1,'2014-11-24 19:42:28','2014-11-24 08:42:28','','Update WP Config','','publish','closed','closed','','update-wp-config','','','2014-11-24 19:42:28','2014-11-24 08:42:28','',0,'http://client.relentpress.com/?post_type=revisr_commits&#038;p=14',0,'revisr_commits','',0),(15,1,'2014-12-11 16:26:23','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2014-12-11 16:26:23','0000-00-00 00:00:00','',0,'http://client.relentpress.com/?p=15',0,'post','',0),(16,1,'2014-12-11 16:31:00','2014-12-11 05:31:00','','Update Plugins','','publish','closed','closed','','update-plugins','','','2014-12-11 16:31:00','2014-12-11 05:31:00','',0,'http://client.relentpress.com/?post_type=revisr_commits&#038;p=16',0,'revisr_commits','',0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

