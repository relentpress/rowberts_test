<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rowberts_charity');

	define('DB_USER', 'rowberts_staging');
	define('DB_PASSWORD', 'Ma&7HZXuxR');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 */
define('AUTH_KEY',         '0 DGACfSsZ0N.0G9O+s]AH-&o=lx(UF9eP:bJTcq!Y+rd?-eY~m&D4I>H6H2.;2C');
define('SECURE_AUTH_KEY',  'H5O$G?sE(Kepo^t7K+wp5+sPs6OU](b;nz}MbPBdkjy?D2A]5gFCPJSry]-yz`T.');
define('LOGGED_IN_KEY',    '78G0u`C.Nxe5|mjE6SJUc/O(`*kJS$;;d-0kV+VU*GpSns<W*4PpkD0=s0IU~3BU');
define('NONCE_KEY',        'Zv}cKe}?{L-7w:^7Oic-N-6oT?EC})=.teom,u]~^!JEuK)D@Z0L_4{z(|oylAVc');
define('AUTH_SALT',        '+sM|-r*:h5q]MUViV`4p4)Y~Fs@:a2Tb0v|,jAhYjmA;d]n4$qGS/Is*8@^[@;1F');
define('SECURE_AUTH_SALT', '2QU0&PnOK Nq1r7Y% BD*-/c8|L<e-hfT|E|~8v&OQJM/,7T?Sv+ O4X?c+Pj{Qm');
define('LOGGED_IN_SALT',   'M0j0e2$W_^#nU$_KY!S+.71O|P,mP@w2@+~lR6F7iWZqJh};31|$|<=0tmvAy19=');
define('NONCE_SALT',       'VTAd=y{}!1J,?MA[!Vj9,&F%llazT40}:(B-Sd_$;N~HnE++bszkkdTREr#3/skb');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/*Enable Auto Updates for WP Core (Major), Themes & Plugins by CER */
define('WP_AUTO_UPDATE_CORE', true);
add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );
