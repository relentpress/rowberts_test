<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */


/** VARIABLE CONNECTION STRINGS BASED ON SERVER **/

/** Developer One **/
if( stristr( $_SERVER['SERVER_NAME'], "beaver.local.dev" ) ) {

	// Dev Environment
	define( 'DB_NAME', 'beaver' );
	define( 'DB_USER', 'root' );
	define( 'DB_PASSWORD', 'root' );
	define( 'DB_HOST', 'localhost' );
	
	//define( 'WP_HOME', 'http://beaver.local.dev');
	//define( 'WP_SITEURL', WP_HOME);
	
	// Dev will always want debug on and caching off
	define( 'WP_DEBUG', true );
	define( 'WP_CACHE', false );	 
} 

/** Developer Two **/
elseif( stristr( $_SERVER['SERVER_NAME'], "localhost" ) ) {
 
	// Staging Environment
	define( 'DB_NAME', 'beaver' );
	define( 'DB_USER', 'root' );
	define( 'DB_PASSWORD', '' );
	define( 'DB_HOST', 'localhost' );
	
	//define( 'WP_HOME', 'http://localhost/beaver'); 
	//define( 'WP_SITEURL', WP_HOME);
	
	// Dev will always want debug on and caching off
	define( 'WP_DEBUG', true );
	define( 'WP_CACHE', false );		 
} 

/** Staging **/
elseif( stristr( $_SERVER['SERVER_NAME'], "beaver.sg.rowberts.com.au" ) ) {
 
	// Staging Environment
	define( 'DB_NAME', 'rowberts_beaver' );
	define( 'DB_USER', 'rowberts_staging' );
	define( 'DB_PASSWORD', 'Ma&7HZXuxR' );
	define( 'DB_HOST', 'localhost' );
	
	//define( 'WP_HOME', 'http://beaver.sg.rowberts.com.au'); 
	//define( 'WP_SITEURL', WP_HOME);
	
	// UAT Environment will always be the same as production so turn off debug and turn on caching
	define( 'WP_DEBUG', false );
	define( 'WP_CACHE', true );		 
} 

/** Live **/
else {
 
	// Production Environment
	define( 'DB_NAME', 'project_live' );
	define( 'DB_USER', 'project_live_user' );
	define( 'DB_PASSWORD', 'password' );
	define( 'DB_HOST', 'localhost' );
	
	//define( 'WP_HOME', 'http://project.com'); 
	//define( 'WP_SITEURL', WP_HOME); 
	
	// Live Environment will always be the same as production so turn off debug and turn on caching
	define( 'WP_DEBUG', false );
	define( 'WP_CACHE', true );
		
}



/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'BdRh@mX~|nPa|9><l5G-]805y~/;gf_ao+f![].9H%eeC?GP;EDV/cs%!+}-c1eF');
define('SECURE_AUTH_KEY',  '~y`WDI!yLSjd5*1nizB=a^xS1qEX1Cy_kp}l;DJe- Dt~; 02]U`-H4Y|^_15|hL');
define('LOGGED_IN_KEY',    '`vckVq|ix;h (9i-`:z7@(79&F)CV-w}s]+#Woo,5</rR+MxAI+6J*`!VVffc<M-');
define('NONCE_KEY',        '{R~n/Ea{e1eE[k`CV~NAwbx=H^OQixiv+,0o^}eFtJOw6~+r&*yZy`cZ@UY-?s :');
define('AUTH_SALT',        '7SsvCiDB,$f]%#vP7cpMW}z-KU+hgL+8zU~&Mogpa|}Ho!q_@sJ1&xGIT+nuv.;X');
define('SECURE_AUTH_SALT', 'VG:S%2$/A|:8I7mMai>d:Qe;gek5,h<T[gQTP+TulqHp-uqGI*XQj18b`l0<I`#a');
define('LOGGED_IN_SALT',   '(l%TYSp|?LXw!V>=U0Le}pnWUGTmld`O+Fh+r4l?ZE3bzj-^+UGBqZe9}S3O]ABX');
define('NONCE_SALT',       '3.<bVeLXj[-%|!T(}2bVgajW6bS56?M.zd:V|.NU2k!9VM:T{k5d)ye6G<nD|q;m');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
