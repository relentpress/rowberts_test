
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_iwp_backup_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_iwp_backup_status` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `historyID` int(11) NOT NULL,
  `taskName` varchar(255) NOT NULL,
  `action` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `stage` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `finalStatus` varchar(50) DEFAULT NULL,
  `statusMsg` varchar(255) NOT NULL,
  `requestParams` text NOT NULL,
  `responseParams` longtext,
  `taskResults` text,
  `startTime` int(11) DEFAULT NULL,
  `endTime` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_iwp_backup_status` WRITE;
/*!40000 ALTER TABLE `wp_iwp_backup_status` DISABLE KEYS */;
INSERT INTO `wp_iwp_backup_status` VALUES (1,25387,'2d44d7286aa20bcca5beded15cc6e1e7','schedule','scheduleBackup','full','finished','completed','0','completed','a:5:{s:9:\"task_name\";s:32:\"2d44d7286aa20bcca5beded15cc6e1e7\";s:9:\"mechanism\";s:10:\"singleCall\";s:4:\"args\";a:15:{s:4:\"type\";s:14:\"scheduleBackup\";s:6:\"action\";s:8:\"schedule\";s:4:\"what\";s:4:\"full\";s:15:\"optimize_tables\";s:0:\"\";s:7:\"exclude\";a:1:{i:0;s:0:\"\";}s:17:\"exclude_file_size\";i:10;s:18:\"exclude_extensions\";s:13:\"eg. .zip,.mp4\";s:7:\"include\";a:1:{i:0;s:0:\"\";}s:13:\"del_host_file\";s:1:\"1\";s:12:\"disable_comp\";s:1:\"1\";s:12:\"fail_safe_db\";N;s:15:\"fail_safe_files\";N;s:5:\"limit\";s:2:\"12\";s:11:\"backup_name\";s:46:\"Internal - Weekly (Mon) @ 1pm UTC (23:00 AEST)\";s:9:\"parentHID\";i:25387;}s:8:\"username\";s:11:\"relentpress\";s:12:\"account_info\";a:1:{s:13:\"iwp_amazon_s3\";a:6:{s:14:\"as3_access_key\";s:20:\"AKIAJI3VH3OBF6CN7UOQ\";s:14:\"as3_secure_key\";s:40:\"aM6GlQO9x6VGAfTT6fG1exfq4tPXIFf5XxVfRZQ4\";s:10:\"as3_bucket\";s:11:\"relentpress\";s:17:\"as3_bucket_region\";s:16:\"s3.amazonaws.com\";s:13:\"as3_directory\";s:10:\"wp_backups\";s:15:\"as3_site_folder\";s:1:\"1\";}}}',NULL,'a:2:{s:15:\"backhack_status\";a:6:{s:14:\"adminHistoryID\";i:25387;s:7:\"db_dump\";a:2:{s:5:\"start\";d:1415861269.3054819;s:3:\"end\";d:1415861269.460731;}s:6:\"db_zip\";a:2:{s:5:\"start\";d:1415861269.4643979;s:3:\"end\";d:1415861269.486136;}s:9:\"files_zip\";a:2:{s:5:\"start\";d:1415861269.8722391;s:3:\"end\";d:1415861274.679172;}s:9:\"amazon_s3\";a:2:{s:5:\"start\";d:1415861274.691736;s:3:\"end\";d:1415861276.846107;}s:8:\"finished\";a:1:{s:3:\"end\";d:1415861276.8546009;}}s:12:\"task_results\";a:1:{i:25387;a:6:{s:4:\"size\";s:8:\"14.71 MB\";s:11:\"backup_name\";s:46:\"Internal - Weekly (Mon) @ 1pm UTC (23:00 AEST)\";s:9:\"mechanism\";s:10:\"singleCall\";s:4:\"time\";i:1415861274;s:14:\"adminHistoryID\";i:25387;s:8:\"amazons3\";s:93:\"beaver.sg.rowberts.com.au_scheduleBackup_full_2014-11-13_359877128de0027239a11d845270e37f.zip\";}}}',1415861269,0),(2,25809,'2d44d7286aa20bcca5beded15cc6e1e7','schedule','scheduleBackup','full','finished','completed','0','completed','a:5:{s:9:\"task_name\";s:32:\"2d44d7286aa20bcca5beded15cc6e1e7\";s:9:\"mechanism\";s:10:\"singleCall\";s:4:\"args\";a:15:{s:4:\"type\";s:14:\"scheduleBackup\";s:6:\"action\";s:8:\"schedule\";s:4:\"what\";s:4:\"full\";s:15:\"optimize_tables\";s:0:\"\";s:7:\"exclude\";a:1:{i:0;s:0:\"\";}s:17:\"exclude_file_size\";i:10;s:18:\"exclude_extensions\";s:13:\"eg. .zip,.mp4\";s:7:\"include\";a:1:{i:0;s:0:\"\";}s:13:\"del_host_file\";s:1:\"1\";s:12:\"disable_comp\";s:1:\"1\";s:12:\"fail_safe_db\";N;s:15:\"fail_safe_files\";N;s:5:\"limit\";s:2:\"12\";s:11:\"backup_name\";s:46:\"Internal - Weekly (Mon) @ 1pm UTC (23:00 AEST)\";s:9:\"parentHID\";i:25809;}s:8:\"username\";s:11:\"relentpress\";s:12:\"account_info\";a:1:{s:13:\"iwp_amazon_s3\";a:6:{s:14:\"as3_access_key\";s:20:\"AKIAJI3VH3OBF6CN7UOQ\";s:14:\"as3_secure_key\";s:40:\"aM6GlQO9x6VGAfTT6fG1exfq4tPXIFf5XxVfRZQ4\";s:10:\"as3_bucket\";s:11:\"relentpress\";s:17:\"as3_bucket_region\";s:16:\"s3.amazonaws.com\";s:13:\"as3_directory\";s:10:\"wp_backups\";s:15:\"as3_site_folder\";s:1:\"1\";}}}',NULL,'a:2:{s:15:\"backhack_status\";a:6:{s:14:\"adminHistoryID\";i:25809;s:7:\"db_dump\";a:2:{s:5:\"start\";d:1416189605.073442;s:3:\"end\";d:1416189605.2192521;}s:6:\"db_zip\";a:2:{s:5:\"start\";d:1416189605.2225549;s:3:\"end\";d:1416189605.2439821;}s:9:\"files_zip\";a:2:{s:5:\"start\";d:1416189605.570075;s:3:\"end\";d:1416189610.3795459;}s:9:\"amazon_s3\";a:2:{s:5:\"start\";d:1416189610.382746;s:3:\"end\";d:1416189612.5184989;}s:8:\"finished\";a:1:{s:3:\"end\";d:1416189612.5264921;}}s:12:\"task_results\";a:1:{i:25809;a:6:{s:4:\"size\";s:8:\"14.71 MB\";s:11:\"backup_name\";s:46:\"Internal - Weekly (Mon) @ 1pm UTC (23:00 AEST)\";s:9:\"mechanism\";s:10:\"singleCall\";s:4:\"time\";i:1416189610;s:14:\"adminHistoryID\";i:25809;s:8:\"amazons3\";s:93:\"beaver.sg.rowberts.com.au_scheduleBackup_full_2014-11-17_eec482892c5b120c947503ee17821df6.zip\";}}}',1416189605,0);
/*!40000 ALTER TABLE `wp_iwp_backup_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

