
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` VALUES (1,1,'2014-10-02 03:01:27','2014-10-02 03:01:27','Welcome to WordPress. This is your first post. Edit or delete it, then start blogging!','Hello world!','','publish','open','open','','hello-world','','','2014-10-02 03:01:27','2014-10-02 03:01:27','',0,'http://beaver.local.dev/?p=1',0,'post','',1),(26,1,'2014-10-30 06:05:15','2014-10-30 06:05:15','','background-2','','inherit','open','open','','background-2','','','2014-10-30 06:05:15','2014-10-30 06:05:15','',0,'http://beaver.local.dev/wp-content/uploads/2014/10/background-2.jpg',0,'attachment','image/jpeg',0),(27,1,'2014-10-30 06:07:10','2014-10-30 06:07:10','','Home','','publish','open','open','','home','','','2014-10-30 06:07:10','2014-10-30 06:07:10','',0,'http://beaver.local.dev/?page_id=27',0,'page','',0),(28,1,'2014-10-30 06:07:10','2014-10-30 06:07:10','','Home','','inherit','open','open','','27-revision-v1','','','2014-10-30 06:07:10','2014-10-30 06:07:10','',27,'http://beaver.local.dev/?p=28',0,'revision','',0),(32,1,'2014-11-19 07:18:15','2014-11-19 07:18:15','','Plugin Updates','','publish','closed','closed','','plugin-updates','','','2014-11-19 07:18:15','2014-11-19 07:18:15','',0,'http://beaver.local.dev/?post_type=revisr_commits&#038;p=32',0,'revisr_commits','',0),(33,1,'2014-11-19 07:19:31','2014-11-19 07:19:31','','Remove Themes','','publish','closed','closed','','remove-themes','','','2014-11-19 07:19:31','2014-11-19 07:19:31','',0,'http://beaver.local.dev/?post_type=revisr_commits&#038;p=33',0,'revisr_commits','',0),(34,1,'2014-11-19 07:58:11','2014-11-19 07:58:11','','New Tools','','publish','closed','closed','','new-tools','','','2014-11-19 07:58:11','2014-11-19 07:58:11','',0,'http://beaver.local.dev/?post_type=revisr_commits&#038;p=34',0,'revisr_commits','',0),(40,1,'2014-11-19 08:30:45','2014-11-19 08:30:45','','Test Changes','','publish','closed','closed','','test-changes','','','2014-11-19 08:30:45','2014-11-19 08:30:45','',0,'http://beaver.local.dev/?post_type=revisr_commits&#038;p=40',0,'revisr_commits','',0),(42,1,'2014-11-19 08:47:35','2014-11-19 08:47:35','','Oak Ridge Test Pages','','publish','open','open','','oak-ridge-test-pages','','','2014-11-19 08:47:35','2014-11-19 08:47:35','',0,'http://beaver.local.dev/?page_id=42',0,'page','',0),(43,1,'2014-11-19 08:47:35','2014-11-19 08:47:35','','Oak Ridge Test Pages','','inherit','open','open','','42-revision-v1','','','2014-11-19 08:47:35','2014-11-19 08:47:35','',42,'http://beaver.local.dev/42-revision-v1/',0,'revision','',0),(44,1,'2014-11-19 08:47:58','2014-11-19 08:47:58','','Trade Tools','','publish','open','open','','trade-tools','','','2014-11-19 09:20:33','2014-11-19 09:20:33','',0,'http://beaver.local.dev/?page_id=44',0,'page','',0),(45,1,'2014-11-19 08:47:58','2014-11-19 08:47:58','','Trade Tools','','inherit','open','open','','44-revision-v1','','','2014-11-19 08:47:58','2014-11-19 08:47:58','',44,'http://beaver.local.dev/44-revision-v1/',0,'revision','',0),(47,1,'2014-11-19 09:20:10','2014-11-19 09:20:10','','Trade Tools','','publish','closed','closed','','acf_trade-tools','','','2014-11-19 09:20:10','2014-11-19 09:20:10','',0,'http://beaver.local.dev/?post_type=acf&#038;p=47',0,'acf','',0),(49,1,'2014-11-28 01:33:23','2014-11-28 01:33:23','','WP Core & Plugin Update','','publish','closed','closed','','wp-core-plugin-update','','','2014-11-28 01:33:23','2014-11-28 01:33:23','',0,'http://beaver.local.dev/?post_type=revisr_commits&#038;p=49',0,'revisr_commits','',0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

