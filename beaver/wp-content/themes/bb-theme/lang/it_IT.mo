��    {      �      �      �  
   �     �     �  
   �     �  	     	     3        O  	   \     f     s  
   |     �     �     �     �     �     �     �     �  
   �     �     �     	     		     	     	     	     2	     :	     A	     H	  .   Q	  g   �	     �	     �	     �	     �	     �	     
     
     
     .
     :
     I
  
   c
     n
  n   v
     �
     �
                 
   #     .  
   4  W   ?  
   �     �     �     �     �     �     �     �     �     
          4     9     I     N  
   Z     e     j     x     �     �     �  	   �     �  	   �     �     �     �     �     �  	   �                         %     :     J     ]     j     x     }     �  -   �  \   �          )     .     F  R   U     �     �     �     �     �     �                    )  $   5  	   Z     d     t  �   �  
   p     {  
   �     �  	   �  
   �  	   �  5   �               #     1  	   9     C     I     O     h     l     p     �     �     �     �     �     �     �     �  	   �     �     �                 B   (  }   k     �  	   �     �                         '     9     M     \     q     �  �   �               *     6     ;     I     U     ^  c   l  
   �     �     �     �               '     5     <     X     e     �     �     �     �     �     �     �     �     �  	   �     �     	          &     2     9     B     I     V     d     p     v     �     �     �     �     �     �     �  
               /   "  �   R     �     �     �       c        w  
   �     �     �     �  $   �  	   �     �            ,   +     X     d     t   % Comments &laquo; Newer Posts (Edit) (required) 1 Column 1 Comment 2 Columns <span class="fl-post-more">Read more &raquo;</span> Accent Color All Pages Archive for  Archives Attachment Bottom Boxed Browse Google Fonts By  CSS Call Us! 1-800-555-5555 Center Bottom Center Center Center Top Centered Choose Menu Code Color Comment Content Content Background Credits Custom Design Disabled ERROR: Please upload an Automator export file. ERROR: The export file has been corrupted. Please export your settings and trying uploading them again. Edit Enabled Fill Fit Fixed Footer Footer Background Footer Column %d Footer Menu Footer Widgets Footer Widgets Background Full Width General Get started by selecting a skin for your theme. Customize it further using the settings in the design section. Header Background Header Menu Heading Font Home Homepage Only Horizontal Image Image File It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. JavaScript Layout Leave a Comment Leave a comment Left Bottom Left Center Left Top Log out Log out of this account Logged in as Mail (will not be published) Menu Menu Background Name No Comments No Sidebar None Nothing Found Older Posts &raquo; Pages: Position Posts Tagged  Posts by  Primary Sidebar Read More Repeat Reply Right Right Bottom Right Center Right Top Scale Scroll Search Search Icon Search results for:  Settings Saved! Settings imported! Sidebar Left Sidebar Right Size Skin Social Icons Sorry! That page doesn&rsquo;t seem to exist. Sorry, but nothing matched your search terms. Please try again with some different keywords. Submit Comment Text Text &amp; Social Icons Theme Settings This will be used by your chosen skin to color elements such as links and buttons. Tile Top Bar Top Bar Column 1 Top Bar Column 2 Top Bar Menu Type and press Enter to search. Vertical Website WooCommerce Sidebar You must be Your comment is awaiting moderation. logged in on %1$s at %2$s to post a comment. Project-Id-Version: Automator v1.1.3
PO-Revision-Date: 2014-09-04 10:39:36+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: CSL v1.x % Commenti &laquo; Post più recenti (Modifica) (obbligatorio) 1 colonna 1 commento 2 colonne <span class="fl-post-more">Leggi tutto &raquo;</span> Colore accento Tutte le pagine Archivio per  Archivi Posizione Basso Boxed Sfoglia i font di Google Di  CSS Chiamaci! 1-800-555-5555 Centro Basso Centro Centro Centro Alto Centrato Scegliere il Menu Codice Colore Commento Contenuto Sfondo contenuto Credits Personalizzato Design Disabilitato ERRORE: Si prega di caricare un file di esportazione di Automator. ERRORE: Il file di esportazione è stato danneggiato. Si prega di esportare le impostazioni e provare a caricarle nuovamente. Modifica Abilitato Riempi Contieni Fisso Footer Sfondo footer Footer colonna %d Menu piè di pagina Footer Widgets Sfondo widget footer Larghezza piena Generale Cominciate selezionando una skin per il vostro tema. Personalizzare ulteriormente utilizzando le impostazioni nella sezione design. Sfondo testata Menu testata Font titoli Home Solo homepage Orizzontale Immagine File immagine Sembra che non riusciamo a trovare quello che stai cercando. Forse una ricerca può essere d'aiuto. JavaScript Layout Lascia un commento Lascia un commento Sinistra Basso Sinistra Centro Sinistra Alto Logout Disconnettersi dall'account Loggato come Mail (non sarà pubblicata) Menu Sfondo menu Nome Nessun commento Nessuna sidebar Nessuno Nessun risultato Post più vecchi &raquo; Pagine: Posizione Tags Articoli Articoli di Sidebar primaria Leggi tutto Ripeti Rispondi Destra Destra Basso Destra Centro Destra Alto Scala Scorrimento Cerca Icona di ricerca Risultati della ricerca per:  Impostazioni salvate! Impostazioni importate! Sidebar sinistra Sidebar destra Dimensione Skin Icone social Mi dispiace! Quella pagina non sembra esistere. Siamo spiacenti, ma non ci sono corrispondenze con i termini di ricerca. Si prega di riprovare con alcune parole chiave diverse. Invia commento Testo Testo &amp; icone social Impostazioni tema Questo verrà utilizzato dalla skin selezionata per colorare elementi come collegamenti e pulsanti. Tassello Barra alta Colonna 1 barra alta Colonna 2 barra alta Menu barra superiore Digita e premi INVIO per la ricerca. Verticale Sito Web Sidebar WooCommerce È necessario essere Il tuo commento è in attesa di moderazione. autenticati in %1$s il %2$s per inviare un commento. 