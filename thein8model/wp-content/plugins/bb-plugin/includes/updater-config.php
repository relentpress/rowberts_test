<?php

if(class_exists('FLUpdater')) {
    FLUpdater::add_product(array(
        'name'      => 'Beaver Builder Plugin (Developer Version)', 
        'version'   => '1.4.0', 
        'slug'      => 'bb-plugin',
        'type'    	=> 'plugin'
    )); 
}