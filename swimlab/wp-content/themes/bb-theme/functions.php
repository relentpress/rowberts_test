<?php

/*

WARNING! DO NOT EDIT THEME FILES IF YOU PLAN ON UPDATING!

Theme files will be overwritten and your changes will be lost 
when updating. Instead, add custom code in the admin under 
Appearance > Theme Settings > Code or create a child theme.

*/

// Defines
define('FL_THEME_VERSION', '1.1.8');
define('FL_THEME_DIR', get_template_directory());
define('FL_THEME_URL', get_template_directory_uri());

// Classes
require_once 'classes/FLAdmin.php';
require_once 'classes/FLColor.php';
require_once 'classes/FLFonts.php';
require_once 'classes/FLTheme.php';

// Theme Actions
add_action('init',                  'FLTheme::init_woocommerce');
add_action('after_setup_theme',     'FLTheme::setup');
add_action('wp_enqueue_scripts',    'FLTheme::enqueue_scripts', 999);
add_action('widgets_init',          'FLTheme::widgets_init');

// Theme Filters
add_filter('body_class',            'FLTheme::body_class');
add_filter('excerpt_more',          'FLTheme::excerpt_more');

// Admin Actions
add_action('admin_init',            'FLAdmin::init');
add_action('admin_menu',            'FLAdmin::menu');