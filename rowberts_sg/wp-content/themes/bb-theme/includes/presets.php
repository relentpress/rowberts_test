<?php

$fl_presets = array(
	
	/**
	 * @name Default
	 */
	'default' 	=> array(
		'name'		=> 'Default',
		'skin'		=> 'default',
		'settings'	=> array(
			'accent_color' 				=> '428aca',
			'bg_color'					=> 'f2f2f2',
			'content_bg_color'			=> 'ffffff',
			'footer_bg_type' 			=> 'content',
			'footer_widgets_bg_type'	=> 'content',
			'header_bg_grad'			=> '0',
			'header_bg_type'			=> 'content',
			'heading_color'				=> '333333',
			'layout'  					=> 'full-width',
			'nav_bg_grad'				=> '0',
			'nav_bg_type'				=> 'content',
			'nav_position'				=> 'right',
			'text_color'				=> '808080',
			'top_bar_bg_grad' 			=> '0',
			'top_bar_bg_type' 			=> 'content'
		)
	),
	
	/**
	 * @name Default Dark
	 */
	'default-dark' => array(
		'name'		=> 'Default Dark',
		'skin'		=> 'default-dark',
		'settings'	=> array(
			'accent_color' 				=> '95bf48',
			'bg_color'					=> 'ffffff',
			'content_bg_color'			=> 'ffffff',
			'footer_bg_type' 			=> 'content',
			'footer_widgets_bg_type'	=> 'content',
			'header_bg_color'			=> '282a2e',
			'header_bg_grad'			=> '0',
			'header_bg_type'			=> 'custom',
			'heading_color'				=> '95bf48',
			'layout'  					=> 'full-width',
			'nav_bg_grad'				=> '0',
			'nav_bg_type'				=> 'content',
			'nav_position'				=> 'right',
			'text_color'				=> '808080',
			'top_bar_bg_grad' 			=> '0',
			'top_bar_bg_type' 			=> 'content'
		)
	),
	
	/**
	 * @name Classic
	 */
	'classic' 		=> array(
		'name'		=> 'Classic',
		'skin'		=> 'classic',
		'settings'	=> array(
			'accent_color' 				=> '483182',
			'bg_color'					=> 'efefe9',
			'content_bg_color'			=> 'ffffff',
			'footer_bg_color' 			=> '',
			'footer_bg_type' 			=> 'content',
			'footer_widgets_bg_color'	=> '',
			'footer_widgets_bg_type' 	=> 'content',
			'header_bg_color'			=> '',
			'header_bg_grad'			=> '0',
			'header_bg_type' 			=> 'content',
			'heading_color'				=> '333333',
			'layout'  					=> 'boxed',
			'nav_bg_color'				=> '483182',
			'nav_bg_grad'				=> '0',
			'nav_bg_type' 				=> 'custom',
			'nav_position'				=> 'bottom',
			'text_color'				=> '808080',
			'top_bar_bg_color' 			=> '',
			'top_bar_bg_grad' 			=> '0',
			'top_bar_bg_type' 			=> 'content'
		)
	),
	
	/**
	 * @name Modern
	 */
	'modern' 	=> array(
		'name'		=> 'Modern',
		'skin'		=> 'modern',
		'settings'	=> array(
			'accent_color' 				=> 'cf6713',
			'bg_color'					=> 'e6e6e6',
			'content_bg_color'			=> 'ffffff',
			'footer_bg_color' 			=> '333333',
			'footer_bg_type' 			=> 'custom',
			'footer_widgets_bg_color'	=> 'fafafa',
			'footer_widgets_bg_type' 	=> 'custom',
			'header_bg_type'			=> 'content',
			'header_bg_grad'			=> '0',
			'heading_color'				=> '333333',
			'layout'  					=> 'boxed',
			'nav_bg_color'				=> 'fafafa',
			'nav_bg_type' 				=> 'custom',
			'nav_bg_grad'				=> '1',
			'nav_position'				=> 'bottom',
			'text_color'				=> '808080',
			'top_bar_bg_color' 			=> '333333',
			'top_bar_bg_grad' 			=> '1',
			'top_bar_bg_type' 			=> 'custom'
		)
	),
	
	/**
	 * @name Bold
	 */
	'bold' 	    => array(
		'name'		=> 'Bold',
		'skin'		=> 'default',
		'settings'	=> array(
			'accent_color' 				=> '428aca',
			'bg_color'					=> 'f2f2f2',
			'content_bg_color'			=> 'ffffff',
			'footer_bg_color' 			=> '428aca',
			'footer_bg_type' 			=> 'custom',
			'footer_widgets_bg_color'	=> '326796',
			'footer_widgets_bg_type' 	=> 'custom',
			'header_bg_color'			=> '326796',
			'header_bg_grad'			=> '1',
			'header_bg_type' 			=> 'custom',
			'heading_color'				=> '333333',
			'layout'  					=> 'full-width',
			'nav_bg_color'				=> '428aca',
			'nav_bg_grad'				=> '0',
			'nav_bg_type' 				=> 'custom',
			'nav_position'				=> 'bottom',
			'text_color'				=> '808080',
			'top_bar_bg_color' 			=> '326796',
			'top_bar_bg_grad' 			=> '0',
			'top_bar_bg_type' 			=> 'custom'
		)
	),
	
	/**
	 * @name Stripe
	 */
	'stripe' 	=> array(
		'name'		=> 'Stripe',
		'skin'		=> 'default',
		'settings'	=> array(
			'accent_color' 				=> '428aca',
			'bg_color'					=> 'e6e6e6',
			'content_bg_color'			=> 'ffffff',
			'footer_bg_color' 			=> '385f82',
			'footer_bg_type' 			=> 'custom',
			'footer_widgets_bg_color'	=> 'fafafa',
			'footer_widgets_bg_type' 	=> 'custom',
			'header_bg_color'			=> '',
			'header_bg_grad'			=> '0',
			'header_bg_type' 			=> 'content',
			'heading_color'				=> '333333',
			'layout'  					=> 'full-width',
			'nav_bg_color'				=> '385f82',
			'nav_bg_grad'				=> '0',
			'nav_bg_type' 				=> 'custom',
			'nav_position'				=> 'bottom',
			'text_color'				=> '808080',
			'top_bar_bg_color' 			=> 'fafafa',
			'top_bar_bg_grad' 			=> '0',
			'top_bar_bg_type' 			=> 'custom'
		)
	),
	
	/**
	 * @name Deluxe
	 */
	'deluxe' 		=> array(
		'name'		=> 'Deluxe',
		'skin'		=> 'deluxe',
		'settings'	=> array(
			'accent_color' 				=> '657f8c',
			'bg_color'					=> 'efefe9',
			'content_bg_color'			=> 'ffffff',
			'footer_bg_color' 			=> '657f8c',
			'footer_bg_type' 			=> 'custom',
			'footer_widgets_bg_color'	=> '1f1f1f',
			'footer_widgets_bg_type' 	=> 'custom',
			'header_bg_color'			=> '1f1f1f',
			'header_bg_grad'			=> '1',
			'header_bg_type' 			=> 'custom',
			'heading_color'				=> '333333',
			'layout'  					=> 'full-width',
			'nav_bg_color'				=> '333333',
			'nav_bg_grad'				=> '1',
			'nav_bg_type' 				=> 'custom',
			'nav_position'				=> 'centered',
			'text_color'				=> '808080',
			'top_bar_bg_color' 			=> '657f8c',
			'top_bar_bg_grad' 			=> '1',
			'top_bar_bg_type' 			=> 'custom'
		)
	),
	
	/**
	 * @name Premier
	 */
	'premier' 	=> array(
		'name'		=> 'Premier',
		'skin'		=> 'premier',
		'settings'	=> array(
			'accent_color' 				=> '319753',
			'bg_color'					=> '262626',
			'content_bg_color'			=> 'ffffff',
			'footer_bg_color' 			=> '',
			'footer_bg_type' 			=> 'none',
			'footer_widgets_bg_color'	=> '',
			'footer_widgets_bg_type' 	=> 'none',
			'header_bg_color'			=> '262626',
			'header_bg_grad'			=> '1',
			'header_bg_type' 			=> 'custom',
			'heading_color'				=> '333333',
			'layout'  					=> 'full-width',
			'nav_bg_color'				=> '319753',
			'nav_bg_grad'				=> '1',
			'nav_bg_type' 				=> 'custom',
			'nav_position'				=> 'bottom',
			'text_color'				=> '808080',
			'top_bar_bg_color' 			=> '',
			'top_bar_bg_grad' 			=> '0',
			'top_bar_bg_type' 			=> 'none'
		)
	),
	
	/**
	 * @name Dusk
	 */
	'dusk' 		=> array(
		'name'		=> 'Dusk',
		'skin'		=> 'premier',
		'settings'	=> array(
			'accent_color' 				=> 'cc3f26',
			'bg_color'					=> '1a1a1a',
			'content_bg_color'			=> '262626',
			'footer_bg_color' 			=> '',
			'footer_bg_type' 			=> 'none',
			'footer_widgets_bg_color'	=> '',
			'footer_widgets_bg_type' 	=> 'none',
			'header_bg_color'			=> '',
			'header_bg_grad'			=> '0',
			'header_bg_type' 			=> 'none',
			'heading_color'				=> 'e6e6e6',
			'layout'  					=> 'boxed',
			'nav_bg_color'				=> 'cc3f26',
			'nav_bg_grad'				=> '1',
			'nav_bg_type' 				=> 'custom',
			'nav_position'				=> 'bottom',
			'text_color'				=> '999999',
			'top_bar_bg_color' 			=> '262626',
			'top_bar_bg_grad' 			=> '0',
			'top_bar_bg_type' 			=> 'custom'
		)
	),
	
	/**
	 * @name Midnight
	 */
	'midnight' 		=> array(
		'name'		=> 'Midnight',
		'skin'		=> 'modern',
		'settings'	=> array(
			'accent_color' 				=> '428aca',
			'bg_color'					=> '1a1a1a',
			'content_bg_color'			=> '262626',
			'footer_bg_color' 			=> '',
			'footer_bg_type' 			=> 'none',
			'footer_widgets_bg_color'	=> '',
			'footer_widgets_bg_type' 	=> 'none',
			'header_bg_color'			=> '262626',
			'header_bg_grad'			=> '1',
			'header_bg_type' 			=> 'custom',
			'heading_color'				=> 'e6e6e6',
			'layout'  					=> 'boxed',
			'nav_bg_color'				=> '',
			'nav_bg_grad'				=> '0',
			'nav_bg_type' 				=> 'none',
			'nav_position'				=> 'right',
			'text_color'				=> '999999',
			'top_bar_bg_color' 			=> '',
			'top_bar_bg_grad' 			=> '0',
			'top_bar_bg_type' 			=> 'none'
		)
	)
);