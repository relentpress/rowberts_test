
(function($){if(typeof FLBuilderLayout!='undefined'){return;}
FLBuilderLayout={init:function()
{var win=$(window);FLBuilderLayout._destroy();$('body').addClass('fl-builder');if(FLBuilderLayout._isTouch()){$('body').addClass('fl-builder-touch');}
if($('.fl-row-bg-parallax').length>0&&!FLBuilderLayout._isTouch()){FLBuilderLayout._scrollParallaxBackgrounds();FLBuilderLayout._initParallaxBackgrounds();win.on('scroll.fl-bg-parallax',FLBuilderLayout._scrollParallaxBackgrounds);}
if($('.fl-bg-video').length>0){FLBuilderLayout._resizeBgVideos();win.on('resize.fl-bg-video',FLBuilderLayout._resizeBgVideos);}
if($('.fl-builder-edit').length===0&&typeof jQuery.fn.waypoint!=='undefined'&&!FLBuilderLayout._isTouch()){FLBuilderLayout._initModuleAnimations();}},_destroy:function()
{var win=$(window);win.off('scroll.fl-bg-parallax');win.off('resize.fl-bg-video');},_isTouch:function()
{if(('ontouchstart'in window)||(window.DocumentTouch&&document instanceof DocumentTouch)){return true;}
return false;},_initParallaxBackgrounds:function()
{$('.fl-row-bg-parallax').each(FLBuilderLayout._initParallaxBackground);},_initParallaxBackground:function()
{var row=$(this),content=row.find('.fl-row-content-wrap'),src=row.data('parallax-image'),img=new Image();if(typeof src!='undefined'){$(img).on('load',function(){content.css('background-image','url('+src+')');});img.src=src;}},_scrollParallaxBackgrounds:function()
{$('.fl-row-bg-parallax').each(FLBuilderLayout._scrollParallaxBackground);},_scrollParallaxBackground:function()
{var win=$(window),row=$(this),content=row.find('.fl-row-content-wrap'),speed=row.data('parallax-speed'),offset=content.offset(),yPos=-((win.scrollTop()-offset.top)/speed);content.css('background-position','center '+yPos+'px');},_resizeBgVideos:function()
{$('.fl-bg-video').each(FLBuilderLayout._resizeBgVideo);},_resizeBgVideo:function()
{var wrap=$(this),wrapHeight=wrap.outerHeight(),wrapWidth=wrap.outerWidth(),vid=wrap.find('video'),vidHeight=vid.data('height'),vidWidth=vid.data('width'),newWidth=wrapWidth,newHeight=Math.round(vidHeight*wrapWidth/vidWidth),newLeft=0,newTop=0;if(vidHeight==''||vidWidth==''){vid.css({'left':'0px','top':'0px','width':newWidth+'px'});}
else{if(newHeight<wrapHeight){newHeight=wrapHeight;newWidth=Math.round(vidWidth*wrapHeight/vidHeight);newLeft=-((newWidth-wrapWidth)/2);}
else{newTop=-((newHeight-wrapHeight)/2);}
vid.css({'left':newLeft+'px','top':newTop+'px','height':newHeight+'px','width':newWidth+'px'});}},_initModuleAnimations:function()
{$('.fl-animation').waypoint({offset:'80%',handler:FLBuilderLayout._doModuleAnimation});},_doModuleAnimation:function()
{var module=$(this),delay=parseFloat(module.data('animation-delay'));if(!isNaN(delay)&&delay>0){setTimeout(function(){module.addClass('fl-animated');},delay*1000);}
else{module.addClass('fl-animated');}}};$(function(){FLBuilderLayout.init();});})(jQuery);var FLBuilderPostGrid;(function($){FLBuilderPostGrid=function(settings)
{this.settings=settings;this.nodeClass='.fl-node-'+settings.id;this.wrapperClass=this.nodeClass+' .fl-post-'+this.settings.layout;this.postClass=this.wrapperClass+'-post';if(this._hasPosts()){this._initLayout();this._initInfiniteScroll();}};FLBuilderPostGrid.prototype={settings:{},nodeClass:'',wrapperClass:'',postClass:'',gallery:null,_hasPosts:function()
{return $(this.postClass).length>0;},_initLayout:function()
{switch(this.settings.layout){case'grid':this._gridLayout();break;case'gallery':this._galleryLayout();break;}
$(this.postClass).css('visibility','visible');},_gridLayout:function()
{var wrap=$(this.wrapperClass);wrap.masonry({columnWidth:this.nodeClass+' .fl-post-grid-sizer',gutter:parseInt(this.settings.postSpacing),isFitWidth:true,itemSelector:this.postClass,transitionDuration:0});wrap.imagesLoaded(function(){wrap.masonry();});},_galleryLayout:function()
{this.gallery=new FLBuilderGalleryGrid({'wrapSelector':this.wrapperClass,'itemSelector':'.fl-post-gallery-post'});},_initInfiniteScroll:function()
{if(this.settings.pagination=='scroll'&&typeof FLBuilder==='undefined'){this._infiniteScroll();}},_infiniteScroll:function(settings)
{$(this.wrapperClass).infinitescroll({navSelector:this.nodeClass+' .fl-builder-pagination',nextSelector:this.nodeClass+' .fl-builder-pagination a.next',itemSelector:this.postClass,prefill:true,bufferPx:200,loading:{msgText:'Loading',finishedMsg:'',img:flBuilderUrl+'img/ajax-loader-grey.gif',speed:1}},$.proxy(this._infiniteScrollComplete,this));setTimeout(function(){$(window).trigger('resize');},100);},_infiniteScrollComplete:function(elements)
{var wrap=$(this.wrapperClass);elements=$(elements);if(this.settings.layout=='grid'){wrap.imagesLoaded(function(){wrap.masonry('appended',elements);elements.css('visibility','visible');});}
else if(this.settings.layout=='gallery'){this.gallery.resize();elements.css('visibility','visible');}}};})(jQuery);(function($){$(function(){new FLBuilderPostGrid({id:'5487e868669e0',layout:'grid',pagination:'numbers',postSpacing:'60',postWidth:'300'});});$(window).on('load',function(){$('.fl-node-5487e868669e0 .fl-post-grid').masonry('reloadItems');});})(jQuery);var wpAjaxUrl='http://rowberts.sg.rowberts.com.au/wp-admin/admin-ajax.php';var flBuilderUrl='http://rowberts.sg.rowberts.com.au/wp-content/plugins/bb-plugin/';