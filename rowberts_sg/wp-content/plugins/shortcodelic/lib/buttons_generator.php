<div id="shortcodelic_buttons_generator" class="shortcodelic_meta shortcodelic_editelement_panel" data-title="<?php _e('Buttons', 'shortcodelic'); ?>">
    <div class="alignleft shortcodelic_options_area">
        <h3><?php _e('Link', 'shortcodelic'); ?>:</h3>
        <input type="text" data-name="link" value="" placeholder="Type a URL">

        <div class="clear"></div>

        <h3><?php _e('Style', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select data-name="style">
                    <option value=''><?php _e('basic style', 'shortcodelic'); ?></option>
                    <?php $shortcodelic_array_buttons_ = get_option('shortcodelic_array_buttons_') == '' ? array() : get_option('shortcodelic_array_buttons_');
                    $button_filter = apply_filters('add_shortcodelic_buttons_options','');
                    if ( $button_filter!='' && is_array($button_filter) ) {
                        $shortcodelic_array_buttons_ = array_merge($button_filter, $shortcodelic_array_buttons_);
                    }
                    if (isset($shortcodelic_array_buttons_) && is_array($shortcodelic_array_buttons_)) foreach ($shortcodelic_array_buttons_ as $key => $value) { ?>
                        <option value='<?php echo $key; ?>'><?php echo $key; ?></option>
                    <?php } else { ?>
                    <?php } ?>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><label class="alignleft" for="link_target_shortcodelic"><?php _e('Open into another window', 'shortcodelic'); ?>:
            <input type="checkbox" id="link_target_shortcodelic" data-name="target" value="true">
            <span></span>
        </label></h3>
        
        <div class="clear"></div>

        <h3><?php _e('Font size', 'shortcodelic'); ?>:</h3>
        <div class="slider_div">
            <input type="text" data-name="fontsize" value="14">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

    </div>

    <div class="alignleft shortcodelic_opts_list_2 shortcodelic_tinymce_area">
        <h3><?php printf( __( 'Content %1$s(%2$sadd an icon too%3$s)%4$s'  ), '<small>', '<a href="#" class="shortcodelic_icon_button">', '</a>', '</small>', 'shortcodelic'); ?>:</h3>
        <textarea data-name="content" placeholder="Text in the link"></textarea>

        <div class="clear"></div>

        <h3><?php _e('Class', 'shortcodelic'); ?>:</h3>
        <input type="text" data-name="class" value="" placeholder="Class for CSS, Javascript purpose">

        <div class="clear"></div>

        <h3><?php _e('ID', 'shortcodelic'); ?>:</h3>
        <input type="text" data-name="id" value="" placeholder="ID for CSS, Javascript purpose">

        <div class="clear"></div>

    </div>

</div><!-- #shortcodelic_buttons_generator -->