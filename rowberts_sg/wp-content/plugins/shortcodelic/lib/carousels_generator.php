<?php
if ( isset($_POST['postid']) ) {
    $scpostid = $_POST['postid'];
    require_once('../../../../wp-load.php');
} else {
    $scpostid = $scpostid;
}
?>
<div id="shortcodelic_carousels_generator" class="shortcodelic_meta" data-title="<?php _e('Shortcode generator &rarr; Carousel', 'shortcodelic'); ?>">
<?php
$values = get_post_custom( $scpostid );
$shortcodelic_carousels = isset( $values['shortcodelic_carousels'] ) ? $values['shortcodelic_carousels'][0] : '';
$shortcodelic_carousels = maybe_unserialize($shortcodelic_carousels);
$ajax_nonce = wp_create_nonce('shortcodelic_carousels_nonce');
$carouselSize = '1';
$carouselClone = '1';
$notEmpty = '0';
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 1280;
?>
    <div class="alignleft shortcodelic_options_area">
        <script type="text/javascript">
            var shortcodelic_carousels_nonce = '<?php echo $ajax_nonce; ?>';
        </script>
        <h3><?php _e('Select among the carousels available on this post', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select id="shortcodelic_carousels_manager_loader" class="shortcodelic_shortcode_manager_loader">
                    <option value='' data-type="carousels"><?php _e('Create a new carousel', 'shortcodelic'); ?></option>
                    <?php if (is_array($shortcodelic_carousels)) {
                        $carouselSize = sizeof($shortcodelic_carousels)+1;
                        $carouselClone = sizeof($shortcodelic_carousels)+1;
                        for ($key = 1; $key <= sizeof($shortcodelic_carousels); $key++) {
                            if ( !empty($shortcodelic_carousels['shortcodelic_carousels_'.$key]) ) {
                            $notEmpty = $notEmpty + 1; ?>
                        <option value='[shortcodelic-carousel postid="<?php echo $scpostid; ?>" carousel="<?php echo $key ?>"]' data-type="carousels" data-link='&amp;shortcodelic_carousel_length=<?php echo $key ?>'>[shortcodelic-carousel postid=&quot;<?php echo $scpostid; ?>&quot; carousel=&quot;<?php echo $key ?>&quot;]</option>
                            <?php } else {
                                $carouselSize = $carouselSize < $key ? $carouselSize : $key;
                                $carouselClone = $carouselClone < $key ? $carouselClone : $key;
                            }
                        }
                    } ?>

                    <?php if ( $notEmpty == 0 ) {
                        delete_post_meta($scpostid, 'shortcodelic_carousels');
                    } ?>
                    </select>
            </span>
        </label>

        <div class="clear"></div>

        <?php
            if ( isset($_POST['shortcodelic_carousel_length']) && $_POST['shortcodelic_carousel_length']!='' ) {
                $carouselSize = $_POST['shortcodelic_carousel_length'];
            }
        ?>

        <div class="shortcodelic_opts_part">
            <div class="shortcodelic_el_list">
                <?php if ( isset($_POST['shortcodelic_carousel_length']) && $_POST['shortcodelic_carousel_length']!='' ) { ?>
                    <div class="alignleft">
                        <h3><?php _e('Clone your carousel', 'shortcodelic'); ?>:</h3>
                        <a href="#" class="clone-set pix_button" data-type="carousels" data-oldcarousels="shortcodelic_carousels_<?php echo $carouselSize; ?>" data-carousels="shortcodelic_carousels_<?php echo $carouselClone; ?>" data-link="&amp;shortcodelic_carousel_length=<?php echo $carouselClone; ?>"><?php _e('Clone this carousel', 'shortcodelic'); ?></a>
                    </div>
                    <div class="alignright">
                        <h3><?php _e('Delete your carousel', 'shortcodelic'); ?>:</h3>
                        <a href="#" class="delete-set pix_button" data-type="carousels" data-carousels="shortcodelic_carousels_<?php echo $carouselSize; ?>"><?php _e('Delete this carousel', 'shortcodelic'); ?></a>
                    </div>
                <?php } ?>
                <div class="clear"></div><hr>

                <h3><?php _e('Edit your carousel', 'shortcodelic'); ?>:</h3>
                <a href="#" class="add-slide pix_button"><?php _e('Add slides', 'shortcodelic'); ?></a>
                <br>
                <br>
                <div class="slides-list alignleft">
                    <?php if (isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide']) && is_array($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'])) { $i = 1; foreach ($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'] as $key => &$carousel) { ?>

                    <?php
                        $attachment_bg = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['bg']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['bg']) : '';
                        $attachment_id = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['id']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['id']): '';
                        $attachment_size = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['size']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['size']): '';
                        $attachment_box = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['box']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['box']): 'true';
                        $attachment_link = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['link']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['link']): '';
                        $attachment_target = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['target']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['target']): '';
                        $attachment_caption = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['caption']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['caption']): '';
                        $attachment_url = wp_get_attachment_url( $attachment_id );
                        $cover = '';
                        if ( $attachment_id != '' ) {
                            $attachment_bg = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
                            $attachment_bg = 'url('.$attachment_bg[0].')';
                            $attachment_url = wp_get_attachment_image_src( $attachment_id, $attachment_size );
                            $attachment_url = $attachment_url[0];
                        } else {
                            $attachment_url = $attachment_bg;
                            $attachment_bg = 'url('.$attachment_bg.')';
                        }

                    ?>
                        <div class="slide-el <?php echo $cover; ?>" style="background-image:<?php echo $attachment_bg; ?>">
                            <span class="slide-el-ui no-slide"><?php echo $i; ?></span>
                            <span class="slide-el-ui edit-slide" data-dest="#shortcodelic_editcarouselslide_panel" data-cols="three"><i class="scicon-awesome-pencil"></i></span>
                            <span class="slide-el-ui clone-slide"><i class="scicon-awesome-docs"></i></span>
                            <span class="slide-el-ui delete-slide"><i class="scicon-awesome-trash"></i></span>
                            <input type="hidden" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][<?php echo $key; ?>][bg]" value="<?php echo $attachment_url; ?>">
                            <input type="hidden" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][<?php echo $key; ?>][id]" value="<?php echo $attachment_id; ?>">
                            <input type="hidden" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][<?php echo $key; ?>][size]" value="<?php echo $attachment_size; ?>">
                            <input type="hidden" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][<?php echo $key; ?>][box]" value="<?php echo $attachment_box; ?>">
                            <input type="hidden" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][<?php echo $key; ?>][link]" value="<?php echo $attachment_link; ?>">
                            <input type="hidden" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][<?php echo $key; ?>][target]" value="<?php echo $attachment_target; ?>">
                            <input type="hidden" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][<?php echo $key; ?>][caption]" value="<?php echo $attachment_caption; ?>">
                        </div><!-- .slide-el -->
                    <?php $i++; } } ?>
                    <div class="clone slide-el">
                        <span class="slide-el-ui no-slide"></span>
                        <span class="slide-el-ui edit-slide" data-dest="#shortcodelic_editcarouselslide_panel" data-cols="three"><i class="scicon-awesome-pencil"></i></span>
                        <span class="slide-el-ui clone-slide"><i class="scicon-awesome-docs"></i></span>
                        <span class="slide-el-ui delete-slide"><i class="scicon-awesome-trash"></i></span>
                        <input type="hidden" data-name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][key][bg]" value="">
                        <input type="hidden" data-name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][key][id]" value="">
                        <input type="hidden" data-name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][key][size]" value="">
                        <input type="hidden" data-name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][key][box]" value="true">
                        <input type="hidden" data-name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][key][link]" value="">
                        <input type="hidden" data-name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][key][target]" value="0">
                        <input type="hidden" data-name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slide][key][caption]" value="">
                    </div><!-- .clone.slide-el -->
                </div><!-- .slides-list -->
            </div><!-- .shortcodelic_el_list -->
        </div><!-- .shortcodelic_opts_part -->
    </div>

    <div class="alignleft shortcodelic_opts_list_2">

        <h3><?php _e('Shortcode <small>(if you want to use it on a different page/post)</small>', 'shortcodelic'); ?>:</h3>
        <input type="text" disabled="disabled" id="shortcodelic_get_sc" value="[shortcodelic-carousel postid=&quot;<?php echo $scpostid; ?>&quot; carousel=&quot;<?php echo $carouselSize; ?>&quot;]">

        <div class="clear"></div>

        <h3><?php _e('Mode', 'shortcodelic'); ?>:</h3>
        <?php $mode = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['mode']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['mode']) : 'horizontal'; ?>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="shortcodelic_carousels_<?php echo $carouselSize; ?>[mode]">
                    <option value='horizontal' <?php selected( $mode, 'horizontal' ); ?>><?php _e('horizontal', 'shortcodelic'); ?></option>
                    <option value='vertical' <?php selected( $mode, 'vertical' ); ?>><?php _e('vertical', 'shortcodelic'); ?></option>
                    <option value='fade' <?php selected( $mode, 'fade' ); ?>><?php _e('fade', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Slide width (in pixels)', 'shortcodelic'); ?>:</h3>
        <?php $slidewidth = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slidewidth']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slidewidth']) : floor(($content_width+(20*3))/4); ?>
        <div class="slider_div">
            <input type="text" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slidewidth]" value="<?php echo $slidewidth; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Timeout (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <?php $timeout = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['timeout']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['timeout']) : '7000'; ?>
        <div class="slider_div milliseconds">
            <input type="text" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[timeout]" value="<?php echo $timeout; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Speed (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <?php $speed = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['speed']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['speed']) : '750'; ?>
        <div class="slider_div milliseconds">
            <input type="text" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[speed]" value="<?php echo $speed; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

    </div>

    <div class="alignleft shortcodelic_opts_list">
        
        <h3><?php _e('Minimum number of slides', 'shortcodelic'); ?>:</h3>
        <?php $minslides = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['minslides']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['minslides']) : 4; ?>
        <div class="slider_div stroke">
            <input type="text" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[minslides]" value="<?php echo $minslides; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Maximum number of slides', 'shortcodelic'); ?>:</h3>
        <?php $maxslides = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['maxslides']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['maxslides']) : 4; ?>
        <div class="slider_div stroke">
            <input type="text" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[maxslides]" value="<?php echo $maxslides; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Margin between each slide', 'shortcodelic'); ?>:</h3>
        <?php $slidemargin = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slidemargin']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slidemargin']) : 20; ?>
        <div class="slider_div grid">
            <input type="text" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[slidemargin]" value="<?php echo $slidemargin; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <?php $autoplay = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['autoplay']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['autoplay']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_carousels_<?php echo $carouselSize; ?>_autoplay"><?php _e('Autoplay', 'shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[autoplay]" value="0">
            <input type="checkbox" id="shortcodelic_carousels_<?php echo $carouselSize; ?>_autoplay" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[autoplay]" value="true" <?php checked( $autoplay, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $hover = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['hover']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['hover']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_carousels_<?php echo $carouselSize; ?>_hover"><?php _e('Pause on hover', 'shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[hover]" value="0">
            <input type="checkbox" id="shortcodelic_carousels_<?php echo $carouselSize; ?>_hover" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[hover]" value="true" <?php checked( $hover, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $play_pause = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['play_pause']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['play_pause']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_carousels_<?php echo $carouselSize; ?>_play_pause"><?php _e('Play/pause button','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[play_pause]" value="0">
            <input type="checkbox" id="shortcodelic_carousels_<?php echo $carouselSize; ?>_play_pause" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[play_pause]" value="true" <?php checked( $play_pause, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $prev_next = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['prev_next']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['prev_next']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_carousels_<?php echo $carouselSize; ?>_prev_next"><?php _e('Prev/next arrows','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[prev_next]" value="0">
            <input type="checkbox" id="shortcodelic_carousels_<?php echo $carouselSize; ?>_prev_next" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[prev_next]" value="true" <?php checked( $prev_next, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $bullets = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['bullets']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['bullets']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_carousels_<?php echo $carouselSize; ?>_bullets"><?php _e('Pagination bullets','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[bullets]" value="0">
            <input type="checkbox" id="shortcodelic_carousels_<?php echo $carouselSize; ?>_bullets" name="shortcodelic_carousels_<?php echo $carouselSize; ?>[bullets]" value="true" <?php checked( $bullets, 'true' ); ?>>
            <span></span>
        </label></h3>

        <div class="clear"></div>
        <br>

    </div><!-- .shortcodelic_opts_list -->
</div><!-- #shortcodelic_carousel_generator -->

<?php /*********** SLIDE EDIT PANEL ***********/ ?>

<div id="shortcodelic_editcarouselslide_panel" class="shortcodelic_meta shortcodelic_editelement_panel" data-title="<?php _e('Shortcode generator &rarr; Carousel &rarr; Slide', 'shortcodelic'); ?>">
    <div class="alignleft shortcodelic_options_area">
        <h3><?php _e('Background', 'shortcodelic'); ?>:</h3>
        <div class="pix_upload upload_image">
            <input type="text" name="slide_bg" disabled="disabled" value="">
            <input type="hidden" name="slide_id" value="">
            <input type="hidden" name="slide_size" value="">
            <span class="img_preview slide_bg"></span>
            <a class="pix_button" href="#"><?php _e('Insert','geode'); ?></a>
        </div>

        <div class="clear"></div>

        <h3><label class="alignleft" for="slidecarousel_box"><?php _e('Enlarge the image on click', 'shortcodelic'); ?>:
            <input type="checkbox" id="slidecarousel_box" name="slide_box" value="true">
            <span></span>
        </label></h3>
        
        <div class="clear"></div>

        <h3><?php _e('Or type a link URL', 'shortcodelic'); ?>:</h3>
        <input type="text" name="slide_link" value="">

        <div class="clear"></div>

        <h3><label class="alignleft" for="slidecarousel_target"><?php _e('Open the link in a new window', 'shortcodelic'); ?>:
            <input type="checkbox" id="slidecarousel_target" name="slide_target" value="true">
            <span></span>
        </label></h3>
        
        <div class="clear"></div>

    </div><!-- .shortcodelic_options_area -->

    <div class="alignleft shortcodelic_options_area_2 shortcodelic_tinymce_area">
    </div><!-- .shortcodelic_options_area_2.shortcodelic_tinymce_area -->

</div>