<?php
if ( isset($_POST['postid']) ) {
    $scpostid = $_POST['postid'];
    require_once('../../../../wp-load.php');
} else {
    $scpostid = $post->ID;
}
?>
<div id="shortcodelic_slideshows_generator" class="shortcodelic_meta" data-title="<?php _e('Shortcode generator &rarr; Slideshow', 'shortcodelic'); ?>">
<?php
$values = get_post_custom( $scpostid );
$shortcodelic_slideshows = isset( $values['shortcodelic_slideshows'] ) ? $values['shortcodelic_slideshows'][0] : '';
$shortcodelic_slideshows = maybe_unserialize($shortcodelic_slideshows);
$ajax_nonce = wp_create_nonce('shortcodelic_slideshows_nonce');
$slideshowSize = '1';
$slideshowClone = '1';
$notEmpty = '0';
global $content_width;
$content_width = isset($content_width) ? $content_width : '980';

if (!function_exists('shortcodelic_match_slide_quotes')) {
    function shortcodelic_match_slide_quotes($matches) {
        return preg_replace("/&quot;/", ";quot;", $matches[0]);
    }
}
?>
    <div class="alignleft shortcodelic_options_area">
        <script type="text/javascript">
            var shortcodelic_slideshows_nonce = '<?php echo $ajax_nonce; ?>';
        </script>
        <h3><?php _e('Select among the slideshows available on this post', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select id="shortcodelic_slideshows_manager_loader" class="shortcodelic_shortcode_manager_loader">
                    <option value='' data-type="slideshows"><?php _e('Create a new slideshow', 'shortcodelic'); ?></option>
                    <?php if (is_array($shortcodelic_slideshows)) {
                        $slideshowSize = sizeof($shortcodelic_slideshows)+1;
                        $slideshowClone = sizeof($shortcodelic_slideshows)+1;
                        for ($key = 1; $key <= sizeof($shortcodelic_slideshows); $key++) {
                            if ( !empty($shortcodelic_slideshows['shortcodelic_slideshows_'.$key]) ) {
                            $notEmpty = $notEmpty + 1; ?>
                        <option value='[shortcodelic-slideshow postid="<?php echo $scpostid; ?>" slideshow="<?php echo $key ?>"]' data-type="slideshows" data-link='&amp;shortcodelic_slideshow_length=<?php echo $key ?>'>[shortcodelic-slideshow postid=&quot;<?php echo $scpostid; ?>&quot; slideshow=&quot;<?php echo $key ?>&quot;]</option>
                            <?php } else {
                                $slideshowSize = $slideshowSize < $key ? $slideshowSize : $key;
                                $slideshowClone = $slideshowClone < $key ? $slideshowClone : $key;
                            }
                        }
                    } ?>

                    <?php if ( $notEmpty == 0 ) {
                        delete_post_meta($scpostid, 'shortcodelic_slideshows');
                    } ?>
                    </select>
            </span>
        </label>

        <div class="clear"></div>

        <?php
            if ( isset($_POST['shortcodelic_slideshow_length']) && $_POST['shortcodelic_slideshow_length']!='' ) {
                $slideshowSize = $_POST['shortcodelic_slideshow_length'];
            }
        ?>

        <div class="shortcodelic_opts_part">
            <div class="shortcodelic_el_list">
                <?php if ( isset($_POST['shortcodelic_slideshow_length']) && $_POST['shortcodelic_slideshow_length']!='' ) { ?>
                    <div class="alignleft">
                        <h3><?php _e('Clone your slideshow', 'shortcodelic'); ?>:</h3>
                        <a href="#" class="clone-set pix_button" data-type="slideshows" data-oldslideshows="shortcodelic_slideshows_<?php echo $slideshowSize; ?>" data-slideshows="shortcodelic_slideshows_<?php echo $slideshowClone; ?>" data-link="&amp;shortcodelic_slideshow_length=<?php echo $slideshowClone; ?>"><?php _e('Clone this slideshow', 'shortcodelic'); ?></a>
                    </div>
                    <div class="alignright">
                        <h3><?php _e('Delete your slideshow', 'shortcodelic'); ?>:</h3>
                        <a href="#" class="delete-set pix_button" data-type="slideshows" data-slideshows="shortcodelic_slideshows_<?php echo $slideshowSize; ?>"><?php _e('Delete this slideshow', 'shortcodelic'); ?></a>
                    </div>
                <?php } ?>
                <div class="clear"></div><hr>

                <h3><?php _e('Edit your slideshow', 'shortcodelic'); ?>:</h3>
                <a href="#" class="add-slide pix_button"><?php _e('Add slides', 'shortcodelic'); ?></a>
                <br>
                <br>
                <div class="slides-list alignleft">
                    <?php if (isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide']) && is_array($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'])) { $i = 1; foreach ($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'] as $key => &$slideshow) { ?>

                    <?php
                        $attachment_bg = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['bg']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['bg']) : '';
                        $attachment_id = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['id']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['id']): '';
                        $attachment_size = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['size']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['size']): '';
                        $attachment_bg_pos = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['bg_pos']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['bg_pos']): 'center';
                        $attachment_timeout = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['timeout']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['timeout']): '';
                        $attachment_speed = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['speed']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['speed']): '';
                        $attachment_effect = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['effect']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['effect']): '';
                        $attachment_mobfx = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['mobfx']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['mobfx']): '';
                        $attachment_longfx = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['longfx']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['longfx']): '';
                        $attachment_easing = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['easing']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['easing']): '';
                        $attachment_fxslide = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['fxslide']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['fxslide']): '';
                        $attachment_link = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['link']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['link']): '';
                        $attachment_target = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['target']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['target']): '';
                        $attachment_video = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['video']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['video']): '';
                        $attachment_stop = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['stop']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['stop']): '';
                        $attachment_elements = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['elements']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['elements']): '';
                        $attachment_elements = preg_replace_callback('|;quot;{(.*?)};quot;|', 'shortcodelic_match_slide_quotes', $attachment_elements);
                        $attachment_url = wp_get_attachment_url( $attachment_id );
                        $cover = '';
                        if ( $attachment_id != '' ) {
                            $attachment_bg = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
                            $attachment_bg = 'url('.$attachment_bg[0].')';
                            $attachment_url = wp_get_attachment_image_src( $attachment_id, $attachment_size );
                            $attachment_url = $attachment_url[0];
                        } else {
                            $attachment_url = $attachment_bg;
                            $attachment_bg = 'url('.$attachment_bg.')';
                        }

                        if ( $attachment_video != '' ) {
                            $attachment_bg = 'url('.SHORTCODELIC_URL.'images/video-icon.svg), '. $attachment_bg;
                        }

                        //$cover = $attachment_type == 'image' ? '' : 'notCover';

                    ?>
                        <div class="slide-el <?php echo $cover; ?>" style="background-image:<?php echo $attachment_bg; ?>">
                            <span class="slide-el-ui no-slide"><?php echo $i; ?></span>
                            <span class="slide-el-ui edit-slide" data-dest="#shortcodelic_editslide_panel" data-cols="three"><i class="scicon-awesome-pencil"></i></span>
                            <span class="slide-el-ui clone-slide"><i class="scicon-awesome-docs"></i></span>
                            <span class="slide-el-ui delete-slide"><i class="scicon-awesome-trash"></i></span>
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][bg]" value="<?php echo $attachment_url; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][id]" value="<?php echo $attachment_id; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][size]" value="<?php echo $attachment_size; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][bg_pos]" value="<?php echo $attachment_bg_pos; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][timeout]" value="<?php echo $attachment_timeout; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][speed]" value="<?php echo $attachment_speed; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][effect]" value="<?php echo $attachment_effect; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][mobfx]" value="<?php echo $attachment_mobfx; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][longfx]" value="<?php echo $attachment_longfx; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][easing]" value="<?php echo $attachment_easing; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][fxslide]" value="<?php echo $attachment_fxslide; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][link]" value="<?php echo $attachment_link; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][target]" value="<?php echo $attachment_target; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][video]" value="<?php echo $attachment_video; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][stop]" value="<?php echo $attachment_stop; ?>">
                            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][<?php echo $key; ?>][elements]" value="<?php echo $attachment_elements; ?>">
                        </div><!-- .slide-el -->
                    <?php $i++; } } ?>
                    <div class="clone slide-el">
                        <span class="slide-el-ui no-slide"></span>
                        <span class="slide-el-ui edit-slide" data-dest="#shortcodelic_editslide_panel" data-cols="three"><i class="scicon-awesome-pencil"></i></span>
                        <span class="slide-el-ui clone-slide"><i class="scicon-awesome-docs"></i></span>
                        <span class="slide-el-ui delete-slide"><i class="scicon-awesome-trash"></i></span>
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][bg]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][id]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][size]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][bg_pos]" value="center">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][timeout]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][speed]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][effect]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][mobfx]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][longfx]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][easing]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][fxslide]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][link]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][target]" value="0">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][video]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][stop]" value="">
                        <input type="hidden" data-name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[slide][key][elements]" value="">
                    </div><!-- .clone.slide-el -->
                </div><!-- .slides-list -->
            </div><!-- .shortcodelic_el_list -->
        </div><!-- .shortcodelic_opts_part -->
    </div>

    <div class="alignleft shortcodelic_opts_list_2">

        <h3><?php _e('Shortcode <small>(if you want to use it on a different page/post)</small>', 'shortcodelic'); ?>:</h3>
        <input type="text" disabled="disabled" id="shortcodelic_get_sc" value="[shortcodelic-slideshow postid=&quot;<?php echo $scpostid; ?>&quot; slideshow=&quot;<?php echo $slideshowSize; ?>&quot;]">

        <div class="clear"></div>

        <h3><?php _e('Layout', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('If you select &quot;fluid&quot; the height and width values will be used just to get the ratio of the slideshow, that will fit its container. If you select &quot;fitting&quot; the height and width values will be used just for the preview, but the slideshow will fit its container. If you select &quot;fixed width and auto height&quot; the height of the slides will be calculated from the ratio of the background images','shortcodelic'); ?></p>"></i>:</h3>
        <?php $layout = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['layout']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['layout']) : ''; ?>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[layout]">
                    <option value='fluid' <?php selected( $layout, 'fluid' ); ?>><?php _e('fluid', 'shortcodelic'); ?></option>
                    <option value='fitting' <?php selected( $layout, 'fitting' ); ?>><?php _e('fitting', 'shortcodelic'); ?></option>
                    <option value='fixed' <?php selected( $layout, 'fixed' ); ?>><?php _e('fixed width and height', 'shortcodelic'); ?></option>
                    <option value='fixed2' <?php selected( $layout, 'fixed2' ); ?>><?php _e('fixed width and auto height', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Width (in pixels)', 'shortcodelic'); ?>:</h3>
        <?php $width = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['width']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['width']) : $content_width; ?>
        <div class="slider_div">
            <input type="text" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[width]" value="<?php echo $width; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Height (in pixels)', 'shortcodelic'); ?>:</h3>
        <?php $height = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['height']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['height']) : round($content_width*(9/16)); ?>
        <div class="slider_div">
            <input type="text" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[height]" value="<?php echo $height; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Effect on changing', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('Pay attention: elaborate effects like &quot;breakdown&quot; or &quot;palettes&quot; could result jerky on some computer or browser, especially if combined with zoom effects','shortcodelic'); ?></p>"></i>:</h3>
        <?php $fx = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['fx']) && is_array($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['fx']) ? $shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['fx'] : array(0 => 'fade'); ?>
        <select name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[fx][]" multiple="multiple">
            <option value='fade' <?php selected( true, in_array('fade', $fx) ); ?>><?php _e('fade', 'shortcodelic'); ?></option>
            <option value='upwards' <?php selected( true, in_array('upwards', $fx) ); ?>><?php _e('upwards', 'shortcodelic'); ?></option>
            <option value='downwards' <?php selected( true, in_array('downwards', $fx) ); ?>><?php _e('downwards', 'shortcodelic'); ?></option>
            <option value='rightwards' <?php selected( true, in_array('rightwards', $fx) ); ?>><?php _e('rightwards', 'shortcodelic'); ?></option>
            <option value='leftwards' <?php selected( true, in_array('leftwards', $fx) ); ?>><?php _e('leftwards', 'shortcodelic'); ?></option>
            <option value='breakdown' <?php selected( true, in_array('breakdown', $fx) ); ?>><?php _e('breakdown', 'shortcodelic'); ?></option>
            <option value='palettes' <?php selected( true, in_array('palettes', $fx) ); ?>><?php _e('palettes', 'shortcodelic'); ?></option>
            <option value='mosaic' <?php selected( true, in_array('mosaic', $fx) ); ?>><?php _e('mosaic', 'shortcodelic'); ?></option>
            <option value='mosaicFromLeftTop' <?php selected( true, in_array('mosaicFromLeftTop', $fx) ); ?>><?php _e('mosaicFromLeftTop', 'shortcodelic'); ?></option>
            <option value='mosaicFromRightTop' <?php selected( true, in_array('mosaicFromRightTop', $fx) ); ?>><?php _e('mosaicFromRightTop', 'shortcodelic'); ?></option>
            <option value='mosaicFromLeftBottom' <?php selected( true, in_array('mosaicFromLeftBottom', $fx) ); ?>><?php _e('mosaicFromLeftBottom', 'shortcodelic'); ?></option>
            <option value='mosaicFromRightBottom' <?php selected( true, in_array('mosaicFromRightBottom', $fx) ); ?>><?php _e('mosaicFromRightBottom', 'shortcodelic'); ?></option>
            <option value='spiral' <?php selected( true, in_array('spiral', $fx) ); ?>><?php _e('spiral', 'shortcodelic'); ?></option>
            <option value='spiralreverse' <?php selected( true, in_array('spiralreverse', $fx) ); ?>><?php _e('spiralreverse', 'shortcodelic'); ?></option>
            <option value='curtainFromLeft' <?php selected( true, in_array('curtainFromLeft', $fx) ); ?>><?php _e('curtainFromLeft', 'shortcodelic'); ?></option>
            <option value='curtainFromRight' <?php selected( true, in_array('curtainFromRight', $fx) ); ?>><?php _e('curtainFromRight', 'shortcodelic'); ?></option>
            <option value='curtainFromTop' <?php selected( true, in_array('curtainFromTop', $fx) ); ?>><?php _e('curtainFromTop', 'shortcodelic'); ?></option>
            <option value='curtainFromBottom' <?php selected( true, in_array('curtainFromBottom', $fx) ); ?>><?php _e('curtainFromBottom', 'shortcodelic'); ?></option>
            <option value='slicesFromLeft' <?php selected( true, in_array('slicesFromLeft', $fx) ); ?>><?php _e('slicesFromLeft', 'shortcodelic'); ?></option>
            <option value='slicesFromRight' <?php selected( true, in_array('slicesFromRight', $fx) ); ?>><?php _e('slicesFromRight', 'shortcodelic'); ?></option>
            <option value='slicesFromTop' <?php selected( true, in_array('slicesFromTop', $fx) ); ?>><?php _e('slicesFromTop', 'shortcodelic'); ?></option>
            <option value='slicesFromBottom' <?php selected( true, in_array('slicesFromBottom', $fx) ); ?>><?php _e('slicesFromBottom', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromLeft' <?php selected( true, in_array('cardShufflingFromLeft', $fx) ); ?>><?php _e('cardShufflingFromLeft', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromRight' <?php selected( true, in_array('cardShufflingFromRight', $fx) ); ?>><?php _e('cardShufflingFromRight', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromTop' <?php selected( true, in_array('cardShufflingFromTop', $fx) ); ?>><?php _e('cardShufflingFromTop', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromBottom' <?php selected( true, in_array('cardShufflingFromBottom', $fx) ); ?>><?php _e('cardShufflingFromBottom', 'shortcodelic'); ?></option>
            <option value='vertical-cut' <?php selected( true, in_array('vertical-cut', $fx) ); ?>><?php _e('vertical-cut', 'shortcodelic'); ?></option>
            <option value='horizontal-cut' <?php selected( true, in_array('horizontal-cut', $fx) ); ?>><?php _e('horizontal-cut', 'shortcodelic'); ?></option>
        </select>
        
        <div class="clear"></div>

        <h3><?php _e('Effect on mobile devices', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('Maybe you want to use easier effect on mobile devices','shortcodelic'); ?></p>"></i>:</h3>
        <?php $mobfx = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['mobfx']) && is_array($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['mobfx']) ? $shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['mobfx'] : array(0 => ''); ?>
        <select name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[mobfx][]" multiple="multiple">
            <option value='' <?php selected( true, in_array('', $mobfx) ); ?>><?php _e('inherit', 'shortcodelic'); ?></option>
            <option value='fade' <?php selected( true, in_array('fade', $mobfx) ); ?>><?php _e('fade', 'shortcodelic'); ?></option>
            <option value='upwards' <?php selected( true, in_array('upwards', $mobfx) ); ?>><?php _e('upwards', 'shortcodelic'); ?></option>
            <option value='downwards' <?php selected( true, in_array('downwards', $mobfx) ); ?>><?php _e('downwards', 'shortcodelic'); ?></option>
            <option value='rightwards' <?php selected( true, in_array('rightwards', $mobfx) ); ?>><?php _e('rightwards', 'shortcodelic'); ?></option>
            <option value='leftwards' <?php selected( true, in_array('leftwards', $mobfx) ); ?>><?php _e('leftwards', 'shortcodelic'); ?></option>
            <option value='breakdown' <?php selected( true, in_array('breakdown', $mobfx) ); ?>><?php _e('breakdown', 'shortcodelic'); ?></option>
            <option value='palettes' <?php selected( true, in_array('palettes', $mobfx) ); ?>><?php _e('palettes', 'shortcodelic'); ?></option>
            <option value='mosaic' <?php selected( true, in_array('mosaic', $mobfx) ); ?>><?php _e('mosaic', 'shortcodelic'); ?></option>
            <option value='mosaicFromLeftTop' <?php selected( true, in_array('mosaicFromLeftTop', $mobfx) ); ?>><?php _e('mosaicFromLeftTop', 'shortcodelic'); ?></option>
            <option value='mosaicFromRightTop' <?php selected( true, in_array('mosaicFromRightTop', $mobfx) ); ?>><?php _e('mosaicFromRightTop', 'shortcodelic'); ?></option>
            <option value='mosaicFromLeftBottom' <?php selected( true, in_array('mosaicFromLeftBottom', $mobfx) ); ?>><?php _e('mosaicFromLeftBottom', 'shortcodelic'); ?></option>
            <option value='mosaicFromRightBottom' <?php selected( true, in_array('mosaicFromRightBottom', $mobfx) ); ?>><?php _e('mosaicFromRightBottom', 'shortcodelic'); ?></option>
            <option value='spiral' <?php selected( true, in_array('spiral', $mobfx) ); ?>><?php _e('spiral', 'shortcodelic'); ?></option>
            <option value='spiralreverse' <?php selected( true, in_array('spiralreverse', $mobfx) ); ?>><?php _e('spiralreverse', 'shortcodelic'); ?></option>
            <option value='curtainFromLeft' <?php selected( true, in_array('curtainFromLeft', $mobfx) ); ?>><?php _e('curtainFromLeft', 'shortcodelic'); ?></option>
            <option value='curtainFromRight' <?php selected( true, in_array('curtainFromRight', $mobfx) ); ?>><?php _e('curtainFromRight', 'shortcodelic'); ?></option>
            <option value='curtainFromTop' <?php selected( true, in_array('curtainFromTop', $mobfx) ); ?>><?php _e('curtainFromTop', 'shortcodelic'); ?></option>
            <option value='curtainFromBottom' <?php selected( true, in_array('curtainFromBottom', $mobfx) ); ?>><?php _e('curtainFromBottom', 'shortcodelic'); ?></option>
            <option value='slicesFromLeft' <?php selected( true, in_array('slicesFromLeft', $mobfx) ); ?>><?php _e('slicesFromLeft', 'shortcodelic'); ?></option>
            <option value='slicesFromRight' <?php selected( true, in_array('slicesFromRight', $mobfx) ); ?>><?php _e('slicesFromRight', 'shortcodelic'); ?></option>
            <option value='slicesFromTop' <?php selected( true, in_array('slicesFromTop', $mobfx) ); ?>><?php _e('slicesFromTop', 'shortcodelic'); ?></option>
            <option value='slicesFromBottom' <?php selected( true, in_array('slicesFromBottom', $mobfx) ); ?>><?php _e('slicesFromBottom', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromLeft' <?php selected( true, in_array('cardShufflingFromLeft', $mobfx) ); ?>><?php _e('cardShufflingFromLeft', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromRight' <?php selected( true, in_array('cardShufflingFromRight', $mobfx) ); ?>><?php _e('cardShufflingFromRight', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromTop' <?php selected( true, in_array('cardShufflingFromTop', $mobfx) ); ?>><?php _e('cardShufflingFromTop', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromBottom' <?php selected( true, in_array('cardShufflingFromBottom', $mobfx) ); ?>><?php _e('cardShufflingFromBottom', 'shortcodelic'); ?></option>
            <option value='vertical-cut' <?php selected( true, in_array('vertical-cut', $mobfx) ); ?>><?php _e('vertical-cut', 'shortcodelic'); ?></option>
            <option value='horizontal-cut' <?php selected( true, in_array('horizontal-cut', $mobfx) ); ?>><?php _e('horizontal-cut', 'shortcodelic'); ?></option>
        </select>
        
        <div class="clear"></div>

        <h3><?php _e('Effect during the timeout', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('Pay attention: zoom effects could result jerky on some computer or browser, especially if combined with elaborate effects like &quot;breakdown&quot; or &quot;palettes&quot;','shortcodelic'); ?></p>"></i>:</h3>
        <?php $longfx = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['longfx']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['longfx']) : ''; ?>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[longfx]">
                    <option value='none' <?php selected( $longfx, 'none' ); ?>><?php _e('none', 'shortcodelic'); ?></option>
                    <option value='zoomout,zoomin' <?php selected( $longfx, 'zoomout,zoomin' ); ?>><?php _e('random', 'shortcodelic'); ?></option>
                    <option value='zoomout' <?php selected( $longfx, 'zoomout' ); ?>><?php _e('zoomout', 'shortcodelic'); ?></option>
                    <option value='zoomin' <?php selected( $longfx, 'zoomin' ); ?>><?php _e('zoomin', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>
        
        <div class="clear"></div>

        <h3><?php _e('Transition on...', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('If you use the zoomout or zoomin effect, the transition effect will be applied always to the next slide','shortcodelic'); ?></p>"></i>:</h3>
        <?php $fxslide = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['fxslide']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['fxslide']) : 'next'; ?>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[fxslide]">
                    <option value="next,current" <?php selected( $fxslide, 'random' ); ?>><?php _e('random', 'shortcodelic'); ?></option>
                    <option value="next" <?php selected( $fxslide, 'next' ); ?>><?php _e('next slide', 'shortcodelic'); ?></option>
                    <option value="current" <?php selected( $fxslide, 'current' ); ?>><?php _e('current slide', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <?php $autoplay = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['autoplay']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['autoplay']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_autoplay"><?php _e('Autoplay', 'shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[autoplay]" value="0">
            <input type="checkbox" id="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_autoplay" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[autoplay]" value="true" <?php checked( $autoplay, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $hover = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['hover']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['hover']) : '0'; ?>
        <h3><label class="alignleft" for="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_hover"><?php _e('Pause on hover', 'shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[hover]" value="0">
            <input type="checkbox" id="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_hover" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[hover]" value="true" <?php checked( $hover, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $loading_bar = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['loading_bar']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['loading_bar']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_loading_bar"><?php _e('Loading bar','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[loading_bar]" value="0">
            <input type="checkbox" id="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_loading_bar" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[loading_bar]" value="true" <?php checked( $loading_bar, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $loading_pie = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['loading_pie']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['loading_pie']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_loading_pie"><?php _e('Loading pie','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[loading_pie]" value="0">
            <input type="checkbox" id="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_loading_pie" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[loading_pie]" value="true" <?php checked( $loading_pie, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $play_pause = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['play_pause']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['play_pause']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_play_pause"><?php _e('Play/pause button','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[play_pause]" value="0">
            <input type="checkbox" id="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_play_pause" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[play_pause]" value="true" <?php checked( $play_pause, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $prev_next = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['prev_next']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['prev_next']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_prev_next"><?php _e('Prev/next arrows','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[prev_next]" value="0">
            <input type="checkbox" id="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_prev_next" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[prev_next]" value="true" <?php checked( $prev_next, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $thumbnails = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['thumbnails']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['thumbnails']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_thumbnails"><?php _e('Thumbnails','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[thumbnails]" value="0">
            <input type="checkbox" id="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_thumbnails" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[thumbnails]" value="true" <?php checked( $thumbnails, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $bullets = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['bullets']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['bullets']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_bullets"><?php _e('Pagination bullets','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[bullets]" value="0">
            <input type="checkbox" id="shortcodelic_slideshows_<?php echo $slideshowSize; ?>_bullets" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[bullets]" value="true" <?php checked( $bullets, 'true' ); ?>>
            <span></span>
        </label></h3>

        <div class="clear"></div>
        <br>

    </div>

    <div class="alignleft shortcodelic_opts_list">
        
        <h3><?php _e('Timeout (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <?php $timeout = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['timeout']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['timeout']) : '7000'; ?>
        <div class="slider_div milliseconds">
            <input type="text" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[timeout]" value="<?php echo $timeout; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Speed (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <?php $speed = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['speed']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['speed']) : '750'; ?>
        <div class="slider_div milliseconds">
            <input type="text" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[speed]" value="<?php echo $speed; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Easing', 'shortcodelic'); ?>:</h3>
        <?php $easing = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['easing']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['easing']) : 'easeOutSine'; ?>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[easing]">
                    <option value="linear" <?php selected( $easing, 'linear' ); ?>><?php _e('linear', 'shortcodelic'); ?></option>
                    <option value="easeOutCubic" <?php selected( $easing, 'easeOutCubic' ); ?>><?php _e('easeOutCubic', 'shortcodelic'); ?></option>
                    <option value="easeInOutCubic" <?php selected( $easing, 'easeInOutCubic' ); ?>><?php _e('easeInOutCubic', 'shortcodelic'); ?></option>
                    <option value="easeInCirc" <?php selected( $easing, 'easeInCirc' ); ?>><?php _e('easeInCirc', 'shortcodelic'); ?></option>
                    <option value="easeOutCirc" <?php selected( $easing, 'easeOutCirc' ); ?>><?php _e('easeOutCirc', 'shortcodelic'); ?></option>
                    <option value="easeInOutCirc" <?php selected( $easing, 'easeInOutCirc' ); ?>><?php _e('easeInOutCirc', 'shortcodelic'); ?></option>
                    <option value="easeInExpo" <?php selected( $easing, 'easeInExpo' ); ?>><?php _e('easeInExpo', 'shortcodelic'); ?></option>
                    <option value="easeOutExpo" <?php selected( $easing, 'easeOutExpo' ); ?>><?php _e('easeOutExpo', 'shortcodelic'); ?></option>
                    <option value="easeInOutExpo" <?php selected( $easing, 'easeInOutExpo' ); ?>><?php _e('easeInOutExpo', 'shortcodelic'); ?></option>
                    <option value="easeInQuad" <?php selected( $easing, 'easeInQuad' ); ?>><?php _e('easeInQuad', 'shortcodelic'); ?></option>
                    <option value="easeOutQuad" <?php selected( $easing, 'easeOutQuad' ); ?>><?php _e('easeOutQuad', 'shortcodelic'); ?></option>
                    <option value="easeInOutQuad" <?php selected( $easing, 'easeInOutQuad' ); ?>><?php _e('easeInOutQuad', 'shortcodelic'); ?></option>
                    <option value="easeInQuart" <?php selected( $easing, 'easeInQuart' ); ?>><?php _e('easeInQuart', 'shortcodelic'); ?></option>
                    <option value="easeOutQuart" <?php selected( $easing, 'easeOutQuart' ); ?>><?php _e('easeOutQuart', 'shortcodelic'); ?></option>
                    <option value="easeInOutQuart" <?php selected( $easing, 'easeInOutQuart' ); ?>><?php _e('easeInOutQuart', 'shortcodelic'); ?></option>
                    <option value="easeInQuint" <?php selected( $easing, 'easeInQuint' ); ?>><?php _e('easeInQuint', 'shortcodelic'); ?></option>
                    <option value="easeOutQuint" <?php selected( $easing, 'easeOutQuint' ); ?>><?php _e('easeOutQuint', 'shortcodelic'); ?></option>
                    <option value="easeInOutQuint" <?php selected( $easing, 'easeInOutQuint' ); ?>><?php _e('easeInOutQuint', 'shortcodelic'); ?></option>
                    <option value="easeInSine" <?php selected( $easing, 'easeInSine' ); ?>><?php _e('easeInSine', 'shortcodelic'); ?></option>
                    <option value="easeOutSine" <?php selected( $easing, 'easeOutSine' ); ?>><?php _e('easeOutSine', 'shortcodelic'); ?></option>
                    <option value="easeInOutSine" <?php selected( $easing, 'easeInOutSine' ); ?>><?php _e('easeInOutSine', 'shortcodelic'); ?></option>
                    <option value="easeInBack" <?php selected( $easing, 'easeInBack' ); ?>><?php _e('easeInBack', 'shortcodelic'); ?></option>
                    <option value="easeOutBack" <?php selected( $easing, 'easeOutBack' ); ?>><?php _e('easeOutBack', 'shortcodelic'); ?></option>
                    <option value="easeInOutBack" <?php selected( $easing, 'easeInOutBack' ); ?>><?php _e('easeInOutBack', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <?php $cols = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['cols']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['cols']) : '8'; ?>
        <h3><?php _e('Slice columns', 'shortcodelic'); ?>:</h3>
        <div class="slider_div grid">
            <input type="text" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[cols]" value="<?php echo $cols; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <?php $rows = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['rows']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['rows']) : '5'; ?>
        <h3><?php _e('Slice rows', 'shortcodelic'); ?>:</h3>
        <div class="slider_div grid">
            <input type="text" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[rows]" value="<?php echo $rows; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <?php $gridcols = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['gridcols']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['gridcols']) : '6'; ?>
        <h3><?php _e('Grid columns', 'shortcodelic'); ?>:</h3>
        <div class="slider_div grid">
            <input type="text" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[gridcols]" value="<?php echo $gridcols; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <?php $gridrows = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['gridrows']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['gridrows']) : '4'; ?>
        <h3><?php _e('Grid rows', 'shortcodelic'); ?>:</h3>
        <div class="slider_div grid">
            <input type="text" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[gridrows]" value="<?php echo $gridrows; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <?php $skin = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['skin']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['skin']) : get_option('shortcodelic_slideshow_main_color'); ?>
        <h3><?php _e('Skin main color', 'shortcodelic'); ?>:</h3>
        <div class="pix_color_picker">
            <input name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[skin]" type="text" value="<?php echo $skin; ?>">
            <a class="pix_button" href="#"></a>
            <div class="colorpicker"></div>
            <i class="scicon-elusive-cancel"></i>
        </div>

        <div class="clear"></div>

        <?php $skinbg = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['skinbg']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['skinbg']) : get_option('shortcodelic_slideshow_bg_color'); ?>
        <h3><?php _e('Skin bg color', 'shortcodelic'); ?>:</h3>
        <div class="pix_color_picker">
            <input name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[skinbg]" type="text" value="<?php echo $skinbg; ?>">
            <a class="pix_button" href="#"></a>
            <div class="colorpicker"></div>
            <i class="scicon-elusive-cancel"></i>
        </div>

        <div class="clear"></div>

        <h3><?php _e('Skin bg opacity', 'shortcodelic'); ?>:</h3>
        <?php $skinopacity = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['skinopacity']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['skinopacity']) : get_option('shortcodelic_slideshow_opacity'); ?>
        <div class="slider_div opacity">
            <input type="text" name="shortcodelic_slideshows_<?php echo $slideshowSize; ?>[skinopacity]" value="<?php echo $skinopacity; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

    </div><!-- .shortcodelic_opts_list -->
</div><!-- #shortcodelic_slideshow_generator -->

<?php /*********** SLIDE EDIT PANEL ***********/ ?>

<div id="shortcodelic_editslide_panel" class="shortcodelic_meta" data-title="<?php _e('Shortcode generator &rarr; Slideshow &rarr; Slide', 'shortcodelic'); ?>">
    <div class="alignleft shortcodelic_options_area">
        <h3><?php _e('Background', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('Background images can only be taken from media library, not pasted directly in the text input','shortcodelic'); ?></p>"></i>:</h3>
        <div class="pix_upload upload_image">
            <input type="text" name="slide_bg" disabled="disabled" value="">
            <input type="hidden" name="slide_id" value="">
            <input type="hidden" name="slide_size" value="">
            <span class="img_preview slide_bg"></span>
            <a class="pix_button" href="#"><?php _e('Insert','geode'); ?></a>
        </div>

        <div class="clear"></div>

        <h3><?php _e('Background position', 'shortcodelic'); ?>:</h3>
        <div class="select-position">
            <label class="alignleft">
                <input type="radio" name="slide_bg_pos" value="left top">
                <span></span>
            </label>
            <label class="alignleft">
                <input type="radio" name="slide_bg_pos" value="top">
                <span></span>
            </label>
            <label class="alignleft">
                <input type="radio" name="slide_bg_pos" value="right top">
                <span></span>
            </label>
            <label class="alignleft">
                <input type="radio" name="slide_bg_pos" value="left center">
                <span></span>
            </label>
            <label class="alignleft">
                <input type="radio" name="slide_bg_pos" value="center">
                <span></span>
            </label>
            <label class="alignleft">
                <input type="radio" name="slide_bg_pos" value="right center">
                <span></span>
            </label>
            <label class="alignleft">
                <input type="radio" name="slide_bg_pos" value="left bottom">
                <span></span>
            </label>
            <label class="alignleft">
                <input type="radio" name="slide_bg_pos" value="bottom">
                <span></span>
            </label>
            <label class="alignleft">
                <input type="radio" name="slide_bg_pos" value="right bottom">
                <span></span>
            </label>
        </div><!-- .select-position -->
        
        <div class="clear"></div>

        <h3><?php _e('Timeout (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <div class="slider_div milliseconds">
            <input type="text" name="slide_timeout" value="">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Speed (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <div class="slider_div milliseconds">
            <input type="text" name="slide_speed" value="">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Transition on...', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="slide_fxslide">
                    <option value=""><?php _e('inherit', 'shortcodelic'); ?></option>
                    <option value="next,current"><?php _e('random', 'shortcodelic'); ?></option>
                    <option value="next"><?php _e('next slide', 'shortcodelic'); ?></option>
                    <option value="current"><?php _e('current slide', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><label class="alignleft" for="slide_stop"><?php _e('Pause the slideshow here', 'shortcodelic'); ?>:
            <input type="checkbox" id="slide_stop" name="slide_stop" value="true">
            <span></span>
        </label></h3>
        
        <div class="clear"></div>

    </div><!-- .shortcodelic_options_area -->

    <div class="alignleft shortcodelic_options_area_2">

        <h3><?php _e('Link', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('If filled, the user will be redirected to the URL paste here by clicking the background of the slides','shortcodelic'); ?></p>"></i>:</h3>
        <input type="text" name="slide_link" value="">

        <div class="clear"></div>

        <h3><label class="alignleft" for="slide_target"><?php _e('Open the link in a new window', 'shortcodelic'); ?>:
            <input type="checkbox" id="slide_target" name="slide_target" value="true">
            <span></span>
        </label></h3>
        
        <div class="clear"></div>

        <h3><?php _e('Video', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('How to use it','shortcodelic'); ?></h3><p><?php _e('Only from external servers allowed by Wordpress. Paste here the URL of the page that contains the video or the iframe provided. For the documentation and the list of servers <a href=\'http://codex.wordpress.org/Embeds\' target=\'_blank\'>click here</a>','shortcodelic'); ?></p>"></i>:</h3>
        <input type="text" name="slide_video" value="">

        <div class="clear"></div>

        <h3><?php _e('Effect on changing', 'shortcodelic'); ?>:</h3>
        <select name="slide_effect" multiple="multiple">
            <option value=''><?php _e('inherit', 'shortcodelic'); ?></option>
            <option value='fade'><?php _e('fade', 'shortcodelic'); ?></option>
            <option value='upwards'><?php _e('upwards', 'shortcodelic'); ?></option>
            <option value='downwards'><?php _e('downwards', 'shortcodelic'); ?></option>
            <option value='rightwards'><?php _e('rightwards', 'shortcodelic'); ?></option>
            <option value='leftwards'><?php _e('leftwards', 'shortcodelic'); ?></option>
            <option value='breakdown'><?php _e('breakdown', 'shortcodelic'); ?></option>
            <option value='palettes'><?php _e('palettes', 'shortcodelic'); ?></option>
            <option value='mosaic'><?php _e('mosaic', 'shortcodelic'); ?></option>
            <option value='mosaicFromLeftTop'><?php _e('mosaicFromLeftTop', 'shortcodelic'); ?></option>
            <option value='mosaicFromRightTop'><?php _e('mosaicFromRightTop', 'shortcodelic'); ?></option>
            <option value='mosaicFromLeftBottom'><?php _e('mosaicFromLeftBottom', 'shortcodelic'); ?></option>
            <option value='mosaicFromRightBottom'><?php _e('mosaicFromRightBottom', 'shortcodelic'); ?></option>
            <option value='spiral'><?php _e('spiral', 'shortcodelic'); ?></option>
            <option value='spiralreverse'><?php _e('spiralreverse', 'shortcodelic'); ?></option>
            <option value='curtainFromLeft'><?php _e('curtainFromLeft', 'shortcodelic'); ?></option>
            <option value='curtainFromRight'><?php _e('curtainFromRight', 'shortcodelic'); ?></option>
            <option value='curtainFromTop'><?php _e('curtainFromTop', 'shortcodelic'); ?></option>
            <option value='curtainFromBottom'><?php _e('curtainFromBottom', 'shortcodelic'); ?></option>
            <option value='slicesFromLeft'><?php _e('slicesFromLeft', 'shortcodelic'); ?></option>
            <option value='slicesFromRight'><?php _e('slicesFromRight', 'shortcodelic'); ?></option>
            <option value='slicesFromTop'><?php _e('slicesFromTop', 'shortcodelic'); ?></option>
            <option value='slicesFromBottom'><?php _e('slicesFromBottom', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromLeft'><?php _e('cardShufflingFromLeft', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromRight'><?php _e('cardShufflingFromRight', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromTop'><?php _e('cardShufflingFromTop', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromBottom'><?php _e('cardShufflingFromBottom', 'shortcodelic'); ?></option>
            <option value='vertical-cut'><?php _e('vertical-cut', 'shortcodelic'); ?></option>
            <option value='horizontal-cut'><?php _e('horizontal-cut', 'shortcodelic'); ?></option>
        </select>
        
        <div class="clear"></div>

        <h3><?php _e('Effect on mobile devices', 'shortcodelic'); ?>:</h3>
        <select name="slide_mobfx" multiple="multiple">
            <option value=''><?php _e('inherit', 'shortcodelic'); ?></option>
            <option value='fade'><?php _e('fade', 'shortcodelic'); ?></option>
            <option value='upwards'><?php _e('upwards', 'shortcodelic'); ?></option>
            <option value='downwards'><?php _e('downwards', 'shortcodelic'); ?></option>
            <option value='rightwards'><?php _e('rightwards', 'shortcodelic'); ?></option>
            <option value='leftwards'><?php _e('leftwards', 'shortcodelic'); ?></option>
            <option value='breakdown'><?php _e('breakdown', 'shortcodelic'); ?></option>
            <option value='palettes'><?php _e('palettes', 'shortcodelic'); ?></option>
            <option value='mosaic'><?php _e('mosaic', 'shortcodelic'); ?></option>
            <option value='mosaicFromLeftTop'><?php _e('mosaicFromLeftTop', 'shortcodelic'); ?></option>
            <option value='mosaicFromRightTop'><?php _e('mosaicFromRightTop', 'shortcodelic'); ?></option>
            <option value='mosaicFromLeftBottom'><?php _e('mosaicFromLeftBottom', 'shortcodelic'); ?></option>
            <option value='mosaicFromRightBottom'><?php _e('mosaicFromRightBottom', 'shortcodelic'); ?></option>
            <option value='spiral'><?php _e('spiral', 'shortcodelic'); ?></option>
            <option value='spiralreverse'><?php _e('spiralreverse', 'shortcodelic'); ?></option>
            <option value='curtainFromLeft'><?php _e('curtainFromLeft', 'shortcodelic'); ?></option>
            <option value='curtainFromRight'><?php _e('curtainFromRight', 'shortcodelic'); ?></option>
            <option value='curtainFromTop'><?php _e('curtainFromTop', 'shortcodelic'); ?></option>
            <option value='curtainFromBottom'><?php _e('curtainFromBottom', 'shortcodelic'); ?></option>
            <option value='slicesFromLeft'><?php _e('slicesFromLeft', 'shortcodelic'); ?></option>
            <option value='slicesFromRight'><?php _e('slicesFromRight', 'shortcodelic'); ?></option>
            <option value='slicesFromTop'><?php _e('slicesFromTop', 'shortcodelic'); ?></option>
            <option value='slicesFromBottom'><?php _e('slicesFromBottom', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromLeft'><?php _e('cardShufflingFromLeft', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromRight'><?php _e('cardShufflingFromRight', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromTop'><?php _e('cardShufflingFromTop', 'shortcodelic'); ?></option>
            <option value='cardShufflingFromBottom'><?php _e('cardShufflingFromBottom', 'shortcodelic'); ?></option>
            <option value='vertical-cut'><?php _e('vertical-cut', 'shortcodelic'); ?></option>
            <option value='horizontal-cut'><?php _e('horizontal-cut', 'shortcodelic'); ?></option>
        </select>
        
        <div class="clear"></div>

        <h3><?php _e('Effect during the timeout', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="slide_longfx">
                    <option value=''><?php _e('inherit', 'shortcodelic'); ?></option>
                    <option value='none'><?php _e('none', 'shortcodelic'); ?></option>
                    <option value='zoomout,zoomin'><?php _e('random', 'shortcodelic'); ?></option>
                    <option value='zoomout'><?php _e('zoomout', 'shortcodelic'); ?></option>
                    <option value='zoomin'><?php _e('zoomin', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>
        
        <div class="clear"></div>

        <h3><?php _e('Easing', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="slide_easing">
                    <option value=""><?php _e('inherit', 'shortcodelic'); ?></option>
                    <option value="linear"><?php _e('linear', 'shortcodelic'); ?></option>
                    <option value="easeOutCubic"><?php _e('easeOutCubic', 'shortcodelic'); ?></option>
                    <option value="easeInOutCubic"><?php _e('easeInOutCubic', 'shortcodelic'); ?></option>
                    <option value="easeInCirc"><?php _e('easeInCirc', 'shortcodelic'); ?></option>
                    <option value="easeOutCirc"><?php _e('easeOutCirc', 'shortcodelic'); ?></option>
                    <option value="easeInOutCirc"><?php _e('easeInOutCirc', 'shortcodelic'); ?></option>
                    <option value="easeInExpo"><?php _e('easeInExpo', 'shortcodelic'); ?></option>
                    <option value="easeOutExpo"><?php _e('easeOutExpo', 'shortcodelic'); ?></option>
                    <option value="easeInOutExpo"><?php _e('easeInOutExpo', 'shortcodelic'); ?></option>
                    <option value="easeInQuad"><?php _e('easeInQuad', 'shortcodelic'); ?></option>
                    <option value="easeOutQuad"><?php _e('easeOutQuad', 'shortcodelic'); ?></option>
                    <option value="easeInOutQuad"><?php _e('easeInOutQuad', 'shortcodelic'); ?></option>
                    <option value="easeInQuart"><?php _e('easeInQuart', 'shortcodelic'); ?></option>
                    <option value="easeOutQuart"><?php _e('easeOutQuart', 'shortcodelic'); ?></option>
                    <option value="easeInOutQuart"><?php _e('easeInOutQuart', 'shortcodelic'); ?></option>
                    <option value="easeInQuint"><?php _e('easeInQuint', 'shortcodelic'); ?></option>
                    <option value="easeOutQuint"><?php _e('easeOutQuint', 'shortcodelic'); ?></option>
                    <option value="easeInOutQuint"><?php _e('easeInOutQuint', 'shortcodelic'); ?></option>
                    <option value="easeInSine"><?php _e('easeInSine', 'shortcodelic'); ?></option>
                    <option value="easeOutSine"><?php _e('easeOutSine', 'shortcodelic'); ?></option>
                    <option value="easeInOutSine"><?php _e('easeInOutSine', 'shortcodelic'); ?></option>
                    <option value="easeInBack"><?php _e('easeInBack', 'shortcodelic'); ?></option>
                    <option value="easeOutBack"><?php _e('easeOutBack', 'shortcodelic'); ?></option>
                    <option value="easeInOutBack"><?php _e('easeInOutBack', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>
        
        <div class="clear"></div>

    </div><!-- .shortcodelic_options_area_2 -->

    <div class="alignleft shortcodelic_options_area_3 shortcodelic_el_list">
        <a href="#" class="add-element pix_button alignleft"><?php _e('Add elements', 'shortcodelic'); ?></a>
        <a href="#" class="slide-preview pix_button alt alignright"><?php _e('Slide preview', 'shortcodelic'); ?></a>
        <div class="cf"></div>
        <br>
        <div class="el-list alignleft">
            <div class="clone element-slide element-sort">
                <textarea readonly="readonly" data-name="slide_elements[elements][key][caption]"></textarea>
                <span class="slide-el-ui-disable-text"></span>
                <span class="slide-el-ui no-el"></span>
                <span class="slide-el-ui edit-el" data-panel="shortcodelic_editslideel_panel"><i class="scicon-awesome-pencil"></i></span>
                <span class="slide-el-ui clone-el"><i class="scicon-awesome-docs"></i></span>
                <span class="slide-el-ui delete-el"><i class="scicon-awesome-trash"></i></span>
                <span class="slide-el-ui show-el"><i class="scicon-awesome-eye"></i></span>
                <span class="slide-el-ui hide-el"><i class="scicon-awesome-eye-off"></i></span>
                <input type="hidden" data-name="slide_elements[elements][key][width]" value="100">
                <input type="hidden" data-name="slide_elements[elements][key][wider]" value="">
                <input type="hidden" data-name="slide_elements[elements][key][narrower]" value="">
                <input type="hidden" data-name="slide_elements[elements][key][bg]" value="<?php echo get_option('shortcodelic_slide_bg_color'); ?>">
                <input type="hidden" data-name="slide_elements[elements][key][opacity]" value="<?php echo get_option('shortcodelic_slide_opacity'); ?>">
                <input type="hidden" data-name="slide_elements[elements][key][from]" value="">
                <input type="hidden" data-name="slide_elements[elements][key][fromeasing]" value="linear">
                <input type="hidden" data-name="slide_elements[elements][key][fromduration]" value="500">
                <input type="hidden" data-name="slide_elements[elements][key][entering]" value="fade">
                <input type="hidden" data-name="slide_elements[elements][key][to]" value="">
                <input type="hidden" data-name="slide_elements[elements][key][toeasing]" value="linear">
                <input type="hidden" data-name="slide_elements[elements][key][toduration]" value="500">
                <input type="hidden" data-name="slide_elements[elements][key][leaving]" value="fade">
                <input type="hidden" data-name="slide_elements[elements][key][position]" value='{"top":0,"left":0}'>
            </div><!-- .clone.element-slide.element-sort -->
        </div>
    </div><!-- .shortcodelic_options_area_3 -->

</div>


<?php /*********** ELEMENT EDIT PANEL ***********/ ?>

<div id="shortcodelic_editslideel_panel" class="shortcodelic_meta shortcodelic_editelement_panel" data-title="<?php _e('Shortcode generator &rarr; Slideshow &rarr; Slide &rarr; Element', 'shortcodelic'); ?>">
    <div class="alignleft shortcodelic_tinymce_area">
    </div><!-- .shortcodelic_tinymce_area -->

    <div class="alignleft shortcodelic_options_area_2">

        <h3><?php _e('Element max width (in percentage)', 'shortcodelic'); ?>:</h3>
        <div class="slider_div percent">
            <input type="text" name="el_width" value="100">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Visible only if the slideshow is wider than (in pixels)', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('If you want that this element is visible only at particular resolution, otherwise leave blank to make the element always visible','shortcodelic'); ?></p>"></i>:</h3>
        <div class="slider_div">
            <input type="text" name="el_wider" value="">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Visible only if the slideshow is narrower than (in pixels)', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('If you want that this element is visible only at particular resolution, otherwise leave blank to make the element always visible','shortcodelic'); ?></p>"></i>:</h3>
        <div class="slider_div">
            <input type="text" name="el_narrower" value="">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Element background opacity', 'shortcodelic'); ?>:</h3>
        <div class="slider_div opacity">
            <input type="text" name="el_opacity" value="<?php echo get_option('shortcodelic_slide_opacity'); ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Element background color', 'shortcodelic'); ?>:</h3>
        <div class="pix_color_picker">
            <input type="text" name="el_bg" value="<?php echo get_option('shortcodelic_slide_bg_color'); ?>">
            <a class="pix_button" href="#"></a>
            <div class="colorpicker"></div>
            <i class="scicon-elusive-cancel"></i>
        </div>

        <div class="clear"></div>

        <h3><?php _e('Entrance effect', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="el_entering">
                    <option value=''><?php _e('none', 'shortcodelic'); ?></option>
                    <option value='fade'><?php _e('fade', 'shortcodelic'); ?></option>
                    <option value='upwards'><?php _e('upwards', 'shortcodelic'); ?></option>
                    <option value='downwards'><?php _e('downwards', 'shortcodelic'); ?></option>
                    <option value='rightwards'><?php _e('rightwards', 'shortcodelic'); ?></option>
                    <option value='leftwards'><?php _e('leftwards', 'shortcodelic'); ?></option>
                    <option value='zoomout'><?php _e('zoomout', 'shortcodelic'); ?></option>
                    <option value='zoomin'><?php _e('zoomin', 'shortcodelic'); ?></option>
                    <option value='zoomout-rotatein'><?php _e('zoomout-rotatein', 'shortcodelic'); ?></option>
                    <option value='zoomout-rotateout'><?php _e('zoomout-rotateout', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>
        <div class="clear"></div>

        <h3><?php _e('Entrance (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <div class="slider_div milliseconds">
            <input type="text" name="el_from" value="">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Entrance easing', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="el_fromeasing">
                    <option value="linear"><?php _e('linear', 'shortcodelic'); ?></option>
                    <option value="easeOutCubic"><?php _e('easeOutCubic', 'shortcodelic'); ?></option>
                    <option value="easeInOutCubic"><?php _e('easeInOutCubic', 'shortcodelic'); ?></option>
                    <option value="easeInCirc"><?php _e('easeInCirc', 'shortcodelic'); ?></option>
                    <option value="easeOutCirc"><?php _e('easeOutCirc', 'shortcodelic'); ?></option>
                    <option value="easeInOutCirc"><?php _e('easeInOutCirc', 'shortcodelic'); ?></option>
                    <option value="easeInExpo"><?php _e('easeInExpo', 'shortcodelic'); ?></option>
                    <option value="easeOutExpo"><?php _e('easeOutExpo', 'shortcodelic'); ?></option>
                    <option value="easeInOutExpo"><?php _e('easeInOutExpo', 'shortcodelic'); ?></option>
                    <option value="easeInQuad"><?php _e('easeInQuad', 'shortcodelic'); ?></option>
                    <option value="easeOutQuad"><?php _e('easeOutQuad', 'shortcodelic'); ?></option>
                    <option value="easeInOutQuad"><?php _e('easeInOutQuad', 'shortcodelic'); ?></option>
                    <option value="easeInQuart"><?php _e('easeInQuart', 'shortcodelic'); ?></option>
                    <option value="easeOutQuart"><?php _e('easeOutQuart', 'shortcodelic'); ?></option>
                    <option value="easeInOutQuart"><?php _e('easeInOutQuart', 'shortcodelic'); ?></option>
                    <option value="easeInQuint"><?php _e('easeInQuint', 'shortcodelic'); ?></option>
                    <option value="easeOutQuint"><?php _e('easeOutQuint', 'shortcodelic'); ?></option>
                    <option value="easeInOutQuint"><?php _e('easeInOutQuint', 'shortcodelic'); ?></option>
                    <option value="easeInSine"><?php _e('easeInSine', 'shortcodelic'); ?></option>
                    <option value="easeOutSine"><?php _e('easeOutSine', 'shortcodelic'); ?></option>
                    <option value="easeInOutSine"><?php _e('easeInOutSine', 'shortcodelic'); ?></option>
                    <option value="easeInBack"><?php _e('easeInBack', 'shortcodelic'); ?></option>
                    <option value="easeOutBack"><?php _e('easeOutBack', 'shortcodelic'); ?></option>
                    <option value="easeInOutBack"><?php _e('easeInOutBack', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>
        
        <div class="clear"></div>

        <h3><?php _e('Entrance duration (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <div class="slider_div milliseconds">
            <input type="text" name="el_fromduration" value="500">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Exit effect', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="el_leaving">
                    <option value=''><?php _e('none', 'shortcodelic'); ?></option>
                    <option value='fade'><?php _e('fade', 'shortcodelic'); ?></option>
                    <option value='upwards'><?php _e('upwards', 'shortcodelic'); ?></option>
                    <option value='downwards'><?php _e('downwards', 'shortcodelic'); ?></option>
                    <option value='rightwards'><?php _e('rightwards', 'shortcodelic'); ?></option>
                    <option value='leftwards'><?php _e('leftwards', 'shortcodelic'); ?></option>
                    <option value='zoomout'><?php _e('zoomout', 'shortcodelic'); ?></option>
                    <option value='zoomin'><?php _e('zoomin', 'shortcodelic'); ?></option>
                    <option value='zoomout-rotatein'><?php _e('zoomout-rotatein', 'shortcodelic'); ?></option>
                    <option value='zoomout-rotateout'><?php _e('zoomout-rotateout', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Exit (in milliseconds)', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('You must use a value bigger than for the entrance. Leave it blank if you want that the element disappear at the end of the slide period','shortcodelic'); ?></p>"></i>:</h3>
        <div class="slider_div milliseconds">
            <input type="text" name="el_to" value="">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Exit easing', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="el_toeasing">
                    <option value="linear"><?php _e('linear', 'shortcodelic'); ?></option>
                    <option value="easeOutCubic"><?php _e('easeOutCubic', 'shortcodelic'); ?></option>
                    <option value="easeInOutCubic"><?php _e('easeInOutCubic', 'shortcodelic'); ?></option>
                    <option value="easeInCirc"><?php _e('easeInCirc', 'shortcodelic'); ?></option>
                    <option value="easeOutCirc"><?php _e('easeOutCirc', 'shortcodelic'); ?></option>
                    <option value="easeInOutCirc"><?php _e('easeInOutCirc', 'shortcodelic'); ?></option>
                    <option value="easeInExpo"><?php _e('easeInExpo', 'shortcodelic'); ?></option>
                    <option value="easeOutExpo"><?php _e('easeOutExpo', 'shortcodelic'); ?></option>
                    <option value="easeInOutExpo"><?php _e('easeInOutExpo', 'shortcodelic'); ?></option>
                    <option value="easeInQuad"><?php _e('easeInQuad', 'shortcodelic'); ?></option>
                    <option value="easeOutQuad"><?php _e('easeOutQuad', 'shortcodelic'); ?></option>
                    <option value="easeInOutQuad"><?php _e('easeInOutQuad', 'shortcodelic'); ?></option>
                    <option value="easeInQuart"><?php _e('easeInQuart', 'shortcodelic'); ?></option>
                    <option value="easeOutQuart"><?php _e('easeOutQuart', 'shortcodelic'); ?></option>
                    <option value="easeInOutQuart"><?php _e('easeInOutQuart', 'shortcodelic'); ?></option>
                    <option value="easeInQuint"><?php _e('easeInQuint', 'shortcodelic'); ?></option>
                    <option value="easeOutQuint"><?php _e('easeOutQuint', 'shortcodelic'); ?></option>
                    <option value="easeInOutQuint"><?php _e('easeInOutQuint', 'shortcodelic'); ?></option>
                    <option value="easeInSine"><?php _e('easeInSine', 'shortcodelic'); ?></option>
                    <option value="easeOutSine"><?php _e('easeOutSine', 'shortcodelic'); ?></option>
                    <option value="easeInOutSine"><?php _e('easeInOutSine', 'shortcodelic'); ?></option>
                    <option value="easeInBack"><?php _e('easeInBack', 'shortcodelic'); ?></option>
                    <option value="easeOutBack"><?php _e('easeOutBack', 'shortcodelic'); ?></option>
                    <option value="easeInOutBack"><?php _e('easeInOutBack', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>
        
        <div class="clear"></div>

        <h3><?php _e('Exit duration (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <div class="slider_div milliseconds">
            <input type="text" name="el_toduration" value="500">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Position', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('This field is automatically updated when you drag the elements in the preview window. If you want to edit it manually, pay attention to preserve the JSON syntax','shortcodelic'); ?></p>"></i>:</h3>
        <input type="text" name="el_position" value='{"top":0,"left":0}'>

        <div class="clear"></div>

    </div><!-- .shortcodelic_options_area_2 -->
</div>


<?php /*********** SLIDE PREVIEW ***********/ ?>

<div id="shortcodelic_slide_preview" class="shortcodelic_meta" data-title="<?php _e('Slide preview (drag &amp; drop)', 'shortcodelic'); ?>">
</div>