<div id="shortcodelic_progress_generator" class="shortcodelic_meta" data-title="<?php _e('Progress indicators', 'shortcodelic'); ?>">
    <div class="alignleft shortcodelic_options_area">
        <h3><?php _e('Label 1', 'shortcodelic'); ?>:</h3>
        <input type="text" data-name="label" value="" placeholder="Put your descriptive text here">

        <div class="clear"></div>

        <h3><?php _e('Display the label instead of the value', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select data-name="label2">
                    <option value='false'><?php _e('false', 'shortcodelic'); ?></option>
                    <option value='true'><?php _e('true', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Unit', 'shortcodelic'); ?>:</h3>
        <input type="text" data-name="unit" value="%" placeholder="Put your descriptive text here">

        <div class="clear"></div>

        <h3><?php _e('Value', 'shortcodelic'); ?>:</h3>
        <div class="slider_div">
            <input type="text" data-name="value" value="75">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Maximum', 'shortcodelic'); ?>:</h3>
        <div class="slider_div">
            <input type="text" data-name="maximum" value="100">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Color of the bar', 'shortcodelic'); ?>:</h3>
        <div class="pix_color_picker">
            <input type="text" data-name="barcolor" value="#ff9541">
            <a class="pix_button" href="#"></a>
            <div class="colorpicker"></div>
            <i class="scicon-elusive-cancel"></i>
        </div>

        <div class="clear"></div>

        <h3><?php _e('Color of the background track', 'shortcodelic'); ?>:</h3>
        <div class="pix_color_picker">
            <input type="text" data-name="trackcolor" value="#eeeeee">
            <a class="pix_button" href="#"></a>
            <div class="colorpicker"></div>
            <i class="scicon-elusive-cancel"></i>
        </div>

        <div class="clear"></div>

    </div>

    <div class="alignleft shortcodelic_opts_list_2">
        <h3><?php _e('Style', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select data-name="style">
                    <option value='bar'><?php _e('bar', 'shortcodelic'); ?></option>
                    <option value='pie'><?php _e('pie', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Bar ending look', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select data-name="linecap">
                    <option value=''><?php _e('squared', 'shortcodelic'); ?></option>
                    <option value='round'><?php _e('rounded', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Bar thickness', 'shortcodelic'); ?>:</h3>
        <div class="slider_div">
            <input type="text" data-name="linewidth" value="5">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Pie radius (for the charts pie only)', 'shortcodelic'); ?>:</h3>
        <div class="slider_div">
            <input type="text" data-name="size" value="100">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Animation (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <div class="slider_div milliseconds">
            <input type="text" data-name="animate" value="2000">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Class', 'shortcodelic'); ?>:</h3>
        <input type="text" data-name="class" value="" placeholder="Class for CSS, Javascript purpose">

        <div class="clear"></div>

        <h3><?php _e('ID', 'shortcodelic'); ?>:</h3>
        <input type="text" data-name="id" value="" placeholder="ID for CSS, Javascript purpose">

        <div class="clear"></div>

    </div>

</div><!-- #shortcodelic_progress_generator -->