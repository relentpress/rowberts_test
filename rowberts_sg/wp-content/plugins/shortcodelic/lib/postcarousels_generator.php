<?php
if ( isset($_POST['postid']) ) {
    $scpostid = $_POST['postid'];
    require_once('../../../../wp-load.php');
} else {
    $scpostid = $scpostid;
}
?>
<div id="shortcodelic_postcarousels_generator" class="shortcodelic_meta" data-title="<?php _e('Shortcode generator &rarr; Carousel', 'shortcodelic'); ?>">
<?php
$values = get_post_custom( $scpostid );
$shortcodelic_postcarousels = isset( $values['shortcodelic_postcarousels'] ) ? $values['shortcodelic_postcarousels'][0] : '';
$shortcodelic_postcarousels = maybe_unserialize($shortcodelic_postcarousels);
$ajax_nonce = wp_create_nonce('shortcodelic_postcarousels_nonce');
$postcarouselSize = '1';
$postcarouselClone = '1';
$notEmpty = '0';
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 1280;
?>
    <div class="alignleft shortcodelic_options_area">
        <script type="text/javascript">
            var shortcodelic_postcarousels_nonce = '<?php echo $ajax_nonce; ?>';
        </script>
        <h3><?php _e('Select among the postcarousels available on this post', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select id="shortcodelic_postcarousels_manager_loader" class="shortcodelic_shortcode_manager_loader">
                    <option value='' data-type="postcarousels"><?php _e('Create a new postcarousel', 'shortcodelic'); ?></option>
                    <?php if (is_array($shortcodelic_postcarousels)) {
                        $postcarouselSize = sizeof($shortcodelic_postcarousels)+1;
                        $postcarouselClone = sizeof($shortcodelic_postcarousels)+1;
                        for ($key = 1; $key <= sizeof($shortcodelic_postcarousels); $key++) {
                            if ( !empty($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$key]) ) {
                            $notEmpty = $notEmpty + 1; ?>
                        <option value='[shortcodelic-postcarousel postid="<?php echo $scpostid; ?>" carousel="<?php echo $key ?>"]' data-type="postcarousels" data-link='&amp;shortcodelic_postcarousel_length=<?php echo $key ?>'>[shortcodelic-postcarousel postid=&quot;<?php echo $scpostid; ?>&quot; carousel=&quot;<?php echo $key ?>&quot;]</option>
                            <?php } else {
                                $postcarouselSize = $postcarouselSize < $key ? $postcarouselSize : $key;
                                $postcarouselClone = $postcarouselClone < $key ? $postcarouselClone : $key;
                            }
                        }
                    } ?>

                    <?php if ( $notEmpty == 0 ) {
                        delete_post_meta($scpostid, 'shortcodelic_postcarousels');
                    } ?>
                    </select>
            </span>
        </label>

        <div class="clear"></div>

        <?php
            if ( isset($_POST['shortcodelic_postcarousel_length']) && $_POST['shortcodelic_postcarousel_length']!='' ) {
                $postcarouselSize = $_POST['shortcodelic_postcarousel_length'];
            }
        ?>

        <div class="shortcodelic_opts_part">
            <?php if ( isset($_POST['shortcodelic_postcarousel_length']) && $_POST['shortcodelic_postcarousel_length']!='' ) { ?>
                <div class="alignleft">
                    <h3><?php _e('Clone your carousel', 'shortcodelic'); ?>:</h3>
                    <a href="#" class="clone-set pix_button" data-type="postcarousels" data-oldpostcarousels="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>" data-postcarousels="shortcodelic_postcarousels_<?php echo $postcarouselClone; ?>" data-link="&amp;shortcodelic_postcarousel_length=<?php echo $postcarouselClone; ?>"><?php _e('Clone this carousel', 'shortcodelic'); ?></a>
                </div>
                <div class="alignright">
                    <h3><?php _e('Delete your carousel', 'shortcodelic'); ?>:</h3>
                    <a href="#" class="delete-set pix_button" data-type="postcarousels" data-postcarousels="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>"><?php _e('Delete this carousel', 'shortcodelic'); ?></a>
                </div>
            <?php } ?>
            <div class="clear"></div><hr>

            <h3><?php _e('Select from one or more categories', 'shortcodelic'); ?>:</h3>
            <?php $cats = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['cats']) && is_array($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['cats']) ? $shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['cats'] : array();
            ?>
            <select name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[cats][]" multiple="multiple">
                <?php 
                shortcodelicHierCatSel( 0, $cats, '' );
                function shortcodelicHierCatSel( $par, $cats, $ind ) {
                    $argcats = array(
                        'hide_empty' => 0,
                        'orderby' => 'name',
                        'hierarchical' => 0,
                        'order' => 'ASC',
                        'parent' => $par
                    );
                    $categories = get_categories($argcats);
                    if ( $categories ) {
                        if ( $par == 0 ) {
                            $ind = '';
                        } else {
                            $ind .= '&#8212;';
                        }
                        foreach ($categories as $category) { ?>
                            <option value='<?php echo $category->term_id; ?>' <?php selected( true, in_array($category->term_id, $cats) ); ?>> <?php echo $ind.$category->cat_name; ?></option>
                        <?php shortcodelicHierCatSel( $category->term_id, $cats, $ind );
                        }
                    }
                }
                ?>
            </select>
            
            <div class="clear"></div>

            <h3><?php _e('Include posts', 'shortcodelic'); ?>:</h3>
            <?php $included = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['included']) && is_array($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['included']) ? $shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['included'] : array();
            ?>
            <select name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[included][]" multiple="multiple">
                <?php 
                    $args = array( 
                        'posts_per_page' => -1,
                    );
                    $posts = get_posts( $args );
                    foreach ( $posts as $postloop ) { ?>
                        <option value='<?php echo $postloop->ID; ?>' <?php selected( true, in_array($postloop->ID, $included) ); ?>> <?php echo $postloop->ID.'- '.$postloop->post_title; ?></option>
                    <?php } 
                    wp_reset_postdata();
                ?>
            </select>
            
            <div class="clear"></div>

            <h3><?php _e('Exclude posts', 'shortcodelic'); ?>:</h3>
            <?php $excluded = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['excluded']) && is_array($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['excluded']) ? $shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['excluded'] : array();
            ?>
            <select name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[excluded][]" multiple="multiple">
                <?php 
                    $args = array( 
                        'posts_per_page' => -1,
                    );
                    $posts = get_posts( $args );
                    foreach ( $posts as $postloop ) { ?>
                        <option value='<?php echo $postloop->ID; ?>' <?php selected( true, in_array($postloop->ID, $excluded) ); ?>> <?php echo $postloop->ID.'- '.$postloop->post_title; ?></option>
                    <?php } 
                    wp_reset_postdata();
                ?>
            </select>
            
            <div class="clear"></div>

            <h3><?php _e('Order', 'shortcodelic'); ?>:</h3>
            <?php $order = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['order']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['order']) : 'DESC'; ?>
            <label class="for_select marginhack">
                <span class="for_select">
                    <select name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[order]">
                        <option value='DESC' <?php selected( $order, 'DESC' ); ?>><?php _e('DESC', 'shortcodelic'); ?></option>
                        <option value='ASC' <?php selected( $order, 'ASC' ); ?>><?php _e('ASC', 'shortcodelic'); ?></option>
                    </select>
                </span>
            </label>

            <div class="clear"></div>

            <h3><?php _e('Order by', 'shortcodelic'); ?>:</h3>
            <?php $orderby = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['orderby']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['orderby']) : 'date'; ?>
            <label class="for_select marginhack">
                <span class="for_select">
                    <select name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[orderby]">
                        <option value='date' <?php selected( $orderby, 'date' ); ?>><?php _e('date', 'shortcodelic'); ?></option>
                        <option value='ID' <?php selected( $orderby, 'ID' ); ?>><?php _e('ID', 'shortcodelic'); ?></option>
                        <option value='author' <?php selected( $orderby, 'author' ); ?>><?php _e('author', 'shortcodelic'); ?></option>
                        <option value='title' <?php selected( $orderby, 'title' ); ?>><?php _e('title', 'shortcodelic'); ?></option>
                        <option value='name' <?php selected( $orderby, 'name' ); ?>><?php _e('name', 'shortcodelic'); ?></option>
                        <option value='modified' <?php selected( $orderby, 'modified' ); ?>><?php _e('modified', 'shortcodelic'); ?></option>
                        <option value='parent' <?php selected( $orderby, 'parent' ); ?>><?php _e('parent', 'shortcodelic'); ?></option>
                        <option value='rand' <?php selected( $orderby, 'rand' ); ?>><?php _e('rand', 'shortcodelic'); ?></option>
                        <option value='comment_count' <?php selected( $orderby, 'comment_count' ); ?>><?php _e('comment_count', 'shortcodelic'); ?></option>
                        <option value='menu_order' <?php selected( $orderby, 'menu_order' ); ?>><?php _e('menu_order', 'shortcodelic'); ?></option>
                        <option value='none' <?php selected( $orderby, 'none' ); ?>><?php _e('none', 'shortcodelic'); ?></option>
                    </select>
                </span>
            </label>

            <div class="clear"></div>

        </div><!-- .shortcodelic_opts_part -->
    </div>

    <div class="alignleft shortcodelic_opts_list_2">

        <h3><?php _e('Shortcode <small>(if you want to use it on a different page/post)</small>', 'shortcodelic'); ?>:</h3>
        <input type="text" disabled="disabled" id="shortcodelic_get_sc" value="[shortcodelic-postcarousel postid=&quot;<?php echo $scpostid; ?>&quot; carousel=&quot;<?php echo $postcarouselSize; ?>&quot;]">

        <div class="clear"></div>

        <h3><?php _e('Mode', 'shortcodelic'); ?>:</h3>
        <?php $mode = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['mode']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['mode']) : 'horizontal'; ?>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[mode]">
                    <option value='horizontal' <?php selected( $mode, 'horizontal' ); ?>><?php _e('horizontal', 'shortcodelic'); ?></option>
                    <option value='vertical' <?php selected( $mode, 'vertical' ); ?>><?php _e('vertical', 'shortcodelic'); ?></option>
                    <option value='fade' <?php selected( $mode, 'fade' ); ?>><?php _e('fade', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Slide width (in pixels)', 'shortcodelic'); ?>:</h3>
        <?php $slidewidth = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['slidewidth']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['slidewidth']) : floor(($content_width+(20*3))/4); ?>
        <div class="slider_div">
            <input type="text" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[slidewidth]" value="<?php echo $slidewidth; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Timeout (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <?php $timeout = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['timeout']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['timeout']) : '7000'; ?>
        <div class="slider_div milliseconds">
            <input type="text" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[timeout]" value="<?php echo $timeout; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Speed (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <?php $speed = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['speed']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['speed']) : '750'; ?>
        <div class="slider_div milliseconds">
            <input type="text" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[speed]" value="<?php echo $speed; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <?php $post_title = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['post_title']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['post_title']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>post_title"><?php _e('Display the post title', 'shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[post_title]" value="0">
            <input type="checkbox" id="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>post_title" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[post_title]" value="true" <?php checked( $post_title, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $featured_img = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['featured_img']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['featured_img']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>featured_img"><?php _e('Display the featured image', 'shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[featured_img]" value="0">
            <input type="checkbox" id="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>featured_img" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[featured_img]" value="true" <?php checked( $featured_img, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $post_excerpt = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['post_excerpt']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['post_excerpt']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>post_excerpt"><?php _e('Display the post excerpt', 'shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[post_excerpt]" value="0">
            <input type="checkbox" id="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>post_excerpt" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[post_excerpt]" value="true" <?php checked( $post_excerpt, 'true' ); ?>>
            <span></span>
        </label></h3>

        <div class="clear"></div>

        <h3><?php _e('Featured image size', 'shortcodelic'); ?>:</h3>
        <?php $size = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['size']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['size']) : 'large'; ?>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[size]">
                    <?php $size_names = apply_filters( 'image_size_names_choose', array('thumbnail' => __('Thumbnail'), 'medium' => __('Medium'), 'large' => __('Large'), 'full' => __('Full Size')) );
                    foreach ( $size_names as $sizeTh => $labelTh ) { ?>
                        <option value='<?php echo $sizeTh; ?>' <?php selected( $size, $sizeTh ); ?>><?php echo $labelTh; ?></option>
                    <?php } ?>
                </select>
            </span>
        </label>

        <div class="clear"></div>

    </div>

    <div class="alignleft shortcodelic_opts_list">
        
        <h3><?php _e('Number of posts', 'shortcodelic'); ?>:</h3>
        <?php $ppp = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['ppp']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['ppp']) : '10'; ?>
        <div class="slider_div percent">
            <input type="text" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[ppp]" value="<?php echo $ppp; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Minimum number of slides', 'shortcodelic'); ?>:</h3>
        <?php $minslides = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['minslides']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['minslides']) : 4; ?>
        <div class="slider_div stroke">
            <input type="text" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[minslides]" value="<?php echo $minslides; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Maximum number of slides', 'shortcodelic'); ?>:</h3>
        <?php $maxslides = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['maxslides']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['maxslides']) : 4; ?>
        <div class="slider_div stroke">
            <input type="text" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[maxslides]" value="<?php echo $maxslides; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Margin between each slide', 'shortcodelic'); ?>:</h3>
        <?php $slidemargin = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['slidemargin']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['slidemargin']) : 20; ?>
        <div class="slider_div grid">
            <input type="text" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[slidemargin]" value="<?php echo $slidemargin; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <?php $autoplay = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['autoplay']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['autoplay']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>_autoplay"><?php _e('Autoplay', 'shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[autoplay]" value="0">
            <input type="checkbox" id="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>_autoplay" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[autoplay]" value="true" <?php checked( $autoplay, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $hover = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['hover']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['hover']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>_hover"><?php _e('Pause on hover', 'shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[hover]" value="0">
            <input type="checkbox" id="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>_hover" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[hover]" value="true" <?php checked( $hover, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $play_pause = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['play_pause']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['play_pause']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>_play_pause"><?php _e('Play/pause button','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[play_pause]" value="0">
            <input type="checkbox" id="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>_play_pause" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[play_pause]" value="true" <?php checked( $play_pause, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $prev_next = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['prev_next']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['prev_next']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>_prev_next"><?php _e('Prev/next arrows','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[prev_next]" value="0">
            <input type="checkbox" id="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>_prev_next" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[prev_next]" value="true" <?php checked( $prev_next, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $bullets = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['bullets']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['bullets']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>_bullets"><?php _e('Pagination bullets','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[bullets]" value="0">
            <input type="checkbox" id="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>_bullets" name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[bullets]" value="true" <?php checked( $bullets, 'true' ); ?>>
            <span></span>
        </label></h3>

        <div class="clear"></div>
        <br>

        <h3><?php _e('Point the featured image to', 'shortcodelic'); ?>:</h3>
        <?php $link = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['link']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['link']) : 'image'; ?>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[link]">
                    <option value='' <?php selected( $link, '' ); ?>><?php _e('nowhere', 'shortcodelic'); ?></option>
                    <option value='post' <?php selected( $link, 'post' ); ?>><?php _e('the post page', 'shortcodelic'); ?></option>
                    <option value='image' <?php selected( $link, 'image' ); ?>><?php _e('the image', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Point the title to', 'shortcodelic'); ?>:</h3>
        <?php $link = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['linktitle']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['linktitle']) : 'post'; ?>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="shortcodelic_postcarousels_<?php echo $postcarouselSize; ?>[linktitle]">
                    <option value='' <?php selected( $link, '' ); ?>><?php _e('nowhere', 'shortcodelic'); ?></option>
                    <option value='post' <?php selected( $link, 'post' ); ?>><?php _e('the post page', 'shortcodelic'); ?></option>
                    <option value='image' <?php selected( $link, 'image' ); ?>><?php _e('the image (if available)', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

    </div><!-- .shortcodelic_opts_list -->
</div><!-- #shortcodelic_postcarousel_generator -->