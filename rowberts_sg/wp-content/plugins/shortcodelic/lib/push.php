<?php
	if ( ! function_exists( 'pix_hex2rgbcompiled' ) ) :
	function pix_hex2rgbcompiled($hex) {
	   $hex = str_replace("#", "", $hex);

	   if(strlen($hex) == 3) {
	      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
	      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
	      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	   } else {
	      $r = hexdec(substr($hex,0,2));
	      $g = hexdec(substr($hex,2,2));
	      $b = hexdec(substr($hex,4,2));
	   }
	   $rgb = array($r, $g, $b);
	   return $rgb[0].','.$rgb[1].','.$rgb[2];
	}
	endif;
	$bgpager = pix_hex2rgbcompiled(stripslashes(get_option('shortcodelic_carousel_commands_bg')));
	$tooltipshadow = pix_hex2rgbcompiled(stripslashes(get_option('shortcodelic_tooltip_shadow_color')));
    $shortcodelic_array_buttons_ = get_option('shortcodelic_array_buttons_');
    $shortcodelic_array_boxes_ = get_option('shortcodelic_array_boxes_');

  $css = get_option('shortcodelic_css_code');

  $css .= "\n/*##############################
#
#	TABS and ACCORDIONS
#
##############################*/
.tabedelic .tabedelic-header {
	border-color: ".stripslashes(get_option('shortcodelic_tab_border_color')).";
}
.tabedelic-headers a {
	color: ".stripslashes(get_option('shortcodelic_tab_color')).";
}
.tabedelic.simple > .tabedelic-headers {
	margin-bottom: -".stripslashes(get_option('shortcodelic_tab_border_width'))."px;
}
.tabedelic.accordion > .tabedelic-headers {
	padding-top: ".stripslashes(get_option('shortcodelic_tab_border_width'))."px;
}
.tabedelic > .tabedelic-headers > .tabedelic-header {
	background: ".stripslashes(get_option('shortcodelic_tab_bg_color')).";
	border: ".stripslashes(get_option('shortcodelic_tab_border_width'))."px solid ".stripslashes(get_option('shortcodelic_tab_border_color')).";
}
.tabedelic.simple > .tabedelic-headers > .tabedelic-header:last-child {
	border-right-width: ".stripslashes(get_option('shortcodelic_tab_border_width'))."px;
}
.tabedelic.accordion {
	padding-top: ".stripslashes(get_option('shortcodelic_tab_border_width'))."px;
}
.tabedelic.accordion > .tabedelic-headers > .tabedelic-header {
	margin-top: -".stripslashes(get_option('shortcodelic_tab_border_width'))."px;
}
.tabedelic.accordion > .tabedelic-headers > .tabedelic-header:last-child {
	border-bottom-color: ".stripslashes(get_option('shortcodelic_tab_border_color'))."; 
}
.tabs.columned.tabedelic > .tabedelic-headers > .tabedelic-header:last-child {
	border-bottom-width: ".stripslashes(get_option('shortcodelic_tab_border_width'))."px; 
}
.tabedelic > .tabedelic-headers > .header-active {
	background: ".stripslashes(get_option('shortcodelic_panel_bg_color')).";
}
.tabedelic.simple > .tabedelic-headers > .header-active:after {
	background: ".stripslashes(get_option('shortcodelic_panel_bg_color')).";
	bottom: -".stripslashes(get_option('shortcodelic_tab_border_width'))."px;
	height: ".stripslashes(get_option('shortcodelic_tab_border_width'))."px;
}
.tabs.tabsleft > .tabedelic-headers > .header-active {
	border-bottom-width: ".stripslashes(get_option('shortcodelic_tab_border_width'))."px;
}
.tabs.tabsright > .tabedelic-headers > .header-active {
	border-bottom-width: ".stripslashes(get_option('shortcodelic_tab_border_width'))."px;
}
.tabs.tabsright > .tabedelic-headers > .tabedelic-header > .icon-tab-open {
	left: ".stripslashes(get_option('shortcodelic_tab_border_width'))."px;
}
.tabedelic .tabedelic-panel {
	background: ".stripslashes(get_option('shortcodelic_panel_bg_color')).";
	border: ".stripslashes(get_option('shortcodelic_tab_border_width'))."px solid ".stripslashes(get_option('shortcodelic_tab_border_color')).";
	color: ".stripslashes(get_option('shortcodelic_panel_text_color')).";
}
.tabedelic.accordion > .tabedelic-headers > .tabedelic-panel {
	top: -".stripslashes(get_option('shortcodelic_tab_border_width'))."px;
}
.tabedelic.accordion > .tabedelic-headers > .tabedelic-panel:last-child {
	border-bottom-width: ".stripslashes(get_option('shortcodelic_tab_border_width'))."px;
}
";

	$css .= "
/*##############################
#
#	TABLES
#
##############################*/
.pix_tables div.table_column {
	padding: 0 ".(stripslashes(get_option('shortcodelic_pricetable_gutter'))/5)."px;
}
.pix_tables .table_column > div .table-cell {
	background-color: ".stripslashes(get_option('shortcodelic_pricetable_background_color')).";
	border: ".stripslashes(get_option('shortcodelic_pricetable_border_width'))."px solid ".stripslashes(get_option('shortcodelic_pricetable_border_color')).";
	color: ".stripslashes(get_option('shortcodelic_pricetable_text_color')).";
	margin-top: -".stripslashes(get_option('shortcodelic_pricetable_border_width'))."px
}
.pix_tables .table_column > div .table-cell.odd {
	background-color: ".stripslashes(get_option('shortcodelic_pricetable_background_color_alt')).";
	border: ".stripslashes(get_option('shortcodelicpricing_border_width_alt'))."px solid ".stripslashes(get_option('shortcodelic_pricetable_border_color_alt')).";
	color: ".stripslashes(get_option('shortcodelic_pricetable_text_color_alt')).";
}
.pix_tables div.table_column > div .table-cell.heading,
.pix_tables div.table_column > div .table-cell.odd.heading {
	background-color: ".stripslashes(get_option('shortcodelic_heading_background_color')).";
	border: ".stripslashes(get_option('shortcodelic_heading_border_width'))."px solid ".stripslashes(get_option('shortcodelic_heading_border_color')).";
	color: ".stripslashes(get_option('shortcodelic_heading_text_color')).";
	margin-top: -".stripslashes(get_option('shortcodelic_heading_border_width'))."px
}
.pix_tables div.table_column > div .table-cell.pricing,
.pix_tables div.table_column > div .table-cell.odd.pricing {
	background-color: ".stripslashes(get_option('shortcodelic_pricing_background_color')).";
	border: ".stripslashes(get_option('shortcodelic_pricing_border_width'))."px solid ".stripslashes(get_option('shortcodelic_pricing_border_color')).";
	color: ".stripslashes(get_option('shortcodelic_pricing_text_color')).";
	margin-top: -".stripslashes(get_option('shortcodelic_pricing_border_width'))."px
}
.pix_tables .table_column:first-child {
	border-left-width: 1px;
}
.pix_tables td,
.pix_tables th,
.entry-content .pix_tables th,
.entry-content .pix_tables td {
	background-color: ".stripslashes(get_option('shortcodelic_table_background_color')).";
	border-left: ".stripslashes(get_option('shortcodelic_table_border_width'))."px solid ".stripslashes(get_option('shortcodelic_table_border_color')).";
	border-top: ".stripslashes(get_option('shortcodelic_table_border_width'))."px solid ".stripslashes(get_option('shortcodelic_table_border_color')).";
	color: ".stripslashes(get_option('shortcodelic_table_text_color')).";
}
.pix_tables.alt td,
.pix_tables.alt th,
.entry-content .pix_tables.alt th,
.entry-content .pix_tables.alt td {
	border: ".stripslashes(get_option('shortcodelic_table_border_width'))."px solid ".stripslashes(get_option('shortcodelic_table_border_color')).";
	border-bottom-width: 0;
}
.pix_tables.off tr:last-child td,
.pix_tables.off tr:last-child th,
.entry-content .pix_tables.off tr:last-child th,
.entry-content .pix_tables.off tr:last-child td {
	border-bottom: ".stripslashes(get_option('shortcodelic_table_border_width'))."px solid ".stripslashes(get_option('shortcodelic_table_border_color')).";
}
.pix_tables tr.odd th,
.pix_tables tr.odd td,
.entry-content .pix_tables tr.odd th,
.entry-content .pix_tables tr.odd td {
	background-color: ".stripslashes(get_option('shortcodelic_table_background_color_alt')).";
	border-left: ".stripslashes(get_option('shortcodelic_table_border_width'))."px solid ".stripslashes(get_option('shortcodelic_table_border_color_alt')).";
	border-top: ".stripslashes(get_option('shortcodelic_table_border_width_alt'))."px solid ".stripslashes(get_option('shortcodelic_table_border_color_alt')).";
	color: ".stripslashes(get_option('shortcodelic_table_text_color_alt')).";
}
.pix_tables.alt th.odd,
.pix_tables.alt th.odd,
.entry-content .pix_tables.alt th.odd,
.entry-content .pix_tables.alt td.odd {
	background-color: ".stripslashes(get_option('shortcodelic_table_background_color_alt')).";
	border: ".stripslashes(get_option('shortcodelic_table_border_width'))."px solid ".stripslashes(get_option('shortcodelic_table_border_color_alt')).";
	color: ".stripslashes(get_option('shortcodelic_table_text_color_alt')).";
	border-bottom-width: 0;
}
.pix_tables.alt tr td:last-child,
.entry-content .pix_tables.alt tr td:last-child {
	border-bottom-width: 1px;
	margin-bottom: 5px;
}
.pix_tables.off tr td:last-child,
.pix_tables.off tr th:last-child,
.entry-content .pix_tables.off tr th:last-child,
.entry-content .pix_tables.off tr td:last-child {
	border-right: ".stripslashes(get_option('shortcodelic_table_border_width'))."px solid ".stripslashes(get_option('shortcodelic_table_border_color')).";
}
.pix_tables.minwidth-reached .table_column {
	margin-bottom: -1px;
}";

	$css .= "
/*##############################
#
#	CAROUSELS
#
##############################*/
.bx-wrapper .bx-controls-direction a {
  background: rgba($bgpager,".stripslashes(get_option('shortcodelic_carousel_commands_opacity')).");
  border: 3px solid ".stripslashes(get_option('shortcodelic_carousel_commands_fore')).";
  color: ".stripslashes(get_option('shortcodelic_carousel_commands_fore')).";
}
.bx-wrapper .bx-pager.bx-default-pager a {
	background: ".stripslashes(get_option('shortcodelic_carousel_nav')).";
	border: 2px solid ".stripslashes(get_option('shortcodelic_carousel_nav')).";
}
.bx-wrapper .bx-controls-auto .bx-start,
.bx-wrapper .bx-controls-auto .bx-stop {
	color: ".stripslashes(get_option('shortcodelic_carousel_nav')).";
}";

	$css .= "
/*##############################
#
#	TOOLTIPS
#
##############################*/
.tooltipster-default {
	background: ".stripslashes(get_option('shortcodelic_tooltip_bg')).";
	border-radius: ".stripslashes(get_option('shortcodelic_tooltip_radius'))."px; 
	border: ".stripslashes(get_option('shortcodelic_tooltip_border'))."px solid ".stripslashes(get_option('shortcodelic_tooltip_border_color')).";
	box-shadow: 0px 0px ".stripslashes(get_option('shortcodelic_tooltip_shadow_size'))."px rgba($tooltipshadow,".stripslashes(get_option('shortcodelic_tooltip_shadow_opacity')).");
	color: ".stripslashes(get_option('shortcodelic_tooltip_color')).";
}
.tooltipster-default a {
	color: ".stripslashes(get_option('shortcodelic_tooltip_link')).";
}";

	$css .= "
/*##############################
#
#	PROGRESS BARS
#
##############################*/
.pix_progress_bar .chart-amount,
.pix_progress_bar .chart-label,
.pix_progress_pie .chart-label {
  background: ".stripslashes(get_option('shortcodelic_progress_tooltip_bg')).";
  color: ".stripslashes(get_option('shortcodelic_progress_tooltip_color')).";
}
.pix_progress_bar .chart-amount:after {
  border-top: 5px solid ".stripslashes(get_option('shortcodelic_progress_tooltip_bg')).";
}";

	$css .= "
/*##############################
#
#	BUTTONS
#
##############################*/
.buttonelic {
	background-color: #ff9541;
	background-image: url(../images/blank.gif);
	background-repeat: repeat;
	color: #ffffff!important;
	-moz-border-radius: 4px;
	border-radius: 4px;
	display: inline-block;
	overflow: hidden;
	padding: .5em 1em;
	position: relative;
	text-decoration: none;
	vertical-align: middle;
	-webkit-transition: all 100ms ease-out!important;
	transition: all 100ms ease-out!important;
}
.buttonelic:hover, .buttonelic:active, .buttonelic:visited {
	background-color: #ff9541;
	color: #ffffff;
}
.buttonelic > span:first-child {
	display: inline-block;
	position: relative;
	z-index: 1;
	-webkit-transition: all 200ms ease-out!important;
	transition: all 200ms ease-out!important;
}
.buttonelic > span:last-child {
	bottom: 0;
	display: block;
	left: 50%;
	opacity: 0;
	position: absolute;
	right: 50%;
	top: 0;
	z-index: 0;
	-webkit-transition: all 200ms ease-out!important;
	transition: all 200ms ease-out!important;
}
.buttonelic [class*=\"scicon-\"] {
	display: inline-block;
	-webkit-transition: all 200ms ease-out!important;
	transition: all 200ms ease-out!important;
	vertical-align: middle;
}
.buttonelic [class*=\"scicon-\"]:before, .buttonelic [class*=\"scicon-\"]:after {
	line-height: inherit;
}
";

if (isset($shortcodelic_array_buttons_) && is_array($shortcodelic_array_buttons_)) foreach ($shortcodelic_array_buttons_ as $key => $value) {
	$css .= "
.buttonelic.$key {
	background-color: ".esc_attr($shortcodelic_array_buttons_[$key]['background'])."!important;
	border: ".esc_attr($shortcodelic_array_buttons_[$key]['borderwidth'])."px solid ".esc_attr($shortcodelic_array_buttons_[$key]['bordercolor'])."!important;
	color: ".esc_attr($shortcodelic_array_buttons_[$key]['color'])."!important;
	-moz-border-radius: ".esc_attr($shortcodelic_array_buttons_[$key]['borderradius'])."px;
	border-radius: ".esc_attr($shortcodelic_array_buttons_[$key]['borderradius'])."px;
}
.buttonelic.$key > span:first-child {
	color: ".esc_attr($shortcodelic_array_buttons_[$key]['color'])."!important;
}
.buttonelic.$key:hover > span:first-child {
	color: ".esc_attr($shortcodelic_array_buttons_[$key]['texthover'])."!important;
}

".esc_attr($shortcodelic_array_buttons_[$key]['style']);

	if ( esc_attr($shortcodelic_array_buttons_[$key]['icon']) == 'hover' ) {
	$css .= "
.buttonelic.$key.buttonicon:hover > span:first-child {
	color: ".esc_attr($shortcodelic_array_buttons_[$key]['bghover'])."!important;
	/*top: 2em;*/
	-webkit-transform: scale(0,0);
	transform: scale(0,0);
}
.buttonelic.$key [class*=\"scicon-\"] {
	color: ".esc_attr($shortcodelic_array_buttons_[$key]['background'])."!important;
	display: block;
	height: 100%;
	left: 0;
	position: absolute;
	text-align: center;
	width: 100%;
	top: 2em;
	z-index: 1;
}
.buttonelic.$key:hover [class*=\"scicon-\"] {
	color: ".esc_attr($shortcodelic_array_buttons_[$key]['texthover'])."!important;
	top: 0;
}";	}

	if ( esc_attr($shortcodelic_array_buttons_[$key]['fx']) == 'expand' ) {
	$css .= "
.buttonelic.$key > span:last-child {
	background-color: ".esc_attr($shortcodelic_array_buttons_[$key]['bghover'])."!important;
	bottom: -".esc_attr($shortcodelic_array_buttons_[$key]['borderwidth'])."px;
	top: -".esc_attr($shortcodelic_array_buttons_[$key]['borderwidth'])."px;
}
.buttonelic.$key:hover > span:last-child {
	-moz-border-radius: ".esc_attr($shortcodelic_array_buttons_[$key]['borderradius'])."px;
	border-radius: ".esc_attr($shortcodelic_array_buttons_[$key]['borderradius'])."px;
	left: -".esc_attr($shortcodelic_array_buttons_[$key]['borderwidth'])."px;
	opacity: 1;
	right: -".esc_attr($shortcodelic_array_buttons_[$key]['borderwidth'])."px;
}
.buttonelic.$key:hover {
	border-color: ".esc_attr($shortcodelic_array_buttons_[$key]['bordercolorhover'])."!important;
	color: ".esc_attr($shortcodelic_array_buttons_[$key]['texthover'])."!important;
}";	} else {
	$css .= "
.buttonelic.$key:hover {
	background-color: ".esc_attr($shortcodelic_array_buttons_[$key]['bghover'])."!important;
	border-color: ".esc_attr($shortcodelic_array_buttons_[$key]['bordercolorhover'])."!important;
	color: ".esc_attr($shortcodelic_array_buttons_[$key]['texthover'])."!important;
}";	
}
}

	$css .= "
/*##############################
#
#	TEXT BOXES
#
##############################*/
";
if (isset($shortcodelic_array_boxes_) && is_array($shortcodelic_array_boxes_)) foreach ($shortcodelic_array_boxes_ as $key => $value) {
	$css .= "
.pix_box.$key {
	background-color: ".esc_attr($shortcodelic_array_boxes_[$key]['background'])."!important;
	border: ".esc_attr($shortcodelic_array_boxes_[$key]['borderwidth'])."px solid ".esc_attr($shortcodelic_array_boxes_[$key]['bordercolor']).";
	color: ".esc_attr($shortcodelic_array_boxes_[$key]['color'])."!important;
	-moz-border-radius: ".esc_attr($shortcodelic_array_boxes_[$key]['borderradius'])."px;
	border-radius: ".esc_attr($shortcodelic_array_boxes_[$key]['borderradius'])."px;
}
.pix_box.$key .close-box-sc {
	background: ".esc_attr($shortcodelic_array_boxes_[$key]['bordercolor'])."!important;
	color: ".esc_attr($shortcodelic_array_boxes_[$key]['background'])."!important;
	-moz-border-radius-topright: ".esc_attr($shortcodelic_array_boxes_[$key]['borderradius'])."px;
	border-top-right-radius: ".(esc_attr($shortcodelic_array_boxes_[$key]['borderradius'])-esc_attr($shortcodelic_array_boxes_[$key]['borderwidth']))."px;
}
".esc_attr($shortcodelic_array_boxes_[$key]['style']);
}


	if ( function_exists('geode_min_css') ) {

		$css = geode_min_css($css);

	}
	echo $css; 
?>