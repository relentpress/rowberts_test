<div id="shortcodelic_fonticons_generator" class="shortcodelic_meta" data-title="<?php _e('Font icon selector', 'shortcodelic'); ?>">
    <div class="alignleft shortcodelic_options_area">

        <h3><?php _e('Select a font set (from <a href="http://fontello.com/" target="_blank">Fontello.com</a> and <a href="https://fontastic.me/" target="_blank">Fontastic.com</a>)', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select select_font_set">
                <select>
                    <option value="awesome">Font Awesome (by Dave Gandy, license SIL)</option>
                    <option value="entypo">Entypo (by Daniel Bruce, license CC BY-SA)</option>
                    <option value="typicons">Typicons (by Stephen Hutchings, license CC BY-SA 3.0)</option>
                    <option value="iconic">Iconic (by P.J. Onori, license SIL)</option>
                    <option value="modern">Modern Pictograms (by John Caserta, license SIL)</option>
                    <option value="meteocons">Meteocons (by Alessio Atzeni, license SIL)</option>
                    <option value="mfg">MFG Labs (by MFG Labs, license SIL)</option>
                    <option value="maki">Maki (by Mapbox, license BSD)</option>
                    <option value="zocial">Zocial (by Sam Collins, license MIT)</option>
                    <option value="brandico">Brandico (by Crowdsourced for Fontello project, license SIL)</option>
                    <option value="elusive">Elusive (by Aristeides Stathopoulos, license SIL)</option>
                    <option value="linecons">Linecons (by Designmodo for Smashing Magazine, license CC BY)</option>
                    <option value="websymbols">Web Symbols (by Just Be Nice studio, license SIL)</option>
                    <option value="streamline">Streamline (by Webalis, license SIL)</option>
                    <option value="foundation">Foundation (by Zurb, license CC BY)</option>
                    <option value="steadysets">Steadysets (by Shapemade, license SIL)</option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Size', 'shortcodelic'); ?>:</h3>
        <div class="slider_div heading">
            <input type="text" data-name="size" value="14">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Icon color', 'shortcodelic'); ?>:</h3>
        <div class="pix_color_picker">
            <input type="text" data-name="color" value="inherit">
            <a class="pix_button" href="#"></a>
            <div class="colorpicker"></div>
            <i class="scicon-elusive-cancel"></i>
        </div>

        <div class="clear"></div>

        <h3><?php _e('Background color (when available)', 'shortcodelic'); ?>:</h3>
        <div class="pix_color_picker">
            <input type="text" data-name="bg" value="transparent">
            <a class="pix_button" href="#"></a>
            <div class="colorpicker"></div>
            <i class="scicon-elusive-cancel"></i>
        </div>

        <div class="clear"></div>

        <h3><?php _e('Style', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select data-name="style">
                    <option value=""><?php _e('clean', 'shortcodelic'); ?></option>
                    <option value="disc"><?php _e('disc', 'shortcodelic'); ?></option>
                    <option value="circle"><?php _e('circle', 'shortcodelic'); ?></option>
                    <option value="rhombus"><?php _e('rhombus', 'shortcodelic'); ?></option>
                    <option value="rhombus-outline"><?php _e('rhombus-outline', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

    </div>

    <div class="alignleft shortcodelic_opts_list_2 shortcodelic_font_list_wrap">

        <h3><?php _e('Find an icon', 'shortcodelic'); ?>:</h3>
        <input type="text" value="" placeholder="<?php _e('enter a keyword', 'shortcodelic'); ?>">
        <div class="clear"></div><hr>

        <div class="shortcodelic_font_list">

<?php /*********** SLIDESHOW GENERATOR ***********/

require_once( SHORTCODELIC_PATH . 'font/scicon-awesome.php' );

?>

        </div><!-- .shortcodelic_font_list -->
    </div>

</div><!-- #shortcodelic_tabs_generator -->