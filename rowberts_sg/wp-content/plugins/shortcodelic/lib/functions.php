<?php

class ShortCodelic{

	/**
	 * @since   2.0.1
	 *
	 * @var     string
	 */
	protected $version = '2.0.1';

	/**
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_slug = 'shortcodelic';

	/**
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_name = 'ShortCodelic';

	/**
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	public function __construct() {
		add_action( 'init', array( &$this, 'load_plugin_textdomain' ) );
		add_action( 'admin_footer', array( &$this, 'check_version' ) );
		add_action( 'admin_menu', array( &$this, 'add_menu' ) );
		add_action( 'admin_head', array( &$this, 'add_shortcodelic_buttons' ) );
		add_filter( 'mce_css', array( &$this, 'add_tinymce_css' ) );
		add_action( 'admin_enqueue_scripts', array( &$this, 'admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( &$this, 'admin_scripts' ) );
		add_action( 'add_meta_boxes', array( &$this, 'add_meta' ) );
		add_action( 'save_post', array( &$this, 'content_save' ) );
		add_action( 'wp_ajax_shortocodelic_data_save', array( &$this, 'save_via_ajax' ) );
		add_action( 'wp_ajax_css_shortcodelic_ajax', array( &$this, 'compile_css_ajax' ) );
		add_action( 'admin_head', array( &$this, 'remove_subpages' ) );
		add_action( 'admin_head', array( &$this, 'js_vars' ) );
		add_action( 'loop_start', array( &$this, 'shortcodelic_remove_sharedaddy' ) );
		add_action( 'wp_head', array( &$this, 'drag_slides' ) );

		add_action( 'wp_ajax_shortcodelic_update_meta', array( &$this, 'ajax_update_meta' ) );
		add_action( 'wp_ajax_shortcodelic_get_thumb', array( &$this, 'ajax_get_thumb' ) );
		add_action( 'wp_ajax_shortcodelic_sanitize', array( &$this, 'sanitize_ajax' ) );
		add_action( 'wp_ajax_download_css', array( &$this, 'download_css' ) );

		add_action( 'wp_enqueue_scripts', array( &$this, 'front_scripts' ) );
		add_action( 'wp_enqueue_scripts', array( &$this, 'front_styles' ) );
		add_action( 'wp_head', array( &$this, 'conditional_scripts' ) );
		add_filter( 'body_class', array( &$this, 'body_class' ) );

		add_filter( 'the_content', array( &$this, 'filter_content' ), 10 );
		add_action( 'the_content', array( &$this, 'shortcodelic_append_sharedaddy' ));

		add_shortcode('shortcodelic-slideshow', array( &$this, 'shortcodelicSlideshowSC') );
		add_shortcode('shortcodelic-tabs', array( &$this, 'shortcodelicTabsSC') );
		add_shortcode('shortcodelic-tables', array( &$this, 'shortcodelicTablesSC') );
		add_shortcode('shortcodelic-maps', array( &$this, 'shortcodelicMapsSC') );
		add_shortcode('shortcodelic-carousel', array( &$this, 'shortcodelicCarouselsSC') );
		add_shortcode('shortcodelic-postcarousel', array( &$this, 'shortcodelicPostCarouselsSC') );
		add_shortcode('shortcodelic-woocarousel', array( &$this, 'shortcodelicWooCarouselsSC') );
		add_shortcode('shortcodelic-progress', array( &$this, 'shortcodelicProgressSC') );
		add_shortcode('shortcodelic-box', array( &$this, 'shortcodelicBoxSC') );

		add_filter( 'manage_posts_columns', array( &$this, 'add_pp_col' ) );
		add_action( 'manage_posts_custom_column', array( &$this, 'add_pp_value' ), 100, 2 );
		add_filter( 'manage_page_posts_columns', array( &$this, 'add_pp_col' )  );
		add_action( 'manage_page_posts_custom_column', array( &$this, 'add_pp_value' ), 100, 2 );

		add_filter( 'pre_set_site_transient_update_plugins', array( &$this, 'check_for_plugin_update' ));
		add_filter( 'plugins_api', array( &$this, 'plugin_api_call' ), 10, 3);

    }

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Fired when the plugin is activated.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
	 */
	public static function activate( $network_wide ) {
		self::add_general();
		self::compile_css();
	}

	/**
	 * Check last version and add new options and styles if necessary.
	 *
	 * @since    2.0.0
	 */
	public function check_version() {
		if ( get_option('shortcodelic_info_update')!=$this->version ) {
			update_option('shortcodelic_info_update',$this->version);
			self::add_general();
			self::compile_css();
		}
	}

	/**
	 * Fired when the plugin is uninstall.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Deactivate" action, false if WPMU is disabled or plugin is deactivated on an individual blog.
	 */
	public static function uninstall( $network_wide ) {

        global $wpdb;
		$options = self::register_options();
		foreach ($options as $key => $value) {
			$shortcodelic_id = $value['id'];
			delete_option($shortcodelic_id);
			$wpdb->query("DELETE FROM $wpdb->options WHERE option_name LIKE '%shortcodelic_array%'");
			$wpdb->query("DELETE FROM $wpdb->options WHERE option_name LIKE '%shortcodelic_security%'");
			$wpdb->query("DELETE FROM $wpdb->options WHERE option_name LIKE '%shortcodelic_hidden%'");
		}

        $results = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_content LIKE '%shortcodelic%' AND post_name NOT LIKE '%autosave%' AND post_name NOT LIKE '%revision%'");
        foreach ( $results as $result ) 
        {
            $id = $result->ID;
            $content = $result->post_content;
            $content = preg_replace('/\[ *shortcodelic-slideshow([^\]])*\]/', '', $content);
            $content = preg_replace('/\<(.*?) class=[\'"]scicon-(.*?)><\/(.*?)>/', '', $content);
            $content = preg_replace('/\[ *shortcodelic-tabs([^\]])*\]/', '', $content);
            $content = preg_replace('/\[ *shortcodelic-tables([^\]])*\]/', '', $content);
            $content = preg_replace('/\[ *shortcodelic-maps([^\]])*\]/', '', $content);
            $content = preg_replace('/\[ *shortcodelic-carousel([^\]])*\]/', '', $content);
            $content = preg_replace('/\[ *shortcodelic-postcarousel([^\]])*\]/', '', $content);
            $content = preg_replace('/\<span(.*?)data-tooltip(.*?)>(.*?)<\/span>/', '$3', $content);
            $content = preg_replace('/\<(.*?)data-tooltip=[\'"](.*?)[\'"]([^\>])*\>(.*?)<\/(.*?)>/', '<$1$3>$4</$5>', $content);
            $content = preg_replace('/\<(.*?)data-ttopts=[\'"](.*?)[\'"]([^\>])*\>(.*?)<\/(.*?)>/', '<$1$3>$4</$5>', $content);
            $content = preg_replace('/\<span(.*?)buttonelic(.*?)>(.*?)<\/span>/', '$3', $content);
            $content = preg_replace('/\<(.*?)buttonelic(.*?)>(.*?)<\/(.*?)>/', '<$1$2>$3</$4>', $content);
            $content = preg_replace('/\<(.*?)data-style=[\'"](.*?)[\'"]([^\>])*\>(.*?)<\/(.*?)>/', '<$1$3>$4</$5>', $content);
            $content = preg_replace('/\<(.*?)data-fontsize=[\'"](.*?)[\'"]([^\>])*\>(.*?)<\/(.*?)>/', '<$1$3>$4</$5>', $content);
            $content = preg_replace('/\[ *shortcodelic-progress([^\]])*\](.*?)\[\/shortcodelic-progress\]/', '', $content);
            
            $wpdb->query( $wpdb->prepare( "UPDATE $wpdb->posts SET post_content = %s WHERE ID = $id", $content ) );
        }

        $post_types = get_post_types(array( 'public' => true ));  
        $args = array(
		'numberposts' => -1,
		'post_type' => $post_types );

		$allposts = get_posts( $args );
		foreach( $allposts as $postinfo ) {
			delete_post_meta( $postinfo->ID, 'shortcodelic_slideshows' );
			delete_post_meta( $postinfo->ID, 'shortcodelic_tabs' );
			delete_post_meta( $postinfo->ID, 'shortcodelic_tables' );
			delete_post_meta( $postinfo->ID, 'shortcodelic_maps' );
			delete_post_meta( $postinfo->ID, 'shortcodelic_carousels' );
			delete_post_meta( $postinfo->ID, 'shortcodelic_postcarousels' );
			delete_post_meta( $postinfo->ID, 'shortcodelic_woocarousels' );
		}
	}

	/**
	 * Load text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		$domain = $this->plugin_slug;
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, WP_LANG_DIR . '/' . $domain . '/' . $locale . '.mo' );
		load_plugin_textdomain( $domain, FALSE, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
	}

	/**
	 * Register and enqueue front-end style sheets.
	 *
	 * @since    1.0.0
	 */
	public function front_styles() {
		global $post,
			$sc_slideshine;

		wp_register_style( $this->plugin_slug .'-fontello', SHORTCODELIC_URL.'css/shortcodelic-fontello.css' );
		wp_register_style( $this->plugin_slug . '-tabs', SHORTCODELIC_URL.'css/tabs.css' );
		wp_register_style( $this->plugin_slug . '-tables', SHORTCODELIC_URL.'css/tables.css' );
		wp_register_style( $this->plugin_slug . '-maps', SHORTCODELIC_URL.'css/maps.css' );
		wp_register_style( $this->plugin_slug . '-slideshine', SHORTCODELIC_URL.'css/slideshine.css' );
		wp_register_style( $this->plugin_slug . '-carousel', SHORTCODELIC_URL.'css/carousel.css' );
		wp_register_style( $this->plugin_slug . '-tooltipster', SHORTCODELIC_URL.'css/tooltipster.css' );
		wp_register_style( $this->plugin_slug . '-progress', SHORTCODELIC_URL.'css/progress.css' );
		wp_register_style( $this->plugin_slug . '-box', SHORTCODELIC_URL.'css/box.css' );

		wp_enqueue_style( $this->plugin_slug . '-fontello' );

		if ( get_option('shortcodelic_inline_css')!='true' ) {
			wp_enqueue_style( $this->plugin_slug, SHORTCODELIC_URL.'css/shortcodelic_compiled.css', array(), $this->version );
		} else {
			$compile_style = self::inline_css();
			$compile_style = str_replace(array("\r", "\n", "\t"), '', $compile_style);
			wp_add_inline_style( $this->plugin_slug . '-fontello', $compile_style );
		}

	    if ( !$post )
	    	return;

		$content = $post->post_content;
		if ( preg_match( '#\[ *shortcodelic-slideshow([^\]])*\]#i', $content ) || preg_match( '#\[ *gallery([^\]])*\]#i', $content ) ) {
			wp_enqueue_style( $this->plugin_slug . '-fontello' );
			wp_enqueue_style( $this->plugin_slug . '-slideshine' );
		}

		if ( preg_match( '#\[ *shortcodelic-tabs([^\]])*\]#i', $content ) ) {
			wp_enqueue_style( $this->plugin_slug . '-box' );
			wp_enqueue_style( $this->plugin_slug . '-progress' );
			wp_enqueue_style( $this->plugin_slug . '-tooltipster' );
			wp_enqueue_style( $this->plugin_slug . '-tables' );
			wp_enqueue_style( $this->plugin_slug . '-maps' );
			wp_enqueue_style( $this->plugin_slug . '-tabs' );
		}

		if ( preg_match( '#\[ *shortcodelic-tables([^\]])*\]#i', $content ) ) {
			wp_enqueue_style( $this->plugin_slug . '-tables' );
		}

		if ( preg_match( '#\[ *shortcodelic-maps([^\]])*\]#i', $content ) ) {
			wp_enqueue_style( $this->plugin_slug . '-maps' );
		}

		if ( preg_match( '#\[ *shortcodelic-carousel([^\]])*\]#i', $content ) || preg_match( '#\[ *shortcodelic-postcarousel([^\]])*\]#i', $content ) ) {
			wp_enqueue_style( $this->plugin_slug . '-carousel' );
		}

		if ( preg_match( '#data-tooltip#i', $content ) ) {
			wp_enqueue_style( $this->plugin_slug . '-tooltipster' );
		}

		if ( preg_match( '#\[ *shortcodelic-progress([^\]])*\]#i', $content ) ) {
			wp_enqueue_style( $this->plugin_slug . '-progress' );
		}

		if ( preg_match( '#\[ *shortcodelic-box([^\]])*\]#i', $content ) ) {
			wp_enqueue_style( $this->plugin_slug . '-box' );
		}

	}

	/**
	 * Register and enqueue front-end scripts.
	 *
	 * @since    2.0.1
	 */
	public function front_scripts() {
		global $post,
			$sc_slideshine;
	
		wp_register_script( $this->plugin_slug . '-modernizr', SHORTCODELIC_URL.'scripts/modernizr.pix.js', array(), '2.6.2' );
		wp_register_script( 'jquery-easing', SHORTCODELIC_URL.'scripts/jquery.easing.min.js', array('jquery') );
		
		wp_register_script( $this->plugin_slug . '-kinetic', SHORTCODELIC_URL.'scripts/jquery.kinetic.min.js', array('jquery'), '1.8.2' );
		wp_register_script( $this->plugin_slug . '-transit', SHORTCODELIC_URL.'scripts/jquery.transit.js', array('jquery'), '0.9.9' );
		wp_register_script( $this->plugin_slug . '-event-move', SHORTCODELIC_URL.'scripts/jquery.event.move.js', array('jquery'), '1.3.1' );
		wp_register_script( $this->plugin_slug . '-google-maps-api', 'http://maps.google.com/maps/api/js?sensor=true', $this->version );
		wp_register_script( $this->plugin_slug . '-gmaps', SHORTCODELIC_URL.'scripts/gmaps.min.js', array('jquery', $this->plugin_slug . '-google-maps-api'), '0.4.5' );
		wp_register_script( $this->plugin_slug . '-maps', SHORTCODELIC_URL.'scripts/maps.js', array($this->plugin_slug.'-modernizr','jquery',$this->plugin_slug.'-gmaps'));
		wp_register_script( $this->plugin_slug . '-imagesloaded', SHORTCODELIC_URL.'scripts/imagesloaded.min.js', array('jquery'), '3.1.4');
		wp_register_script( $this->plugin_slug . '-bxslider', SHORTCODELIC_URL.'scripts/jquery.bxslider.min.js', array('jquery',$this->plugin_slug.'-imagesloaded'), '4.1.1' );
		wp_register_script( $this->plugin_slug . '-carousel', SHORTCODELIC_URL.'scripts/carousel.js', array($this->plugin_slug.'-modernizr','jquery',$this->plugin_slug.'-bxslider'));
		wp_register_script( $this->plugin_slug . '-tooltipster', SHORTCODELIC_URL.'scripts/jquery.tooltipster.min.js', array('jquery'), '2.1.4' );
		wp_register_script( $this->plugin_slug . '-tabs', SHORTCODELIC_URL.'scripts/tabs.js', array($this->plugin_slug.'-modernizr','jquery','jquery-easing',$this->plugin_slug . '-transit',$this->plugin_slug . '-plugins'));
		wp_register_script( $this->plugin_slug . '-chart', SHORTCODELIC_URL.'scripts/Chart.js', array('jquery'), '' );
		wp_register_script( $this->plugin_slug . '-cookie', SHORTCODELIC_URL.'scripts/jquery.cookie.js', array('jquery'), '1.4.0' );
		wp_register_script( $this->plugin_slug . '-tooltips', SHORTCODELIC_URL.'scripts/tooltips.js', array($this->plugin_slug.'-modernizr','jquery',$this->plugin_slug.'-tooltipster'));
		wp_register_script( $this->plugin_slug . '-slideshine', SHORTCODELIC_URL.'scripts/slideshine.js', array($this->plugin_slug.'-modernizr','jquery','jquery-ui-core','jquery-easing',$this->plugin_slug . '-transit',$this->plugin_slug . '-kinetic',$this->plugin_slug . '-event-move',$this->plugin_slug . '-plugins','jquery-ui-draggable'));
		wp_enqueue_script( $this->plugin_slug . '-plugins', SHORTCODELIC_URL.'scripts/plugins.js', array('jquery'), $this->version );

	    if ( !$post )
	    	return;
		$content = $post->post_content;

		if ( preg_match( '#\[ *shortcodelic-slideshow([^\]])*\]#i', $content ) || preg_match( '#\[ *gallery([^\]])*\]#i', $content ) ) {
			wp_enqueue_script( $this->plugin_slug . '-slideshine' );
		}

		if ( preg_match( '#\[ *shortcodelic-tabs([^\]])*\]#i', $content ) ) {
			wp_enqueue_script( $this->plugin_slug . '-tabs' );
		}

		if ( preg_match( '#\[ *shortcodelic-tables([^\]])*\]#i', $content ) ) {
			wp_enqueue_script( $this->plugin_slug . '-tables', SHORTCODELIC_URL.'scripts/tables.js', array($this->plugin_slug.'-modernizr','jquery'));
		}

		if ( preg_match( '#\[ *shortcodelic-maps([^\]])*\]#i', $content ) ) {
			wp_enqueue_script( $this->plugin_slug . '-maps' );
		}

		if ( preg_match( '#\[ *shortcodelic-carousel([^\]])*\]#i', $content ) || preg_match( '#\[ *shortcodelic-postcarousel([^\]])*\]#i', $content ) ) {
			wp_enqueue_script( $this->plugin_slug . '-carousel' );
		}

		if ( preg_match( '#data-tooltip#i', $content ) ) {
			wp_enqueue_script( $this->plugin_slug . '-tooltips' );
		}

		if ( preg_match( '#\[ *shortcodelic-progress([^\]])*\]#i', $content ) ) {
			wp_enqueue_script( $this->plugin_slug . '-progress', SHORTCODELIC_URL.'scripts/progress.js', array($this->plugin_slug.'-chart',$this->plugin_slug.'-modernizr','jquery','jquery-easing',$this->plugin_slug . '-transit',$this->plugin_slug . '-plugins'));
		}

		if ( preg_match( '#\[ *shortcodelic-box([^\]])*\]#i', $content ) ) {
			wp_enqueue_script( $this->plugin_slug . '-box', SHORTCODELIC_URL.'scripts/box.js', array($this->plugin_slug.'-modernizr','jquery',$this->plugin_slug.'-cookie'));
		}

	}

	/**
	 * Conditional scripts
	 *
	 * @since    1.0.0
	 */
	public function conditional_scripts() {
		echo '<!--[if lte IE 8]>
	<script type="text/javascript" src="'.SHORTCODELIC_URL.'scripts/excanvas.js"></script>
<![endif]-->
';
	}

	/**
	 * Drag slides element on preview iframe
	 *
	 * @since    2.0.1
	 */
	public function drag_slides() {
		echo '<script>
	jQuery(document).ready(function(){
		parent.jQuery("body").bind("sc_slideshine_preview_started", function(){
			jQuery(".sc_draggable").each(function(){
				var t = jQuery(this),
					ind = parseFloat(jQuery(this).attr("data-index"));
				t.draggable({
					iframeFix: true,
					stop: function(event, ui){
						parent.jQuery(".element-slide-preview").eq(ind).attr("data-position", JSON.stringify(ui.position));
					}
				});
			});
		});
	});
</script>';
	}

	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 * @since    1.0.0
	 */
	public function admin_styles() {
		global $pagenow;
		if ( $pagenow == 'post.php' || $pagenow == 'post-new.php' ) {
			wp_enqueue_style( 'wp-jquery-ui-dialog' );
			wp_enqueue_style( 'wp-pointer' );
			wp_enqueue_style( 'farbtastic' );
			wp_enqueue_style( $this->plugin_slug .'-fontello', SHORTCODELIC_URL.'css/shortcodelic-fontello.css', array(), $this->version );
			wp_enqueue_style( $this->plugin_slug .'-open-sans', SHORTCODELIC_URL.'css/open_sans.css', array(), $this->version );
			wp_enqueue_style( $this->plugin_slug .'-meta', SHORTCODELIC_URL.'css/meta.css', array(), $this->version );
		} elseif ( $pagenow == 'edit.php' ) {
			wp_enqueue_style( $this->plugin_slug .'-fontello', SHORTCODELIC_URL.'css/shortcodelic-fontello.css', array(), $this->version );
			wp_enqueue_style( $this->plugin_slug .'-columns', SHORTCODELIC_URL.'css/columns.css', array(), $this->version );
		} elseif ('admin.php' == $pagenow && isset($_GET['page']) && $_GET['page']=='shortcodelic_admin') {
			wp_enqueue_style( 'farbtastic' );
			wp_enqueue_style( 'wp-jquery-ui-dialog' );
			wp_enqueue_style( $this->plugin_slug .'-codemirror', SHORTCODELIC_URL.'css/codemirror.css', array(), $this->version );
			wp_enqueue_style( $this->plugin_slug .'-codemirror-skin', SHORTCODELIC_URL.'css/codemirror-skin.css', array(), $this->version );
			wp_enqueue_style( $this->plugin_slug .'-fontello', SHORTCODELIC_URL.'css/shortcodelic-fontello.css', array(), $this->version );
			wp_enqueue_style( $this->plugin_slug .'-open-sans', SHORTCODELIC_URL.'css/open_sans.css', array(), $this->version );
			wp_enqueue_style( $this->plugin_slug .'-admin', SHORTCODELIC_URL.'css/admin.css', array(), $this->version );
		}
	}

	/**
	 * Register and enqueue admin-specific scripts.
	 *
	 * @since    1.0.0
	 */
	public function admin_scripts() {
		global $pagenow;
		if ( $pagenow == 'post.php' || $pagenow == 'post-new.php' ) {
			wp_enqueue_script( 'wp-pointer' );
			wp_enqueue_script( 'farbtastic' );
			wp_enqueue_script( $this->plugin_slug . '-modernizr', SHORTCODELIC_URL.'scripts/modernizr.pix.js', array(), '2.6.2' );
			wp_enqueue_script( $this->plugin_slug . '-ui-touch-punch', SHORTCODELIC_URL.'scripts/jquery.ui.touch-punch.min.js', array('jquery-ui-mouse'), '0.2.2', false );
			wp_enqueue_script( $this->plugin_slug . '-meta', SHORTCODELIC_URL.'scripts/meta.js', array($this->plugin_slug.'-modernizr','farbtastic','jquery','jquery-ui-core','wp-pointer',$this->plugin_slug.'-ui-touch-punch','jquery-ui-slider','jquery-ui-sortable','jquery-ui-resizable','jquery-ui-dialog','jquery-ui-draggable') );
		} elseif ('admin.php' == $pagenow && isset($_GET['page']) && $_GET['page']=='shortcodelic_admin') {
			wp_enqueue_script( 'farbtastic' );
			wp_enqueue_script( $this->plugin_slug . '-modernizr', SHORTCODELIC_URL.'scripts/modernizr.pix.js', array(), '2.6.2' );
			wp_enqueue_script( $this->plugin_slug . '-ui-touch-punch', SHORTCODELIC_URL.'scripts/jquery.ui.touch-punch.min.js', array('jquery-ui-mouse'), '0.2.2', false );
			wp_enqueue_script( $this->plugin_slug . '-ui-slider', SHORTCODELIC_URL.'scripts/jquery.ui.slider.js', array('jquery-ui-core','jquery-ui-mouse','jquery-ui-widget') );
			wp_enqueue_script( $this->plugin_slug . '-codemirror', SHORTCODELIC_URL.'scripts/codemirror.js', array('jquery'), '3.14', false );
			wp_enqueue_script( $this->plugin_slug . '-css-mode', SHORTCODELIC_URL.'scripts/css.js', array($this->plugin_slug.'-codemirror'), '3.14', false );
			wp_enqueue_script( $this->plugin_slug . '-livequery', SHORTCODELIC_URL.'scripts/jquery.livequery.js', array('jquery'), '1.1.1', false );
			wp_enqueue_script( $this->plugin_slug . '-easing', SHORTCODELIC_URL.'scripts/jquery.easing.min.js', array('jquery'), '1.3', false );
			wp_enqueue_script( $this->plugin_slug . '-admin', SHORTCODELIC_URL.'scripts/admin.js', array($this->plugin_slug.'-modernizr','jquery','jquery-ui-core',$this->plugin_slug.'-ui-touch-punch','jquery-ui-sortable','jquery-ui-draggable','jquery-ui-droppable',$this->plugin_slug.'-ui-slider',$this->plugin_slug.'-css-mode',$this->plugin_slug . '-livequery', $this->plugin_slug.'-easing','jquery-ui-dialog'));
		}
	}

	/**
	 * Add the class "shortcodelic" to the front-end body
	 *
	 * @since    1.0.0
	 */
	public function body_class($classes) {
		$classes[] = 'shortcodelic';
		return $classes;
	}


	/**
	 * Add the metaboxes: the grid builder and its tabs to switch between builder and preview.
	 *
	 * @since    1.0.0
	 */
	public function add_meta() {
		global $post;
		$typenow = get_post_type();

		if (!function_exists('shortcodelic_shortcode_generator')) {
	        add_meta_box( 'shortcodelic_shortcode_generator', 'ShortCodelic Shortcode Generator', 'shortcodelic_shortcode_generator', $typenow, 'normal', 'low' );
			function shortcodelic_shortcode_generator( $post, $display ) {
			    require_once( SHORTCODELIC_PATH.'lib/shortcode_generator.php' );
			}
		}

	}

	/**
	 * Save the data sent thorugh metaboxes.
	 *
	 * @since    1.0.0
	 */
	public function content_save( $post_id ) {
	    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		foreach ($_POST as $key => $value) {
			if ( preg_match("/shortcodelic_(.+)_nonce/", $key) ) {
			    if( !isset( $_POST[$key] ) || !wp_verify_nonce( $_POST[$key], $key ) ) return;

			    if( !current_user_can( 'edit_post', $post_id ) ) return;

			    $meta_key = str_replace("_nonce", "", $key);
			    
			    if( isset( $_POST[$meta_key] ) )
			        update_post_meta( $post_id, $meta_key, $_POST[$meta_key] );
			}
		}
	        
	}

	/**
	 * Replace the html comments in the post content to create the grid
	 *
	 * @since    1.0.0
	 *
	 */
	public function filter_content($content) {

		if (!function_exists('shortcodelic_match_quotes')) {
			function shortcodelic_match_quotes($matches) {
	            return preg_replace('/&quot;/','"',$matches[0]);
	        	$icopts = json_decode($matches[0]);
	        }
		}

		if (!function_exists('shortcodelic_match_quotes_2')) {
			function shortcodelic_match_quotes_2($matches) {
	        	$icopts = json_decode($matches[1]);
	        	$icoin = '';
	        	$icoout = '';
	        	if ( isset($icopts->style) ) {
		        	$icoout .= 'data-style="'.$icopts->style.'" ';
	        	}
	        	if ( isset($icopts->size) ) {
		        	$icoin .= 'font-size:'.$icopts->size.'px;';
	        	}
	        	if ( isset($icopts->color) ) {
		        	$icoin .= 'color:'.$icopts->color.';';
	        	}
	        	if ( isset($icopts->bg) ) {
	        		if ( isset($icopts->style) && $icopts->style=='disc' ) {
			        	$icoin .= 'background-color:'.$icopts->bg.';';
			        } elseif ( isset($icopts->style) && $icopts->style=='circle' ) {
			        	$icoin .= 'border-color:'.$icopts->color.';';
			        }
	        	}
	        	if ( $icoin!='' ) {
	        		$icoout .= 'style="'.$icoin.'"';
	        	}
	            return $icoout;
	        }
		}

        /*$content = preg_replace('/\"\{\"(.*?)\"\}\"/', '\'{"$1"}\'', $content);
		$content = preg_replace('/<p>\[shortcodelic-(.*?)\]<\/p>/', '[shortcodelic-$1]', $content);
		$content = preg_replace('/<p>\[\/shortcodelic-(.*?)\]<\/p>/', '[shortcodelic-$1]', $content);
		$content = preg_replace('/<span (.*?)><\/p>/', '<span $1>', $content);
		$content = preg_replace('/<div (.*?)><\/p>/', '<div $1>', $content);
		$content = preg_replace('/<p><\/span>/', '</span>', $content);
		$content = preg_replace('/<p><\/div>/', '</div>', $content);
		$content = preg_replace('/<p>\[shortcodelic-(.*?)\]<\/p>/', '[shortcodelic-$1]', $content);
		$content = preg_replace('/\[shortcodelic-(.*?)\]<\/p>/', '</p>[shortcodelic-$1]', $content);
		$content = preg_replace('/<p>\[\/shortcodelic-(.*?)\]<\/p>/', '[/shortcodelic-$1]', $content);
		$content = preg_replace('/<p>\[shortcodelic-(.*?)\]/', '[shortcodelic-$1]', $content);
		$content = preg_replace('/\[\/shortcodelic-(.*?)\]<\/p>/', '[/shortcodelic-$1]', $content);*/
		$content = preg_replace('/<p>\[shortcodelic-(.*?)\]<\/p>/', '[shortcodelic-$1]', $content);
		$content = preg_replace('/\<(.*?) class=([\'|"])scicon-(.*?)>(.*?)<\/(.*?)>/', '<$1 class=$2scicon-$3></$5>', $content);
		$content = preg_replace_callback(
	        '/data-icopts=([\'|"]){(.*?)}([\'|"])/',
	        'shortcodelic_match_quotes',
	        $content
	    );
		$content = preg_replace('/data-icopts=([\'|"]){(.*?)}([\'|"])/', 'data-icopts=\'{$2}\'', $content);
		$content = preg_replace_callback(
	        '/data-icopts=\'(.*?)\'/',
	        'shortcodelic_match_quotes_2',
	        $content
	    );

		return $content;
	}

	/**
	 * Add plugin menu.
	 *
	 * @since    1.0.0
	 */
	public function add_menu() {
		if (function_exists('add_options_page')) {
			add_menu_page($this->plugin_name, $this->plugin_name, 'activate_plugins', 'shortcodelic_admin', array( $this, 'register_options' ));
			add_submenu_page('shortcodelic_admin', 'Very general', 'Very general', 'activate_plugins', 'shortcodelic_verygeneral', array( $this, 'register_options' ));
			add_submenu_page('shortcodelic_admin', 'Register', 'Register', 'activate_plugins', 'shortcodelic_register', array( $this, 'register_options' ));
			add_submenu_page('shortcodelic_admin', 'Slideshows', 'Slideshows', 'activate_plugins', 'shortcodelic_slideshows', array( $this, 'register_options' ));
			add_submenu_page('shortcodelic_admin', 'Tabs', 'Tabs', 'activate_plugins', 'shortcodelic_tabs', array( $this, 'register_options' ));
			add_submenu_page('shortcodelic_admin', 'Tables', 'Tables', 'activate_plugins', 'shortcodelic_tables', array( $this, 'register_options' ));
			add_submenu_page('shortcodelic_admin', 'Carousels', 'Carousels', 'activate_plugins', 'shortcodelic_carousels', array( $this, 'register_options' ));
			add_submenu_page('shortcodelic_admin', 'Tooltips', 'Tooltips', 'activate_plugins', 'shortcodelic_tooltips', array( $this, 'register_options' ));
			add_submenu_page('shortcodelic_admin', 'Progress bars', 'Progress bars', 'activate_plugins', 'shortcodelic_progress', array( $this, 'register_options' ));
			add_submenu_page('shortcodelic_admin', 'Buttons', 'Buttons', 'activate_plugins', 'shortcodelic_buttons', array( $this, 'register_options' ));
			add_submenu_page('shortcodelic_admin', 'Text boxes', 'Text boxes', 'activate_plugins', 'shortcodelic_boxes', array( $this, 'register_options' ));
			add_submenu_page('shortcodelic_admin', 'Compile CSS file', 'Compile CSS file', 'activate_plugins', 'shortcodelic_css', array( $this, 'register_options' ));
		}
	}

	/**
	 * Options.
	 *
	 * @since    1.0.0
	 */
	public static function register_options() {
	    global $options;

	    $shortCodelic = new ShortCodelic();
	    $version = $shortCodelic->version;

	    $page_template = locate_template( array( 'page.php' ) );
        $shortcodelic_post_type = (!empty($page_template)) ? array("page" => "page") : '';

        if (!empty($page_template)) {
	        $shortcodelic_page_template['default'] = 'default';
	    }
        $templates = get_page_templates();
        $shortcodelic_page_template = array();
        foreach ( $templates as $template_name => $template_filename ) {
        	$shortcodelic_page_template[$template_filename] = $template_filename;
        }

		$options = array (
			array( "id" => "shortcodelic_info_update",
				"std" => $version),
			array( "id" => "shortcodelic_allow_ajax",
				"std" => "true"),
			array( "id" => "shortcodelic_inline_css",
				"std" => "0"),
			array( "id" => "shortcodelic_no_trace",
				"std" => "0"),
			array( "id" => "shortcodelic_user_name",
				"std" => ""),
			array( "id" => "shortcodelic_license_key",
				"std" => ""),
			array( "id" => "shortcodelic_slideshow_main_color",
				"std" => "#ffffff"),
			array( "id" => "shortcodelic_slideshow_bg_color",
				"std" => "#000000"),
			array( "id" => "shortcodelic_slideshow_opacity",
				"std" => "0.2"),
			array( "id" => "shortcodelic_slide_bg_color",
				"std" => "#ffffff"),
			array( "id" => "shortcodelic_slide_opacity",
				"std" => "0"),
			array( "id" => "shortcodelic_tab_border_color",
				"std" => "#eeeeee"),
			array( "id" => "shortcodelic_tab_bg_color",
				"std" => "#fafafa"),
			array( "id" => "shortcodelic_tab_color",
				"std" => "#21759b"),
			array( "id" => "shortcodelic_tab_border_width",
				"std" => "1"),
			array( "id" => "shortcodelic_panel_bg_color",
				"std" => "#ffffff"),
			array( "id" => "shortcodelic_panel_text_color",
				"std" => "#444444"),
			array( "id" => "shortcodelic_heading_border_color",
				"std" => "#ed7c57"),
			array( "id" => "shortcodelic_heading_background_color",
				"std" => "#d94839"),
			array( "id" => "shortcodelic_heading_border_width",
				"std" => "1"),
			array( "id" => "shortcodelic_heading_text_color",
				"std" => "#ffffff"),
			array( "id" => "shortcodelic_pricing_border_color",
				"std" => "#d94839"),
			array( "id" => "shortcodelic_pricing_background_color",
				"std" => "#b32516"),
			array( "id" => "shortcodelic_pricing_border_width",
				"std" => "1"),
			array( "id" => "shortcodelic_pricing_text_color",
				"std" => "#ffffff"),
			array( "id" => "shortcodelic_pricetable_border_color",
				"std" => "#dddddd"),
			array( "id" => "shortcodelic_pricetable_background_color",
				"std" => "#ffffff"),
			array( "id" => "shortcodelic_pricetable_text_color",
				"std" => "#252525"),
			array( "id" => "shortcodelic_pricetable_border_color_alt",
				"std" => "#dddddd"),
			array( "id" => "shortcodelic_pricetable_border_width",
				"std" => "1"),
			array( "id" => "shortcodelic_pricetable_background_color_alt",
				"std" => "#fafafa"),
			array( "id" => "shortcodelic_pricetable_text_color_alt",
				"std" => "#252525"),
			array( "id" => "shortcodelic_pricetable_gutter",
				"std" => "30"),
			array( "id" => "shortcodelic_table_border_color",
				"std" => "#dddddd"),
			array( "id" => "shortcodelic_table_background_color",
				"std" => "#ffffff"),
			array( "id" => "shortcodelic_table_text_color",
				"std" => "#252525"),
			array( "id" => "shortcodelic_table_border_color_alt",
				"std" => "#dddddd"),
			array( "id" => "shortcodelic_table_border_width",
				"std" => "1"),
			array( "id" => "shortcodelic_table_background_color_alt",
				"std" => "#fafafa"),
			array( "id" => "shortcodelic_table_text_color_alt",
				"std" => "#252525"),
			array( "id" => "shortcodelic_carousel_commands_fore",
				"std" => "#ffffff"),
			array( "id" => "shortcodelic_carousel_commands_bg",
				"std" => "#000000"),
			array( "id" => "shortcodelic_carousel_commands_opacity",
				"std" => "0.2"),
			array( "id" => "shortcodelic_carousel_nav",
				"std" => "#eeeeee"),
			array( "id" => "shortcodelic_tooltip_color",
				"std" => "#eeeeee"),
			array( "id" => "shortcodelic_tooltip_link",
				"std" => "#56d8f5"),
			array( "id" => "shortcodelic_tooltip_bg",
				"std" => "#333333"),
			array( "id" => "shortcodelic_tooltip_radius",
				"std" => "2"),
			array( "id" => "shortcodelic_tooltip_border",
				"std" => "2"),
			array( "id" => "shortcodelic_tooltip_border_color",
				"std" => "#222222"),
			array( "id" => "shortcodelic_tooltip_shadow_size",
				"std" => "5"),
			array( "id" => "shortcodelic_tooltip_shadow_color",
				"std" => "#000000"),
			array( "id" => "shortcodelic_tooltip_shadow_opacity",
				"std" => "0.3"),
			array( "id" => "shortcodelic_progress_tooltip_bg",
				"std" => "#222222"),
			array( "id" => "shortcodelic_progress_tooltip_color",
				"std" => "#eeeeee"),
			array( "id" => "shortcodelic_css_code",
				"std" => '\/* Shortcodelic custom stylesheet *\/'),
		);
		
		self::shortcodelic_admin( array( &$this, 'register_options' ) );
		self::shortcodelic_verygeneral( array( &$this, 'register_options' ) );
		self::shortcodelic_register( array( &$this, 'register_options' ) );
		self::shortcodelic_slideshows( array( &$this, 'register_options' ) );
		self::shortcodelic_tabs( array( &$this, 'register_options' ) );
		self::shortcodelic_tables( array( &$this, 'register_options' ) );
		self::shortcodelic_carousels( array( &$this, 'register_options' ) );
		self::shortcodelic_tooltips( array( &$this, 'register_options' ) );
		self::shortcodelic_progress( array( &$this, 'register_options' ) );
		self::shortcodelic_buttons( array( &$this, 'register_options' ) );
		self::shortcodelic_boxes( array( &$this, 'register_options' ) );
		self::shortcodelic_css( array( &$this, 'register_options' ) );

		return $options;
	}

	/**
	 * Display the menu for admin panel.
	 *
	 * @since    1.0.0
	 */
	public static function shortcodelic_admin() {
		require_once( SHORTCODELIC_PATH . 'lib/admin/panel.php' );
	}

	/**
	 * Display the general admin page.
	 *
	 * @since    1.0.0
	 */
	public static function shortcodelic_verygeneral() {
		require_once( SHORTCODELIC_PATH . 'lib/admin/general.php' );
	}

	/**
	 * Display the slideshow default options.
	 *
	 * @since    1.0.0
	 */
	public static function shortcodelic_slideshows() {
		require_once( SHORTCODELIC_PATH . 'lib/admin/slideshows.php' );
	}

	/**
	 * Display the tab default options.
	 *
	 * @since    1.0.0
	 */
	public static function shortcodelic_tabs() {
		require_once( SHORTCODELIC_PATH . 'lib/admin/tabs.php' );
	}

	/**
	 * Display the table default options.
	 *
	 * @since    1.0.0
	 */
	public static function shortcodelic_tables() {
		require_once( SHORTCODELIC_PATH . 'lib/admin/tables.php' );
	}

	/**
	 * Display the carousel default options.
	 *
	 * @since    1.0.0
	 */
	public static function shortcodelic_carousels() {
		require_once( SHORTCODELIC_PATH . 'lib/admin/carousels.php' );
	}

	/**
	 * Display the tooltip default options.
	 *
	 * @since    1.0.0
	 */
	public static function shortcodelic_tooltips() {
		require_once( SHORTCODELIC_PATH . 'lib/admin/tooltips.php' );
	}

	/**
	 * Display the progress bar default options.
	 *
	 * @since    1.0.0
	 */
	public static function shortcodelic_progress() {
		require_once( SHORTCODELIC_PATH . 'lib/admin/progress.php' );
	}

	/**
	 * Display the buttons default options.
	 *
	 * @since    1.0.0
	 */
	public static function shortcodelic_buttons() {
		require_once( SHORTCODELIC_PATH . 'lib/admin/buttons.php' );
	}

	/**
	 * Display the text boxes default options.
	 *
	 * @since    1.0.0
	 */
	public static function shortcodelic_boxes() {
		require_once( SHORTCODELIC_PATH . 'lib/admin/boxes.php' );
	}

	/**
	 * Display the register form.
	 *
	 * @since    1.0.0
	 */
	public static function shortcodelic_register() {
		require_once( SHORTCODELIC_PATH . 'lib/admin/register.php' );
	}

	/**
	 * Display the CSS compiler.
	 *
	 * @since    1.0.0
	 */
	public static function shortcodelic_css() {
		require_once( SHORTCODELIC_PATH . 'lib/admin/css.php' );
	}

	/**
	 * Register options in the database.
	 *
	 * @since    1.0.0
	 */
	public static function add_general() {
		global $options;
		self::register_options();
		
		foreach ($options as $value) :
			if(!get_option($value['id'])){
				add_option($value['id'], $value['std']);
			}
		endforeach;
	}

	/**
	 * Save the options on the admin panel via AJAX.
	 *
	 * @since    1.0.0
	 */
	public function save_via_ajax() {
		global $options;
		check_ajax_referer('shortcodelic_data', 'shortcodelic_security');

		$data = $_POST;
		unset($data['shortcodelic_security'], $data['action']);

		foreach ($_REQUEST as $key => $value) {
			if ( preg_match("/shortcodelic_array/", $key) ) {
				delete_option($key);
				if(!get_option($key)) {
					add_option($key, $value);
				} else {
					update_option($key, $value);
				}
			}
		}
		
		foreach ($_REQUEST as $key => $value) {
			if( isset($_REQUEST[$key]) ) {
				update_option($key, $value);
			}
		}		
	}

	/**
	 * Remove the subpages via CSS.
	 *
	 * @since    1.0.0
	 */
	public function remove_subpages() { ?>
	    <style type="text/css" media="screen">
	        #toplevel_page_shortcodelic_admin ul, #toplevel_page_shortcodelic_admin .wp-menu-toggle, #toplevel_page_shortcodelic_admin .wp-submenu, #toplevel_page_shortcodelic_admin.wp-not-current-submenu .wp-menu-arrow {
	            display: none!important;
	        }
	    </style>
	<?php }

	/**
	 * Set the content width as JS var.
	 *
	 * @since    1.0.0
	 */
	public static function js_vars() {
		global $content_width, $post, $pagenow;

		if ( ! isset( $content_width ) ) $content_width = 1280;

		if ( $pagenow == 'post.php' || $pagenow == 'post-new.php' ) {
		?>

		<script type="text/javascript">
		//<![CDATA[
			var shortcodelic_content_width = "<?php echo $content_width; ?>", shortcodelic_url = "<?php echo SHORTCODELIC_URL; ?>", pix_post_id = "<?php echo $post->ID; ?>", pix_referer = "<?php echo wp_get_referer(); ?>", shortcodelic_modal = "close", shortcodelic_video_icon = "<?php echo SHORTCODELIC_URL; ?>images/video-icon.svg";
		//]]>
		</script>

		<?php
		}
	}

	/**
	 * The css compiler.
	 *
	 * @since    2.0.0
	 */
	public function compile_css() {

		WP_Filesystem();
		global $wp_filesystem;

		$css_file = SHORTCODELIC_PATH . 'css/shortcodelic_compiled.css';
		$target_file = SHORTCODELIC_PATH.'lib/push.php';

		ob_start();
		require($target_file);
		$css = ob_get_clean();

		$wp_filesystem->put_contents( $css_file, $css, FS_CHMOD_FILE );

	}


	/**
	 * The css compiler via AJAX.
	 *
	 * @since    2.0.0
	 */
	public function compile_css_ajax() {

		WP_Filesystem();
		global $wp_filesystem;

		$css_file = SHORTCODELIC_PATH . 'css/shortcodelic_compiled.css';
		$target_file = SHORTCODELIC_PATH.'lib/push.php';

		ob_start();
		require($target_file);
		$css = ob_get_clean();

		$wp_filesystem->put_contents( $css_file, $css, FS_CHMOD_FILE );

	    die();
	}

	/**
	 * Add custom stylesheet to tinyMCE.
	 *
	 * @since    1.0.0
	 */
	public static function add_tinymce_css($wp) {
        $wp .= ',' . SHORTCODELIC_URL . 'css/tinymce.css,' . SHORTCODELIC_URL . 'css/shortcodelic-fontello.css';
        return $wp;
    }

	/**
	 * Sanitize titles via AJAX.
	 *
	 * @since    1.0.0
	 */
	public static function sanitize_ajax() {
		$title = sanitize_title($_POST['title']);
		echo $title;
		die();
    }

	/**
	 * Download custom CSS.
	 *
	 * @since    1.0.0
	 */
	public static function download_css() {
		$target_file = SHORTCODELIC_PATH.'lib/push.php';
		ob_start();
		require($target_file);
		$css = ob_get_clean();
		echo $css;
		die();
    }

	/**
	 * Download custom CSS.
	 *
	 * @since    1.0.0
	 */
	public static function inline_css() {
		$target_file = SHORTCODELIC_PATH.'lib/push.php';
		ob_start();
		require($target_file);
		$css = ob_get_clean();
		return $css;
    }

/*=========================================================================================*/

	/**
	 * Set the button on tinyMCE editor.
	 *
	 * @since    1.0.0
	 */
	public static function add_shortcodelic_buttons() {

		global $post, $pagenow;
			
		if ( $pagenow == 'post.php' || $pagenow == 'post-new.php' ) {
			if ( !current_user_can('edit_posts', $post->ID) && !current_user_can('edit_pages', $post->ID) )
				return;
			if ( get_user_option('rich_editing') == 'true' ) {
				add_filter('mce_external_plugins', 'add_shortcodelic_js');
				add_filter('mce_buttons', 'register_shortcodelic_buttons_page');
			}

			function add_shortcodelic_js($plugin_array) {
				$plugin_array['shortcodelic'] = SHORTCODELIC_URL.'/scripts/sc_buttons.js';
				return $plugin_array;
			}

			function register_shortcodelic_buttons_page($buttons) {
				array_push(
					$buttons,
					"shortcodelic_sc",
					"shortcodelic_fonticon",
					"shortcodelic_tooltip",
					"shortcodelic_removetooltip",
					"shortcodelic_progress",
					"shortcodelic_buttons",
					"shortcodelic_removebutton",
					"shortcodelic_box",
					"shortcodelic_removespan"
				);
				return $buttons;
			}
		}
	}

	/**
	 * Set the button on tinyMCE editor.
	 *
	 * @since    1.0.0
	 */

	public static function ajax_update_meta() {

		check_ajax_referer('shortcodelic_'.$_POST['type'].'_nonce', 'security');

		//print_r($_POST); /*to test with an alert on ajax success*/

		wp_parse_str( $_POST['content'], $data );
		$post_id = $_POST['post_id'];
		$values = get_post_custom( $post_id );
		$meta_key = $_POST['meta_key'];
		if ( isset($values[$meta_key]) ) {
			$values = $values[$meta_key][0];
			$values = maybe_unserialize($values);
			if ( isset($_POST['slideshow']) ) {
				$slideshow = $_POST['slideshow'];
				$values[$slideshow] = '';
				//$data = array_merge($values, $data);
			} elseif ( isset($_POST['tabs']) ) {
				$tabs = $_POST['tabs'];
				$values[$tabs] = '';
			} elseif ( isset($_POST['tables']) ) {
				$tables = $_POST['tables'];
				$values[$tables] = '';
			} elseif ( isset($_POST['maps']) ) {
				$maps = $_POST['maps'];
				$values[$maps] = '';
			} elseif ( isset($_POST['carousels']) ) {
				$carousels = $_POST['carousels'];
				$values[$carousels] = '';
			} elseif ( isset($_POST['postcarousels']) ) {
				$postcarousels = $_POST['postcarousels'];
				$values[$postcarousels] = '';
			} elseif ( isset($_POST['woocarousels']) ) {
				$woocarousels = $_POST['woocarousels'];
				$values[$woocarousels] = '';
			} 
			$data = array_merge($values, $data);
		}
        update_post_meta( $post_id, $meta_key, $data );

		die();
	}	

	/**
	 * Return a thumbnail
	 *
	 * @since    1.0.0
	 */

	public static function ajax_get_thumb() {

		check_ajax_referer('shortcodelic_slideshows_nonce', 'security');

		//echo $_POST['content'];

        $attachment_id = $_POST['content'];
        $attachment_bg = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
        echo $attachment_bg[0];

		die();
	}	

	public static function add_pp_col($cols) { 

		global $post;
		//$post_type = $post->post_type;

		$cols['shortcodelic'] = __('SCE','shortcodelic'); 

		/*if ( $typenow != 'testimonial' ) {
			$cols['template'] = __('Template'); 
		}
		if ( $typenow == 'portfolio' ) {
			$cols['galleries'] = __('Galleries'); 
			$cols['portfolio_tag'] = __('Tags'); 
		}*/
		return $cols;
	}
 
	public static function add_pp_value($column_name) {

		global $post;
				
		switch ( $column_name ) {
			case 'shortcodelic':
				$values = get_post_custom( $post->ID );
				$out = '<i class="scicon-awesome-cancel-circled" style="color:#bbbbbb; font-size:14px"></i>';
				foreach ($values as $key => $value) {
					if ( preg_match("/shortcodelic_(.+)/", $key, $match) ) {
						$shortcodelic = isset( $values[$match[0]] ) ? $values[$match[0]][0] : '';
						$shortcodelic = maybe_unserialize($shortcodelic);
	                    if (is_array($shortcodelic)) {
	                        $pluginSize = sizeof($shortcodelic)+1;
	                        for ($key = 1; $key <= sizeof($shortcodelic); $key++) {
	                            if ( !empty($shortcodelic[$match[0].'_'.$key]) ) {
            						$out = '<i class="scicon-awesome-ok-circled" style="color:#1abc9c; font-size:14px"></i>';
	                            }
	                        }
	                    }
					}
				}
				echo $out;
				break;
		}
	}
  
	/**
	 * HEX to RGB (http://bavotasan.com/2011/convert-hex-color-to-rgb-using-php/).
	 *
	 * @since    1.0.0
	 */

	public static function hex2rgb($hex) {
	   $hex = str_replace("#", "", $hex);

	   if(strlen($hex) == 3) {
	      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
	      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
	      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	   } else {
	      $r = hexdec(substr($hex,0,2));
	      $g = hexdec(substr($hex,2,2));
	      $b = hexdec(substr($hex,4,2));
	   }
	   $rgb = array($r, $g, $b);
	   //return implode(",", $rgb); // returns the rgb values separated by commas
	   return $rgb; // returns an array with the rgb values
	}

	/**
	 * Slideshow shortcode.
	 *
	 * @since    2.0.0
	 */

	public function shortcodelicSlideshowSC($atts) {

		global $post;

		extract(shortcode_atts(array(
	        'postid' => '',
	        'slideshow' => ''
	    ), $atts));

	    $postid = $postid == '' ? $post->ID : $postid;

		wp_enqueue_style( $this->plugin_slug . '-fontello' );
		wp_enqueue_style( $this->plugin_slug . '-slideshine' );
		wp_enqueue_script( $this->plugin_slug . '-slideshine' );

		if (!function_exists('shortcodelic_match_quotes')) {
			function shortcodelic_match_quotes($matches) {
	            return preg_replace('/&quot;/','"',$matches[0]);
	        	$icopts = json_decode($matches[0]);
	        }
		}

		if (!function_exists('shortcodelic_match_quotes_3')) {
			function shortcodelic_match_quotes_3($matches) {
	            return preg_replace('/"/','&quot;',$matches[0]);
	        	$icopts = json_decode($matches[0]);
	        }
		}

        global $wp_embed;
	    $out = '';

		$values = get_post_custom( $postid );
		$shortcodelic_slideshows = isset( $values['shortcodelic_slideshows'] ) ? $values['shortcodelic_slideshows'][0] : '';
		$shortcodelic_slideshows = maybe_unserialize($shortcodelic_slideshows);
		$slideshowSize = $slideshow;

        if (is_array($shortcodelic_slideshows)) {

        	$skin = esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['skin']);
        	$skinrgb = $this->hex2rgb($skin);
        	$skinbg = esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['skinbg']);
        	$skinbgrgb = $this->hex2rgb($skinbg);
        	$skinopacity = esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['skinopacity']);

        	$out .= "<!--[if lte IE 8]><div class='ie8-'><![endif]-->";
        	$out .= "<!--[if IE 9]><div class='ie9'><![endif]-->";

        	$slide_h = esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['height']);
        	$slide_w = esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['width']);

        	$out .= "<div class='pix_slideshine pix_slideshine-post-id-$postid pix_slideshine-$slideshow' data-opts='{";
	        	$out .= "\"layout\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['layout'])."\",";
	        	$out .= "\"height\":\"$slide_h\",";
	        	$out .= "\"width\":\"$slide_w\",";
	        	$out .= "\"fx\":\"".implode(",", $shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['fx'])."\",";
	        	$out .= "\"mobfx\":\"".implode(",", $shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['mobfx'])."\",";
	        	$out .= "\"longfx\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['longfx'])."\",";
	        	$out .= "\"timeout\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['timeout'])."\",";
	        	$out .= "\"speed\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['speed'])."\",";
	        	$out .= "\"easing\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['easing'])."\",";
	        	$out .= "\"fxslide\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['fxslide'])."\",";
	        	$out .= "\"autoplay\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['autoplay'])."\",";
	        	$out .= "\"hover\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['hover'])."\",";
	        	$out .= "\"thumbnails\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['thumbnails'])."\",";
	        	$out .= "\"bullets\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['bullets'])."\",";
	        	$out .= "\"rows\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['rows'])."\",";
	        	$out .= "\"cols\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['cols'])."\",";
	        	$out .= "\"gridrows\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['gridrows'])."\",";
	        	$out .= "\"gridcols\":\"".esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['gridcols'])."\",";
	        	$out .= "\"skin\":\"$skin\",";
	        	$out .= "\"skinrgb\":\"$skinrgb[0],$skinrgb[1],$skinrgb[2]\",";
	        	$out .= "\"skinbg\":\"$skinbg\",";
	        	$out .= "\"skinbgrgb\":\"$skinbgrgb[0],$skinbgrgb[1],$skinbgrgb[2]\",";
	        	$out .= "\"skinopacity\":\"$skinopacity\"";
        	$out .= "}'>";

	        if (isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide']) && is_array($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'])) { 
	        	$i = 1; foreach ($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'] as $key => &$slide) {

                    $attachment_bg = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['bg']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['bg']) : '';
                    $attachment_id = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['id']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['id']): '';
                    $attachment_size = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['size']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['size']): '';
                    $attachment_bg_pos = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['bg_pos']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['bg_pos']): 'center';
                    $attachment_timeout = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['timeout']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['timeout']): '';
                    $attachment_speed = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['speed']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['speed']): '';
                    $attachment_effect = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['effect']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['effect']): '';
                    $attachment_mobfx = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['mobfx']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['mobfx']): '';
                    $attachment_longfx = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['longfx']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['longfx']): '';
                    $attachment_easing = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['easing']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['easing']): '';
                    $attachment_fxslide = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['fxslide']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['fxslide']): '';
                    $attachment_link = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['link']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['link']): '';
                    $attachment_target = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['target']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['target']): '';
                    $attachment_video = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['video']) ? $shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['video']: '';
                    $attachment_stop = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['stop']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['stop']): '';
                    $attachment_elements = isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['elements']) ? esc_attr($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['slide'][$key]['elements']): '';
                    $attachment_elements = preg_replace('/\"\{\"(.*?)\"\}\"/', '\'{"$1"}\'', $attachment_elements);

                    $attachment_url = wp_get_attachment_url( $attachment_id );
                    $cover = '';
                    if ( $attachment_id != '' ) {
                        $attachment_bg = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
                        $attachment_bg = 'url('.$attachment_bg[0].')';
                        $attachment_url = wp_get_attachment_image_src( $attachment_id, $attachment_size );
                        $attachment_url = $attachment_url[0];
                    } else {
                        $attachment_url = $attachment_bg;
                        $attachment_bg = 'url('.$attachment_bg.')';
                    }

                    $attachment_bg_video = $attachment_video != '' ? 'url('.SHORTCODELIC_URL.'images/video-icon.svg)' : '';

	                $out .= "<div>
	                    <div data-opts='{";
				        	$out .= "\"src\":\"$attachment_url\",";
				        	$out .= "\"thumb\":\"$attachment_bg\",";
				        	$out .= "\"thumb2\":\"$attachment_bg_video\",";
				        	$out .= "\"bg_pos\":\"$attachment_bg_pos\",";
				        	$out .= "\"timeout\":\"$attachment_timeout\",";
				        	$out .= "\"speed\":\"$attachment_speed\",";
				        	$out .= "\"fx\":\"$attachment_effect\",";
				        	$out .= "\"mobfx\":\"$attachment_mobfx\",";
				        	$out .= "\"longfx\":\"$attachment_longfx\",";
				        	$out .= "\"fxslide\":\"$attachment_fxslide\",";
				        	$out .= "\"stop\":\"$attachment_stop\",";
				        	$out .= "\"link\":\"$attachment_link\",";
				        	$out .= "\"target\":\"$attachment_target\"";
	                $out .= "}'>";
			        	if ( $attachment_video != '' ) {
				        	$out .= do_shortcode($wp_embed->autoembed(wp_specialchars_decode($attachment_video, ENT_QUOTES)));
				        }

	                	$out .= "</div>";

				        if ( $attachment_elements != '' ) {
    	                    $attachment_elements = html_entity_decode($attachment_elements);
    	                    $attachment_elements = preg_replace("/\"\{/", "{", $attachment_elements);
    	                    $attachment_elements = preg_replace("/\}\"/", "}", $attachment_elements);
    	                    $attachment_elements = preg_replace("/\;quot\;/", "&quot;", $attachment_elements);
    	                    $attachment_elements = preg_replace("/\&quot\;(top|left)\&quot\;/", "\"$1\"", $attachment_elements);
							$attachment_elements = preg_replace_callback(
						        '/data-(.+?)=&quot;\{(.+?)\}&quot;/',
						        'shortcodelic_match_quotes_3',
						        $attachment_elements
						    );
							$attachment_elements = preg_replace_callback(
						        '/data-(.+?)=\{(.+?)\}/',
						        'shortcodelic_match_quotes_3',
						        $attachment_elements
						    );
							$attachment_elements = preg_replace_callback(
						        '/data-(.+?)=[\'"]\{(.+?)\}[\'"]/',
						        'shortcodelic_match_quotes_3',
						        $attachment_elements
						    );
    	                    $attachment_elements = preg_replace("/data-(.+?)=&quot;\{(.+?)\}&quot;/", "data-$1='$2'", $attachment_elements);
    	                    $attachment_elements = preg_replace("/data-(.+?)=(\'\")\{(.+?)\}(\'\")/", "data-$1='$2'", $attachment_elements);
    	                    $attachment_elements = preg_replace("/data-(.+?)=\{(.+?)\}/", "data-$1='$2'", $attachment_elements);
    	                    $attachment_elements = json_decode($attachment_elements);
    	                    $element = array();

    	                    if (!empty($attachment_elements)) {
								foreach ($attachment_elements as $key => $value) {
									$i = preg_replace("/slide_elements\[elements\]\[(.*?)\]\[(.*?)\]/", "$1", $attachment_elements[$key]->name);
									$k = preg_replace("/slide_elements\[elements\]\[(.*?)\]\[(.*?)\]/", "$2", $attachment_elements[$key]->name);
									$v = $attachment_elements[$key]->value;
									$element[$i][$k] = $v;
								}

								foreach ($element as $key => $value) {
									if ( isset($key) ) {
										$elements = $element[$key];
							        	if ( !empty($elements['caption']) ) {
								        	$out .= "<div class='slideshine_caption slideshine_caption_".($key-1)."' style='
									        		max-width:".$elements['width']."%;
									        		left:".(($elements['position']->left / $slide_w) * 100)."%;
									        		top:".(($elements['position']->top / $slide_h) * 100)."%;'
								        		data-caption='{";
									        	$out .= "\"wider\":\"".$elements['wider']."\",";
									        	$out .= "\"narrower\":\"".$elements['narrower']."\",";
									        	$out .= "\"bg\":\"".$elements['bg']."\",";
									        	$data_rgb = $this->hex2rgb($elements['bg']);
									        	$out .= "\"rgb\":\"rgba($data_rgb[0],$data_rgb[1],$data_rgb[2],".$elements['opacity'].")\",";
									        	$out .= "\"entering\":\"".$elements['entering']."\",";
									        	$out .= "\"from\":\"".$elements['from']."\",";
									        	$out .= "\"fromduration\":\"".$elements['fromduration']."\",";
									        	$out .= "\"fromeasing\":\"".$elements['fromeasing']."\",";
									        	$out .= "\"leaving\":\"".$elements['leaving']."\",";
									        	$out .= "\"to\":\"".$elements['to']."\",";
									        	$out .= "\"toduration\":\"".$elements['toduration']."\",";
									        	$out .= "\"toeasing\":\"".$elements['toeasing']."\"";
							                $out .= "}'>";
							                $the_content = $elements['caption'];
				    	                    $the_content = preg_replace("/data-(.+?)=\'\"(.+?)\"\'/", "data-$1='{\"$2\"}'", $the_content);
				    	                    $the_content = preg_replace("/data-(.+?)=\'&quot;(.+?)&quot;\'/", "data-$1='{\"$2\"}'", $the_content);
											$the_content = preg_replace_callback(
										        '/data-(.+?)=\'\{(.+?)\}\'/',
										        'shortcodelic_match_quotes',
										        $the_content
										    );
							                $the_content = wp_specialchars_decode($the_content, ENT_QUOTES); $the_content = $wp_embed->autoembed(apply_filters('the_content', $the_content));
							                $out .= $the_content;
							                $out .= "</div>";
							            }
							        }
						        }
	    	                }

			            }


                	$out .= "</div>";
	            }
	        }

	        	if ( isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['loading_bar']) && $shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['loading_bar']!='true' ) { 
		        	$slideshine_hidden = "slideshine_hidden";
		        } else {
		        	$slideshine_hidden = "";		        	
		        }
	        	$out .= "<div class='slideshine_loader $slideshine_hidden' style='background: $skin;'></div><!-- .slideshine_loader -->";
	        	if ( isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['loading_pie']) && $shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['loading_pie']=='true' ) {
		        	$out .= "<div class='slideshine_pie slideshine_fontsized $slideshine_hidden'><canvas id='canvas_$postid-$slideshow'></canvas></div><!-- .slideshine_pie -->";
		        	$bordered = "";
		        	$bged = "";
		        } else {
		        	$bordered = " bordered";
		        	$bged = " background: rgba($skinbgrgb[0],$skinbgrgb[1],$skinbgrgb[2],$skinopacity);";
		        }

	        	if ( isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['play_pause']) && $shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['play_pause']=='true' ) { 
		        	$out .= "<div class='slideshine_play scicon-entypo-play' style='color: $skin;'></div><!-- .slideshine_play --><div class='slideshine_pause scicon-entypo-pause' style='color: $skin;'></div><!-- .slideshine_pause -->";
			        $out .= "<div class='slideshine_pie_bg slideshine_fontsized slideshine_skinbg$bordered' style='color: $skin; border-color: $skin; background: $skinbg; background: rgba($skinbgrgb[0],$skinbgrgb[1],$skinbgrgb[2],$skinopacity);'></div>";
		        }

	        	if ( isset($shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['prev_next']) && $shortcodelic_slideshows['shortcodelic_slideshows_'.$slideshowSize]['prev_next']=='true' ) { 
		        	$out .= "<div class='slideshine_prev slideshine_fontsized scicon-entypo-left-open-big' style='color: $skin; border-color: $skin; background: $skinbg; background: rgba($skinbgrgb[0],$skinbgrgb[1],$skinbgrgb[2],$skinopacity);'></div><!-- .slideshine_prev --><div class='slideshine_next slideshine_fontsized scicon-entypo-right-open-big' style='color: $skin; border-color: $skin; background: $skinbg; background: rgba($skinbgrgb[0],$skinbgrgb[1],$skinbgrgb[2],$skinopacity);'></div><!-- .slideshine_next -->";
		        }

		        $out .= "<div class='slideshine_waiter hiddenspinner'>
		        	<div class='spinner slideshine_fontsized' style='border-top-color: $skin;$bged'></div>
		        </div>";

        	$out .= "</div><!-- .pix_slideshine.pix_slideshine-post-id-$postid.pix_slideshine-$slideshow -->";

        	$out .= "<div class='slideshine_margin slideshine_margin-id-$postid-slideshow-$slideshow'></div>";

        	$out .= "<!--[if IE 9]></div><![endif]-->";
        	$out .= "<!--[if lte IE 8]></div><![endif]-->";

	    }

		return $out;

	}	

	/**
	 * Tabs/accordion shortcode.
	 *
	 * @since    2.0.0
	 */
	public function shortcodelicTabsSC($atts) {

		wp_enqueue_style( $this->plugin_slug . '-tabs' );
		wp_enqueue_script( $this->plugin_slug . '-tabs', SHORTCODELIC_URL.'scripts/tabs.js', array($this->plugin_slug.'-modernizr','jquery','jquery-easing',$this->plugin_slug . '-transit',$this->plugin_slug . '-plugins'));

		global $post;

		if (!function_exists('shortcodelic_match_quotes')) {
			function shortcodelic_match_quotes($matches) {
	            return preg_replace('/&quot;/','"',$matches[0]);
	        	$icopts = json_decode($matches[0]);
	        }
		}

		if (!function_exists('shortcodelic_match_quotes_2')) {
			function shortcodelic_match_quotes_2($matches) {
	        	$icopts = json_decode($matches[1]);
	        	$icoin = '';
	        	$icoout = '';
	        	if ( isset($icopts->style) ) {
		        	$icoout .= 'data-style="'.$icopts->style.'" ';
	        	}
	        	if ( isset($icopts->size) ) {
		        	$icoin .= 'font-size:'.$icopts->size.'px;';
	        	}
	        	if ( isset($icopts->color) ) {
		        	$icoin .= 'color:'.$icopts->color.';';
	        	}
	        	if ( isset($icopts->bg) ) {
	        		if ( isset($icopts->style) && $icopts->style=='disc' ) {
			        	$icoin .= 'background-color:'.$icopts->bg.';';
			        } elseif ( isset($icopts->style) && $icopts->style=='circle' ) {
			        	$icoin .= 'border-color:'.$icopts->color.';';
			        }
	        	}
	        	if ( $icoin!='' ) {
	        		$icoout .= 'style="'.$icoin.'"';
	        	}
	            return $icoout;
	        }
		}

		extract(shortcode_atts(array(
	        'postid' => '',
	        'tabs' => ''
	    ), $atts));

	    $postid = $postid == '' ? $post->ID : $postid;

        global $wp_embed;
	    $out = '';

		$values = get_post_custom( $postid );
		$shortcodelic_tabs = isset( $values['shortcodelic_tabs'] ) ? $values['shortcodelic_tabs'][0] : '';
		$shortcodelic_tabs = maybe_unserialize($shortcodelic_tabs);
		$tabsSize = $tabs;

        if (is_array($shortcodelic_tabs)) {

        	$tabsvsacc = esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tabsvsacc']);
        	$active = esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['active']);
        	$minwidth = esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['minwidth']);
        	$easing = esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['easing']);
        	$speed = esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['speed']);
        	$fx = implode(",", $shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['fx']);

        	$out .= "<!--[if lte IE 8]><div class='ie8-'><![endif]-->";
        	$out .= "<!--[if IE 9]><div class='ie9'><![endif]-->";

        	$type = 'tabs';
        	$columned = '';
        	if ( $tabsvsacc == 'accordion' ) { $type = $tabsvsacc; } else { $columned = ' simple exsimple'; }
        	if ( $tabsvsacc == 'tabsleft' || $tabsvsacc == 'tabsright' ) $columned = " columned $tabsvsacc";

        	$out .= "<div class='pix_tabs$columned pix_tabs-post-$postid pix_tabs-$tabs' data-opts='{\"active\":".($active-1).",\"type\":\"$type\",\"minwidth\":$minwidth,\"easing\":\"$easing\",\"speed\":\"$speed\",\"fx\":\"$fx\"}'>";

	        if (isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab']) && is_array($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'])) { 
	        	foreach ($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'] as $key => &$slide) {

                    $title = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['title']) ? esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['title']) : '';
                    $icon = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['icon']) ? $shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['icon'] : '';
                    $icon_2 = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['icon_2']) ? $shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['icon_2'] : '';
                    $content = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['caption']) ? esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['caption']) : '<p>'.__('content', 'shortcodelic').'</p>';
	                $the_content = wp_specialchars_decode($content, ENT_QUOTES); $the_content = $wp_embed->autoembed(apply_filters('the_content', $the_content));
                    $out .= "<a href='#tab-$key-$postid-$tabs' class='tab header-$key-$postid-$tabs'><span class='icon-tab-open alignleft'>$icon</span><span class='icon-tab-closed alignleft'>$icon_2</span>$title</a>";
                    $out .= "<div id='tab-$key-$postid-$tabs'>$the_content</div><!-- #tab-$key-$postid-$tabs -->";
                    
                }
	        }

        	$out .= "</div><!-- .pix_$tabsvsacc -->";

        	$out .= "<!--[if IE 9]></div><![endif]-->";
        	$out .= "<!--[if lte IE 8]></div><![endif]-->";

			$out = preg_replace('/\<(.*?) class=([\'|"])scicon-(.*?)>(.*?)<\/(.*?)>/', '<$1 class=$2scicon-$3></$5>', $out);
			$out = preg_replace_callback(
		        '/data-icopts=([\'|"]){(.*?)}([\'|"])/',
		        'shortcodelic_match_quotes',
		        $out
		    );
			$out = preg_replace('/data-icopts=([\'|"]){(.*?)}([\'|"])/', 'data-icopts=\'{$2}\'', $out);
			$out = preg_replace_callback(
		        '/data-icopts=\'(.*?)\'/',
		        'shortcodelic_match_quotes_2',
		        $out
		    );
	    }

		return $out;

	}	


	/**
	 * Data tables shortcode.
	 *
	 * @since    2.0.0
	 */
	public function shortcodelicTablesSC($atts) {

		wp_enqueue_style( $this->plugin_slug . '-tables' );
		wp_enqueue_script( $this->plugin_slug . '-tables', SHORTCODELIC_URL.'scripts/tables.js', array($this->plugin_slug.'-modernizr','jquery'));

		global $post;

		if (!function_exists('shortcodelic_match_quotes')) {
			function shortcodelic_match_quotes($matches) {
	            return preg_replace('/&quot;/','"',$matches[0]);
	        	$icopts = json_decode($matches[0]);
	        }
		}

		if (!function_exists('shortcodelic_match_quotes_3')) {
			function shortcodelic_match_quotes_3($matches) {
	            return preg_replace('/"/','&quot;',$matches[0]);
	        	$icopts = json_decode($matches[0]);
	        }
		}

		extract(shortcode_atts(array(
	        'postid' => '',
	        'table' => ''
	    ), $atts));

	    $postid = $postid == '' ? $post->ID : $postid;

	    $out = '';

		$values = get_post_custom( $postid );
		$shortcodelic_tables = isset( $values['shortcodelic_tables'] ) ? $values['shortcodelic_tables'][0] : '';
		$shortcodelic_tables = maybe_unserialize($shortcodelic_tables);
		$tablesSize = $table;
		$rows = count($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table']);
		$rowW = 100/$rows;

        if (is_array($shortcodelic_tables)) {

	        $style = esc_attr($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['style']);
	        $minwidth = esc_attr($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['minwidth']);

        	$out .= "<!--[if lte IE 8]><div class='ie8-'><![endif]-->";
        	$out .= "<!--[if IE 9]><div class='ie9'><![endif]-->";

	        if (isset($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table']) && is_array($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'])) { 
	            $maxrows = 0;
	        	foreach ($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'] as $key => &$column) {
	                $table_elements = isset($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'][$key]['elements']) ? esc_attr($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'][$key]['elements']): '';
                    $table_elements = preg_replace('/\"\{\"(.*?)\"\}\"/', '\'{"$1"}\'', $table_elements);
                    $arr_elements = html_entity_decode($table_elements);
                    $arr_elements = preg_replace("/\"\{/", "{", $arr_elements);
                    $arr_elements = preg_replace("/\}\"/", "}", $arr_elements);
                    $arr_elements = preg_replace("/\;quot\;/", "&quot;", $arr_elements);
					$arr_elements = preg_replace_callback(
				        '/data-(.+?)=&quot;\{(.+?)\}&quot;/',
				        'shortcodelic_match_quotes_3',
				        $arr_elements
				    );
					$arr_elements = preg_replace_callback(
				        '/data-(.+?)=\{(.+?)\}/',
				        'shortcodelic_match_quotes_3',
				        $arr_elements
				    );
					$arr_elements = preg_replace_callback(
				        '/data-(.+?)=[\'"]\{(.+?)\}[\'"]/',
				        'shortcodelic_match_quotes_3',
				        $arr_elements
				    );
                    $arr_elements = preg_replace("/data-(.+?)=&quot;\{(.+?)\}&quot;/", "data-$1='$2'", $arr_elements);
                    $arr_elements = preg_replace("/data-(.+?)=(\'\")\{(.+?)\}(\'\")/", "data-$1='$2'", $arr_elements);
                    $arr_elements = preg_replace("/data-(.+?)=\{(.+?)\}/", "data-$1='$2'", $arr_elements);
                    $arr_elements = json_decode($arr_elements);
                    
	                if (!empty($arr_elements)) {
						foreach ($arr_elements as $k3y => $valu3) {
							$i = preg_replace("/slide_elements\[elements\]\[(.*?)\]\[(.*?)\]/", "$1", $arr_elements[$k3y]->name);
							$k = preg_replace("/slide_elements\[elements\]\[(.*?)\]\[(.*?)\]/", "$2", $arr_elements[$k3y]->name);
							$v = $arr_elements[$k3y]->value;
							$element[$key][$i][$k] = $v;
						}
						$maxrows = count($element[$key]) > $maxrows ? count($element[$key]) : $maxrows ;
					}
	        	}

	        	if ( $style == 'pricing' ) {
		        	$out .= "<div class='pix_tables pix_tables-post-$postid pix_tables-table-$table pricing' data-opts='{\"minwidth\":$minwidth}'>";
			        	foreach ($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'] as $key => &$column) {
			                $cell_active = isset($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'][$key]['active']) ? esc_attr($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'][$key]['active']): '';
							$highlighted = $cell_active != '' ? ' col-highlighted' : '';
							$highlighted .= $key%2 == 0 ? ' odd' : ' even';
							$out .= "<div class='table_column$highlighted' style='width:$rowW%'><div>";
							if ( isset($element[$key]) ) {
				        		foreach ($element[$key] as $el_key => $el_val ) {
									$alttd = $el_key%2 == 0 ? ' odd' : ' even';
									if ( isset($el_key) ) {
										$el = $element[$key][$el_key];
			    	                    $the_content = preg_replace("/data-(.+?)=\'\"(.+?)\"\'/", "data-$1='{\"$2\"}'", $el['caption']);
			    	                    $the_content = preg_replace("/data-(.+?)=\'&quot;(.+?)&quot;\'/", "data-$1='{\"$2\"}'", $the_content);
										$the_content = preg_replace_callback(
									        '/data-(.+?)=\'\{(.+?)\}\'/',
									        'shortcodelic_match_quotes',
									        $the_content
									    );
									    $the_content = wp_specialchars_decode($the_content, ENT_QUOTES);
										$out .= "<span class='table-cell ".$el['class']." ".$el['customclass']."$alttd'>".apply_filters('the_content', $the_content)."</span>";
									}
								}
							}
							$out .= "</div></div>";
						}
		        	$out .= "</div><!-- .pix_tables -->";
					$out .= "<div class='clear'></div>";
				} else {
		        	$out .= "<div class='pix_tables pix_tables-post-$postid pix_tables-table-$table data off' data-opts='{\"minwidth\":$minwidth}'>";
						$out .= "<table>";
							$out .= "<colgroup>";
				        	foreach ($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'] as $key => &$column) {
				                $cell_active = isset($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'][$key]['active']) ? esc_attr($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'][$key]['active']): '';
								$highlighted = $cell_active != '' ? ' col-highlighted' : '';
								$highlighted .= $key%2 == 0 ? ' odd' : ' even';
								$out .= "<col class='table_column$highlighted'>";
							}
							$out .= "</colgroup>";
							for ($irow = 1; $irow <= $maxrows; $irow++) {
								$alttr = $irow%2 == 0 ? ' odd' : ' even';
								$out .= "<tr class='$alttr'>";
					        	foreach ($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'] as $key => &$column) {
					        		$td = $irow==1 ? 'th' : 'td';
									$alttd = $key%2 == 0 ? ' odd' : ' even';
									if ( $element[$key][$irow]['caption']=='' ) {
										$out .= "<$td class='empty$alttd' style='width:$rowW%'>&nbsp;</$td>";
									} else {
			    	                    $the_content = preg_replace("/data-(.+?)=\'\"(.+?)\"\'/", "data-$1='{\"$2\"}'", $element[$key][$irow]['caption']);
			    	                    $the_content = preg_replace("/data-(.+?)=\'&quot;(.+?)&quot;\'/", "data-$1='{\"$2\"}'", $the_content);
										$the_content = preg_replace_callback(
									        '/data-(.+?)=\'\{(.+?)\}\'/',
									        'shortcodelic_match_quotes',
									        $the_content
									    );
									    $the_content = wp_specialchars_decode($the_content, ENT_QUOTES);
										$out .= "<$td class='".$element[$key][$irow]['class']." ".$element[$key][$irow]['customclass']."$alttd' style='width:$rowW%'>".apply_filters('the_content', $the_content)."</$td>";
									}
								}
								$out .= "</tr>";
							}
						$out .= "</table>";
		        	$out .= "</div><!-- .pix_tables -->";
		        	$out .= "<div class='pix_tables pix_tables-post-$postid pix_tables-table-$table data alt' data-opts='{\"minwidth\":$minwidth}'>";
						$out .= "<table>";
				        	foreach ($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'] as $key => &$column) {
				                $cell_active = isset($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'][$key]['active']) ? esc_attr($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'][$key]['active']): '';
								$highlighted = $cell_active != '' ? ' col-highlighted' : '';
								$highlighted .= $key%2 == 0 ? ' odd' : ' even';
								$out .= "<tr>";
				        		foreach ($element[$key] as $el_key => $el_val ) {
									$alttd = $el_key%2 == 0 ? ' odd' : ' even';
									if ( isset($el_key) ) {
										$el = $element[$key][$el_key];
										if ( $el_key == 1 ) {
											$td = 'th';
										} else {
											$td = 'td';
										}
			    	                    $the_content = preg_replace("/data-(.+?)=\'\"(.+?)\"\'/", "data-$1='{\"$2\"}'", $el['caption']);
			    	                    $the_content = preg_replace("/data-(.+?)=\'&quot;(.+?)&quot;\'/", "data-$1='{\"$2\"}'", $the_content);
										$the_content = preg_replace_callback(
									        '/data-(.+?)=\'\{(.+?)\}\'/',
									        'shortcodelic_match_quotes',
									        $the_content
									    );
									    $the_content = wp_specialchars_decode($the_content, ENT_QUOTES);
										$out .= "<$td class='".$el['class']."  ".$el['customclass']."$alttd' style='width:$rowW%'>".apply_filters('the_content', $the_content)."</$td>";
									}
								}
								$out .= "</tr>";
							}
						$out .= "</table>";
		        	$out .= "</div><!-- .pix_tables -->";
					$out .= "<div class='clear'></div>";
				}

			}

        	$out .= "<!--[if IE 9]></div><![endif]-->";
        	$out .= "<!--[if lte IE 8]></div><![endif]-->";

	    }

		return $out;

	}	

	/**
	 * Maps shortcode.
	 *
	 * @since    2.0.0
	 */
	public function shortcodelicMapsSC($atts) {

		if (!function_exists('shortcodelic_match_apostrophes')) {
			function shortcodelic_match_apostrophes($matches) {
	            $match = preg_replace('/\'/','[apostrophe;]',$matches[0]);
	            $match = preg_replace('/\"/','[quotes;]',$match);
	            return $match;
	        }
		}

		wp_enqueue_style( $this->plugin_slug . '-gmaps' );
		wp_enqueue_script( $this->plugin_slug . '-maps', SHORTCODELIC_URL.'scripts/maps.js', array($this->plugin_slug.'-modernizr','jquery',$this->plugin_slug.'-gmaps'));

		global $post;

		extract(shortcode_atts(array(
	        'postid' => '',
	        'map' => ''
	    ), $atts));

	    $postid = $postid == '' ? $post->ID : $postid;

	    $out = '';

		$values = get_post_custom( $postid );
		$shortcodelic_maps = isset( $values['shortcodelic_maps'] ) ? $values['shortcodelic_maps'][0] : '';
		$shortcodelic_maps = maybe_unserialize($shortcodelic_maps);
		$mapsSize = $map;

        if (is_array($shortcodelic_maps)) {

            $coords = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['coords']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['coords']) : '41.890322,12.492312';
            $width = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['width']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['width']) : '100';
            $height = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['height']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['height']) : '56';
	        $type = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['type']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['type']) : 'roadmap';
	        $heading = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['heading']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['heading']) : '90';
	        $pitch = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['pitch']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['pitch']) : '0';
            $zoom = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['zoom']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['zoom']) : '12';
            $maptype = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['maptype']) ? strtoupper(esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['maptype'])) : 'TOP_RIGHT';
            $pancontrol = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['pancontrol']) ? strtoupper(esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['pancontrol'])) : 'TOP_LEFT';
            $zoomcontrol = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['zoomcontrol']) ? strtoupper(esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['zoomcontrol'])) : 'TOP_LEFT';
            $scalecontrol = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['scalecontrol']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['scalecontrol']) : '""';
            $swcontrol = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['swcontrol']) ? strtoupper(esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['swcontrol'])) : 'TOP_LEFT';
            $start = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['start']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['start']) : '""';
            $end = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['end']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['end']) : '';
            $travelmode = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['travelmode']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['travelmode']) : '""';
            $color = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['color']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['color']) : '#d42027';
            $opacity = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['opacity']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['opacity']) : '0.75';
            $strokeweight = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['strokeweight']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['strokeweight']) : '5';
            $markers = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers']) && is_array($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers']) ? stripslashes(json_encode($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'])) : '""';
			$markers = preg_replace_callback(
		        '/<p>(.*?)<\/p>/',
		        'shortcodelic_match_apostrophes',
		        $markers
		    );
            $style = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['style']) ? html_entity_decode($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['style']) : '';

        	$out .= "<!--[if lte IE 8]><div class='ie8-'><![endif]-->";
        	$out .= "<!--[if IE 9]><div class='ie9'><![endif]-->";

        	$out .= "<div id='pix_maps-$map' class='pix_maps pix_maps-post-$postid pix_maps-map-$map' data-opts='{\"coords\":\"$coords\",\"width\":\"$width\",\"height\":\"$height\",\"type\":\"$type\",\"heading\":\"$heading\",\"pitch\":\"$pitch\",\"zoom\":\"$zoom\",\"maptype\":\"$maptype\",\"pancontrol\":\"$pancontrol\",\"zoomcontrol\":\"$zoomcontrol\",\"scalecontrol\":\"$scalecontrol\",\"swcontrol\":\"$swcontrol\",\"start\":\"$start\",\"end\":\"$end\",\"travelmode\":\"$travelmode\",\"color\":\"$color\",\"opacity\":\"$opacity\",\"strokeweight\":\"$strokeweight\",\"markers\":$markers,\"id\":\"$postid"."_$map\"}'>";

        	if ( $style!='' ) {
        		$style = preg_replace("/\s+/", " ", $style);
        		$style = preg_replace("/var(.+?)\=/", "", $style);
        		$style = preg_replace("/;/", "", $style);
        		$out .= "<script type='text/javascript'>
        			var styles_$postid"."_$map = $style;
        		</script>";
        	} else {
        		$out .= "<script type='text/javascript'>
        			var styles_$postid"."_$map = '';
        		</script>";
        	}

        	$out .= "</div><!-- .pix_maps -->";
			$out .= "<div class='clear'></div>";

        	$out .= "<!--[if IE 9]></div><![endif]-->";
        	$out .= "<!--[if lte IE 8]></div><![endif]-->";

        }

		return $out;

	}	

	/**
	 * Carousel shortcode.
	 *
	 * @since    2.0.0
	 */
	public function shortcodelicCarouselsSC($atts) {

		global $content_widthm, $post;
		if ( ! isset( $content_width ) ) $content_width = 1280;

		wp_enqueue_style( $this->plugin_slug . '-carousel' );
		wp_enqueue_script( $this->plugin_slug . '-carousel', SHORTCODELIC_URL.'scripts/carousel.js', array($this->plugin_slug.'-modernizr','jquery',$this->plugin_slug.'-bxslider'));

		extract(shortcode_atts(array(
	        'postid' => '',
	        'carousel' => ''
	    ), $atts));

	    $postid = $postid == '' ? $post->ID : $postid;


	    $out = '';

		$values = get_post_custom( $postid );
		$shortcodelic_carousels = isset( $values['shortcodelic_carousels'] ) ? $values['shortcodelic_carousels'][0] : '';
		$shortcodelic_carousels = maybe_unserialize($shortcodelic_carousels);
		$carouselSize = $carousel;

        if (is_array($shortcodelic_carousels)) {

	        $mode = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['mode']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['mode']) : 'horizontal';
        	$slidewidth = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slidewidth']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slidewidth']) : floor(($content_width+(20*3))/4);
        	$timeout = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['timeout']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['timeout']) : '7000';
	        $speed = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['speed']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['speed']) : '750';
	        $autoplay = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['autoplay']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['autoplay']) : 'true';
	        $hover = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['hover']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['hover']) : 'true';
        	$play_pause = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['play_pause']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['play_pause']) : 'true';
        	$prev_next = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['prev_next']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['prev_next']) : 'true';
        	$bullets = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['bullets']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['bullets']) : 'true';
        	$minslides = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['minslides']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['minslides']) : 4;
        	$maxslides = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['maxslides']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['maxslides']) : 4;
        	$slidemargin = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slidemargin']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slidemargin']) : 20;

        	$out .= "<!--[if lte IE 8]><div class='ie8-'><![endif]-->";
        	$out .= "<!--[if IE 9]><div class='ie9'><![endif]-->";

        	$out .= "<div id='pix_carousels-post-$postid-carousel-$carousel' class='pix_carousels pix_carousels-post-$postid pix_carousels-carousel-$carousel' data-opts='{\"mode\":\"$mode\",\"slidewidth\":\"$slidewidth\",\"timeout\":\"$timeout\",\"speed\":\"$speed\",\"autoplay\":\"$autoplay\",\"hover\":\"$hover\",\"play_pause\":\"$play_pause\",\"prev_next\":\"$prev_next\",\"bullets\":\"$bullets\",\"minslides\":\"$minslides\",\"maxslides\":\"$maxslides\",\"slidemargin\":\"$slidemargin\"}'>";

		        if (isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide']) && is_array($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'])) { 
		        	$i = 1; foreach ($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'] as $key => &$slide) {

                        $attachment_bg = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['bg']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['bg']) : '';
                        $attachment_id = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['id']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['id']): '';
                        $attachment_size = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['size']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['size']): '';
                        $attachment_box = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['box']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['box']): 'true';
                        $attachment_link = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['link']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['link']): '';
                        $attachment_target = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['target']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['target']): '';
                        $attachment_caption = isset($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['caption']) ? esc_attr($shortcodelic_carousels['shortcodelic_carousels_'.$carouselSize]['slide'][$key]['caption']): '';

                        $cover = '';
                        if ( $attachment_id != '' ) {
                            $attachment_bg = wp_get_attachment_image( $attachment_id, $attachment_size );
                        } else {
                            $attachment_bg = "<img src='$attachment_size'>";
                        }

		                $out .= "<div>";
		                $closelink = false;

		                if ( $attachment_box == 'true' && $attachment_id != '' ) {
                	        $attachment_full = wp_get_attachment_image_src( $attachment_id, 'full' );
		                	$out .= "<a href='$attachment_full[0]' data-rel='pix_carousels-post-$postid-carousel-$carousel'>";
		                	$closelink = true;
		                } elseif ( $attachment_link != '' ) {
		                	$out .= "<a href='$attachment_link' target='$attachment_target'>";
		                	$closelink = true;
		                }
		                $out .= $attachment_bg;
		                if ( $closelink == true ) {
		                	$out .= "</a>";
		                }
		                $the_content = wp_specialchars_decode($attachment_caption, ENT_QUOTES);
		                $out .= $the_content;
	                	$out .= "</div>";
		            }
		        }
        	$out .= "</div><!-- .pix_carousels -->";
			$out .= "<div class='clear'></div>";

        	$out .= "<!--[if IE 9]></div><![endif]-->";
        	$out .= "<!--[if lte IE 8]></div><![endif]-->";

        }

		return $out;

	}	

	/**
	 * Post carousel shortcode.
	 *
	 * @since    2.0.0
	 */
	public function shortcodelicPostCarouselsSC($atts) {

		global $content_width, $post;
		if ( ! isset( $content_width ) ) $content_width = 1280;

		wp_enqueue_style( $this->plugin_slug . '-carousel' );
		wp_enqueue_script( $this->plugin_slug . '-carousel', SHORTCODELIC_URL.'scripts/carousel.js', array($this->plugin_slug.'-modernizr','jquery',$this->plugin_slug.'-bxslider'));

		extract(shortcode_atts(array(
	        'postid' => '',
	        'carousel' => ''
	    ), $atts));

	    $postid = $postid == '' ? $post->ID : $postid;

	    $out = '';

		$values = get_post_custom( $postid );
		$shortcodelic_postcarousels = isset( $values['shortcodelic_postcarousels'] ) ? $values['shortcodelic_postcarousels'][0] : '';
		$shortcodelic_postcarousels = maybe_unserialize($shortcodelic_postcarousels);
		$postcarouselSize = $carousel;

        if (is_array($shortcodelic_postcarousels)) {

	        $cats = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['cats']) && is_array($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['cats']) ? $shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['cats'] : array();
	        $included = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['included']) && is_array($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['included']) ? $shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['included'] : array();
	        $excluded = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['excluded']) && is_array($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['excluded']) ? $shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['excluded'] : array();
	        $order = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['order']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['order']) : 'DESC';
	        $orderby = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['orderby']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['orderby']) : 'date';
	        $mode = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['mode']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['mode']) : 'horizontal';
        	$slidewidth = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['slidewidth']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['slidewidth']) : floor(($content_width+(20*3))/4);
        	$timeout = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['timeout']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['timeout']) : '7000';
	        $speed = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['speed']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['speed']) : '750';
	        $ppp = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['ppp']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['ppp']) : '10';
	        $post_title = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['post_title']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['post_title']) : 'true';
	        $featured_img = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['featured_img']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['featured_img']) : 'true';
	        $post_excerpt = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['post_excerpt']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['post_excerpt']) : 'true';
	        $link = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['link']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['link']) : 'post';
	        $linktitle = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['linktitle']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['linktitle']) : 'post';
	        $size = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['size']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['size']) : 'large';
	        $autoplay = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['autoplay']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['autoplay']) : 'true';
	        $hover = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['hover']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['hover']) : 'true';
        	$play_pause = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['play_pause']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['play_pause']) : 'true';
        	$prev_next = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['prev_next']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['prev_next']) : 'true';
        	$bullets = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['bullets']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['bullets']) : 'true';
        	$minslides = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['minslides']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['minslides']) : 2;
        	$maxslides = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['maxslides']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['maxslides']) : 4;
        	$slidemargin = isset($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['slidemargin']) ? esc_attr($shortcodelic_postcarousels['shortcodelic_postcarousels_'.$postcarouselSize]['slidemargin']) : 20;

        	$out .= "<!--[if lte IE 8]><div class='ie8-'><![endif]-->";
        	$out .= "<!--[if IE 9]><div class='ie9'><![endif]-->";

        	$out .= "<div id='pix_postcarousels-$carousel' class='pix_postcarousels pix_postcarousels-post-$postid pix_postcarousels-postcarousel-$carousel' data-opts='{\"mode\":\"$mode\",\"slidewidth\":\"$slidewidth\",\"timeout\":\"$timeout\",\"speed\":\"$speed\",\"autoplay\":\"$autoplay\",\"hover\":\"$hover\",\"play_pause\":\"$play_pause\",\"prev_next\":\"$prev_next\",\"bullets\":\"$bullets\",\"minslides\":\"$minslides\",\"maxslides\":\"$maxslides\",\"slidemargin\":\"$slidemargin\"}'>";

                $car_IDs = array();

        		$car_args1 = array(
        			'post_type' => 'post',
        			'taxonomy' => 'category',
        			'terms' => $cats,
        			'posts_per_page' => 100000000,
        			'orderby' => $orderby,
        			'order' => $order
    			);

                $query_car1 = new wp_query( $car_args1 );
                if ( $query_car1->have_posts() ) {
                    while ($query_car1->have_posts()) : $query_car1->the_post();
                        array_push( $car_IDs, $query_car1->post->ID );
                    endwhile;
                }
		        wp_reset_postdata();

        		$car_args2 = array(
        			'post_type' => 'post',
        			'post__in' => $included,
        			'post__not_in' => $excluded,
        			'posts_per_page' => 100000000,
        			'orderby' => $orderby,
        			'order' => $order
    			);

                $query_car2 = new wp_query( $car_args2 );
                if ( $query_car2->have_posts() ) {
                    while ($query_car2->have_posts()) : $query_car2->the_post();
                        array_push( $car_IDs, $query_car2->post->ID );
                    endwhile;
                }
		        wp_reset_postdata();

		        $car_IDs = array_unique($car_IDs);
        		$car_args = array(
        			'post_type' => 'post',
        			'post__in' => $car_IDs,
        			'posts_per_page' => $ppp,
        			'orderby' => $orderby,
        			'order' => $order
    			);

                $query_car = new wp_query( $car_args );
                if( $query_car->have_posts() ) {
                	while ( $query_car->have_posts() ) { $query_car->the_post();
						$out .= '<div>';
							$thumb = false;
							if ( has_post_thumbnail() ) {
								$thumb_id = get_post_thumbnail_id();
								$postTh = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
								
								if ( $link == 'post' ) {
									$out .= '<a href="'. get_permalink(). '" title="'. sprintf( esc_attr__( 'Go to %s', 'shortcodelic' ), the_title_attribute( 'echo=0' ) ) .'">';
								} elseif ( $link == 'image' ) {
									$thumb = true;
									$out .= '<a href="'.$postTh[0].'" data-rel="pix_carousels-post-'.$postid.'-carousel-'.$carousel.'">';
								}
								$out .= get_the_post_thumbnail(get_the_id(), $size );
								if ( $link != '' ) {
									$out .= '</a>';
								}
							}
							if ( $post_title == 'true' ) {
		                		$out .= '<h4>';
								if ( $linktitle == 'post' ) {
									$out .= '<a href="'. get_permalink(). '" title="'. sprintf( esc_attr__( 'Go to %s', 'shortcodelic' ), the_title_attribute( 'echo=0' ) ) .'">';
								} elseif ( $linktitle == 'image' && $thumb == true ) {
									$out .= '<a href="'.$postTh[0].'" data-rel="pix_carousels-post-'.$postid.'-carousel-'.$carousel.'">';
								}
		                		$out .= get_the_title();
								if ( $linktitle == 'post' || ($linktitle == 'image' && $thumb == true) ) {
									$out .= '</a>';
								}
		                		$out .= '</h4>';
		                	}
							if ( $post_excerpt == 'true' ) {
		                		$out .= wpautop('<p>'.get_the_excerpt().'</p>');
		                	}
						$out .= '</div>';
                	}
                }
                wp_reset_postdata();


		        
        	$out .= "</div><!-- .pix_postcarousels -->";
			$out .= "<div class='clear'></div>";

        	$out .= "<!--[if IE 9]></div><![endif]-->";
        	$out .= "<!--[if lte IE 8]></div><![endif]-->";

        }

		return $out;

	}	

	/**
	 * Check if WooCommerce is active.
	 *
	 * @since    2.0.0
	 */
	public function sc_is_woocommerce_active(){
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		if (is_plugin_active('woocommerce/woocommerce.php')) {
		    return true;
		} else {
		    return false;
		}
	}

	/**
	 * WooCommerce carousel shortcode.
	 *
	 * @since    2.0.0
	 */
	public function shortcodelicWooCarouselsSC($atts) {

		global $post, $woocommerce, $woocommerce_loop;

		if (self::sc_is_woocommerce_active()) {

			wp_enqueue_style( $this->plugin_slug . '-carousel' );
			wp_enqueue_script( $this->plugin_slug . '-carousel', SHORTCODELIC_URL.'scripts/carousel.js', array($this->plugin_slug.'-modernizr','jquery',$this->plugin_slug.'-bxslider'));

			extract(shortcode_atts(array(
		        'postid' => '',
		        'carousel' => ''
		    ), $atts));

		    $postid = $postid == '' ? $post->ID : $postid;

		    $out = '';

			$values = get_post_custom( $postid );
			$shortcodelic_woocarousels = isset( $values['shortcodelic_woocarousels'] ) ? $values['shortcodelic_woocarousels'][0] : '';
			$shortcodelic_woocarousels = maybe_unserialize($shortcodelic_woocarousels);
			$woocarouselSize = $carousel;

	        if (is_array($shortcodelic_woocarousels)) {

		        $type = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['type']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['type']) : '';
		        $category = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['cats']) && is_array($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['cats']) ? $shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['cats'] : array();
		        $included = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['included']) && is_array($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['included']) ? $shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['included'] : array();
		        $excluded = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['excluded']) && is_array($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['excluded']) ? $shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['excluded'] : array();
		        $order = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['order']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['order']) : 'DESC';
		        $orderby = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['orderby']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['orderby']) : 'date';
		        $mode = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['mode']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['mode']) : 'horizontal';
	        	$columns = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['column_n']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['column_n']) : '4';
	        	$timeout = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['timeout']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['timeout']) : '7000';
		        $speed = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['speed']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['speed']) : '750';
		        $per_page = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['ppp']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['ppp']) : '10';
		        $size = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['size']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['size']) : 'large';
		        $autoplay = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['autoplay']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['autoplay']) : 'true';
		        $hover = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['hover']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['hover']) : 'true';
	        	$play_pause = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['play_pause']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['play_pause']) : 'true';
	        	$prev_next = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['prev_next']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['prev_next']) : 'true';
	        	$bullets = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['bullets']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['bullets']) : 'true';
	        	$minslides = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['minslides']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['minslides']) : 4;
	        	$maxslides = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['maxslides']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['maxslides']) : 4;
	        	$slidemargin = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['slidemargin']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['slidemargin']) : 20;

	        	$out .= "<!--[if lte IE 8]><div class='ie8-'><![endif]-->";
	        	$out .= "<!--[if IE 9]><div class='ie9'><![endif]-->";

	        	$out .= "<div id='pix_woocarousels-$carousel' class='pix_woocarousels pix_woocarousels-post-$postid pix_woocarousels-woocarousel-$carousel pix-letmebe' data-opts='{\"mode\":\"$mode\",\"timeout\":\"$timeout\",\"speed\":\"$speed\",\"autoplay\":\"$autoplay\",\"hover\":\"$hover\",\"play_pause\":\"$play_pause\",\"prev_next\":\"$prev_next\",\"bullets\":\"$bullets\",\"minslides\":\"$minslides\",\"maxslides\":\"$maxslides\",\"slidemargin\":\"$slidemargin\"}'>";

	                if ( $type!='' &&  $type!='categories' ) {
		                $out .= do_shortcode("[$type per_page='$per_page' columns='$columns' orderby='$orderby' order='$order']");
		            } else {
						$ordering_args = $woocommerce->query->get_catalog_ordering_args( $orderby, $order );

					  	$args = array(
							'post_type'				=> 'product',
							'post_status' 			=> 'publish',
							'ignore_sticky_posts'	=> 1,
							'orderby' 				=> $ordering_args['orderby'],
							'order' 				=> $ordering_args['order'],
							'posts_per_page' 		=> $per_page,
							'meta_query' 			=> array(
								array(
									'key' 			=> '_visibility',
									'value' 		=> array('catalog', 'visible'),
									'compare' 		=> 'IN'
								)
							),
							'tax_query' 			=> array(
						    	array(
							    	'taxonomy' 		=> 'product_cat',
									'terms' 		=> $category,
									'field' 		=> 'slug',
									'operator' 		=> 'IN'
								)
						    )
						);

						if ( isset( $ordering_args['meta_key'] ) ) {
					 		$args['meta_key'] = $ordering_args['meta_key'];
					 	}

					  	ob_start();

						$products = new WP_Query( $args );

						$woocommerce_loop['columns'] = $columns;

						if ( $products->have_posts() ) : ?>

							<?php woocommerce_product_loop_start(); ?>

								<?php while ( $products->have_posts() ) : $products->the_post(); ?>

									<?php woocommerce_get_template_part( 'content', 'product' ); ?>

								<?php endwhile; // end of the loop. ?>

							<?php woocommerce_product_loop_end(); ?>

						<?php endif;

						wp_reset_postdata();

						$out .= '<div class="woocommerce">' . ob_get_clean() . '</div>';
		            }
			        
	        	$out .= "</div><!-- .pix_woocarousels -->";
				$out .= "<div class='clear'></div>";

	        	$out .= "<!--[if IE 9]></div><![endif]-->";
	        	$out .= "<!--[if lte IE 8]></div><![endif]-->";

	        }

			return $out;
		}

	}	

	/**
	 * Progress indicators.
	 *
	 * @since    1.0.0
	 */
	public function shortcodelicProgressSC($atts, $content = null) {

		global $content_width;
		if ( ! isset( $content_width ) ) $content_width = 1280;

		wp_enqueue_style( $this->plugin_slug . '-progress' );
		wp_enqueue_script( $this->plugin_slug . '-progress', SHORTCODELIC_URL.'scripts/progress.js', array($this->plugin_slug.'-chart',$this->plugin_slug.'-modernizr','jquery','jquery-easing',$this->plugin_slug . '-transit',$this->plugin_slug . '-plugins'));

		extract(shortcode_atts(array(
	        'label2' => '',
	        'unit' => '',
	        'percent' => '',
	        'value' => '',
	        'maximum' => '',
	        'barcolor' => '',
	        'trackcolor' => '',
	        'style' => '',
	        'linecap' => '',
	        'linewidth' => '',
	        'size' => '',
	        'animate' => '',
	        'class' => '',
	        'id' => ''
	    ), $atts));

	    if ( $value=='' ) {
	    	$value = $percent;
	    	$maximum = 100;
	    }

	    $out = '';

	    $id = $id !== '' ? " id='$id'" : '';

    	if ( $style=='pie') {
	    	$out .= "<span class='pix_progress_$style $class'$id data-opts='{\"barColor\":\"$barcolor\",\"trackColor\":\"$trackcolor\",\"lineCap\":\"$linecap\",\"lineWidth\":\"$linewidth\",\"size\":\"$size\",\"animate\":\"$animate\"}'>
	    	<canvas class=\"doughnut\" width=\"$size\" height=\"$size\" data-value=\"$value\" data-maximum=\"$maximum\"></canvas>";
    	} else {
    	$out .= "<span class='pix_progress_$style $class'$id data-opts='{\"barColor\":\"$barcolor\",\"trackColor\":\"$trackcolor\",\"lineCap\":\"$linecap\",\"lineWidth\":\"$linewidth\",\"size\":\"$size\",\"animate\":\"$animate\"}'>
    		<span data-value=\"$value\" data-maximum=\"$maximum\">";
	    }

    	if ( $label2=='true' ) {
	    	$out .= "<span class='chart-amount'>
	    		<span class='percent label'>$content</span>
	    	</span>";
    	} else {
	    	$out .= "<span class='chart-amount'>
	    		<span class='percent'></span><span class='unit'>$unit</span>
	    	</span>
	    	<span class='chart-label'>$content</span>";
    	}

    	if ( $style!='pie') {
	    	$out .= "</span>";
    	}

    	$out .= "</span>";

		return $out;

	}	

	/**
	 * Text boxes.
	 *
	 * @since    1.0.0
	 */
	public function shortcodelicBoxSC($atts, $content = null) {

		global $post;

		wp_enqueue_style( $this->plugin_slug . '-box' );
		wp_enqueue_style( $this->plugin_slug . '-fontello' );
		wp_enqueue_script( $this->plugin_slug . '-box', SHORTCODELIC_URL.'scripts/box.js', array($this->plugin_slug.'-modernizr','jquery',$this->plugin_slug.'-cookie'));

		extract(shortcode_atts(array(
	        'position' => '',
	        'custom' => '',
	        'dismiss' => ''
	    ), $atts));

		preg_match("/\<(.*?) class=[\'|\"]scicon-(.*?)<\/(.*?)>/", $content, $icon);
		if ( isset($icon[0]) ) {
			$icon = "<span class='shortcodelic-featured-icon'>$icon[0]</span>";
			$icon = preg_replace('/\<p\>/', '', $icon);
			$icon = preg_replace('/\<\/p\>/', '', $icon);
			$content = preg_replace("/\<(.*?) class=[\'|\"]scicon-(.*?)<\/(.*?)>/", "", $content, 1);
		} else {
			$icon = '';
		}

		$close = $dismiss != '' ? '<span class="close-box-sc"><i class="scicon-iconic-cancel"></i></span>' : '';
		$cookie = $dismiss != '' ? "data-cookie='".site_url()."_ID_".$post->ID."_".$dismiss."'" : '';

	    $out = '';

    	if ( $position == 'topicon' || $position == 'lefticon' ) {
	    	$out .= "<span class='pix_box $custom $position' $cookie>$close$icon<span class='shortcodelic-content-box'>".('<p>'.$content)."</span></span>";
	    } else {
	    	$out .= "<span class='pix_box $custom $position' $cookie>$close<span class='shortcodelic-content-box'>".('<p>'.$content)."</span>$icon</span>";
	    }

		$out = force_balance_tags($out);
		$out = preg_replace('/\<p\>\<\/p\>/', '', $out);

		return "<!--[if lte IE 8]><span class='ie8-'><![endif]-->
    	<!--[if IE 9]><span class='ie9'><![endif]-->" . $out . "
    	<!--[if IE 9]></span><![endif]-->
    	<!--[if lte IE 8]></span><![endif]-->";


	}	

	/**
	 * License checker.
	 *
	 * @since    1.2.0
	 */
	public function check_license($context) {

		$request_url = 'http://www.pixedelic.com/api/products/shortcodelic.php';

		$request_string = array(
				'body' => array(
					'action' => 'check_shortcodelic_license', 
					'id' => '6076939',
					'username' => $_REQUEST['shortcodelic_user_name'],
					'license' => $_REQUEST['shortcodelic_license_key']
				)
			);
		
		$raw_response = wp_remote_post($request_url, $request_string);

		if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200)) {
			$body = addslashes($raw_response['body']);
			if( stripos($body,'perfect')!=false ) { ?>
<script type="text/javascript">
/* <![CDATA[ */
var shortcodelic_check_license = 'true',
	shortcodelic_check_message = '<?php echo $body; ?>';
/* ]]> */
</script>
			<?php } else { ?>
<script type="text/javascript">
/* <![CDATA[ */
var shortcodelic_check_license = 'false',
	shortcodelic_check_message = '<?php echo $body; ?>';
/* ]]> */
</script>
			<?php }
		}
		
	}

	/**
	 * Check for update.
	 *
	 * @since    1.2.0
	 */
	public function check_for_plugin_update($checked_data) {
		global $wp_version;

		$api_url = 'http://www.pixedelic.com/api/';

		if (empty($checked_data->checked))
			return $checked_data;
		
		$args = array(
			'dir' => sanitize_title( $this->plugin_name ),
			'slug' => $this->plugin_slug,
			'version' => $checked_data->checked[ SHORTCODELIC_NAME ],
			'id' => '6076939',
			'user' => get_option('shortcodelic_user_name'),
			'license' => get_option('shortcodelic_license_key')
		);

		$request_string = array(
				'body' => array(
					'action' => 'basic_check', 
					'request' => serialize($args),
					'api-key' => md5(get_bloginfo('url'))
				),
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
			);
		
		$raw_response = wp_remote_post($api_url, $request_string);

		if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
			$response = unserialize($raw_response['body']);

		if (is_object($response) && !empty($response)) // Feed the update data into WP updater
			$checked_data->response[ SHORTCODELIC_NAME ] = $response;
		
		return $checked_data;
	}

	/**
	 * Call for update.
	 *
	 * @since    1.2.0
	 */
	public function plugin_api_call($def, $action, $args) {
		global $wp_version;
		
		$api_url = 'http://www.pixedelic.com/api/';
		
		if (!isset($args->slug) || ($args->slug != $this->plugin_slug))
			return false;
		
		$plugin_info = get_site_transient('update_plugins');
		$current_version = $plugin_info->checked[ SHORTCODELIC_NAME ];
		$args->version = $current_version;
		
		$request_string = array(
				'body' => array(
					'action' => $action, 
					'request' => serialize($args),
					'api-key' => md5(get_bloginfo('url'))
				),
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
			);
		
		$request = wp_remote_post($api_url, $request_string);

		if (is_wp_error($request)) {
			$res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
		} else {
			$res = unserialize($request['body']);

			if ($res === false)
				$res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
		}
		
		return $res;
	}


	/**
	 * Workaround for Sharedaddy.
	 *
	 * @since    2.0.0
	 */
	public function shortcodelic_remove_sharedaddy() {
	    remove_filter( 'the_content', 'sharing_display',19 );
	    if ( class_exists( 'Jetpack_Likes' ) ) {
	        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
	    }
	}
 

	/**
	 * Workaround for Sharedaddy (2).
	 *
	 * @since    2.0.0
	 */
	public function shortcodelic_append_sharedaddy($content) {
		if ( function_exists( 'sharing_display' ) ) {
			$content .= sharing_display( '', false );
		}
 
		if ( class_exists( 'Jetpack_Likes' ) ) {
		    $custom_likes = new Jetpack_Likes;
		    $content .= $custom_likes->post_likes( '' );
		}

		return $content;
	}

}