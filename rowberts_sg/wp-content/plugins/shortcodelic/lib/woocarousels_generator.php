<?php
if ( isset($_POST['postid']) ) {
    $scpostid = $_POST['postid'];
    require_once('../../../../wp-load.php');
} else {
    $scpostid = $scpostid;
}
?>
<div id="shortcodelic_woocarousels_generator" class="shortcodelic_meta" data-title="<?php _e('Shortcode generator &rarr; WooCommerce Carousel', 'shortcodelic'); ?>">
<?php
$values = get_post_custom( $scpostid );
$shortcodelic_woocarousels = isset( $values['shortcodelic_woocarousels'] ) ? $values['shortcodelic_woocarousels'][0] : '';
$shortcodelic_woocarousels = maybe_unserialize($shortcodelic_woocarousels);
$ajax_nonce = wp_create_nonce('shortcodelic_woocarousels_nonce');
$woocarouselSize = '1';
$woocarouselClone = '1';
$notEmpty = '0';
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 1280;
?>
    <div class="alignleft shortcodelic_options_area">
        <script type="text/javascript">
            var shortcodelic_woocarousels_nonce = '<?php echo $ajax_nonce; ?>';
        </script>
        <h3><?php _e('Select among the carousels available on this post', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select id="shortcodelic_woocarousels_manager_loader" class="shortcodelic_shortcode_manager_loader">
                    <option value='' data-type="woocarousels"><?php _e('Create a new carousel', 'shortcodelic'); ?></option>
                    <?php if (is_array($shortcodelic_woocarousels)) {
                        $woocarouselSize = sizeof($shortcodelic_woocarousels)+1;
                        $woocarouselClone = sizeof($shortcodelic_woocarousels)+1;
                        for ($key = 1; $key <= sizeof($shortcodelic_woocarousels); $key++) {
                            if ( !empty($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$key]) ) {
                            $notEmpty = $notEmpty + 1; ?>
                        <option value='[shortcodelic-woocarousel postid="<?php echo $scpostid; ?>" carousel="<?php echo $key ?>"]' data-type="woocarousels" data-link='&amp;shortcodelic_woocarousel_length=<?php echo $key ?>'>[shortcodelic-woocarousel postid=&quot;<?php echo $scpostid; ?>&quot; carousel=&quot;<?php echo $key ?>&quot;]</option>
                            <?php } else {
                                $woocarouselSize = $woocarouselSize < $key ? $woocarouselSize : $key;
                                $woocarouselClone = $woocarouselClone < $key ? $woocarouselClone : $key;
                            }
                        }
                    } ?>

                    <?php if ( $notEmpty == 0 ) {
                        delete_post_meta($scpostid, 'shortcodelic_woocarousels');
                    } ?>
                    </select>
            </span>
        </label>

        <div class="clear"></div>

        <?php
            if ( isset($_POST['shortcodelic_woocarousel_length']) && $_POST['shortcodelic_woocarousel_length']!='' ) {
                $woocarouselSize = $_POST['shortcodelic_woocarousel_length'];
            }
        ?>

        <div class="shortcodelic_opts_part">
            <?php if ( isset($_POST['shortcodelic_woocarousel_length']) && $_POST['shortcodelic_woocarousel_length']!='' ) { ?>
                <div class="alignleft">
                    <h3><?php _e('Clone your carousel', 'shortcodelic'); ?>:</h3>
                    <a href="#" class="clone-set pix_button" data-type="woocarousels" data-oldwoocarousels="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>" data-woocarousels="shortcodelic_woocarousels_<?php echo $woocarouselClone; ?>" data-link="&amp;shortcodelic_woocarousel_length=<?php echo $woocarouselClone; ?>"><?php _e('Clone this carousel', 'shortcodelic'); ?></a>
                </div>
                <div class="alignright">
                    <h3><?php _e('Delete your carousel', 'shortcodelic'); ?>:</h3>
                    <a href="#" class="delete-set pix_button" data-type="woocarousels" data-woocarousels="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>"><?php _e('Delete this carousel', 'shortcodelic'); ?></a>
                </div>
            <?php } ?>
            <div class="clear"></div><hr>

            <h3><?php _e('Select from recent products, featured ones, or from categories', 'shortcodelic'); ?>:</h3>
            <?php $type = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['type']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['type']) : ''; ?>
            <label class="for_select marginhack">
                <span class="for_select">
                    <select name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[type]">
                        <option value='' <?php selected( $type, '' ); ?>><?php _e('Choose...', 'shortcodelic'); ?></option>
                        <option value='categories' <?php selected( $type, 'categories' ); ?>><?php _e('Categories or IDs', 'shortcodelic'); ?></option>
                        <option value='recent_products' <?php selected( $type, 'recent_products' ); ?>><?php _e('Recent products', 'shortcodelic'); ?></option>
                        <option value='featured_products' <?php selected( $type, 'featured_products' ); ?>><?php _e('Featured products', 'shortcodelic'); ?></option>
                        <option value='sale_products' <?php selected( $type, 'sale_products' ); ?>><?php _e('Sale products', 'shortcodelic'); ?></option>
                        <option value='best_selling_products' <?php selected( $type, 'best_selling_products' ); ?>><?php _e('Best selling products', 'shortcodelic'); ?></option>
                        <option value='top_rated_products' <?php selected( $type, 'top_rated_products' ); ?>><?php _e('Top rated products', 'shortcodelic'); ?></option>
                    </select>
                </span>
            </label>
            
            <div class="clear"></div>

            <h3><?php _e('Select from one or more categories', 'shortcodelic'); ?>:</h3>
            <?php $cats = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['cats']) && is_array($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['cats']) ? $shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['cats'] : array();
            ?>
            <select name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[cats][]" multiple="multiple">
                <?php 
                shortcodelicHierWooCatSel( 0, $cats, '' );
                function shortcodelicHierWooCatSel( $par, $cats, $ind ) {
                    $argcats = array(
                        'taxonomy' => 'product_cat',
                        'hide_empty' => 0,
                        'orderby' => 'name',
                        'hierarchical' => 0,
                        'order' => 'ASC',
                        'parent' => $par,
                    );
                    $categories = get_categories($argcats);
                    if ( $categories ) {
                        if ( $par == 0 ) {
                            $ind = '';
                        } else {
                            $ind .= '&#8212;';
                        }
                        foreach ($categories as $category) { ?>
                            <option value='<?php echo $category->slug; ?>' <?php selected( true, in_array($category->slug, $cats) ); ?>> <?php echo $ind.$category->name; ?></option>
                        <?php shortcodelicHierWooCatSel( $category->term_id, $cats, $ind );
                        }
                    }
                }
                ?>
            </select>
            
            <div class="clear"></div>

            <h3><?php _e('Include products', 'shortcodelic'); ?>:</h3>
            <?php $included = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['included']) && is_array($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['included']) ? $shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['included'] : array();
            ?>
            <select name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[included][]" multiple="multiple">
                <?php 
                    $args = array( 
                        'posts_per_page' => -1,
                        'post_type' => 'product'
                    );
                    $posts = get_posts( $args );
                    foreach ( $posts as $postloop ) { ?>
                        <option value='<?php echo $postloop->ID; ?>' <?php selected( true, in_array($postloop->ID, $included) ); ?>> <?php echo $postloop->ID.'- '.$postloop->post_title; ?></option>
                    <?php } 
                    wp_reset_postdata();
                ?>
            </select>
            
            <div class="clear"></div>

            <h3><?php _e('Exclude products', 'shortcodelic'); ?>:</h3>
            <?php $excluded = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['excluded']) && is_array($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['excluded']) ? $shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['excluded'] : array();
            ?>
            <select name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[excluded][]" multiple="multiple">
                <?php 
                    $args = array( 
                        'posts_per_page' => -1,
                        'post_type' => 'product'
                    );
                    $posts = get_posts( $args );
                    foreach ( $posts as $postloop ) { ?>
                        <option value='<?php echo $postloop->ID; ?>' <?php selected( true, in_array($postloop->ID, $excluded) ); ?>> <?php echo $postloop->ID.'- '.$postloop->post_title; ?></option>
                    <?php } 
                    wp_reset_postdata();
                ?>
            </select>
            
            <div class="clear"></div>

            <h3><?php _e('Order', 'shortcodelic'); ?>:</h3>
            <?php $order = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['order']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['order']) : 'DESC'; ?>
            <label class="for_select marginhack">
                <span class="for_select">
                    <select name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[order]">
                        <option value='DESC' <?php selected( $order, 'DESC' ); ?>><?php _e('DESC', 'shortcodelic'); ?></option>
                        <option value='ASC' <?php selected( $order, 'ASC' ); ?>><?php _e('ASC', 'shortcodelic'); ?></option>
                    </select>
                </span>
            </label>

            <div class="clear"></div>

            <h3><?php _e('Order by', 'shortcodelic'); ?>:</h3>
            <?php $orderby = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['orderby']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['orderby']) : 'date'; ?>
            <label class="for_select marginhack">
                <span class="for_select">
                    <select name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[orderby]">
                        <option value='date' <?php selected( $orderby, 'date' ); ?>><?php _e('date', 'shortcodelic'); ?></option>
                        <option value='title' <?php selected( $orderby, 'title' ); ?>><?php _e('title', 'shortcodelic'); ?></option>
                        <option value='name' <?php selected( $orderby, 'name' ); ?>><?php _e('name', 'shortcodelic'); ?></option>
                        <option value='modified' <?php selected( $orderby, 'modified' ); ?>><?php _e('modified', 'shortcodelic'); ?></option>
                        <option value='rand' <?php selected( $orderby, 'rand' ); ?>><?php _e('rand', 'shortcodelic'); ?></option>
                        <option value='menu_order' <?php selected( $orderby, 'menu_order' ); ?>><?php _e('menu_order', 'shortcodelic'); ?></option>
                    </select>
                </span>
            </label>

            <div class="clear"></div>

        </div><!-- .shortcodelic_opts_part -->
    </div>

    <div class="alignleft shortcodelic_opts_list_2">

        <h3><?php _e('Shortcode <small>(if you want to use it on a different page/post)</small>', 'shortcodelic'); ?>:</h3>
        <input type="text" disabled="disabled" id="shortcodelic_get_sc" value="[shortcodelic-woocarousel postid=&quot;<?php echo $scpostid; ?>&quot; carousel=&quot;<?php echo $woocarouselSize; ?>&quot;]">

        <div class="clear"></div>

        <h3><?php _e('Mode', 'shortcodelic'); ?>:</h3>
        <?php $mode = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['mode']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['mode']) : 'horizontal'; ?>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[mode]">
                    <option value='horizontal' <?php selected( $mode, 'horizontal' ); ?>><?php _e('horizontal', 'shortcodelic'); ?></option>
                    <option value='vertical' <?php selected( $mode, 'vertical' ); ?>><?php _e('vertical', 'shortcodelic'); ?></option>
                    <option value='fade' <?php selected( $mode, 'fade' ); ?>><?php _e('fade', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>

        <div class="clear"></div>

        <h3><?php _e('Columns', 'shortcodelic'); ?>:</h3>
        <?php $column_n = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['column_n']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['column_n']) : 4; ?>
        <div class="slider_div columns">
            <input type="text" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[column_n]" value="<?php echo $column_n; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Timeout (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <?php $timeout = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['timeout']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['timeout']) : '7000'; ?>
        <div class="slider_div milliseconds">
            <input type="text" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[timeout]" value="<?php echo $timeout; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Speed (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <?php $speed = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['speed']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['speed']) : '750'; ?>
        <div class="slider_div milliseconds">
            <input type="text" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[speed]" value="<?php echo $speed; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

    </div>

    <div class="alignleft shortcodelic_opts_list">
        
        <h3><?php _e('Number of posts', 'shortcodelic'); ?>:</h3>
        <?php $ppp = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['ppp']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['ppp']) : '10'; ?>
        <div class="slider_div percent">
            <input type="text" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[ppp]" value="<?php echo $ppp; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Minimum number of slides', 'shortcodelic'); ?>:</h3>
        <?php $minslides = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['minslides']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['minslides']) : 4; ?>
        <div class="slider_div stroke">
            <input type="text" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[minslides]" value="<?php echo $minslides; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Maximum number of slides', 'shortcodelic'); ?>:</h3>
        <?php $maxslides = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['maxslides']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['maxslides']) : 4; ?>
        <div class="slider_div stroke">
            <input type="text" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[maxslides]" value="<?php echo $maxslides; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <h3><?php _e('Margin between each slide', 'shortcodelic'); ?>:</h3>
        <?php $slidemargin = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['slidemargin']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['slidemargin']) : 20; ?>
        <div class="slider_div grid">
            <input type="text" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[slidemargin]" value="<?php echo $slidemargin; ?>">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

        <?php $autoplay = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['autoplay']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['autoplay']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>_autoplay"><?php _e('Autoplay', 'shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[autoplay]" value="0">
            <input type="checkbox" id="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>_autoplay" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[autoplay]" value="true" <?php checked( $autoplay, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $hover = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['hover']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['hover']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>_hover"><?php _e('Pause on hover', 'shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[hover]" value="0">
            <input type="checkbox" id="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>_hover" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[hover]" value="true" <?php checked( $hover, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $play_pause = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['play_pause']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['play_pause']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>_play_pause"><?php _e('Play/pause button','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[play_pause]" value="0">
            <input type="checkbox" id="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>_play_pause" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[play_pause]" value="true" <?php checked( $play_pause, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $prev_next = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['prev_next']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['prev_next']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>_prev_next"><?php _e('Prev/next arrows','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[prev_next]" value="0">
            <input type="checkbox" id="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>_prev_next" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[prev_next]" value="true" <?php checked( $prev_next, 'true' ); ?>>
            <span></span>
        </label></h3>

        <?php $bullets = isset($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['bullets']) ? esc_attr($shortcodelic_woocarousels['shortcodelic_woocarousels_'.$woocarouselSize]['bullets']) : 'true'; ?>
        <h3><label class="alignleft" for="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>_bullets"><?php _e('Pagination bullets','shortcodelic'); ?>:
            <input type="hidden" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[bullets]" value="0">
            <input type="checkbox" id="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>_bullets" name="shortcodelic_woocarousels_<?php echo $woocarouselSize; ?>[bullets]" value="true" <?php checked( $bullets, 'true' ); ?>>
            <span></span>
        </label></h3>

        <div class="clear"></div>
        <br>

        <div class="clear"></div>

    </div><!-- .shortcodelic_opts_list -->
</div><!-- #shortcodelic_woocarousel_generator -->