<?php
	global $options;

    if (isset($_GET['page']) && $_GET['page']=='shortcodelic_verygeneral') {
?>

        <section id="pix_content_loaded">
            <h3><?php _e('Options','shortcodelic'); ?>: <small><?php _e('Very general','shortcodelic'); ?></small></h3>

            <form method="post" class="dynamic_form cf" action="<?php echo admin_url("admin.php?page=shortcodelic_admin"); ?>">
            
                <div class="pix_columns cf">
                    <div class="pix_column_divider alignleft"></div><!-- .pix_column_divider -->
                    <div class="pix_column alignleft">

                        <label for="shortcodelic_allow_ajax">
                            <input type="hidden" name="shortcodelic_allow_ajax" value="0">
                            <input type="checkbox" id="shortcodelic_allow_ajax" name="shortcodelic_allow_ajax" value="true" <?php checked( get_option('shortcodelic_allow_ajax'), 'true' ); ?>>
                            <span></span>
                            <?php _e('Enable ajax to save data','shortcodelic'); ?> <small>(<a href="#" data-dialog="<?php _e('Where available (not on this page, for instance) your options will be saved without refreshing the page.<br>If you encounter any problem just switch this field off.','shortcodelic'); ?>"><?php _e('more info','shortcodelic'); ?></a>)</small>
                        </label>
                        <br>
                        <br>

                        <label for="shortcodelic_inline_css">
                            <input type="hidden" name="shortcodelic_inline_css" value="0">
                            <input type="checkbox" id="shortcodelic_inline_css" name="shortcodelic_inline_css" value="true" <?php checked( get_option('shortcodelic_inline_css'), 'true' ); ?>>
                            <span></span>
                            <?php _e('Put the custom CSS inline','shortcodelic'); ?>
                        </label>
                        <br>
                        <br>
                        <br>

                    </div><!-- .pix_column.first -->
                    <div class="pix_column alignright">

                        <label for="shortcodelic_no_trace">
                            <input type="hidden" name="shortcodelic_no_trace" value="0">
                            <input type="checkbox" id="shortcodelic_no_trace" name="shortcodelic_no_trace" value="true" <?php checked( get_option('shortcodelic_no_trace'), 'true' ); ?>>
                            <span></span>
                            <?php _e('Remove any trace of Shortcodelic when you uninstall it','shortcodelic'); ?> <small>(<?php _e('a backup is always recommended...','shortcodelic'); ?> <a href="#" data-dialog="<?php _e('If you tick this checkbox you will lose any trace of the plugin and, if you install it again, you will have to start from scratch','shortcodelic'); ?>"><?php _e('more info','shortcodelic'); ?></a>)</small>
                        </label>
                        <br>
                        <br>
                        <br>

                    </div><!-- .pix_column.second -->
                </div><!-- .pix_columns -->

                <div class="clear"></div>

                <input type="hidden" name="action" value="shortocodelic_data_save" />
                <input type="hidden" name="shortcodelic_security" value="<?php echo wp_create_nonce('shortcodelic_data'); ?>" />
                <button type="submit" class="pix-save-options pix_button fake_button alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <button type="submit" class="pix-save-options pix_button fake_button2 alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <button type="submit" class="pix-save-options pix_button alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <div id="gradient-save-button"></div>

            </form><!-- .dynamic_form -->

        </section><!-- #pix_content_loaded -->
<?php 
	}