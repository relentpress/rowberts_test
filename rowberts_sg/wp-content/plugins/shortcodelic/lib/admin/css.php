<?php
	global $options;

    if (isset($_GET['page']) && $_GET['page']=='shortcodelic_css') {
?>

        <section id="pix_content_loaded">
            <h3><?php _e('Options','shortcodelic'); ?>: <small><?php _e('Compile CSS file','shortcodelic'); ?></small></h3>

            <?php if (get_option('shortcodelic_allow_ajax')=='true') { ?>
            <form action="/" class="dynamic_form ajax_form cf">
            <?php } else { ?>
            <form method="post" class="dynamic_form cf" action="<?php echo admin_url("admin.php?page=shortcodelic_admin"); ?>">
            <?php } ?>
            
                <div class="pix_columns cf">

                    <div class="clear"></div>
                    <label for="shortcodelic_css_code"><?php _e( 'Custom CSS', 'shortcodelic' ); ?>:</label>
                    <textarea name="shortcodelic_css_code" id="shortcodelic_css_code"><?php echo stripslashes(esc_html(get_option('shortcodelic_css_code'))); ?></textarea>

                    <br>
                    <br>
                    <br>

                </div><!-- .pix_columns -->

                <div class="clear"></div>

                <a class="pix-save-options save-css pix_button alignleft" href="#"><?php _e('Get the entire CSS file','shortcodelic'); ?></a>

                <input type="hidden" name="download_css" value="" />
                <input type="hidden" name="compile_css" value="compile_css" />
                <input type="hidden" name="action" value="shortocodelic_data_save" />
                <input type="hidden" name="shortcodelic_security" value="<?php echo wp_create_nonce('shortcodelic_data'); ?>" />

                <button type="submit" class="pix-save-options pix_button compile push alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <div id="gradient-save-button"></div>

            </form><!-- .dynamic_form -->

        </section><!-- #pix_content_loaded -->
<?php 
	}