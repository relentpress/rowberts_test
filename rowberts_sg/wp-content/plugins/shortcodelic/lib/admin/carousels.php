<?php
	global $options;

    if (isset($_GET['page']) && $_GET['page']=='shortcodelic_carousels') {
?>

        <section id="pix_content_loaded">
            <h3><?php _e('Options','shortcodelic'); ?>: <small><?php _e('Carousels','shortcodelic'); ?></small></h3>

            <?php if (get_option('shortcodelic_allow_ajax')=='true') { ?>
            <form action="/" class="dynamic_form ajax_form cf">
            <?php } else { ?>
            <form method="post" class="dynamic_form cf" action="<?php echo admin_url("admin.php?page=shortcodelic_admin"); ?>">
            <?php } ?>

                <div class="pix_columns cf">
                    <div class="pix_column_divider alignleft"></div><!-- .pix_column_divider -->
                    <div class="pix_column alignleft">

                        <label for="shortcodelic_carousel_commands_fore"><?php _e('Prev/next main color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_carousel_commands_fore" name="shortcodelic_carousel_commands_fore" type="text" value="<?php echo esc_attr(get_option('shortcodelic_carousel_commands_fore')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_carousel_commands_bg"><?php _e('Prev/next second color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_carousel_commands_bg" name="shortcodelic_carousel_commands_bg" type="text" value="<?php echo esc_attr(get_option('shortcodelic_carousel_commands_bg')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                    </div><!-- .pix_column.first -->
                    <div class="pix_column alignright">

                        <label for="shortcodelic_carousel_commands_opacity"><?php _e( 'Prev/next second color opacity', 'shortcodelic' ); ?>:</label>
                        <div class="slider_div opacity">
                            <input type="text" value="<?php echo stripslashes(esc_html(get_option('shortcodelic_carousel_commands_opacity'))); ?>" name="shortcodelic_carousel_commands_opacity" id="shortcodelic_carousel_commands_opacity">
                            <div class="slider_cursor"></div>
                        </div><!-- .slider_div -->
                        <br>

                        <label for="shortcodelic_carousel_nav"><?php _e( 'Navigation commands first color', 'shortcodelic' ); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_carousel_nav" name="shortcodelic_carousel_nav" type="text" value="<?php echo esc_attr(get_option('shortcodelic_carousel_nav')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                    </div><!-- .pix_column.second -->

                </div><!-- .pix_columns -->

                <div class="clear"></div>

                <input type="hidden" name="compile_css" value="compile_css" />

                <input type="hidden" name="action" value="shortocodelic_data_save" />
                <input type="hidden" name="shortcodelic_security" value="<?php echo wp_create_nonce('shortcodelic_data'); ?>" />
                <button type="submit" class="pix-save-options pix_button fake_button alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <button type="submit" class="pix-save-options pix_button fake_button2 alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <button type="submit" class="pix-save-options pix_button alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <div id="gradient-save-button"></div>

            </form><!-- .dynamic_form -->

        </section><!-- #pix_content_loaded -->
<?php 
	}