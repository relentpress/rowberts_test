<?php
	global $options;

    if (isset($_GET['page']) && $_GET['page']=='shortcodelic_tables') {
?>

        <section id="pix_content_loaded">
            <h3><?php _e('Options','shortcodelic'); ?>: <small><?php _e('Tables','shortcodelic'); ?></small></h3>

            <?php if (get_option('shortcodelic_allow_ajax')=='true') { ?>
            <form action="/" class="dynamic_form ajax_form cf">
            <?php } else { ?>
            <form method="post" class="dynamic_form cf" action="<?php echo admin_url("admin.php?page=shortcodelic_admin"); ?>">
            <?php } ?>

                <h4><?php _e('Pricing tables','shortcodelic'); ?></h4>

                <hr>
            
                <div class="pix_columns cf">
                    <div class="pix_column_divider alignleft"></div><!-- .pix_column_divider -->
                    <div class="pix_column alignleft">

                        <label for="shortcodelic_heading_border_color"><?php _e('Heading border color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_heading_border_color" name="shortcodelic_heading_border_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_heading_border_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_heading_background_color"><?php _e('Heading background color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_heading_background_color" name="shortcodelic_heading_background_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_heading_background_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                    </div><!-- .pix_column.first -->
                    <div class="pix_column alignright">

                        <label for="shortcodelic_heading_border_width"><?php _e( 'Heading border width', 'shortcodelic' ); ?>:</label>
                        <div class="slider_div stroke">
                            <input type="text" value="<?php echo stripslashes(esc_html(get_option('shortcodelic_heading_border_width'))); ?>" name="shortcodelic_heading_border_width" id="shortcodelic_heading_border_width">
                            <div class="slider_cursor"></div>
                        </div><!-- .slider_div -->
                        <br>

                        <label for="shortcodelic_heading_text_color"><?php _e('Heading text color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_heading_text_color" name="shortcodelic_heading_text_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_heading_text_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                    </div><!-- .pix_column.second -->

                </div><!-- .pix_columns -->

                <div class="clear"></div>

                <hr>

                <div class="pix_columns cf">
                    <div class="pix_column_divider alignleft"></div><!-- .pix_column_divider -->
                    <div class="pix_column alignleft">

                        <label for="shortcodelic_pricing_border_color"><?php _e('Pricing border color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_pricing_border_color" name="shortcodelic_pricing_border_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_pricing_border_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_pricing_background_color"><?php _e('Pricing background color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_pricing_background_color" name="shortcodelic_pricing_background_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_pricing_background_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                    </div><!-- .pix_column.first -->
                    <div class="pix_column alignright">

                        <label for="shortcodelic_pricing_border_width"><?php _e( 'Pricing border width', 'shortcodelic' ); ?>:</label>
                        <div class="slider_div stroke">
                            <input type="text" value="<?php echo stripslashes(esc_html(get_option('shortcodelic_pricing_border_width'))); ?>" name="shortcodelic_pricing_border_width" id="shortcodelic_pricing_border_width">
                            <div class="slider_cursor"></div>
                        </div><!-- .slider_div -->
                        <br>

                        <label for="shortcodelic_pricing_text_color"><?php _e('Pricing text color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_pricing_text_color" name="shortcodelic_pricing_text_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_pricing_text_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                    </div><!-- .pix_column.second -->

                </div><!-- .pix_columns -->

                <div class="clear"></div>

                <hr>

                <div class="pix_columns cf">
                    <div class="pix_column_divider alignleft"></div><!-- .pix_column_divider -->
                    <div class="pix_column alignleft">

                        <label for="shortcodelic_pricetable_border_color"><?php _e('Cell border color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_pricetable_border_color" name="shortcodelic_pricetable_border_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_pricetable_border_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_pricetable_background_color"><?php _e('Cell background color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_pricetable_background_color" name="shortcodelic_pricetable_background_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_pricetable_background_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_pricetable_text_color"><?php _e('Cell text color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_pricetable_text_color" name="shortcodelic_pricetable_text_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_pricetable_text_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_pricetable_border_width"><?php _e( 'Cell border width', 'shortcodelic' ); ?>:</label>
                        <div class="slider_div stroke">
                            <input type="text" value="<?php echo stripslashes(esc_html(get_option('shortcodelic_pricetable_border_width'))); ?>" name="shortcodelic_pricetable_border_width" id="shortcodelic_pricetable_border_width">
                            <div class="slider_cursor"></div>
                        </div><!-- .slider_div -->
                        <br>

                    </div><!-- .pix_column.first -->
                    <div class="pix_column alignright">

                        <label for="shortcodelic_pricetable_border_color_alt"><?php _e('Cell alternative border color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_pricetable_border_color_alt" name="shortcodelic_pricetable_border_color_alt" type="text" value="<?php echo esc_attr(get_option('shortcodelic_pricetable_border_color_alt')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_pricetable_background_color_alt"><?php _e('Cell alternative background color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_pricetable_background_color_alt" name="shortcodelic_pricetable_background_color_alt" type="text" value="<?php echo esc_attr(get_option('shortcodelic_pricetable_background_color_alt')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_pricetable_text_color_alt"><?php _e('Cell alternative text color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_pricetable_text_color_alt" name="shortcodelic_pricetable_text_color_alt" type="text" value="<?php echo esc_attr(get_option('shortcodelic_pricetable_text_color_alt')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                    </div><!-- .pix_column.second -->

                </div><!-- .pix_columns -->

                <div class="clear"></div>

                <hr>

                <div class="pix_columns cf">
                    <div class="pix_column alignleft">

                        <label for="shortcodelic_pricetable_gutter"><?php _e('Column gutter','shortcodelic'); ?>:</label>
                        <div class="slider_div border">
                            <input type="text" value="<?php echo stripslashes(esc_html(get_option('shortcodelic_pricetable_gutter'))); ?>" name="shortcodelic_pricetable_gutter" id="shortcodelic_pricetable_gutter">
                            <div class="slider_cursor"></div>
                        </div><!-- .slider_div -->
                        <br>

                    </div><!-- .pix_column.first -->
                </div><!-- .pix_columns -->

                <div class="clear"></div>

                <hr><br>
            
                <h4><?php _e('Data tables','shortcodelic'); ?></h4>

                <hr>
            
                <div class="pix_columns cf">
                    <div class="pix_column_divider alignleft"></div><!-- .pix_column_divider -->
                    <div class="pix_column alignleft">

                        <label for="shortcodelic_table_border_color"><?php _e('Cell border color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_table_border_color" name="shortcodelic_table_border_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_table_border_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_table_background_color"><?php _e('Cell background color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_table_background_color" name="shortcodelic_table_background_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_table_background_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_table_text_color"><?php _e('Cell text color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_table_text_color" name="shortcodelic_table_text_color" type="text" value="<?php echo esc_attr(get_option('shortcodelic_table_text_color')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_table_border_width"><?php _e( 'Cell border width', 'shortcodelic' ); ?>:</label>
                        <div class="slider_div stroke">
                            <input type="text" value="<?php echo stripslashes(esc_html(get_option('shortcodelic_table_border_width'))); ?>" name="shortcodelic_table_border_width" id="shortcodelic_table_border_width">
                            <div class="slider_cursor"></div>
                        </div><!-- .slider_div -->
                        <br>

                    </div><!-- .pix_column.first -->
                    <div class="pix_column alignright">

                        <label for="shortcodelic_table_border_color_alt"><?php _e('Cell alternative border color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_table_border_color_alt" name="shortcodelic_table_border_color_alt" type="text" value="<?php echo esc_attr(get_option('shortcodelic_table_border_color_alt')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_table_background_color_alt"><?php _e('Cell alternative background color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_table_background_color_alt" name="shortcodelic_table_background_color_alt" type="text" value="<?php echo esc_attr(get_option('shortcodelic_table_background_color_alt')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                        <label for="shortcodelic_table_text_color_alt"><?php _e('Cell alternative text color','shortcodelic'); ?>:</label>
                        <div class="pix_color_picker">
                            <input id="shortcodelic_table_text_color_alt" name="shortcodelic_table_text_color_alt" type="text" value="<?php echo esc_attr(get_option('shortcodelic_table_text_color_alt')); ?>">
                            <a class="pix_button" href="#"></a>
                            <div class="colorpicker"></div>
                            <i class="scicon-elusive-cancel"></i>
                        </div>
                        <br>

                    </div><!-- .pix_column.second -->

                </div><!-- .pix_columns -->

                <div class="clear"></div>

                <input type="hidden" name="compile_css" value="compile_css" />

                <input type="hidden" name="action" value="shortocodelic_data_save" />
                <input type="hidden" name="shortcodelic_security" value="<?php echo wp_create_nonce('shortcodelic_data'); ?>" />
                <button type="submit" class="pix-save-options pix_button fake_button alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <button type="submit" class="pix-save-options pix_button fake_button2 alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <button type="submit" class="pix-save-options pix_button alignright"><?php _e('Save options','shortcodelic'); ?><i class="scicon-awesome-ok"></i></button>
                <div id="gradient-save-button"></div>

            </form><!-- .dynamic_form -->

        </section><!-- #pix_content_loaded -->
<?php 
	}