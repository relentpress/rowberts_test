<?php
if ( isset($_POST['postid']) ) {
    $scpostid = $_POST['postid'];
    require_once('../../../../wp-load.php');
} else {
    $scpostid = $scpostid;
}
?>
<div id="shortcodelic_tables_generator" class="shortcodelic_meta" data-title="<?php _e('Shortcode generator &rarr; Data tables', 'shortcodelic'); ?>">
<?php
$values = get_post_custom( $scpostid );
$shortcodelic_tables = isset( $values['shortcodelic_tables'] ) ? $values['shortcodelic_tables'][0] : '';
$shortcodelic_tables = maybe_unserialize($shortcodelic_tables);
$ajax_nonce = wp_create_nonce('shortcodelic_tables_nonce');
$tablesSize = '1';
$tablesClone = '1';
$notEmpty = '0';

if (!function_exists('shortcodelic_match_tab_quotes')) {
    function shortcodelic_match_tab_quotes($matches) {
        return preg_replace("/&quot;/", ";quot;", $matches[0]);
    }
}
?>
    <div class="alignleft shortcodelic_options_area">
        <script type="text/javascript">
            var shortcodelic_tables_nonce = '<?php echo $ajax_nonce; ?>';
        </script>
        <h3><?php _e('Select among the tables available on this post', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select id="shortcodelic_tables_manager_loader" class="shortcodelic_shortcode_manager_loader">
                    <option value='' data-type="tables"><?php _e('Create a new table', 'shortcodelic'); ?></option>
                    <?php if (is_array($shortcodelic_tables)) {
                        $tablesSize = sizeof($shortcodelic_tables)+1;
                        $tablesClone = sizeof($shortcodelic_tables)+1;
                        for ($key = 1; $key <= sizeof($shortcodelic_tables); $key++) {
                            if ( !empty($shortcodelic_tables['shortcodelic_tables_'.$key]) ) {
                            $notEmpty = $notEmpty + 1; ?>
                        <option value='[shortcodelic-tables postid="<?php echo $scpostid; ?>" table="<?php echo $key ?>"]' data-type="tables" data-link='&amp;shortcodelic_tables_length=<?php echo $key ?>'>[shortcodelic-tables postid=&quot;<?php echo $scpostid; ?>&quot; table=&quot;<?php echo $key ?>&quot;]</option>
                            <?php } else {
                                $tablesSize = $tablesSize < $key ? $tablesSize : $key;
                                $tablesClone = $tablesClone < $key ? $tablesClone : $key;
                            }
                        }
                    } ?>

                    <?php if ( $notEmpty == 0 ) {
                        delete_post_meta($scpostid, 'shortcodelic_tables');
                    } ?>
                    </select>
            </span>
        </label>

        <div class="clear"></div>

	        <?php
	            if ( isset($_POST['shortcodelic_tables_length']) && $_POST['shortcodelic_tables_length']!='' ) {
	                $tablesSize = $_POST['shortcodelic_tables_length'];
	            }
	        ?>

        <div class="shortcodelic_opts_part">

	        <h3><?php _e('Shortcode <small>(if you want to use it on a different page/post)</small>', 'shortcodelic'); ?>:</h3>
	        <input type="text" disabled="disabled" id="shortcodelic_get_sc" value="[shortcodelic-tables postid=&quot;<?php echo $scpostid; ?>&quot; table=&quot;<?php echo $tablesSize; ?>&quot;]">

	        <div class="clear"></div>

            <?php if ( isset($_POST['shortcodelic_tables_length']) && $_POST['shortcodelic_tables_length']!='' ) { ?>
                <div class="alignleft">
                    <h3><?php _e('Clone this table', 'shortcodelic'); ?>:</h3>
                    <a href="#" class="clone-set pix_button" data-type="tables" data-oldtables="shortcodelic_tables_<?php echo $tablesSize; ?>" data-tables="shortcodelic_tables_<?php echo $tablesClone; ?>" data-link="&amp;shortcodelic_tables_length=<?php echo $tablesClone; ?>"><?php _e('Clone this table', 'shortcodelic'); ?></a>
                </div>
                <div class="alignright">
                    <h3><?php _e('Delete this table', 'shortcodelic'); ?>:</h3>
                    <a href="#" class="delete-set pix_button" data-type="tables" data-tables="shortcodelic_tables_<?php echo $tablesSize; ?>"><?php _e('Delete this table', 'shortcodelic'); ?></a>
                </div>
	            <div class="clear"></div><hr>
            <?php } ?>

	        <h3><?php _e('Style', 'shortcodelic'); ?>:</h3>
	        <?php $style = isset($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['style']) ? esc_attr($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['style']) : ''; ?>
	        <label class="for_select marginhack">
	            <span class="for_select">
	                <select name="shortcodelic_tables_<?php echo $tablesSize; ?>[style]">
	                    <option value="data" <?php selected( $style, '' ); ?>><?php _e('data', 'shortcodelic'); ?></option>
	                    <option value="pricing" <?php selected( $style, 'pricing' ); ?>><?php _e('pricing', 'shortcodelic'); ?></option>
	                </select>
	            </span>
	        </label>

	        <h3><?php _e('Display as one column table at width...', 'shortcodelic'); ?>:</h3>
	        <?php $minwidth = isset($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['minwidth']) ? esc_attr($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['minwidth']) : '0'; ?>
	        <div class="slider_div">
	            <input type="text" name="shortcodelic_tables_<?php echo $tablesSize; ?>[minwidth]" value="<?php echo $minwidth; ?>">
	            <div class="slider_cursor"></div>
	        </div><!-- .slider_div -->

	        <div class="clear"></div>

        </div><!-- .shortcodelic_opts_part -->

    </div>

    <div class="alignleft shortcodelic_opts_list_2 shortcodelic_table_creator_area">

        <div class="shortcodelic_el_list">
            <h3><?php _e('Edit your tables', 'shortcodelic'); ?>:</h3>
	        <a href="#" class="add-element pix_button"><?php _e('Add a column', 'shortcodelic'); ?></a>
            <br>
            <br>
            <div class="el-list table-column-list alignleft">
	            <div class="clone element-table element-sort">
	                <input type="hidden" data-name="shortcodelic_tables_<?php echo $tablesSize; ?>[table][key][elements]" value="">
	                <span class="table-el-ui-disable-text"></span>
	                <span class="table-el-ui no-el"></span>
	                <span class="table-el-ui edit-el column-el" data-panel="shortcodelic_edittable_panel"><i class="scicon-awesome-pencil"></i></span>
	                <span class="table-el-ui clone-el"><i class="scicon-awesome-docs"></i></span>
	                <span class="table-el-ui delete-el delete-col"><i class="scicon-awesome-trash"></i></span>
	                <span class="table-el-ui active-el"><i class="scicon-awesome-check"></i></span>
	                <span class="table-el-ui deactive-el"><i class="scicon-awesome-check-empty"></i></span>
	            </div><!-- .clone.element-table.element-sort -->
                <?php if (isset($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table']) && is_array($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'])) { $i = 1; foreach ($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'] as $key => &$tables) { ?>

                    <?php 
		                $cell_active = isset($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'][$key]['active']) ? esc_attr($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'][$key]['active']): '';
		                $table_elements = isset($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'][$key]['elements']) ? esc_attr($shortcodelic_tables['shortcodelic_tables_'.$tablesSize]['table'][$key]['elements']): '';
                        $table_elements = preg_replace_callback('|;quot;{(.*?)};quot;|', 'shortcodelic_match_tab_quotes', $table_elements);
	                    $arr_elements = html_entity_decode($table_elements);
	                    $arr_elements = preg_replace("/\"\{/", "{", $arr_elements);
	                    $arr_elements = preg_replace("/\}\"/", "}", $arr_elements);
	                    $arr_elements = preg_replace("/\;quot\;/", "&quot;", $arr_elements);
	                    $arr_elements = json_decode($arr_elements);
                    	$show = '';
                    	$hide = '';
                    	if ( $cell_active == 'true' ) {
                    		$show = ' style="display:block"';
                    		$hide = ' style="display:none"';
                    	}
                    ?>
		            <div class="element-table element-sort">
		                <input type="hidden" name="shortcodelic_tables_<?php echo $tablesSize; ?>[table][<?php echo $key; ?>][active]" value="<?php echo $cell_active; ?>">
		                <input type="hidden" name="shortcodelic_tables_<?php echo $tablesSize; ?>[table][<?php echo $key; ?>][elements]" value="<?php echo $table_elements; ?>">
		                <?php
    	                    if (!empty($arr_elements)) {
                                $element = array();
								foreach ($arr_elements as $k3y => $valu3) {
									$i = preg_replace("/slide_elements\[elements\]\[(.*?)\]\[(.*?)\]/", "$1", $arr_elements[$k3y]->name);
									$k = preg_replace("/slide_elements\[elements\]\[(.*?)\]\[(.*?)\]/", "$2", $arr_elements[$k3y]->name);
									$v = $arr_elements[$k3y]->value;
									$element[$i][$k] = $v;
								}
								foreach ($element as $k3y => $valu3) {
									if ( isset($k3y) ) {
										$elements = $element[$k3y];
										echo '<span class="table-el-block">'.$elements['caption'].'</span>';
									}
								}
							}
		                ?>
		                <span class="table-el-ui-disable-text"></span>
		                <span class="table-el-ui no-el"><?php echo $key; ?></span>
		                <span class="table-el-ui edit-el column-el" data-panel="shortcodelic_edittable_panel"><i class="scicon-awesome-pencil"></i></span>
		                <span class="table-el-ui clone-el"><i class="scicon-awesome-docs"></i></span>
		                <span class="table-el-ui delete-el delete-col"><i class="scicon-awesome-trash"></i></span>
		                <span class="table-el-ui active-el"<?php echo $show; ?>><i class="scicon-awesome-check"></i></span>
		                <span class="table-el-ui deactive-el"<?php echo $hide; ?>><i class="scicon-awesome-check-empty"></i></span>
		            </div><!-- .clone.element-table.element-sort -->
                <?php $i++; } } ?>
            </div><!-- .el-list -->
        </div><!-- .shortcodelic_el_list -->

        <div class="clear"></div>

    </div>
</div><!-- #shortcodelic_tables_generator -->

<?php /*********** COLUMN EDIT PANEL ***********/ ?>

<div id="shortcodelic_editcolumn_panel" class="shortcodelic_meta" data-title="<?php _e('Shortcode generator &rarr; Data tables &rarr; Column', 'shortcodelic'); ?>">

    <div class="alignleft shortcodelic_options_area shortcodelic_el_list">
        <a href="#" class="add-element pix_button alignleft"><?php _e('Add elements', 'shortcodelic'); ?></a>
        <div class="cf"></div>
        <br>
        <div class="el-list alignleft">
            <div class="clone element-slide element-sort">
                <textarea readonly="readonly" data-name="slide_elements[elements][key][caption]"></textarea>
                <input type="hidden" data-name="slide_elements[elements][key][class]" value="">
                <input type="hidden" data-name="slide_elements[elements][key][customclass]" value="">
                <span class="slide-el-ui-disable-text"></span>
                <span class="slide-el-ui no-el"></span>
                <span class="slide-el-ui edit-el" data-panel="shortcodelic_edittable_panel"><i class="scicon-awesome-pencil"></i></span>
                <span class="slide-el-ui clone-el"><i class="scicon-awesome-docs"></i></span>
                <span class="slide-el-ui delete-el"><i class="scicon-awesome-trash"></i></span>
            </div><!-- .clone.element-slide.element-sort -->
        </div>
    </div><!-- .shortcodelic_options_area_3 -->

</div>


<?php /*********** CELL EDIT PANEL ***********/ ?>

<div id="shortcodelic_edittable_panel" class="shortcodelic_meta shortcodelic_editelement_panel" data-title="<?php _e('Shortcode generator &rarr; Data tables &rarr; Column &rarr; Cell', 'shortcodelic'); ?>">

    <div class="alignleft shortcodelic_options_area">

        <h3><?php _e('Cell class', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select name="el_class">
                    <option value=""><?php _e('none', 'shortcodelic'); ?></option>
                    <option value="heading"><?php _e('heading', 'shortcodelic'); ?></option>
                    <option value="pricing"><?php _e('pricing', 'shortcodelic'); ?></option>
                </select>
            </span>
        </label>
        
        <div class="clear"></div>

        <h3><?php _e('Custom CSS class', 'shortcodelic'); ?>:</h3>
        <input type="text" name="el_customclass" value="">

        <div class="clear"></div>

    </div><!-- .shortcodelic_options_area -->

    <div class="alignleft shortcodelic_options_area_2 shortcodelic_tinymce_area">
    </div><!-- .shortcodelic_tinymce_area -->

</div>

