<?php
if ( isset($_POST['postid']) ) {
    $scpostid = $_POST['postid'];
    require_once('../../../../wp-load.php');
} else {
    $scpostid = $scpostid;
}
?>
<div id="shortcodelic_maps_generator" class="shortcodelic_meta" data-title="<?php _e('Shortcode generator &rarr; Maps', 'shortcodelic'); ?>">
<?php
$values = get_post_custom( $scpostid );
$shortcodelic_maps = isset( $values['shortcodelic_maps'] ) ? $values['shortcodelic_maps'][0] : '';
$shortcodelic_maps = maybe_unserialize($shortcodelic_maps);
$ajax_nonce = wp_create_nonce('shortcodelic_maps_nonce');
$mapsSize = '1';
$mapsClone = '1';
$notEmpty = '0';
?>
    <div class="alignleft shortcodelic_options_area">
        <script type="text/javascript">
            var shortcodelic_maps_nonce = '<?php echo $ajax_nonce; ?>';
        </script>
        <h3><?php _e('Select among the maps available on this post', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select id="shortcodelic_maps_manager_loader" class="shortcodelic_shortcode_manager_loader">
                    <option value='' data-type="maps"><?php _e('Create a new map', 'shortcodelic'); ?></option>
                    <?php if (is_array($shortcodelic_maps)) {
                        $mapsSize = sizeof($shortcodelic_maps)+1;
                        $mapsClone = sizeof($shortcodelic_maps)+1;
                        for ($key = 1; $key <= sizeof($shortcodelic_maps); $key++) {
                            if ( !empty($shortcodelic_maps['shortcodelic_maps_'.$key]) ) {
                            $notEmpty = $notEmpty + 1; ?>
                        <option value='[shortcodelic-maps postid="<?php echo $scpostid; ?>" map="<?php echo $key ?>"]' data-type="maps" data-link='&amp;shortcodelic_maps_length=<?php echo $key ?>'>[shortcodelic-maps postid=&quot;<?php echo $scpostid; ?>&quot; map=&quot;<?php echo $key ?>&quot;]</option>
                            <?php } else {
                                $mapsSize = $mapsSize < $key ? $mapsSize : $key;
                                $mapsClone = $mapsClone < $key ? $mapsClone : $key;
                            }
                        }
                    } ?>

                    <?php if ( $notEmpty == 0 ) {
                        delete_post_meta($scpostid, 'shortcodelic_maps');
                    } ?>
                    </select>
            </span>
        </label>

        <div class="clear"></div>

	        <?php
	            if ( isset($_POST['shortcodelic_maps_length']) && $_POST['shortcodelic_maps_length']!='' ) {
	                $mapsSize = $_POST['shortcodelic_maps_length'];
	            }
	        ?>

        <div class="shortcodelic_opts_part">

	        <h3><?php _e('Shortcode <small>(if you want to use it on a different page/post)</small>', 'shortcodelic'); ?>:</h3>
	        <input type="text" disabled="disabled" id="shortcodelic_get_sc" value="[shortcodelic-maps postid=&quot;<?php echo $scpostid; ?>&quot; map=&quot;<?php echo $mapsSize; ?>&quot;]">

	        <div class="clear"></div>

            <?php if ( isset($_POST['shortcodelic_maps_length']) && $_POST['shortcodelic_maps_length']!='' ) { ?>
                <div class="alignleft">
                    <h3><?php _e('Clone this map', 'shortcodelic'); ?>:</h3>
                    <a href="#" class="clone-set pix_button" data-type="maps" data-oldmaps="shortcodelic_maps_<?php echo $mapsSize; ?>" data-maps="shortcodelic_maps_<?php echo $mapsClone; ?>" data-link="&amp;shortcodelic_maps_length=<?php echo $mapsClone; ?>"><?php _e('Clone this map', 'shortcodelic'); ?></a>
                </div>
                <div class="alignright">
                    <h3><?php _e('Delete this map', 'shortcodelic'); ?>:</h3>
                    <a href="#" class="delete-set pix_button" data-type="maps" data-maps="shortcodelic_maps_<?php echo $mapsSize; ?>"><?php _e('Delete this map', 'shortcodelic'); ?></a>
                </div>
	            <div class="clear"></div><hr>
            <?php } ?>

            <h3><?php _e('Latitude and longitude (for instance: 41.890322,12.492312)', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('The default values include the latitude and longitude of the Colosseum (the Flavian Amphitheatre in Rome). To get latitude and longitute of a place on Google Maps just right click on a point on the map and select &quot;What\'s here&quot; from the context menu. Please take a look to this link: <a href=\'http://www.wikihow.com/Get-Longitude-and-Latitude-from-Google-Maps\' target=\'_blank\'>http://www.wikihow.com/Get-Longitude-and-Latitude-from-Google-Maps</a>','shortcodelic'); ?></p>"></i>:</h3>
            <?php $coords = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['coords']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['coords']) : '41.890322,12.492312'; ?>
            <input type="text" name="shortcodelic_maps_<?php echo $mapsSize; ?>[coords]" value="<?php echo $coords; ?>">

            <div class="clear"></div>

            <h3><?php _e('Width (percentage based on the map parent element)', 'shortcodelic'); ?>:</h3>
            <?php $width = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['width']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['width']) : '100'; ?>
            <div class="slider_div percentage">
                <input type="text" name="shortcodelic_maps_<?php echo $mapsSize; ?>[width]" value="<?php echo $width; ?>">
                <div class="slider_cursor"></div>
            </div><!-- .slider_div -->

            <div class="clear"></div>

            <h3><?php _e('Height (percentage based on the width here above)', 'shortcodelic'); ?>:</h3>
            <?php $height = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['height']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['height']) : '56'; ?>
            <div class="slider_div percentage">
                <input type="text" name="shortcodelic_maps_<?php echo $mapsSize; ?>[height]" value="<?php echo $height; ?>">
                <div class="slider_cursor"></div>
            </div><!-- .slider_div -->

            <div class="clear"></div>

	        <h3><?php _e('Type', 'shortcodelic'); ?>:</h3>
	        <?php $type = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['type']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['type']) : 'roadmap'; ?>
	        <label class="for_select marginhack">
	            <span class="for_select">
	                <select name="shortcodelic_maps_<?php echo $mapsSize; ?>[type]">
                        <option value="roadmap" <?php selected( $type, 'roadmap' ); ?>><?php _e('roadmap', 'shortcodelic'); ?></option>
                        <option value="hybrid" <?php selected( $type, 'hybrid' ); ?>><?php _e('hybrid', 'shortcodelic'); ?></option>
                        <option value="satellite" <?php selected( $type, 'satellite' ); ?>><?php _e('satellite', 'shortcodelic'); ?></option>
                        <option value="terrain" <?php selected( $type, 'terrain' ); ?>><?php _e('terrain', 'shortcodelic'); ?></option>
                        <option value="streetview" <?php selected( $type, 'streetview' ); ?>><?php _e('streetview', 'shortcodelic'); ?></option>
	                </select>
	            </span>
	        </label>

            <div class="clear"></div>

            <h3><?php _e('Zoom', 'shortcodelic'); ?>:</h3>
            <?php $zoom = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['zoom']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['zoom']) : '12'; ?>
            <div class="slider_div zoom">
                <input type="text" name="shortcodelic_maps_<?php echo $mapsSize; ?>[zoom]" value="<?php echo $zoom; ?>">
                <div class="slider_cursor"></div>
            </div><!-- .slider_div -->

            <div class="clear"></div>

            <h3><?php _e('Heading', 'shortcodelic'); ?>:</h3>
            <?php $heading = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['heading']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['heading']) : '90'; ?>
            <div class="slider_div heading">
                <input type="text" name="shortcodelic_maps_<?php echo $mapsSize; ?>[heading]" value="<?php echo $heading; ?>">
                <div class="slider_cursor"></div>
            </div><!-- .slider_div -->

            <h3><?php _e('Pitch', 'shortcodelic'); ?>:</h3>
            <?php $pitch = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['pitch']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['pitch']) : '0'; ?>
            <div class="slider_div pitch">
                <input type="text" name="shortcodelic_maps_<?php echo $mapsSize; ?>[pitch]" value="<?php echo $pitch; ?>">
                <div class="slider_cursor"></div>
            </div><!-- .slider_div -->

            <div class="clear"></div>

            <h3><?php _e('Style', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('You can create your own style online with free resources like: <a href=\'http://www.evoluted.net/thinktank/web-design/custom-google-maps-style-tool\' target=\'_blank\'>http://www.evoluted.net/thinktank/web-design/custom-google-maps-style-tool</a>','shortcodelic'); ?></p>"></i>:</h3>
            <?php $style = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['style']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['style']) : ''; ?>
            <textarea name="shortcodelic_maps_<?php echo $mapsSize; ?>[style]"><?php echo $style; ?></textarea>

            <div class="clear"></div>

        </div><!-- .shortcodelic_opts_part -->

    </div>

    <div class="alignleft shortcodelic_opts_list">

            <h3><?php _e('Map type controls', 'shortcodelic'); ?>:</h3>
            <?php $maptype = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['maptype']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['maptype']) : 'top_right'; ?>
            <label class="for_select marginhack">
                <span class="for_select">
                    <select name="shortcodelic_maps_<?php echo $mapsSize; ?>[maptype]">
                        <option value="hide" <?php selected( $maptype, 'hide' ); ?>><?php _e('hide', 'shortcodelic'); ?></option>
                        <option value="top_center" <?php selected( $maptype, 'top_center' ); ?>><?php _e('top_center', 'shortcodelic'); ?></option>
                        <option value="top_left" <?php selected( $maptype, 'top_left' ); ?>><?php _e('top_left', 'shortcodelic'); ?></option>
                        <option value="top_right" <?php selected( $maptype, 'top_right' ); ?>><?php _e('top_right', 'shortcodelic'); ?></option>
                        <option value="left_top" <?php selected( $maptype, 'left_top' ); ?>><?php _e('left_top', 'shortcodelic'); ?></option>
                        <option value="right_top" <?php selected( $maptype, 'right_top' ); ?>><?php _e('right_top', 'shortcodelic'); ?></option>
                        <option value="left_center" <?php selected( $maptype, 'left_center' ); ?>><?php _e('left_center', 'shortcodelic'); ?></option>
                        <option value="right_center" <?php selected( $maptype, 'right_center' ); ?>><?php _e('right_center', 'shortcodelic'); ?></option>
                        <option value="left_bottom" <?php selected( $maptype, 'left_bottom' ); ?>><?php _e('left_bottom', 'shortcodelic'); ?></option>
                        <option value="right_bottom" <?php selected( $maptype, 'right_bottom' ); ?>><?php _e('right_bottom', 'shortcodelic'); ?></option>
                        <option value="bottom_center" <?php selected( $maptype, 'bottom_center' ); ?>><?php _e('bottom_center', 'shortcodelic'); ?></option>
                        <option value="bottom_left" <?php selected( $maptype, 'bottom_left' ); ?>><?php _e('bottom_left', 'shortcodelic'); ?></option>
                        <option value="bottom_right" <?php selected( $maptype, 'bottom_right' ); ?>><?php _e('bottom_right', 'shortcodelic'); ?></option>
                    </select>
                </span>
            </label>

            <div class="clear"></div>

            <h3><?php _e('Pan controls', 'shortcodelic'); ?>:</h3>
            <?php $pancontrol = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['pancontrol']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['pancontrol']) : 'top_left'; ?>
            <label class="for_select marginhack">
                <span class="for_select">
                    <select name="shortcodelic_maps_<?php echo $mapsSize; ?>[pancontrol]">
                        <option value="hide" <?php selected( $pancontrol, 'hide' ); ?>><?php _e('hide', 'shortcodelic'); ?></option>
                        <option value="top_center" <?php selected( $pancontrol, 'top_center' ); ?>><?php _e('top_center', 'shortcodelic'); ?></option>
                        <option value="top_left" <?php selected( $pancontrol, 'top_left' ); ?>><?php _e('top_left', 'shortcodelic'); ?></option>
                        <option value="top_right" <?php selected( $pancontrol, 'top_right' ); ?>><?php _e('top_right', 'shortcodelic'); ?></option>
                        <option value="left_top" <?php selected( $pancontrol, 'left_top' ); ?>><?php _e('left_top', 'shortcodelic'); ?></option>
                        <option value="right_top" <?php selected( $pancontrol, 'right_top' ); ?>><?php _e('right_top', 'shortcodelic'); ?></option>
                        <option value="left_center" <?php selected( $pancontrol, 'left_center' ); ?>><?php _e('left_center', 'shortcodelic'); ?></option>
                        <option value="right_center" <?php selected( $pancontrol, 'right_center' ); ?>><?php _e('right_center', 'shortcodelic'); ?></option>
                        <option value="left_bottom" <?php selected( $pancontrol, 'left_bottom' ); ?>><?php _e('left_bottom', 'shortcodelic'); ?></option>
                        <option value="right_bottom" <?php selected( $pancontrol, 'right_bottom' ); ?>><?php _e('right_bottom', 'shortcodelic'); ?></option>
                        <option value="bottom_center" <?php selected( $pancontrol, 'bottom_center' ); ?>><?php _e('bottom_center', 'shortcodelic'); ?></option>
                        <option value="bottom_left" <?php selected( $pancontrol, 'bottom_left' ); ?>><?php _e('bottom_left', 'shortcodelic'); ?></option>
                        <option value="bottom_right" <?php selected( $pancontrol, 'bottom_right' ); ?>><?php _e('bottom_right', 'shortcodelic'); ?></option>
                    </select>
                </span>
            </label>

            <div class="clear"></div>

            <h3><?php _e('Zoom controls', 'shortcodelic'); ?>:</h3>
            <?php $zoomcontrol = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['zoomcontrol']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['zoomcontrol']) : 'top_left'; ?>
            <label class="for_select marginhack">
                <span class="for_select">
                    <select name="shortcodelic_maps_<?php echo $mapsSize; ?>[zoomcontrol]">
                        <option value="hide" <?php selected( $zoomcontrol, 'hide' ); ?>><?php _e('hide', 'shortcodelic'); ?></option>
                        <option value="top_center" <?php selected( $zoomcontrol, 'top_center' ); ?>><?php _e('top_center', 'shortcodelic'); ?></option>
                        <option value="top_left" <?php selected( $zoomcontrol, 'top_left' ); ?>><?php _e('top_left', 'shortcodelic'); ?></option>
                        <option value="top_right" <?php selected( $zoomcontrol, 'top_right' ); ?>><?php _e('top_right', 'shortcodelic'); ?></option>
                        <option value="left_top" <?php selected( $zoomcontrol, 'left_top' ); ?>><?php _e('left_top', 'shortcodelic'); ?></option>
                        <option value="right_top" <?php selected( $zoomcontrol, 'right_top' ); ?>><?php _e('right_top', 'shortcodelic'); ?></option>
                        <option value="left_center" <?php selected( $zoomcontrol, 'left_center' ); ?>><?php _e('left_center', 'shortcodelic'); ?></option>
                        <option value="right_center" <?php selected( $zoomcontrol, 'right_center' ); ?>><?php _e('right_center', 'shortcodelic'); ?></option>
                        <option value="left_bottom" <?php selected( $zoomcontrol, 'left_bottom' ); ?>><?php _e('left_bottom', 'shortcodelic'); ?></option>
                        <option value="right_bottom" <?php selected( $zoomcontrol, 'right_bottom' ); ?>><?php _e('right_bottom', 'shortcodelic'); ?></option>
                        <option value="bottom_center" <?php selected( $zoomcontrol, 'bottom_center' ); ?>><?php _e('bottom_center', 'shortcodelic'); ?></option>
                        <option value="bottom_left" <?php selected( $zoomcontrol, 'bottom_left' ); ?>><?php _e('bottom_left', 'shortcodelic'); ?></option>
                        <option value="bottom_right" <?php selected( $zoomcontrol, 'bottom_right' ); ?>><?php _e('bottom_right', 'shortcodelic'); ?></option>
                    </select>
                </span>
            </label>

            <div class="clear"></div>

            <h3><?php _e('Scale controls', 'shortcodelic'); ?>:</h3>
            <?php $scalecontrol = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['scalecontrol']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['scalecontrol']) : ''; ?>
            <label class="for_select marginhack">
                <span class="for_select">
                    <select name="shortcodelic_maps_<?php echo $mapsSize; ?>[scalecontrol]">
                        <option value="" <?php selected( $scalecontrol, '' ); ?>><?php _e('show', 'shortcodelic'); ?></option>
                        <option value="hide" <?php selected( $scalecontrol, 'hide' ); ?>><?php _e('hide', 'shortcodelic'); ?></option>
                    </select>
                </span>
            </label>

            <div class="clear"></div>

            <h3><?php _e('Streetview control', 'shortcodelic'); ?>:</h3>
            <?php $swcontrol = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['swcontrol']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['swcontrol']) : 'top_left'; ?>
            <label class="for_select marginhack">
                <span class="for_select">
                    <select name="shortcodelic_maps_<?php echo $mapsSize; ?>[swcontrol]">
                        <option value="hide" <?php selected( $swcontrol, 'hide' ); ?>><?php _e('hide', 'shortcodelic'); ?></option>
                        <option value="top_center" <?php selected( $swcontrol, 'top_center' ); ?>><?php _e('top_center', 'shortcodelic'); ?></option>
                        <option value="top_left" <?php selected( $swcontrol, 'top_left' ); ?>><?php _e('top_left', 'shortcodelic'); ?></option>
                        <option value="top_right" <?php selected( $swcontrol, 'top_right' ); ?>><?php _e('top_right', 'shortcodelic'); ?></option>
                        <option value="left_top" <?php selected( $swcontrol, 'left_top' ); ?>><?php _e('left_top', 'shortcodelic'); ?></option>
                        <option value="right_top" <?php selected( $swcontrol, 'right_top' ); ?>><?php _e('right_top', 'shortcodelic'); ?></option>
                        <option value="left_center" <?php selected( $swcontrol, 'left_center' ); ?>><?php _e('left_center', 'shortcodelic'); ?></option>
                        <option value="right_center" <?php selected( $swcontrol, 'right_center' ); ?>><?php _e('right_center', 'shortcodelic'); ?></option>
                        <option value="left_bottom" <?php selected( $swcontrol, 'left_bottom' ); ?>><?php _e('left_bottom', 'shortcodelic'); ?></option>
                        <option value="right_bottom" <?php selected( $swcontrol, 'right_bottom' ); ?>><?php _e('right_bottom', 'shortcodelic'); ?></option>
                        <option value="bottom_center" <?php selected( $swcontrol, 'bottom_center' ); ?>><?php _e('bottom_center', 'shortcodelic'); ?></option>
                        <option value="bottom_left" <?php selected( $swcontrol, 'bottom_left' ); ?>><?php _e('bottom_left', 'shortcodelic'); ?></option>
                        <option value="bottom_right" <?php selected( $swcontrol, 'bottom_right' ); ?>><?php _e('bottom_right', 'shortcodelic'); ?></option>
                    </select>
                </span>
            </label>

            <div class="clear"></div><hr>

            <h3><?php _e('Route starting point: latitude and longitude (for instance: 41.890322,12.492312)', 'shortcodelic'); ?>:</h3>
            <?php $start = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['start']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['start']) : ''; ?>
            <input type="text" name="shortcodelic_maps_<?php echo $mapsSize; ?>[start]" value="<?php echo $start; ?>">

            <div class="clear"></div>

            <h3><?php _e('Route destination point: latitude and longitude (for instance: 41.890322,12.492312)', 'shortcodelic'); ?>:</h3>
            <?php $end = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['end']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['end']) : ''; ?>
            <input type="text" name="shortcodelic_maps_<?php echo $mapsSize; ?>[end]" value="<?php echo $end; ?>">

            <div class="clear"></div>

            <h3><?php _e('Travel mode', 'shortcodelic'); ?>:</h3>
            <?php $travelmode = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['travelmode']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['travelmode']) : ''; ?>
            <label class="for_select marginhack">
                <span class="for_select">
                    <select name="shortcodelic_maps_<?php echo $mapsSize; ?>[travelmode]">
                        <option value="driving" <?php selected( $travelmode, 'driving' ); ?>><?php _e('driving', 'shortcodelic'); ?></option>
                        <option value="bicycling" <?php selected( $travelmode, 'bicycling' ); ?>><?php _e('bicycling', 'shortcodelic'); ?></option>
                        <option value="walking" <?php selected( $travelmode, 'walking' ); ?>><?php _e('walking', 'shortcodelic'); ?></option>
                    </select>
                </span>
            </label>

            <h3><?php _e('Color of the path', 'shortcodelic'); ?>:</h3>
            <?php $color = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['color']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['color']) : '#d42027'; ?>
            <div class="pix_color_picker">
                <input type="text" name="shortcodelic_maps_<?php echo $mapsSize; ?>[color]" value="<?php echo $color; ?>">
                <a class="pix_button" href="#"></a>
                <div class="colorpicker"></div>
                <i class="scicon-awesome-cancel"></i>
            </div>

            <h3><?php _e('Opacity of the path stroke', 'shortcodelic'); ?>:</h3>
            <?php $opacity = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['opacity']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['opacity']) : '0.75'; ?>
            <div class="slider_div opacity">
                <input type="text" name="shortcodelic_maps_<?php echo $mapsSize; ?>[opacity]" value="<?php echo $opacity; ?>">
                <div class="slider_cursor"></div>
            </div><!-- .slider_div -->

            <h3><?php _e('Width of the path stroke', 'shortcodelic'); ?>:</h3>
            <?php $strokeweight = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['strokeweight']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['strokeweight']) : '5'; ?>
            <div class="slider_div stroke">
                <input type="text" name="shortcodelic_maps_<?php echo $mapsSize; ?>[strokeweight]" value="<?php echo $strokeweight; ?>">
                <div class="slider_cursor"></div>
            </div><!-- .slider_div -->

            <div class="clear"></div>

    </div>

    <div class="alignleft shortcodelic_opts_list_2">

        <div class="shortcodelic_el_list">
            <h3><?php _e('Add markers to your map', 'shortcodelic'); ?>:</h3>
            <a href="#" class="add-element pix_button"><?php _e('Add a new marker', 'shortcodelic'); ?></a>
            <br>
            <br>
            <div class="el-list alignleft">
                <div class="clone element-sort">
                    <textarea readonly="readonly" data-name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][key][caption]"><p><?php _e('content', 'shortcodelic'); ?></p></textarea>
                    <span class="tab-el-ui-disable-text"></span>
                    <span class="tab-el-ui no-el"></span>
                    <span class="tab-el-ui edit-el" data-panel="shortcodelic_editmarker_panel"><i class="scicon-awesome-pencil"></i></span>
                    <span class="tab-el-ui clone-el"><i class="scicon-awesome-docs"></i></span>
                    <span class="tab-el-ui delete-el"><i class="scicon-awesome-trash"></i></span>
                    <input type="hidden" data-name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][key][bg]" value="">
                    <input type="hidden" data-name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][key][id]" value="">
                    <input type="hidden" data-name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][key][size]" value="">
                    <input type="hidden" data-name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][key][hdpi]" value="">
                    <input type="hidden" data-name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][key][coords]" value="">
                    <input type="hidden" data-name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][key][fx]" value="">
                    <input type="hidden" data-name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][key][delay]" value="">
                </div><!-- .clone.element-sort -->
                <?php if (isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers']) && is_array($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'])) { $i = 1; foreach ($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'] as $key => &$markers) { ?>

                    <?php 
                        $marker_content = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['caption']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['caption']) : '<p>'.__('content', 'shortcodelic').'</p>';
                        $marker_bg = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['bg']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['bg']) : '';
                        $marker_id = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['id']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['id']) : '';
                        $marker_size = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['size']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['size']) : '';
                        $marker_hdpi = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['hdpi']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['hdpi']) : '';
                        $marker_coords = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['coords']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['coords']) : '';
                        $marker_fx = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['fx']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['fx']) : '';
                        $marker_delay = isset($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['delay']) ? esc_attr($shortcodelic_maps['shortcodelic_maps_'.$mapsSize]['markers'][$key]['delay']) : '0';
                    ?>
                    <div class="element-sort">
                        <textarea readonly="readonly" name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][<?php echo $key; ?>][caption]"><?php echo $marker_content; ?></textarea>
                        <span class="tab-el-ui-disable-text"></span>
                        <span class="tab-el-ui no-el"><?php echo $key; ?></span>
                        <span class="tab-el-ui edit-el" data-panel="shortcodelic_editmarker_panel"><i class="scicon-awesome-pencil"></i></span>
                        <span class="tab-el-ui clone-el"><i class="scicon-awesome-docs"></i></span>
                        <span class="tab-el-ui delete-el"><i class="scicon-awesome-trash"></i></span>
                        <input type="hidden" name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][<?php echo $key; ?>][bg]" value="<?php echo $marker_bg; ?>">
                        <input type="hidden" name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][<?php echo $key; ?>][id]" value="<?php echo $marker_id; ?>">
                        <input type="hidden" name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][<?php echo $key; ?>][size]" value="<?php echo $marker_size; ?>">
                        <input type="hidden" name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][<?php echo $key; ?>][hdpi]" value="<?php echo $marker_hdpi; ?>">
                        <input type="hidden" name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][<?php echo $key; ?>][coords]" value="<?php echo $marker_coords; ?>">
                        <input type="hidden" name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][<?php echo $key; ?>][fx]" value="<?php echo $marker_fx; ?>">
                        <input type="hidden" name="shortcodelic_maps_<?php echo $mapsSize; ?>[markers][<?php echo $key; ?>][delay]" value="<?php echo $marker_delay; ?>">
                    </div><!-- .clone.element-tab.element-sort -->
                <?php $i++; } } ?>
            </div><!-- .el-list -->
        </div><!-- .shortcodelic_el_list -->

        <div class="clear"></div>

    </div>

</div><!-- #shortcodelic_maps_generator -->

<?php /*********** TAB EDIT PANEL ***********/ ?>

<div id="shortcodelic_editmarker_panel" class="shortcodelic_meta shortcodelic_editelement_panel" data-title="<?php _e('Shortcode generator &rarr; Maps &rarr; Marker', 'shortcodelic'); ?>">

    <div class="alignleft shortcodelic_options_area">

        <h3><?php _e('Upload your own marker', 'shortcodelic'); ?>:</h3>
        <div class="pix_upload upload_image">
            <input type="text" name="el_bg" value="">
            <input type="hidden" name="el_id" value="">
            <input type="hidden" name="el_size" value="">
            <span class="img_preview slide_bg"></span>
            <a class="pix_button" href="#"><?php _e('Insert','geode'); ?></a>
        </div>

        <div class="clear"></div>

        <h3><label class="alignleft" for="el_hdpi"><?php _e('Is HDPI ready?', 'shortcodelic'); ?> <i class="shortcodelic-wp-pointer scicon-modern-question" data-content="<h3><?php _e('More info','shortcodelic'); ?></h3><p><?php _e('By ticking this box the image you use will be resized at 50% to be displayed sharped also on Retina devices','shortcodelic'); ?></p>"></i>:
            <input type="checkbox" id="el_hdpi" name="el_hdpi" value="true">
            <span></span>
        </label></h3>
        
        <div class="clear"></div>

        <h3><?php _e('Latitude and longitude if different from the default ones', 'shortcodelic'); ?>:</h3>
        <input type="text" name="el_coords" value="">

        <div class="clear"></div>

        <h3><label class="alignleft" for="el_fx"><?php _e('Drop effect on loading', 'shortcodelic'); ?>:
            <input type="checkbox" id="el_fx" name="el_fx" value="true">
            <span></span>
        </label></h3>
        
        <div class="clear"></div>

        <h3><?php _e('Drop effect after (in milliseconds)', 'shortcodelic'); ?>:</h3>
        <div class="slider_div milliseconds">
            <input type="text" name="el_delay" value="0">
            <div class="slider_cursor"></div>
        </div><!-- .slider_div -->

        <div class="clear"></div>

    </div><!-- .shortcodelic_options_area -->

    <div class="alignleft shortcodelic_options_area_2 shortcodelic_tinymce_area">
    </div><!-- .shortcodelic_tinymce_area -->

</div>

