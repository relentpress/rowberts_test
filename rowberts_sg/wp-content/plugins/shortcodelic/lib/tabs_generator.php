<?php
if ( isset($_POST['postid']) ) {
    $scpostid = $_POST['postid'];
    require_once('../../../../wp-load.php');
} else {
    $scpostid = $post->ID;
}
?>
<div id="shortcodelic_tabs_generator" class="shortcodelic_meta" data-title="<?php _e('Shortcode generator &rarr; Tabs &amp; Accordions', 'shortcodelic'); ?>">
<?php
$values = get_post_custom( $scpostid );
$shortcodelic_tabs = isset( $values['shortcodelic_tabs'] ) ? $values['shortcodelic_tabs'][0] : '';
$shortcodelic_tabs = maybe_unserialize($shortcodelic_tabs);
$ajax_nonce = wp_create_nonce('shortcodelic_tabs_nonce');
$tabsSize = '1';
$tabsClone = '1';
$notEmpty = '0';
?>
    <div class="alignleft shortcodelic_options_area">
        <script type="text/javascript">
            var shortcodelic_tabs_nonce = '<?php echo $ajax_nonce; ?>';
        </script>
        <h3><?php _e('Select among the tabs available on this post', 'shortcodelic'); ?>:</h3>
        <label class="for_select marginhack">
            <span class="for_select">
                <select id="shortcodelic_tabs_manager_loader" class="shortcodelic_shortcode_manager_loader">
                    <option value='' data-type="tabs"><?php _e('Create a new set of tabs', 'shortcodelic'); ?></option>
                    <?php if (is_array($shortcodelic_tabs)) {
                        $tabsSize = sizeof($shortcodelic_tabs)+1;
                        $tabsClone = sizeof($shortcodelic_tabs)+1;
                        for ($key = 1; $key <= sizeof($shortcodelic_tabs); $key++) {
                            if ( !empty($shortcodelic_tabs['shortcodelic_tabs_'.$key]) ) {
                            $notEmpty = $notEmpty + 1; ?>
                        <option value='[shortcodelic-tabs postid="<?php echo $scpostid; ?>" tabs="<?php echo $key ?>"]' data-type="tabs" data-link='&amp;shortcodelic_tabs_length=<?php echo $key ?>'>[shortcodelic-tabs postid=&quot;<?php echo $scpostid; ?>&quot; tabs=&quot;<?php echo $key ?>&quot;]</option>
                            <?php } else {
                                $tabsSize = $tabsSize < $key ? $tabsSize : $key;
                                $tabsClone = $tabsClone < $key ? $tabsClone : $key;
                            }
                        }
                    } ?>

                    <?php if ( $notEmpty == 0 ) {
                        delete_post_meta($scpostid, 'shortcodelic_tabs');
                    } ?>
                    </select>
            </span>
        </label>

        <div class="clear"></div>

	        <?php
	            if ( isset($_POST['shortcodelic_tabs_length']) && $_POST['shortcodelic_tabs_length']!='' ) {
	                $tabsSize = $_POST['shortcodelic_tabs_length'];
	            }
	        ?>

        <div class="shortcodelic_opts_part">

	        <h3><?php _e('Shortcode <small>(if you want to use it on a different page/post)</small>', 'shortcodelic'); ?>:</h3>
	        <input type="text" disabled="disabled" id="shortcodelic_get_sc" value="[shortcodelic-tabs postid=&quot;<?php echo $scpostid; ?>&quot; tabs=&quot;<?php echo $tabsSize; ?>&quot;]">

	        <div class="clear"></div>

            <?php if ( isset($_POST['shortcodelic_tabs_length']) && $_POST['shortcodelic_tabs_length']!='' ) { ?>
                <div class="alignleft">
                    <h3><?php _e('Clone these tabs', 'shortcodelic'); ?>:</h3>
                    <a href="#" class="clone-set pix_button" data-type="tabs" data-oldtabs="shortcodelic_tabs_<?php echo $tabsSize; ?>" data-tabs="shortcodelic_tabs_<?php echo $tabsClone; ?>" data-link="&amp;shortcodelic_tabs_length=<?php echo $tabsClone; ?>"><?php _e('Clone these tabs', 'shortcodelic'); ?></a>
                </div>
                <div class="alignright">
                    <h3><?php _e('Delete these tabs', 'shortcodelic'); ?>:</h3>
                    <a href="#" class="delete-set pix_button" data-type="tabs" data-tabs="shortcodelic_tabs_<?php echo $tabsSize; ?>"><?php _e('Delete these tabs', 'shortcodelic'); ?></a>
                </div>
	            <div class="clear"></div><hr>
            <?php } ?>

	        <h3><?php _e('Display as...', 'shortcodelic'); ?>:</h3>
	        <?php $tabsvsacc = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tabsvsacc']) ? esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tabsvsacc']) : 'tabs'; ?>
	        <label class="for_select marginhack">
	            <span class="for_select">
	                <select name="shortcodelic_tabs_<?php echo $tabsSize; ?>[tabsvsacc]">
	                    <option value="tabs" <?php selected( $tabsvsacc, 'tabs' ); ?>><?php _e('tabs', 'shortcodelic'); ?></option>
	                    <option value="tabsleft" <?php selected( $tabsvsacc, 'tabsleft' ); ?>><?php _e('left columned tabs', 'shortcodelic'); ?></option>
	                    <option value="tabsright" <?php selected( $tabsvsacc, 'tabsright' ); ?>><?php _e('right columned tabs', 'shortcodelic'); ?></option>
	                    <option value="accordion" <?php selected( $tabsvsacc, 'accordion' ); ?>><?php _e('accordion', 'shortcodelic'); ?></option>
	                </select>
	            </span>
	        </label>

	        <h3><?php _e('Display as accordion at width (in pixels)', 'shortcodelic'); ?>:</h3>
	        <?php $minwidth = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['minwidth']) ? esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['minwidth']) : '0'; ?>
	        <div class="slider_div">
	            <input type="text" name="shortcodelic_tabs_<?php echo $tabsSize; ?>[minwidth]" value="<?php echo $minwidth; ?>">
	            <div class="slider_cursor"></div>
	        </div><!-- .slider_div -->

	        <div class="clear"></div>

	        <h3><?php _e('Easing', 'shortcodelic'); ?>:</h3>
	        <?php $easing = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['easing']) ? esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['easing']) : 'easeOutSine'; ?>
	        <label class="for_select marginhack">
	            <span class="for_select">
	                <select name="shortcodelic_tabs_<?php echo $tabsSize; ?>[easing]">
	                    <option value="linear" <?php selected( $easing, 'linear' ); ?>><?php _e('linear', 'shortcodelic'); ?></option>
	                    <option value="easeOutCubic" <?php selected( $easing, 'easeOutCubic' ); ?>><?php _e('easeOutCubic', 'shortcodelic'); ?></option>
	                    <option value="easeInOutCubic" <?php selected( $easing, 'easeInOutCubic' ); ?>><?php _e('easeInOutCubic', 'shortcodelic'); ?></option>
	                    <option value="easeInCirc" <?php selected( $easing, 'easeInCirc' ); ?>><?php _e('easeInCirc', 'shortcodelic'); ?></option>
	                    <option value="easeOutCirc" <?php selected( $easing, 'easeOutCirc' ); ?>><?php _e('easeOutCirc', 'shortcodelic'); ?></option>
	                    <option value="easeInOutCirc" <?php selected( $easing, 'easeInOutCirc' ); ?>><?php _e('easeInOutCirc', 'shortcodelic'); ?></option>
	                    <option value="easeInExpo" <?php selected( $easing, 'easeInExpo' ); ?>><?php _e('easeInExpo', 'shortcodelic'); ?></option>
	                    <option value="easeOutExpo" <?php selected( $easing, 'easeOutExpo' ); ?>><?php _e('easeOutExpo', 'shortcodelic'); ?></option>
	                    <option value="easeInOutExpo" <?php selected( $easing, 'easeInOutExpo' ); ?>><?php _e('easeInOutExpo', 'shortcodelic'); ?></option>
	                    <option value="easeInQuad" <?php selected( $easing, 'easeInQuad' ); ?>><?php _e('easeInQuad', 'shortcodelic'); ?></option>
	                    <option value="easeOutQuad" <?php selected( $easing, 'easeOutQuad' ); ?>><?php _e('easeOutQuad', 'shortcodelic'); ?></option>
	                    <option value="easeInOutQuad" <?php selected( $easing, 'easeInOutQuad' ); ?>><?php _e('easeInOutQuad', 'shortcodelic'); ?></option>
	                    <option value="easeInQuart" <?php selected( $easing, 'easeInQuart' ); ?>><?php _e('easeInQuart', 'shortcodelic'); ?></option>
	                    <option value="easeOutQuart" <?php selected( $easing, 'easeOutQuart' ); ?>><?php _e('easeOutQuart', 'shortcodelic'); ?></option>
	                    <option value="easeInOutQuart" <?php selected( $easing, 'easeInOutQuart' ); ?>><?php _e('easeInOutQuart', 'shortcodelic'); ?></option>
	                    <option value="easeInQuint" <?php selected( $easing, 'easeInQuint' ); ?>><?php _e('easeInQuint', 'shortcodelic'); ?></option>
	                    <option value="easeOutQuint" <?php selected( $easing, 'easeOutQuint' ); ?>><?php _e('easeOutQuint', 'shortcodelic'); ?></option>
	                    <option value="easeInOutQuint" <?php selected( $easing, 'easeInOutQuint' ); ?>><?php _e('easeInOutQuint', 'shortcodelic'); ?></option>
	                    <option value="easeInSine" <?php selected( $easing, 'easeInSine' ); ?>><?php _e('easeInSine', 'shortcodelic'); ?></option>
	                    <option value="easeOutSine" <?php selected( $easing, 'easeOutSine' ); ?>><?php _e('easeOutSine', 'shortcodelic'); ?></option>
	                    <option value="easeInOutSine" <?php selected( $easing, 'easeInOutSine' ); ?>><?php _e('easeInOutSine', 'shortcodelic'); ?></option>
	                    <option value="easeInBack" <?php selected( $easing, 'easeInBack' ); ?>><?php _e('easeInBack', 'shortcodelic'); ?></option>
	                    <option value="easeOutBack" <?php selected( $easing, 'easeOutBack' ); ?>><?php _e('easeOutBack', 'shortcodelic'); ?></option>
	                    <option value="easeInOutBack" <?php selected( $easing, 'easeInOutBack' ); ?>><?php _e('easeInOutBack', 'shortcodelic'); ?></option>
	                </select>
	            </span>
	        </label>

	        <div class="clear"></div>

	        <h3><?php _e('Transition speed (in milliseconds)', 'shortcodelic'); ?>:</h3>
	        <?php $speed = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['speed']) ? esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['speed']) : '250'; ?>
	        <div class="slider_div milliseconds">
	            <input type="text" name="shortcodelic_tabs_<?php echo $tabsSize; ?>[speed]" value="<?php echo $speed; ?>">
	            <div class="slider_cursor"></div>
	        </div><!-- .slider_div -->

	        <div class="clear"></div>

	        <h3><?php _e('Effect on changing', 'shortcodelic'); ?>:</h3>
	        <?php $fx = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['fx']) && is_array($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['fx']) ? $shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['fx'] : array(0 => ''); ?>
	        <select name="shortcodelic_tabs_<?php echo $tabsSize; ?>[fx][]" multiple="multiple">
	            <option value='' <?php selected( true, in_array('', $fx) ); ?>><?php _e('none', 'shortcodelic'); ?></option>
	            <option value='fade' <?php selected( true, in_array('fade', $fx) ); ?>><?php _e('fade', 'shortcodelic'); ?></option>
	            <option value='upwards' <?php selected( true, in_array('upwards', $fx) ); ?>><?php _e('up and back', 'shortcodelic'); ?></option>
	            <option value='downwards' <?php selected( true, in_array('downwards', $fx) ); ?>><?php _e('down and back', 'shortcodelic'); ?></option>
	            <option value='rightwards' <?php selected( true, in_array('rightwards', $fx) ); ?>><?php _e('right and back', 'shortcodelic'); ?></option>
	            <option value='leftwards' <?php selected( true, in_array('leftwards', $fx) ); ?>><?php _e('left and back', 'shortcodelic'); ?></option>
	            <option value='zoomout' <?php selected( true, in_array('zoomout', $fx) ); ?>><?php _e('zoomout and back', 'shortcodelic'); ?></option>
	            <option value='zoomin' <?php selected( true, in_array('zoomin', $fx) ); ?>><?php _e('zoomin and back', 'shortcodelic'); ?></option>
	        </select>
	        
	        <div class="clear"></div>

        </div><!-- .shortcodelic_opts_part -->

    </div>

    <div class="alignleft shortcodelic_opts_list_2">

        <div class="shortcodelic_el_list">
            <?php 
            	$tab_active = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['active']) ? esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['active']) : '1';
            ?>
            <input type="hidden" name="shortcodelic_tabs_<?php echo $tabsSize; ?>[active]" value="<?php echo $tab_active; ?>">
            <h3><?php _e('Edit your tabs', 'shortcodelic'); ?>:</h3>
	        <a href="#" class="add-element pix_button"><?php _e('Add a new tab', 'shortcodelic'); ?></a>
            <br>
            <br>
            <div class="el-list alignleft">
	            <div class="clone element-tab element-sort">
	                <input type="text" readonly="readonly" data-name="shortcodelic_tabs_<?php echo $tabsSize; ?>[tab][key][title]" value="<?php _e('Title', 'shortcodelic'); ?>">
	                <input type="hidden" data-name="shortcodelic_tabs_<?php echo $tabsSize; ?>[tab][key][icon]" value="">
	                <input type="hidden" data-name="shortcodelic_tabs_<?php echo $tabsSize; ?>[tab][key][icon_2]" value="">
	                <textarea readonly="readonly" data-name="shortcodelic_tabs_<?php echo $tabsSize; ?>[tab][key][caption]"><p><?php _e('content', 'shortcodelic'); ?></p></textarea>
	                <span class="tab-el-ui-disable-text"></span>
	                <span class="tab-el-ui no-el"></span>
	                <span class="tab-el-ui edit-el" data-panel="shortcodelic_edittab_panel"><i class="scicon-awesome-pencil"></i></span>
	                <span class="tab-el-ui clone-el"><i class="scicon-awesome-docs"></i></span>
	                <span class="tab-el-ui delete-el"><i class="scicon-awesome-trash"></i></span>
	                <span class="tab-el-ui tick-el"><i class="scicon-awesome-check"></i></span>
	                <span class="tab-el-ui untick-el"><i class="scicon-awesome-check-empty"></i></span>
	            </div><!-- .clone.element-tab.element-sort -->
                <?php if (isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab']) && is_array($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'])) { $i = 1; foreach ($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'] as $key => &$tabs) { ?>

                    <?php 
                    	$tab_content = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['caption']) ? esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['caption']) : '<p>'.__('content', 'shortcodelic').'</p>';
                    	$tab_icon = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['icon']) ? esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['icon']) : '';
                    	$tab_icon_2 = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['icon_2']) ? esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['icon_2']) : '';
                    	$tab_title = isset($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['title']) ? esc_attr($shortcodelic_tabs['shortcodelic_tabs_'.$tabsSize]['tab'][$key]['title']) : __('Title', 'shortcodelic');
                    	$show = '';
                    	$hide = '';
                    	if ( $key==$tab_active ) {
                    		$show = ' style="display:block"';
                    		$hide = ' style="display:none"';
                    	}
                    ?>
		            <div class="element-tab element-sort">
		                <input type="text" readonly="readonly" name="shortcodelic_tabs_<?php echo $tabsSize; ?>[tab][<?php echo $key; ?>][title]" value="<?php echo $tab_title; ?>">
		                <input type="hidden" name="shortcodelic_tabs_<?php echo $tabsSize; ?>[tab][<?php echo $key; ?>][icon]" value="<?php echo $tab_icon; ?>">
		                <input type="hidden" name="shortcodelic_tabs_<?php echo $tabsSize; ?>[tab][<?php echo $key; ?>][icon_2]" value="<?php echo $tab_icon_2; ?>">
		                <textarea readonly="readonly" name="shortcodelic_tabs_<?php echo $tabsSize; ?>[tab][<?php echo $key; ?>][caption]"><?php echo $tab_content; ?></textarea>
		                <span class="tab-el-ui-disable-text"></span>
		                <span class="tab-el-ui no-el"><?php echo $key; ?></span>
		                <span class="tab-el-ui edit-el" data-panel="shortcodelic_edittab_panel"><i class="scicon-awesome-pencil"></i></span>
		                <span class="tab-el-ui clone-el"><i class="scicon-awesome-docs"></i></span>
		                <span class="tab-el-ui delete-el"><i class="scicon-awesome-trash"></i></span>
		                <span class="tab-el-ui tick-el"<?php echo $show; ?>><i class="scicon-awesome-check"></i></span>
		                <span class="tab-el-ui untick-el"<?php echo $hide; ?>><i class="scicon-awesome-check-empty"></i></span>
		            </div><!-- .clone.element-tab.element-sort -->
                <?php $i++; } } ?>
            </div><!-- .el-list -->
        </div><!-- .shortcodelic_el_list -->

        <div class="clear"></div>

    </div>
</div><!-- #shortcodelic_tabs_generator -->

<?php /*********** TAB EDIT PANEL ***********/ ?>

<div id="shortcodelic_edittab_panel" class="shortcodelic_meta shortcodelic_editelement_panel" data-title="<?php _e('Shortcode generator &rarr; Tabs &amp; Accordions &rarr; Tab', 'shortcodelic'); ?>">

    <div class="alignleft shortcodelic_options_area">

        <h3><?php _e('Title', 'shortcodelic'); ?>:</h3>
        <input type="text" name="el_title" value="">

        <div class="clear"></div>

        <h3><?php printf( __( 'Icon on tab open %1$s(%2$sadd click here%3$s)%4$s'  ), '<small>', '<a href="#" class="shortcodelic_icon_button">', '</a>', '</small>', 'shortcodelic'); ?>:</h3>
        <input type="text" name="el_icon" value="">

        <div class="clear"></div>

        <h3><?php printf( __( 'Icon on tab closed %1$s(%2$sadd click here%3$s)%4$s'  ), '<small>', '<a href="#" class="shortcodelic_icon_button_2">', '</a>', '</small>', 'shortcodelic'); ?>:</h3>
        <input type="text" name="el_icon_2" value="">

        <div class="clear"></div>

    </div><!-- .shortcodelic_options_area -->

    <div class="alignleft shortcodelic_options_area_2 shortcodelic_tinymce_area">
    </div><!-- .shortcodelic_tinymce_area -->

</div>

