jQuery(document).ready(function(){
	jQuery('.pix_carousels, .pix_postcarousels, .pix_woocarousels ul.products').each(function(){
		var t = jQuery(this),
			parent = t.parents('[data-opts]').eq(0),
			opts = typeof t.data('opts')!='undefined' ? t.data('opts') : parent.data('opts'),
			pager = opts.bullets == 'true' ? true : false,
			controls = opts.prev_next == 'true' ? true : false,
			autocontrols = opts.play_pause == 'true' ? true : false,
			autostart = opts.autoplay == 'true' ? true : false,
			hover = opts.hover == 'true' ? true : false;
		t.imagesLoaded(function(){
			t.bxSlider({
				slideWidth: '400000',
				useCSS: false,
				minSlides: opts.minslides,
				maxSlides: opts.maxslides,
				slideMargin: parseFloat(opts.slidemargin),
				mode: opts.mode,
				speed: parseFloat(opts.speed),
				infiniteLoop: true,
				hideControlOnEnd: true,
				adaptiveHeight: false,
				adaptiveHeightSpeed: parseFloat(opts.speed)/2,
				pager: pager,
				controls: controls,
				auto: true,
				autoControls: autocontrols,
				pause: parseFloat(opts.timeout),
				autoStart: autostart,
				autoHover: hover
			});
		});
	});
});
