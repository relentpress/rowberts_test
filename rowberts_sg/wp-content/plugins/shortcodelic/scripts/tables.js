jQuery(function() {
	jQuery( ".pix_tables" ).each(function(){
		var t = jQuery(this),
			minwidth = parseFloat(t.data('opts').minwidth);
		jQuery(window).bind('resize', function(){
			var w = t.parent().width();
			if (w <= minwidth) {
				t.addClass('minwidth-reached');
			} else {
				t.removeClass('minwidth-reached');
			}
		}).triggerHandler('resize');
	});
});