if (!jQuery.support.transition) jQuery.fn.transition = jQuery.fn.animate;

// Slideshine slideshow version 0.0.1 - a jQuery slideshow with many effects, transitions, easy to customize, using canvas and mobile ready, based on jQuery 1.9+
// Copyright (c) 2013 by Manuel Masia - www.pixedelic.com
// Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
;(function($){$.fn.slideshine = function(opts, callback) {

	$(this).each(function(opts, callback){

		var target = $(this).addClass('slideshine_wrap').wrapInner('<div class="slideshine_slides" />'),

		defaults = {
			timeout : 6000, //the milliseconds between a slide and the next one
			speed : 800, //the milliseconds that the transition effect takes
			easing : 'easeOutSine',
			prev : '.slideshine_prev',
			next : '.slideshine_next',
			pause : '.slideshine_pause',
			play : '.slideshine_play',
			loader : '.slideshine_loader',
			pie : '.slideshine_pie',
			waiter : '.slideshine_waiter',
			autoplay : true,
			hover : true,
			fullscreen : false,
			fx : 'random',
			mobfx : '',
			gridcols : 6,
			gridrows : 4,
			cols : 8,
			rows : 5,
			fxslide : 'next',
			bullets : true,
			thumbnails : false,
			slideReady : function(){},
			scrolledIntoView : function(){},
			scrolledOutOfView : function() {}
		};
		opts = $.extend({}, defaults, target.data('opts'), opts);

		var optsprev = $(opts.prev, target).addClass('opacity_0'),
			optsnext = $(opts.next, target).addClass('opacity_0'),
			optspause = $(opts.pause, target).addClass('opacity_0'),
			optsplay = $(opts.play, target).addClass('opacity_0'),
			optsloader = $(opts.loader, target),
			optspie = $(opts.pie, target),
			optswaiter = $(opts.waiter, target).addClass('hiddenspinner');

		opts.timeout = parseFloat(opts.timeout);
		opts.speed = parseFloat(opts.speed);

		var wrap = target.find('.slideshine_slides'),
			arrSlides = [],
			arrImages = [],
			arrThumbs = [],
			arrThumbsVideo = [],
			fx = ['0','1'],
			hFixed2,
			set,
			timeSet = 0,
			winW,
			dir = 'forward',
			current = 0,
			rad = 0,
			moving = false,
			hover = false,
			waiterSet,
			pie_bg = $('.slideshine_pie_bg', target),
			piestroke = parseFloat(optswaiter.find('.spinner').css('borderTopWidth')),
			piew = parseFloat(pie_bg.width()),
			arrSort = [];

		if ( Modernizr.rgba ) {
			wrap.css({background: 'rgba('+opts.skinbgrgb+','+opts.skinopacity+')'});
		} else {
			wrap.css({background: opts.skinbg});
		}

		if ( Modernizr.canvas && optspie.find('canvas').length ) {
			var canvas = optspie.find('canvas').get(0);
		} else if ( !Modernizr.canvas && optspie.find('canvas').length ) {
			optsloader.removeClass('slideshine_hidden');
		}

		if (Modernizr.csstransitions) {
			var matrixToArray = function(matrix) {
				if ( typeof matrix != 'undefined' ) {
					return matrix.substr(7, matrix.length - 8).split(', ');
				} else {
					return '1,1';
				}
			};
		}

		wrap.after(optsloader).after(optspie).after(optswaiter).after(pie_bg);

		if(!optsloader.hasClass('slideshine_hidden')) optsloader.before('<div class="slideshine_wrap_loader" />');

		if ( Modernizr.rgba ) {
			$('.slideshine_wrap_loader', target).css({
				background: 'rgba('+opts.skinbgrgb+','+opts.skinopacity+')'
			});
		} else {
			$('.slideshine_wrap_loader', target).css({
				background: opts.skinbg
			});
		}

		wrap.after('<div class="slideshine_play_pause slideshine_fontsized" />');

		var playpause = $('.slideshine_play_pause', target);

		var fontSize = opts.width*0.04 >= 30 ? opts.width*0.04 : 30;
		fontSize = fontSize <= 50 ? fontSize : 50;

		$('.slideshine_fontsized', target).css({
			fontSize: fontSize
		});

		playpause.append(optsplay).append(optspause);
		wrap.after(optsprev).after(optsnext);

		optsplay.add(optspause).add(optsprev).add(optsnext);

		//populate an array with all the images
		$(' > div', wrap).each(function(){

			var index = $(this).index();
			$(this).addClass('slideshineSlide').addClass('slideshineSlide_'+index);

			$(' > div', wrap).eq(index).find('div[data-opts]').each(function(){
				//firstly populate the array with all the background images
				var dataBg = $(this).data('opts').src;
				arrSlides[index] = dataBg;
				//then populate the array with all the thumbs
				var dataTh = $(this).data('opts').thumb;
				arrThumbs[index] = dataTh;
				//then populate the array with all the video thumbs
				var dataVide = $(this).data('opts').thumb2;
				arrThumbsVideo[index] = dataVide;
			});

		});

		var arrSlidesLenght = arrSlides.length,
			counter = 0,
			bg = 0,
			running = false,
			pause = opts.autoplay == 'true' ? false : true,
			scrolled = false,
			sliding = false;

		target.hover(function(){
			if ( opts.hover == 'true' ) {
				hover = true;
			}
			target.addClass('hover');
		}, function(){
			hover = false;
			target.removeClass('hover');
		});

		$('iframe',target).each(function(){
			var t = $(this);
			var src = t.attr('src');
			t.attr('data-src',src).removeAttr('src');
		});

		var appearSlide = function(counter){
			/*$(window).triggerHandler('load');*/
			clearTimeout(waiterSet);
			optswaiter.transition({opacity:0},250,function(){
				$(this).addClass('hiddenspinner');
			});
			if ( pie_bg.hasClass('notBordered') ) {
				pie_bg.removeClass('notBordered').addClass('bordered');
			}
			if ( !optsplay.length || playpause.css('opacity')==0 ) {
				pie_bg.addClass('opacity_0');
			}
			var slide = target.find('.slideshineSlide_'+counter),
				previous = target.find('.slideshine_current').removeClass('slideshine_current').css({zIndex:0}),
				checkAgain;

			var manageCaptions = function(){
				$('.slideshine_caption', slide).each(function(){
					var t = $(this),
						dataCapt = t.data('caption');

					if ( opts.layout == 'fitting' ) {
						if ( !$('.caption_wrap', slide ).length ) {
							slide.append('<div class="caption_wrap" />');
						}
						$('.caption_wrap', slide).append(t);
					}

					if ( !$('.pixlightfx', t).length ) {
						$('*', t).each(function(){
							var fontSize = $(this).css('font-size'),
								lineHeight = $(this).css('line-height'),
								marginTop = $(this).css('margin-top'),
								marginLeft = $(this).css('margin-left'),
								marginBottom = $(this).css('margin-bottom'),
								marginRight = $(this).css('margin-right'),
								paddingTop = $(this).css('padding-top'),
								paddingLeft = $(this).css('padding-left'),
								paddingBottom = $(this).css('padding-bottom'),
								paddingRight = $(this).css('padding-right');
							$(this).attr('data-style','{}');
							$(this).data('style').fontSize = fontSize;
							$(this).data('style').lineHeight = lineHeight;
							$(this).data('style').marginTop = marginTop;
							$(this).data('style').marginLeft = marginLeft;
							$(this).data('style').marginBottom = marginBottom;
							$(this).data('style').marginRight = marginRight;
							$(this).data('style').paddingTop = paddingTop;
							$(this).data('style').paddingLeft = paddingLeft;
							$(this).data('style').paddingBottom = paddingBottom;
							$(this).data('style').paddingRight = paddingRight;
						});

						$('*', t).filter(function (index) {
							return $(this).css("display") === "block" && !$(this).is('img') && !$(this).is('iframe') && !$(this).hasClass('mejs-container') && !$(this).parents('.mejs-container').length;
						}).each(function(){
							$(this).wrapInner('<span class="pixlightfx" />');
						}).filter(function (index) {
							return !$('.notpixlight',this).length;
						}).find('.pixlightfx').wrapInner('<span class="pixlight" />');

						if ( Modernizr.rgba ) {
							$('.pixlight', t).css({
								backgroundColor: dataCapt.rgb
							});
						} else {
							$('.pixlight', t).css({
								backgroundColor: dataCapt.bg
							});
						}

						if ( dataCapt.entering !== '' ) {
							$('.pixlightfx', t).css({ opacity : 0 });
						}
					}

				});

				var localResize = function(){
					var scale = target.width() / parseFloat(opts.width);

					if ( $('.caption_wrap', slide ).length ) {
						var w = target.outerWidth(),
							h = target.outerHeight(),
							t = $(this),
							wrapSize;
						if ( w/h > opts.width/opts.height ) {
							wrapSize = (h/opts.height)*opts.width;
							scale = h/opts.height;
							$('.caption_wrap', slide ).css({
								height : h,
								width : wrapSize,
								marginLeft : (w-wrapSize)/2,
								marginTop : 0
							});
						} else {
							wrapSize = (opts.height/opts.width)*w;
							scale = w/opts.width;
							$('.caption_wrap', slide ).css({
								height : wrapSize,
								width : w,
								marginTop : (h-wrapSize)/2,
								marginLeft : 0
							});
						}
					}

					$('.slideshine_caption', target).each(function(){
						var t = $(this),
							dataCapt = t.data('caption');
						if ( dataCapt.wider !== '' ) {
							if ( target.width()<dataCapt.wider ) {
								t.show().removeClass('noway');
							} else {
								t.hide().addClass('noway');
							}
						}
						if ( dataCapt.narrower !== '' ) {
							if ( target.width()>dataCapt.narrower ) {
								t.show().removeClass('noway');
							} else {
								t.hide().addClass('noway');
							}
						}
					});
					$('.slideshine_caption *', slide).each(function(){
						if ( typeof $(this).data('style') != 'undefined' ) {
							var fontSize = parseFloat($(this).data('style').fontSize),
								lineHeight = parseFloat($(this).data('style').lineHeight),
								marginTop = parseFloat($(this).data('style').marginTop),
								marginLeft = parseFloat($(this).data('style').marginLeft),
								marginBottom = parseFloat($(this).data('style').marginBottom),
								marginRight = parseFloat($(this).data('style').marginRight),
								paddingTop = parseFloat($(this).data('style').paddingTop),
								paddingLeft = parseFloat($(this).data('style').paddingLeft),
								paddingBottom = parseFloat($(this).data('style').paddingBottom),
								paddingRight = parseFloat($(this).data('style').paddingRight);
							$(this).css({
								fontSize : Math.floor(fontSize*scale)+'px',
								lineHeight : Math.floor(lineHeight*scale)+'px',
								marginTop : Math.floor(marginTop*scale)+'px',
								marginLeft : Math.floor(marginLeft*scale)+'px',
								marginBottom : Math.floor(marginBottom*scale)+'px',
								marginRight : Math.floor(marginRight*scale)+'px',
								paddingTop : Math.floor(paddingTop*scale)+'px',
								paddingLeft : Math.floor(paddingLeft*scale)+'px',
								paddingBottom : Math.floor(paddingBottom*scale)+'px',
								paddingRight : Math.floor(paddingRight*scale)+'px'
							});
						}
					});
				};
				localResize();
				$(window).bind('resize',function(){ localResize(); });
			};

			if (  $('html').hasClass('touch') ) {
				var movCounter;
				target.off('move').off('moveend').on('move', function(e) {
					if (  Math.abs(e.distX)>10 && sliding === false ) {
						if ( e.distX < 0 ) {
							hover = false;
							dir = 'forward';
							movCounter = counter + 1;
						} else if ( e.distX > 0 ) {
							hover = false;
							dir = 'backward';
							movCounter = counter - 1;
						}
					}
				}).on('moveend', function() {
					nextSlide(movCounter);
				});
			}

			$('iframe',previous).each(function(){
				var t = $(this);
				var src = t.attr('src');
				t.attr('data-src',src).removeAttr('src');
			});

			optsSlide = {};
			$.each( slide.find('[data-opts]').data('opts'), function( key, value ) {
				if ( value !== '' ) {
					optsSlide[key] = value;
				}
			});
			optsSlide = $.extend({}, opts, optsSlide);

			if( mobiledetector() && (opts.mobfx !== '' || optsSlide.mobfx !== '') ) {
				optsSlide.mobfx = optsSlide.mobfx !== '' ? optsSlide.mobfx : opts.mobfx;
				optsSlide.fx = optsSlide.mobfx;
			}

			if ( typeof optsSlide.fx != 'undefined' ) {
				optsSlide.fx = optsSlide.fx.split(',');
				optsSlide.fx = shuffle(optsSlide.fx);
				optsSlide.fx = optsSlide.fx[0];
			}

			if ( typeof optsSlide.fxslide != 'undefined' ) {
				optsSlide.fxslide = optsSlide.fxslide.split(',');
				optsSlide.fxslide = shuffle(optsSlide.fxslide);
				optsSlide.fxslide = optsSlide.fxslide[0];
			}

			if ( typeof optsSlide.longfx != 'undefined' ) {
				optsSlide.longfx = optsSlide.longfx.split(',');
				optsSlide.longfx = shuffle(optsSlide.longfx);
				optsSlide.longfx = optsSlide.longfx[0];
			}

			if (Modernizr.csstransitions && previous.hasClass('zooming')) {
				optsSlide.fxslide = 'next';
			}

			/*if ( jQuery('.mejs-container', slide).length ) {
				optsSlide.fx = 'fade';
				optsSlide.fxslide = 'current';
			}
			if ( jQuery('.mejs-container', previous).length ) {
				optsSlide.fx = 'fade';
				optsSlide.fxslide = 'next';
			}*/

			var resizeBg = function(counter){
				if ( typeof counter === 'undefined' || counter === false || counter === null ) {
					counter = 0;
				}
				var w = target.outerWidth(),
					h = target.outerHeight(),
					t = $(this),
					img = $('.slideshineSlide_'+counter, target).find('div[data-opts] img.slideshineBgs'),
					iframe = $('.slideshineSlide_'+counter, target).find('div[data-opts] > iframe'),
					imgW = img.naturalWidth(),
					imgH = img.naturalHeight();

				iframe.width(w).height(h);

				if ( optsSlide.layout != 'fixed2' ) {

					if((imgW/imgH)>(w/h)) {
						var r = h / imgH,
							d, d1, d2;
						d = d1 = d2 = (w - (imgW*r))*0.5;
						if ( typeof optsSlide.bg_pos != 'undefined' ) {
							if ( optsSlide.bg_pos.match( /right/i ) ) {
								d1 = d*2;
								d2 = 0;
							} else if ( optsSlide.bg_pos.match( /left/i ) ) {
								d1 = 0;
								d2 = d*2;
							}
						}
						img.css({
							'height' : h,
							'margin-left' : d1+'px',
							'margin-right' : d2+'px',
							'margin-top' : 0,
							'position' : 'absolute',
							'visibility' : 'visible',
							'width' : imgW*r
						});
					} else {
						var r = w / imgW;
						var d = (h - (imgH*r))*0.5;
						if ( typeof optsSlide.bg_pos != 'undefined' ) {
							if ( optsSlide.bg_pos.match( /top/i ) ) {
								d = 0;
							} else if ( optsSlide.bg_pos.match( /bottom/i ) ) {
								d = d*2;
							}
						}
						img.css({
							'height' : imgH*r,
							'margin-left' : 0,
							'margin-right' : 0,
							'margin-top' : d+'px',
							'position' : 'absolute',
							'visibility' : 'visible',
							'width' : w
						});
					}

				} else {

					img.css({
						'height' : imgH * (w/imgW),
						'margin-top' : 0,
						'position' : 'absolute',
						'visibility' : 'visible',
						'width' : w
					});

					hFixed2 = imgH * (w/imgW);

					if ( running == false ) {
						target.css({ height : hFixed2 });
					}
				}

				optswaiter.removeClass('hiddenspinner');

			};

			var appearCaption = function(attheend) {
				var timeNow = Math.floor(timeSet*10000);
				if ( attheend === false ) {
					$('.slideshine_caption', slide).each(function(){
						var t = $(this),
							dataCapt = t.data('caption'),
							lspan = $('.pixlightfx', t).length,
							winW = target.width(),
							winH = target.height(),
							currInd = slide.index(),
							setFrom, setCap;

						//if ( timeNow == dataCapt.from ) {

							if ( !t.hasClass('noway') ) t.show();

							if ( Modernizr.csstransforms ) {
								$('.pixlightfx', t).css({ scale: 1, rotate: '0deg', x: 0, y: 0, opacity: 0 });
							} else {
								t.animate({ opacity : 0, marginTop: 0, marginLeft: 0 },1);
							}
						var setFromFunc = function(timeFrom){
							clearTimeout(setFrom);
							setFrom = setTimeout( function(){

								var delay = 0;

								if ( dataCapt.entering !== '' ) {
									switch(dataCapt.entering){
										case 'upwards':
											if ( Modernizr.csstransforms ) {
												$('.pixlightfx', t).css({ y : (150) });
											} else {
												t.css({ marginTop : (150) });
											}
											break;
										case 'downwards':
											if ( Modernizr.csstransforms ) {
												$('.pixlightfx', t).css({ y : '-'+(150)+'px' });
											} else {
												t.css({ marginTop : '-'+(150)+'px' });
											}
											break;
										case 'rightwards':
											if ( Modernizr.csstransforms ) {
												$('.pixlightfx', t).css({ x : '-'+(150)+'px' });
											} else {
												t.css({ marginLeft : '-'+(150)+'px' });
											}
											break;
										case 'leftwards':
											if ( Modernizr.csstransforms ) {
												$('.pixlightfx', t).css({ x : (150) });
											} else {
												t.css({ marginLeft : (150) });
											}
											break;
										case 'zoomout':
											if ( Modernizr.csstransforms ) $('.pixlightfx', t).css({ scale: 2 });
											break;
										case 'zoomin':
											if ( Modernizr.csstransforms ) $('.pixlightfx', t).css({ scale: 0.5 });
											break;
										case 'zoomout-rotatein':
											if ( Modernizr.csstransforms ) $('.pixlightfx', t).css({ scale: 1.35, rotate: '30deg' });
											break;
										case 'zoomout-rotateout':
											if ( Modernizr.csstransforms ) $('.pixlightfx', t).css({ scale: 1.35, rotate: '-30deg' });
											break;
									}
									if ( Modernizr.csstransforms ) {
										$('.pixlightfx', t).each(function(){
											var pixlight = $(this),
												setLight;
											clearTimeout(setLight);
											setLight = setTimeout(function(){
												pixlight.stop().transition({ opacity : 1, x: 0, y: 0, scale: 1, rotate: '0deg' }, (parseFloat(dataCapt.fromduration)), dataCapt.fromeasing);
											}, delay);
											delay = delay + (parseFloat(dataCapt.fromduration)/lspan);
										});
									} else {
										t.add('.pixlightfx', t).stop().animate({ opacity : 1, marginTop: 0, marginLeft: 0 }, (parseFloat(dataCapt.fromduration)), dataCapt.fromeasing);
									}
								}
							}, timeFrom);
						};
						var fromD = new Date(), fromDifference, stopFrom = false;
						setFromFunc(parseFloat(dataCapt.from));

						jQuery(window).bind('scroll shortcodelic_ev', function(){
							if ( scrolled === true ) {
								if ( stopFrom === false ) {
									stopFrom = true;
									clearTimeout(setFrom);
									fromDifference = new Date() - fromD;
								}
							} else {
								if ( stopFrom === true && target.is(':visible') ) {
									stopFrom = false;
									var resumeD = parseFloat(dataCapt.from)-fromDifference;
									if ( resumeD > 0 ) {
										fromD = new Date();
										setFromFunc(resumeD);
									}
								}
							}
						}).triggerHandler('shortcodelic_ev');
						//}

						if ( parseFloat(dataCapt.to) < (parseFloat(dataCapt.from)+parseFloat(dataCapt.fromduration)+500)) {
							dataCapt.to = (parseFloat(dataCapt.to)+parseFloat(dataCapt.fromduration)+500);
						}

						//if ( timeNow == dataCapt.to ) {
						var setToFunc = function(timeTo){
							clearTimeout(setCap);
							setCap = setTimeout( function(){

								var trans2, delay = 0;

								if ( dataCapt.leaving !== '' && parseFloat(dataCapt.to) > parseFloat(dataCapt.from) ) {
									switch(dataCapt.leaving){
										case 'upwards':
											trans = Modernizr.csstransforms ? { y : '-'+(150)+'px', opacity: 0 } : { marginTop : '-'+(150)+'px', opacity: 0 };
											break;
										case 'downwards':
											trans = Modernizr.csstransforms ? { y : (150), opacity: 0 } : { marginTop : (150), opacity: 0 };
											break;
										case 'rightwards':
											trans = Modernizr.csstransforms ? { x : (150), opacity: 0 } : { marginLeft : (150), opacity: 0 };
											break;
										case 'leftwards':
											trans = Modernizr.csstransforms ? { x : '-'+(150)+'px', opacity: 0 } : { marginLeft : '-'+(150)+'px', opacity: 0 };
											break;
										case 'zoomout':
											trans = Modernizr.csstransforms ? { scale: 0.5, opacity: 0 } : { opacity: 0 };
											break;
										case 'zoomin':
											trans = Modernizr.csstransforms ? { scale: 2, opacity: 0 } : { opacity: 0 };
											break;
										case 'zoomout-rotatein':
											trans = Modernizr.csstransforms ? { scale: 0.5, rotate: '-30deg', opacity: 0 } : { opacity: 0 };
											break;
										case 'zoomout-rotateout':
											trans = Modernizr.csstransforms ? { scale: 1.35, rotate: '30deg', opacity: 0 } : { opacity: 0 };
											break;
										default:
											trans = { opacity: 0 };
									}
									if ( Modernizr.csstransforms ) {
										$('.pixlightfx', t).each(function(){
											var pixlight = $(this),
												setLight2;
											clearTimeout(setLight2);
											setLight2 = setTimeout(function(){
												pixlight.stop().transition(trans, (parseFloat(dataCapt.toduration)), dataCapt.toeasing, function(){
													pixlight.removeClass('visible');
												});
											}, delay);
											delay = delay + (parseFloat(dataCapt.toduration/lspan));
										});
									} else {
										t.stop().animate(trans, (parseFloat(dataCapt.toduration)), dataCapt.toeasing, function(){
											t.removeClass('visible');
										});
									}
								}
							}, timeTo);
						};
						var toD = new Date(), toDifference, stopTo = false;
						setToFunc(parseFloat(dataCapt.to));
						//}
						jQuery(window).bind('scroll shortcodelic_ev', function(){
							if ( scrolled === true ) {
								if ( stopTo === false ) {
									stopTo = true;
									//setCap.pause();
									clearTimeout(setCap);
									toDifference = new Date() - toD;
								}
							} else {
								if ( stopTo === true ) {
									stopTo = false;
									var resumeD = parseFloat(dataCapt.to)-toDifference;
									if ( resumeD > 0 ) {
										toD = new Date();
										setToFunc(resumeD);
									}
								}
								//setCap.resume();
							}
						}).triggerHandler('shortcodelic_ev');
					});
				} else {
					$('.slideshine_caption', slide).each(function(){
						var t = $(this),
							trans,
							dataCapt = t.data('caption'),
							lspan = $('.pixlightfx', t).length,
							winW = target.width(),
							winH = target.height();

						switch(dataCapt.leaving){
							case 'upwards':
								trans = Modernizr.csstransforms ? { y : '-'+(150)+'px', opacity: 0 } : { marginTop : '-'+(150)+'px', opacity: 0 };
								break;
							case 'downwards':
								trans = Modernizr.csstransforms ? { y : (150), opacity: 0 } : { marginTop : (150), opacity: 0 };
								break;
							case 'rightwards':
								trans = Modernizr.csstransforms ? { x : (150), opacity: 0 } : { marginLeft : (150), opacity: 0 };
								break;
							case 'leftwards':
								trans = Modernizr.csstransforms ? { x : '-'+(150)+'px', opacity: 0 } : { marginLeft : '-'+(150)+'px', opacity: 0 };
								break;
							case 'zoomout':
								trans = Modernizr.csstransforms ? { scale: 0.5, opacity: 0 } : { opacity: 0 };
								break;
							case 'zoomin':
								trans = Modernizr.csstransforms ? { scale: 2, opacity: 0 } : { opacity: 0 };
								break;
							case 'zoomout-rotatein':
								trans = Modernizr.csstransforms ? { scale: 0.5, rotate: '-30deg', opacity: 0 } : { opacity: 0 };
								break;
							case 'zoomout-rotateout':
								trans = Modernizr.csstransforms ? { scale: 1.35, rotate: '30deg', opacity: 0 } : { opacity: 0 };
								break;
							default:
								trans = { opacity: 0 };
						}
						if ( Modernizr.csstransforms ) {
							$('.pixlightfx', t).each(function(){
								var pixlight = $(this);
								pixlight.stop().transition(trans, (parseFloat(dataCapt.toduration)), dataCapt.toeasing, function(){
									pixlight.removeClass('visible');
								});
							});
						} else {
							t.stop().animate(trans, (parseFloat(dataCapt.toduration)), dataCapt.toeasing, function(){
								t.removeClass('visible');
							});
						}
					});
				}
			};

			var nextSlide = function(counter){

				appearCaption(true);

				sliding = true;

				clearInterval(set);
				if ( counter < 0 ) {
					counter = (arrSlidesLenght-1);
				} else if ( counter >= arrSlidesLenght ) {
					counter = 0;
				}
				current = counter;
				optspie.stop(true,false).fadeOut(500,function(){
					if ( pause !== true ) {
						optsplay.addClass('opacity_0');
					}
					if (typeof canvas != 'undefined' && canvas.getContext) {
						timeSet = 0;
						drawProgress(0);
					}
				});
				if ( !optsplay.length || playpause.css('opacity')==0 ) {
					pie_bg.addClass('opacity_0');
				}
				if ($('.slideshine_wrap_loader', target).length) {
					optsloader.fadeOut(500,function(){
						$(this).css({
							width: 0
						}).show();
						preloadBgs(counter, true);
					});
				} else {
					preloadBgs(counter, true);
				}

			};

			var drawProgress = function(percent){
  
				/*optspie.html('<div class="slice'+(percent > 50?'  gt50':'')+'"><div class="pie"></div>'+(percent > 50?'<div class="pie fill"></div>':'')+(percent > 75?'<div class="pie fill75"></div>':'')+'</div>');
				var deg = 360/100*percent;
				$('.slice .pie', optspie).css({
					transform:'rotate('+deg+'deg)',
					borderColor: opts.skin
				});*/

				radNew = percent/100;

				piew = parseFloat(pie_bg.width());

				if ( Modernizr.canvas && typeof canvas != 'undefined' ) {
					canvas.setAttribute("width", piew);
					canvas.setAttribute("height", piew);
					//canvas.setAttribute("style", "background:#ff0000");
			
					if (canvas && canvas.getContext && $('.slideshine_slides', target).length && piew > 0 ) {
						var ctx = canvas.getContext("2d");
						ctx.rotate(Math.PI*(3/2));
						ctx.translate(-piew,0);
						ctx.clearRect(0,0,piew,piew);
						ctx.globalCompositeOperation = 'source-over';
						ctx.beginPath();
						ctx.arc((piew)/2, (piew)/2, (piew)/2-(piestroke/2),0,Math.PI*2*radNew,false);
						ctx.lineWidth = piestroke;
						ctx.strokeStyle = opts.skin;
						ctx.stroke();
						ctx.closePath();
					}
				}
			};

			var runSlide = function(counter) {

				var timeSum = 0.005;
				clearInterval(set);

				optspie.fadeIn(250);
				pie_bg.removeClass('opacity_0');

				if (timeSet===0) appearCaption(false);

				set = setInterval( function(){
					
					if ( !$('.slideshine_slides', target).length ) clearInterval(set);
					winW = target.width();
					if(timeSet<=1.002 && pause === false && scrolled === false && hover === false){
						timeSet = (timeSet+timeSum);
					} else if (timeSet<=1 && (pause !== false || scrolled !== false || hover !== false)){
						timeSet = timeSet;
					} else {
						if( pause === false && scrolled === false && hover === false ) {
							clearInterval(set);
							nextSlide(counter+1);
						}
					}
					if ( pause === false && scrolled === false && hover === false ) {
						$(optsloader).css({width:winW*timeSet});
						if (canvas && canvas.getContext) drawProgress(timeSet*100);
					}
				},optsSlide.timeout*timeSum);
			};

			var appendThumbs = function(){
				wrap.after('<div class="slideshine_thumbs" />');
				var thumbsCont = $('.slideshine_thumbs', target),
					contW = 0,
					contH = 0,
					thumbA;
				$.each( arrThumbs, function( key, value ) {
					if ( value !== '' ) {
						var thumbAVid = arrThumbsVideo[key]!=='' ? '<span class="slideshine_thumb_video" style="background-image:' + arrThumbsVideo[key] + '"></span>' : '';
						if ( key == 0 ) {
							thumbA = $('<span data-index="' + key + '" class="slideshine_current_th" style="background-image:' + value + '">' + thumbAVid + '<span></span></span>');
						} else {
							thumbA = $('<span data-index="' + key + '" style="background-image:' + value + '">' + thumbAVid + '<span></span></span>');
						}
						thumbsCont.append(thumbA);
						contW = contW + thumbA.width();
						contH = thumbA.height() > contH ? thumbA.height() : contH;
					}
				});
				var setCursor;
				target.next('.slideshine_margin').css({height:contH});
				thumbsCont.height(contH).wrapInner('<div class="slideshine_thumbs_scroller" />').kinetic({
					y : false,
					moved : function(){
						moving = true;
					},
					stopped : function(){
						setTimeout(function(){
							moving = false;
						},10);
					}
				}).bind('mouseout',function(){
					moving = false;				
				});
				var localResize = function(){
					if ( thumbsCont.width() < contW ) {
						thumbsCont.addClass('widerThan');
					} else {
						thumbsCont.removeClass('widerThan');
					}
				};
				localResize();
				$(window).bind('resize',function(){ localResize(); });
				$('.slideshine_thumbs_scroller', target).width(contW);
				$('span[data-index]', thumbsCont).off('click touchend');
				$('span[data-index]', thumbsCont).on('click touchend', function(){
					if ( moving === false && sliding === false && !$(this).is('.slideshine_current_th')) {
						var ind = $(this).data('index');
						if ( ind < current ) {
							dir = 'backward';
						} else {
							dir = 'forward';
						}
						nextSlide(ind);
					}
				}).find('> span:not(.slideshine_thumb_video)');

			};

			if ( running === false && opts.thumbnails == 'true' && opts.bullets != 'true' ) {
				appendThumbs();
			}

			var appendBullets = function(){
				wrap.after('<div class="slideshine_bullets slideshine_fontsized" />');
				var bulletsCont = $('.slideshine_bullets', target),
					bullet, bulletCss, bulletCss2;
				$.each( arrSlides, function( key, value ) {
					if ( opts.thumbnails == 'true' ) {
						if ( arrThumbs[key] !== '' ) {
							var thumbAVid = arrThumbsVideo[key]!=='' ? '<span class="slideshine_thumb_video" style="background-image:' + arrThumbsVideo[key] + '"></span>' : '',
							thumbA = '<span class="thumb" style="background-image:' + arrThumbs[key] + '">' + thumbAVid + '<span></span></span>';
						}
					} else {
						var thumbA = '';
					}
					if ( key === 0 ) {
						bullet = $('<span data-index="' + key + '" class="slideshine_current_blt"><span class="current"></span>'+thumbA+'</span>');
					} else {
						bullet = $('<span data-index="' + key + '"><span class="current"></span>'+thumbA+'</span>');
					}
					bulletsCont.append(bullet);
				});

				if ( Modernizr.rgba ) {
					bulletCss = {backgroundColor: 'rgba('+opts.skinbgrgb+','+opts.skinopacity+')', borderColor: opts.skin};
				} else {
					bulletCss = {background: opts.skinbg, borderColor: opts.skin};
				}

				$('span[data-index] span.current', bulletsCont)
					.css({background: opts.skin});

				var dur = parseFloat($('span[data-index] span.thumb').css('transition-duration'))*1000;

				$('span[data-index]', bulletsCont)
					.css(bulletCss)
					.off('click touchend');
						$('span[data-index]', bulletsCont).removeClass('slideshine_current_blt')
					.on('click touchend', function(){
						if ( sliding === false && !$(this).is('.slideshine_current_blt')) {
							var ind = $(this).data('index');
							if ( ind < current ) {
								dir = 'backward';
							} else {
								dir = 'forward';
							}
							nextSlide(ind);
						}
					});
				if ( Modernizr.csstransforms ) {
					$('span[data-index]', bulletsCont).hover(function(){
						$('span.thumb',this).fadeIn(dur);
					}, function(){
						$('span.thumb',this).fadeOut(dur);
					});
				}

			};

			if ( running === false && opts.bullets == 'true' ) {
				appendBullets();
			}

			clearTimeout(checkAgain);

			if ( slide.hasClass('loaded') ) {

				pause = optsSlide.stop != 'true' ? pause : true;

				if ( pause === true ) {
					optsplay.removeClass('opacity_0');
					optspause.addClass('opacity_0');
				} else {
					optspause.removeClass('opacity_0');
					optsplay.addClass('opacity_0');
				}

				sliding = true;

				winW = parseFloat(target.width());
				var winH = parseFloat(target.height());

				resizeBg(counter);

				var afterSlide = function(counter) {

					sliding = false;

					previous.stop().css({left:0,top:0,x:0,y:0,display:'none',transformOrigin:'50% 50%'}).removeClass('zooming');
					slide.css({left:0,top:0,x:0,y:0,transformOrigin:'50% 50%'});
					if ( $('iframe',slide).length ) {
						$('iframe',slide).each(function(){
							var t = $(this);
							var src = t.attr('data-src');
							t.attr('src',src).removeAttr('data-src');
						}).one('load',function(){
							timeSet = 0;
							opts.slideReady.call(this);
							target.trigger('slideReady');
							runSlide(counter);
						});
					} else {
						timeSet = 0;
						opts.slideReady.call(this);
						target.trigger('slideReady');
						runSlide(counter);
					}

				};

				var zoomFx = function(el) {
					if (Modernizr.csstransitions && optsSlide.longfx!='none' && optsSlide.longfx!=='') { //the clone workaround is because of Transit doens't stop the css animations
						var zoom1, zoom2;

						cloneImg = el.clone();
						el.after(cloneImg).remove();

						if ( optsSlide.longfx == 'zoomout' ) {
							zoom1 = { scale: 1.35 };
							zoom2 = { scale: 1.01 };
						} else {
							zoom1 = { scale: 1.01 };
							zoom2 = { scale: 1.35 };
						}
						cloneImg.css( zoom1 );
						slide.addClass('zooming');
						if ( pause !== true && scrolled !== true ) {
							cloneImg.transition( zoom2,((parseFloat(optsSlide.speed)*3)+parseFloat(optsSlide.timeout*1.5)),function(){
								slide.removeClass('zooming');
							});
						}
					}
				};

				sliding = true;

				//appearCaption(false);

				if ( running === false ) {

					preloadBgs(counter+1, false);
					manageCaptions();

					var imgToClone = slide.find(' > [data-opts] > img');
					zoomFx(imgToClone);

					slide.addClass('slideshine_current').css({zIndex:1}).fadeIn(optsSlide.speed,function(){
						afterSlide(counter);
					});

				} else {

					preloadBgs(counter+1, false);
					manageCaptions();

					slide.addClass('slideshine_current').css({zIndex:1}).show().find(' > [data-opts]').addClass('opacity_0');
					$('.slideshine_caption', previous).fadeOut(optsSlide.speed);
					var i, dir1, dir2;

					var spiralAlg = function(rows, cols) {
						var rows2 = rows/2, x, y, z, n=0, orderMosaic = [];
						for (z = 0; z < rows2; z++){
							y = z;
							for (x = z; x < cols - z - 1; x++) {
								orderMosaic[n++] = y * cols + x;
							}
							x = cols - z - 1;
							for (y = z; y < rows - z - 1; y++) {
								orderMosaic[n++] = y * cols + x;
							}
							y = rows - z - 1;
							for (x = cols - z - 1; x > z; x--) {
								orderMosaic[n++] = y * cols + x;
							}
							x = z;
							for (y = rows - z - 1; y > z; y--) {
								orderMosaic[n++] = y * cols + x;
							}
						}

						return orderMosaic;
					};

					/* Create the array with the default order of blocks */
					var arrSortFunc = function(rows, cols) {
						var arrSort = [],
							blocks = rows*cols;
						for (i=0; i < blocks; i++) {
							arrSort[i] = i;
						}
						return arrSort;
					};

					/*Randomize degrees*/
					var arrRandomDeg = function(max, min) {
						var no = Math.floor ( Math.random() * (max - min) + min ),
							sign = Math.random() < 0.5 ? -1 : 1;

						return (no*sign);
					};

					/* Defining the values for the various animation effects */
					var arrSort = [],
						delay = [],
						newOrder = [],
						coeffDelay = 1,
						coeffSpeed = 1,
						rows = 1,
						cols = 1,
						startingpoint = {},
						endingpoint = {},
						wrapH = wrap.height()+1,
						wrapW = wrap.width()+1,
						cloneH,
						cloneW;
					switch(optsSlide.fx){
						case 'breakdown':
							rows = parseFloat(optsSlide.gridrows);
							cols = parseFloat(optsSlide.gridcols);
							arrSort = shuffle(arrSortFunc(rows, cols));
							delay = arrSortFunc(rows, cols);
							cloneH = Math.round(wrapH/rows);
							cloneW = Math.round(wrapW/cols);
							coeffDelay = 1.2;
							coeffSpeed = 0.75;
							break;
						case 'palettes':
							rows = parseFloat(optsSlide.gridrows);
							cols = parseFloat(optsSlide.gridcols);
							arrSort = shuffle(arrSortFunc(rows, cols));
							delay = arrSortFunc(rows, cols);
							cloneH = Math.round(wrapH/rows);
							cloneW = Math.round(wrapW/cols);
							if ( optsSlide.fxslide != 'current' ) {
								startingpoint = { opacity: 0 };
								endingpoint = { opacity: 1 };
							} else {
								startingpoint = { opacity: 1 };
								endingpoint = { opacity: 0 };
							}
							coeffDelay = 0.75;
							coeffSpeed = 1.2;
							break;
						case 'mosaic':
							rows = parseFloat(optsSlide.gridrows);
							cols = parseFloat(optsSlide.gridcols);
							arrSort = shuffle(arrSortFunc(rows, cols));
							delay = arrSortFunc(rows, cols);
							cloneH = Math.round(wrapH/rows);
							cloneW = Math.round(wrapW/cols);
							coeffDelay = 1.2;
							coeffSpeed = 0.75;
							if (Modernizr.csstransforms) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, x: (cloneW/2), y: (cloneH/2), width: 0, opacity: 0};
									endingpoint = { height: cloneH, x: 0, y: 0, width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, x: 0, y: 0, width: cloneW, opacity: 1};
									endingpoint = { height: 0, x: (cloneW/2), y: (cloneH/2), width: 0, opacity: 0};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, marginLeft: (cloneW/2), marginTop: (cloneH/2), width: 0, opacity: 0};
									endingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
									endingpoint = { height: 0, marginLeft: (cloneW/2), marginTop: (cloneH/2), width: 0, opacity: 0};
								}
							}
							break;
						case 'mosaicFromLeftTop':
							rows = parseFloat(optsSlide.gridrows);
							cols = parseFloat(optsSlide.gridcols);
							for (var y = 0; y < rows; y++) {
								for (var x = 0; x < cols; x++) {
									newOrder.push(x + y);
								}
							}
							arrSort = arrSortFunc(rows, cols);
							delay = newOrder;
							cloneH = Math.round(wrapH/rows);
							cloneW = Math.round(wrapW/cols);
							coeffDelay = 5;
							coeffSpeed = 0.7;
							if (Modernizr.csstransforms3d) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, x: '-'+(cloneW*0.5)+'px', y: '-'+(cloneH*0.5)+'px', width: 0, opacity: 0, transformOrigin: '0 0', scale: .7, rotate: '45deg', rotate3d: '1,1,0,90deg'};
									endingpoint = { height: cloneH, x: 0, y: 0, width: cloneW, opacity: 1, scale: 1, rotate: '0', rotate3d: '1,1,0,0' };
								} else {
									startingpoint = { height: cloneH, x: 0, y: 0, width: cloneW, opacity: 1, transformOrigin: '100% 100%', scale: 1, rotate: '0'};
									endingpoint = { height: 0, x: cloneW, y: cloneH, width: 0, opacity: 0, scale: .7, rotate: '90deg', rotate3d: '0,1,1,-180deg'};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, marginLeft: '-'+(cloneW*0.5)+'px', marginTop: '-'+(cloneH*0.5)+'px', width: 0, opacity: 0};
									endingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
									endingpoint = { height: 0, marginLeft: cloneW, marginTop: cloneH*1.5, width: 0, opacity: 0};
								}
							}
							break;
						case 'mosaicFromRightTop':
							rows = parseFloat(optsSlide.gridrows);
							cols = parseFloat(optsSlide.gridcols);
							for (var y = 0; y < rows; y++) {
								for (var x = cols; x > 0; x--) {
									newOrder.push(x + y);
								}
							}
							arrSort = arrSortFunc(rows, cols);
							delay = newOrder;
							cloneH = Math.round(wrapH/rows);
							cloneW = Math.round(wrapW/cols);
							coeffDelay = 5;
							coeffSpeed = 0.7;
							if (Modernizr.csstransforms3d) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, x: cloneW*1.5, y: '-'+(cloneH*0.5)+'px', width: 0, opacity: 0, transformOrigin: '100% 0', scale: .7, rotate: '-45deg', rotate3d: '1,1,0,-90deg'};
									endingpoint = { height: cloneH, x: 0, y: 0, width: cloneW, opacity: 1, scale: 1, rotate: '0', rotate3d: '1,1,0,0'};
								} else {
									startingpoint = { height: cloneH, x: 0, y: 0, width: cloneW, opacity: 1, transformOrigin: '0% 100%', scale: 1, rotate: '0'};
									endingpoint = { height: 0, x: '-'+cloneW*0.5+'px', y: cloneH, width: 0, opacity: 0, scale: .7, rotate: '130deg', rotate3d: '0,1,1,-90deg'};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, marginLeft: cloneW*1.5, marginTop: '-'+(cloneH*0.5)+'px', width: 0, opacity: 0};
									endingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
									endingpoint = { height: 0, marginLeft: '-'+cloneW*0.5+'px', marginTop: cloneH, width: 0, opacity: 0};
								}
							}
							break;
						case 'mosaicFromLeftBottom':
							rows = parseFloat(optsSlide.gridrows);
							cols = parseFloat(optsSlide.gridcols);
							for (var y = rows; y > 0; y--) {
								for (var x = 0; x < cols; x++) {
									newOrder.push(x + y);
								}	
							}
							arrSort = arrSortFunc(rows, cols);
							delay = newOrder;
							cloneH = Math.round(wrapH/rows);
							cloneW = Math.round(wrapW/cols);
							coeffDelay = 5;
							coeffSpeed = 0.7;
							if (Modernizr.csstransforms3d) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, x: '-'+(cloneW*0.5)+'px', y: cloneH*1.5, width: 0, opacity: 0, transformOrigin: '100% 0', scale: .7, rotate: '-45deg', rotate3d: '1,1,0,-90deg'};
									endingpoint = { height: cloneH, x: 0, y: 0, width: cloneW, opacity: 1, scale: 1, rotate: '0', rotate3d: '1,1,0,0'};
								} else {
									startingpoint = { height: cloneH, x: 0, y: 0, width: cloneW, opacity: 1, transformOrigin: '100% 100%', scale: 1, rotate: '0'};
									endingpoint = { height: 0, x: cloneW*1.5, y: '-'+(cloneH*0.5)+'px', width: 0, opacity: 0, scale: .7, rotate: '-45deg', rotate3d: '1,1,0,90deg'};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, marginLeft: '-'+(cloneW*0.5)+'px', marginTop: cloneH*1.5, width: 0, opacity: 0};
									endingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
									endingpoint = { height: 0, marginLeft: cloneW, marginTop: 0, width: 0, opacity: 0};
								}
							}
							break;
						case 'mosaicFromRightBottom':
							rows = parseFloat(optsSlide.gridrows);
							cols = parseFloat(optsSlide.gridcols);
							for (var y = 0; y < rows; y++) {
								for (var x = 0; x < cols; x++) {
									newOrder.push(x + y);
								}
							}
							arrSort = arrSortFunc(rows, cols);
							delay = newOrder.reverse();
							cloneH = Math.round(wrapH/rows);
							cloneW = Math.round(wrapW/cols);
							coeffDelay = 5;
							coeffSpeed = 0.7;
							if (Modernizr.csstransforms3d) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, x: cloneW*1.5, marginTop: cloneH*1.5, width: 0, opacity: 0, transformOrigin: '100% 0', scale: .7, rotate: '-45deg', rotate3d: '1,1,0,-90deg'};
									endingpoint = { height: cloneH, x: 0, marginTop: 0, width: cloneW, opacity: 1, scale: 1, rotate: '0', rotate3d: '1,1,0,0'};
								} else {
									startingpoint = { height: cloneH, x: 0, y: 0, marginTop: 0, width: cloneW, opacity: 1, transformOrigin: '100% 0%', scale: 1, rotate: '0'};
									endingpoint = { height: 0, x: '-'+(cloneW*0.5)+'px', y: '-'+(cloneH*0.5)+'px', marginTop: 0, width: 0, opacity: 0, scale: .7, rotate: '45deg', rotate3d: '1,1,0,-90deg'};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, marginLeft: cloneW*1.5, marginTop: cloneH*1.5, width: 0, opacity: 0};
									endingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
									endingpoint = { height: 0, marginLeft: 0, marginTop: 0, width: 0, opacity: 0};
								}
							}
							break;
						case 'spiral':
							rows = parseFloat(optsSlide.gridrows);
							cols = parseFloat(optsSlide.gridcols);
							arrSort = spiralAlg(rows, cols);
							delay = arrSortFunc(rows, cols);
							cloneH = Math.round(wrapH/rows);
							cloneW = Math.round(wrapW/cols);
							coeffSpeed = 0.7;
							if (Modernizr.csstransforms) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, x: cloneW/2, y: cloneH/2, width: 0, opacity: 0};
									endingpoint = { height: cloneH, x: 0, y: 0, width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, x: 0, y: 0, width: cloneW, opacity: 1};
									endingpoint = { height: 0, x: cloneW/2, y: cloneH/2, width: 0, opacity: 0};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, marginLeft: cloneW/2, marginTop: cloneH/2, width: 0, opacity: 0};
									endingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
									endingpoint = { height: 0, marginLeft: cloneW/2, marginTop: cloneH/2, width: 0, opacity: 0};
								}
							}
							break;
						case 'spiralreverse':
							rows = parseFloat(optsSlide.gridrows);
							cols = parseFloat(optsSlide.gridcols);
							arrSort = spiralAlg(rows, cols);
							delay = arrSortFunc(rows, cols).reverse();
							cloneH = Math.round(wrapH/rows);
							cloneW = Math.round(wrapW/cols);
							coeffSpeed = 0.7;
							if (Modernizr.csstransforms) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, x: cloneW/2, y: cloneH/2, width: 0, opacity: 0};
									endingpoint = { height: cloneH, x: 0, y: 0, width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, x: 0, y: 0, width: cloneW, opacity: 1};
									endingpoint = { height: 0, x: cloneW/2, y: cloneH/2, width: 0, opacity: 0};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, marginLeft: cloneW/2, marginTop: cloneH/2, width: 0, opacity: 0};
									endingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, marginLeft: 0, marginTop: 0, width: cloneW, opacity: 1};
									endingpoint = { height: 0, marginLeft: cloneW/2, marginTop: cloneH/2, width: 0, opacity: 0};
								}
							}
							break;
						case 'curtainFromLeft':
							rows = 1;
							cols = parseFloat(optsSlide.cols);
							cloneH = wrapH;
							cloneW = Math.round(wrapW/cols);
							arrSort = arrSortFunc(rows, cols);
							delay = arrSort;
							coeffSpeed = 1.15;
							coeffDelay = 1.15;
							if (Modernizr.csstransforms) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: cloneH, x: cloneW, width: 0, opacity: 0, transformOrigin: '100% 0', rotateY: '180deg'};
									endingpoint = { x: 0, width: cloneW, opacity: 1, rotateY: '0deg'};
								} else {
									startingpoint = { height: cloneH, x: 0, width: cloneW, opacity: 1, transformOrigin: '0 0', rotateY: '0deg'};
									endingpoint = { x: 0, width: 0, opacity: 0, rotateY: '180deg'};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: cloneH, marginLeft: cloneW, width: 0, opacity: 0};
									endingpoint = { marginLeft: 0, width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, marginLeft: 0, width: cloneW, opacity: 1};
									endingpoint = { marginLeft: cloneW, width: 0, opacity: 0};
								}
							}
							break;
						case 'curtainFromRight':
							rows = 1;
							cols = parseFloat(optsSlide.cols);
							cloneH = wrapH;
							cloneW = Math.round(wrapW/cols);
							arrSort = arrSortFunc(rows, cols).reverse();
							delay = arrSortFunc(rows, cols);
							coeffSpeed = 1.15;
							coeffDelay = 1.15;
							if (Modernizr.csstransforms) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: cloneH, width: cloneW, opacity: 0, transformOrigin: '0% 0%', rotateY: '180deg'};
									endingpoint = { opacity: 1, rotateY: '0deg'};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1, transformOrigin: '100% 0', rotateY: '0deg'};
									endingpoint = { x: cloneW, opacity: 0, rotateY: '180deg'};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: cloneH, marginLeft: 0, width: 0, opacity: 0};
									endingpoint = { marginLeft: 0, width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, marginLeft: 0, width: cloneW, opacity: 1};
									endingpoint = { marginLeft: cloneW, width: 0, opacity: 0};
								}
							}
							break;
						case 'curtainFromTop':
							rows = parseFloat(optsSlide.rows);
							cols = 1;
							cloneH = Math.round(wrapH/rows);
							cloneW = wrapW;
							arrSort = arrSortFunc(rows, cols);
							delay = arrSortFunc(rows, cols);
							coeffSpeed = 1.15;
							coeffDelay = 1.15;
							if (Modernizr.csstransforms) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { width: cloneW, opacity: 0, transformOrigin: '0% 100%', rotateX: '180deg'};
									endingpoint = { opacity: 1, rotateX: '0deg'};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1, transformOrigin: '0 0', rotateX: '0deg'};
									endingpoint = { y: cloneH, opacity: 0, rotateX: '180deg'};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, width: cloneW, marginTop: cloneH, opacity: 0};
									endingpoint = { height: cloneH, marginTop: 0, opacity: 1};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1};
									endingpoint = { marginTop: cloneH, height: 0, opacity: 0};
								}
							}
							break;
						case 'curtainFromBottom':
							rows = parseFloat(optsSlide.rows);
							cols = 1;
							cloneH = Math.round(wrapH/rows);
							cloneW = wrapW;
							arrSort = arrSortFunc(rows, cols).reverse();
							delay = arrSortFunc(rows, cols);
							coeffSpeed = 1.15;
							coeffDelay = 1.15;
							if (Modernizr.csstransforms) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { width: cloneW, opacity: 0, transformOrigin: '0% 0%', rotateX: '-180deg'};
									endingpoint = { opacity: 1, rotateX: '0deg'};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1, transformOrigin: '0 100%', rotateX: '0deg'};
									endingpoint = { opacity: 0, rotateX: '-180deg'};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, width: cloneW, marginTop: 0, opacity: 0};
									endingpoint = { height: cloneH, opacity: 1};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1};
									endingpoint = { height: 0, opacity: 0};
								}
							}
							break;
						case 'slicesFromLeft':
							rows = 1;
							cols = parseFloat(optsSlide.cols);
							cloneH = wrapH;
							cloneW = Math.round(wrapW/cols);
							arrSort = arrSortFunc(rows, cols);
							delay = arrSortFunc(rows, cols);
							coeffDelay = 1.35;
							coeffSpeed = 0.75;
							if (Modernizr.csstransforms) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: cloneH, width: cloneW, opacity: 0, transformOrigin: '50% 0%', rotateX: '180deg', rotateY: '90deg', rotate: '25deg', y: '-'+(cloneH/2)+'px'};
									endingpoint = { opacity: 1, rotateX: '0deg', rotateY: '0deg', rotate: '0deg', y: 0};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1, transformOrigin: '50% 100%', rotateX: '0deg', rotateY: '0deg', rotate: '0deg', y: 0};
									endingpoint = { opacity: 0, rotateX: '-135deg', rotateY: '-5deg', rotate: '25deg', y: cloneH*1.5};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, width: cloneW, opacity: 0};
									endingpoint = { height: cloneH, opacity: 1};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1};
									endingpoint = { height: 0, opacity: 0};
								}
							}
							break;
						case 'slicesFromRight':
							rows = 1;
							cols = parseFloat(optsSlide.cols);
							cloneH = wrapH;
							cloneW = Math.round(wrapW/cols);
							arrSort = arrSortFunc(rows, cols).reverse();
							delay = arrSortFunc(rows, cols);
							coeffDelay = 1.35;
							coeffSpeed = 0.75;
							if (Modernizr.csstransforms) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: cloneH, width: cloneW, opacity: 0, transformOrigin: '50% 0%', rotateX: '180deg', rotateY: '90deg', rotate: '45deg', y: '-'+(cloneH/2)+'px'};
									endingpoint = { opacity: 1, rotateX: '0deg', rotateY: '0deg', rotate: '0deg', y: 0};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1, transformOrigin: '50% 100%', rotateX: '0deg', rotateY: '0deg', rotate: '0deg', y: 0};
									endingpoint = { opacity: 0, rotateX: '180deg', rotateY: '90deg', rotate: '45deg', y: cloneH*1.5};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: 0, width: cloneW, opacity: 0};
									endingpoint = { height: cloneH, opacity: 1};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1};
									endingpoint = { height: 0, opacity: 0};
								}
							}
							break;
						case 'slicesFromTop':
							cols = 1;
							rows = parseFloat(optsSlide.rows);
							cloneH = Math.round(wrapH/rows);
							cloneW = wrapW;
							arrSort = arrSortFunc(rows, cols);
							delay = arrSortFunc(rows, cols);
							coeffDelay = 1.35;
							coeffSpeed = 0.65;
							if (Modernizr.csstransforms) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: cloneH, width: cloneW, opacity: 0, transformOrigin: '100% 50%', rotateX: '-45deg', rotateY: '-5deg', rotate: '45deg', x: '-'+(cloneW/2)+'px'};
									endingpoint = { opacity: 1, rotateX: '0deg', rotateY: '0deg', rotate: '0deg', x: 0};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1, transformOrigin: '100% 50%', rotateX: '0deg', rotateY: '0deg', rotate: '0deg', y: 0};
									endingpoint = { opacity: 0, rotateX: '90deg', rotateY: '-90deg', rotate: '45deg', y: cloneH*1.5};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: cloneH, width: 0, opacity: 0};
									endingpoint = { width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1};
									endingpoint = { width: 0, opacity: 0};
								}
							}
							break;
						case 'slicesFromBottom':
							cols = 1;
							rows = parseFloat(optsSlide.rows);
							cloneH = Math.round(wrapH/rows);
							cloneW = wrapW;
							arrSort = arrSortFunc(rows, cols).reverse();
							delay = arrSortFunc(rows, cols);
							coeffDelay = 1.35;
							coeffSpeed = 0.65;
							if (Modernizr.csstransforms) {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: cloneH, width: cloneW, opacity: 0, transformOrigin: '0% 50%', rotateX: '180deg', rotateY: '45deg', rotate: '-45deg', x: cloneW*1.5, y: cloneH*1.5};
									endingpoint = { opacity: 1, rotateX: '0deg', rotateY: '0deg', rotate: '0deg', x: 0, y: 0};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1, transformOrigin: '0% 100%', rotateX: '0deg', rotateY: '0deg', rotate: '0deg', x: 0};
									endingpoint = { opacity: 0, rotateX: '90deg', rotateY: '-45deg', rotate: '-45deg', x: '-'+(cloneW*0.5)+'px'};
								}
							} else {
								if ( optsSlide.fxslide != 'current' ) {
									startingpoint = { height: cloneH, width: 0, opacity: 0};
									endingpoint = { width: cloneW, opacity: 1};
								} else {
									startingpoint = { height: cloneH, width: cloneW, opacity: 1};
									endingpoint = { width: 0, opacity: 0};
								}
							}
							break;
						case 'cardShufflingFromLeft':
							rows = 1;
							cols = parseFloat(optsSlide.cols);
							cloneH = wrapH;
							cloneW = Math.round(wrapW/cols);
							arrSort = arrSortFunc(rows, cols);
							delay = arrSortFunc(rows, cols);
							if ( optsSlide.fxslide != 'current' ) {
								startingpoint = { height: 0, width: cloneW, opacity: 0};
								endingpoint = { height: cloneH, opacity: 1};
							} else {
								startingpoint = { height: cloneH, width: cloneW, opacity: 1};
								endingpoint = { height: 0, opacity: 0};
							}
							break;
						case 'cardShufflingFromRight':
							rows = 1;
							cols = parseFloat(optsSlide.cols);
							cloneH = wrapH;
							cloneW = Math.round(wrapW/cols);
							arrSort = arrSortFunc(rows, cols).reverse();
							delay = arrSortFunc(rows, cols);
							if ( optsSlide.fxslide != 'current' ) {
								startingpoint = { height: 0, width: cloneW, opacity: 0};
								endingpoint = { height: cloneH,opacity: 1};
							} else {
								startingpoint = { height: cloneH, width: cloneW, opacity: 1};
								endingpoint = { height: 0, opacity: 0};
							}
							break;
						case 'cardShufflingFromTop':
							cols = 1;
							rows = parseFloat(optsSlide.rows);
							cloneH = Math.round(wrapH/rows);
							cloneW = wrapW;
							arrSort = arrSortFunc(rows, cols);
							delay = arrSortFunc(rows, cols);
							if ( optsSlide.fxslide != 'current' ) {
								startingpoint = { height: cloneH, width: 0, opacity: 0};
								endingpoint = { width: cloneW, opacity: 1};
							} else {
								startingpoint = { height: cloneH, width: cloneW, opacity: 1};
								endingpoint = { width: 0, opacity: 0};
							}
							break;
						case 'cardShufflingFromBottom':
							cols = 1;
							rows = parseFloat(optsSlide.rows);
							cloneH = Math.round(wrapH/rows);
							cloneW = wrapW;
							arrSort = arrSortFunc(rows, cols).reverse();
							delay = arrSortFunc(rows, cols);
							if ( optsSlide.fxslide != 'current' ) {
								startingpoint = { height: cloneH, width: 0, opacity: 0};
								endingpoint = { width: cloneW, opacity: 1};
							} else {
								startingpoint = { height: cloneH, width: cloneW, opacity: 1};
								endingpoint = { width: 0, opacity: 0};
							}
							break;
						case 'vertical-cut':
							cols = 2;
							rows = 1;
							cloneH = wrapH;
							cloneW = Math.round(wrapW/cols);
							arrSort = arrSortFunc(rows, cols);
							if ( optsSlide.fxslide != 'current' ) {
								startingpoint = { height: cloneH, width: cloneW, opacity: 0};
								endingpoint = { opacity: 1};
							} else {
								startingpoint = { height: cloneH, width: cloneW, opacity: 1};
								endingpoint = { opacity: 0};
							}
							break;
						case 'horizontal-cut':
							cols = 1;
							rows = 2;
							cloneH = Math.round(wrapH/rows);
							cloneW = wrapW;
							arrSort = arrSortFunc(rows, cols);
							if ( optsSlide.fxslide != 'current' ) {
								startingpoint = { height: cloneH, width: cloneW};
							} else {
								startingpoint = { height: cloneH, width: cloneW};
							}
							break;
						case 'downwards':
							if ( optsSlide.layout == 'fixed2' ) {
								winH = hFixed2;
							}
							if ( dir == 'backward' ) {
								dir1 = (winH-1);
								dir2 = '-'+(winH/2)+'px';
							} else {
								dir1 = '-'+(winH-1)+'px';
								dir2 = winH/2;
							}
							break;
						case 'upwards':
							if ( optsSlide.layout == 'fixed2' ) {
								winH = hFixed2;
							}
							if ( dir == 'backward' ) {
								dir1 = '-'+(winH-1)+'px';
								dir2 = winH/2;
							} else {
								dir1 = (winH-1);
								dir2 = '-'+(winH/2)+'px';
							}
							break;
						case 'leftwards':
							if ( dir == 'backward' ) {
								dir1 = '-'+(winW-1)+'px';
								dir2 = winW/2;
							} else {
								dir1 = (winW-1);
								dir2 = '-'+(winW/2)+'px';
							}
							break;
						case 'rightwards':
							if ( dir == 'backward' ) {
								dir1 = (winW-1);
								dir2 = '-'+(winW/2)+'px';
							} else {
								dir1 = '-'+(winW-1)+'px';
								dir2 = winW/2;
							}
							break;
					}


					/* Run the animation effects */
					var setStartFunc = function(i, blocks){
						if ( i==blocks ) {
							previous.find(' > [data-opts]').removeClass('opacity_0').end().find('.cloneSlide').remove();
							slide.find(' > [data-opts]').removeClass('opacity_0').end().find('.cloneSlide').fadeOut(250,function(){$(this).remove();});
							afterSlide(counter);
						}
					};
					var blocks = rows*cols,
						fxSlide, fxPrevious;

					/* mosaic effects */
					switch(optsSlide.fx){
						case 'breakdown':
						case 'palettes':
						case 'mosaic':
						case 'mosaicFromLeftTop':
						case 'mosaicFromRightTop':
						case 'mosaicFromLeftBottom':
						case 'mosaicFromRightBottom':
						case 'spiral':
						case 'spiralreverse':
						case 'curtainFromLeft':
						case 'curtainFromRight':
						case 'curtainFromTop':
						case 'curtainFromBottom':
						case 'slicesFromLeft':
						case 'slicesFromRight':
						case 'slicesFromTop':
						case 'slicesFromBottom':
						case 'cardShufflingFromLeft':
						case 'cardShufflingFromRight':
						case 'cardShufflingFromTop':
						case 'cardShufflingFromBottom':
						case 'vertical-cut':
						case 'horizontal-cut':
							if ( optsSlide.fxslide != 'current' ) {
								fxSlide = slide;
							} else {
								fxSlide = previous;
							}
							var imgToClone = fxSlide.find(' > [data-opts] > img');
							zoomFx(slide.find(' > [data-opts] > img'));
							for (i=0; i < blocks; i++) {
								var cloneBg = imgToClone.clone(),
									cloneSlide = $('<div class="cloneSlide" />').one().append(cloneBg);
								fxSlide.append(cloneSlide);
								if ( optsSlide.fxslide != 'current' ) {
									zoomFx(cloneBg);
								}
								cloneSlide
									.css({left: cloneW*(i%cols), top: cloneH*(Math.floor(i/cols)), height: cloneH+1, width: cloneW+1})
									.find('> img')
										.css({top: '-'+(cloneH*Math.floor(i/cols))+'px', left: '-'+cloneW*(i%cols)+'px'});
							}


							var inew=0;
							for (i=0; i < blocks; i++) {

								var somethingelsestart = {},
									somethingelseend = {};

								switch(optsSlide.fx){
									case 'breakdown':
										if ( optsSlide.fxslide != 'current' ) {
											startingpoint = { scale: 0, opacity: 0, rotate: arrRandomDeg(360,180)+'deg' };
											endingpoint = { scale: 1, rotate: '0', opacity: 1 };
										} else {
											startingpoint = { scale: 1, opacity: 1, rotate: 0 };
											endingpoint = { scale: 0, rotate: arrRandomDeg(360,180)+'deg', opacity: 0 };
										}
										break;
									case 'palettes':
										if ( optsSlide.fxslide != 'current' ) {
											startingpoint = { scale: 0, opacity: 0, rotateY: arrRandomDeg(540,90)+'deg', rotateX: arrRandomDeg(540,90)+'deg' };
											endingpoint = { scale: 1, rotateY: '0', rotateX: '0', opacity: 1 };
										} else {
											startingpoint = { scale: 1, opacity: 1, rotateY: 0, rotateX: 0 };
											endingpoint = { scale: 0, rotateY: arrRandomDeg(540,90)+'deg', rotateX: arrRandomDeg(540,90)+'deg', opacity: 0 };
										}
										break;
									case 'cardShufflingFromLeft':
										if ( optsSlide.fxslide != 'current' ) {
											if (i%2===0) {
												if (Modernizr.csstransforms) {
													startingpoint = { rotate: '-15deg', rotateX: '90deg', transformOrigin: '0% 0%', opacity: 0, y: '-'+(cloneH*0.3)+'px' };
													endingpoint = { rotate: '0deg', rotateX: '0deg', opacity: 1, y: 0 };
												} else {
													startingpoint = { opacity: 0, marginTop: '-'+cloneH+'px' };
													endingpoint = { opacity: 1, marginTop: 0 };
												}
											} else {
												if (Modernizr.csstransforms) {
													startingpoint = { rotate: '15deg', rotateX: '-90deg', transformOrigin: '0 100%', opacity: 0, y: (cloneH*0.3) };
													endingpoint = { rotate: '0deg', rotateX: '0deg', opacity: 1, y: 0 };
												} else {
													startingpoint = { opacity: 0, marginTop: cloneH };
													endingpoint = { opacity: 1, marginTop: 0 };
												}
											}
										} else {
											if (i%2===0) {
												if (Modernizr.csstransforms) {
													startingpoint = { transformOrigin: '100% 0%', rotate: '0', rotateX: '0deg', opacity: 1, y: 0 };
													endingpoint = { rotate: '15deg', rotateX: '90deg', opacity: 0, y: '-'+(cloneH*0.5)+'px' };
												} else {
													startingpoint = { opacity: 1, marginTop: 0 };
													endingpoint = { opacity: 0, marginTop: cloneH };
												}
											} else {
												if (Modernizr.csstransforms) {
													startingpoint = { transformOrigin: '100% 100%', rotate: '0', rotateX: '0deg', opacity: 1, y: 0 };
													endingpoint = { rotate: '-15deg', rotateX: '-90deg', opacity: 0, y: (cloneH*0.5) };
												} else {
													startingpoint = { opacity: 1, marginTop: 0 };
													endingpoint = { opacity: 0, marginTop: '-'+cloneH+'px' };
												}
											}
										}
										break;
									case 'cardShufflingFromRight':
										if ( optsSlide.fxslide != 'current' ) {
											if (i%2===0) {
												if (Modernizr.csstransforms) {
													startingpoint = { rotate: '15deg', rotateX: '90deg', transformOrigin: '0% 0%', opacity: 0, y: '-'+(cloneH*0.3)+'px' };
													endingpoint = { rotate: '0deg', rotateX: '0deg', opacity: 1, y: 0 };
												} else {
													startingpoint = { opacity: 0, marginTop: '-'+cloneH+'px' };
													endingpoint = { opacity: 1, marginTop: 0 };
												}
											} else {
												if (Modernizr.csstransforms) {
													startingpoint = { rotate: '-15deg', rotateX: '-90deg', transformOrigin: '0 100%', opacity: 0, y: (cloneH*0.3) };
													endingpoint = { rotate: '0deg', rotateX: '0deg', opacity: 1, y: 0 };
												} else {
													startingpoint = { opacity: 0, marginTop: cloneH };
													endingpoint = { opacity: 1, marginTop: 0 };
												}
											}
										} else {
											if (i%2===0) {
												if (Modernizr.csstransforms) {
													startingpoint = { transformOrigin: '0% 0%', rotate: '0', rotateX: '0deg', opacity: 1, y: 0 };
													endingpoint = { rotate: '-15deg', rotateX: '90deg', opacity: 0, y: '-'+(cloneH*0.5)+'px' };
												} else {
													startingpoint = { opacity: 1, marginTop: 0 };
													endingpoint = { opacity: 0, marginTop: cloneH };
												}
											} else {
												if (Modernizr.csstransforms) {
													startingpoint = { transformOrigin: '0% 100%', rotate: '0', rotateX: '0deg', opacity: 1, y: 0 };
													endingpoint = { rotate: '15deg', rotateX: '-90deg', opacity: 0, y: (cloneH*0.5) };
												} else {
													startingpoint = { opacity: 1, marginTop: 0 };
													endingpoint = { opacity: 0, marginTop: '-'+cloneH+'px' };
												}
											}
										}
										break;
									case 'cardShufflingFromTop':
										if ( optsSlide.fxslide != 'current' ) {
											if (i%2===0) {
												if (Modernizr.csstransforms) {
													startingpoint = { transformOrigin: '0% 0%', rotate: '15deg', rotateY: '90deg', opacity: 0 };
													endingpoint = { rotate: '0', rotateY: '0deg', opacity: 1, x: 0 };
												} else {
													startingpoint = { opacity: 0, marginLeft: '-'+cloneW+'px' };
													endingpoint = { opacity: 1, marginLeft: 0 };
												}
											} else {
												if (Modernizr.csstransforms) {
													startingpoint = { transformOrigin: '100% 0%', rotate: '15deg', rotateY: '90deg', opacity: 0 };
													endingpoint = { rotate: '0', rotateY: '0deg', opacity: 1, x: 0 };
												} else {
													startingpoint = { opacity: 0, marginLeft: cloneW };
													endingpoint = { opacity: 1, marginLeft: 0 };
												}
											}
										} else {
											if (i%2===0) {
												if (Modernizr.csstransforms) {
													startingpoint = { transformOrigin: '0% 0%', rotate: '0', rotateY: '0deg', opacity: 1, x: 0 };
													endingpoint = { rotate: '-15deg', rotateY: '45deg', opacity: 0, x: '-'+(cloneW)+'px' };
												} else {
													startingpoint = { opacity: 1, marginLeft: 0 };
													endingpoint = { opacity: 0, marginLeft: cloneW };
												}
											} else {
												if (Modernizr.csstransforms) {
													startingpoint = { transformOrigin: '100% 0%', rotate: '0', rotateY: '0deg', opacity: 1, x: 0 };
													endingpoint = { rotate: '15deg', rotateY: '-45deg', opacity: 0, x: (cloneW) };
												} else {
													startingpoint = { opacity: 1, marginLeft: 0 };
													endingpoint = { opacity: 0, marginLeft: '-'+cloneW+'px' };
												}
											}
										}
										break;
									case 'cardShufflingFromBottom':
										if ( optsSlide.fxslide != 'current' ) {
											if (i%2===0) {
												if (Modernizr.csstransforms) {
													startingpoint = { transformOrigin: '0% 0%', rotate: '15deg', rotateY: '90deg', opacity: 0 };
													endingpoint = { rotate: '0', rotateY: '0deg', opacity: 1, x: 0 };
												} else {
													startingpoint = { opacity: 0, marginLeft: '-'+cloneW+'px' };
													endingpoint = { opacity: 1, marginLeft: 0 };
												}
											} else {
												if (Modernizr.csstransforms) {
													startingpoint = { transformOrigin: '100% 0%', rotate: '15deg', rotateY: '90deg', opacity: 0 };
													endingpoint = { rotate: '0', rotateY: '0deg', opacity: 1, x: 0 };
												} else {
													startingpoint = { opacity: 0, marginLeft: cloneW };
													endingpoint = { opacity: 1, marginLeft: 0 };
												}
											}
										} else {
											if (i%2===0) {
												if (Modernizr.csstransforms) {
													startingpoint = { transformOrigin: '0% 0%', rotate: '0', rotateY: '0deg', opacity: 1, x: 0 };
													endingpoint = { rotate: '-15deg', rotateY: '45deg', opacity: 0, x: '-'+(cloneW)+'px' };
												} else {
													startingpoint = { opacity: 1, marginLeft: 0 };
													endingpoint = { opacity: 0, marginLeft: cloneW };
												}
											} else {
												if (Modernizr.csstransforms) {
													startingpoint = { transformOrigin: '100% 0%', rotate: '0', rotateY: '0deg', opacity: 1, x: 0 };
													endingpoint = { rotate: '15deg', rotateY: '-45deg', opacity: 0, x: (cloneW) };
												} else {
													startingpoint = { opacity: 1, marginLeft: 0 };
													endingpoint = { opacity: 0, marginLeft: '-'+cloneW+'px' };
												}
											}
										}
										break;
									case 'vertical-cut':
										if ( optsSlide.fxslide != 'current' ) {
											if (i%2===0) {
												if (Modernizr.csstransforms) {
													startingpoint = { scale: 1.5, rotate: '15deg', rotateY: '45deg', opacity: 0, y: (cloneH*0.5), x: '-'+(cloneW)+'px' };
													endingpoint = { scale: 1, rotate: '0', rotateY: '0deg', opacity: 1, y: 0, x: 0 };
												} else {
													startingpoint = { opacity: 0, marginLeft: '-'+(cloneW)+'px' };
													endingpoint = { opacity: 1, marginLeft: 0 };
												}
											} else {
												if (Modernizr.csstransforms) {
													startingpoint = { scale: 1.5, rotate: '15deg', rotateY: '-45deg', opacity: 0, y: '-'+(cloneH*0.5)+'px', x: (cloneW) };
													endingpoint = { scale: 1, rotate: '0', rotateY: '0deg', opacity: 1, y: 0, x: 0 };
												} else {
													startingpoint = { opacity: 0, marginLeft: cloneW };
													endingpoint = { opacity: 1, marginLeft: 0 };
												}
											}
										} else {
											if (i%2===0) {
												if (Modernizr.csstransforms) {
													startingpoint = { scale: 1, rotate: '0', rotateY: '0deg', opacity: 1, y: 0, x: 0 };
													endingpoint = { scale: 1.5, rotate: '15deg', rotateY: '45deg', opacity: 0, y: (cloneH*0.5), x: '-'+(cloneW)+'px' };
												} else {
													startingpoint = { opacity: 1, marginLeft: 0 };
													endingpoint = { opacity: 0, marginLeft: '-'+cloneW+'px' };
												}
											} else {
												if (Modernizr.csstransforms) {
													startingpoint = { scale: 1, rotate: '0', rotateY: '0deg', opacity: 1, y: 0, x: 0 };
													endingpoint = { scale: 1.5, rotate: '15deg', rotateY: '-45deg', opacity: 0, y: '-'+(cloneH*0.5)+'px', x: (cloneW) };
												} else {
													startingpoint = { opacity: 1, marginLeft: 0 };
													endingpoint = { opacity: 0, marginLeft: cloneW };
												}
											}
										}
										break;
									case 'horizontal-cut':
										if ( optsSlide.fxslide != 'current' ) {
											if (i%2===0) {
												if (Modernizr.csstransforms) {
													startingpoint = { scale: 1.5, rotate: '-15deg', rotateX: '-45deg', opacity: 0, y: '-'+(cloneH*1.5)+'px', x: '-'+(cloneW*0.5)+'px' };
													endingpoint = { scale: 1, rotate: '0', rotateX: '0deg', opacity: 1, y: 0, x: 0 };
												} else {
													startingpoint = { opacity: 0, marginTop: '-'+(cloneH*1.5)+'px' };
													endingpoint = { opacity: 1, marginTop: 0 };
												}
											} else {
												if (Modernizr.csstransforms) {
													startingpoint = { scale: 1.5, rotate: '-15deg', rotateX: '45deg', opacity: 0, y: (cloneH*1.5), x: (cloneW*0.5) };
													endingpoint = { scale: 1, rotate: '0', rotateX: '0deg', opacity: 1, y: 0, x: 0 };
												} else {
													startingpoint = { opacity: 0, marginTop: (cloneH*1.5) };
													endingpoint = { opacity: 1, marginTop: 0 };
												}
											}
										} else {
											if (i%2===0) {
												if (Modernizr.csstransforms) {
													startingpoint = { scale: 1, rotate: '0', rotateX: '0deg', opacity: 1, y: 0, x: 0 };
													endingpoint = { scale: 1.5, rotate: '-15deg', rotateX: '-45deg', opacity: 0, y: '-'+(cloneH*1.5)+'px', x: '-'+(cloneW*0.2)+'px' };
												} else {
													startingpoint = { opacity: 1, marginTop: 0 };
													endingpoint = { opacity: 0, marginTop: '-'+(cloneH*1.5)+'px' };
												}
											} else {
												if (Modernizr.csstransforms) {
													startingpoint = { scale: 1, rotate: '0', rotateX: '0deg', opacity: 1, y: 0, x: 0 };
													endingpoint = { scale: 1.5, rotate: '-15deg', rotateX: '45deg', opacity: 0, y: (cloneH*1.5), x: (cloneW*0.2) };
												} else {
													startingpoint = { opacity: 1, marginTop: 0 };
													endingpoint = { opacity: 0, marginTop: (cloneH*1.5) };
												}
											}
										}
										break;
								}

								if ( optsSlide.fxslide != 'current' ) {
									slide.find('.cloneSlide').eq(arrSort[i]).show().css(startingpoint).css({zIndex:(blocks-i)}).delay(delay[i]*coeffDelay*(optsSlide.speed/(rows*cols))).transition(endingpoint,(optsSlide.speed*(coeffSpeed)), optsSlide.easing, function(){
										inew = inew+1;
										setStartFunc(inew, blocks);
									});
								} else {
									slide.find(' > [data-opts]').removeClass('opacity_0');
									previous.css({zIndex:3}).find(' > [data-opts]').addClass('opacity_0').end()
									.find('.cloneSlide').eq(arrSort[i]).show().css(startingpoint).css({zIndex:i}).delay(delay[i]*coeffDelay*(optsSlide.speed/(rows*cols))).transition(endingpoint,optsSlide.speed*(coeffSpeed), optsSlide.easing, function(){
										inew = inew+1;
										setStartFunc(inew, blocks);
									});
								}
							}
							break;

						/* scroll effects */
						case 'upwards':
						case 'downwards':
							zoomFx(slide.find(' > [data-opts] > img'));
							if (Modernizr.csstransforms) {
								slide.find(' > [data-opts]').show().removeClass('opacity_0').end().addClass('slideshine_current').css({zIndex:1,y:dir1}).transition({y:0},optsSlide.speed, optsSlide.easing, function(){
									afterSlide(counter);
								});
								previous.find(' > [data-opts]').removeClass('opacity_0').end().transition({y:dir2},optsSlide.speed, optsSlide.easing);
							} else {
								slide.find(' > [data-opts]').removeClass('opacity_0').end().addClass('slideshine_current').show().css({zIndex:1,top:dir1}).transition({top:0},optsSlide.speed, optsSlide.easing, function(){
									afterSlide(counter);
								});
								previous.find(' > [data-opts]').removeClass('opacity_0').end().transition({top:dir2},optsSlide.speed, optsSlide.easing);
							}
							break;
						case 'leftwards':
						case 'rightwards':
							zoomFx(slide.find(' > [data-opts] > img'));
							if (Modernizr.csstransforms) {
								slide.find(' > [data-opts]').removeClass('opacity_0').end().addClass('slideshine_current').show().css({zIndex:1,x:dir1}).transition({x:0},optsSlide.speed, optsSlide.easing, function(){
									afterSlide(counter);
								});
								previous.find(' > [data-opts]').removeClass('opacity_0').end().transition({x:dir2},optsSlide.speed, optsSlide.easing);
							} else {
								slide.find(' > [data-opts]').removeClass('opacity_0').end().addClass('slideshine_current').show().css({zIndex:1,left:dir1}).transition({left:0},optsSlide.speed, optsSlide.easing, function(){
									afterSlide(counter);
								});
								previous.find(' > [data-opts]').removeClass('opacity_0').end().transition({left:dir2},optsSlide.speed, optsSlide.easing);
							}
							break;

						/* fade effect */
						case 'fade':
						default:
							zoomFx(slide.find(' > [data-opts] > img'));
							previous.find(' > [data-opts]').removeClass('opacity_0');
							slide.find(' > [data-opts]').removeClass('opacity_0').end()
								.addClass('slideshine_current').css({zIndex:1,opacity:0}).show().transition({opacity:1},optsSlide.speed, optsSlide.easing, function(){
								afterSlide(counter);
							});

					}


				}

				if ( optsSlide.layout == 'fixed2' ) {
					target.transition({ height : hFixed2 }, (optsSlide.speed/2), optsSlide.easing);
				}

				dir = 'forward';

				if ( opts.thumbnails == 'true' && opts.bullets != 'true' ) {

					var thumbsCont = $('.slideshine_thumbs', target);
					$('span[data-index]', thumbsCont).removeClass('slideshine_current_th');
					$('span[data-index='+current+']', thumbsCont).addClass('slideshine_current_th');

					var currentPos = $('span[data-index='+current+']', thumbsCont).position().left,
						currentOff = $('span[data-index='+current+']', thumbsCont).offset().left,
						currentW = $('span[data-index='+current+']', thumbsCont).width(),
						contOff = thumbsCont.offset().left,
						contW = thumbsCont.width();

					if ( currentOff < contOff || (currentOff+currentW) > (contOff+contW) ) {
						if ( moving === false ) {
							thumbsCont.animate({scrollLeft: currentPos}, optsSlide.speed, optsSlide.easing, function(){
								setTimeout(function(){
									moving = false;
								},10);
							});
						}
					}

				}

				if ( opts.bullets == 'true' ) {
					var bulletsCont = $('.slideshine_bullets', target);
					$('span[data-index]', bulletsCont).removeClass('slideshine_current_blt');
					$('span[data-index='+current+']', bulletsCont).addClass('slideshine_current_blt');
				}

				resizeBg(counter);

			} else {

				pause = true;

				checkAgain = setTimeout(function(){
					preloadBgs(counter, true);
					appearSlide(counter);
				},100);

			}

			$(optsprev).off('click');
			$(optsprev).on('click',function(){
				if(sliding === false) {
					dir = 'backward';
					nextSlide(counter-1);
					return false;
				}
			});

			$(optsnext).off('click');
			$(optsnext).on('click',function(){
				if(sliding === false) {
					nextSlide(counter+1);
					return false;
				}
			});

			$(optspause).off('click');
			$(optspause).on('click',function(){
				clearInterval(set);
				pause = true;
				if(sliding === false && Modernizr.csstransforms) {
					var orImg = slide.find(' > [data-opts] > img'),
						cssImg = orImg.css('transform'),
						cloneImg = orImg.clone();
					orImg.after(cloneImg).remove();
					cssImg = matrixToArray(cssImg);
					cloneImg.css({ scale: cssImg[0] });
				}
				$(this).addClass('opacity_0');
				$(optsplay).removeClass('opacity_0');
			});

			$(optsplay).off('click');
			$(optsplay).on('click',function(){
				pause = false;
				hover = false;
				runSlide(counter);
				$(this).addClass('opacity_0');
				$(optspause).removeClass('opacity_0');
                if (Modernizr.csstransforms && optsSlide.longfx!='none') { //the clone workaround is because of Transit doens't stop the css animations
                    var zoom;
                    if ( optsSlide.longfx == 'zoomout' ) {
                        zoom = { scale: 1.01 };
                    } else {
                        zoom = { scale: 1.35 };
                    }
                    slide.find('img').transition( zoom,((optsSlide.speed*3)+(optsSlide.timeout-timeSet)), optsSlide.easing); //scale: 1.01 fixes a bug in the browsers that don't support css transitions
                    //slide.find('img').css( zoom ); //scale: 1.01 fixes a bug in the browsers that don't support css transitions
                }
			});

			$(window).bind('resize',function(){
				drawProgress(timeSet*100);
				resizeBg(counter);
			});

		};

		var preloadImgs = function(img, adv){
			var imgLength = $(' > div', wrap).eq(img).find('div[data-opts] img').length,
				counterImg = 1;
			if ( imgLength === 0 ) {
				$('.slideshineSlide_'+(img-1), target).addClass('loaded');
				if ( adv === true ) {
					appearSlide(img-1);
					running = true;
				}

				//preloadBgs(img);
			} else {
				$(' > div', wrap).eq(img).find('div[data-opts] img').each(function(){
					if(!this.complete) {
						$(this).bind('load',function(){
							counterImg = counterImg + 1;
							if ( counterImg > imgLength && img < arrSlidesLenght ) {
								preloadBgs(img+1, adv);
							}
						});
					} else {
						$('.slideshineSlide_'+(img-1), target).addClass('loaded');
						if ( adv === true ) {
							running = true;
							appearSlide(img-1);
						}
						//preloadBgs(img);
					}
				});
			}
		};

		var preloadBgs = function(bg, adv){

			if ( adv === true ) {
				waiterSet = setTimeout(function(){
					optswaiter.removeClass('hiddenspinner').transition({opacity:1},250);
					if ( !pie_bg.parents('.ie9-').length ) {
						pie_bg.removeClass('opacity_0');
					}
					if ( pie_bg.hasClass('bordered') ) {
						pie_bg.removeClass('bordered').addClass('notBordered');
					}
				}, 100);
			}

			var imgUrl = arrSlides[bg];

			if ( typeof imgUrl !== 'undefined' && imgUrl !== false && imgUrl !== null ) {
				$('.slideshineSlide_'+bg, target);
				$('<img />').one('load',function(){
					if ( !$('.slideshineSlide_'+bg, target).find('div[data-opts] img').length ) {
						//$('.slideshineSlide_'+bg, target).find('div[data-opts]').html('<img src="'+imgUrl+'" class="slideshineBgs">');
						$('.slideshineSlide_'+bg, target).find('div[data-opts]').prepend('<img src="'+imgUrl+'" class="slideshineBgs">');
					}
					var link = $('.slideshineSlide_'+bg, target).find('div[data-opts]').data('opts').link,
						targetLink = $('.slideshineSlide_'+bg, target).find('div[data-opts]').data('opts').target !== '' ? ' target="_blank"' : '';
					if ( link !== '' ) {
						link = $('<a class="slideshine_100_link" href="' + link + '"' + targetLink + ' />');
						$('.slideshineSlide_'+bg, target).find('img').wrap(link);
					}
					bg = bg+1;
					preloadImgs(bg, adv);
				}).attr('src', imgUrl).each(function() {
					if(this.complete) {
						$(this).load();
					}
				});
			}
		};

		if ( opts.layout == 'fixed' ) {
			target.css({
				'height' : parseFloat(opts.height),
				'width' : parseFloat(opts.width)
			});
		} else if ( opts.layout == 'fixed2' ) {
			target.css({
				'width' : parseFloat(opts.width)
			});
			var localResize = function(){
				target.css({
					'height' : hFixed2
				});
			};
			localResize();
			$(window).bind('resize',function(){ localResize(); });
		} else if ( opts.layout == 'fitting' ) {
			var localResize = function(){
				var r = opts.width/opts.height,
					w = target.actual('width'),
					h = target.actual('width'),
					hPar = target.parent().actual('height'),
					scale;

				target.css({height:hPar});

				if ( w/hPar > opts.width/opts.height ) {
					scale = hPar/opts.height;
				} else {
					scale = w/opts.width;
				}

				var zoom = (100*scale)+'%';
				$('.slideshine_caption img', target).css({
					scale: scale,
					transformOrigin: '0 0'
				});
				$('.slideshine_caption iframe', target).each(function(){
					var attrW = $(this).attr('width'),
						attrH = $(this).attr('height');
					$(this).css({
						height: attrH*scale,
						width: attrW*scale
					});
				});
			
				var fontSize = target.width()*0.04 >= 30 ? target.width()*0.04 : 30;
				fontSize = fontSize <= 50 ? fontSize : 50;

				$('.slideshine_fontsized', target).css({
					fontSize: fontSize
				});
			};
			localResize();
			$(window).bind('resize',function(){ localResize(); });
		} else {
			var localResize = function(){
				var r = opts.width/opts.height,
					w = target.actual('width'),
					h = Math.floor(w / r);
				target.css({height:h});
				var scale = w/opts.width,
					zoom = (100*scale)+'%';
				$('.slideshine_caption img', target).css({
					scale: scale,
					transformOrigin: '0 0'
				});
				$('.slideshine_caption iframe', target).each(function(){
					var attrW = $(this).attr('width'),
						attrH = $(this).attr('height');
					$(this).css({
						height: attrH*scale,
						width: attrW*scale
					});
				});
			
				var fontSize = target.width()*0.04 >= 30 ? target.width()*0.04 : 30;
				fontSize = fontSize <= 50 ? fontSize : 50;

				$('.slideshine_fontsized', target).css({
					fontSize: fontSize
				});
			};
			localResize();
			$(window).bind('resize',function(){ localResize(); });

		}

		preloadBgs(bg, true);

		var response = function(){
		};

		$(window).bind('scroll',function(){
			if(isScrolledIntoViewHalf(target) && target.is(':visible')) {
				scrolled = false; target.removeClass('scrolled');
				opts.scrolledIntoView.call(this);
			} else {
				scrolled = true; target.addClass('scrolled');
				opts.scrolledOutOfView.call(this);
			}
		});
		$(window).triggerHandler('scroll');

		jQuery(window).bind('shortcodelic_ev', function(){
			if((target).is(':visible')) {
				scrolled = false; target.removeClass('scrolled');
				opts.scrolledIntoView.call(this);
			} else {
				scrolled = true; target.addClass('scrolled');
				opts.scrolledOutOfView.call(this);
			}
		}).triggerHandler('shortcodelic_ev');

	});

};})(jQuery);


jQuery(document).ready(function(){
	jQuery('.pix_slideshine').slideshine();
	jQuery(window).triggerHandler('resize');
});