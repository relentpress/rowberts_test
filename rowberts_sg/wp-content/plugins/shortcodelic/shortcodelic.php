<?php 
/*
Plugin Name: Shortcodelic
Plugin URI: http://www.pixedelic.com/plugins/shortcodelic
Description: So many shortcode into the same plugin
Version: 2.0.1
Author: Manuel Masia | Pixedelic.com
Author URI: http://www.pixedelic.com
License: GPL2
*/

define( 'SHORTCODELIC_PATH', plugin_dir_path( __FILE__ ) );
define( 'SHORTCODELIC_URL', plugin_dir_url( __FILE__ ) );
define( 'SHORTCODELIC_NAME', plugin_basename( __FILE__ ) );

require_once( SHORTCODELIC_PATH . 'lib/functions.php' );

register_activation_hook( __FILE__, array( 'ShortCodelic', 'activate' ) );
register_uninstall_hook( __FILE__, array( 'ShortCodelic', 'uninstall' ) );

ShortCodelic::get_instance();