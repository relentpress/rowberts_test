<script type="text/javascript">

	<?php echo FLFonts::js(); ?>
	<?php include FL_THEME_DIR . '/includes/presets.php'; ?>

	jQuery.extend(FLThemeSettings, {
	    homeUrl             : '<?php echo home_url(); ?>',
		presetThumbUrl 		: '<?php echo FL_THEME_URL; ?>/img/presets/',
		presets				: JSON.parse('<?php echo json_encode($fl_presets); ?>'),
		strings 			: {
			selected			: '<?php _e('Selected', 'fl-automator'); ?>',
			chooseSkin  		: '<?php _e('Choose Skin', 'fl-automator'); ?>',
			selectPhoto 		: '<?php _e('Select Photo', 'fl-automator'); ?>',
			preview  			: '<?php _e('Preview', 'fl-automator'); ?>'
		}
	});

	jQuery(function(){
        FLThemeSettings.init();
    });

</script>
<div class="wrap fl-settings">

	<h2><?php esc_attr_e('Theme Settings', 'fl-automator'); ?></h2>

	<?php foreach(FLAdmin::$messages as $message) : ?>
	<div class="<?php echo $message['type']; ?>">
	   <p><strong><?php echo $message['message']; ?></strong></p>
    </div>
	<?php endforeach; ?>
	
	<div class="fl-settings-nav">
        <ul>
            <?php FLAdmin::render_settings_menu(); ?>
        </ul>
		<div class="fl-settings-save">
        	<input type="submit" name="update" class="button-primary" value="<?php esc_attr_e('Save Settings', 'fl-automator'); ?>" />
		</div>
	</div>
	
	<div class="fl-settings-content">
	
        <form method="post" action="" enctype="multipart/form-data">
        
            <?php foreach(FLAdmin::$forms as $form_id => $form) : ?>
            
            <div id="fl-<?php echo $form_id; ?>-form" class="fl-settings-form">
        
                <h3 class="fl-settings-form-header"><?php echo $form['title']; ?></h3>
                
                <?php if(!empty($form['description'])) : // FORM DESCRIPTION ?>
                <p><?php echo $form['description']; ?></p>
                <?php endif; ?>
            
                <?php foreach($form['sections'] as $section_id => $section) : // SECTIONS LOOP ?>
                <div id="fl-<?php echo $section_id; ?>-settings-section" class="fl-settings-section">
                
                    <?php if(!empty($section['title'])) : // SECTION TITLE ?>
                    <h3 class="fl-settings-section-header"><?php echo $section['title']; ?></h3>
                    <?php endif; ?>
                    
                    <?php if(isset($section['fields'])) : ?>
                    <table class="form-table">
                    
                        <?php foreach($section['fields'] as $field_id => $field) : // FIELDS LOOP ?>
                        <tr id="fl-field-<?php echo $field_id; ?>" valign="top">
                            
                            <?php if(!empty($field['label'])) : // FIELD LABEL ?>
                    		<th scope="row">
                    			<label for="<?php echo $field_id; ?>"><?php echo $field['label']; ?></label>
                    			<?php if(!empty($field['help'])) : // FIELD TOOLTIP ?>
                    			<span class="fl-tooltip">
                                    <span class="fl-tooltip-icon"></span>
                                    <span class="fl-tooltip-text"><?php echo $field['help']; ?></span>
                    			</span>
                                <?php endif; ?>
                    		</th>
                            <?php endif; ?>
                                
                    		<td>
                    		
                        		<?php if($field['type'] == 'text') : // TEXT INPUT ?>
                            	<input name="<?php echo $field_id; ?>" type="text" value="<?php echo htmlspecialchars($settings->$field_id); ?>" <?php if(isset($field['size'])) echo ' class="fl-text-input-' . $field['size'] . '"'; ?> />
                                <?php endif; ?>
                                
                                <?php if($field['type'] == 'textarea') : // TEXTAREA ?>
                            	<textarea name="<?php echo $field_id; ?>" rows="<?php if(isset($field['rows'])) echo $field['rows']; else echo '8'; ?>"><?php echo htmlspecialchars($settings->$field_id); ?></textarea>
                                <?php endif; ?>
                                
                                <?php if($field['type'] == 'select') : // SELECT INPUT ?>
                    			<select name="<?php echo $field_id; ?>" data-saved-setting="<?php echo $settings->$field_id; ?>"<?php if(isset($field['toggle'])) echo " data-toggle='". json_encode($field['toggle']) ."'"; ?>>
                                    <?php foreach($field['options'] as $key => $val) : ?>
                                    <option value="<?php echo $key; ?>" <?php selected($settings->$field_id, $key); ?>><?php echo $val; ?></option>
                                    <?php endforeach; ?>
                    			</select>
                                <?php endif; ?>
                                
                                <?php if($field['type'] == 'checkbox') : // CHECKBOX INPUT ?>
                                <input name="<?php echo $field_id; ?>" type="checkbox" value="<?php echo $field['value']; ?>" <?php checked($settings->$field_id, $field['value']); ?> />
                                <?php endif; ?>
                                
                                <?php if($field['type'] == 'image') : // IMAGE INPUT ?>
                                <div class="fl-img-wrap <?php if(empty($settings->$field_id)) echo 'fl-img-empty'; ?>">
                        			<input class="fl-img-select" type="button" value="<?php _e('Select Photo', 'fl-automator'); ?>" />
                        			<img class="fl-img" src="<?php if(isset($settings->$field_id)) echo $settings->$field_id; ?>" />
                        			<input class="fl-img-replace" name="replace" type="button" value="Replace" />
                        			<input class="fl-img-value" name="<?php echo $field_id; ?>" type="hidden" value="<?php if(isset($settings->$field_id)) echo $settings->$field_id; ?>" />
                        		</div>
                                <?php endif; ?>
                                
                                <?php if($field['type'] == 'color-picker') : // COLOR PICKER ?>
                                <div class="fl-color-picker<?php if(empty($settings->$field_id)) echo ' fl-empty'; ?>">
                                    <div class="fl-color-picker-color"></div>
                                    <input name="<?php echo $field_id; ?>" type="hidden" value="<?php echo $settings->$field_id; ?>" class="fl-color-picker-value" />
                                    <?php if(isset($field['show_reset']) && $field['show_reset']) : ?>
                                    <div class="fl-color-picker-clear"></div>
                                    <?php endif; ?>
                                </div>
                                <?php endif; ?>
                                
                                <?php if($field['type'] == 'font') : // FONT ?>
                    			<select name="<?php echo $field_id; ?>" class="fl-font-select"<?php if(isset($field['weight'])) echo ' data-weight="'. $field['weight'] .'" '; ?>>
                                    <?php FLFonts::display_select_options($settings->$field_id); ?>
                    			</select>
                                <?php endif; ?>
                    		
                        		<?php if($field['type'] == 'hidden') : // HIDDEN INPUT ?>
                            	<input name="<?php echo $field_id; ?>" type="hidden" value="<?php echo htmlspecialchars($settings->$field_id); ?>" />
                                <?php endif; ?>
                                
                        		<?php if($field['type'] == 'file') : // FILE INPUT ?>
                            	<input name="<?php echo $field_id; ?>" type="file" value="" />
                                <?php endif; ?>
                                
                        		<?php if($field['type'] == 'button') : // BUTTON INPUT ?>
                            	<input name="<?php echo $field_id; ?>" type="button" value="<?php echo $field['value']; ?>" />
                                <?php endif; ?>
                                
                                <?php if($field['type'] == 'code') : // CODE ?>
                            	<textarea name="<?php echo $field_id; ?>" data-editor="<?php echo $field['editor']; ?>" rows="<?php if(isset($field['rows'])) echo $field['rows']; else echo '8'; ?>"><?php echo htmlspecialchars($settings->$field_id); ?></textarea>
                                <?php endif; ?>
                                
                                <?php if(!empty($field['help-after'])) : // HELP AFTER ?>
                                    <span class="fl-help-after"><?php echo $field['help-after']; ?></span>
                                <?php endif; ?>
                    		
                    		</td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                    <?php endif; ?>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endforeach; ?>
            <div class="fl-settings-save">
            	<input type="submit" name="update" class="button-primary" value="<?php esc_attr_e('Save Settings', 'fl-automator') ?>" />
            	<?php wp_nonce_field('_fl_update_settings', '_fl_update_settings_nonce'); ?>
            </div>
        </form>
    </div>
    <div class="clear"></div>
</div>