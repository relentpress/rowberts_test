<?php

if(class_exists('FLUpdater')) {
    FLUpdater::add_product(array(
        'name'      => 'Automator', 
        'version'   => '1.1.9', 
        'slug'      => 'fl-automator',
        'type'    	=> 'theme'
    ));
}