<?php

// Defines
define('FL_CHILD_THEME_DIR', get_stylesheet_directory());
define('FL_CHILD_THEME_URL', get_stylesheet_directory_uri());

// Classes
require_once 'classes/FLChildTheme.php';

// Actions
add_action('fl_head', 'FLChildTheme::stylesheet');

// Display 50 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 50;' ), 20 );

// Add product category description to shop page
add_action( 'woocommerce_after_subcategory_title', 'custom_add_product_description', 12);
function custom_add_product_description ($category) {
$cat_id        =    $category->term_id;
$prod_term    =    get_term($cat_id,'product_cat');
$description=    $prod_term->description;
echo '<div>'.$description.'</div>';
}